﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class BoardD : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private BoardDesc oInvMat = null;
        public Decimal fkbox = 0;
        private BoardDesc oBoxSp = new BoardDesc();
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sCustName = null;
            string sName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_BoardDesc> oInvMats = null;

            try { sCustName = p_Params.Single(p => p.ParameterName == "eId").ParameterValue; }
            catch { }
            //try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            //catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oInvMats = _oEntites.Vw_BoardDesc;

            try
            {
                if (!string.IsNullOrEmpty(sCustName))
                {
                    oInvMats = oInvMats.Where(p => p.Pk_Material==decimal.Parse(sCustName));
                } 
                if (!string.IsNullOrEmpty(sName))
                {
                    //oInvMats = oInvMats.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInvMats.Count();

            //oInvMats.OrderByDescending(p => p.Pk_BoxID);
            oInvMats = oInvMats.OrderByDescending(p => p.Pk_Material);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInvMats = oInvMats.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInvMats.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

   

  
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                Inv_Material oInvMat=WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Material),omValues) as Inv_Material;

                //BoardDesc oNewMaterialCategory = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoardDesc), omValues) as BoardDesc;

                //string sCategory = omValues["Category"].ToString();
                //decimal dCatId = _oEntites.Inv_MaterialCategory.Where(p => p.Name.ToLower() == sCategory.ToLower()).Single().Pk_MaterialCategory;

                //oNewMaterialCategory.Fk_MaterialCategory = dCatId;
                ////
                //oNewMaterialCategory.Fk_UnitId = 1;
                //_oEntites.AddToInv_Material(oNewMaterialCategory);

                if (omValues.ContainsKey("Parts"))
                {
                    string sBoxDetails = omValues["Parts"].ToString();
                    object[] sOBoxDetails = JsonConvert.DeserializeObject<object[]>(sBoxDetails);

                }

                //int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                //int branchVal = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
           

                //Stock oStockMaster = new Stock();
                //oStockMaster.Fk_BranchID = Convert.ToInt32(branchVal);
                //oStockMaster.Fk_Tanent = Convert.ToInt32(tannent);
                //oStockMaster.Quantity = 0;
                //oNewMaterialCategory.Stocks.Add(oStockMaster);





                //string sParts = omValues["parts"].ToString();
                //object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                //foreach (object oItem in Parts)
                //{
                //    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                //    ItemPartProperty oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ItemPartProperty), oProperties) as ItemPartProperty;
                //    oPart.Weight = Convert.ToDecimal(oProperties["weight"].ToString());
                //    oPart.NoBoards = 1;
                //    oPart.TakeUpFactor = Convert.ToDecimal(oProperties["TakeUpFactor"].ToString());



                //    string sLayers = oProperties["layers"].ToString();
                //    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);
                //    int i = 0;
                //    foreach (object oLayerItem in Layers)
                //    {
                //        //decimal deckel;
                //        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());

                //        oLayerProperties["rate"] = DBNull.Value;
                //    }


                //}


                //_oEntites.SaveChanges();
                //oResult.Success = true;
                //oResult.Message = oNewMaterialCategory.Pk_Material.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;


        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //BoardDesc oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoardDesc), omValues) as BoardDesc;
                //BoardDesc OPOrdFromShelf = _oEntites.BoardDesc.Where(p => p.Pk_BoxID == oPOrd.Pk_BoxID).Single();
                //object orefPO = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(BoardDesc), omValues, ref orefPO);


                //string sMaterials = omValues["BoxDetails"].ToString();
                //List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                //List<decimal> UpdatedPk = new List<decimal>();
                //List<BoxChild> NewMaterialsList = new List<BoxChild>();
                //foreach (object oItem in Materials)
                //{
                //    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                //    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                //    if (oDataProperties2["Pk_BoxCID"] == null || oDataProperties2["Pk_BoxCID"].ToString() == "")
                //    {
                //        BoxChild oNewPOrderD
                //            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxChild), oDataProperties2) as BoxChild;

                //        NewMaterialsList.Add(oNewPOrderD);
                //    }
                //    else
                //    {
                //        //Handel Update here
                //        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_BoxCID"]);
                //        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                //        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                //        //     gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);


                //        BoxChild oPOrderDetFromShelf = _oEntites.BoxChild.Where(p => p.Pk_BoxCID == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                //        object oPOrderDet
                //                            = _oEntites.BoxChild.Where(p => p.Pk_BoxCID == dPkPOrdDet).First();

                //        _oEntites.DeleteObject(oPOrderDet);


                //        BoxChild oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxChild), oDataProperties2) as BoxChild;

                //        NewMaterialsList.Add(oIndentDetails);
                //        UpdatedPk.Add(dPkPOrdDet);

                //    }


                //}
                // Handeling Deleted Records

                //List<BoxChild> oDeletedRecords = OPOrdFromShelf.BoxChilds.Where(p => !UpdatedPk.Contains(p.Pk_BoxCID)).ToList();



                ////Add new elements
                //foreach (BoxChild oNewMaterialDetail in NewMaterialsList)
                //{
                //    OPOrdFromShelf.BoxChilds.Add(oNewMaterialDetail);
                //}

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
            //    BoardDesc oBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoardDesc), omValues) as BoardDesc;
            //    BoardDesc oBoxFromShelf = _oEntites.BoardDesc.Where(p => p.Pk_BoxID == oBox.Pk_BoxID).Single();
            //    object orefItems = oBoxFromShelf;
            //    //_oEntites.DeleteObject(oEmployeesFromShelf);

            //    _oEntites.DeleteObject(oBoxFromShelf);

            //    _oEntites.SaveChanges();
            //    oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oInvMat;

            }
            set
            {
                oInvMat = value as BoardDesc;
            }
        }

        public decimal ID
        {
            get
            {
                return oInvMat.Pk_Value;
            }
            set
            {
                oInvMat = _oEntites.BoardDesc.Where(p => p.Pk_Value == value).Single();
            }
        
        }

        public object GetRaw()
        {
            return oInvMat;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


