﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class WireCert : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private WireCertificate oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Id = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<WireCertificate> oColors = null;

            try { sPk_Id = p_Params.Single(p => p.ParameterName == "Pk_Id").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.WireCertificate;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Id))
                {
                    oColors = oColors.Where(p => p.Pk_Id == decimal.Parse(sPk_Id));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oColors = oColors.Where(p => p.Inv_Material.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_Id);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchMaterialName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<WireCertificate> oMaterialName = null;
            oMaterialName = _oEntites.WireCertificate;

            oSearchResult.RecordCount = oMaterialName.Count();
            oMaterialName.OrderBy(p => p.Pk_Id);

            List<EntityObject> oFilteredItem = oMaterialName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                WireCertificate oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WireCertificate), omValues) as WireCertificate;

                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        WireCertificateDetails oNewPOrderD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WireCertificateDetails), oDataProperties) as WireCertificateDetails;

                        oNewPOrderD.Fk_Params = Convert.ToDecimal(oDataProperties["Pk_CharID"]);
                        //oNewPOrderD.Amount = (oNewPOrderD.Quantity * oNewPOrderD.Rate);

                        oNewColor.WireCertificateDetails.Add(oNewPOrderD);


                    }


                }
                _oEntites.AddToWireCertificate(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                WireCertificate oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WireCertificate), omValues) as WireCertificate;
                WireCertificate oColorFromShelf = _oEntites.WireCertificate.Where(p => p.Pk_Id == oColor.Pk_Id).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(WireCertificate), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                WireCertificate oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(WireCertificate), omValues) as WireCertificate;
                WireCertificate oColorFromShelf = _oEntites.WireCertificate.Where(p => p.Pk_Id == oColor.Pk_Id).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as WireCertificate;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_Id;
            }
            set
            {
                oColor = _oEntites.WireCertificate.Where(p => p.Pk_Id == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


