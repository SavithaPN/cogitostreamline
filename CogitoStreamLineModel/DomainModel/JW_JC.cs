﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using System.Data.SqlClient;
using System.Data;

namespace CogitoStreamLineModel.DomainModel
{
    public class JW_JC : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private JW_JobCardMaster oBoxSp = new JW_JobCardMaster();
        //private est_BoxEstimation oEstBox = new est_BoxEstimation();
        public decimal fkbox = 0;
        public decimal fkboxest = 0;
        public decimal TKF = 0;
        public decimal ply = 0;
       

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            decimal? sSpec = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sCustomer = null;
            string sVendor = null;
 string sFromJDate = null;
 string sPk_JobCardID = null;

            
        string sToJDate = null;
            string sSorting = null;


            IEnumerable<JW_JobCardMaster> oBoxSps = null;

            SearchResult oSearchResult = new SearchResult();
            try { sPk_JobCardID = p_Params.Single(p => p.ParameterName == "Pk_JobCardID").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            catch { }
            try { sFromJDate = p_Params.Single(p => p.ParameterName == "FromJDate").ParameterValue; }
            catch { }
            try { sToJDate = p_Params.Single(p => p.ParameterName == "ToJDate").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBoxSps = _oEntites.JW_JobCardMaster;

            try
            {
                //if (!string.IsNullOrEmpty(sCustomer))
                //{

                //    oBoxSps = oBoxSps.Where(p => p.BoxMaster.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sVendor))
                {
                    oBoxSps = oBoxSps.Where(p => p.gen_Vendor.VendorName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sPk_JobCardID))
                {

                    oBoxSps = oBoxSps.Where(p => p.Pk_JobCardID == Convert.ToDecimal(sPk_JobCardID));
                }
                if (!string.IsNullOrEmpty(sFromJDate))
                {
                    oBoxSps = oBoxSps.Where(p => (p.JDate >= Convert.ToDateTime(sFromJDate)));
                }
                if (!string.IsNullOrEmpty(sToJDate))
                {
                    oBoxSps = oBoxSps.Where(p => (p.JDate <= Convert.ToDateTime(sToJDate)));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oBoxSps.Count();
            oBoxSps = oBoxSps.OrderByDescending(p => p.Pk_JobCardID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBoxSps = oBoxSps.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oBoxSps.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        //public bool SetSpecId(decimal p_dSpec)
        //{
        //    if (_oEntites.BoxSpecs.Where(p => p.Fk_BoxID == p_dSpec).Count() > 0)
        //    {
        //        oBoxSp = _oEntites.JW_JobCardMasters.Where(p => p.Pk_JobCardID == p_dSpec).First();

        //        return true;
        //    }

        //    return false;
        //}

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        //public SearchResult SearchPaper(List<SearchParameter> p_Params)
        //{
        //    //We make a call that Search can only be done for Order based on 
        //    //First name, Last name and OrderName
        //    decimal? sGSM = null;
        //    decimal? sBF = null;
        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<Inv_Material> oBoxSps = null;

        //    SearchResult oSearchResult = new SearchResult();

        //    try { sGSM = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "GSM").ParameterValue); }
        //    catch { }
        //    try { sBF = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "BF").ParameterValue); }
        //    catch { }
        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }


        //    oBoxSps = _oEntites.JW_JobCardMasters;

        //    try
        //    {
        //        if (sGSM != null)
        //        {
        //            oBoxSps = oBoxSps.Where(p => p.GSM == sGSM);
        //        }
        //        if (sBF != null)
        //        {
        //            oBoxSps = oBoxSps.Where(p => p.BF == sBF);
        //        }

        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }
        //    oSearchResult.RecordCount = oBoxSps.Count();
        //    //oBoxSps = oBoxSps.OrderByDescending(p => p.Pk_BoxSpecID);


        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {

        //        var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //        var skip = int.Parse(sPageSize) * (page - 1);

        //        oBoxSps = oBoxSps.Select(p => p)
        //                            .Skip(skip)
        //                            .Take(int.Parse(sPageSize));
        //    }

        //    List<EntityObject> oFilteredOrders = oBoxSps.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredOrders;

        //    return oSearchResult;

        //}


        public ModelManuplationResult CreateNew()
        {
            // decimal GTotal = 0;

            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                if (!omValues.ContainsKey("JDate"))
                    omValues.Add("JDate", DateTime.Now);

                omValues.Add("Fk_Status", 1);
                //omValues.Add("ChkQuality", 0);


                JW_JobCardMaster oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_JobCardMaster), omValues) as JW_JobCardMaster;
                Dictionary<string, object> omValuesJProcess = new Dictionary<string, object>();
                decimal? CPly = 0;
                var BoxID = Convert.ToDecimal(omValues["Fk_BoxID"]);
                int RCount1 = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).Count();
                if (RCount1 > 0)
                {
                    CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                    Vw_BoxDet PlyVal = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).First();




                    oNewJob.JDate = Convert.ToDateTime(omValues["JDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    _oEntites.AddToJW_JobCardMaster(oNewJob);


                    _oEntites.SaveChanges();

                    oResult.Success = true;
                    oResult.Message = " JC No.-" + oNewJob.Pk_JobCardID.ToString() + " -ItemName- " + oNewJob.BoxMaster.Name + " -Prdn Qty- " + oNewJob.TotalQty + " -Customer- " + oNewJob.gen_Order.gen_Customer.CustomerName + " -Cust.PO.No.- " + oNewJob.gen_Order.Cust_PO;
                }
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            //int BType1 = 0;
            //decimal T1u = 1.5M;
            //decimal cuttingsize1, boardArea1, layerWeight1;
            //decimal deckel1 = 0;


            try
            {
                //omValues["Fk_BoxID"] is used to pass other parameters also
                string myString = omValues["Fk_BoxID"].ToString();
                string[] parts = myString.Split(',');
                decimal Pk_BoxEstChild = 0;
                bool bIsBoxSpecData = false;
                //If from boxspecification  only box data to be updated
                if (parts.Length != 1)
                {
                    bIsBoxSpecData = true;
                }
                //if from estimation both box and estimation data to be updated
                else
                {
                    //omValues["Fk_BoxID"] is used to pass other parameters also
                    //Hence resetting it with only FK_BoxID
                    omValues["Fk_BoxID"] = decimal.Parse(parts[0]);

                    string boxEstChildId = omValues["BoxEstimationChild"].ToString();
                    Pk_BoxEstChild = decimal.Parse(boxEstChildId);
                }

                //eId = parts[0];

                BoxSpec oBoxSp = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxSpec), omValues) as BoxSpec;
                //if (bIsBoxSpecData)
                //{
                decimal Specid = decimal.Parse(omValues["ID"].ToString());

                BoxSpec oBoxSpFromShelf = _oEntites.BoxSpecs.Where(p => p.Pk_BoxSpecID == Specid).Single();

                object orefBSpec = oBoxSpFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(BoxSpec), omValues, ref orefBSpec);

                string sParts = omValues["parts"].ToString();
                object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                foreach (object oItem in Parts)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    decimal id = decimal.Parse(oProperties["id"].ToString());
                    ItemPartProperty oItemPartFromShelf = _oEntites.ItemPartProperty.Where(p => p.Pk_PartPropertyID == id).Single();
                    oItemPartFromShelf.Weight = (int?)Convert.ToDecimal(oProperties["weight"].ToString());

                    object orefItemPart = oItemPartFromShelf;
                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ItemPartProperty), oProperties, ref orefItemPart);
                    //oItemPartFromShelf.Weight = int.Parse(oProperties["weight"].ToString());

                    string sLayers = oProperties["layers"].ToString();
                    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);
                    int i = 1;
                    foreach (object oLayerItem in Layers)
                    {

                        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                        decimal layertid = decimal.Parse(oLayerProperties["id"].ToString());
                        Items_Layers oItemLayerFromShelf = _oEntites.Items_Layers.Where(p => p.Pk_LayerID == layertid).Single();



                        object orefItemLayer = oItemLayerFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Items_Layers), oLayerProperties, ref orefItemLayer);

                    }


                }

                //}

                if (!bIsBoxSpecData)
                {

                    //string myString = omValues["Fk_BoxID"].ToString();
                    //string[] parts = myString.Split(',');
                    ////string eqchild = parts[2];
                    ////string boxEstId = parts[4];
                    //string boxEstChildId = parts[6];
                    ////eId = parts[0];

                    //decimal Pk_BoxEstChild = decimal.Parse(boxEstChildId);

                    est_BoxEstimationChild BoxEstChildFromShelf = _oEntites.est_BoxEstimationChild.Where(p => p.Pk_BoxEstimationChild == Pk_BoxEstChild).Single();

                    est_BoxEstimation oEstBoxFromShelf = BoxEstChildFromShelf.est_BoxEstimation;
                    //Subtrcting the old total price from grand total
                    oEstBoxFromShelf.GrandTotal = (oEstBoxFromShelf.GrandTotal == null ? 0 : oEstBoxFromShelf.GrandTotal) - (BoxEstChildFromShelf.TotalPrice == null ? 0 : BoxEstChildFromShelf.TotalPrice);

                    object orefBEstChild = BoxEstChildFromShelf;
                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_BoxEstimationChild), omValues, ref orefBEstChild);

                    decimal TotPrice = decimal.Parse(omValues["totalPrice"].ToString());
                    //Adding the updated total price to the grand total
                    oEstBoxFromShelf.GrandTotal = (oEstBoxFromShelf.GrandTotal == null ? 0 : oEstBoxFromShelf.GrandTotal) + TotPrice;
                }
                _oEntites.SaveChanges();
                oResult.Message = oBoxSp.Pk_BoxSpecID.ToString();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        public EntityObject DAO
        {
            get
            {
                return oBoxSp;
            }
            set
            {
                oBoxSp = value as JW_JobCardMaster;
            }
        }

        public decimal ID
        {
            get
            {
                return oBoxSp.Pk_JobCardID;
            }
            set
            {
                oBoxSp = _oEntites.JW_JobCardMaster.Where(p => p.Pk_JobCardID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBoxSp;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public EntityObject AddDetail(string sBasketName, EntityObject oDetail)
        {
            throw new NotImplementedException();
        }

        public EntityObject RemoveDetail(string sBasketName, decimal dPk)
        {
            throw new NotImplementedException();
        }

        public EntityObject UpdateDetail(string sBasketName, decimal dPk, Dictionary<string, object> oValues)
        {
            throw new NotImplementedException();
        }

        public List<EntityObject> GetDetails(string sBasketName)
        {
            throw new NotImplementedException();
        }
    }
}
