﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class COARep : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private COA oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First ParaName, Last ParaName and username
            string sPk_ID = null;
            string sFk_Invno = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<COA> oColors = null;

            try { sPk_ID = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            try { sFk_Invno = p_Params.Single(p => p.ParameterName == "Fk_Invno").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.COA;

            try
            {
                if (!string.IsNullOrEmpty(sPk_ID))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_ID));
                }
                if (!string.IsNullOrEmpty(sFk_Invno))
                {
                    oColors = oColors.Where(p => p.Fk_Invno == decimal.Parse(sFk_Invno));

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<COA> oName = null;
            oName = _oEntites.COA;

            oSearchResult.RecordCount = oName.Count();
            oName.OrderBy(p => p.Pk_ID);

            List<EntityObject> oFilteredItem = oName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

      
            try {

                COA oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(COA), omValues) as COA;

                    if (omValues.ContainsKey("COADetails"))
                    {
                        string sInvoiceDetails = omValues["COADetails"].ToString();
                        object[] COADetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetails);


                        foreach (object oDtetails in COADetails)
                        {
                            Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                            //Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                            COADetails oInvDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(COADetails), oProperties) as COADetails;

                            //oInvDetail.Fk_ParaID = Convert.ToDecimal(oProperties["Fk_ParaID"]);
                            //oInvDetail.Remarks =oProperties["Remarks"].ToString();


                            oNewColor.COADetails.Add(oInvDetail);
                     

                           

                        }
                      

                    }

                    //foreach (object oItem in Parts)
                    //{
                    //    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    //    ItemPartProperty oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ItemPartProperty), oProperties) as ItemPartProperty;


                    oNewColor.RDate = DateTime.Now;



                    _oEntites.AddToCOA(oNewColor);
                    _oEntites.SaveChanges();

                    oResult.Success = true;
                    oResult.Message = oNewColor.Pk_ID.ToString();
                
            }   
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                COA oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(COA), omValues) as COA;
                COA oColorFromShelf = _oEntites.COA.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(COA), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                COA oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(COA), omValues) as COA;
                COA oColorFromShelf = _oEntites.COA.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as COA;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_ID;
            }
            set
            {
                oColor = _oEntites.COA.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


