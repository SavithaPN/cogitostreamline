﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class OutSource : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Outsourcing oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sToDate = null;
            string sIssDate = null;
            string sFromDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Outsourcing> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_SrcID").ParameterValue; }
            catch { }
            try { sIssDate = p_Params.Single(p => p.ParameterName == "IssueDate").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.Outsourcing;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_SrcID == decimal.Parse(sPk_CharID));
                }
              
                //if (!string.IsNullOrEmpty(sIssDate))
                //{
                //    oColors = oColors.Where(p => (p.IssueDate == Convert.ToDateTime(sIssDate)));
                //}

                if (!string.IsNullOrEmpty(sFromDate))
                {

 DateTime SFrom = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                    oColors = oColors.Where(p => (p.IssueDate >= Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {

                    DateTime STo = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                   
                    oColors = oColors.Where(p => (p.IssueDate <= Convert.ToDateTime(STo)));
                }
                //if (!string.IsNullOrEmpty(sName))
                //{
                //    oColors = oColors.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_SrcID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchTask(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_OSTasks> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_SrcID").ParameterValue; }
            catch { }
            //try { sDate = p_Params.Single(p => p.ParameterName == "IssueDate").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.Vw_OSTasks;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_SrcID == decimal.Parse(sPk_CharID));
                }
              
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_SrcID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oColors = oColors.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Outsourcing> oName = null;
            oName = _oEntites.Outsourcing;

            oSearchResult.RecordCount = oName.Count();
            oName.OrderBy(p => p.Pk_SrcID);

            List<EntityObject> oFilteredItem = oName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Outsourcing oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Outsourcing), omValues) as Outsourcing;
                decimal fkMat = 0;
                if (omValues.ContainsKey("Materials"))
                {
                    string sIndentDetails = omValues["Materials"].ToString();
                    object[] IndentDetails = JsonConvert.DeserializeObject<object[]>(sIndentDetails);

                    oNewColor.Fk_Status = 1;

                    foreach (object oDtetails in IndentDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties["Fk_Material"].ToString() == "")
                        {
                            oDataProperties["Fk_Material"] = 110011;
                            fkMat = 110011;
                        }
                        //else
                        //{
                     
                        // fkMat =0;
                        //}
                       //var MatID= oDataProperties["Fk_Material"];

                        
                       // int Rcount1 = _oEntites.Inv_Material.Where(p => p.Pk_Material == MatID).Count();

                        //if (Rcount1 > 0)
                        //{ }
                        OutsourcingChild oOutDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(OutsourcingChild), oDataProperties) as OutsourcingChild;
                        oOutDetail.Ply = 1;
                        oNewColor.OutsourcingChild.Add(oOutDetail);



                        //////Stock///

                        //if (fkMat != 110011)
                        //{
                            //if (fkMat == 0)
                            //{
                            //    fkMat = 110011;
                            //}
                            //else
                            //{
                                int Rcount = _oEntites.Inv_Material.Where(p => p.Pk_Material == oOutDetail.Fk_Material).Count();
                                if (Rcount > 0)
                                {
                                    Inv_Material oInv_Master = _oEntites.Inv_Material.Where(p => p.Pk_Material == oOutDetail.Fk_Material).Single();

                                    if (oInv_Master.Fk_MaterialCategory != 4)
                                    {
                                        Stock oInv_StockMaster = _oEntites.Stocks.Where(p => p.Fk_Material == oOutDetail.Fk_Material).Single();
                                        oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity - oOutDetail.IssueWeight);
                                    }
                                    else
                                    {
                                        PaperStock oInv_StockMaster = _oEntites.PaperStock.Where(p => p.Fk_Material == oOutDetail.Fk_Material && p.RollNo == oOutDetail.ReelNo).Single();
                                        PaperStock oInv_StockMaster1 = _oEntites.PaperStock.Where(p => p.Fk_Material == oOutDetail.Fk_Material && p.RollNo == oOutDetail.ReelNo).Single();
                                        oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster1.Quantity - oOutDetail.IssueWeight);
                                    }
                        
                                }

                              

}
                    }


                oNewColor.IssueDate = Convert.ToDateTime(omValues["IssueDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                _oEntites.AddToOutsourcing(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Outsourcing oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Outsourcing), omValues) as Outsourcing;
                Outsourcing oColorFromShelf = _oEntites.Outsourcing.Where(p => p.Pk_SrcID == oColor.Pk_SrcID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Outsourcing), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Outsourcing oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Outsourcing), omValues) as Outsourcing;
                Outsourcing oColorFromShelf = _oEntites.Outsourcing.Where(p => p.Pk_SrcID == oColor.Pk_SrcID).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as Outsourcing;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_SrcID;
            }
            set
            {
                oColor = _oEntites.Outsourcing.Where(p => p.Pk_SrcID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


