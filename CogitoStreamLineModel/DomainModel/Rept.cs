﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Rept : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private ideActivity oBank = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_BankID = null;
            string sBankName = null;
            string sAccName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<ideActivity> oBanks = null;

            try { sPk_BankID = p_Params.Single(p => p.ParameterName == "Pk_BankID").ParameterValue; }
            catch { }
            try { sBankName = p_Params.Single(p => p.ParameterName == "BankName").ParameterValue; }
            catch { }
            try { sAccName = p_Params.Single(p => p.ParameterName == "AccountName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBanks = _oEntites.ideActivity;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BankID))
                {
                    oBanks = oBanks.Where(p => p.Pk_Activities == decimal.Parse(sPk_BankID));
                }
                //if (!string.IsNullOrEmpty(sBankName))
                //{
                //    oBanks = oBanks.Where(p => p.BankName.IndexOf(sBankName, StringComparison.OrdinalIgnoreCase) >= 0);

                //}
                //if (!string.IsNullOrEmpty(sAccName))
                //{
                //    oBanks = oBanks.Where(p => p.AccountName.IndexOf(sAccName, StringComparison.OrdinalIgnoreCase) >= 0);

                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oBanks.Count();

            oBanks.OrderBy(p => p.Pk_Activities);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBanks = oBanks.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oBanks.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchEnq(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Enquiry = null;
            string sCustomer = null;
            string sEnquiryFromDate = null;
            string sEnquiryToDate = null;
            string sCommunicationType = null;
            string sState = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            string sTanent = null;
            IEnumerable<eq_Enquiry> oEnquires = null;

            try { sPk_Enquiry = p_Params.Single(p => p.ParameterName == "Pk_Enquiry").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try
            { sEnquiryFromDate = p_Params.Single(p => p.ParameterName == "EnquiryFromDate").ParameterValue; }
            catch { }
            try
            { sEnquiryToDate = p_Params.Single(p => p.ParameterName == "EnquiryToDate").ParameterValue; }
            catch { }

            try { sCommunicationType = p_Params.Single(p => p.ParameterName == "CommunicationType").ParameterValue; }
            catch { }
            try { sState = p_Params.Single(p => p.ParameterName == "State").ParameterValue; }
            catch { }
            try { sTanent = HttpContext.Current.Session["Tanent"].ToString(); }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oEnquires = _oEntites.eq_Enquiry;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Enquiry))
                {
                    oEnquires = oEnquires.Where(p => p.Pk_Enquiry == decimal.Parse(sPk_Enquiry));
                }
                //if (!string.IsNullOrEmpty(sEnquiryDate))
                //{
                //    oEnquires = oEnquires.Where(p => p.Date == Convert.ToDateTime(sEnquiryDate));
                //}

                if (!string.IsNullOrEmpty(sEnquiryFromDate))
                {
                    oEnquires = oEnquires.Where(p => (p.Date >= Convert.ToDateTime(sEnquiryFromDate)));
                }
                if (!string.IsNullOrEmpty(sEnquiryToDate))
                {
                    oEnquires = oEnquires.Where(p => (p.Date <= Convert.ToDateTime(sEnquiryToDate)));
                }
                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oEnquires = oEnquires.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCommunicationType))
                {
                    oEnquires = oEnquires.Where(p => p.gen_Communication.Type.IndexOf(sCommunicationType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sState))
                {
                    oEnquires = oEnquires.Where(p => p.wfState.State.IndexOf(sState, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(sTanent))
                {
                    oEnquires = oEnquires.Where(p => p.Fk_Tanent == Convert.ToInt32(sTanent));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEnquires.Count();
            oEnquires = oEnquires.OrderByDescending(p => p.Pk_Enquiry);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEnquires = oEnquires.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredEnquires = oEnquires.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredEnquires;

            return oSearchResult;
        }

        public SearchResult SearchTon(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Enquiry = null;
            
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
          
            IEnumerable<TonnValues> oEnquires = null;

            try { sPk_Enquiry = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oEnquires = _oEntites.TonnValues;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Enquiry))
                {
                    oEnquires = oEnquires.Where(p => p.Pk_ID == decimal.Parse(sPk_Enquiry));
                }
                //if (!string.IsNullOrEmpty(sEnquiryDate))
                //{
                //    oEnquires = oEnquires.Where(p => p.Date == Convert.ToDateTime(sEnquiryDate));
                //}

            
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEnquires.Count();
            oEnquires = oEnquires.OrderByDescending(p => p.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEnquires = oEnquires.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredEnquires = oEnquires.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredEnquires;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideActivity oNewBank = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideActivity), omValues) as ideActivity;

                _oEntites.AddToideActivity(oNewBank);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideActivity oBank = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideActivity), omValues) as ideActivity;
                ideActivity oBankFromShelf = _oEntites.ideActivity.Where(p => p.Pk_Activities == oBank.Pk_Activities).Single();
                object orefItems = oBankFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ideActivity), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideActivity oBank = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideActivity), omValues) as ideActivity;
                ideActivity oBankFromShelf = _oEntites.ideActivity.Where(p => p.Pk_Activities == oBank.Pk_Activities).Single();
                object orefItems = oBankFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oBankFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oBank;

            }
            set
            {
                oBank = value as ideActivity;
            }
        }

        public decimal ID
        {
            get
            {
                return oBank.Pk_Activities;
            }
            set
            {
                oBank = _oEntites.ideActivity.Where(p => p.Pk_Activities == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBank;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


