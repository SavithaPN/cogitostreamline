﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class JCSamples : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Sample_JobCardMaster oJob = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            string sCustomer = null;
            string sBoxName = null;
            string sFromJDate = null;
            string sToJDate = null;
            string sCustomerOrder = null;
            string sFk_Order = null;
            string sInvNo = null;
            string sOnlyPending = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Sample_JobCardMaster> oJobCard = null;

            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try { sBoxName = p_Params.Single(p => p.ParameterName == "BoxName").ParameterValue; }
            catch { }
            try { sFk_Order = p_Params.Single(p => p.ParameterName == "Fk_Order").ParameterValue; }
            catch { }
            try { sFromJDate = p_Params.Single(p => p.ParameterName == "FromJDate").ParameterValue; }
            catch { }
            try { sToJDate = p_Params.Single(p => p.ParameterName == "ToJDate").ParameterValue; }
            catch { }

            try { sCustomerOrder = p_Params.Single(p => p.ParameterName == "CustomerOrder").ParameterValue; }
            catch { }
            try { sInvNo = p_Params.Single(p => p.ParameterName == "InvNo").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oJobCard = _oEntites.Sample_JobCardMaster;

            try
            {
                //if (!string.IsNullOrEmpty(sCustomer))
                //{

                //    oJobCard = oJobCard.Where(p => p.BoxMaster.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //}

                //if (!string.IsNullOrEmpty(sCustomerOrder))
                //{
                //    oJobCard = oJobCard.Where(p => p.gen_Order.Pk_Order == Convert.ToDecimal(sCustomerOrder));
                //}
                //if (!string.IsNullOrEmpty(sFk_Order))
                //{
                //    oJobCard = oJobCard.Where(p => p.gen_Order.Pk_Order == Convert.ToDecimal(sFk_Order));
                //}
                if (!string.IsNullOrEmpty(sInvNo))
                {
                    oJobCard = oJobCard.Where(p => p.Invno.Trim().ToUpper() == sInvNo.Trim().ToUpper());
                }

                if (!string.IsNullOrEmpty(sFromJDate))
                {
                    oJobCard = oJobCard.Where(p => (p.JDate >= Convert.ToDateTime(sFromJDate)));
                }
                if (!string.IsNullOrEmpty(sToJDate))
                {
                    oJobCard = oJobCard.Where(p => (p.JDate <= Convert.ToDateTime(sToJDate)));
                }
                //if (!string.IsNullOrEmpty(sOnlyPending))
                //{

                //    oJobCard = oJobCard.Where(p => p.wfStates.State.IndexOf(sOnlyPending, StringComparison.OrdinalIgnoreCase) >= 0);

                //}



            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oJobCard.Count();
            oJobCard = oJobCard.OrderByDescending(p => p.Pk_JobCardID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oJobCard = oJobCard.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oJobCard.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
        public SearchResult SearchPaperStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sFk_BoxID = null;
            string sFk_Enquiry = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_BoxPaperStock> oPaperStock = null;

            try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_BoxPaperStock;

            try
            {
                if (!string.IsNullOrEmpty(sFk_BoxID))
                {
                    oPaperStock = oPaperStock.Where(p => p.Fk_BoxID == decimal.Parse(sFk_BoxID));
                }
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oPaperStock = oPaperStock.Where(p => p.Fk_Enquiry == decimal.Parse(sFk_Enquiry));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchBoxPaper(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sFk_BoxID = null;
            string sFk_Enquiry = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_BoxPaper> oPaperStock = null;

            try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_BoxPaper;

            try
            {
                if (!string.IsNullOrEmpty(sFk_BoxID))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_BoxID == decimal.Parse(sFk_BoxID));
                }
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oPaperStock = oPaperStock.Where(p => p.Fk_Enquiry == decimal.Parse(sFk_Enquiry));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchJCPaperStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_PaperStock = null;
            string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PaperStock> oPaperStock = null;

            try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            catch { }
            try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.PaperStock;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PaperStock))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_PaperStock == decimal.Parse(sPk_PaperStock));
                }
                if (!string.IsNullOrEmpty(sFk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_PaperStock);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchAssignedStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //string sPk_PaperStock = null;
            string sPk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PStock> oPaperStock = null;

            //try { sPk_PaperStock = p_Params.Single(p => p.ParameterName == "Pk_PaperStock").ParameterValue; }
            //catch { }
            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oPaperStock = _oEntites.Vw_PStock;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oPaperStock = oPaperStock.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPaperStock.Count();
            oPaperStock = oPaperStock.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPaperStock = oPaperStock.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPaperStock.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            // decimal GTotal = 0;

            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                //if (!omValues.ContainsKey("JDate"))
                //    omValues.Add("JDate", DateTime.Now);

                omValues.Add("Fk_Status", 1);
                //omValues.Add("ChkQuality", 0);
                Sample_JobCardMaster oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Sample_JobCardMaster), omValues) as Sample_JobCardMaster;
                Dictionary<string, object> omValuesJProcess = new Dictionary<string, object>();
                decimal? CPly = 0;
                var BoxID = Convert.ToDecimal(omValues["Fk_BoxID"]);
                int RCount1 = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).Count();
                if (RCount1 > 0)
                {
                    CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                    Vw_BoxDet PlyVal = _oEntites.Vw_BoxDet.Where(p => p.Pk_BoxID == BoxID).First();

                    CPly = PlyVal.PlyVal;
                    if (CPly == 3)
                    {
                        CPly = 1;
                    }
                    if (CPly == 5)
                    {
                        CPly = 2;
                    }
                    if (CPly == 7)
                    {
                        CPly = 3;
                    }
                    if (CPly == 9)
                    {
                        CPly = 4;
                    }
                    if (CPly == 11)
                    {
                        CPly = 5;
                    }
                }


                string ProcessID = "";
                if (omValues.ContainsKey("Corr"))
                {
                    oNewJob.Corr = "Yes";
                    string pName = "Corrugation";

                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    // omValuesJProcess.Add("Fk_ProcessID", oPMast.Pk_Process);
                    // omValuesJProcess.Clear(); 

                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Corr = "No";
                }


                if (omValues.ContainsKey("TopP"))
                {
                    oNewJob.TopP = "Yes";
                    string pName = "TopPaper";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    //omValuesJProcess.Add("Fk_ProcessID", oPMast.Pk_Process);

                    //omValuesJProcess.Clear(); 
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.TopP = "No";
                }


                if (omValues.ContainsKey("Pasting"))
                {

                    oNewJob.Pasting = "Yes";
                    string pName = "Pasting";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    // omValuesJProcess.Add("Fk_ProcessID", oPMast.Pk_Process);
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }

                else
                {
                    oNewJob.Pasting = "No";
                }

                if (omValues.ContainsKey("Rotary"))
                {
                    oNewJob.Rotary = "Yes";
                    string pName = "Rotary";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Rotary = "No";
                }


                if (omValues.ContainsKey("PrintingP"))
                {
                    oNewJob.PrintingP = "Yes";
                    string pName = "Printing";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }

                else
                {
                    oNewJob.PrintingP = "No";
                }

                if (omValues.ContainsKey("Punching"))
                {
                    oNewJob.Punching = "Yes";
                    string pName = "Punching";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Punching = "No";
                }



                if (omValues.ContainsKey("Slotting"))
                {
                    oNewJob.Slotting = "Yes";
                    string pName = "Slotting";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Slotting = "No";
                }


                if (omValues.ContainsKey("Pinning"))
                {
                    oNewJob.Pinning = "Yes";
                    string pName = "Pinning";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                { oNewJob.Pinning = "No"; }

                if (omValues.ContainsKey("Gumming"))
                {
                    oNewJob.Gumming = "Yes";
                    string pName = "Gumming";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                { oNewJob.Gumming = "No"; }


                if (omValues.ContainsKey("Bundling"))
                {
                    oNewJob.Bundling = "Yes";
                    string pName = "Bundling";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                { oNewJob.Bundling = "No"; }

                if (omValues.ContainsKey("Finishing"))
                {
                    oNewJob.Finishing = "Yes";
                    string pName = "Finishing";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }
                else
                {
                    oNewJob.Finishing = "Yes";
                    string pName = "Finishing";
                    ProcessMaster oPMast = _oEntites.ProcessMaster.Where(p => p.ProcessName == pName).Single();
                    ProcessID = ProcessID + "`" + oPMast.Pk_Process;
                }

                object[] SelVal = ProcessID.Split('`');

                int i = 0;


                foreach (object oItem in SelVal)
                {

                    SampJProcess oJobProcess = new SampJProcess();
                    if (i > 0)
                    {
                        decimal SVal = Convert.ToDecimal(SelVal[i]);
                        if (SVal > 0)
                        {
                            int j = 0;
                            var CtrVal = 0;
                            if (SVal == 1)
                            {
                                for (j = 13; j <= 17; j++)
                                {
                                    if (CtrVal < CPly)
                                    {
                                        SampJProcess oJobProcess1 = new SampJProcess();
                                        oJobProcess1.Fk_ProcessID = j;
                                        oNewJob.SampJProcess.Add(oJobProcess1);
                                    }
                                    CtrVal = CtrVal + 1;

                                }
                                i = i + 1;
                            }
                            ///////////////////////////////////////////////////  

                            else if (SVal == 3)
                            {
                                oJobProcess.Fk_ProcessID = Convert.ToDecimal(SelVal[i]);
                                int k = 0;
                                var CtrVal1 = 0;
                                for (k = 18; k <= 24; k++)
                                {
                                    if (CtrVal1 < CPly)
                                    {
                                        SampJProcess oJobProcess1 = new SampJProcess();
                                        oJobProcess1.Fk_ProcessID = k;
                                        oNewJob.SampJProcess.Add(oJobProcess1);
                                    }
                                    CtrVal1 = CtrVal1 + 1;
                                }
                                i = i + 1;
                            }

                            else
                            {
                                oJobProcess.Fk_ProcessID = Convert.ToDecimal(SelVal[i]);

                                oNewJob.SampJProcess.Add(oJobProcess);
                                i = i + 1;
                            }

                        }
                    }
                    else
                    { i = i + 1; }
                }

                if (omValues.ContainsKey("JobDetails"))
                {
                    string sJobDetails = omValues["JobDetails"].ToString();
                    object[] JobDetails = JsonConvert.DeserializeObject<object[]>(sJobDetails);


                    foreach (object oDtetails in JobDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        Sample_JobCardDetails oJobDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Sample_JobCardDetails), oDataProperties) as Sample_JobCardDetails;
                        oJobDetails.RM_Consumed = Convert.ToDecimal(oDataProperties["Quantity"]);            ///Assigned Stock
                        oNewJob.Sample_JobCardDetails.Add(oJobDetails);
                    }
                }

                oNewJob.StartTime = "";
                oNewJob.EndTime = "";

             
                if (omValues.ContainsKey("Invno"))
                {
                    var AsPerVal = omValues["Invno"].ToString();


                }

                if (omValues.ContainsKey("Process"))
                {
                    string sEnquiryChilds = omValues["Process"].ToString();
                    object[] EnquiryChilds = JsonConvert.DeserializeObject<object[]>(sEnquiryChilds);

                    foreach (object oEqChild in EnquiryChilds)
                    {
                        SampJobProcess oJCProcess = new SampJobProcess();
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oEqChild.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        SampJobProcess oEnquiryChild = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampJobProcess), oDataProperties) as SampJobProcess;
                        //var BCode = oDataProperties[""].ToString();
                        //oEnquiryChild.TagNumber = oDataProperties["Barcode"].ToString();

                        //PriceList oPC = _oEntites.PriceList.Where(p => p.PLBarcode == BCode).Single();
                        //var PID = oPC.Pk_ID;
                        //ProductM oJC = _oEntites.ProductM.Where(p => p.Fk_Gen == PID).Single();
                        //oEnquiryChild.Fk_FrameID = oJC.Pk_PrdID;
                        // oJCProcess.Fk_JobCardID
                        oNewJob.SampJobProcess.Add(oEnquiryChild);
                    }
                }

                oNewJob.StartTime = "";
                oNewJob.EndTime = "";


                // var dateVal = DateTime.Now;
                oNewJob.JDate = Convert.ToDateTime(omValues["JDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                _oEntites.AddToSample_JobCardMaster(oNewJob);


                _oEntites.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewJob.Pk_JobCardID.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                decimal dFk_BoxID = decimal.Parse(omValues["Fk_BoxID"].ToString());
                decimal dFk_Order = decimal.Parse(omValues["Fk_Order"].ToString());
                BoxStock oBoxStock = _oEntites.BoxStock.Where(p => p.Fk_BoxID == dFk_BoxID).SingleOrDefault();

                Gen_OrderChild oGenChild = _oEntites.Gen_OrderChild.Where(p => p.Fk_BoxID == dFk_BoxID && p.Fk_OrderID == dFk_Order).SingleOrDefault();


                if (oBoxStock == null)
                {
                    oBoxStock = new BoxStock();
                    oBoxStock.Fk_BoxID = dFk_BoxID;
                    oBoxStock.Quantity = decimal.Parse(omValues["TotalQty"].ToString());
                    _oEntites.AddToBoxStock(oBoxStock);

                }
                else
                {
                    decimal oPk_JobCardID = decimal.Parse(omValues["Pk_JobCardID"].ToString());
                    Sample_JobCardMaster oJobFromShelf = _oEntites.Sample_JobCardMaster.Where(p => p.Pk_JobCardID == oPk_JobCardID).Single();
                    //subtract the old quantity
                    oBoxStock.Quantity = Convert.ToDecimal(oBoxStock.Quantity - oJobFromShelf.TotalQty);
                    //Adding the updated quantity
                    oBoxStock.Quantity = Convert.ToDecimal(oBoxStock.Quantity + decimal.Parse(omValues["TotalQty"].ToString()));
                }


                Sample_JobCardMaster oNewJob = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Sample_JobCardMaster), omValues) as Sample_JobCardMaster;

                //if (omValues["Fk_Status"].ToString()== "True")
                //{
                //    omValues["Fk_Status"] = 4;//Closed status

                //    oGenChild.Fk_Status = 4;
                //}
                //if (omValues["ChkQuality"].ToString() == "True")
                //{
                //    omValues["ChkQuality"] = 1;//Pass


                //}
                //else
                //    omValues["Fk_Status"] = 1;//New Status.

                //if (omValues["ChkQuality"].ToString() == "1")
                //{
                //    omValues["ChkQuality"] = 1;//Closed status


                //}
                //else
                //    omValues["ChkQuality"] = 0;//New Status.
                omValues["StartTime"] = omValues["StartTime"].ToString();
                omValues["EndTime"] = omValues["EndTime"].ToString();

                //omValues["StartDate"] = Convert.ToDateTime(omValues["StartDate"].ToString());
                //omValues["EndDate"] = Convert.ToDateTime(omValues["EndDate"].T
                Sample_JobCardMaster oNewJobFromShelf = _oEntites.Sample_JobCardMaster.Where(p => p.Pk_JobCardID == oNewJob.Pk_JobCardID).Single();
                object orefJobFromShelf = oNewJobFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Sample_JobCardMaster), omValues, ref orefJobFromShelf);

                List<decimal> oUpdateList = new List<decimal>();
                if (omValues.ContainsKey("JobDetails"))
                {
                    string sJobDetail = omValues["JobDetails"].ToString();
                    object[] sJobDetails = JsonConvert.DeserializeObject<object[]>(sJobDetail);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<Sample_JobCardDetails> oJobDetList = new List<Sample_JobCardDetails>();

                    foreach (object oIndent in sJobDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_JobCardDet") && oDataProperties["Pk_JobCardDet"] != null && oDataProperties["Pk_JobCardDet"].ToString() != "")
                        {
                            decimal Pk_JobCardDet = decimal.Parse(oDataProperties["Pk_JobCardDet"].ToString());

                            Sample_JobCardDetails oInvDet = _oEntites.Sample_JobCardDetails.Where(p => p.Pk_JobCardDet == Pk_JobCardDet).Single();

                            object orefSchedule = oInvDet;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Sample_JobCardDetails), oDataProperties, ref orefSchedule);
                            UpdatedPk.Add(Pk_JobCardDet);

                        }
                        else
                        {
                            Sample_JobCardDetails oInvDets = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Sample_JobCardDetails), oDataProperties) as Sample_JobCardDetails;
                            // oInvDets.RequiredDate = null;
                            oJobDetList.Add(oInvDets);
                        }
                    }

                    List<Sample_JobCardDetails> oDeletedRecords = oNewJobFromShelf.Sample_JobCardDetails.Where(p => !UpdatedPk.Contains(p.Pk_JobCardDet)).ToList();

                    foreach (Sample_JobCardDetails oDeletedDetail in oDeletedRecords)
                    {
                        oNewJobFromShelf.Sample_JobCardDetails.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (Sample_JobCardDetails oNewIndentDetail in oJobDetList)
                    {
                        oNewJobFromShelf.Sample_JobCardDetails.Add(oNewIndentDetail);
                    }

                }



                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        //public ModelManuplationResult Delete()
        //{
        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        Sample_JobCardMaster oOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Sample_JobCardMaster), omValues) as Sample_JobCardMaster;
        //        Sample_JobCardMaster oOrderFromShelf = _oEntites.Sample_JobCardMaster.Where(p => p.CustomerPO == oOrder.CustomerPO).Single();
        //        _oEntites.DeleteObject(oOrderFromShelf);
        //        _oEntites.SaveChanges();
        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oJob;
            }
            set
            {
                oJob = value as Sample_JobCardMaster;
            }
        }

        public decimal ID
        {
            get
            {
                return oJob.Pk_JobCardID;
            }
            set
            {
                oJob = _oEntites.Sample_JobCardMaster.Where(p => p.Pk_JobCardID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oJob;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }




        //public Dictionary<int, double> GetMonthWiseOrderCount()
        //{

        //    Dictionary<int, double> oResult = new Dictionary<int, double>();

        //    for (int i = 1; i <= 12; i++)
        //    {
        //        int Count = _oEntites.Sample_JobCardMaster.Where(p => p.OrderDate.Month == i).Count();

        //        oResult.Add(i, Count);
        //    }

        //    return oResult;
        //}

        //public Dictionary<string, int> GetOrderSatusCount()
        //{

        //    Dictionary<string, int> oResult = new Dictionary<string, int>();

        //    IEnumerable<wfState> oStates = _oEntites.wfStates;

        //    foreach (wfState oState in oStates)
        //    {
        //        int Count = _oEntites.Sample_JobCardMaster.Where(p => p.wfState.State == oState.State).Count();

        //        if (Count > 0)
        //        {
        //            oResult.Add(oState.State, Count);
        //        }
        //    }

        //    return oResult;
        //}

        //public Dictionary<string, int> GetDeliveryScheduleStatus()
        //{

        //    Dictionary<string, int> oResult = new Dictionary<string, int>();

        //    int MissedCount = _oEntites.gen_DeliverySchedule.Where(p => (p.DeliveryDate <= DateTime.Today && (p.DeliveryCompleted == null || p.DeliveryCompleted == false))).Count();

        //    int CompletedCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryCompleted == true).Count();

        //    int PendingCount = _oEntites.gen_DeliverySchedule.Where(p => (p.DeliveryDate > DateTime.Today && (p.DeliveryCompleted == null || p.DeliveryCompleted == false))).Count();

        //    oResult.Add("Pending", PendingCount);
        //    oResult.Add("Completed", CompletedCount);
        //    oResult.Add("Missed", MissedCount);

        //    return oResult;
        //}

        //public double OnTimeDeliveryPercentage()
        //{

        //    int TotalCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryDate < DateTime.Today).Count();

        //    int CompletedCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryCompleted == true && (p.DeliveryDate >= p.DeliveredDate)).Count();

        //    double oResult = TotalCount != 0 ? ((CompletedCount * 100) / TotalCount) : 0;

        //    return oResult;
        //}

        //public Dictionary<string, string> OrderDeliveries()
        //{
        //    Dictionary<string, string> oResult = new Dictionary<string, string>();
        //    //int Count = 0;
        //    //IEnumerable<Inv_Material> oMaterials = _oEntites.Inv_Material;
        //    DateTime todate = Convert.ToDateTime(DateTime.Today.AddDays(1));
        //    int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
        //    IEnumerable<vw_OrderDeliveryDetails> oOrder = _oEntites.vw_OrderDeliveryDetails.Where(p => p.DeliveryDate == todate && p.Fk_Tanent == tannent);
        //    //  IEnumerable<gen_Vendor> ovendor = _oEntites.gen_Vendor;
        //    int ss = 0;
        //    foreach (vw_OrderDeliveryDetails oOrd in oOrder)
        //    {
        //        oResult.Add(ss.ToString(), oOrd.CustomerName + "-" + oOrd.Quantity);
        //        ss++;
        //    }
        //    return oResult;
        //}


        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        //ModelManuplationResult IDomainObject.CreateNew()
        //{
        //    throw new NotImplementedException();
        //}

        EntityObject IDomainObject.DAO
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        ModelManuplationResult IDomainObject.Delete()
        {
            throw new NotImplementedException();
        }

        void IDomainObject.Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }



    }
}
