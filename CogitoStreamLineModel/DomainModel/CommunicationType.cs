﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class CommunicationType: IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_Communication> oCommunication = null;
            oCommunication = _oEntites.gen_Communication;

            oSearchResult.RecordCount = oCommunication.Count();
            oCommunication.OrderBy(p => p.Type);

            List<EntityObject> oFilteredCommunication = oCommunication.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredCommunication;

            return oSearchResult;
        }

        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult Update()
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public decimal ID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public object GetRaw()
        {
            throw new NotImplementedException();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
