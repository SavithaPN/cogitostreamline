﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class ActivitiesRepository
    {

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        //public SearchResult Search(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();

        //    //We make a call that Search can only be done for User based on 
        //    //First name, Last name and username
        //    string sPk_Activitys = null;
        //    string sActivityName = null;
        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<ideActivity> oActivitys = null;

        //    try { sPk_Activitys = p_Params.Single(p => p.ParameterName == "Pk_Activities").ParameterValue; }
        //    catch { }
        //    try { sActivityName = p_Params.Single(p => p.ParameterName == "ActivityName").ParameterValue; }
        //    catch { }

        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }


        //    oActivitys = _oEntites.ideActivities;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sPk_Activitys))
        //        {
        //            oActivitys = oActivitys.Where(p => p.Pk_Activities == decimal.Parse(sPk_Activitys));
        //        }
        //        if (!string.IsNullOrEmpty(sActivityName))
        //        {
        //            oActivitys = oActivitys.Where(p => p.ActivityName.IndexOf(sActivityName, StringComparison.OrdinalIgnoreCase) >= 0);
        //        }

        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }

        //    oSearchResult.RecordCount = oActivitys.Count();
        //    oActivitys.OrderBy(p => p.Pk_Activities);
        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {

        //        var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //        var skip = int.Parse(sPageSize) * (page - 1);

        //        oActivitys = oActivitys.Select(p => p)
        //                            .Skip(skip)
        //                            .Take(int.Parse(sPageSize));
        //    }

        //    List<EntityObject> oFilteredItems = oActivitys.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItems;
        //    return oSearchResult;
        //}


        public object SearchModules(string sUserName, string sModuleSeach)
        {
            List<ideActivity> oModule = new List<ideActivity>();
            //Now need to get the modules to display for the first screen
            ideRole oRole = _oEntites.ideUser.
                                First(p => p.UserName.Equals(sUserName, StringComparison.InvariantCultureIgnoreCase))
                                .ideRoleUser.First().ideRole;
            IEnumerable<ideActivityPermission> oPermittedModiles = oRole.ideActivityPermission.Where(p => p.ideActivity.Fk_ParentActivity != null && ((p.ideActivity.ActivityName != null && p.ideActivity.ActivityName.IndexOf(sModuleSeach, StringComparison.OrdinalIgnoreCase) >= 0)));
                IEnumerable<ideActivity> oAllModules = oPermittedModiles.Select(p => p.ideActivity);

            foreach (ideActivity otempModule in oAllModules)
            {
                oModule.Add(otempModule);

            }


            var oModulesToDisplay = oModule.Select(a => new
            {
                Pk_Activities = a.Pk_Activities,
                ActivityName = a.ActivityName,
                //Description = a.Description,
                Link = a.Link,
                Image = a.ImageName
                //Style = a.Style
            }).ToList();

            return oModulesToDisplay;
        }

        public List<ideActivity> GetHomePageActivities(string sUserName)
        {
            List<ideActivity> oActivity = new List<ideActivity>();
            //Now need to get the Activitys to display for the first screen
            
            

            ideRole oRole = _oEntites.ideUser.
                                  First(p => p.UserName.Equals(sUserName, StringComparison.InvariantCultureIgnoreCase))
                                  .ideRoleUser.First().ideRole;
            IEnumerable<ideActivityPermission> oPermittedActivities = oRole.ideActivityPermission;

            IEnumerable<ideActivity> oAllActivities = oPermittedActivities.Select(p => p.ideActivity).OrderBy(p => p.OrderBy);


            foreach (ideActivity otempActivity in oAllActivities)
            {
                if (otempActivity.Fk_ParentActivity == null)
                {
                    oActivity.Add(otempActivity);
                }
            }

            return oActivity;
            
        }
        public List<ideActivity> GetSubActivities(decimal id, string sUserName)
        {
            List<ideActivity> oActivity = new List<ideActivity>();
            //Now need to get the Activitys to display for the first screen
            ideRole oRole = _oEntites.ideUser.
                                First(p => p.UserName.Equals(sUserName, StringComparison.InvariantCultureIgnoreCase))
                                .ideRoleUser.First().ideRole;

            IEnumerable<ideActivityPermission> oPermittedModiles = oRole.ideActivityPermission;

            IEnumerable<ideActivity> oAllActivitys = oPermittedModiles.Select(p => p.ideActivity).OrderBy(p=>p.OrderBy);


            foreach (ideActivity otempActivity in oAllActivitys)
            {
                if (otempActivity.Fk_ParentActivity == id)
                {
                    oActivity.Add(otempActivity);
                }

            }

            return oActivity;
        }
    }
}






