﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class DispatchM
//    {
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;
using System.Data;

namespace CogitoStreamLineModel.DomainModel
{
    public class DispatchM : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private DispatchMast oInv_Billing = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_Invoice = null;
            string sInvno = null;
            string sFromDate = null;
            string sToDate = null;
            string sCustomer = null;
            string sOrderno = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<DispatchMast> oInv_Billings = null;

            try { sPk_Invoice = p_Params.Single(p => p.ParameterName == "Pk_Dispatch").ParameterValue; }
            catch { }
            try { sInvno = p_Params.Single(p => p.ParameterName == "Invno").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "Fk_Customer").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "TxtFromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "TxtToDate").ParameterValue; }
            catch { }
            try { sOrderno = p_Params.Single(p => p.ParameterName == "OrderNo").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oInv_Billings = _oEntities.DispatchMast;
            // DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Invoice))
                {
                    oInv_Billings = oInv_Billings.Where(p => p.Pk_Dispatch == decimal.Parse(sPk_Invoice));
                }

                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oInv_Billings = oInv_Billings.Where(p => p.Fk_Customer != null && p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    DateTime FrmDate = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oInv_Billings = oInv_Billings.Where(p => (p.Dis_Date >= Convert.ToDateTime(FrmDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    DateTime ToDate = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oInv_Billings = oInv_Billings.Where(p => (p.Dis_Date <= Convert.ToDateTime(ToDate)));
                }
                //if (!string.IsNullOrEmpty(sOrderno))
                //{
                //    oInv_Billings = oInv_Billings.Where(p => p.Fk_OrderNo == decimal.Parse(sOrderno));
                //}
                //if (!string.IsNullOrEmpty(sInvno))
                //{
                //    oInv_Billings = oInv_Billings.Where(p => p.Invno.IndexOf(sInvno, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oInv_Billings.Count();
            oInv_Billings = oInv_Billings.OrderByDescending(p => p.Pk_Dispatch);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInv_Billings = oInv_Billings.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInv_Billings.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public ModelManuplationResult CreateNew()
        {
            decimal GTotal = 0;

            double t1 = 0.06;
            decimal EDVAL = 0;

            decimal NValue = 0;
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                DispatchMast oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchMast), omValues) as DispatchMast;

                if (omValues.ContainsKey("DispatchDetails"))
                {
                    string sInvoiceDetails = omValues["DispatchDetails"].ToString();
                    object[] DispatchDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetails);


                    foreach (object oDtetails in DispatchDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        DispatchChild oInvDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchChild), oDataProperties) as DispatchChild;

                        //var TType = omValues["TaxType"];

                        //Tax oTax = _oEntities.Tax.Where(p => p.TaxName == TType).Single();

                        //oInvDetail.Fk_TaxID = oTax.PkTax;
                        oNewInv.DispatchChild.Add(oInvDetail);
                        //GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oInvDetail.NetAmount);
                        //EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);



                    }
                    //NValue = (Convert.ToDecimal(oNewInv.NETVALUE));

                }


              //  oNewInv.Dis_Date = DateTime.Now;

                oNewInv.Dis_Date = Convert.ToDateTime(omValues["Dis_Date"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                //oNewInv.Cancel = false;
                //oNewInv.GrandTotal = GTotal;
                //oNewInv.NETVALUE = NValue;
                //oNewInv.ED = EDVAL;


                _oEntities.AddToDispatchMast(oNewInv);
                _oEntities.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewInv.Pk_Dispatch.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        //public ModelManuplationResult Update()
        //{
        //    decimal GTotal = 0;
        //    double t2 = 0.055;
        //    double t1 = 0.06;
        //    double t3 = 0.02;
        //    decimal nTotal = 0;
        //    decimal nTotal1 = 0;
        //    decimal NValue = 0;
        //    string ttax = "";
        //    decimal EDVAL = 0;
        //    bool FForm;
        //    bool CanValue ;

        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        DispatchMast oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchMast), omValues) as DispatchMast;
        //        DispatchMast oNewInvFromShelf = _oEntities.DispatchMast.Where(p => p.Pk_Dispatch == oNewInv.Pk_Dispatch).Single();

        //        ttax = oNewInv.TaxType.TrimEnd();

        //        if (oNewInv.FORM_H != null && oNewInv.FORM_H == true)
        //        {
        //            FForm = true;
        //        }
        //        else
        //        { FForm = false; }

        //       //if (oNewInv.Cancel ==1)
        //       // {
        //       //     CanValue =true;
        //       // }
        //       //   else
        //       //{
        //       //    CanValue = false;
        //       //}

        //        object orefInvFromShelf = oNewInvFromShelf;
        //        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(DispatchMast), omValues, ref orefInvFromShelf);



        //        List<decimal> oUpdateList = new List<decimal>();
        //        if (omValues.ContainsKey("DispatchDetails"))
        //        {
        //            string sInvoiceDetail = omValues["DispatchDetails"].ToString();
        //            object[] sInvoiceDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetail);
        //            List<decimal> UpdatedPk = new List<decimal>();
        //            List<DispatchChild> oInvDetList = new List<DispatchChild>();

        //            foreach (object oIndent in sInvoiceDetails)
        //            {
        //                Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
        //                Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

        //                if (oDataProperties.ContainsKey("Pk_DispDet") && oDataProperties["Pk_DispDet"] != null && oDataProperties["Pk_DispDet"].ToString() != "")
        //                {
        //                    decimal Pk_DispDet = decimal.Parse(oDataProperties["Pk_DispDet"].ToString());
        //                    //Handel Update here
        //                    //decimal dPkMIndentDetail = Convert.ToDecimal(oDataProperties["Pk_MaterialOrderDetailsId"]);
        //                    decimal dFkBox = Convert.ToDecimal(oDataProperties["Fk_BoxID"]);
        //                    //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

        //                    DispatchChild oIndentDetailsFromShelf = _oEntities.DispatchChild.Where(p => p.Pk_DispDet == Pk_DispDet && p.Fk_BoxID == dFkBox).Single();

        //                    object oIndentDetails
        //                                        = _oEntities.DispatchChild.Where(p => p.Pk_DispDet == Pk_DispDet).First();



        //                    UpdatedPk.Add(Pk_DispDet);
        //                    ///                           /object orefSchedule = oIndentFromShelf;
        //                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(DispatchChild), oDataProperties, ref oIndentDetails);
        //                //    DispatchChild oInvDet = _oEntities.DispatchChild.Where(p => p.Pk_DispDet == Pk_DispDet).Single();

        //                //    object orefSchedule = oInvDet;
        //                //    GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oDataProperties["NetAmount"]);

        //                //    EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);

        //                //    if (ttax == "VAT")
        //                //    {


        //                //        if (FForm == true)
        //                //        {
        //                //            NValue = (Convert.ToDecimal(GTotal));
        //                //        }
        //                //        else
        //                //        {
        //                //            nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                //            nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t2);
        //                //            NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                //        }



        //                //    }

        //                //    if (ttax == "CST")
        //                //    {
        //                //        nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                //        nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t3);
        //                //        NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                //    }

        //                    //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(DispatchChild), oDataProperties, ref orefSchedule);
        //                    //UpdatedPk.Add(Pk_DispDet);

        //                //}

        //                //else
        //                //{
        //                //    DispatchChild oInvDets = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchChild), oDataProperties) as DispatchChild;

        //                //    GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oInvDets.NetAmount);
        //                //    oInvDetList.Add(oInvDets);


        //                //    GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oDataProperties["NetAmount"]);

        //                //    EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);

        //                //    if (ttax == "VAT")
        //                //    {


        //                //        if (FForm == true)
        //                //        {
        //                //            NValue = (Convert.ToDecimal(GTotal));
        //                //        }
        //                //        else
        //                //        {
        //                //            nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                //            nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t2);
        //                //            NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                //        }



        //                //    }

        //                    //if (ttax == "CST")
        //                    //{
        //                    //    nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                    //    nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t3);
        //                    //    NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                    //}

        //                }

        //            }

        //            List<DispatchChild> oDeletedRecords = oNewInvFromShelf.DispatchChild.Where(p => !UpdatedPk.Contains(p.Pk_DispDet)).ToList();

        //            foreach (DispatchChild oDeletedDetail in oDeletedRecords)
        //            {
        //                oNewInvFromShelf.DispatchChild.Remove(oDeletedDetail);
        //            }

        //            //Add new elements
        //            foreach (DispatchChild oNewIndentDetail in oInvDetList)
        //            {
        //                oNewInvFromShelf.DispatchChild.Add(oNewIndentDetail);
        //            }

        //        }
        //        oNewInv.GrandTotal = GTotal;
        //        oNewInvFromShelf.GrandTotal = GTotal;

        //        oNewInv.NETVALUE = NValue;
        //        oNewInvFromShelf.NETVALUE = NValue;
        //        //oNewInv.Cancel = CanValue;
        //        oNewInv.ED = EDVAL;
        //        oNewInvFromShelf.ED = EDVAL;

        //        //oNewInv.DNTimeofRemoval = DateTime.Now;
        //        //oNewInvFromShelf.DNTimeofRemoval = DateTime.Now; 
        //        //oNewInv.DNTimeofInv = DateTime.Now;
        //        //oNewInvFromShelf.DNTimeofInv = DateTime.Now;


        //        _oEntities.SaveChanges();

        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                DispatchMast oMaterialIndent = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchMast), omValues) as DispatchMast;
                DispatchMast oMaterialIndentFromShelf = _oEntities.DispatchMast.Where(p => p.Pk_Dispatch == oMaterialIndent.Pk_Dispatch).Single();
                object orefMaterialIndentFromShelf = oMaterialIndentFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(DispatchMast), omValues, ref orefMaterialIndentFromShelf);
                _oEntities.SaveChanges();
                //List<decimal> oUpdateList = new List<decimal>();
                //if (omValues.ContainsKey("DispatchDetails"))
                //{
                string sMaterialIndent = omValues["DispatchDetails"].ToString();
                object[] sMaterialIndents = JsonConvert.DeserializeObject<object[]>(sMaterialIndent);
                List<decimal> UpdatedPk = new List<decimal>();
                List<DispatchChild> NewIndentList = new List<DispatchChild>();

                foreach (object oIndent in sMaterialIndents)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                    Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                    if (oDataProperties.ContainsKey("Pk_DispDet") && oDataProperties["Pk_DispDet"] != null && oDataProperties["Pk_DispDet"].ToString() != "")
                    {
                        decimal Pk_DispDet = decimal.Parse(oDataProperties["Pk_DispDet"].ToString());



                        //Handel Update here
                        decimal dPkMIndentDetail = Convert.ToDecimal(oDataProperties["Pk_DispDet"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties["Fk_BoxID"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        DispatchChild oIndentDetailsFromShelf = _oEntities.DispatchChild.Where(p => p.Pk_DispDet == dPkMIndentDetail && p.Fk_BoxID == dFkMaterial).Single();

                        object oIndentDetails
                                            = _oEntities.DispatchChild.Where(p => p.Pk_DispDet == dPkMIndentDetail).First();


                        UpdatedPk.Add(dPkMIndentDetail);
                        ///                           /object orefSchedule = oIndentFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(DispatchChild), oDataProperties, ref oIndentDetails);
                        // UpdatedPk.Add(Pk_DispDet);

                        //add a new item into the PendingTrack` here




                        decimal FkMaterial = 0;
                        decimal IndQty = 0;

                        if (oDataProperties.ContainsKey("Fk_BoxID").ToString() != "")
                        {
                            FkMaterial = decimal.Parse(oDataProperties["Fk_BoxID"].ToString());
                            IndQty = decimal.Parse(oDataProperties["Quantity"].ToString());
                        }

                        var TAmt = Convert.ToDouble(oDataProperties["TotalAmount"].ToString());
                        var TaxVal = 0.06;
                        var AftTaxAmt = Convert.ToDouble(TAmt * TaxVal);

                        //oMaterialIndentFromShelf.GrandTotal = decimal.Parse(oDataProperties["TotalAmount"].ToString());
                        //oMaterialIndentFromShelf.NETVALUE = Convert.ToDecimal(TAmt) + Convert.ToDecimal(AftTaxAmt);



                        _oEntities.SaveChanges();
                        //help
                        ///error occuring here on edit save - for a new product added during edit mode--because there is no fk material in pending track table

                        //PendingTrack oPendingFromShelf = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oMaterialIndentFromShelf.Pk_Dispatch && p.Fk_BoxID == FkMaterial).Single();

                        //oPendingFromShelf.Quantity = IndQty;

                        //oPendingFromShelf.Pending = IndQty;

                    }
                    //else
                    //{



                    //    DispatchChild oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchChild), oDataProperties) as DispatchChild;
                    //    NewIndentList.Add(oIndentDetails);


                    //    PendingTrack oPendingNew = new PendingTrack();

                    //    oPendingNew.Fk_Indent = Convert.ToDecimal(oIndentDetails.Fk_MaterialOrderMasterId);
                    //    oPendingNew.Fk_BoxID = Convert.ToDecimal(oIndentDetails.Fk_BoxID);
                    //    oPendingNew.Quantity = Convert.ToDecimal(oIndentDetails.Quantity);
                    //    oPendingNew.AlreadyInwarded = 0;
                    //    oPendingNew.Pending = Convert.ToDecimal(oIndentDetails.Quantity);  //////////////////CHECK 
                    //    oPendingNew.QC_Qty = 0;



                    //}
                }

                List<DispatchChild> oDeletedRecords = oMaterialIndentFromShelf.DispatchChild.Where(p => !UpdatedPk.Contains(p.Pk_DispDet)).ToList();

                foreach (DispatchChild oDeletedDetail in oDeletedRecords)
                {
                    oMaterialIndentFromShelf.DispatchChild.Remove(oDeletedDetail);
                }

                //Add new elements
                foreach (DispatchChild oNewIndentDetail in NewIndentList)
                {
                    oMaterialIndentFromShelf.DispatchChild.Add(oNewIndentDetail);
                }

                //}
                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }




        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                DispatchMast oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(DispatchMast), omValues) as DispatchMast;
                DispatchMast oNewInvFromShelf = _oEntities.DispatchMast.Where(p => p.Pk_Dispatch == oNewInv.Pk_Dispatch).Single();
                oNewInvFromShelf.DispatchChild.Clear();
                _oEntities.DeleteObject(oNewInvFromShelf);

                /////////////////////////////////////


                List<decimal> oUpdateList = new List<decimal>();
                if (omValues.ContainsKey("DispatchDetails"))
                {
                    string sInvoiceDetail = omValues["DispatchDetails"].ToString();
                    object[] sInvoiceDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetail);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<DispatchChild> oInvDetList = new List<DispatchChild>();

                    foreach (object oIndent in sInvoiceDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        decimal Pk_DispDet = decimal.Parse(oDataProperties["Pk_DispDet"].ToString());

                    }

                    //////////////////////////////////////////
                    _oEntities.SaveChanges();
                    oResult.Success = true;
                }
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public EntityObject DAO
        {
            get
            {
                return oInv_Billing;
            }
            set
            {
                oInv_Billing = value as DispatchMast;
            }
        }


        public decimal ID
        {
            get
            {
                return oInv_Billing.Pk_Dispatch;
            }

            set
            {
                oInv_Billing = _oEntities.DispatchMast.Where(p => p.Pk_Dispatch == value).Single();
            }
        }

        public object GetRaw()
        {
            return oInv_Billing;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Dictionary<string, string> MonthlySalesStatus()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);

            decimal oCurrrentmonth = Convert.ToDecimal(_oEntities.vw_MonthlySalesStatus.Where(p => p.Months == ((DateTime.Now.Month) - 1) && p.Years == DateTime.Now.Year).Sum(s => s.GrandTotal));
            decimal opreviousmonth = Convert.ToDecimal(_oEntities.vw_MonthlySalesStatus.Where(p => p.Months == ((DateTime.Now.Month) - 2) && p.Years == DateTime.Now.Year).Sum(s => s.GrandTotal));

            if (opreviousmonth > oCurrrentmonth)
            {
                oResult.Add("0", "Improve the Sales");
            }
            return oResult;
        }

    }
}
