﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class PaperCert : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private PaperCertificate oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Id = null;
            string sName = null;
            string sVName = null;
            string sPONO = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PaperCertificate> oColors = null;

            try { sPk_Id = p_Params.Single(p => p.ParameterName == "Pk_Id").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sVName = p_Params.Single(p => p.ParameterName == "VName").ParameterValue; }
            catch { }
            try { sPONO = p_Params.Single(p => p.ParameterName == "PoNoDisplay").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.PaperCertificate;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Id))
                {
                    oColors = oColors.Where(p => p.Pk_Id == decimal.Parse(sPk_Id));
                }

                if (!string.IsNullOrEmpty(sName))
                {
                    oColors = oColors.Where(p => p.Inv_Material.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sPONO))
                {
                    oColors = oColors.Where(p => p.PurchaseOrderM.PkDisp.IndexOf(sPONO, StringComparison.OrdinalIgnoreCase) >= 0);
                    //oColors = oColors.Where(p => p.PurchaseOrderM.Pk_PONo == decimal.Parse(sPONO));
                }
                if (!string.IsNullOrEmpty(sVName))
                {
                    oColors = oColors.Where(p => p.gen_Vendor.VendorName.IndexOf(sVName, StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_Id);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchMaterialName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<PaperCertificate> oMaterialName = null;
            oMaterialName = _oEntites.PaperCertificate;

            oSearchResult.RecordCount = oMaterialName.Count();
            oMaterialName.OrderBy(p => p.Pk_Id);

            List<EntityObject> oFilteredItem = oMaterialName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PaperCertificate oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificate), omValues) as PaperCertificate;
                decimal MValue = 0;
                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        PaperCertificateDetails oNewPOrderD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificateDetails), oDataProperties) as PaperCertificateDetails;
                      
                        
                        if (Convert.ToDecimal(oDataProperties["Pk_CharID"]) == 2 || Convert.ToDecimal(oDataProperties["Pk_CharID"]) == 5)
                        {
                             MValue = Convert.ToDecimal(oNewPOrderD.Result1);
                            oNewPOrderD.Result1 = Math.Round(MValue, 2).ToString();

                            if (oNewPOrderD.Result2.Length > 0)
                            {
                                MValue = Convert.ToDecimal(oNewPOrderD.Result2);
                                oNewPOrderD.Result2 = Math.Round(MValue, 2).ToString();
                            }

                            if (oNewPOrderD.Result3.Length > 0)
                            {

                                MValue = Convert.ToDecimal(oNewPOrderD.Result3);
                                oNewPOrderD.Result3 = Math.Round(MValue, 2).ToString();
                            }

                            if (oNewPOrderD.Result4.Length > 0)
                            {
                                MValue = Convert.ToDecimal(oNewPOrderD.Result4);
                                oNewPOrderD.Result4 = Math.Round(MValue, 2).ToString();
                            }

                            if (oNewPOrderD.Result5.Length > 0)
                            {
                                MValue = Convert.ToDecimal(oNewPOrderD.Result5);
                                oNewPOrderD.Result5 = Math.Round(MValue, 2).ToString();
                            }

                            if (oNewPOrderD.Result6.Length > 0)
                            {
                                MValue = Convert.ToDecimal(oNewPOrderD.Result6);
                                oNewPOrderD.Result6 = Math.Round(MValue, 2).ToString();
                            }
                        }

                        if (Convert.ToDecimal(oDataProperties["Pk_CharID"]) == 3 || Convert.ToDecimal(oDataProperties["Pk_CharID"]) == 4)
                        {
                            var CharLen = oDataProperties["Result1"].ToString().Length;
                            if (CharLen == 3)
                            {
                                oNewPOrderD.Result1 = oNewPOrderD.Result1 + "0";
                            }
                            var CharLen1 = oDataProperties["Result2"].ToString().Length;
                            if (CharLen1 == 3)
                            {
                                oNewPOrderD.Result2 = oNewPOrderD.Result2 + "0";
                            }
                            var CharLen2= oDataProperties["Result3"].ToString().Length;
                            if (CharLen2 == 3)
                            {
                                oNewPOrderD.Result3 = oNewPOrderD.Result3 + "0";
                            }
                            var CharLen3 = oDataProperties["Result4"].ToString().Length;
                            if (CharLen3 == 3)
                            {
                                oNewPOrderD.Result4 = oNewPOrderD.Result4 + "0";
                            }
                            var CharLen4 = oDataProperties["Result5"].ToString().Length;
                            if (CharLen4 == 3)
                            {
                                oNewPOrderD.Result5 = oNewPOrderD.Result5 + "0";
                            }
                            var CharLen5 = oDataProperties["Result6"].ToString().Length;
                            if (CharLen5 == 3)
                            {
                                oNewPOrderD.Result6 = oNewPOrderD.Result6 + "0";
                            }
                        }

                        oNewPOrderD.Fk_Characteristics= Convert.ToDecimal(oDataProperties["Pk_CharID"]);
                        //oNewPOrderD.Amount = (oNewPOrderD.Quantity * oNewPOrderD.Rate);

                        oNewColor.PaperCertificateDetails.Add(oNewPOrderD);


                    }


                }
               // oNewColor.Fk_Status = 1;
                oNewColor.InvDate = Convert.ToDateTime(omValues["InvDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                if (omValues.ContainsKey("InvNo"))
                {
                    oNewColor.Invno = omValues["InvNo"].ToString();
                }
                else
                {
                    oNewColor.Invno = "1";
                }
                _oEntites.AddToPaperCertificate(oNewColor);
                _oEntites.SaveChanges();
                oResult.Success = true;


                //PaperCertificate oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificate), omValues) as PaperCertificate;

                //_oEntites.AddToPaperCertificate(oNewColor);

                //_oEntites.SaveChanges();

                //oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

           //PaperCertificate oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificate), omValues) as PaperCertificate;
                //PaperCertificate oColorFromShelf = _oEntites.PaperCertificate.Where(p => p.Pk_Id == oColor.Pk_Id).Single();
                //object orefItems = oColorFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PaperCertificate), omValues, ref orefItems);
                //_oEntites.SaveChanges();
                //oResult.Success = true;

              

            try
            {
                PaperCertificate oMaterialIndent = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificate), omValues) as PaperCertificate;
                PaperCertificate oMaterialIndentFromShelf = _oEntites.PaperCertificate.Where(p => p.Pk_Id == oMaterialIndent.Pk_Id).Single();
                object orefMaterialIndentFromShelf = oMaterialIndentFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PaperCertificate), omValues, ref orefMaterialIndentFromShelf);

                //List<decimal> oUpdateList = new List<decimal>();
                //if (omValues.ContainsKey("IndentDetails"))
                //{
                string sMaterialIndent = omValues["Materials"].ToString();
                object[] sMaterialIndents = JsonConvert.DeserializeObject<object[]>(sMaterialIndent);
                List<decimal> UpdatedPk = new List<decimal>();
                List<PaperCertificateDetails> NewIndentList = new List<PaperCertificateDetails>();

                foreach (object oIndent in sMaterialIndents)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                    Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                    if (oDataProperties.ContainsKey("Pk_IdDet") && oDataProperties["Pk_IdDet"] != null && oDataProperties["Pk_IdDet"].ToString() != "")
                    {
                        decimal Pk_IdDet = decimal.Parse(oDataProperties["Pk_IdDet"].ToString());



                        //Handel Update here
                        decimal dPkMIndentDetail = Convert.ToDecimal(oDataProperties["Pk_IdDet"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties["Fk_Characteristics"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        PaperCertificateDetails oIndentDetailsFromShelf = _oEntites.PaperCertificateDetails.Where(p => p.Pk_IdDet == dPkMIndentDetail && p.Fk_Characteristics == dFkMaterial).Single();

                        object oIndentDetails
                                            = _oEntites.PaperCertificateDetails.Where(p => p.Pk_IdDet == dPkMIndentDetail).First();


                        UpdatedPk.Add(dPkMIndentDetail);
                        ///                           /object orefSchedule = oIndentFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PaperCertificateDetails), oDataProperties, ref oIndentDetails);
                        // UpdatedPk.Add(Pk_MaterialOrderDetailsId);

                        //add a new item into the PendingTrack` here




                        decimal FkMaterial = 0;
                        //decimal IndQty = 0;

                        if (oDataProperties.ContainsKey("Fk_Characteristics").ToString() != "")
                        {
                            FkMaterial = decimal.Parse(oDataProperties["Fk_Characteristics"].ToString());
                           // IndQty = decimal.Parse(oDataProperties["Quantity"].ToString());
                        }


                        //help
                        ///error occuring here on edit save - for a new product added during edit mode--because there is no fk material in pending track table

                        //PendingTrack oPendingFromShelf = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oMaterialIndentFromShelf.Pk_MaterialOrderMasterId && p.Fk_Material == FkMaterial).Single();

                        //oPendingFromShelf.Quantity = IndQty;

                        //oPendingFromShelf.Pending = IndQty;

                    }
                    else
                    {



                        PaperCertificateDetails oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificateDetails), oDataProperties) as PaperCertificateDetails;
                        oIndentDetails.Fk_Characteristics = decimal.Parse(oDataProperties["Pk_CharID"].ToString());
                        NewIndentList.Add(oIndentDetails);


                        //PendingTrack oPendingNew = new PendingTrack();

                        //oPendingNew.Fk_Indent = Convert.ToDecimal(oIndentDetails.Fk_MaterialOrderMasterId);
                        //oPendingNew.Fk_Material = Convert.ToDecimal(oIndentDetails.Fk_Material);
                        //oPendingNew.Quantity = Convert.ToDecimal(oIndentDetails.Quantity);
                        //oPendingNew.AlreadyInwarded = 0;
                        //oPendingNew.Pending = Convert.ToDecimal(oIndentDetails.Quantity);  //////////////////CHECK 
                        //oPendingNew.QC_Qty = 0;



                    }
                }

                List<PaperCertificateDetails> oDeletedRecords = oMaterialIndentFromShelf.PaperCertificateDetails.Where(p => !UpdatedPk.Contains(p.Pk_IdDet)).ToList();

                foreach (PaperCertificateDetails oDeletedDetail in oDeletedRecords)
                {
                    oMaterialIndentFromShelf.PaperCertificateDetails.Remove(oDeletedDetail);
                }

                //Add new elements
                foreach (PaperCertificateDetails oNewIndentDetail in NewIndentList)
                {
                    oMaterialIndentFromShelf.PaperCertificateDetails.Add(oNewIndentDetail);
                }

                //}
             _oEntites.SaveChanges();

                oResult.Success = true;




            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PaperCertificate oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PaperCertificate), omValues) as PaperCertificate;
                PaperCertificate oColorFromShelf = _oEntites.PaperCertificate.Where(p => p.Pk_Id == oColor.Pk_Id).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as PaperCertificate;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_Id;
            }
            set
            {
                oColor = _oEntites.PaperCertificate.Where(p => p.Pk_Id == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


