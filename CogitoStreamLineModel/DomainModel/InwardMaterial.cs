﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class InwardMaterial : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private MaterialInwardM oInward = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sFromDate = null;
            string sToDate= null;

            string sVendor = null;
            string sInwardNo = null;
            string sMaterial = null;
            
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<MaterialInwardM> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_Inward").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            catch { }
            try { sMaterial = p_Params.Single(p => p.ParameterName == "Material").ParameterValue; }
            catch { }

            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }
            
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.MaterialInwardM;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }
                if (!string.IsNullOrEmpty(sVendor))
                {
                    oInwards = oInwards.Where(p => p.Inv_MaterialIndentMaster.gen_Vendor.VendorName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sMaterial))
                //{
                //    oInwards = oInwards.Where(p =>  p.Inv_Material.Name.IndexOf(sMaterial, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    DateTime SFrom = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                    oInwards = oInwards.Where(p => (p.Inward_Date >= Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {

                    DateTime STo = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                    oInwards = oInwards.Where(p => (p.Inward_Date <= Convert.ToDateTime(STo)));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
          //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
          oInwards=  oInwards.OrderByDescending(p=>p.Pk_Inward);
                //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInwards = oInwards.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchOthersStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName

            string sPk_Material = null;
            string sMatName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_OthersStock> oOrders = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMatName = p_Params.Single(p => p.ParameterName == "MatName").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntities.Vw_OthersStock;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oOrders = oOrders.Where(p => p.Pk_Material == Convert.ToDecimal(sPk_Material));
                }

                if (!string.IsNullOrEmpty(sMatName))
                {
                    oOrders = oOrders.Where(p => p.MatName.IndexOf(sMatName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchInwdDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
           
            string sInwardNo = null;

            string sPk_Mat = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_InwardRep> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_Inward").ParameterValue; }
            catch { }
            try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }
            

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.Vw_InwardRep;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }
             
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_Inward);
            //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oInwards = oInwards.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchInwardDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username

            string sInwardNo = null;

            string sPk_Mat = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_GRNMatData> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "InwardNo").ParameterValue; }
            catch { }
            try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.Vw_GRNMatData;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_Inward);
            //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oInwards = oInwards.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public SearchResult SearchInwdDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username

            string sInwardNo = null;

            string sPk_Mat = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_TotInwdQty> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_Inward").ParameterValue; }
            catch { }
            try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.Vw_TotInwdQty;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }
                if (!string.IsNullOrEmpty(sPk_Mat))
                {
                    oInwards = oInwards.Where(p => p.Pk_Material == decimal.Parse(sPk_Mat));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_Inward);
            //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oInwards = oInwards.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }


        public ModelManuplationResult CreateNew()
        {
            CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                decimal PkVal=0;
                MaterialInwardM oNewInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardM), omValues) as MaterialInwardM;
                //PurchaseOrderM oPurOrder=new PurchaseOrderM();

                var PoNo = omValues["PONo"].ToString();

                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                      
                        MaterialInwardD oNewInwardD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardD), oDataProperties) as MaterialInwardD;

                        if (oNewInwardD.Fk_Mill == 0)
                        {
                           oNewInwardD.Fk_Mill = 34;
                           //oNewInwardD.Fk_Mill = 3;
                        }
                        oNewInward.MaterialInwardD.Add(oNewInwardD);
                  
                       

                        /////////////////stock

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oNewInwardD.Fk_Material).Single();
                        
                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {

                            int StkCnt = _oEntities.Stocks.Where(p => p.Fk_Material == oNewInwardD.Fk_Material).Count();

                            if (StkCnt > 0)
                            {
                                Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oNewInwardD.Fk_Material).Single();
                                oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oNewInwardD.Quantity);
                            }
                            else
                            {
                                Stock oStockVal = new Stock();
                                oStockVal.SafeValue = 0;
                                oStockVal.Fk_Material = oNewInwardD.Fk_Material;
                                oStockVal.Quantity = Convert.ToDecimal(oNewInwardD.Quantity);
                                oStockVal.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                                oStockVal.Fk_BranchID = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                                _oEntities.AddToStocks(oStockVal);

                            }

                        }

                        else
                        {
                            int StkCnt1 = _oEntities.PaperStock.Where(p => p.Fk_Material == oNewInwardD.Fk_Material).Count();

                            //if (StkCnt1 > 0)
                            //{
                            //    PaperStock oStockVal = _oEntities.PaperStock.Where(p => p.Fk_Material == oNewInwardD.Fk_Material).Single();
                            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oNewInwardD.Quantity);
                            //    oStockVal.RollNo = oNewInwardD.RollNo;
                            //}
                            //else
                            //{
                                PaperStock oStockVal = new PaperStock();
                             
                                oStockVal.Fk_Material = oNewInwardD.Fk_Material;
                                oStockVal.Quantity = Convert.ToDecimal(oNewInwardD.Quantity);
                                oStockVal.RollNo = oNewInwardD.RollNo;
                                _oEntities.AddToPaperStock(oStockVal);

                            //}

                        }

                            }


                }
                if (PoNo == "")
                {
                    if (omValues.ContainsKey("Fk_QC"))
                    {
                        var PURDate = Convert.ToDateTime(DateTime.Now,
    System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                        PurchaseOrderM oPur = new PurchaseOrderM();

                        oPur.PODate = PURDate;
                        oPur.Fk_Vendor = oNewInward.Fk_QC;
                        oPur.Fk_Status = 1;
                         oPur.Fk_Indent = 539;
                       // oPur.Fk_Indent = 50;
                        oPur.TaxType = "0%";
                        oPur.ED = 0;
                        oPur.GrandTotal = 0;
                        oPur.NetValue = 0;
                        oPur.CGST = 0;
                        oPur.SGST = 0;

                        var POMax =
                (from PurchaseOrderM in dc.PurchaseOrderM

                 orderby PurchaseOrderM.Pk_PONo descending

                 select PurchaseOrderM.Pk_PONo).Take(1);


                        decimal PoVal = POMax.First();
                        decimal FixVal = 66;

                        var insVal = Convert.ToDecimal(PoVal - FixVal);
                        var YrNext = (DateTime.Now.AddYears(1).Year);
                        oPur.PkDisp = insVal + "/" + (DateTime.Now.Year).ToString() + "-" + YrNext;

                        _oEntities.AddToPurchaseOrderM(oPur);
                        _oEntities.SaveChanges();
                        PkVal = Convert.ToDecimal(oPur.Pk_PONo);

                    }
               
                if (omValues.ContainsKey("PONO").ToString().Length == 0)
                {
                  //  oNewInward.PONo = 454;
                }
                else
                {
                    oNewInward.PONo = PkVal;
                }
                if (omValues.ContainsKey("Fk_Indent").ToString().Length == 0)
                {
                    oNewInward.Fk_Indent = 539;
                }
                else
                {
                    oNewInward.Fk_Indent = 539;
                   // oNewInward.Fk_Indent = 50;
                } 
                 }
                

                    
                //INSERT INTO PURCHASEORDERM VALUES('09/17/2019',71,1,'0%',0,0,0,539,0,0,NULL,'408/2019-2020',NULL,NULL,NULL,NULL,NULL,NULL)
              
                oNewInward.Fk_InwardBy = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                
              //  oNewInward.PONo = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                //oNewInward.Fk_Indent = oNewInward.QualityCheck.PurchaseOrderM.Fk_Indent.GetValueOrDefault;
                //oNewInward.Fk_Indent = oNewInward.PurchaseOrderM.Fk_Indent;
            //    oNewInward.Inward_Date = DateTime.Now;


                oNewInward.Inward_Date = Convert.ToDateTime(omValues["Inward_Date"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                oNewInward.Pur_InvDate = Convert.ToDateTime(omValues["Pur_InvDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                _oEntities.AddToMaterialInwardM(oNewInward);
                
                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
            
            
       
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                MaterialInwardM oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardM), omValues) as MaterialInwardM;
                MaterialInwardM OPOrdFromShelf = _oEntities.MaterialInwardM.Where(p => p.Pk_Inward == oPOrd.Pk_Inward).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(MaterialInwardM), omValues, ref orefPO);


                string sMaterials = omValues["Materials"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<MaterialInwardD> NewMaterialsList = new List<MaterialInwardD>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_InwardDet"] == null || oDataProperties2["Pk_InwardDet"].ToString() == "")
                    {
                        MaterialInwardD oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardD), oDataProperties2) as MaterialInwardD;

                        NewMaterialsList.Add(oNewPOrderD);

                
                        
                         Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oNewPOrderD.Fk_Material).Single();

                         if (oInv_Master.Fk_MaterialCategory != 4)
                         {


                             Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oNewPOrderD.Fk_Material).Single();

                             oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oNewPOrderD.Quantity);
                             oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oNewPOrderD.Quantity);
                         }
                         else
                         {

                             //oStockVal.RollNo = oNewInwardD.RollNo;
                               int StkCnt2 = _oEntities.PaperStock.Where(p => p.Fk_Material ==  oNewPOrderD.Fk_Material && p.RollNo == oNewPOrderD.RollNo).Count();

                               if (StkCnt2 > 0)
                               {
                                   PaperStock oStockVal = _oEntities.PaperStock.Where(p => p.Fk_Material == oNewPOrderD.Fk_Material && p.RollNo == oNewPOrderD.RollNo).Single();

                                   oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oNewPOrderD.Quantity);
                                   oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oNewPOrderD.Quantity);
                               }
                               else
                               {
                                   //_oEntities.DeleteObject(oPOrderDet);
                               }
                         }
                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_InwardDet"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                   //     gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == dFkMaterial).Single();

                        MaterialInwardD oPOrderDetFromShelf = _oEntities.MaterialInwardD.Where(p => p.Pk_InwardDet == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                        object oPOrderDet
                                            = _oEntities.MaterialInwardD.Where(p => p.Pk_InwardDet == dPkPOrdDet).First();

                        _oEntities.DeleteObject(oPOrderDet);


                        MaterialInwardD oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardD), oDataProperties2) as MaterialInwardD;


                        //QualityChild oQC = _oEntities.QualityChild.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.FkQualityCheck == oPOrd.Fk_QC).Single();
                        //oQC.PendingQty = Convert.ToDecimal(oQC.PendingQty - oIndentDetails.Quantity);
                    

                        NewMaterialsList.Add(oIndentDetails);
                        UpdatedPk.Add(dPkPOrdDet);


                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {


                            Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oIndentDetails.Fk_Material).Single();

                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oPOrderDetFromShelf.Quantity);
                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oIndentDetails.Quantity);
                        }
                        else
                        {

                            int StkCnt2 = _oEntities.PaperStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oIndentDetails.RollNo).Count();

                            if (StkCnt2 > 0)
                            {

                                PaperStock oStockVal = _oEntities.PaperStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oIndentDetails.RollNo).Single();

                                oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oPOrderDetFromShelf.Quantity);
                                oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oIndentDetails.Quantity);

                            }
                            else
                            {
                                object oPOrderDet1
                                        = _oEntities.PaperStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oPOrderDetFromShelf.RollNo).First();
                            
                                _oEntities.DeleteObject(oPOrderDet1);

                                PaperStock oStockVal1 = new PaperStock();

                                oStockVal1.Fk_Material = dFkMaterial;
                                oStockVal1.Quantity = Convert.ToDecimal(oDataProperties2["Quantity"]);
                                oStockVal1.RollNo =oDataProperties2["RollNo"].ToString();
                                _oEntities.AddToPaperStock(oStockVal1);
                            }
                        }
                    }

                    
                }
                // Handeling Deleted Records

                List<MaterialInwardD> oDeletedRecords = OPOrdFromShelf.MaterialInwardD.Where(p => !UpdatedPk.Contains(p.Pk_InwardDet)).ToList();



                //Add new elements
                foreach (MaterialInwardD oNewMaterialDetail in NewMaterialsList)
                {
                    OPOrdFromShelf.MaterialInwardD.Add(oNewMaterialDetail);
                }

                //object orefPurchaseM = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PurchaseOrderM), omValues, ref orefPurchaseM);

                //OPOrdFromShelf.GrandTotal = gtot;

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            //    MaterialInwardM oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardM), omValues) as MaterialInwardM;
            //    MaterialInwardM oInwardFromShelf = _oEntities.MaterialInwardM.Where(p => p.Pk_Inward == oInward.Pk_Inward).Single();
            //    object orefItems = oInwardFromShelf;

            //    var TanentID=Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            //    var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            //    Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == branchid).Single();

            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInwardFromShelf.Quantity);
            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oInward.Quantity);

            //    PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded)-Convert.ToDecimal(oInwardFromShelf.Quantity);
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded) + Convert.ToDecimal(omValues["Quantity"]);
            //    MaterialInwardM oIwd = _oEntities.MaterialInwardM.Where(p => p.Fk_Material == oInwardFromShelf.Fk_Material && p.Pk_Inward == oInward.Pk_Inward).Single();
            //    //QualityCheck oQualitycheckPending = _oEntities.QualityChecks.Where(p => p.Pk_QualityCheck == oInward.Fk_QualityCheck && p.Fk_Material == oInward.Fk_Material).Single();
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending + oIwd.Quantity);
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending - oInward.Quantity);
                
            //    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(MaterialInwardM), omValues, ref orefItems);

            //    //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();


           
                 
                   
                  

            //       decimal InwdFkMaterial = 0;
            //       InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);

                

            //       //oPendingNew.Quantity = Convert.ToDecimal(oQCheck.Quantity);29.12
            //       //oPendingNew.AlreadyInwarded = Convert.ToDecimal(oInward.Quantity);29.12


            // //      oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oInward.Quantity);



            //    _oEntities.SaveChanges();
            //    oResult.Success = true;
            //}


            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
        //        MaterialInwardM oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MaterialInwardM), omValues) as MaterialInwardM;
        //        MaterialInwardM oInwardFromShelf = _oEntities.MaterialInwardMs.Where(p => p.Pk_Inward == oInward.Pk_Inward).Single();
        //        object orefItems = oInwardFromShelf;
        //        //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();



        //        var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
        //        var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);

        //        Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material ).Single();

        //        oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInward.Quantity);



        //        decimal InwdFkMaterial = 0;
        //        InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);

        //        PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == InwdFkMaterial).Single();

               
        //        oPendingNew.AlreadyInwarded = oPendingNew.Quantity - Convert.ToDecimal(oInward.Quantity);
        //       // oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oPendingNew.AlreadyInwarded);



        //        _oEntities.DeleteObject(oInwardFromShelf);

        //         _oEntities.SaveChanges();

        //        oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oInward;

            }
            set
            {
                oInward = value as MaterialInwardM;
            }
        }

        public decimal ID
        {
            get
            {
                return oInward.Pk_Inward;
            }
            set
            {
                oInward = _oEntities.MaterialInwardM.Where(p => p.Pk_Inward == value).Single();
            }
        }

        public object GetRaw()
        {
            return oInward;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Dictionary<string, string> InwardStock()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            IEnumerable<MaterialInwardM> oMaterialInwardMs = _oEntities.MaterialInwardM;
            oMaterialInwardMs.OrderByDescending(ss => ss.Pk_Inward);
            int s = 0;
            foreach (MaterialInwardM oMatInw in oMaterialInwardMs)
            {
                oResult.Add(s.ToString(), oMatInw.Pk_Inward + " - " + DateTime.Parse(oMatInw.Inward_Date.ToString()).ToString("dd/MM/yyyy") );
                s++;
            }
            return oResult;
        }
    }
}



