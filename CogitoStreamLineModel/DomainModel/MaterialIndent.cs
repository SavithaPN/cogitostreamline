﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class MaterialIndent : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Inv_MaterialIndentMaster oInv_MaterialIndentMaster = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_MaterialIndentId = null;
            //string sMaterialIndentDate = null;
            string sOnlyPending = null;
            string sBranch = null;
            string sVendor= null;
            string sFromIndentDate = null;
            string sToIndentDate = null;
            string sStatus = null;
            string sOrderNumber = null;
            string sMaterial = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_MaterialIndentMaster> oInv_MaterialIndentMasters = null;

            try { sPk_MaterialIndentId = p_Params.Single(p => p.ParameterName == "Pk_MaterialIndent").ParameterValue; }
            catch { }
            try { sFromIndentDate = p_Params.Single(p => p.ParameterName == "FromIndentDate").ParameterValue; }
            catch { }
            try { sToIndentDate = p_Params.Single(p => p.ParameterName == "ToIndentDate").ParameterValue; }
            catch { }
            try { sBranch = p_Params.Single(p => p.ParameterName == "Branch").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            catch { }
            try { sStatus = p_Params.Single(p => p.ParameterName == "Status").ParameterValue; }
            catch { }
            try { sOrderNumber = p_Params.Single(p => p.ParameterName == "OrderNumber").ParameterValue; }
            catch { }
            try { sMaterial = p_Params.Single(p => p.ParameterName == "Material").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oInv_MaterialIndentMasters = _oEntities.Inv_MaterialIndentMaster;
          //  DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialIndentId))
                {
                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sPk_MaterialIndentId));
                }
                if (!string.IsNullOrEmpty(sFromIndentDate))
                {

                    DateTime SFrom = Convert.ToDateTime(sFromIndentDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => (p.MaterialIndentDate >= Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sToIndentDate))
                {

                    DateTime STo = Convert.ToDateTime(sToIndentDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => (p.MaterialIndentDate <= Convert.ToDateTime(STo)));
                }
                if (!string.IsNullOrEmpty(sBranch))
                {
                 //   oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.(WebGareCore.wgTenant.).IndexOf(sBranch, StringComparison.OrdinalIgnoreCase) >= 0);

                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => (p.Fk_Tanent!=null && p.wgTenant.TanentName.IndexOf(sBranch,StringComparison.OrdinalIgnoreCase)>=0));
                }
                if (!string.IsNullOrEmpty(sVendor))
                {
                //    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Fk_VendorId == decimal.Parse(sVendor));
                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p =>(p.Fk_VendorId !=null && p.gen_Vendor.VendorName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0));
                }
                if (!string.IsNullOrEmpty(sStatus))
                {
                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Fk_Status == decimal.Parse(sStatus));
                }
                if (!string.IsNullOrEmpty(sOrderNumber))
                {
                    List<decimal> oMaterialDetails = _oEntities.Inv_MaterialIndentDetails.Where(x => x.Fk_CustomerOrder == decimal.Parse(sOrderNumber))
                                                                                         .Select(x => x.Fk_MaterialOrderMasterId).OfType<decimal>().ToList();

                   oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => oMaterialDetails.Contains(p.Pk_MaterialOrderMasterId));
                }
                if (!string.IsNullOrEmpty(sMaterial))
                {
                    List<decimal> oMaterialDetails = _oEntities.Inv_MaterialIndentDetails.Where(x => x.Fk_Material == decimal.Parse(sMaterial))
                                                                                         .Select(x => x.Fk_MaterialOrderMasterId).OfType<decimal>().ToList();

                    oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => oMaterialDetails.Contains(p.Pk_MaterialOrderMasterId));
                }
                if (!string.IsNullOrEmpty(sOnlyPending))
                {
                    if (bool.Parse(sOnlyPending))
                    {
                        oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Where(p => p.Fk_Status != 3 && p.Fk_Status != 10 && p.Fk_Status == 11);
                    }
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oInv_MaterialIndentMasters.Count();
                oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.OrderByDescending(p => p.Pk_MaterialOrderMasterId);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInv_MaterialIndentMasters = oInv_MaterialIndentMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInv_MaterialIndentMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchIndentDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_MIndent> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIndent").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntities.Vw_MIndent;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sPk_CharID));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oColors = oColors.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_MaterialOrderMasterId);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oColors = oColors.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        public SearchResult SearchIndentCertDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_IndentCertificate> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIndent").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntities.Vw_IndentCertificate;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sPk_CharID));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oColors = oColors.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_MaterialOrderMasterId);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oColors = oColors.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchIndentPO(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_IndentPOSearch> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIndent").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntities.Vw_IndentPOSearch;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sPk_CharID));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oColors = oColors.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_MaterialOrderMasterId);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oColors = oColors.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchPO(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            //string CheckDate = "", string Vendor = "", string InvoiceNo = "", string MaterialInward = "", 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_PONo = null;
            //string sMaterialIndentDate = null;
            string sVendorName = null;
            string sMaterialName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PO> oQualityCheck = null;

            try { sPk_PONo = p_Params.Single(p => p.ParameterName == "Pk_PONo").ParameterValue; }
            catch { }
            try { sMaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }

            try { sVendorName = p_Params.Single(p => p.ParameterName == "VendorName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oQualityCheck = _oEntities.Vw_PO;

            try
            {

                if (!string.IsNullOrEmpty(sPk_PONo))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.Pk_PONo == decimal.Parse(sPk_PONo));
                }

                if (!string.IsNullOrEmpty(sVendorName))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.VendorName != null && p.VendorName.IndexOf(sVendorName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sMaterialName))
                {
                    oQualityCheck = oQualityCheck.Where(p => p.Name.IndexOf(sMaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oQualityCheck.Count();
            oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_PONo);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oQualityCheck = oQualityCheck.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oQualityCheck.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                Inv_MaterialIndentMaster oNewMaterialIndent = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentMaster), omValues) as Inv_MaterialIndentMaster;

                
                                  oNewMaterialIndent.MaterialIndentDate = Convert.ToDateTime(omValues["MaterialIndentDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                if (omValues.ContainsKey("IndentDetails"))
                {
                    string sIndentDetails = omValues["IndentDetails"].ToString();
                    object[] IndentDetails = JsonConvert.DeserializeObject<object[]>(sIndentDetails);

                    foreach (object oDtetails in IndentDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        Inv_MaterialIndentDetails oIndentDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentDetails), oDataProperties) as Inv_MaterialIndentDetails;
                        

                        oIndentDetail.RequiredDate = Convert.ToDateTime(oDataProperties["RequiredDate"].ToString(),
    System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                  var MatFk= Convert.ToInt32(oIndentDetail.Fk_Material);
                         Inv_Material MatDet = _oEntities.Inv_Material.Where(p => p.Pk_Material == MatFk).Single();

                         oIndentDetail.GSM = MatDet.GSM;

                        oIndentDetail.BF = MatDet.BF;
                        oIndentDetail.Deckle = MatDet.Deckle;


 oNewMaterialIndent.Inv_MaterialIndentDetails.Add(oIndentDetail);
                        //PendingTrack oPendingNew = new PendingTrack();

                        //oNewMaterialIndent.Inv_MaterialIndentDetails.Add(oIndentDetail);


                        //oPendingNew.Fk_Material = Convert.ToDecimal(oIndentDetail.Fk_Material);
                        //oPendingNew.Quantity = Convert.ToDecimal(oIndentDetail.Quantity);
                        //oPendingNew.AlreadyInwarded = 0;
                        //oPendingNew.Pending = Convert.ToDecimal(oIndentDetail.Quantity);  //////////////////CHECK 
                        //oPendingNew.QC_Qty = 0;
                        //oNewMaterialIndent.PendingTracks.Add(oPendingNew);


                    }
                }
                oNewMaterialIndent.Fk_Status = _oEntities.wfStates.Where(p => p.State == "New").Single().Pk_State;
                oNewMaterialIndent.Fk_UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

                oNewMaterialIndent.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                oNewMaterialIndent.Fk_Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                oNewMaterialIndent.Fk_VendorId = 1;
                //oNewOrder.OrderDate = DateTime.Today.ToString();
                _oEntities.AddToInv_MaterialIndentMaster(oNewMaterialIndent);
                _oEntities.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewMaterialIndent.Pk_MaterialOrderMasterId.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                Inv_MaterialIndentMaster oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentMaster), omValues) as Inv_MaterialIndentMaster;
                Inv_MaterialIndentMaster OPOrdFromShelf = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_MaterialOrderMasterId == oPOrd.Pk_MaterialOrderMasterId).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_MaterialIndentMaster), omValues, ref orefPO);



                string sMaterials = omValues["IndentDetails"].ToString(); 
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<Inv_MaterialIndentDetails> NewMaterialsList = new List<Inv_MaterialIndentDetails>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_MaterialOrderDetailsId"] == null || oDataProperties2["Pk_MaterialOrderDetailsId"].ToString() == "")
                    {
                        Inv_MaterialIndentDetails oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentDetails), oDataProperties2) as Inv_MaterialIndentDetails;

                        NewMaterialsList.Add(oNewPOrderD);



                       
                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_MaterialOrderDetailsId"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        //     gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == dFkMaterial).Single();

                        
                        int StkCnt1 = _oEntities.Inv_MaterialIndentDetails.Where(p => p.Pk_MaterialOrderDetailsId == dPkPOrdDet && p.Fk_Material == dFkMaterial).Count();
                        if (StkCnt1 > 0)
                        {
                            Inv_MaterialIndentDetails oPOrderDetFromShelf = _oEntities.Inv_MaterialIndentDetails.Where(p => p.Pk_MaterialOrderDetailsId == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();
                        }

                        object oPOrderDet
                                            = _oEntities.Inv_MaterialIndentDetails.Where(p => p.Pk_MaterialOrderDetailsId == dPkPOrdDet).First();

                        _oEntities.DeleteObject(oPOrderDet);


                        Inv_MaterialIndentDetails oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentDetails), oDataProperties2) as Inv_MaterialIndentDetails;


                        //QualityChild oQC = _oEntities.QualityChild.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.FkQualityCheck == oPOrd.Fk_QC).Single();
                        //oQC.PendingQty = Convert.ToDecimal(oQC.PendingQty - oIndentDetails.Quantity);


                        NewMaterialsList.Add(oIndentDetails);
                        UpdatedPk.Add(dPkPOrdDet);


                      
                    }


                }
                // Handeling Deleted Records

                List<Inv_MaterialIndentDetails> oDeletedRecords = OPOrdFromShelf.Inv_MaterialIndentDetails.Where(p => !UpdatedPk.Contains(p.Pk_MaterialOrderDetailsId)).ToList();



                //Add new elements
                foreach (Inv_MaterialIndentDetails oNewMaterialDetail in NewMaterialsList)
                {
                    OPOrdFromShelf.Inv_MaterialIndentDetails.Add(oNewMaterialDetail);
                }

                //object orefPurchaseM = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PurchaseOrderM), omValues, ref orefPurchaseM);

                //OPOrdFromShelf.GrandTotal = gtot;

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            //    Inv_MaterialIndentMaster oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentMaster), omValues) as Inv_MaterialIndentMaster;
            //    Inv_MaterialIndentMaster oInwardFromShelf = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_Inward == oInward.Pk_Inward).Single();
            //    object orefItems = oInwardFromShelf;

            //    var TanentID=Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            //    var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            //    Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == branchid).Single();

            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInwardFromShelf.Quantity);
            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oInward.Quantity);

            //    PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded)-Convert.ToDecimal(oInwardFromShelf.Quantity);
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded) + Convert.ToDecimal(omValues["Quantity"]);
            //    Inv_MaterialIndentMaster oIwd = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Fk_Material == oInwardFromShelf.Fk_Material && p.Pk_Inward == oInward.Pk_Inward).Single();
            //    //QualityCheck oQualitycheckPending = _oEntities.QualityChecks.Where(p => p.Pk_QualityCheck == oInward.Fk_QualityCheck && p.Fk_Material == oInward.Fk_Material).Single();
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending + oIwd.Quantity);
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending - oInward.Quantity);

            //    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_MaterialIndentMaster), omValues, ref orefItems);

            //    //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();







            //       decimal InwdFkMaterial = 0;
            //       InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);



            //       //oPendingNew.Quantity = Convert.ToDecimal(oQCheck.Quantity);29.12
            //       //oPendingNew.AlreadyInwarded = Convert.ToDecimal(oInward.Quantity);29.12


            // //      oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oInward.Quantity);



            //    _oEntities.SaveChanges();
            //    oResult.Success = true;
            //}


            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        //public ModelManuplationResult Update()
        //{
        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        Inv_MaterialIndentMaster oMaterialIndent = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentMaster), omValues) as Inv_MaterialIndentMaster;
        //        Inv_MaterialIndentMaster oMaterialIndentFromShelf = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_MaterialOrderMasterId == oMaterialIndent.Pk_MaterialOrderMasterId).Single();
        //        object orefMaterialIndentFromShelf = oMaterialIndentFromShelf;
        //        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_MaterialIndentMaster), omValues, ref orefMaterialIndentFromShelf);

        //        //List<decimal> oUpdateList = new List<decimal>();
        //        //if (omValues.ContainsKey("IndentDetails"))
        //        //{
        //        string sMaterialIndent = omValues["IndentDetails"].ToString();
        //        object[] sMaterialIndents = JsonConvert.DeserializeObject<object[]>(sMaterialIndent);
        //        List<decimal> UpdatedPk = new List<decimal>();
        //        List<Inv_MaterialIndentDetails> NewIndentList = new List<Inv_MaterialIndentDetails>();

        //        foreach (object oIndent in sMaterialIndents)
        //        {
        //            Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
        //            Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

        //            if (oDataProperties.ContainsKey("Pk_MaterialOrderDetailsId") && oDataProperties["Pk_MaterialOrderDetailsId"] != null && oDataProperties["Pk_MaterialOrderDetailsId"].ToString() != "")
        //            {
        //                decimal Pk_MaterialOrderDetailsId = decimal.Parse(oDataProperties["Pk_MaterialOrderDetailsId"].ToString());



        //                //Handel Update here
        //                decimal dPkMIndentDetail = Convert.ToDecimal(oDataProperties["Pk_MaterialOrderDetailsId"]);
        //                decimal dFkMaterial = Convert.ToDecimal(oDataProperties["Fk_Material"]);
        //                //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

        //                int StkCnt2 = _oEntities.Inv_MaterialIndentDetails.Where(p => p.Pk_MaterialOrderDetailsId == dPkMIndentDetail && p.Fk_Material == dFkMaterial).Count();

        //                if (StkCnt2 > 0)
        //                {
        //                    Inv_MaterialIndentDetails oIndentDetailsFromShelf = _oEntities.Inv_MaterialIndentDetails.Where(p => p.Pk_MaterialOrderDetailsId == dPkMIndentDetail && p.Fk_Material == dFkMaterial).Single();

        //                    object oIndentDetails
        //                                        = _oEntities.Inv_MaterialIndentDetails.Where(p => p.Pk_MaterialOrderDetailsId == dPkMIndentDetail).First();


        //                    UpdatedPk.Add(dPkMIndentDetail);
        //                    ///                           /object orefSchedule = oIndentFromShelf;
        //                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_MaterialIndentDetails), oDataProperties, ref oIndentDetails);
        //                    // UpdatedPk.Add(Pk_MaterialOrderDetailsId);

        //                    //add a new item into the PendingTrack` here




        //                    decimal FkMaterial = 0;
        //                    decimal IndQty = 0;

        //                    if (oDataProperties.ContainsKey("Fk_Material").ToString() != "")
        //                    {
        //                        FkMaterial = decimal.Parse(oDataProperties["Fk_Material"].ToString());
        //                        IndQty = decimal.Parse(oDataProperties["Quantity"].ToString());
        //                    }

        //                }
        //                //help
        //                ///error occuring here on edit save - for a new product added during edit mode--because there is no fk material in pending track table

        //                //PendingTrack oPendingFromShelf = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oMaterialIndentFromShelf.Pk_MaterialOrderMasterId && p.Fk_Material == FkMaterial).Single();

        //                //oPendingFromShelf.Quantity = IndQty;

        //                //oPendingFromShelf.Pending = IndQty;

        //            }
        //            else
        //            {



        //                Inv_MaterialIndentDetails oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentDetails), oDataProperties) as Inv_MaterialIndentDetails;
        //                NewIndentList.Add(oIndentDetails);


        //                PendingTrack oPendingNew = new PendingTrack();

        //                oPendingNew.Fk_Indent = Convert.ToDecimal(oIndentDetails.Fk_MaterialOrderMasterId);
        //                oPendingNew.Fk_Material = Convert.ToDecimal(oIndentDetails.Fk_Material);
        //                oPendingNew.Quantity = Convert.ToDecimal(oIndentDetails.Quantity);
        //                oPendingNew.AlreadyInwarded = 0;
        //                oPendingNew.Pending = Convert.ToDecimal(oIndentDetails.Quantity);  //////////////////CHECK 
        //                oPendingNew.QC_Qty = 0;



        //            }
        //        }

        //        List<Inv_MaterialIndentDetails> oDeletedRecords = oMaterialIndentFromShelf.Inv_MaterialIndentDetails.Where(p => !UpdatedPk.Contains(p.Pk_MaterialOrderDetailsId)).ToList();

        //        foreach (Inv_MaterialIndentDetails oDeletedDetail in oDeletedRecords)
        //        {
        //            oMaterialIndentFromShelf.Inv_MaterialIndentDetails.Remove(oDeletedDetail);
        //        }

        //        //Add new elements
        //        foreach (Inv_MaterialIndentDetails oNewIndentDetail in NewIndentList)
        //        {
        //            oMaterialIndentFromShelf.Inv_MaterialIndentDetails.Add(oNewIndentDetail);
        //        }

        //        //}
        //        _oEntities.SaveChanges();

        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_MaterialIndentMaster oMaterialIndent = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_MaterialIndentMaster), omValues) as Inv_MaterialIndentMaster;
                Inv_MaterialIndentMaster oMaterialIndentFromShelf = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_MaterialOrderMasterId == oMaterialIndent.Pk_MaterialOrderMasterId).Single();
                oMaterialIndentFromShelf.Inv_MaterialIndentDetails.Clear();
                _oEntities.DeleteObject(oMaterialIndentFromShelf);

                /////////////////////////////////////


                List<decimal> oUpdateList = new List<decimal>();
                if (omValues.ContainsKey("IndentDetails"))
                {
                    string sMaterialIndent = omValues["IndentDetails"].ToString();
                    object[] sMaterialIndents = JsonConvert.DeserializeObject<object[]>(sMaterialIndent);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<Inv_MaterialIndentDetails> NewIndentList = new List<Inv_MaterialIndentDetails>();

                    foreach (object oIndent in sMaterialIndents)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        decimal Pk_MaterialOrderDetailsId = decimal.Parse(oDataProperties["Pk_MaterialOrderDetailsId"].ToString());


                        PendingTrack oPendingNew = new PendingTrack();
                        decimal FkMaterial = 0;
                        decimal IndQty = 0;

                        if (oDataProperties.ContainsKey("Fk_Material").ToString() != "")
                        {
                            FkMaterial = decimal.Parse(oDataProperties["Fk_Material"].ToString());
                            IndQty = decimal.Parse(oDataProperties["Quantity"].ToString());
                        }

                        PendingTrack oPendingFromShelf = _oEntities.PendingTracks.Where(p => p.Fk_Indent == Pk_MaterialOrderDetailsId && p.Fk_Material == FkMaterial).Single();
                        _oEntities.DeleteObject(oPendingFromShelf);

                    }


                    //////////////////////////////////////////
                    _oEntities.SaveChanges();
                    oResult.Success = true;
                }
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public EntityObject DAO
        {
            get
            {
                return oInv_MaterialIndentMaster;
            }
            set
            {
                oInv_MaterialIndentMaster = value as Inv_MaterialIndentMaster;
            }
        }

        

        public decimal ID
        {
            get
            {
                return oInv_MaterialIndentMaster.Pk_MaterialOrderMasterId;
            }
            //set
            //{
            //    oInv_MaterialIndentMaster.Pk_MaterialOrderMasterId = value;
            //}
            set
            {
                oInv_MaterialIndentMaster = _oEntities.Inv_MaterialIndentMaster.Where(p => p.Pk_MaterialOrderMasterId == value).Single();
            }
        }

        public object GetRaw()
        {
            return oInv_MaterialIndentMaster;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    

        public Dictionary<string, int> GetIndentRejectedCount()
        {
            Dictionary<string, int> oResult = new Dictionary<string, int>();
            int Count = 0;
            IEnumerable<wfState> oStates = _oEntities.wfStates;

            IEnumerable<Inv_MaterialIndentMaster> oMaterial = _oEntities.Inv_MaterialIndentMaster.Where(p => p.wfState.State == "Rejected");
            IEnumerable<gen_Vendor> ovendor=_oEntities.gen_Vendor;
            int s = 0;
            foreach (gen_Vendor oven in ovendor)
            {
                Count = oMaterial.Where(ss => ss.Fk_VendorId == oven.Pk_Vendor).Count();
                if (Count > 0)
                {
                    oResult.Add(s.ToString(), Count);
                    s++;
                }
            }
            return oResult;
        }

        public Dictionary<string, string> IndentApproval()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            IEnumerable<Inv_MaterialIndentMaster> oMaterialIndent = _oEntities.Inv_MaterialIndentMaster.Where(p =>p.wfState.State=="New" && p.Fk_Tanent == tannent);
            int s = 0;
            foreach (Inv_MaterialIndentMaster oMaten in oMaterialIndent)
            {
                //oResult.Add(s.ToString(), oMaten.Pk_MaterialOrderMasterId + " - " + DateTime.Parse(oMaten.MaterialIndentDate.ToString()).ToString("dd/MM/yyyy") + " - " + oMaten.gen_Vendor.VendorName);
                oResult.Add(s.ToString(),oMaten.Pk_MaterialOrderMasterId.ToString());
                s++;
            }
            return oResult;
        }
    }
}
