﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
    public class Mill : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_Mill ogen_Mill = null;

        public SearchResult SearchMill(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_Mill> oMill = null;
            oMill = _oEntities.gen_Mill;

            oSearchResult.RecordCount = oMill.Count();
            oMill.OrderBy(p => p.MillName);

            List<EntityObject> oFilteredMill = oMill.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredMill;

            return oSearchResult;
        }

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Mill = null;
            string sMillName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Mill> oMills = null;

            try { sPk_Mill = p_Params.Single(p => p.ParameterName == "Pk_Mill").ParameterValue; }
            catch { }
            try { sMillName = p_Params.Single(p => p.ParameterName == "MillName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMills = _oEntities.gen_Mill;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Mill))
                {
                    oMills = oMills.Where(p => p.Pk_Mill == decimal.Parse(sPk_Mill));
                }
                if (!string.IsNullOrEmpty(sMillName))
                {
                    oMills = oMills.Where(p => p.MillName.IndexOf(sMillName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oMills.Count();
          oMills= oMills.OrderBy(p => p.MillName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMills = oMills.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredMill = oMills.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredMill;

            return oSearchResult;

        }



        public SearchResult SearchNew(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sContactPersonName = null;
            string sFk_Mill = null;

            
            IEnumerable<VW_MillDuplicate> oVMills = null;

            
            try { sFk_Mill = p_Params.Single(p => p.ParameterName == "Pk_Mill").ParameterValue; }
            catch { }
            try { sContactPersonName = p_Params.Single(p => p.ParameterName == "ContactPersonName").ParameterValue; }
            catch { }



            oVMills = _oEntities.VW_MillDuplicate;

            try
            {

                if (!string.IsNullOrEmpty(sFk_Mill))
                {
                    oVMills = oVMills.Where(p => p.Fk_Mill == decimal.Parse(sFk_Mill));
                }
                if (!string.IsNullOrEmpty(sContactPersonName))
                {
                    oVMills = oVMills.Where(p => p.ContactPersonName.IndexOf(sContactPersonName, StringComparison.OrdinalIgnoreCase) >= 0);
                                   }
                }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oVMills.Count();
            oVMills.OrderBy(p => p.Fk_Mill);


            List<EntityObject> oFilteredMill = oVMills.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredMill;

            return oSearchResult;

        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Mill oNewMill = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Mill), omValues) as gen_Mill;

                if (omValues.ContainsKey("Contacts"))
                {
                    string sContacts = omValues["Contacts"].ToString();
                    object[] Contacts = JsonConvert.DeserializeObject<object[]>(sContacts);

                    foreach (object oItem in Contacts)
                    {
                        gen_MillContacts oContact = new gen_MillContacts();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oContact.Fk_ContactPerson = decimal.Parse(oProperties["id"]);
                        oNewMill.gen_MillContacts.Add(oContact);
                    }

                }

                oNewMill.FkTenantID=Convert.ToInt32(HttpContext.Current.Session["Tanent"]);

                _oEntities.AddTogen_Mill(oNewMill);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Mill oMills = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Mill), omValues) as gen_Mill;
                gen_Mill oMillsFromShelf = _oEntities.gen_Mill.Where(p => p.Pk_Mill == oMills.Pk_Mill).Single();

                oMillsFromShelf.gen_MillContacts.Clear();

                if (omValues.ContainsKey("Contacts"))
                {
                    string sContacts = omValues["Contacts"].ToString();
                    object[] Contacts = JsonConvert.DeserializeObject<object[]>(sContacts);

                    foreach (object oItem in Contacts)
                    {
                        gen_MillContacts oContact = new gen_MillContacts();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oContact.Fk_ContactPerson = decimal.Parse(oProperties["id"]);
                        oMillsFromShelf.gen_MillContacts.Add(oContact);
                    }


                }

                object orefMill = oMillsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Mill), omValues, ref orefMill);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Mill oMills = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Mill), omValues) as gen_Mill;
                gen_Mill oMillsFromShelf = _oEntities.gen_Mill.Where(p => p.Pk_Mill == oMills.Pk_Mill).Single();

                List<gen_ContactPerson> ovContacts = oMillsFromShelf.gen_MillContacts.Select(p => p.gen_ContactPerson).ToList();
                oMillsFromShelf.gen_MillContacts.Clear();

                _oEntities.DeleteObject(oMillsFromShelf);
                _oEntities.SaveChanges();

                foreach (gen_ContactPerson oContcat in ovContacts)
                {
                    _oEntities.DeleteObject(oContcat);
                }
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return ogen_Mill;
            }
            set
            {
                ogen_Mill = value as gen_Mill;
            }
        }

        public decimal ID
        {
            get
            {
                return ogen_Mill.Pk_Mill;
            }
            set
            {
                ogen_Mill = _oEntities.gen_Mill.Where(p => p.Pk_Mill == value).Single();
            }
        }

        public object GetRaw()
        {
            return ogen_Mill;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
