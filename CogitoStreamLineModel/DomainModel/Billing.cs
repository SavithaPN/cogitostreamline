﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;
using System.Data;

namespace CogitoStreamLineModel.DomainModel
{
    public class Billing : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Inv_Billing oInv_Billing = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_Invoice = null;
            string sInvno = null;
            string sFromDate = null;
            string sToDate = null;
            string sCustomer = null;
            string sOrderno = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Inv_Billing> oInv_Billings = null;

            try { sPk_Invoice = p_Params.Single(p => p.ParameterName == "Pk_Invoice").ParameterValue; }
            catch { }
            try { sInvno = p_Params.Single(p => p.ParameterName == "Invno").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "Fk_Customer").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "TxtFromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "TxtToDate").ParameterValue; }
            catch { }
            try { sOrderno = p_Params.Single(p => p.ParameterName == "OrderNo").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oInv_Billings = _oEntities.Inv_Billing;
            // DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Invoice))
                {
                    oInv_Billings = oInv_Billings.Where(p => p.Pk_Invoice == decimal.Parse(sPk_Invoice));
                }

                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oInv_Billings = oInv_Billings.Where(p => p.Fk_Customer != null && p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    DateTime FrmDate = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                    oInv_Billings = oInv_Billings.Where(p => (p.InvDate >= Convert.ToDateTime(FrmDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    DateTime ToDate = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                    oInv_Billings = oInv_Billings.Where(p => (p.InvDate <= Convert.ToDateTime(ToDate)));
                }
                //if (!string.IsNullOrEmpty(sOrderno))
                //{
                //    oInv_Billings = oInv_Billings.Where(p => p.Fk_OrderNo == decimal.Parse(sOrderno));
                //}
                if (!string.IsNullOrEmpty(sInvno))
                {
                    oInv_Billings = oInv_Billings.Where(p => p.Invno.IndexOf(sInvno, StringComparison.OrdinalIgnoreCase) >= 0);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oInv_Billings.Count();
            oInv_Billings = oInv_Billings.OrderByDescending(p => p.Pk_Invoice);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInv_Billings = oInv_Billings.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInv_Billings.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

//        public ModelManuplationResult CreateNew()
//        {
//            decimal GTotal = 0;

//            double t1 = 0.12;
//            decimal EDVAL = 0;

//            decimal NValue = 0;
//            ModelManuplationResult oResult = new ModelManuplationResult();
//            try
//            {
//                Inv_Billing oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Billing), omValues) as Inv_Billing;
//                var OrdNo=Convert.ToDecimal(omValues["Fk_OrderNo"]);
//                 //var PartID=omValues["PartId"];
//                if (omValues.ContainsKey("InvoiceDetails"))
//                {
//                    string sInvoiceDetails = omValues["InvoiceDetails"].ToString();
//                    object[] InvoiceDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetails);


//                    foreach (object oDtetails in InvoiceDetails)
//                    {
//                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
//                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
//                        Inv_BillingDetails oInvDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_BillingDetails), oDataProperties) as Inv_BillingDetails;


//                        //oInvDetail.Fk_Material = null;
//                        oInvDetail.UnitVal = Convert.ToString(oDataProperties["Unit"]);
//                          var StkIDVal= Convert.ToDecimal(oDataProperties["StkID"]);

//                        var TType=omValues["TaxType"];

//                Tax oTax = _oEntities.Tax.Where(p => p.TaxName == TType).Single();

//                oInvDetail.Fk_TaxID = oTax.PkTax;
//                        oNewInv.Inv_BillingDetails.Add(oInvDetail);
//                        GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oInvDetail.NetAmount);
//                        EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);

                        
//                          /////////////////stock

//                        BoxStock oBoxStock = _oEntities.BoxStock.Where(p => p.Fk_BoxID == oInvDetail.Fk_BoxID && p.Pk_StockID == StkIDVal).Single();

//                        oBoxStock.Quantity = Convert.ToDecimal(oBoxStock.Quantity - oInvDetail.Quantity);
//                        _oEntities.SaveChanges();
//                        ///////////////////////////

//                        /////////////////////////Close the Order
//                        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
//                        var PartID=oInvDetail.Fk_PartID;

//                             int RCount = _oEntities.Gen_OrderChild.Where(p => p.Fk_OrderID == OrdNo && p.Fk_PartID==PartID).Count();

//                             if (RCount > 0)
//                             {


//                                 Gen_OrderChild oOrder = _oEntities.Gen_OrderChild.Where(p => p.Fk_OrderID == OrdNo && p.Fk_PartID == PartID).Single();
//                                 var OrdQty = oOrder.OrdQty;



//                                 int i = 0;
//                                 List<Vw_InvOrdQty> oInvList = new List<Vw_InvOrdQty>();
//                                 oInvList = dc.Vw_InvOrdQty.Where(p => p.Fk_OrderNo == OrdNo && p.Fk_PartID == PartID).Select(p => p).OfType<Vw_InvOrdQty>().ToList();
//                                 var ListCount = oInvList.Count();
//                                 if (ListCount > 0)
//                                 {
//                                     var DelSum = 0;
//                                     for (int j = 0; j < ListCount; j++)
//                                     {
//                                         DelSum = DelSum + Convert.ToInt16(oInvList.ElementAt(j).Quantity);


//                                     }
//                                     if (DelSum >= OrdQty)
//                                     {
//                                         oOrder.Fk_Status = 4;
//                                         _oEntities.SaveChanges();
//                                     }
//                                     //if(oInvDetail
//                                 }
//                             }
//                        /////////////////////////
//                    }
//                    NValue = (Convert.ToDecimal(oNewInv.NETVALUE));


//                }
//                         oNewInv.InvDate  = Convert.ToDateTime(omValues["InvDate"].ToString(),
//System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


//                oNewInv.Cancel = false;
//                oNewInv.GrandTotal =Math.Round(GTotal,2);
//                oNewInv.NETVALUE = NValue;
//                oNewInv.ED = EDVAL;


//                _oEntities.AddToInv_Billing(oNewInv);
//                _oEntities.SaveChanges();

//                oResult.Success = true;
//                oResult.Message = oNewInv.Pk_Invoice.ToString();
//            }
//            catch (Exception e)
//            {
//                oResult.Success = false;
//                oResult.Message = e.Message;
//                oResult.Exception = e;
//            }

//            return oResult;
//        }
        public ModelManuplationResult CreateNew()
        {
            decimal GTotal = 0;
            ////////////////////////taxvalue field in child table contains order number
            double t1 = 0.12;
            decimal EDVAL = 0;

            decimal NValue = 0;
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                Inv_Billing oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Billing), omValues) as Inv_Billing;
                var OrdNo = Convert.ToDecimal(omValues["Fk_OrderNo"]);
                //var PartID=omValues["PartId"];
                if (omValues.ContainsKey("InvoiceDetails"))
                {
                    string sInvoiceDetails = omValues["InvoiceDetails"].ToString();
                    object[] InvoiceDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetails);


                    foreach (object oDtetails in InvoiceDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        Inv_BillingDetails oInvDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_BillingDetails), oDataProperties) as Inv_BillingDetails;


                        if (oInvDetail.Fk_Material == null)
                        {
                            oInvDetail.UnitVal = Convert.ToString(oDataProperties["Unit"]);
                            var StkIDVal = Convert.ToDecimal(oDataProperties["StkID"]);

                            var TType = omValues["TaxType"];

                            Tax oTax = _oEntities.Tax.Where(p => p.TaxName == TType).Single();

                            oInvDetail.Fk_TaxID = oTax.PkTax;
                            oNewInv.Inv_BillingDetails.Add(oInvDetail);
                            GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oInvDetail.NetAmount);
                            EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);


                            /////////////////stock

                            BoxStock oBoxStock = _oEntities.BoxStock.Where(p => p.Fk_BoxID == oInvDetail.Fk_BoxID && p.Pk_StockID == StkIDVal).Single();

                            oBoxStock.Quantity = Convert.ToDecimal(oBoxStock.Quantity - oInvDetail.Quantity);
                            _oEntities.SaveChanges();
                            ///////////////////////////

                            /////////////////////////Close the Order
                            CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                            var PartID = oInvDetail.Fk_PartID;

                            int RCount = _oEntities.Gen_OrderChild.Where(p => p.Fk_OrderID == OrdNo && p.Fk_PartID == PartID).Count();

                            if (RCount > 0)
                            {


                                Gen_OrderChild oOrder = _oEntities.Gen_OrderChild.Where(p => p.Fk_OrderID == OrdNo && p.Fk_PartID == PartID).Single();
                                var OrdQty = oOrder.OrdQty;



                                int i = 0;
                                List<Vw_InvOrdQty> oInvList = new List<Vw_InvOrdQty>();
                                oInvList = dc.Vw_InvOrdQty.Where(p => p.Fk_OrderNo == OrdNo && p.Fk_PartID == PartID).Select(p => p).OfType<Vw_InvOrdQty>().ToList();
                                var ListCount = oInvList.Count();
                                if (ListCount > 0)
                                {
                                    var DelSum = 0;
                                    for (int j = 0; j < ListCount; j++)
                                    {
                                        DelSum = DelSum + Convert.ToInt16(oInvList.ElementAt(j).Quantity);


                                    }
                                    if (DelSum >= OrdQty)
                                    {
                                        oOrder.Fk_Status = 4;
                                        _oEntities.SaveChanges();
                                    }
                                    //if(oInvDetail
                                }
                            }
                            /////////////////////////
                        }

                        else
                        {

                            oInvDetail.UnitVal = Convert.ToString(oDataProperties["Unit"]);
                            //  var StkIDVal = Convert.ToDecimal(oDataProperties["StkID"]);
                            oInvDetail.Fk_BoxID = null;
                            var TType = omValues["TaxType"];

                            Tax oTax = _oEntities.Tax.Where(p => p.TaxName == TType).Single();

                            oInvDetail.Fk_TaxID = oTax.PkTax;
                            oNewInv.Inv_BillingDetails.Add(oInvDetail);
                            GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oInvDetail.NetAmount);
                            EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);


                            /////////////////stock

                            int PCount1 = _oEntities.Stocks.Where(p => p.Fk_Material == oInvDetail.Fk_Material).Count();

                            if (PCount1 == 1)
                            {
                                Stock oBoxStock = _oEntities.Stocks.Where(p => p.Fk_Material == oInvDetail.Fk_Material).SingleOrDefault();
                           oBoxStock.Quantity = Convert.ToDecimal(oBoxStock.Quantity - oInvDetail.Quantity);
                                _oEntities.SaveChanges(); 
                            
                            }
                            else if (PCount1 > 1)
                            {
                                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                                List<Stock> oPrdList = new List<Stock>();
                                oPrdList = dc.Stocks.Where(p => p.Fk_Material == oInvDetail.Fk_Material).Select(s => s).OfType<Stock>().ToList();

                                int j = 0;
                                var stkID = 0;
                                var stkID1 = 0;
                                var TQty = 0;
                                for (j = 0; j < PCount1; j++)
                                {
                                    if (j == 0)
                                    {
                                        stkID = Convert.ToInt32(oPrdList.ElementAt(j).Pk_Stock);
                                    }
                                    else
                                    {

                                        stkID1 = Convert.ToInt32(oPrdList.ElementAt(j).Pk_Stock);
                                    }
                                    TQty = TQty + Convert.ToInt32(oPrdList.ElementAt(j).Quantity);

                                    if (stkID1 != 0)
                                    {
                                        Stock oBoxStock1 = _oEntities.Stocks.Where(p => p.Pk_Stock == stkID1).FirstOrDefault();

                                        _oEntities.DeleteObject(oBoxStock1);
                                    }
                                }

                                Stock oBoxStock = _oEntities.Stocks.Where(p => p.Pk_Stock == stkID).FirstOrDefault();


                                oBoxStock.Quantity = TQty - oInvDetail.Quantity;
                                _oEntities.SaveChanges();


                            }
                            }
                        //}
                        NValue = (Convert.ToDecimal(oNewInv.NETVALUE));

                    }
                }


                //oNewInv.InvDate = DateTime.Now;
                oNewInv.InvDate = Convert.ToDateTime(omValues["InvDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                oNewInv.Cancel = false;
                oNewInv.GrandTotal = GTotal;

                var TType1 = omValues["TaxType"];
                Tax oTax1 = _oEntities.Tax.Where(p => p.TaxName == TType1).SingleOrDefault();
                var TaxVal = oTax1.TaxValue;

                var NetV = (GTotal) + (Convert.ToDecimal((TaxVal / 100) * 2) * (GTotal));

                oNewInv.NETVALUE = NetV;
                oNewInv.ED = NetV;


                _oEntities.AddToInv_Billing(oNewInv);
                _oEntities.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewInv.Pk_Invoice.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        //public ModelManuplationResult Update()
        //{
        //    decimal GTotal = 0;
        //    double t2 = 0.055;
        //    double t1 = 0.06;
        //    double t3 = 0.02;
        //    decimal nTotal = 0;
        //    decimal nTotal1 = 0;
        //    decimal NValue = 0;
        //    string ttax = "";
        //    decimal EDVAL = 0;
        //    bool FForm;
        //    bool CanValue ;

        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        Inv_Billing oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Billing), omValues) as Inv_Billing;
        //        Inv_Billing oNewInvFromShelf = _oEntities.Inv_Billing.Where(p => p.Pk_Invoice == oNewInv.Pk_Invoice).Single();

        //        ttax = oNewInv.TaxType.TrimEnd();

        //        if (oNewInv.FORM_H != null && oNewInv.FORM_H == true)
        //        {
        //            FForm = true;
        //        }
        //        else
        //        { FForm = false; }

        //       //if (oNewInv.Cancel ==1)
        //       // {
        //       //     CanValue =true;
        //       // }
        //       //   else
        //       //{
        //       //    CanValue = false;
        //       //}

        //        object orefInvFromShelf = oNewInvFromShelf;
        //        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_Billing), omValues, ref orefInvFromShelf);
                


        //        List<decimal> oUpdateList = new List<decimal>();
        //        if (omValues.ContainsKey("InvoiceDetails"))
        //        {
        //            string sInvoiceDetail = omValues["InvoiceDetails"].ToString();
        //            object[] sInvoiceDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetail);
        //            List<decimal> UpdatedPk = new List<decimal>();
        //            List<Inv_BillingDetails> oInvDetList = new List<Inv_BillingDetails>();

        //            foreach (object oIndent in sInvoiceDetails)
        //            {
        //                Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
        //                Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

        //                if (oDataProperties.ContainsKey("Pk_Inv_Details") && oDataProperties["Pk_Inv_Details"] != null && oDataProperties["Pk_Inv_Details"].ToString() != "")
        //                {
        //                    decimal Pk_Inv_Details = decimal.Parse(oDataProperties["Pk_Inv_Details"].ToString());
        //                    //Handel Update here
        //                    //decimal dPkMIndentDetail = Convert.ToDecimal(oDataProperties["Pk_MaterialOrderDetailsId"]);
        //                    decimal dFkBox = Convert.ToDecimal(oDataProperties["Fk_BoxID"]);
        //                    //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

        //                    Inv_BillingDetails oIndentDetailsFromShelf = _oEntities.Inv_BillingDetails.Where(p => p.Pk_Inv_Details == Pk_Inv_Details && p.Fk_BoxID == dFkBox).Single();

        //                    object oIndentDetails
        //                                        = _oEntities.Inv_BillingDetails.Where(p => p.Pk_Inv_Details == Pk_Inv_Details).First();



        //                    UpdatedPk.Add(Pk_Inv_Details);
        //                    ///                           /object orefSchedule = oIndentFromShelf;
        //                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_BillingDetails), oDataProperties, ref oIndentDetails);
        //                //    Inv_BillingDetails oInvDet = _oEntities.Inv_BillingDetails.Where(p => p.Pk_Inv_Details == Pk_Inv_Details).Single();

        //                //    object orefSchedule = oInvDet;
        //                //    GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oDataProperties["NetAmount"]);

        //                //    EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);

        //                //    if (ttax == "VAT")
        //                //    {


        //                //        if (FForm == true)
        //                //        {
        //                //            NValue = (Convert.ToDecimal(GTotal));
        //                //        }
        //                //        else
        //                //        {
        //                //            nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                //            nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t2);
        //                //            NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                //        }



        //                //    }

        //                //    if (ttax == "CST")
        //                //    {
        //                //        nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                //        nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t3);
        //                //        NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                //    }

        //                    //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_BillingDetails), oDataProperties, ref orefSchedule);
        //                    //UpdatedPk.Add(Pk_Inv_Details);

        //                //}

        //                //else
        //                //{
        //                //    Inv_BillingDetails oInvDets = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_BillingDetails), oDataProperties) as Inv_BillingDetails;

        //                //    GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oInvDets.NetAmount);
        //                //    oInvDetList.Add(oInvDets);


        //                //    GTotal = Convert.ToDecimal(GTotal) + Convert.ToDecimal(oDataProperties["NetAmount"]);

        //                //    EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);

        //                //    if (ttax == "VAT")
        //                //    {


        //                //        if (FForm == true)
        //                //        {
        //                //            NValue = (Convert.ToDecimal(GTotal));
        //                //        }
        //                //        else
        //                //        {
        //                //            nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                //            nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t2);
        //                //            NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                //        }



        //                //    }

        //                    //if (ttax == "CST")
        //                    //{
        //                    //    nTotal = (Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1));
        //                    //    nTotal1 = (Convert.ToDecimal(nTotal) + Convert.ToDecimal(GTotal)) * Convert.ToDecimal(t3);
        //                    //    NValue = (Convert.ToDecimal(GTotal) + Convert.ToDecimal(nTotal) + Convert.ToDecimal(nTotal1));

        //                    //}

        //                }

        //            }

        //            List<Inv_BillingDetails> oDeletedRecords = oNewInvFromShelf.Inv_BillingDetails.Where(p => !UpdatedPk.Contains(p.Pk_Inv_Details)).ToList();

        //            foreach (Inv_BillingDetails oDeletedDetail in oDeletedRecords)
        //            {
        //                oNewInvFromShelf.Inv_BillingDetails.Remove(oDeletedDetail);
        //            }

        //            //Add new elements
        //            foreach (Inv_BillingDetails oNewIndentDetail in oInvDetList)
        //            {
        //                oNewInvFromShelf.Inv_BillingDetails.Add(oNewIndentDetail);
        //            }

        //        }
        //        oNewInv.GrandTotal = GTotal;
        //        oNewInvFromShelf.GrandTotal = GTotal;

        //        oNewInv.NETVALUE = NValue;
        //        oNewInvFromShelf.NETVALUE = NValue;
        //        //oNewInv.Cancel = CanValue;
        //        oNewInv.ED = EDVAL;
        //        oNewInvFromShelf.ED = EDVAL;

        //        //oNewInv.DNTimeofRemoval = DateTime.Now;
        //        //oNewInvFromShelf.DNTimeofRemoval = DateTime.Now; 
        //        //oNewInv.DNTimeofInv = DateTime.Now;
        //        //oNewInvFromShelf.DNTimeofInv = DateTime.Now;


        //        _oEntities.SaveChanges();

        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Billing oMaterialIndent = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Billing), omValues) as Inv_Billing;
                Inv_Billing oMaterialIndentFromShelf = _oEntities.Inv_Billing.Where(p => p.Pk_Invoice == oMaterialIndent.Pk_Invoice).Single();
                object orefMaterialIndentFromShelf = oMaterialIndentFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_Billing), omValues, ref orefMaterialIndentFromShelf);
                _oEntities.SaveChanges();
                //List<decimal> oUpdateList = new List<decimal>();
                //if (omValues.ContainsKey("InvoiceDetails"))
                //{
                string sMaterialIndent = omValues["InvoiceDetails"].ToString();
                object[] sMaterialIndents = JsonConvert.DeserializeObject<object[]>(sMaterialIndent);
                List<decimal> UpdatedPk = new List<decimal>();
                List<Inv_BillingDetails> NewIndentList = new List<Inv_BillingDetails>();

                foreach (object oIndent in sMaterialIndents)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                    Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                    if (oDataProperties.ContainsKey("Pk_Inv_Details") && oDataProperties["Pk_Inv_Details"] != null && oDataProperties["Pk_Inv_Details"].ToString() != "")
                    {
                        decimal Pk_Inv_Details = decimal.Parse(oDataProperties["Pk_Inv_Details"].ToString());



                        //Handel Update here
                        decimal dPkMIndentDetail = Convert.ToDecimal(oDataProperties["Pk_Inv_Details"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties["Fk_BoxID"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        Inv_BillingDetails oIndentDetailsFromShelf = _oEntities.Inv_BillingDetails.Where(p => p.Pk_Inv_Details == dPkMIndentDetail && p.Fk_BoxID == dFkMaterial).Single();

                        object oIndentDetails
                                            = _oEntities.Inv_BillingDetails.Where(p => p.Pk_Inv_Details == dPkMIndentDetail).First();


                        UpdatedPk.Add(dPkMIndentDetail);
                        ///                           /object orefSchedule = oIndentFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Inv_BillingDetails), oDataProperties, ref oIndentDetails);
                        // UpdatedPk.Add(Pk_Inv_Details);

                        //add a new item into the PendingTrack` here




                        decimal FkMaterial = 0;
                        decimal IndQty = 0;

                        if (oDataProperties.ContainsKey("Fk_BoxID").ToString() != "")
                        {
                            FkMaterial = decimal.Parse(oDataProperties["Fk_BoxID"].ToString());
                            IndQty = decimal.Parse(oDataProperties["Quantity"].ToString());
                        }

                        var TAmt = Convert.ToDouble(oDataProperties["TotalAmount"].ToString());
                        var TaxVal = 0.06;
                         var AftTaxAmt =Convert.ToDouble(TAmt * TaxVal);

                         oMaterialIndentFromShelf.GrandTotal = decimal.Parse(oDataProperties["TotalAmount"].ToString());
                         oMaterialIndentFromShelf.NETVALUE = Convert.ToDecimal(TAmt) + Convert.ToDecimal(AftTaxAmt);
                     


                        _oEntities.SaveChanges();
                        //help
                        ///error occuring here on edit save - for a new product added during edit mode--because there is no fk material in pending track table

                        //PendingTrack oPendingFromShelf = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oMaterialIndentFromShelf.Pk_Invoice && p.Fk_BoxID == FkMaterial).Single();

                        //oPendingFromShelf.Quantity = IndQty;

                        //oPendingFromShelf.Pending = IndQty;

                    }
                    //else
                    //{



                    //    Inv_BillingDetails oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_BillingDetails), oDataProperties) as Inv_BillingDetails;
                    //    NewIndentList.Add(oIndentDetails);


                    //    PendingTrack oPendingNew = new PendingTrack();

                    //    oPendingNew.Fk_Indent = Convert.ToDecimal(oIndentDetails.Fk_MaterialOrderMasterId);
                    //    oPendingNew.Fk_BoxID = Convert.ToDecimal(oIndentDetails.Fk_BoxID);
                    //    oPendingNew.Quantity = Convert.ToDecimal(oIndentDetails.Quantity);
                    //    oPendingNew.AlreadyInwarded = 0;
                    //    oPendingNew.Pending = Convert.ToDecimal(oIndentDetails.Quantity);  //////////////////CHECK 
                    //    oPendingNew.QC_Qty = 0;



                    //}
                }

                List<Inv_BillingDetails> oDeletedRecords = oMaterialIndentFromShelf.Inv_BillingDetails.Where(p => !UpdatedPk.Contains(p.Pk_Inv_Details)).ToList();

                foreach (Inv_BillingDetails oDeletedDetail in oDeletedRecords)
                {
                    oMaterialIndentFromShelf.Inv_BillingDetails.Remove(oDeletedDetail);
                }

                //Add new elements
                foreach (Inv_BillingDetails oNewIndentDetail in NewIndentList)
                {
                    oMaterialIndentFromShelf.Inv_BillingDetails.Add(oNewIndentDetail);
                }

                //}
                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        
        
        
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Inv_Billing oNewInv = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Inv_Billing), omValues) as Inv_Billing;
                Inv_Billing oNewInvFromShelf = _oEntities.Inv_Billing.Where(p => p.Pk_Invoice == oNewInv.Pk_Invoice).Single();
                oNewInvFromShelf.Inv_BillingDetails.Clear();
                _oEntities.DeleteObject(oNewInvFromShelf);

                /////////////////////////////////////


                List<decimal> oUpdateList = new List<decimal>();
                if (omValues.ContainsKey("InvoiceDetails"))
                {
                    string sInvoiceDetail = omValues["InvoiceDetails"].ToString();
                    object[] sInvoiceDetails = JsonConvert.DeserializeObject<object[]>(sInvoiceDetail);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<Inv_BillingDetails> oInvDetList = new List<Inv_BillingDetails>();

                    foreach (object oIndent in sInvoiceDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIndent.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        decimal Pk_Inv_Details = decimal.Parse(oDataProperties["Pk_Inv_Details"].ToString());

                    }

                    //////////////////////////////////////////
                    _oEntities.SaveChanges();
                    oResult.Success = true;
                }
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public EntityObject DAO
        {
            get
            {
                return oInv_Billing;
            }
            set
            {
                oInv_Billing = value as Inv_Billing;
            }
        }


        public decimal ID
        {
            get
            {
                return oInv_Billing.Pk_Invoice;
            }

            set
            {
                oInv_Billing = _oEntities.Inv_Billing.Where(p => p.Pk_Invoice == value).Single();
            }
        }

        public object GetRaw()
        {
            return oInv_Billing;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Dictionary<string, string> MonthlySalesStatus()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);

            decimal oCurrrentmonth = Convert.ToDecimal(_oEntities.vw_MonthlySalesStatus.Where(p => p.Months == ((DateTime.Now.Month) - 1) && p.Years == DateTime.Now.Year).Sum(s => s.GrandTotal));
            decimal opreviousmonth = Convert.ToDecimal(_oEntities.vw_MonthlySalesStatus.Where(p => p.Months == ((DateTime.Now.Month) - 2) && p.Years == DateTime.Now.Year).Sum(s => s.GrandTotal));

            if (opreviousmonth > oCurrrentmonth)
            {
                oResult.Add("0", "Improve the Sales");
            }
            return oResult;
        }

    }
}
