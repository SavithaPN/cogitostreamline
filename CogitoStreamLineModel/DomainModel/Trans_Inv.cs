﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Trans_Inv : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Trans_Invoice oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Color = null;
            string sColorName = null;
            string sInvno = null;
            string sTransporter = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Trans_Invoice> oColors = null;

            try { sPk_Color = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            try { sColorName = p_Params.Single(p => p.ParameterName == "VehicleNo").ParameterValue; }
            catch { }

            try { sTransporter = p_Params.Single(p => p.ParameterName == "Fk_Transporter").ParameterValue; }
            catch { }
            try { sInvno = p_Params.Single(p => p.ParameterName == "Invno").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.Trans_Invoice;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Color))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_Color));
                }
                if (!string.IsNullOrEmpty(sTransporter))
                {
                    oColors = oColors.Where(p => p.Fk_Transporter == decimal.Parse(sTransporter));
                }
                if (!string.IsNullOrEmpty(sColorName))
                {
                    oColors = oColors.Where(p => p.VehicleNo.IndexOf(sColorName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sInvno))
                {
                    oColors = oColors.Where(p => p.Invno.IndexOf(sInvno, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchColorName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Trans_Invoice> oColorName = null;
            oColorName = _oEntites.Trans_Invoice;

            oSearchResult.RecordCount = oColorName.Count();
            oColorName.OrderBy(p => p.Pk_ID);

            List<EntityObject> oFilteredItem = oColorName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Trans_Invoice oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Trans_Invoice), omValues) as Trans_Invoice;
                oNewColor.BalAmt = Convert.ToDecimal(omValues["Amount"]);
                _oEntites.AddToTrans_Invoice(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Trans_Invoice oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Trans_Invoice), omValues) as Trans_Invoice;
                Trans_Invoice oColorFromShelf = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Trans_Invoice), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Trans_Invoice oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Trans_Invoice), omValues) as Trans_Invoice;
                Trans_Invoice oColorFromShelf = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as Trans_Invoice;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_ID;
            }
            set
            {
                oColor = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


