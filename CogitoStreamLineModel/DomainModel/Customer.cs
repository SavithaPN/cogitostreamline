﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore.CommonObjects;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;
using System.Web.SessionState;

namespace CogitoStreamLineModel.DomainModel
{
    public class Customer : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_Customer ogen_Customer = null;
        private bool disposed = false;
        #endregion Variables

        #region Properties
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return ogen_Customer;
            }
            set
            {
                ogen_Customer = value as gen_Customer;
            }
        }
        public decimal ID
        {
            get
            {
                return ogen_Customer.Pk_Customer;
            }
            set
            {
                ogen_Customer = _oEntities.gen_Customer.Where(p => p.Pk_Customer == value).Single();
            }
        }
        #endregion Properties

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Customer = null;
            string sCustomerName = null;
            string sCustomerType = null;
            string sCustomerContact = null;
            string sLandLine = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Customer> oCustomers = null;

            try { sPk_Customer = p_Params.Single(p => p.ParameterName == "Pk_Customer").ParameterValue; }
            catch { }
            try { sCustomerName = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sCustomerType = p_Params.Single(p => p.ParameterName == "CustomerType").ParameterValue; }
            catch { }
            try { sCustomerContact = p_Params.Single(p => p.ParameterName == "CustomerContact").ParameterValue; }
            catch { }
            try { sLandLine = p_Params.Single(p => p.ParameterName == "LandLine").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oCustomers = _oEntities.gen_Customer;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Customer))
                {
                    oCustomers = oCustomers.Where(p => p.Pk_Customer == decimal.Parse(sPk_Customer));
                }
                if (!string.IsNullOrEmpty(sCustomerName))
                {
                    oCustomers = oCustomers.Where(p => p.CustomerName.IndexOf(sCustomerName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sCustomerType))
                {
                    oCustomers = oCustomers.Where(p => p.CustomerType.IndexOf(sCustomerType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCustomerContact))
                {
                    //oCustomers = oCustomers.Where(p => p.CustomerContact == Convert.ToDecimal(sCustomerContact));
                    string ph = sCustomerContact.ToString();
                    oCustomers = oCustomers.Where(p => p.CustomerContact.ToString().IndexOf(ph, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sLandLine))
                {
                    oCustomers = oCustomers.Where(p => p.LandLine == Convert.ToDecimal(sLandLine));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oCustomers.Count();
            // oCustomers.OrderBy(p => p.Pk_Customer);

            oCustomers = oCustomers.OrderBy(p => p.CustomerName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oCustomers = oCustomers.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredCustomers = oCustomers.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredCustomers;

            return oSearchResult;
        }
        ///////////////////////////////////
        public SearchResult SearchPK(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Customer = null;
            string sCustomerName = null;
            string sCustomerCity = null;
            string sCustomerState = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_CustShip> oCustomers = null;

            try { sPk_Customer = p_Params.Single(p => p.ParameterName == "CustCode").ParameterValue; }
            catch { }
            try { sCustomerName = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sCustomerCity = p_Params.Single(p => p.ParameterName == "SCity").ParameterValue; }
            catch { }
            try { sCustomerState = p_Params.Single(p => p.ParameterName == "SState").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oCustomers = _oEntities.Vw_CustShip;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Customer))
                {
                    oCustomers = oCustomers.Where(p => p.Fk_CustomerId == decimal.Parse(sPk_Customer));
                }
                if (!string.IsNullOrEmpty(sCustomerName))
                {
                    oCustomers = oCustomers.Where(p => p.ShippingCustomerName.IndexOf(sCustomerName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCustomerCity))
                {
                    oCustomers = oCustomers.Where(p => p.ShippingCity.IndexOf(sCustomerCity, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(sCustomerState))
                {
                    oCustomers = oCustomers.Where(p => p.ShippingState.IndexOf(sCustomerState, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oCustomers.Count();
            // oCustomers.OrderBy(p => p.Pk_Customer);

            oCustomers = oCustomers.OrderByDescending(p => p.Pk_Customer);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oCustomers = oCustomers.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredCustomers = oCustomers.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredCustomers;

            return oSearchResult;
        }

        public SearchResult SearchContacts(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Customer = null;
            string sContactPersonName = null;
            IEnumerable<vw_CustomerDetails> oCustomers = null;

            try { sPk_Customer = p_Params.Single(p => p.ParameterName == "Pk_Customer").ParameterValue; }
            catch { }
            try { sContactPersonName = p_Params.Single(p => p.ParameterName == "ContactPersonName").ParameterValue; }
            catch { }

            oCustomers = _oEntities.vw_CustomerDetails;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Customer))
                {
                    oCustomers = oCustomers.Where(p => p.Pk_Customer == decimal.Parse(sPk_Customer));
                }
                if (!string.IsNullOrEmpty(sContactPersonName))
                {
                    oCustomers = oCustomers.Where(p => p.ContactPersonName == sContactPersonName);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oCustomers.Count();
            oCustomers.OrderBy(p => p.Pk_Customer);

            List<EntityObject> oFilteredCustomers = oCustomers.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredCustomers;

            return oSearchResult;
        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                gen_Customer oNewCustomers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Customer), omValues) as gen_Customer;

                if (omValues.ContainsKey("Contacts"))
                {
                    string sContacts = omValues["Contacts"].ToString();
                    object[] Contacts = JsonConvert.DeserializeObject<object[]>(sContacts);

                    foreach (object oItem in Contacts)
                    {
                        gen_CustomerContacts oContact = new gen_CustomerContacts();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oContact.Fk_ContactPerson = decimal.Parse(oProperties["id"]);
                        oNewCustomers.gen_CustomerContacts.Add(oContact);
                    }
                }
                if (omValues.ContainsKey("ShippingDetails"))
                {
                    string sShippingDetails = omValues["ShippingDetails"].ToString();
                    object[] ShippingDetails = JsonConvert.DeserializeObject<object[]>(sShippingDetails);

                    foreach (object oShipping in ShippingDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oShipping.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        gen_CustomerShippingDetails oShippingDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_CustomerShippingDetails), oDataProperties) as gen_CustomerShippingDetails;
                        //oShippingDetails.shi = decimal.Parse(omValues["fk_Enquiry"].ToString());
                        //oShippingDetails.DeliveredDate = null;
                        oNewCustomers.gen_CustomerShippingDetails.Add(oShippingDetails);


                    }
                }

                oNewCustomers.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
 omValues["CreatedDateOn"] = DateTime.Now;
//                oNewCustomers.CreatedDateOn = Convert.ToDateTime(omValues["CreatedDateOn"].ToString(),
//System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



               // oNewCustomers.CreatedDateOn = Convert.ToDateTime(DateTime.Now.ToString());
//              

//                oNewCustomers.CreatedDateOn = Convert.ToDateTime(omValues["CreatedDateOn"].ToString(),
//System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                _oEntities.AddTogen_Customer(oNewCustomers);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                gen_Customer oCustomers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Customer), omValues) as gen_Customer;
                gen_Customer oCustomersFromShelf = _oEntities.gen_Customer.Where(p => p.Pk_Customer == oCustomers.Pk_Customer).Single();

                oCustomersFromShelf.gen_CustomerContacts.Clear();

                if (omValues.ContainsKey("Contacts"))
                {
                    string sContacts = omValues["Contacts"].ToString();
                    object[] Contacts = JsonConvert.DeserializeObject<object[]>(sContacts);

                    foreach (object oItem in Contacts)
                    {
                        gen_CustomerContacts oContact = new gen_CustomerContacts();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oContact.Fk_ContactPerson = decimal.Parse(oProperties["id"]);
                        oCustomersFromShelf.gen_CustomerContacts.Add(oContact);
                    }
                }
                List<decimal> oUpdateList = new List<decimal>();

                if (omValues.ContainsKey("ShippingDetails"))
                {
                    string sShippingDetailss = omValues["ShippingDetails"].ToString();
                    object[] ShippingDetailss = JsonConvert.DeserializeObject<object[]>(sShippingDetailss);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<gen_CustomerShippingDetails> NewDeliveryList = new List<gen_CustomerShippingDetails>();
                    foreach (object oSchedule in ShippingDetailss)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oSchedule.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_CustomerShippingId") && oDataProperties["Pk_CustomerShippingId"] != null && oDataProperties["Pk_CustomerShippingId"].ToString() != "")
                        {
                            decimal pkShippingDetails = decimal.Parse(oDataProperties["Pk_CustomerShippingId"].ToString());

                            gen_CustomerShippingDetails oScheduleFromShelf = _oEntities.gen_CustomerShippingDetails.Where(p => p.Pk_CustomerShippingId == pkShippingDetails).Single();

                            object orefSchedule = oScheduleFromShelf;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_CustomerShippingDetails), oDataProperties, ref orefSchedule);
                            UpdatedPk.Add(pkShippingDetails);
                        }
                        else
                        {
                            gen_CustomerShippingDetails oShippingDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_CustomerShippingDetails), oDataProperties) as gen_CustomerShippingDetails;
                            NewDeliveryList.Add(oShippingDetails);
                        }
                    }

                    List<gen_CustomerShippingDetails> oDeletedRecords = oCustomersFromShelf.gen_CustomerShippingDetails.Where(p => !UpdatedPk.Contains(p.Pk_CustomerShippingId)).ToList();

                    foreach (gen_CustomerShippingDetails oDeletedDetail in oDeletedRecords)
                    {
                        oCustomersFromShelf.gen_CustomerShippingDetails.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (gen_CustomerShippingDetails oNewDeliveryDetail in NewDeliveryList)
                    {
                        oCustomersFromShelf.gen_CustomerShippingDetails.Add(oNewDeliveryDetail);
                    }
                }
                oCustomersFromShelf.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                object orefCustomers = oCustomersFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Customer), omValues, ref orefCustomers);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Customer oCustomers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Customer), omValues) as gen_Customer;
                gen_Customer oCustomersFromShelf = _oEntities.gen_Customer.Where(p => p.Pk_Customer == oCustomers.Pk_Customer).Single();

                List<gen_ContactPerson> ovContacts = oCustomersFromShelf.gen_CustomerContacts.Select(p => p.gen_ContactPerson).ToList();

                oCustomersFromShelf.gen_CustomerContacts.Clear();

                _oEntities.DeleteObject(oCustomersFromShelf);
                _oEntities.SaveChanges();

                foreach (gen_ContactPerson oContcat in ovContacts)
                {
                    _oEntities.DeleteObject(oContcat);
                }
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public object GetRaw()
        {
            return ogen_Customer;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public List<int> QuaterlyCustomersYear()
        {

            List<int> oResult = new List<int>();
            int Count = 0;
            for (int i = DateTime.Now.Year - 2; i <= DateTime.Now.Year; i++)
            {
                //for (int j = i+2; j <= i + 2; j++)
                //{
                Count = _oEntities.gen_Customer.Where(p => p.CreatedDateOn.Year == i).Count();
                //}
                oResult.Add(Count);
            }
            return oResult;
        }


        public Dictionary<string, int> QuaterlyCustomers()
        {

            Dictionary<string, int> oResult = new Dictionary<string, int>();
            int Count = 0;
            int s = 0;
            for (int k = DateTime.Now.Year - 2; k <= DateTime.Now.Year; k++)
            {
                for (int i = 1; i <= 12; i = i + 3)
                {
                    Count = _oEntities.gen_Customer.Where(p => p.CreatedDateOn.Month >= i && p.CreatedDateOn.Month <= i + 2 && p.CreatedDateOn.Year == k).Count();

                    oResult.Add(s.ToString(), Count);
                    s++;
                }
            }
            return oResult;
        }
    
        
        
        //public Dictionary<string, string> CustomerCount()
        //{
        //    Dictionary<string, string> oResult = new Dictionary<string, string>();
        //    int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
        //    int Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
        //    IEnumerable<gen_Customer> oCustNos = _oEntities.gen_Customer.Where(p => p.Pk_BranchID == Branch && p.Fk_Tanent == tannent);
        //    //IEnumerable<Vw_NoOrderServices> oOrdNos = _oEntities.Vw_NoOrderServices.Where(p => p.Pk_BranchID == Branch  && p.Fk_Tanent == tannent && p.OrderDate > oFrom && p.OrderDate<=oTo);
        //    int s = 0;
        //    foreach (Vw_NoOrderServices oOrd in oOrdNos)
        //    {
        //        oResult.Add(s.ToString(), oOrd.Pk_Order + " - " + DateTime.Parse(oOrd.OrderDate.ToString()).ToString("dd/MM/yyyy"));
        //        s++;
        //    }
        //    return oResult;
        //}
        #endregion Methods
    }
}
