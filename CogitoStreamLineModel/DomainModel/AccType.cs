﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class AccType : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private AccountType oAcc = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_AccountTypeID = null;
            string sAccTypeName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<AccountType> oAccs = null;

            try { sPk_AccountTypeID = p_Params.Single(p => p.ParameterName == "Pk_AccountTypeID").ParameterValue; }
            catch { }
            try { sAccTypeName = p_Params.Single(p => p.ParameterName == "AccTypeName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oAccs = _oEntites.AccountType;

            try
            {
                if (!string.IsNullOrEmpty(sPk_AccountTypeID))
                {
                    oAccs = oAccs.Where(p => p.Pk_AccountTypeID == decimal.Parse(sPk_AccountTypeID));
                }
                if (!string.IsNullOrEmpty(sAccTypeName))
                {
                    oAccs = oAccs.Where(p => p.AccTypeName.IndexOf(sAccTypeName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oAccs.Count();

            oAccs.OrderBy(p => p.Pk_AccountTypeID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oAccs = oAccs.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oAccs.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        //public SearchResult SearchAccTypeName(List<SearchParameter> p_Params)
        //{

        //    SearchResult oSearchResult = new SearchResult();
        //    IEnumerable<AccountType> oAccTypeName = null;
        //    oAccTypeName = _oEntites.AccountType;

        //    oSearchResult.RecordCount = oAccTypeName.Count();
        //    oAccTypeName.OrderBy(p => p.Pk_AccountTypeID);

        //    List<EntityObject> oFilteredItem = oAccTypeName.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItem;

        //    return oSearchResult;
        //}

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                AccountType oNewAcc = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(AccountType), omValues) as AccountType;

                _oEntites.AddToAccountType(oNewAcc);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                AccountType oAcc = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(AccountType), omValues) as AccountType;
                AccountType oAccFromShelf = _oEntites.AccountType.Where(p => p.Pk_AccountTypeID == oAcc.Pk_AccountTypeID).Single();
                object orefItems = oAccFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(AccountType), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                AccountType oAcc = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(AccountType), omValues) as AccountType;
                AccountType oAccFromShelf = _oEntites.AccountType.Where(p => p.Pk_AccountTypeID == oAcc.Pk_AccountTypeID).Single();
                object orefItems = oAccFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oAccFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oAcc;

            }
            set
            {
                oAcc = value as AccountType;
            }
        }

        public decimal ID
        {
            get
            {
                return oAcc.Pk_AccountTypeID;
            }
            set
            {
                oAcc = _oEntites.AccountType.Where(p => p.Pk_AccountTypeID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oAcc;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


