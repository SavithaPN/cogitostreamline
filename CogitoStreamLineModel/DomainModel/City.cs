﻿//Name          : City Class
//Description   : Contains the Country class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0012
//Modifications : 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class City : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private gen_City oCity = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CityID = null;
            string sCityName = null;
            string sCountry = null;
            string sStateId = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_City> oCitys = null;

            try { sPk_CityID = p_Params.Single(p => p.ParameterName == "Pk_CityID").ParameterValue; }
            catch { }
            try { sCityName = p_Params.Single(p => p.ParameterName == "CityName").ParameterValue; }
            catch { }
            try { sCountry = p_Params.Single(p => p.ParameterName == "Country").ParameterValue; }
            catch { }
            try { sStateId = p_Params.Single(p => p.ParameterName == "StateId").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oCitys = _oEntites.gen_City;
            try
            {
                if (!string.IsNullOrEmpty(sPk_CityID))
                {
                    oCitys = oCitys.Where(p => p.Pk_CityID == decimal.Parse(sPk_CityID));
                }
                if (!string.IsNullOrEmpty(sCityName))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oCitys = oCitys.Where(p => p.CityName.ToLower() == (sCityName.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sCountry))
                {
                    oCitys = oCitys.Where(p => p.Country == decimal.Parse(sCountry));
                }
                if (!string.IsNullOrEmpty(sStateId))
                {
                    oCitys = oCitys.Where(p => p.Fk_StateID == decimal.Parse(sStateId));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oCitys.Count();
         oCitys= oCitys.OrderBy(p => p.CityName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oCitys = oCitys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oCitys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchCityName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_City> oCityName = null;
            oCityName = _oEntites.gen_City;

            oSearchResult.RecordCount = oCityName.Count();
            oCityName.OrderBy(p => p.Pk_CityID);

            List<EntityObject> oFilteredItem = oCityName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_City oNewCitys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_City), omValues) as gen_City;

                _oEntites.AddTogen_City(oNewCitys);

                _oEntites.SaveChanges();
                oResult.Success = true;
            


            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_City oCitys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_City), omValues) as gen_City;
                gen_City oCitysFromShelf = _oEntites.gen_City.Where(p => p.Pk_CityID == oCitys.Pk_CityID).Single();
                object orefItems = oCitysFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_City), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_City oCitys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_City), omValues) as gen_City;
                gen_City oCitysFromShelf = _oEntites.gen_City.Where(p => p.Pk_CityID == oCitys.Pk_CityID).Single();
                object orefItems = oCitysFromShelf;


                IEnumerable<gen_ContactPerson> ogenCont = _oEntites.gen_ContactPerson.Where(p => p.Address_City == oCitys.Pk_CityID);
                if (ogenCont.Count() > 0)
                {
                    oResult.Success = false;
                    oResult.Message = "Cannot Delete as this City is Referenced else where";
                }

                else
                {
                    IEnumerable<gen_VendorContacts> ogenVendorCont = _oEntites.gen_VendorContacts.Where(p => p.Fk_ContactPerson == oCitys.Pk_CityID);
                    if (ogenVendorCont.Count() > 0)
                    {
                        oResult.Success = false;
                        oResult.Message = "Cannot Delete as this City is Referenced else where";
                    }


                    else
                    {
                        IEnumerable<gen_CustomerContacts> ogenCustomerCont = _oEntites.gen_CustomerContacts.Where(p => p.Fk_ContactPerson == oCitys.Pk_CityID);
                        if (ogenCustomerCont.Count() > 0)
                        {
                            oResult.Success = false;
                            oResult.Message = "Cannot Delete as this City is Referenced else where";
                        }
                    }


                    _oEntites.DeleteObject(oCitysFromShelf);

                    _oEntites.SaveChanges();
                    oResult.Success = true;
                }


              
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oCity;

            }
            set
            {
                oCity = value as gen_City;
            }
        }

        public decimal ID
        {
            get
            {
                return oCity.Pk_CityID;
            }
            set
            {
                oCity = _oEntites.gen_City.Where(p => p.Pk_CityID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oCity;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




