﻿//Name          : Employee Class
//Description   : Contains the Employee class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 21/10/2015
//Crh Number    : 
//Modifications : 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
    public class Employee : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private emp_EmployeeMaster oemp_EmployeeMaster = null;
        

        //public SearchResult Search1(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();
        //    IEnumerable<gen_EmployeeType> oEmployees = null;
        //    oEmployees = _oEntites.gen_EmployeeType;

        //    oSearchResult.RecordCount = oEmployees.Count();
        //    oEmployees.OrderBy(p => p.EmployeeTypeName);

        //    List<EntityObject> oFilteredVendorRelationShip = oEmployees.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredVendorRelationShip;

        //    return oSearchResult;
        //}

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Employee based on 
            //First name, Last name and Mobile
            string sPk_EmployeeId = null;
            string sFirstName = null;
            string sLastName = null;
            string sMobile = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<emp_EmployeeMaster> oEmployees = null;

            try { sPk_EmployeeId = p_Params.Single(p => p.ParameterName == "Pk_EmployeeId").ParameterValue; }
            catch { }
            try { sFirstName = p_Params.Single(p => p.ParameterName == "FirstName").ParameterValue; }
            catch { }
            try { sLastName = p_Params.Single(p => p.ParameterName == "LastName").ParameterValue; }
            catch { }
            try { sMobile = p_Params.Single(p => p.ParameterName == "Mobile").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oEmployees = _oEntites.emp_EmployeeMaster;

            try
            {
                if (!string.IsNullOrEmpty(sPk_EmployeeId))
                {
                    oEmployees = oEmployees.Where(p => p.Pk_EmployeeId==Convert.ToDecimal(sPk_EmployeeId));
                }
                if (!string.IsNullOrEmpty(sFirstName))
                {
                    oEmployees = oEmployees.Where(p => p.FirstName.IndexOf(sFirstName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sLastName))
                {
                    oEmployees = oEmployees.Where(p => p.LastName.IndexOf(sLastName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sMobile))
                {
                    oEmployees = oEmployees.Where(p => p.Mobile==sMobile);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEmployees.Count();
            oEmployees.OrderBy(p => p.FirstName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEmployees = oEmployees.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredEmployees = oEmployees.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredEmployees;

            return oSearchResult;

        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_EmployeeMaster oNewEmployee = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_EmployeeMaster), omValues) as emp_EmployeeMaster;
                if (omValues.ContainsKey("Documents"))
                {
                    string sDocs = omValues["Documents"].ToString();
                    if (sDocs != "")
                    {
                        object[] Docs = JsonConvert.DeserializeObject<object[]>(sDocs);

                        foreach (object oItem in Docs)
                        {
                            emp_EmployeeImage oEmployeeImages = new emp_EmployeeImage();
                            Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                            oEmployeeImages.ImageName = oProperties["FileName"];
                            oEmployeeImages.ImageSize = oProperties["FileSize"];
                            oNewEmployee.emp_EmployeeImage.Add(oEmployeeImages);
                        }
                    }
                }
                oNewEmployee.IsActive = true;
                _oEntites.AddToemp_EmployeeMaster(oNewEmployee);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_EmployeeMaster oEmployee = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_EmployeeMaster), omValues) as emp_EmployeeMaster;
                emp_EmployeeMaster oEmployeeFromShelf = _oEntites.emp_EmployeeMaster.Where(p => p.Pk_EmployeeId == oEmployee.Pk_EmployeeId).Single();
                oEmployeeFromShelf.emp_EmployeeImage.Clear();
                if (omValues.ContainsKey("Documents"))
                {
                    string sDocs = omValues["Documents"].ToString();
                    object[] Docs = JsonConvert.DeserializeObject<object[]>(sDocs);

                    foreach (object oItem in Docs)
                    {
                        emp_EmployeeImage oEmployeeImages = new emp_EmployeeImage();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oEmployeeImages.ImageName = oProperties["FileName"];
                        oEmployeeImages.ImageSize = oProperties["FileSize"];
                        oEmployeeFromShelf.emp_EmployeeImage.Add(oEmployeeImages);
                    }
                }
                object orefEmployee = oEmployeeFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(emp_EmployeeMaster), omValues, ref orefEmployee);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_EmployeeMaster oEmployee = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_EmployeeMaster), omValues) as emp_EmployeeMaster;
                emp_EmployeeMaster oEmployeeFromShelf = _oEntites.emp_EmployeeMaster.Where(p => p.Pk_EmployeeId == oEmployee.Pk_EmployeeId).Single();
                _oEntites.DeleteObject(oEmployeeFromShelf);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oemp_EmployeeMaster;
            }
            set
            {
                oemp_EmployeeMaster = value as emp_EmployeeMaster;
            }
        }

        public decimal ID
        {
            get
            {
                return oemp_EmployeeMaster.Pk_EmployeeId;
            }
            set
            {
                oemp_EmployeeMaster = _oEntites.emp_EmployeeMaster.Where(p => p.Pk_EmployeeId == value).Single();
            }
        }

        public object GetRaw()
        {
            return oemp_EmployeeMaster;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


      
    }
}
