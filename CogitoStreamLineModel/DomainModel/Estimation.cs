﻿  

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class Estimation : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private est_Estimation oEstimation = new est_Estimation();
        private est_BoxEstimation oBoxEstimation = new est_BoxEstimation();

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            string sEstimate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_EstDet1> oEstimates = null;

            SearchResult oSearchResult = new SearchResult();

            try { sEstimate = p_Params.Single(p => p.ParameterName == "Fk_BoxEstimation").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oEstimates = _oEntites.Vw_EstDet1;

            try
            {
                if (sEstimate != null)
                {
                    oEstimates = oEstimates.Where(p => p.Fk_BoxEstimation == decimal.Parse(sEstimate));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEstimates.Count();
            oEstimates = oEstimates.OrderByDescending(p => p.Fk_BoxEstimation);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEstimates = oEstimates.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oEstimates.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }


        public SearchResult SearchEst(List<SearchParameter> p_Params)
        {
            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            decimal? sEstimate = null;
            string sStartIndex = null;
            decimal? sFk_Enquiry = null;
            string sEstDate = null;
            string sCustomer = null;
            string sPageSize = null;
            string sSorting = null;
            //IEnumerable<Vw_Estimate> oEstimates = null;
            IEnumerable<Vw_Est_Det> oEstimates = null;

            SearchResult oSearchResult = new SearchResult();

            try { sEstimate = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Pk_BoxEstimation").ParameterValue); }
            catch { }

            try { sFk_Enquiry = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue); }
            catch { }
            try { sEstDate = p_Params.Single(p => p.ParameterName == "EstDate").ParameterValue; }
            catch { }

            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            //oEstimates = _oEntites.Vw_Estimate;
            oEstimates = _oEntites.Vw_Est_Det;


            try
            {
                if (sEstimate != null)
                {
                    //oEstimates = oEstimates.Where(p => p.Pk_Estimate == sEstimate);
                    oEstimates = oEstimates.Where(p => p.Pk_BoxEstimation == sEstimate);
                }
                if (sFk_Enquiry != null)
                {
                    //oEstimates = oEstimates.Where(p => p.Pk_Estimate == sEstimate);
                    oEstimates = oEstimates.Where(p => p.Fk_Enquiry == sFk_Enquiry);
                }

                if (!string.IsNullOrEmpty(sEstDate))
                {
                    //var CHK = Convert.ToDateTime(sEstDate.Substring(0, 10));
                    oEstimates = oEstimates.Where(p => DateTime.Parse(p.EstDATE.ToString()).ToString("dd/MM/yyyy") == sEstDate);
                }
                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oEstimates = oEstimates.Where(p => p.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEstimates.Count();
            //oEstimates = oEstimates.OrderByDescending(p => p.Pk_Estimate);
            oEstimates = oEstimates.OrderByDescending(p => p.Pk_BoxEstimation);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEstimates = oEstimates.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oEstimates.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
        public bool SetEnquiryId(decimal p_dEnquiry)
        {
            if (_oEntites.est_Estimation.Where(p => p.Fk_Enquiry == p_dEnquiry).Count() > 0)
            {
                oEstimation = _oEntites.est_Estimation.Where(p => p.Fk_Enquiry == p_dEnquiry).First();
                return true;
            }

            return false;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }


        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {

                est_BoxEstimation oNewBoxEst = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_BoxEstimation), omValues) as est_BoxEstimation;
          est_BoxEstimationChild oNewBoxEstChild=new est_BoxEstimationChild();



          oNewBoxEst.Date = DateTime.Now;
                  oNewBoxEstChild.Fk_EnquiryChild=Convert.ToDecimal(omValues["Fk_EnquiryChild"]);
                 oNewBoxEstChild.ConvRate=Convert.ToDecimal(omValues["ConvRate"]);
       oNewBoxEstChild.ConvValue=Convert.ToDecimal(omValues["ConvValue"]);
       oNewBoxEstChild.GMarginPercentage=Convert.ToDecimal(omValues["GMarginPercentage"]);
       oNewBoxEstChild.GMarginValue=Convert.ToDecimal(omValues["GMarginValue"]);
      oNewBoxEstChild.TaxesPercntage=Convert.ToDecimal(omValues["TaxesPercntage"]);
       oNewBoxEstChild.TaxesValue=Convert.ToDecimal(omValues["TaxesValue"]);
       oNewBoxEstChild.TransportValue=Convert.ToDecimal(omValues["TransportValue"]);
       oNewBoxEstChild.WeightHValue=Convert.ToDecimal(omValues["Printing"]);
       //oNewBoxEstChild.HandlingChanrgesValue=Convert.ToDecimal(omValues["Fk_EnquiryChild"]);
       //oNewBoxEstChild.PackingChargesValue=Convert.ToDecimal(omValues["Fk_EnquiryChild"]);
       oNewBoxEstChild.RejectionPercentage=Convert.ToDecimal(omValues["Development"]);
       oNewBoxEstChild.RejectionValue=Convert.ToDecimal(omValues["Inventory"]);
       oNewBoxEstChild.TotalWeight=Convert.ToDecimal(omValues["TotalWeight"]);
       oNewBoxEstChild.TotalPrice=Convert.ToDecimal(omValues["TotalPrice"]);
       oNewBoxEstChild.VATPercentage=Convert.ToDecimal(omValues["VATPercentage"]);
       oNewBoxEstChild.VATValue=Convert.ToDecimal(omValues["VATValue"]);
       oNewBoxEstChild.CSTPercentage=Convert.ToDecimal(omValues["TaxesPercntage"]);
       oNewBoxEstChild.CSTValue=Convert.ToDecimal(omValues["TaxesValue"]);
       oNewBoxEstChild.TransportPercentage = Convert.ToDecimal(omValues["TransportPercentage"]);
       var CalcQty = Convert.ToDecimal(omValues["QtyValue"]);
       var Pval = Convert.ToDecimal(omValues["TotalPrice"]);
       if (CalcQty > 1)
           oNewBoxEst.GrandTotal = Pval / CalcQty;
       else

       { oNewBoxEst.GrandTotal = Convert.ToDecimal(omValues["TotalPrice"]); }
       _oEntites.AddToest_BoxEstimationChild(oNewBoxEstChild);

                            _oEntites.SaveChanges();
             
          _oEntites.AddToest_BoxEstimation(oNewBoxEst);

          _oEntites.SaveChanges();
          oNewBoxEstChild.Fk_BoxEstimation = oNewBoxEst.Pk_BoxEstimation;
          _oEntites.SaveChanges();
          oResult.Success = true;
                // oResult.Success = true;
          oResult.Message = oNewBoxEst.Pk_BoxEstimation.ToString();

               // /////////////////////////////////// old code
               // var dateval = DateTime.Now;
               // Dictionary<string, object> omValuesChild = new Dictionary<string,object>();
               // decimal grandTotal = 0;

               //est_BoxEstimation oEstimateFromShelf = _oEntites.est_BoxEstimation.Where(p => p.Pk_BoxEstimation == 26).Single();

               // omValues.Add("Fk_Customer", oEstimateFromShelf.est_BoxEstimationChild.FirstOrDefault().eq_EnquiryChild.eq_Enquiry.Customer);
               // omValues.Add("Description", "");
               // omValues.Add("QuotationDATE", dateval);
               // //omValues.Add("GrandTotal", oEstimateFromShelf.GrandTotal);
               // omValues.Add("Fk_BoxEstimation", oEstimateFromShelf.Pk_BoxEstimation);
                   
               // QuotationMaster oNewQuote = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationMaster), omValues) as QuotationMaster;

               // foreach (var boxEstChild in oEstimateFromShelf.est_BoxEstimationChild)
               //    {
               //        omValuesChild.Add("Fk_BoxID", boxEstChild.eq_EnquiryChild.Fk_BoxID);
               //        omValuesChild.Add("Quantity", boxEstChild.eq_EnquiryChild.Quantity);
               //        omValuesChild.Add("Price", boxEstChild.TotalPrice);                       
               //        omValuesChild.Add("Amount", boxEstChild.eq_EnquiryChild.Quantity * boxEstChild.TotalPrice);
               //        grandTotal = (decimal)grandTotal + (decimal)(boxEstChild.eq_EnquiryChild.Quantity * boxEstChild.TotalPrice);
               //        QuotationDetail oNewDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(QuotationDetail), omValuesChild) as QuotationDetail;
               //        oNewQuote.QuotationDetails.Add(oNewDetail);
               //        omValuesChild.Clear();
               //    }

               // /////////////////////////////////////////////////////////////oldcode
               // oNewQuote.Grandtotal = grandTotal;
               // _oEntites.AddToQuotationMaster(oNewQuote);
               // _oEntites.SaveChanges();

               // oResult.Success = true;
               // oResult.Message = oNewQuote.Pk_Quotation.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
               
            //ModelManuplationResult oResult = new ModelManuplationResult();

            //try
            //{
            //    est_Estimation oEstimation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_Estimation), omValues) as est_Estimation;

            //    string sParts = omValues["parts"].ToString();
            //    object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

            //    foreach (object oItem in Parts)
            //    {
            //        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
            //        est_EstimationPart oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_EstimationPart), oProperties) as est_EstimationPart;

            //        string sLayers = oProperties["layers"].ToString();
            //        object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

            //        foreach (object oLayerItem in Layers)
            //        {
            //            Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
            //            est_Layer oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_Layer), oLayerProperties) as est_Layer;

            //            oPart.est_Layer.Add(oLayer);
            //        }

            //        oEstimation.est_EstimationPart.Add(oPart);
                   
            //    }
                
            //    _oEntites.AddToest_Estimation(oEstimation);
            //    _oEntites.SaveChanges();
            //    oResult.Message = oEstimation.Pk_Estimate.ToString();
            //    oResult.Success = true;

              

            //}
            //catch (Exception e)
            //{   
            //    oResult.Success = false;
            //    oResult.Message = e.Message;
            //    oResult.Exception = e;
            //}
            //return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                est_Estimation oEstimation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_Estimation), omValues) as est_Estimation;
                decimal estid = decimal.Parse(omValues["id"].ToString());
                est_Estimation oEstimateFromShelf = _oEntites.est_Estimation.Where(p => p.Pk_Estimate == estid).Single();
                
                object orefEstimate = oEstimateFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_Estimation), omValues, ref orefEstimate);

                string sParts = omValues["parts"].ToString();
                object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                foreach (object oItem in Parts)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    decimal id= decimal.Parse(oProperties["id"].ToString());
                    est_EstimationPart oEstimatePartFromShelf = _oEntites.est_EstimationPart.Where(p => p.Pk_DetailedEstimationPart == id).Single();

                    object orefEstimatePart = oEstimatePartFromShelf;
                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_EstimationPart), oProperties, ref orefEstimatePart);

                    string sLayers = oProperties["layers"].ToString();
                    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                    foreach (object oLayerItem in Layers)
                    {

                        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                        decimal layertid = decimal.Parse(oLayerProperties["id"].ToString());
                        est_Layer oEstimateLayerFromShelf = _oEntites.est_Layer.Where(p => p.Pk_Layer == layertid).Single();

                        object orefEstimateLayer = oEstimateLayerFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_Layer), oLayerProperties, ref orefEstimateLayer);
                        
                    }
                   

                }

                _oEntites.SaveChanges();
                oResult.Message = oEstimation.Pk_Estimate.ToString();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        public EntityObject DAO
        {
            get
            {
                //return oEstimation;
                return oBoxEstimation;
            }
            set
            {
                //oEstimation = value as est_Estimation;
                oBoxEstimation = value as est_BoxEstimation;
            }
        }

        public decimal ID
        {
            get
            {
                //return oEstimation.Pk_Estimate;
                return oBoxEstimation.Pk_BoxEstimation;
            }
            set
            {
                //oEstimation = _oEntites.est_Estimation.Where(p => p.Pk_Estimate == value).Single();
                oBoxEstimation = _oEntites.est_BoxEstimation.Where(p => p.Pk_BoxEstimation == value).Single();
            }
        }

        public object GetRaw()
        {
            //return oEstimation;
            return oBoxEstimation;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public EntityObject AddDetail(string sBasketName, EntityObject oDetail)
        {
            throw new NotImplementedException();
        }

        public EntityObject RemoveDetail(string sBasketName, decimal dPk)
        {
            throw new NotImplementedException();
        }

        public EntityObject UpdateDetail(string sBasketName, decimal dPk, Dictionary<string, object> oValues)
        {
            throw new NotImplementedException();
        }

        public List<EntityObject> GetDetails(string sBasketName)
        {
            throw new NotImplementedException();
        }


    }
}
