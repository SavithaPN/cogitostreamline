﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
using System.Web;


namespace CogitoStreamLineModel.DomainModel
{
    public class CustComplaints : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private CustomerComplaint oComplaint = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_ComplaintID = null;
            string sComplaintDate = null;
            string sFk_CustomerID = null;
            string sFk_User = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<CustomerComplaint> oComplaints = null;

            try { sPk_ComplaintID = p_Params.Single(p => p.ParameterName == "Pk_ComplaintID").ParameterValue; }
            catch { }
            try { sComplaintDate = p_Params.Single(p => p.ParameterName == "ComplaintDate").ParameterValue; }
            catch { }
            try { sFk_CustomerID = p_Params.Single(p => p.ParameterName == "Fk_CustomerID").ParameterValue; }
            catch { }
            try { sFk_User = p_Params.Single(p => p.ParameterName == "Fk_User").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oComplaints = _oEntites.CustomerComplaints;
            try
            {
                if (!string.IsNullOrEmpty(sPk_ComplaintID))
                {
                    oComplaints = oComplaints.Where(p => p.Pk_ComplaintID == decimal.Parse(sPk_ComplaintID));
                }
                if (!string.IsNullOrEmpty(sComplaintDate))
                {
                    oComplaints = oComplaints.Where(p => p.ComplaintDate == Convert.ToDateTime(sComplaintDate));
                }
                //if (!string.IsNullOrEmpty(sFk_CustomerID))
                //{
                //    oComplaints = oComplaints.Where(p => p.Fk_CustomerID == decimal.Parse(sFk_CustomerID));
                //}oMaterials.Where(p =>p.Fk_Color!=null && p.gen_Color.ColorName.IndexOf(sColor, StringComparison.OrdinalIgnoreCase) >= 0);
                if (!string.IsNullOrEmpty(sFk_CustomerID))
                {
                    oComplaints = oComplaints.Where(p => p.Fk_CustomerID != null && p.gen_Customer.CustomerName.IndexOf(sFk_CustomerID, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFk_User))
                {
                    oComplaints = oComplaints.Where(p => p.Fk_User == decimal.Parse(sFk_User));
                }
            }



            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oComplaints.Count();

            oComplaints.OrderBy(p => p.Pk_ComplaintID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oComplaints = oComplaints.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oComplaints.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchComlaint(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<CustomerComplaint> oCmpName = null;
            oCmpName = _oEntites.CustomerComplaints;

            oSearchResult.RecordCount = oCmpName.Count();
            oCmpName.OrderBy(p => p.Pk_ComplaintID);

            List<EntityObject> oFilteredItem = oCmpName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                CustomerComplaint oNewComplaints = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(CustomerComplaint), omValues) as CustomerComplaint;


                oNewComplaints.Fk_User = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

                oNewComplaints.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);

                oNewComplaints.Fk_Status = _oEntites.wfStates.Where(p => p.State == "New").Single().Pk_State;


                _oEntites.AddToCustomerComplaints(oNewComplaints);




                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                CustomerComplaint oComplaints = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(CustomerComplaint), omValues) as CustomerComplaint;
                CustomerComplaint oComplaintsFromShelf = _oEntites.CustomerComplaints.Where(p => p.Pk_ComplaintID == oComplaints.Pk_ComplaintID).Single();
                object orefItems = oComplaintsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(CustomerComplaint), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                CustomerComplaint oComplaints = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(CustomerComplaint), omValues) as CustomerComplaint;
                CustomerComplaint oComplaintsFromShelf = _oEntites.CustomerComplaints.Where(p => p.Pk_ComplaintID == oComplaints.Pk_ComplaintID).Single();
                object orefItems = oComplaintsFromShelf;
                //_oEntites.DeleteObject(oComplaintsFromShelf);

                _oEntites.DeleteObject(oComplaintsFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oComplaint;

            }
            set
            {
                oComplaint = value as CustomerComplaint;
            }
        }

        public decimal ID
        {
            get
            {
                return oComplaint.Pk_ComplaintID;
            }
            set
            {
                oComplaint = _oEntites.CustomerComplaints.Where(p => p.Pk_ComplaintID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oComplaint;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public EntityObject AddDetail(string sBasketName, EntityObject oDetail)
        {
            throw new NotImplementedException();
        }

        public EntityObject RemoveDetail(string sBasketName, decimal dPk)
        {
            throw new NotImplementedException();
        }

        public EntityObject UpdateDetail(string sBasketName, decimal dPk, Dictionary<string, object> oValues)
        {
            throw new NotImplementedException();
        }

        public List<EntityObject> GetDetails(string sBasketName)
        {
            throw new NotImplementedException();
        }
    }
}


