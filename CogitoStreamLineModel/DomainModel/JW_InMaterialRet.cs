﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class JW_InMaterialRet : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private JW_RM_RetMast oInward = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sFromDate = null;
            string sToDate = null;

            string sVendor = null;
            string sInwardNo = null;
            string sMaterial = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<JW_RM_RetMast> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_Inward").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            catch { }
            try { sMaterial = p_Params.Single(p => p.ParameterName == "Material").ParameterValue; }
            catch { }

            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.JW_RM_RetMast;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }
                if (!string.IsNullOrEmpty(sVendor))
                {
                    oInwards = oInwards.Where(p => p.gen_Customer.CustomerName != null && p.gen_Customer.CustomerName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sMaterial))
                //{
                //    oInwards = oInwards.Where(p =>  p.Inv_Material.Name.IndexOf(sMaterial, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oInwards = oInwards.Where(p => (p.Inward_Date >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oInwards = oInwards.Where(p => (p.Inward_Date <= Convert.ToDateTime(sToDate)));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_Inward);
            //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInwards = oInwards.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public SearchResult SearchInwdDet(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username

            string sInwardNo = null;

            string sPk_Mat = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_JW_RetRep> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_Inward").ParameterValue; }
            catch { }
            try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.Vw_JW_RetRep;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_Inward);
            //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oInwards = oInwards.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public SearchResult SearchInwdDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username

            string sInwardNo = null;

            string sPk_Mat = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_TotInwdQty> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_Inward").ParameterValue; }
            catch { }
            try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.Vw_TotInwdQty;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_Inward == decimal.Parse(sInwardNo));
                }
                if (!string.IsNullOrEmpty(sPk_Mat))
                {
                    oInwards = oInwards.Where(p => p.Pk_Material == decimal.Parse(sPk_Mat));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_Inward);
            //OrderBy(p => p.Pk_Inward);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oInwards = oInwards.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }


        public ModelManuplationResult CreateNew()
        {

            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                JW_RM_RetMast oNewInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetMast), omValues) as JW_RM_RetMast;
                //PurchaseOrderM oPurOrder=new PurchaseOrderM();

                var CustVal = omValues["Fk_Customer"].ToString();

                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        JW_RM_RetDet oNewInwardD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetDet), oDataProperties) as JW_RM_RetDet;


                        oNewInward.Inward_Date = Convert.ToDateTime(omValues["Inward_Date"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                        oNewInward.JW_RM_RetDet.Add(oNewInwardD);



                        /////////////////stock

                        // Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oNewInwardD.Fk_Material).Single();
                        var custCode = Convert.ToDecimal(CustVal);
                        int StkCnt1 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oNewInwardD.Fk_Material && p.Fk_Customer == custCode).Count();

                        if (StkCnt1 > 0)
                        {
                            JobWorkRMStock oStockVal = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oNewInwardD.Fk_Material && p.Fk_Customer == custCode).Single();
                            oStockVal.StockValue = Convert.ToDecimal(oStockVal.StockValue - oNewInwardD.Quantity);
                            //oStockVal.RollNo = oNewInwardD.RollNo;
                        }
                        else
                        {
                            JobWorkRMStock oStockVal = new JobWorkRMStock();
                            oStockVal.Fk_Customer = Convert.ToDecimal(CustVal);
                            oStockVal.Fk_Material = oNewInwardD.Fk_Material;
                            oStockVal.StockValue = Convert.ToDecimal(oNewInwardD.Quantity);
                            oStockVal.RollNo = oNewInwardD.RollNo;
                            _oEntities.AddToJobWorkRMStock(oStockVal);

                        }

                        _oEntities.SaveChanges();

                    }


                }

               // oNewInward.Inward_Date = DateTime.Now;

                oNewInward.Inward_Date = Convert.ToDateTime(omValues["Inward_Date"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                _oEntities.AddToJW_RM_RetMast(oNewInward);

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }



        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                JW_RM_RetMast oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetMast), omValues) as JW_RM_RetMast;
                JW_RM_RetMast OPOrdFromShelf = _oEntities.JW_RM_RetMast.Where(p => p.Pk_Inward == oPOrd.Pk_Inward).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JW_RM_RetMast), omValues, ref orefPO);


                string sMaterials = omValues["Materials"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<JW_RM_RetDet> NewMaterialsList = new List<JW_RM_RetDet>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_InwardDet"] == null || oDataProperties2["Pk_InwardDet"].ToString() == "")
                    {
                        JW_RM_RetDet oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetDet), oDataProperties2) as JW_RM_RetDet;

                        NewMaterialsList.Add(oNewPOrderD);



                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oNewPOrderD.Fk_Material).Single();

                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {


                            Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oNewPOrderD.Fk_Material).Single();

                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oNewPOrderD.Quantity);
                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oNewPOrderD.Quantity);
                        }
                        else
                        {

                            //oStockVal.RollNo = oNewInwardD.RollNo;
                            int StkCnt2 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oNewPOrderD.Fk_Material && p.RollNo == oNewPOrderD.RollNo).Count();

                            if (StkCnt2 > 0)
                            {
                                JobWorkRMStock oStockVal = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oNewPOrderD.Fk_Material && p.RollNo == oNewPOrderD.RollNo).Single();

                                oStockVal.StockValue = Convert.ToDecimal(oStockVal.StockValue - oNewPOrderD.Quantity);
                                oStockVal.StockValue = Convert.ToDecimal(oStockVal.StockValue + oNewPOrderD.Quantity);
                            }
                            else
                            {
                                //_oEntities.DeleteObject(oPOrderDet);
                            }
                        }
                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_InwardDet"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        //     gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == dFkMaterial).Single();

                        JW_RM_RetDet oPOrderDetFromShelf = _oEntities.JW_RM_RetDet.Where(p => p.Pk_InwardDet == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                        object oPOrderDet
                                            = _oEntities.JW_RM_RetDet.Where(p => p.Pk_InwardDet == dPkPOrdDet).First();

                        _oEntities.DeleteObject(oPOrderDet);


                        JW_RM_RetDet oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetDet), oDataProperties2) as JW_RM_RetDet;


                        //QualityChild oQC = _oEntities.QualityChild.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.FkQualityCheck == oPOrd.Fk_QC).Single();
                        //oQC.PendingQty = Convert.ToDecimal(oQC.PendingQty - oIndentDetails.Quantity);


                        NewMaterialsList.Add(oIndentDetails);
                        UpdatedPk.Add(dPkPOrdDet);


                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {


                            Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oIndentDetails.Fk_Material).Single();

                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oPOrderDetFromShelf.Quantity);
                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oIndentDetails.Quantity);
                        }
                        else
                        {

                            int StkCnt2 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oIndentDetails.RollNo).Count();

                            if (StkCnt2 > 0)
                            {

                                JobWorkRMStock oStockVal = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oIndentDetails.RollNo).Single();

                                oStockVal.StockValue = Convert.ToDecimal(oStockVal.StockValue - oPOrderDetFromShelf.Quantity);
                                oStockVal.StockValue = Convert.ToDecimal(oStockVal.StockValue + oIndentDetails.Quantity);

                            }
                            else
                            {
                                object oPOrderDet1
                                        = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oPOrderDetFromShelf.RollNo).First();

                                _oEntities.DeleteObject(oPOrderDet1);

                                JobWorkRMStock oStockVal1 = new JobWorkRMStock();

                                oStockVal1.Fk_Material = dFkMaterial;
                                oStockVal1.StockValue = Convert.ToDecimal(oDataProperties2["Quantity"]);
                                oStockVal1.RollNo = oDataProperties2["RollNo"].ToString();
                                _oEntities.AddToJobWorkRMStock(oStockVal1);
                            }
                        }
                    }


                }
                // Handeling Deleted Records

                List<JW_RM_RetDet> oDeletedRecords = OPOrdFromShelf.JW_RM_RetDet.Where(p => !UpdatedPk.Contains(p.Pk_InwardDet)).ToList();



                //Add new elements
                foreach (JW_RM_RetDet oNewMaterialDetail in NewMaterialsList)
                {
                    OPOrdFromShelf.JW_RM_RetDet.Add(oNewMaterialDetail);
                }

                //object orefPurchaseM = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PurchaseOrderM), omValues, ref orefPurchaseM);

                //OPOrdFromShelf.GrandTotal = gtot;

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            //    JW_RM_RetMast oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetMast), omValues) as JW_RM_RetMast;
            //    JW_RM_RetMast oInwardFromShelf = _oEntities.JW_RM_RetMast.Where(p => p.Pk_Inward == oInward.Pk_Inward).Single();
            //    object orefItems = oInwardFromShelf;

            //    var TanentID=Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            //    var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            //    Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == branchid).Single();

            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInwardFromShelf.Quantity);
            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oInward.Quantity);

            //    PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded)-Convert.ToDecimal(oInwardFromShelf.Quantity);
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded) + Convert.ToDecimal(omValues["Quantity"]);
            //    JW_RM_RetMast oIwd = _oEntities.JW_RM_RetMast.Where(p => p.Fk_Material == oInwardFromShelf.Fk_Material && p.Pk_Inward == oInward.Pk_Inward).Single();
            //    //QualityCheck oQualitycheckPending = _oEntities.QualityChecks.Where(p => p.Pk_QualityCheck == oInward.Fk_QualityCheck && p.Fk_Material == oInward.Fk_Material).Single();
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending + oIwd.Quantity);
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending - oInward.Quantity);

            //    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JW_RM_RetMast), omValues, ref orefItems);

            //    //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();







            //       decimal InwdFkMaterial = 0;
            //       InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);



            //       //oPendingNew.Quantity = Convert.ToDecimal(oQCheck.Quantity);29.12
            //       //oPendingNew.AlreadyInwarded = Convert.ToDecimal(oInward.Quantity);29.12


            // //      oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oInward.Quantity);



            //    _oEntities.SaveChanges();
            //    oResult.Success = true;
            //}


            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //        JW_RM_RetMast oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JW_RM_RetMast), omValues) as JW_RM_RetMast;
                //        JW_RM_RetMast oInwardFromShelf = _oEntities.MaterialInwardMs.Where(p => p.Pk_Inward == oInward.Pk_Inward).Single();
                //        object orefItems = oInwardFromShelf;
                //        //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();



                //        var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                //        var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);

                //        Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material ).Single();

                //        oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInward.Quantity);



                //        decimal InwdFkMaterial = 0;
                //        InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);

                //        PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == InwdFkMaterial).Single();


                //        oPendingNew.AlreadyInwarded = oPendingNew.Quantity - Convert.ToDecimal(oInward.Quantity);
                //       // oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oPendingNew.AlreadyInwarded);



                //        _oEntities.DeleteObject(oInwardFromShelf);

                //         _oEntities.SaveChanges();

                //        oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oInward;

            }
            set
            {
                oInward = value as JW_RM_RetMast;
            }
        }

        public decimal ID
        {
            get
            {
                return oInward.Pk_Inward;
            }
            set
            {
                oInward = _oEntities.JW_RM_RetMast.Where(p => p.Pk_Inward == value).Single();
            }
        }

        public object GetRaw()
        {
            return oInward;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Dictionary<string, string> InwardStock()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            IEnumerable<JW_RM_RetMast> oMaterialInwardMs = _oEntities.JW_RM_RetMast;
            oMaterialInwardMs.OrderByDescending(ss => ss.Pk_Inward);
            int s = 0;
            foreach (JW_RM_RetMast oMatInw in oMaterialInwardMs)
            {
                oResult.Add(s.ToString(), oMatInw.Pk_Inward + " - " + DateTime.Parse(oMatInw.Inward_Date.ToString()).ToString("dd/MM/yyyy"));
                s++;
            }
            return oResult;
        }
    }
}



