﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class Rejection : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private CustomerRejection oRejection = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_RejectionId = null;
            string sRejectionDate = null;
            string sFk_OrderNo = null;
            string sFk_Customer = null;
            string sRejected_Quantity = null;
            string sReason = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<CustomerRejection> oRejections = null;

            try { sPk_RejectionId = p_Params.Single(p => p.ParameterName == "Pk_RejectionId").ParameterValue; }
            catch { }
            try { sRejectionDate = p_Params.Single(p => p.ParameterName == "RejectionDate").ParameterValue; }
            catch { }
            try { sFk_OrderNo = p_Params.Single(p => p.ParameterName == "Fk_OrderNo").ParameterValue; }
            catch { }
            try { sFk_Customer = p_Params.Single(p => p.ParameterName == "Fk_Customer").ParameterValue; }
            catch { }
            try { sRejected_Quantity = p_Params.Single(p => p.ParameterName == "Rejected_Quantity").ParameterValue; }
            catch { }
            try { sReason = p_Params.Single(p => p.ParameterName == "Reason").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oRejections = _oEntites.CustomerRejections;
            try
            {
                if (!string.IsNullOrEmpty(sPk_RejectionId))
                {
                    oRejections = oRejections.Where(p => p.Pk_RejectionId == decimal.Parse(sPk_RejectionId));
                }
                if (!string.IsNullOrEmpty(sRejectionDate))
                {
                    oRejections = oRejections.Where(p => (p.RejectionDate == Convert.ToDateTime(sRejectionDate)));
                }
                if (!string.IsNullOrEmpty(sFk_OrderNo))
                {
                    oRejections = oRejections.Where(p => p.Fk_OrderNo == decimal.Parse(sFk_OrderNo));
                }
                if (!string.IsNullOrEmpty(sFk_Customer))
                {
                    //oRejections = oRejections.Where(p => p.Fk_Customer == decimal.Parse(sFk_Customer));
                    oRejections = oRejections.Where(p => p.gen_Customer.CustomerName.IndexOf(sFk_Customer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
               
                if (!string.IsNullOrEmpty(sRejected_Quantity))
                {
                    oRejections = oRejections.Where(p => p.Rejected_Quantity == decimal.Parse(sRejected_Quantity));
                }
                if (!string.IsNullOrEmpty(sReason))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oRejections = oRejections.Where(p => p.Reason.ToLower() == (sReason.ToLower().ToString().Trim()));

                }
               
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oRejections.Count();
            oRejections=oRejections.OrderByDescending(p => p.Pk_RejectionId);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oRejections = oRejections.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oRejections.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchPaymentName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<CustomerRejection> oRejName = null;
            oRejName = _oEntites.CustomerRejections;

            oSearchResult.RecordCount = oRejName.Count();
            oRejName.OrderBy(p => p.Pk_RejectionId);

            List<EntityObject> oFilteredItem = oRejName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                CustomerRejection oNewRejs = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(CustomerRejection), omValues) as CustomerRejection;

                _oEntites.AddToCustomerRejections(oNewRejs);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                CustomerRejection oRejections = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(CustomerRejection), omValues) as CustomerRejection;
                CustomerRejection oRejsFromShelf = _oEntites.CustomerRejections.Where(p => p.Pk_RejectionId == oRejections.Pk_RejectionId).Single();
                object orefItems = oRejsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(CustomerRejection), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                CustomerRejection oRejections = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(CustomerRejection), omValues) as CustomerRejection;
                CustomerRejection oRejsFromShelf = _oEntites.CustomerRejections.Where(p => p.Pk_RejectionId == oRejections.Pk_RejectionId).Single();
                object orefItems = oRejsFromShelf;
                _oEntites.DeleteObject(oRejsFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oRejection;

            }
            set
            {
                oRejection = value as CustomerRejection;
            }
        }

        public decimal ID
        {
            get
            {
                return oRejection.Pk_RejectionId;
            }
            set
            {
                oRejection = _oEntites.CustomerRejections.Where(p => p.Pk_RejectionId == value).Single();
            }
        }

        public object GetRaw()
        {
            return oRejection;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




