﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class OrderOthers : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_Order ogen_Order = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName
            string sProductname = null;
            string sOnlyPending = null;
            string sPk_Order = null;
            string sFk_Enquiry = null;
            string sFromEnquiryDate = null;
            string sToEnquiryDate = null;
            string sFromOrderDate = null;
            string sOrderDate = "";
            string sToOrderDate = null;
            string sCustomer = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Order> oOrders = null;

            try { sPk_Order = p_Params.Single(p => p.ParameterName == "Pk_Order").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sFromEnquiryDate = p_Params.Single(p => p.ParameterName == "FromEnquiryDate").ParameterValue; }
            catch { }
            try { sToEnquiryDate = p_Params.Single(p => p.ParameterName == "ToEnquiryDate").ParameterValue; }
            catch { }
            try { sFromOrderDate = p_Params.Single(p => p.ParameterName == "FromOrderDate").ParameterValue; }
            catch { }
            try { sToOrderDate = p_Params.Single(p => p.ParameterName == "ToOrderDate").ParameterValue; }
            catch { }
            try { sOrderDate = p_Params.Single(p => p.ParameterName == "OrderDate").ParameterValue; }
            catch { }
            try { sProductname = p_Params.Single(p => p.ParameterName == "ProductName").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.gen_Order;

            try
            {
                if (!string.IsNullOrEmpty(sFk_Enquiry))
                {
                    oOrders = oOrders.Where(p => p.Fk_Enquiry == Convert.ToDecimal(sFk_Enquiry));
                }
                if (!string.IsNullOrEmpty(sPk_Order))
                {
                    oOrders = oOrders.Where(p => p.Pk_Order == Convert.ToDecimal(sPk_Order));
                }
                if (!string.IsNullOrEmpty(sFromEnquiryDate) && !string.IsNullOrEmpty(sToEnquiryDate))
                {
                    oOrders = oOrders.Where(p => (p.eq_Enquiry.Date >= Convert.ToDateTime(sFromEnquiryDate) && p.eq_Enquiry.Date <= Convert.ToDateTime(sToEnquiryDate)));
                }
                if (!string.IsNullOrEmpty(sFromOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate >= Convert.ToDateTime(sFromOrderDate)));
                }
                if (!string.IsNullOrEmpty(sToOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate <= Convert.ToDateTime(sToOrderDate)));
                }
                if (!string.IsNullOrEmpty(sOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate == Convert.ToDateTime(sOrderDate)));
                }
                if (!string.IsNullOrEmpty(sProductname))
                {
                    oOrders = oOrders.Where(p => p.Product.IndexOf(sProductname, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oOrders = oOrders.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sOnlyPending))
                //{

                //        oOrders = oOrders.Where(p => p.wfState.State.IndexOf(sOnlyPending, StringComparison.OrdinalIgnoreCase) >= 0);

                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Order);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchSch(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName
            string sProductname = null;
            string sOnlyPending = null;
            string sPk_Order = null;
            string sFk_Enquiry = null;
            string sFromEnquiryDate = null;
            string sToEnquiryDate = null;
            string sFromOrderDate = null;
            string sOrderDate = "";
            string sToOrderDate = null;
            string sCustomer = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_SchOrders> oOrders = null;

            try { sPk_Order = p_Params.Single(p => p.ParameterName == "Pk_Order").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sFromEnquiryDate = p_Params.Single(p => p.ParameterName == "FromEnquiryDate").ParameterValue; }
            catch { }
            try { sToEnquiryDate = p_Params.Single(p => p.ParameterName == "ToEnquiryDate").ParameterValue; }
            catch { }
            try { sFromOrderDate = p_Params.Single(p => p.ParameterName == "FromOrderDate").ParameterValue; }
            catch { }
            try { sToOrderDate = p_Params.Single(p => p.ParameterName == "ToOrderDate").ParameterValue; }
            catch { }
            try { sOrderDate = p_Params.Single(p => p.ParameterName == "OrderDate").ParameterValue; }
            catch { }
            try { sProductname = p_Params.Single(p => p.ParameterName == "ProductName").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.Vw_SchOrders;

            try
            {
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oOrders = oOrders.Where(p => p.Fk_Enquiry == Convert.ToDecimal(sFk_Enquiry));
                //}
                if (!string.IsNullOrEmpty(sPk_Order))
                {
                    oOrders = oOrders.Where(p => p.Pk_Order == Convert.ToDecimal(sPk_Order));
                }
                //if (!string.IsNullOrEmpty(sFromEnquiryDate) && !string.IsNullOrEmpty(sToEnquiryDate))
                //{
                //    oOrders = oOrders.Where(p => (p.eq_Enquiry.Date >= Convert.ToDateTime(sFromEnquiryDate) && p.eq_Enquiry.Date <= Convert.ToDateTime(sToEnquiryDate)));
                //}
                if (!string.IsNullOrEmpty(sFromOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate >= Convert.ToDateTime(sFromOrderDate)));
                }
                if (!string.IsNullOrEmpty(sToOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate <= Convert.ToDateTime(sToOrderDate)));
                }
                if (!string.IsNullOrEmpty(sOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate == Convert.ToDateTime(sOrderDate)));
                }
                //if (!string.IsNullOrEmpty(sProductname))
                //{
                //    oOrders = oOrders.Where(p => p.Product.IndexOf(sProductname, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sCustomer))
                //{
                //    oOrders = oOrders.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sOnlyPending))
                //{

                //        oOrders = oOrders.Where(p => p.wfState.State.IndexOf(sOnlyPending, StringComparison.OrdinalIgnoreCase) >= 0);

                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Order);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchUnSch(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName
            string sProductname = null;
            string sOnlyPending = null;
            string sPk_Order = null;
            string sFk_Enquiry = null;
            string sFromEnquiryDate = null;
            string sToEnquiryDate = null;
            string sFromOrderDate = null;
            string sOrderDate = "";
            string sToOrderDate = null;
            string sCustomer = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_UnSchOrders> oOrders = null;

            try { sPk_Order = p_Params.Single(p => p.ParameterName == "Pk_Order").ParameterValue; }
            catch { }
            try { sFk_Enquiry = p_Params.Single(p => p.ParameterName == "Fk_Enquiry").ParameterValue; }
            catch { }
            try { sFromEnquiryDate = p_Params.Single(p => p.ParameterName == "FromEnquiryDate").ParameterValue; }
            catch { }
            try { sToEnquiryDate = p_Params.Single(p => p.ParameterName == "ToEnquiryDate").ParameterValue; }
            catch { }
            try { sFromOrderDate = p_Params.Single(p => p.ParameterName == "FromOrderDate").ParameterValue; }
            catch { }
            try { sToOrderDate = p_Params.Single(p => p.ParameterName == "ToOrderDate").ParameterValue; }
            catch { }
            try { sOrderDate = p_Params.Single(p => p.ParameterName == "OrderDate").ParameterValue; }
            catch { }
            try { sProductname = p_Params.Single(p => p.ParameterName == "ProductName").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "CustomerName").ParameterValue; }
            catch { }
            try { sOnlyPending = p_Params.Single(p => p.ParameterName == "OnlyPending").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.Vw_UnSchOrders;

            try
            {
                //if (!string.IsNullOrEmpty(sFk_Enquiry))
                //{
                //    oOrders = oOrders.Where(p => p.Fk_Enquiry == Convert.ToDecimal(sFk_Enquiry));
                //}
                if (!string.IsNullOrEmpty(sPk_Order))
                {
                    oOrders = oOrders.Where(p => p.Pk_Order == Convert.ToDecimal(sPk_Order));
                }
                //if (!string.IsNullOrEmpty(sFromEnquiryDate) && !string.IsNullOrEmpty(sToEnquiryDate))
                //{
                //    oOrders = oOrders.Where(p => (p.eq_Enquiry.Date >= Convert.ToDateTime(sFromEnquiryDate) && p.eq_Enquiry.Date <= Convert.ToDateTime(sToEnquiryDate)));
                //}
                if (!string.IsNullOrEmpty(sFromOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate >= Convert.ToDateTime(sFromOrderDate)));
                }
                if (!string.IsNullOrEmpty(sToOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate <= Convert.ToDateTime(sToOrderDate)));
                }
                if (!string.IsNullOrEmpty(sOrderDate))
                {
                    oOrders = oOrders.Where(p => (p.OrderDate == Convert.ToDateTime(sOrderDate)));
                }
                //if (!string.IsNullOrEmpty(sProductname))
                //{
                //    oOrders = oOrders.Where(p => p.Product.IndexOf(sProductname, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sCustomer))
                //{
                //    oOrders = oOrders.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sOnlyPending))
                //{

                //        oOrders = oOrders.Where(p => p.wfState.State.IndexOf(sOnlyPending, StringComparison.OrdinalIgnoreCase) >= 0);

                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Order);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public SearchResult SearchOBox(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName

            string sPk_Order = null;
            string sFk_BoxID = null;
            string sPartId = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PaperWtBox> oOrders = null;

            //try { sPk_Order = p_Params.Single(p => p.ParameterName == "Pk_Order").ParameterValue; }
            //catch { }
            try { sFk_BoxID = p_Params.Single(p => p.ParameterName == "Fk_BoxID").ParameterValue; }
            catch { }
            try { sPartId = p_Params.Single(p => p.ParameterName == "PartId").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.Vw_PaperWtBox;

            try
            {

                //if (!string.IsNullOrEmpty(sPk_Order))
                //{
                //    oOrders = oOrders.Where(p => p.Pk_Order == Convert.ToDecimal(sPk_Order));
                //}

                if (!string.IsNullOrEmpty(sFk_BoxID))
                {
                    oOrders = oOrders.Where(p => p.Pk_BoxID == Convert.ToDecimal(sFk_BoxID));
                }

                if (!string.IsNullOrEmpty(sPartId))
                {
                    oOrders = oOrders.Where(p => p.Pk_PartPropertyID == Convert.ToDecimal(sPartId));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_BoxID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

  

       
        public SearchResult SearchOrdDetOthers(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName

            string sPk_Material = null;
            string sMatName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_OthersBill> oOrders = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMatName = p_Params.Single(p => p.ParameterName == "MatName").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.Vw_OthersBill;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oOrders = oOrders.Where(p => p.Pk_Material == Convert.ToDecimal(sPk_Material));
                }

                if (!string.IsNullOrEmpty(sMatName))
                {
                    oOrders = oOrders.Where(p => p.MaterialName.IndexOf(sMatName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
        
        
        
        public SearchResult SearchOrdItems(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName

            string sPk_Material = null;
            string sMatName = null;
            string sORd = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Others_Order> oOrders = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMatName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }

            try { sORd = p_Params.Single(p => p.ParameterName == "OrdNo").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.Vw_Others_Order;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oOrders = oOrders.Where(p => p.Pk_Material == Convert.ToDecimal(sPk_Material));
                }
                //if (!string.IsNullOrEmpty(sORd))
                //{
                //    oOrders = oOrders.Where(p => p.Pk_Order == Convert.ToDecimal(sORd));
                //}
                if (!string.IsNullOrEmpty(sMatName))
                {
                    oOrders = oOrders.Where(p => p.MaterialName.IndexOf(sMatName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }


        public SearchResult SearchOrdItemsOthers(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for OrderOthers based on 
            //First name, Last name and OrderName

            string sPk_Material = null;
            string sMatName = null;
            string sORd = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Others_OrderList> oOrders = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMatName = p_Params.Single(p => p.ParameterName == "MatName").ParameterValue; }
            catch { }

            try { sORd = p_Params.Single(p => p.ParameterName == "OrdNo").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntites.Vw_Others_OrderList;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oOrders = oOrders.Where(p => p.Pk_Material == Convert.ToDecimal(sPk_Material));
                }
                if (!string.IsNullOrEmpty(sORd))
                {
                    oOrders = oOrders.Where(p => p.Pk_Order == Convert.ToDecimal(sORd));
                }
                if (!string.IsNullOrEmpty(sMatName))
                {
                    oOrders = oOrders.Where(p => p.MatName.IndexOf(sMatName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Order oNewOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Order), omValues) as gen_Order;

                if (omValues.ContainsKey("deliverySchedules"))
                {
                    string sDeliverySchedules = omValues["deliverySchedules"].ToString();
                    object[] DeliverySchedules = JsonConvert.DeserializeObject<object[]>(sDeliverySchedules);

                    foreach (object oSchedule in DeliverySchedules)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oSchedule.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        Gen_OrderChild oDeliverySchedule = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Gen_OrderChild), oDataProperties) as Gen_OrderChild;
                        //oDeliverySchedule.Ddate = Convert.ToDateTime(oDataProperties["Ddate"]);
                        //oDeliverySchedule.Fk_Status = _oEntites.wfStates.Where(p => p.State == "New").Single().Pk_State;
                        //oNewOrder.Gen_OrderChild.Add(oDeliverySchedule);

    //                    oDeliverySchedule.Ddate = Convert.ToDateTime(oDataProperties["Ddate"].ToString(),
    //System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        oDeliverySchedule.Fk_BoxID = null;
                        oDeliverySchedule.Fk_PartID = null;
                        var PkStockID=0 ;
                        if (oDataProperties["EnqQty"].ToString().Length > 0)
                        {

                             PkStockID = Convert.ToInt32(oDataProperties["EnqQty"]);
                        }
                        else
                        {
                             PkStockID = 0;
                        }
                        if (PkStockID > 0)
                        { oDeliverySchedule.EnqQty = PkStockID; }
                        else
                        { oDeliverySchedule.EnqQty = 0; }
                        oDeliverySchedule.Fk_Status = _oEntites.wfStates.Where(p => p.State == "New").Single().Pk_State;
                        oNewOrder.Gen_OrderChild.Add(oDeliverySchedule);

                    }
                }
                if (omValues.ContainsKey("fk_Enquiry"))
                {
                    if (omValues["fk_Enquiry"].ToString() == "undefined")
                    {
                        oNewOrder.Fk_Enquiry = 11;
                    }
                    else
                    {
                        oNewOrder.Fk_Enquiry = Convert.ToDecimal(omValues["fk_Enquiry"]);
                    }

                }
                else
                {
                    oNewOrder.Fk_Enquiry = 11;
                }
                if (omValues["Product"].ToString() == "2")
                {
                    oNewOrder.Product = "Job Work";
                }
                else if (omValues["Product"].ToString() == "1")
                {
                    oNewOrder.Product = "Sales";
                }
                else
                {
                    oNewOrder.Product = "";
                }
                oNewOrder.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                oNewOrder.Fk_Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                //oNewOrder.Fk_Status = _oEntites.wfStates.Where(p => p.State == "New").Single().Pk_State;
                oNewOrder.OrderDate = Convert.ToDateTime(omValues["OrderDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                _oEntites.AddTogen_Order(oNewOrder);
                _oEntites.SaveChanges();


                //_oEntites.SaveChanges();
                oResult.Success = true;
                oResult.Message = oNewOrder.Pk_Order.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Order oOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Order), omValues) as gen_Order;
                gen_Order oOrderFromShelf = _oEntites.gen_Order.Where(p => p.Pk_Order == oOrder.Pk_Order).Single();
                object orefOrder = oOrderFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Order), omValues, ref orefOrder);

                List<decimal> oUpdateList = new List<decimal>();

                if (omValues.ContainsKey("deliverySchedules"))
                {
                    string sDeliverySchedules = omValues["deliverySchedules"].ToString();
                    object[] DeliverySchedules = JsonConvert.DeserializeObject<object[]>(sDeliverySchedules);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<Gen_OrderChild> NewDeliveryList = new List<Gen_OrderChild>();
                    foreach (object oSchedule in DeliverySchedules)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oSchedule.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());


                        //    string[] a = oDataProperties["Ddate"].ToString().Split('/');
                        //   var ss = DateTime.ParseExact(a[1] + '/' + a[0] + '/' + a[2] + " 00:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                        //   var dd = Convert.ToDateTime(a[1] + '/' + a[0] + '/' + a[2]);


                        //int  sYear = int.Parse(a[2]);
                        //int sMonth = int.Parse(a[1]);
                        //int sDay = int.Parse(a[0]);
                        //DateTime ss1 = DateTime.ParseExact(sMonth + '/' + sDay + '/' + sYear + " 00:00", "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                        //   //DateTime dd1 = int.Parse(sDay + "/" + sMonth + "/" + sYear);
                        //oDataProperties["Ddate"] = ss1;
                        //int dmonth = Convert.ToInt32(a[1]);
                        //int dyear = Convert.ToInt32(a[2]);



                        //var dd = DateTime.Parse(oDataProperties["Ddate"].ToString());
                        //oDataProperties["Ddate"] =Convert.ToDateTime(dd);
                        //DateTime.Parse(oDataProperties["Ddate"].ToString()).ToString("MM/dd/yyyy");
                        if (oDataProperties.ContainsKey("Pk_OrderChild") && oDataProperties["Pk_OrderChild"] != null && oDataProperties["Pk_OrderChild"].ToString() != "")
                        {
                            decimal pkDeliverySchedule = decimal.Parse(oDataProperties["Pk_OrderChild"].ToString());

                            Gen_OrderChild oScheduleFromShelf = _oEntites.Gen_OrderChild.Where(p => p.Pk_OrderChild == pkDeliverySchedule).Single();

                            object orefSchedule = oScheduleFromShelf;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Gen_OrderChild), oDataProperties, ref orefSchedule);
                            UpdatedPk.Add(pkDeliverySchedule);
                        }
                        else
                        {
                            Gen_OrderChild oDeliverySchedule = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Gen_OrderChild), oDataProperties) as Gen_OrderChild;
                            //oDeliverySchedule.DeliveredDate = null;
                            NewDeliveryList.Add(oDeliverySchedule);
                        }
                    }

                    List<Gen_OrderChild> oDeletedRecords = oOrderFromShelf.Gen_OrderChild.Where(p => !UpdatedPk.Contains(p.Pk_OrderChild)).ToList();

                    foreach (Gen_OrderChild oDeletedDetail in oDeletedRecords)
                    {
                        oOrderFromShelf.Gen_OrderChild.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (Gen_OrderChild oNewDeliveryDetail in NewDeliveryList)
                    {
                        oOrderFromShelf.Gen_OrderChild.Add(oNewDeliveryDetail);
                    }
                }
                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }


        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Order oOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Order), omValues) as gen_Order;
                gen_Order oOrderFromShelf = _oEntites.gen_Order.Where(p => p.Pk_Order == oOrder.Pk_Order).Single();
                _oEntites.DeleteObject(oOrderFromShelf);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return ogen_Order;
            }
            set
            {
                ogen_Order = value as gen_Order;
            }
        }

        public decimal ID
        {
            get
            {
                return ogen_Order.Pk_Order;
            }
            set
            {
                ogen_Order = _oEntites.gen_Order.Where(p => p.Pk_Order == value).Single();
            }
        }

        public object GetRaw()
        {
            return ogen_Order;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }




        public Dictionary<int, double> GetMonthWiseOrderCount()
        {

            Dictionary<int, double> oResult = new Dictionary<int, double>();

            for (int i = 1; i <= 12; i++)
            {
                int Count = _oEntites.gen_Order.Where(p => p.OrderDate.Month == i).Count();

                oResult.Add(i, Count);
            }

            return oResult;
        }

        //public Dictionary<string, int> GetOrderSatusCount()
        //{

        //    Dictionary<string, int> oResult = new Dictionary<string, int>();

        //    IEnumerable<wfState> oStates = _oEntites.wfStates;

        //    foreach (wfState oState in oStates)
        //    {
        //        int Count = _oEntites.gen_Order.Where(p => p.wfState.State == oState.State).Count();

        //        if (Count > 0)
        //        {
        //            oResult.Add(oState.State, Count);
        //        }
        //    }

        //    return oResult;
        //}

        public Dictionary<string, int> GetDeliveryScheduleStatus()
        {

            Dictionary<string, int> oResult = new Dictionary<string, int>();

            int MissedCount = _oEntites.gen_DeliverySchedule.Where(p => (p.DeliveryDate <= DateTime.Today && (p.DeliveryCompleted == null || p.DeliveryCompleted == false))).Count();

            int CompletedCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryCompleted == true).Count();

            int PendingCount = _oEntites.gen_DeliverySchedule.Where(p => (p.DeliveryDate > DateTime.Today && (p.DeliveryCompleted == null || p.DeliveryCompleted == false))).Count();

            oResult.Add("Pending", PendingCount);
            oResult.Add("Completed", CompletedCount);
            oResult.Add("Missed", MissedCount);

            return oResult;
        }

        public double OnTimeDeliveryPercentage()
        {

            int TotalCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryDate < DateTime.Today).Count();

            int CompletedCount = _oEntites.gen_DeliverySchedule.Where(p => p.DeliveryCompleted == true && (p.DeliveryDate >= p.DeliveredDate)).Count();

            double oResult = TotalCount != 0 ? ((CompletedCount * 100) / TotalCount) : 0;

            return oResult;
        }

        public Dictionary<string, string> OrderDeliveries()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            //int Count = 0;
            //IEnumerable<Inv_Material> oMaterials = _oEntites.Inv_Material;
            DateTime todate = Convert.ToDateTime(DateTime.Today.AddDays(1));
            int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            IEnumerable<vw_OrderDeliveryDetails> oOrder = _oEntites.vw_OrderDeliveryDetails.Where(p => p.DeliveryDate == todate && p.Fk_Tanent == tannent);
            //  IEnumerable<gen_Vendor> ovendor = _oEntites.gen_Vendor;
            int ss = 0;
            foreach (vw_OrderDeliveryDetails oOrd in oOrder)
            {
                oResult.Add(ss.ToString(), oOrd.CustomerName + "-" + oOrd.Quantity);
                ss++;
            }
            return oResult;
        }
    }
}
