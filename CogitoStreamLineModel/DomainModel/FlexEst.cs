﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore;
using Newtonsoft.Json;
using CogitoStreamLineModel;

namespace ManufacturingModel.DomainModel
{
    public class FlexEst : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private est_Estimation oEstimation = new est_Estimation();
        public Decimal fkbox = 0;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName
            decimal? sEstimate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<est_Estimation> oEstimates = null;

            SearchResult oSearchResult = new SearchResult();

            try { sEstimate = Convert.ToDecimal(p_Params.Single(p => p.ParameterName == "Estimate").ParameterValue); }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oEstimates = _oEntites.est_Estimation;

            try
            {
                if (sEstimate != null)
                {
                    oEstimates = oEstimates.Where(p => p.Pk_Estimate == sEstimate);
                }
               
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEstimates.Count();
            oEstimates = oEstimates.OrderByDescending(p => p.Pk_Estimate);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEstimates = oEstimates.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oEstimates.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }

        public bool SetEnquiryId(decimal p_dEnquiry)
        {
            if (_oEntites.est_Estimation.Where(p => p.Fk_Enquiry == p_dEnquiry).Count() > 0)
            {
                oEstimation = _oEntites.est_Estimation.Where(p => p.Fk_Enquiry == p_dEnquiry).First();
                return true;
            }

            return false;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                est_Estimation oEstimation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_Estimation), omValues) as est_Estimation;
                //BoxSpec oBoxSp = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxSpec), omValues) as BoxSpec;

                string sParts = omValues["parts"].ToString();
                object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                foreach (object oItem in Parts)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    est_EstimationPart oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_EstimationPart), oProperties) as est_EstimationPart;

                    string sLayers = oProperties["layers"].ToString();
                    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                    foreach (object oLayerItem in Layers)
                    {
                        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                        est_Layer oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_Layer), oLayerProperties) as est_Layer;

                        oPart.est_Layer.Add(oLayer);
                    }

                    oEstimation.est_EstimationPart.Add(oPart);
                   
                }
                
                /////////////////////////////////////box master

                //BoxMaster oNewBox = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxMaster), omValues) as BoxMaster;


                //oNewBox.Name = "Checking";
                //oNewBox.Fk_BoxType = 1;
          
                    

                ////_oEntites.BoxMaster(oNewBox);

                //_oEntites.SaveChanges();

                //oResult.Success = true;

                //fkbox = oNewBox.Pk_BoxID;
                //////////////////////////////box specs


                //foreach (object oItem in Parts)
                //{
                //    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                //    ItemPartProperty oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ItemPartProperty), oProperties) as ItemPartProperty;

                //    string sLayers = oProperties["layers"].ToString();
                //    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                //    foreach (object oLayerItem in Layers)
                //    {
                //        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                //        Items_Layers oLayer = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Items_Layers), oLayerProperties) as Items_Layers;

                //        oPart.Items_Layers.Add(oLayer);
                //    }

                //    oBoxSp.ItemPartProperty.Add(oPart);

                //}

                //oBoxSp.Fk_BoxID = fkbox;

                _oEntites.AddToest_Estimation(oEstimation);
                _oEntites.SaveChanges();
                oResult.Message = oEstimation.Pk_Estimate.ToString();
                oResult.Success = true;

                //_oEntites.AddToBoxSpecs(oBoxSp);
                //_oEntites.SaveChanges();
                //oResult.Message = oBoxSp.Pk_BoxSpecID.ToString();
                //oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                est_Estimation oEstimation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(est_Estimation), omValues) as est_Estimation;
                decimal estid = decimal.Parse(omValues["id"].ToString());
                est_Estimation oEstimateFromShelf = _oEntites.est_Estimation.Where(p => p.Pk_Estimate == estid).Single();
                
                object orefEstimate = oEstimateFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_Estimation), omValues, ref orefEstimate);

                string sParts = omValues["parts"].ToString();
                object[] Parts = JsonConvert.DeserializeObject<object[]>(sParts);

                foreach (object oItem in Parts)
                {
                    Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    decimal id= decimal.Parse(oProperties["id"].ToString());
                    est_EstimationPart oEstimatePartFromShelf = _oEntites.est_EstimationPart.Where(p => p.Pk_DetailedEstimationPart == id).Single();

                    object orefEstimatePart = oEstimatePartFromShelf;
                    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_EstimationPart), oProperties, ref orefEstimatePart);

                    string sLayers = oProperties["layers"].ToString();
                    object[] Layers = JsonConvert.DeserializeObject<object[]>(sLayers);

                    foreach (object oLayerItem in Layers)
                    {

                        Dictionary<string, object> oLayerProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oLayerItem.ToString());
                        decimal layertid = decimal.Parse(oLayerProperties["id"].ToString());
                        est_Layer oEstimateLayerFromShelf = _oEntites.est_Layer.Where(p => p.Pk_Layer == layertid).Single();

                        object orefEstimateLayer = oEstimateLayerFromShelf;
                        WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(est_Layer), oLayerProperties, ref orefEstimateLayer);
                        
                    }
                   

                }

                _oEntites.SaveChanges();
                oResult.Message = oEstimation.Pk_Estimate.ToString();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            throw new NotImplementedException();
        }

        public EntityObject DAO
        {
            get
            {
                return oEstimation;
            }
            set
            {
                oEstimation = value as est_Estimation;
            }
        }

        public decimal ID
        {
            get
            {
                return oEstimation.Pk_Estimate;
            }
            set
            {
                oEstimation = _oEntites.est_Estimation.Where(p => p.Pk_Estimate == value).Single();
            }
        }

        public object GetRaw()
        {
            return oEstimation;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public EntityObject AddDetail(string sBasketName, EntityObject oDetail)
        {
            throw new NotImplementedException();
        }

        public EntityObject RemoveDetail(string sBasketName, decimal dPk)
        {
            throw new NotImplementedException();
        }

        public EntityObject UpdateDetail(string sBasketName, decimal dPk, Dictionary<string, object> oValues)
        {
            throw new NotImplementedException();
        }

        public List<EntityObject> GetDetails(string sBasketName)
        {
            throw new NotImplementedException();
        }
    }
}
