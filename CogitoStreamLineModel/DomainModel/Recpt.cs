﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class Recpt : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private Receipt oReceipt = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_ReceiptID = null;
            string sFk_Customer = null;
            string sAmount = null;
            string sChequeNo = null;
            string sChequeDate = null;
            string sDescription = null;
            string sBankName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Receipt> oReceipts = null;

            try { sPk_ReceiptID = p_Params.Single(p => p.ParameterName == "Pk_ReceiptID").ParameterValue; }
            catch { }
            try { sFk_Customer = p_Params.Single(p => p.ParameterName == "Fk_Customer").ParameterValue; }
            catch { }
            try { sAmount = p_Params.Single(p => p.ParameterName == "Amount").ParameterValue; }
            catch { }
            try { sChequeNo = p_Params.Single(p => p.ParameterName == "ChequeNo").ParameterValue; }
            catch { }
            try { sChequeDate = p_Params.Single(p => p.ParameterName == "ChequeDate").ParameterValue; }
            catch { }
            try { sDescription = p_Params.Single(p => p.ParameterName == "Description").ParameterValue; }
            catch { }
            try { sBankName = p_Params.Single(p => p.ParameterName == "BankName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oReceipts = _oEntites.Receipt;
            try
            {
                if (!string.IsNullOrEmpty(sPk_ReceiptID))
                {
                    oReceipts = oReceipts.Where(p => p.Pk_ReceiptID == decimal.Parse(sPk_ReceiptID));
                }

                if (!string.IsNullOrEmpty(sFk_Customer))
                {
                    oReceipts = oReceipts.Where(p => p.Fk_Customer != null && p.gen_Customer.CustomerName.IndexOf(sFk_Customer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sAmount))
                {
                    oReceipts = oReceipts.Where(p => p.Amount == decimal.Parse(sAmount));
                }


                if (!string.IsNullOrEmpty(sChequeNo))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oReceipts = oReceipts.Where(p => p.ChequeNo.ToLower() == (sChequeNo.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sChequeDate))
                {
                    oReceipts = oReceipts.Where(p => (p.ChequeDate == Convert.ToDateTime(sChequeDate)));
                }
                if (!string.IsNullOrEmpty(sDescription))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oReceipts = oReceipts.Where(p => p.Description.ToLower() == (sDescription.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sBankName))
                {
                    //oCitys = oCitys.Where(p => p.CityName.IndexOf(sCityName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oReceipts = oReceipts.Where(p => p.BankName.ToLower() == (sBankName.ToLower().ToString().Trim()));

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oReceipts.Count();
            oReceipts.OrderBy(p => p.Pk_ReceiptID);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oReceipts = oReceipts.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oReceipts.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchReceiptName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Receipt> oReceiptName = null;
            oReceiptName = _oEntites.Receipt;

            oSearchResult.RecordCount = oReceiptName.Count();
            oReceiptName.OrderBy(p => p.Pk_ReceiptID);

            List<EntityObject> oFilteredItem = oReceiptName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Receipt oNewReceipts = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Receipt), omValues) as Receipt;

                _oEntites.AddToReceipt(oNewReceipts);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Receipt oReceipts = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Receipt), omValues) as Receipt;
                Receipt oReceiptsFromShelf = _oEntites.Receipt.Where(p => p.Pk_ReceiptID == oReceipts.Pk_ReceiptID).Single();
                object orefItems = oReceiptsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Receipt), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Receipt oReceipts = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Receipt), omValues) as Receipt;
                Receipt oReceiptsFromShelf = _oEntites.Receipt.Where(p => p.Pk_ReceiptID == oReceipts.Pk_ReceiptID).Single();
                object orefItems = oReceiptsFromShelf;
                _oEntites.DeleteObject(oReceiptsFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oReceipt;

            }
            set
            {
                oReceipt = value as Receipt;
            }
        }

        public decimal ID
        {
            get
            {
                return oReceipt.Pk_ReceiptID;
            }
            set
            {
                oReceipt = _oEntites.Receipt.Where(p => p.Pk_ReceiptID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oReceipt;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




