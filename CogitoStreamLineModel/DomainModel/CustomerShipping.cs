﻿//Name          : CustomerShipping Class
//Description   : Contains the CustomerShipping class definition, every page in the application will be instance of this class with following
//                 1. Shipping 2. Data binding 3. Loading preferences
//Author        : shantha
//Date 	        : 06/05/2016



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;


namespace CogitoStreamLineModel.DomainModel
{
    public class CustomerShipping : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_CustomerShippingDetails oCustShip = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_CustomerShippingId = null;
            string sShippingCustomerName  = null;
            string sShippingCountry = null;
            string sShippingState = null;
            string sShippingCity = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_CustomerShippingDetails> oCustShips = null;

            try { sPk_CustomerShippingId = p_Params.Single(p => p.ParameterName == "Pk_CustomerShippingId").ParameterValue; }
            catch { }
            try { sShippingCustomerName = p_Params.Single(p => p.ParameterName == "ShippingCustomerName").ParameterValue; }
            catch { }
            try { sShippingCountry = p_Params.Single(p => p.ParameterName == "ShippingCountry").ParameterValue; }
            catch { }
            try { sShippingCity = p_Params.Single(p => p.ParameterName == "ShippingCity").ParameterValue; }
            catch { }
            try { sShippingState = p_Params.Single(p => p.ParameterName == "ShippingState").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oCustShips = _oEntites.gen_CustomerShippingDetails;
            try
            {
                if (!string.IsNullOrEmpty(sPk_CustomerShippingId))
                {
                    oCustShips = oCustShips.Where(p => p.Pk_CustomerShippingId == decimal.Parse(sPk_CustomerShippingId));
                }
                if (!string.IsNullOrEmpty(sShippingCustomerName))
                {
                    oCustShips = oCustShips.Where(p => p.ShippingCustomerName.ToLower() == (sShippingCustomerName.ToLower().ToString().Trim()));
                }
                if (!string.IsNullOrEmpty(sShippingCountry))
                {
                    oCustShips = oCustShips.Where(p => p.ShippingCountry.ToLower() == (sShippingCountry.ToLower().ToString().Trim()));
                }
                if (!string.IsNullOrEmpty(sShippingCity))
                {
                    oCustShips = oCustShips.Where(p => p.ShippingCity.ToLower() == (sShippingCity.ToLower().ToString().Trim()));
                }
                if (!string.IsNullOrEmpty(sShippingState))
                {
                    oCustShips = oCustShips.Where(p => p.ShippingState.ToLower() == (sShippingState.ToLower().ToString().Trim()));
                }
               
               
            }
            catch (System.NullReferenceException)
            {

            }

            oSearchResult.RecordCount = oCustShips.Count();

            oCustShips.OrderBy(p => p.Pk_CustomerShippingId);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oCustShips = oCustShips.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oCustShips.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        //public SearchResult SearchBranchName(List<SearchParameter> p_Params)
        //{

        //    SearchResult oSearchResult = new SearchResult();
        //    IEnumerable<Branch> oBranchName = null;
        //    oBranchName = _oEntites.Branches;

        //    oSearchResult.RecordCount = oBranchName.Count();
        //    oBranchName.OrderBy(p => p.Pk_BranchID);

        //    List<EntityObject> oFilteredItem = oBranchName.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItem;

        //    return oSearchResult;
        //}

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Branch oNewBranches = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Branch), omValues) as Branch;
                oNewBranches.Fk_Tanent = Convert.ToInt32(System.Web.HttpContext.Current.Session["Tanent"]);


                _oEntites.AddToBranch(oNewBranches);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Branch oBranches = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Branch), omValues) as Branch;
                Branch oBranchesFromShelf = _oEntites.Branch.Where(p => p.Pk_BranchID == oBranches.Pk_BranchID).Single();
                object orefItems = oBranchesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Branch), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Branch oBranches = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Branch), omValues) as Branch;
                Branch oBranchesFromShelf = _oEntites.Branch.Where(p => p.Pk_BranchID == oBranches.Pk_BranchID).Single();
                object orefItems = oBranchesFromShelf;
                //_oEntites.DeleteObject(oBranchesFromShelf);

                _oEntites.DeleteObject(oBranchesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oCustShip;

            }
            set
            {
                oCustShip = value as gen_CustomerShippingDetails;
            }
        }

        public decimal ID
        {
            get
            {
                return oCustShip.Pk_CustomerShippingId;
            }
            set
            {
                oCustShip = _oEntites.gen_CustomerShippingDetails.Where(p => p.Pk_CustomerShippingId == value).Single();
            }
        }

        public object GetRaw()
        {
            return oCustShip;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


