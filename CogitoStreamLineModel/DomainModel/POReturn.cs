﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class POReturn
//    {
//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
    public class POReturn : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private POReturnMaster oPOrder = null;
        #endregion Variables

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            string sPk_PoRetID = null;
            //string sPoRetDate = null;
            string sFromDate = null;
            string sToDate = null;
            string sPONO = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<POReturnMaster> oPOrd = null;

            try { sPk_PoRetID = p_Params.Single(p => p.ParameterName == "Pk_PoRetID").ParameterValue; }
            catch { }
            try { sPONO = p_Params.Single(p => p.ParameterName == "Fk_PoNo").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "TxtFromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "TxtToDate").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oPOrd = _oEntities.POReturnMaster;

            try
            {
                if (!string.IsNullOrEmpty(sPk_PoRetID))
                {
                    oPOrd = oPOrd.Where(p => p.Pk_PoRetID == decimal.Parse(sPk_PoRetID));
                }
                if (!string.IsNullOrEmpty(sPONO))
                {
                    oPOrd = oPOrd.Where(p => p.Fk_PoNo == decimal.Parse(sPONO));
                }
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oPOrd = oPOrd.Where(p => (p.PoRetDate >= Convert.ToDateTime(sFromDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    oPOrd = oPOrd.Where(p => (p.PoRetDate <= Convert.ToDateTime(sToDate)));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oPOrd.Count();
            oPOrd = oPOrd.OrderByDescending(p => p.Pk_PoRetID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oPOrd = oPOrd.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oPOrd.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchIssueDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_MaterialID = null;
            string sPk_MaterialName = null;
            //    string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<vw_StockMaterial> oIssueIssueMasters = null;

            try { sPk_MaterialID = p_Params.Single(p => p.ParameterName == "Pk_MaterialID").ParameterValue; }
            catch { }
            try { sPk_MaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            //try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueIssueMasters = _oEntities.vw_StockMaterial;

            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialID))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialID));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialName))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Name.IndexOf(sPk_MaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sFk_Material))
                //{
                //    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueIssueMasters.Count();
            oIssueIssueMasters = oIssueIssueMasters.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueIssueMasters = oIssueIssueMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueIssueMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchIndentDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sIndentNo = null;
            string sPk_MaterialID = null;
            string sPk_MaterialName = null;
            //    string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_MIndent> oIssueIssueMasters = null;

            try { sIndentNo = p_Params.Single(p => p.ParameterName == "IndentNo").ParameterValue; }
            catch { }
            try { sPk_MaterialID = p_Params.Single(p => p.ParameterName == "Pk_MaterialID").ParameterValue; }
            catch { }
            try { sPk_MaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            //try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueIssueMasters = _oEntities.Vw_MIndent;

            try
            {
                if (!string.IsNullOrEmpty(sIndentNo))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_MaterialOrderMasterId == decimal.Parse(sIndentNo));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialID))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialID));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialName))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Name.IndexOf(sPk_MaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                //if (!string.IsNullOrEmpty(sFk_Material))
                //{
                //    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueIssueMasters.Count();
            oIssueIssueMasters = oIssueIssueMasters.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueIssueMasters = oIssueIssueMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueIssueMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                POReturnMaster oNewPOrder = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnMaster), omValues) as POReturnMaster;

                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        POReturnDetails oNewPOrderD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnDetails), oDataProperties) as POReturnDetails;


                        oNewPOrderD.Amount = (oNewPOrderD.Quantity * oNewPOrderD.Rate);

                        oNewPOrder.POReturnDetails.Add(oNewPOrderD);


                    }


                }
                //oNewPOrder.Fk_Status = 1;

                _oEntities.AddToPOReturnMaster(oNewPOrder);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            decimal gtot = 0;
            decimal edval = 0;
            //decimal taxval = 0;


            try
            {

                POReturnMaster oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnMaster), omValues) as POReturnMaster;
                POReturnMaster OPOrdFromShelf = _oEntities.POReturnMaster.Where(p => p.Pk_PoRetID == oPOrd.Pk_PoRetID).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(POReturnMaster), omValues, ref orefPO);


                string sMaterials = omValues["Materials"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<POReturnDetails> NewMaterialsList = new List<POReturnDetails>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_PODet"] == null || oDataProperties2["Pk_PODet"].ToString() == "")
                    {
                        POReturnDetails oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnDetails), oDataProperties2) as POReturnDetails;

                        NewMaterialsList.Add(oNewPOrderD);


                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_PoRetDetID"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);

                        //gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oDataProperties2["Amount"]);


                        POReturnDetails oPOrderDetFromShelf = _oEntities.POReturnDetails.Where(p => p.Pk_PoRetDetID == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                        object oPOrderDet
                                            = _oEntities.POReturnDetails.Where(p => p.Pk_PoRetDetID == dPkPOrdDet).First();

                        _oEntities.DeleteObject(oPOrderDet);


                        POReturnDetails oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnDetails), oDataProperties2) as POReturnDetails;

                        NewMaterialsList.Add(oIndentDetails);



                        UpdatedPk.Add(dPkPOrdDet);

                    }
                }


                // Handeling Deleted Records

                List<POReturnDetails> oDeletedRecords = OPOrdFromShelf.POReturnDetails.Where(p => !UpdatedPk.Contains(p.Pk_PoRetDetID)).ToList();



                //Add new elements
                foreach (POReturnDetails oNewMaterialDetail in NewMaterialsList)
                {

                    gtot = Convert.ToDecimal(gtot) + Convert.ToDecimal(oNewMaterialDetail.Amount);
                    OPOrdFromShelf.POReturnDetails.Add(oNewMaterialDetail);
                }

                //object orefPurchaseM = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(POReturnMasters), omValues, ref orefPurchaseM);
                //EDVAL = Convert.ToDecimal(GTotal) + Convert.ToDecimal(GTotal) * Convert.ToDecimal(t1);
                //OPOrdFromShelf.GrandTotal = gtot;

                //oPOrd.GrandTotal = Convert.ToDecimal( gtot);
                edval = Convert.ToDecimal(gtot) + Convert.ToDecimal(gtot) * Convert.ToDecimal(0.06);
                //OPOrdFromShelf.ED = edval;

                //if (OPOrdFromShelf.TaxType == "VAT")
                //{
                //    taxval = Convert.ToDecimal(edval) + Convert.ToDecimal(edval) * Convert.ToDecimal(0.055);
                //}

                //else if (OPOrdFromShelf.TaxType == "CST")
                //{

                //    taxval = Convert.ToDecimal(edval) + Convert.ToDecimal(edval) * Convert.ToDecimal(0.02);
                //}

                //OPOrdFromShelf.NetValue = Convert.ToDecimal(taxval);


                POReturnMaster oNewPO = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnMaster), omValues) as POReturnMaster;

                //var ss = OPOrdFromShelf.Fk_Status.ToString();
                //var newss = oPOrd.Fk_Status.ToString();
                //string ss1 = omValues["Fk_Status"].ToString();
                //if (ss1 == "True")
                //{
                //    OPOrdFromShelf.Fk_Status = 4;
                //    //omValues["Fk_Status"] = 4;//Closed status
                //}

                //else
                //    OPOrdFromShelf.Fk_Status = 1;//New Status

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();


            try
            {
                POReturnMaster oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(POReturnMaster), omValues) as POReturnMaster;
                POReturnMaster OInvIndentMasterFromShelf = _oEntities.POReturnMaster.Where(p => p.Pk_PoRetID == oPOrd.Pk_PoRetID).Single();
                //  var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                if (omValues.ContainsKey("MaterialData"))
                {
                    string sMaterials = omValues["MaterialData"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        decimal FkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);

                        //POReturnDetails oIssueReturnDetailsFromShelf = _oEntities.POReturnDetails.Where(p => p.Fk_IssueReturnMasterId == OInvIndentMasterFromShelf.Pk_IssueReturnMasterId && p.Fk_Material == FkMaterial).Single();
                        //int sFkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);

                        //Stock oInv_Stock = _oEntities.Stocks.Where(p => p.Fk_Material == sFkMaterial && p.Fk_Tanent == OInvIndentMasterFromShelf.MaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();

                        //if ((oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity) >= 0)
                        //{
                        //    MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_IssueID == oPOrd.Fk_IssueId).Single();
                        //    oIssuedetails.ReturnQuantity = (oIssuedetails.ReturnQuantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                        //    oInv_Stock.Quantity = Convert.ToDecimal(oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity);


                        //    _oEntities.DeleteObject(oIssueReturnDetailsFromShelf);

                        //    _oEntities.SaveChanges();
                        //}
                    }
                }
                int oIssueReturnDet = _oEntities.POReturnDetails.Where(p => p.Fk_PoRetID == oPOrd.Pk_PoRetID).Count();
                if (oIssueReturnDet == 0)
                {
                    OInvIndentMasterFromShelf = _oEntities.POReturnMaster.Where(p => p.Pk_PoRetID == oPOrd.Pk_PoRetID).Single();
                    _oEntities.DeleteObject(OInvIndentMasterFromShelf);
                    _oEntities.SaveChanges();
                }
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }


            return oResult;
        }
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oPOrder;
            }
            set
            {
                oPOrder = value as POReturnMaster;
            }
        }
        public decimal ID
        {
            get
            {
                return oPOrder.Pk_PoRetID;
            }
            set
            {
                oPOrder = _oEntities.POReturnMaster.Where(p => p.Pk_PoRetID == value).Single();
            }
        }
        public object GetRaw()
        {
            return oPOrder;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion Methods

    }
}
