﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class IssueForSamples
//    {
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class IssueForSamples : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private SampMaterialIssue oMaterialIssue = null;

        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_MaterialIssueID = null;
            //string sMaterialIndentDate = null;
            string sIssueDate = null;
            string sFk_UserID = null;
            // string sFk_Tanent = null;
            string sFk_OrderNo = null;
            string sDCNo = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<SampMaterialIssue> oMaterialIssues = null;

            try { sPk_MaterialIssueID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIssueID").ParameterValue; }
            catch { }
            try { sIssueDate = p_Params.Single(p => p.ParameterName == "IssueDate").ParameterValue; }
            catch { }
            try { sFk_UserID = p_Params.Single(p => p.ParameterName == "Fk_UserID").ParameterValue; }
            catch { }
            try { sFk_OrderNo = p_Params.Single(p => p.ParameterName == "Fk_OrderNo").ParameterValue; }
            catch { }
            try { sDCNo = p_Params.Single(p => p.ParameterName == "DCNo").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterialIssues = _oEntities.SampMaterialIssue;
            // DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialIssueID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_MaterialIssueID == decimal.Parse(sPk_MaterialIssueID));
                }
                if (!string.IsNullOrEmpty(sIssueDate))
                {

                    DateTime SFrom = Convert.ToDateTime(sIssueDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oMaterialIssues = oMaterialIssues.Where(p => (p.IssueDate >= Convert.ToDateTime(SFrom)));

                    //oMaterialIssues = oMaterialIssues.Where(p => (p.IssueDate == Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sFk_UserID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_UserID == decimal.Parse(sFk_UserID));
                }
                //if (!string.IsNullOrEmpty(sFk_OrderNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_OrderNo == decimal.Parse(sFk_OrderNo));
                //}
                if (!string.IsNullOrEmpty(sDCNo))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.DCNo.IndexOf(sDCNo, StringComparison.OrdinalIgnoreCase) >= 0);

                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oMaterialIssues.Count();
            oMaterialIssues = oMaterialIssues.OrderByDescending(p => p.Pk_MaterialIssueID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMaterialIssues = oMaterialIssues.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMaterialIssues.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }




        public SearchResult SearchIssueDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_MaterialIssueID = null;
            //string sMaterialIndentDate = null;
            string sIssueDate = null;
            string sFk_UserID = null;
            string sPk_MatIssueID = null;
            string sPk_JobCardID = null;
            string sFk_OrderNo = null;
            string sDCNo = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Samp_MatIssue> oMaterialIssues = null;

            try { sPk_MatIssueID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIssueID").ParameterValue; }
            catch { }
            try { sPk_MaterialIssueID = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }
            try { sPk_JobCardID = p_Params.Single(p => p.ParameterName == "Pk_JobCardID").ParameterValue; }
            catch { }

            try { sIssueDate = p_Params.Single(p => p.ParameterName == "DateValue").ParameterValue; }
            catch { }
            try { sFk_UserID = p_Params.Single(p => p.ParameterName == "Fk_UserID").ParameterValue; }
            catch { }
            try { sFk_OrderNo = p_Params.Single(p => p.ParameterName == "Fk_OrderNo").ParameterValue; }
            catch { }
            try { sDCNo = p_Params.Single(p => p.ParameterName == "DCNo").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterialIssues = _oEntities.Vw_Samp_MatIssue;
            // DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialIssueID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialIssueID));
                }
                if (!string.IsNullOrEmpty(sPk_JobCardID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_JobCardID == decimal.Parse(sPk_JobCardID));
                }
                if (!string.IsNullOrEmpty(sPk_MatIssueID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_MaterialIssueID == decimal.Parse(sPk_MatIssueID));
                }
                if (!string.IsNullOrEmpty(sIssueDate))
                {

                    oMaterialIssues = oMaterialIssues.Where(p => (p.IssueDate >= Convert.ToDateTime(sIssueDate)));
                }
                //if (!string.IsNullOrEmpty(sFk_UserID))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_UserID == decimal.Parse(sFk_UserID));
                //}
                //if (!string.IsNullOrEmpty(sFk_OrderNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_OrderNo == decimal.Parse(sFk_OrderNo));
                //}
                //if (!string.IsNullOrEmpty(sDCNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.DCNo.IndexOf(sDCNo, StringComparison.OrdinalIgnoreCase) >= 0);

                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oMaterialIssues.Count();
            oMaterialIssues = oMaterialIssues.OrderByDescending(p => p.Pk_MaterialIssueID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oMaterialIssues = oMaterialIssues.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            //if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            //{

            //    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
            //    var skip = int.Parse(sPageSize) * (page - 1);

            //    oMaterialIssues = oMaterialIssues.Select(p => p)
            //                        .Skip(skip)
            //                        .Take(int.Parse(sPageSize));
            //}

            List<EntityObject> oFilteredItems = oMaterialIssues.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public SearchResult SearchAssDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_MaterialIssueID = null;
            //string sMaterialIndentDate = null;
            string sIssueDate = null;
            string sFk_UserID = null;
            string sPk_MatIssueID = null;
            string sPk_JobCardID = null;
            string sFk_OrderNo = null;
            string sDCNo = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_AssignedStock> oMaterialIssues = null;

            try { sPk_MatIssueID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIssueID").ParameterValue; }
            catch { }
            try { sPk_MaterialIssueID = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }
            try { sPk_JobCardID = p_Params.Single(p => p.ParameterName == "Pk_JobCardID").ParameterValue; }
            catch { }

            try { sIssueDate = p_Params.Single(p => p.ParameterName == "DateValue").ParameterValue; }
            catch { }
            try { sFk_UserID = p_Params.Single(p => p.ParameterName == "Fk_UserID").ParameterValue; }
            catch { }
            try { sFk_OrderNo = p_Params.Single(p => p.ParameterName == "Fk_OrderNo").ParameterValue; }
            catch { }
            try { sDCNo = p_Params.Single(p => p.ParameterName == "DCNo").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterialIssues = _oEntities.Vw_AssignedStock;
            // DateTime oDateTime;
            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialIssueID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialIssueID));
                }
                if (!string.IsNullOrEmpty(sPk_JobCardID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_JobCardID == decimal.Parse(sPk_JobCardID));
                }
                //if (!string.IsNullOrEmpty(sPk_MatIssueID))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_MaterialIssueID == decimal.Parse(sPk_MatIssueID));
                //}
                //if (!string.IsNullOrEmpty(sIssueDate))
                //{

                //    oMaterialIssues = oMaterialIssues.Where(p => (p.IssueDate >= Convert.ToDateTime(sIssueDate)));
                //}
                //if (!string.IsNullOrEmpty(sFk_UserID))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_UserID == decimal.Parse(sFk_UserID));
                //}
                //if (!string.IsNullOrEmpty(sFk_OrderNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_OrderNo == decimal.Parse(sFk_OrderNo));
                //}
                //if (!string.IsNullOrEmpty(sDCNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.DCNo.IndexOf(sDCNo, StringComparison.OrdinalIgnoreCase) >= 0);

                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oMaterialIssues.Count();
            oMaterialIssues = oMaterialIssues.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oMaterialIssues = oMaterialIssues.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            //if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            //{

            //    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
            //    var skip = int.Parse(sPageSize) * (page - 1);

            //    oMaterialIssues = oMaterialIssues.Select(p => p)
            //                        .Skip(skip)
            //                        .Take(int.Parse(sPageSize));
            //}

            List<EntityObject> oFilteredItems = oMaterialIssues.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }


        public List<EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                SampMaterialIssue oNewMaterialIssue = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampMaterialIssue), omValues) as SampMaterialIssue;
                oNewMaterialIssue.Fk_UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);
                oNewMaterialIssue.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                var JcNo = Convert.ToDecimal(omValues["Fk_JobCardID"]);

                oNewMaterialIssue.IssueDate = Convert.ToDateTime(omValues["IssueDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                if (omValues.ContainsKey("IssueDetails"))
                {

                    string sIssueDetails = omValues["IssueDetails"].ToString();

                    if (sIssueDetails == "1")
                    {
                        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                        List<Vw_AssignedStock> oPrdList = new List<Vw_AssignedStock>();
                        oPrdList = dc.Vw_AssignedStock.Where(s => s.Pk_JobCardID == JcNo).Select(s => s).OfType<Vw_AssignedStock>().ToList();

                        int ListC = oPrdList.Count();
                        int i = 0;
                        //     SampMaterialIssueDetails oIssueDetail = new SampMaterialIssueDetails();
                        for (i = 0; i < ListC; i++)
                        {
                            SampMaterialIssueDetails oIssueDetail = new SampMaterialIssueDetails();
                            var Fk_Mat = Convert.ToDecimal(oPrdList.ElementAt(i).Pk_Material);
                            var Quantity = Convert.ToDecimal(oPrdList.ElementAt(i).Quantity);

                            var RollNo = oPrdList.ElementAt(i).RollNo;
                            var Pk_StockID = Convert.ToDecimal(oPrdList.ElementAt(i).Pk_PaperStock);
                            var Fk_Mill = Convert.ToDecimal(oPrdList.ElementAt(i).Pk_Mill);

                            oIssueDetail.Fk_Material = Fk_Mat;
                            oIssueDetail.Fk_Mill = Fk_Mill;
                            oIssueDetail.Pk_StockID = Pk_StockID;
                            oIssueDetail.Quantity = Quantity;
                            oIssueDetail.ReturnQuantity = 0;
                            oIssueDetail.Weight = 0;
                            oIssueDetail.RollNo = RollNo;

                            oNewMaterialIssue.SampMaterialIssueDetails.Add(oIssueDetail);



                        }
                    }
                    else
                    {

                        object[] IssueDetails = JsonConvert.DeserializeObject<object[]>(sIssueDetails);

                        foreach (object oDtetails in IssueDetails)
                        {
                            Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                            Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                            SampMaterialIssueDetails oIssueDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampMaterialIssueDetails), oDataProperties) as SampMaterialIssueDetails;
                            oIssueDetail.ReturnQuantity = 0;

                            oNewMaterialIssue.SampMaterialIssueDetails.Add(oIssueDetail);

                            //////Stock///
                            Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oIssueDetail.Fk_Material).Single();
                            if (oInv_Master.Fk_MaterialCategory != 4)
                            {
                                Stock oInv_StockMaster = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.Fk_Tanent == oIssueDetail.SampMaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();
                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity - oIssueDetail.Quantity);
                            }
                            else
                            {
                                int RCount = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.RollNo == oIssueDetail.RollNo).Count();
                                if (RCount > 0)
                                {

                                    PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.RollNo == oIssueDetail.RollNo).Single();
                                    PaperStock oInv_StockMaster1 = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.RollNo == oIssueDetail.RollNo).Single();
                                    oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster1.Quantity - oIssueDetail.Quantity);
                                }
                                else
                                {
                                    int RCount1 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.RollNo == oIssueDetail.RollNo).Count();
                                    if (RCount1 > 0)
                                    {

                                        JobWorkRMStock oInv_StockMaster = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.RollNo == oIssueDetail.RollNo).Single();
                                        JobWorkRMStock oInv_StockMaster1 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIssueDetail.Fk_Material && p.RollNo == oIssueDetail.RollNo).Single();
                                        oInv_StockMaster.StockValue = Convert.ToDecimal(oInv_StockMaster1.StockValue - oIssueDetail.Quantity);
                                    }
                                }
                            }
                        }


                    }
                }

                _oEntities.AddToSampMaterialIssue(oNewMaterialIssue);
                _oEntities.SaveChanges();

                oResult.Success = true;
                oResult.Message = oNewMaterialIssue.Pk_MaterialIssueID.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampMaterialIssue oMaterialIssue = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampMaterialIssue), omValues) as SampMaterialIssue;
                SampMaterialIssue oMaterialIssueFromShelf = _oEntities.SampMaterialIssue.Where(p => p.Pk_MaterialIssueID == oMaterialIssue.Pk_MaterialIssueID).Single();
                object orefMaterialIssueFromShelf = oMaterialIssueFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(SampMaterialIssue), omValues, ref orefMaterialIssueFromShelf);
                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);

                List<decimal> oUpdateList = new List<decimal>();

                if (omValues.ContainsKey("IssueDetails"))
                {
                    string sMaterialIssue = omValues["IssueDetails"].ToString();
                    object[] sMaterialIssues = JsonConvert.DeserializeObject<object[]>(sMaterialIssue);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<SampMaterialIssueDetails> NewIssueList = new List<SampMaterialIssueDetails>();

                    foreach (object oIssue in sMaterialIssues)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oIssue.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_MaterialIssueDetailsID") && oDataProperties["Pk_MaterialIssueDetailsID"] != null && oDataProperties["Pk_MaterialIssueDetailsID"].ToString() != "")
                        {
                            decimal Pk_MaterialIssueDetailsID = decimal.Parse(oDataProperties["Pk_MaterialIssueDetailsID"].ToString());

                            SampMaterialIssueDetails oIssueFromShelf = _oEntities.SampMaterialIssueDetails.Where(p => p.Pk_MaterialIssueDetailsID == Pk_MaterialIssueDetailsID).Single();

                            Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oIssueFromShelf.Fk_Material).Single();

                            //stocks//

                            oIssueFromShelf.ReturnQuantity = 0;
                            if (oInv_Master.Fk_MaterialCategory != 4)
                            {
                                Stock oStocks = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueFromShelf.Fk_Material && p.Fk_Tanent == oMaterialIssueFromShelf.Fk_Tanent && p.Fk_BranchID == branchid).Single();

                                oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oIssueFromShelf.Quantity);
                                oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity) - Convert.ToDecimal(oDataProperties["Quantity"]);
                            }

                            else
                            {
                                PaperStock oStocks = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueFromShelf.Fk_Material && p.Pk_PaperStock == oIssueFromShelf.Pk_StockID).Single();

                                oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oIssueFromShelf.Quantity);
                                oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity) - Convert.ToDecimal(oDataProperties["Quantity"]);
                            }

                            UpdatedPk.Add(Pk_MaterialIssueDetailsID);


                            object orefSchedule = oIssueFromShelf;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(SampMaterialIssueDetails), oDataProperties, ref orefSchedule);

                        }
                        else
                        {
                            SampMaterialIssueDetails oIssues = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampMaterialIssueDetails), oDataProperties) as SampMaterialIssueDetails;

                            Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oIssues.Fk_Material).Single();
                            oIssues.ReturnQuantity = 0;
                            if (oInv_Master.Fk_MaterialCategory != 4)
                            {

                                Stock oStocks = _oEntities.Stocks.Where(p => p.Fk_Material == oIssues.Fk_Material && p.Fk_Tanent == oMaterialIssueFromShelf.Fk_Tanent && p.Fk_BranchID == branchid).Single();
                                if (Convert.ToDecimal(oStocks.Quantity - oIssues.Quantity) >= 0)
                                {
                                    oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity - oIssues.Quantity);
                                    NewIssueList.Add(oIssues);
                                }

                            }
                            else
                            {
                                PaperStock oStocks = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssues.Fk_Material && p.Pk_PaperStock == oIssues.Pk_StockID).Single();
                                if (Convert.ToDecimal(oStocks.Quantity - oIssues.Quantity) >= 0)
                                {
                                    oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity - oIssues.Quantity);
                                    NewIssueList.Add(oIssues);
                                }

                            }


                        }
                    }

                    List<SampMaterialIssueDetails> oDeletedRecords = oMaterialIssueFromShelf.SampMaterialIssueDetails.Where(p => !UpdatedPk.Contains(p.Pk_MaterialIssueDetailsID)).ToList();

                    foreach (SampMaterialIssueDetails oDeletedDetail in oDeletedRecords)
                    {

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oDeletedDetail.Fk_Material).Single();

                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {
                            Stock oStocks = _oEntities.Stocks.Where(p => p.Fk_Material == oDeletedDetail.Fk_Material && p.Fk_Tanent == oMaterialIssueFromShelf.Fk_Tanent && p.Fk_BranchID == branchid).Single();
                            oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oDeletedDetail.Quantity);
                        }

                        else
                        {
                            PaperStock oStocks = _oEntities.PaperStock.Where(p => p.Fk_Material == oDeletedDetail.Fk_Material && p.Pk_PaperStock == oDeletedDetail.Pk_StockID).Single();
                            oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oDeletedDetail.Quantity);
                        }
                        oMaterialIssueFromShelf.SampMaterialIssueDetails.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (SampMaterialIssueDetails oNewIssueDetail in NewIssueList)
                    {
                        oMaterialIssueFromShelf.SampMaterialIssueDetails.Add(oNewIssueDetail);
                    }

                }
                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                SampMaterialIssue oMaterialIssue = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(SampMaterialIssue), omValues) as SampMaterialIssue;
                SampMaterialIssue oMaterialIssueFromShelf = _oEntities.SampMaterialIssue.Where(p => p.Pk_MaterialIssueID == oMaterialIssue.Pk_MaterialIssueID).Single();
                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                if (omValues.ContainsKey("MaterialData"))
                {
                    string sMaterials = omValues["MaterialData"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        decimal FkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);
                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == FkMaterial).Single();

                        SampMaterialIssueDetails oIssueReturnDetailsFromShelf = _oEntities.SampMaterialIssueDetails.Where(p => p.Fk_IssueID == oMaterialIssueFromShelf.Pk_MaterialIssueID && p.Fk_Material == FkMaterial).Single();
                        int sFkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);

                        Stock oInv_Stock = _oEntities.Stocks.Where(p => p.Fk_Material == sFkMaterial && p.Fk_Tanent == oMaterialIssueFromShelf.Fk_Tanent && p.Fk_BranchID == branchid).Single();
                        PaperStock oStocks = _oEntities.PaperStock.Where(p => p.Fk_Material == sFkMaterial && p.Pk_PaperStock == oIssueReturnDetailsFromShelf.Pk_StockID).Single();


                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {
                            if ((oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.Quantity) >= 0)
                            {

                                oInv_Stock.Quantity = Convert.ToDecimal(oInv_Stock.Quantity + oIssueReturnDetailsFromShelf.ReturnQuantity);


                                _oEntities.DeleteObject(oIssueReturnDetailsFromShelf);

                                _oEntities.SaveChanges();
                            }
                        }
                        else
                        {
                            if ((oStocks.Quantity - oIssueReturnDetailsFromShelf.Quantity) >= 0)
                            {

                                oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oIssueReturnDetailsFromShelf.ReturnQuantity);


                                _oEntities.DeleteObject(oIssueReturnDetailsFromShelf);

                                _oEntities.SaveChanges();
                            }

                        }
                    }
                }

                oMaterialIssueFromShelf.SampMaterialIssueDetails.Clear();
                _oEntities.DeleteObject(oMaterialIssueFromShelf);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public EntityObject DAO
        {
            get
            {
                return oMaterialIssue;
            }
            set
            {
                oMaterialIssue = value as SampMaterialIssue;
            }
        }



        public decimal ID
        {
            get
            {
                return oMaterialIssue.Pk_MaterialIssueID;
            }
            //set
            //{
            //    oInv_MaterialIndentMaster.Pk_MaterialOrderMasterId = value;
            //}
            set
            {
                oMaterialIssue = _oEntities.SampMaterialIssue.Where(p => p.Pk_MaterialIssueID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oMaterialIssue;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
