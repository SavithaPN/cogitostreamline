﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;

namespace CogitoStreamLineModel.DomainModel
{
    public class IssueReturns : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private IssueReturn oIssueReturns = null;
        #endregion Variables

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            string sPk_IssueReturnMasterId = null;
            string sPk_IssueReturnDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<IssueReturn> oIssueReturns = null;

            try { sPk_IssueReturnMasterId = p_Params.Single(p => p.ParameterName == "Pk_IssueReturnMasterId").ParameterValue; }
            catch { }
            try { sPk_IssueReturnDate = p_Params.Single(p => p.ParameterName == "IssueReturnDate").ParameterValue; }
            catch { }
            
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueReturns = _oEntities.IssueReturn;

            try
            {
                if (!string.IsNullOrEmpty(sPk_IssueReturnMasterId))
                {
                    oIssueReturns = oIssueReturns.Where(p => p.Pk_IssueReturnMasterId == decimal.Parse(sPk_IssueReturnMasterId));
                }
                if (!string.IsNullOrEmpty(sPk_IssueReturnDate))
                {
                    DateTime SFrom = Convert.ToDateTime(sPk_IssueReturnDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oIssueReturns = oIssueReturns.Where(p => (p.IssueReturnDate >= Convert.ToDateTime(SFrom)));
                    //oIssueReturns = oIssueReturns.Where(p => DateTime.Parse(p.IssueReturnDate.ToString()).ToString("dd/MM/yyyy") == SFrom);
                }
               
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueReturns.Count();
            oIssueReturns = oIssueReturns.OrderByDescending(p => p.Pk_IssueReturnMasterId);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueReturns = oIssueReturns.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueReturns.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        public SearchResult SearchIssueDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_MaterialIssueID = null;
            string sPk_MaterialName = null;
            string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<MaterialIssueDetail> oIssueIssueMasters = null;

            try { sPk_MaterialIssueID = p_Params.Single(p => p.ParameterName == "Pk_MaterialIssueID").ParameterValue; }
            catch { }
            try { sPk_MaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueIssueMasters = _oEntities.MaterialIssueDetails;

            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialIssueID))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Fk_IssueID == decimal.Parse(sPk_MaterialIssueID));
                }
                if (!string.IsNullOrEmpty(sPk_MaterialName))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Inv_Material.Name.IndexOf(sPk_MaterialName, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sFk_Material))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                }


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueIssueMasters.Count();
            oIssueIssueMasters = oIssueIssueMasters.OrderByDescending(p => p.Pk_MaterialIssueDetailsID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueIssueMasters = oIssueIssueMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueIssueMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchReturnDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_MaterialIssueID = null;
            string sPk_MaterialName = null;
            string sFk_Material = null;
            string sDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_Iss_Return> oIssueIssueMasters = null;

            try { sPk_MaterialIssueID = p_Params.Single(p => p.ParameterName == "IssueID").ParameterValue; }
            catch { }
            try { sPk_MaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sDate = p_Params.Single(p => p.ParameterName == "DateValue").ParameterValue; }
            catch { }
            try { sFk_Material = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oIssueIssueMasters = _oEntities.Vw_Iss_Return;

            try
            {
                if (!string.IsNullOrEmpty(sPk_MaterialIssueID))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Fk_IssueId == decimal.Parse(sPk_MaterialIssueID));
                }
               
                if (!string.IsNullOrEmpty(sFk_Material))
                {
                    oIssueIssueMasters = oIssueIssueMasters.Where(p => p.Pk_Material == decimal.Parse(sFk_Material));
                }
                if (!string.IsNullOrEmpty(sDate))
                {

                    oIssueIssueMasters = oIssueIssueMasters.Where(p => (p.IssueReturnDate >= Convert.ToDateTime(sDate)));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oIssueIssueMasters.Count();
            oIssueIssueMasters = oIssueIssueMasters.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oIssueIssueMasters = oIssueIssueMasters.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oIssueIssueMasters.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
      
      
        
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }


        //public ModelManuplationResult CreateNew()
        //{
        //    ModelManuplationResult oResult = new ModelManuplationResult();

        //    try
        //    {
        //        var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
        //        var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
        //        IssueReturn oNewIssueReturns = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturn), omValues) as IssueReturn;
        //        if (omValues.ContainsKey("Materials"))
        //        {
        //            string sMaterials = omValues["Materials"].ToString();
        //            object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

        //            foreach (object oItem in Materials)
        //            {
        //                Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
        //                Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
        //                IssueReturnDetails oIssueReturnDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturnDetails), oDataProperties) as IssueReturnDetails;
                         
        //                oNewIssueReturns.IssueReturnDetails.Add(oIssueReturnDetails);

        //                MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Fk_IssueID == oNewIssueReturns.Fk_IssueId).Single();
        //                oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity + oIssueReturnDetails.ReturnQuantity);

        //                Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oIssueReturnDetails.Fk_Material).Single();
        //                if (oInv_Master.Fk_MaterialCategory != 4)
        //                {
        //                    Stock oInv_StockMaster = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material).Single();
        //                    oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);
        //                }
        //                else
        //                {
        //                    PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Pk_PaperStock == oIssuedetails.Pk_StockID).Single();
        //                    oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);
        //                }


        //                MaterialIssue oIssue = _oEntities.MaterialIssue.Where(p => p.Pk_MaterialIssueID == oNewIssueReturns.Fk_IssueId).Single();

        //                JobCardDetails OJDet=_oEntities.JobCardDetails.Where(p => p.Fk_JobCardID  == oIssue.Fk_JobCardID && p.Fk_Material == oIssueReturnDetails.Fk_Material).Single();

        //                OJDet.QtyConsumed = Convert.ToDecimal(OJDet.RM_Consumed - oIssueReturnDetails.ReturnQuantity);
        //            }
        //        }
        //        oNewIssueReturns.Fk_Branch = branchid;
        //        oNewIssueReturns.Fk_Tanent = TanentID;
        //        oNewIssueReturns.Fk_EnteredUserId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
        //        _oEntities.AddToIssueReturn(oNewIssueReturns);
        //        _oEntities.SaveChanges();
        //        oResult.Success = true;
        //    }
        //    catch (Exception e)
        //    {
        //        oResult.Success = false;
        //        oResult.Message = e.Message;
        //        oResult.Exception = e;
        //    }

        //    return oResult;
        //}



        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                IssueReturn oNewIssueReturns = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturn), omValues) as IssueReturn;


                oNewIssueReturns.IssueReturnDate = Convert.ToDateTime(omValues["IssueReturnDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                oNewIssueReturns.IssueDate = Convert.ToDateTime(omValues["IssueDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        IssueReturnDetails oIssueReturnDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturnDetails), oDataProperties) as IssueReturnDetails;

                        oNewIssueReturns.IssueReturnDetails.Add(oIssueReturnDetails);

                        MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Fk_IssueID == oNewIssueReturns.Fk_IssueId && p.RollNo == oIssueReturnDetails.RollNo).Single();
                        oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity + oIssueReturnDetails.ReturnQuantity);

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oIssueReturnDetails.Fk_Material).Single();
                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {
                            Stock oInv_StockMaster = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material).Single();
                            oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);
                        }
                        else
                        {
                            int StkCnt1 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.RollNo == oIssuedetails.RollNo).Count();

                            int StkCnt2 = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Pk_PaperStock == oIssuedetails.Pk_StockID).Count();

                            if (StkCnt1 > 0)
                            {
                                JobWorkRMStock oInv_StockMaster = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.RollNo == oIssuedetails.RollNo).Single();
                                oInv_StockMaster.StockValue = Convert.ToDecimal(oInv_StockMaster.StockValue + oIssueReturnDetails.ReturnQuantity);
                            }

                            if (StkCnt2 > 0)
                            {
                                PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Pk_PaperStock == oIssuedetails.Pk_StockID).Single();
                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);
                            }

                            //if (omValues.ContainsKey("Product"))
                            //{
                                //if (omValues["Product"] == "Job Work")
                                //{
                                //    JobWorkRMStock oInv_StockMaster = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.RollNo == oIssuedetails.RollNo).Single();
                                //    oInv_StockMaster.StockValue = Convert.ToDecimal(oInv_StockMaster.StockValue + oIssueReturnDetails.ReturnQuantity);
                                //}
                                //else
                                //{
                                //    PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Pk_PaperStock == oIssuedetails.Pk_StockID).Single();
                                //    oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);
                                //}
                            //}
                        }

                        
                        MaterialIssue oIssue = _oEntities.MaterialIssue.Where(p => p.Pk_MaterialIssueID == oNewIssueReturns.Fk_IssueId).Single();
                        int StkCnt4 = _oEntities.JobCardDetails.Where(p => p.Fk_JobCardID == oIssue.Fk_JobCardID && p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Fk_PaperStock == oIssuedetails.Pk_StockID).Count();
                        if (StkCnt4 > 0)
                        {
                            JobCardDetails OJDet = _oEntities.JobCardDetails.Where(p => p.Fk_JobCardID == oIssue.Fk_JobCardID && p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Fk_PaperStock == oIssuedetails.Pk_StockID).Single();

                            OJDet.QtyConsumed = Convert.ToDecimal(OJDet.RM_Consumed - oIssueReturnDetails.ReturnQuantity);
                        }
                    }
                }
                oNewIssueReturns.Fk_Branch = branchid;
                oNewIssueReturns.Fk_Tanent = TanentID;
                oNewIssueReturns.Fk_EnteredUserId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                _oEntities.AddToIssueReturn(oNewIssueReturns);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
         
        
        
      
        
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                IssueReturn oIssueReturns = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturn), omValues) as IssueReturn;
                IssueReturn OInvOENIssueMasterFromShelf = _oEntities.IssueReturn.Where(p => p.Pk_IssueReturnMasterId == oIssueReturns.Pk_IssueReturnMasterId).Single();
                object orefIndent = OInvOENIssueMasterFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(IssueReturn), omValues, ref orefIndent);

                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                string sMaterials = omValues["Materials"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<IssueReturnDetails> NewMaterialsList = new List<IssueReturnDetails>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_IssueReturnDetailsId"] == null || oDataProperties2["Pk_IssueReturnDetailsId"].ToString() == "")
                    {
                        IssueReturnDetails oIssueReturnDetails
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturnDetails), oDataProperties2) as IssueReturnDetails;

                        NewMaterialsList.Add(oIssueReturnDetails);

                        //stock details

                        MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                        oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity + oIssueReturnDetails.ReturnQuantity);

                        //Stock oStocks = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Fk_Tanent == OInvOENIssueMasterFromShelf.MaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();
                        //oStocks.Quantity = Convert.ToDecimal(oStocks.Quantity + oIssueReturnDetails.ReturnQuantity);

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oIssueReturnDetails.Fk_Material).Single();
                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {
                            Stock oInv_StockMaster = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material).Single();
                            oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);
                        }
                        else
                        {
                            PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueReturnDetails.Fk_Material && p.Pk_PaperStock == oIssueReturnDetails.Pk_StockID).Single();
                            oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + oIssueReturnDetails.ReturnQuantity);


                             MaterialIssue oIssue = _oEntities.MaterialIssue.Where(p => p.Pk_MaterialIssueID == oIssueReturns.Fk_IssueId).Single();

                        JobCardDetails OJDet = _oEntities.JobCardDetails.Where(p => p.Fk_JobCardID == oIssue.Fk_JobCardID && p.Fk_Material == oIssueReturnDetails.Fk_Material).Single();

                        OJDet.QtyConsumed = Convert.ToDecimal(OJDet.RM_Consumed - oIssueReturnDetails.ReturnQuantity);
                        }

                       

                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkIssueDetail = Convert.ToDecimal(oDataProperties2["Pk_IssueReturnDetailsId"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                        //decimal dQuantity = Convert.ToDecimal(oDataProperties2["Quantity"]);
                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == dFkMaterial).Single();
                        
                        IssueReturnDetails oIssueReturnDetailsFromShelf = _oEntities.IssueReturnDetails.Where(p => p.Pk_IssueReturnDetailsId == dPkIssueDetail && p.Fk_Material == dFkMaterial).Single();

                        object oIssueReturnDetails
                                            = _oEntities.IssueReturnDetails.Where(p => p.Pk_IssueReturnDetailsId == dPkIssueDetail).First();

                        //stock details

                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {
                            Stock oInv_StockMaster = _oEntities.Stocks.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_Tanent == OInvOENIssueMasterFromShelf.MaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();



                            if ((Convert.ToDecimal(oInv_StockMaster.Quantity) - Convert.ToDecimal(oIssueReturnDetailsFromShelf.ReturnQuantity) + Convert.ToDecimal(oDataProperties2["ReturnQuantity"])) >= 0)
                            {
                                MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                                oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                                oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity + Convert.ToDecimal(oDataProperties2["ReturnQuantity"]));


                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + Convert.ToDecimal(oDataProperties2["ReturnQuantity"]));
                                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(IssueReturnDetails), oDataProperties2, ref oIssueReturnDetails);
                            }
                        }
                        else
                        {
                            PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Pk_PaperStock == oIssueReturnDetailsFromShelf.Pk_StockID).Single();
                            if ((Convert.ToDecimal(oInv_StockMaster.Quantity) - Convert.ToDecimal(oIssueReturnDetailsFromShelf.ReturnQuantity) + Convert.ToDecimal(oDataProperties2["ReturnQuantity"])) >= 0)
                            {
                                MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                                oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                                oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity + Convert.ToDecimal(oDataProperties2["ReturnQuantity"]));


                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity + Convert.ToDecimal(oDataProperties2["ReturnQuantity"]));
                                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(IssueReturnDetails), oDataProperties2, ref oIssueReturnDetails);


                                MaterialIssue oIssue = _oEntities.MaterialIssue.Where(p => p.Pk_MaterialIssueID == oIssueReturns.Fk_IssueId).Single();



                                JobCardDetails OJDet = _oEntities.JobCardDetails.Where(p => p.Fk_JobCardID == oIssue.Fk_JobCardID && p.Fk_Material == oInv_StockMaster.Fk_Material).Single();

                                OJDet.QtyConsumed = Convert.ToDecimal(OJDet.RM_Consumed - Convert.ToDecimal(oDataProperties2["ReturnQuantity"]));
                            }

                        }

                        UpdatedPk.Add(dPkIssueDetail);

                    }
                }
                // Handeling Deleted Records

                List<IssueReturnDetails> oDeletedRecords = OInvOENIssueMasterFromShelf.IssueReturnDetails.Where(p => !UpdatedPk.Contains(p.Pk_IssueReturnDetailsId)).ToList();

                foreach (IssueReturnDetails oDeletedDetail in oDeletedRecords)
                {

                    Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oDeletedDetail.Fk_Material).Single();
                        

                    //stock details
                    if (oInv_Master.Fk_MaterialCategory != 4)
                    {
                        Stock oInv_StockMaster = _oEntities.Stocks.Where(p => p.Fk_Material == oDeletedDetail.Fk_Material && p.Fk_Tanent == OInvOENIssueMasterFromShelf.MaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();
                        if ((Convert.ToDecimal(oInv_StockMaster.Quantity) - Convert.ToDecimal(oDeletedDetail.ReturnQuantity)) >= 0)
                        {
                            MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oDeletedDetail.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                            oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity - oDeletedDetail.ReturnQuantity);
                            oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity) - Convert.ToDecimal(oDeletedDetail.ReturnQuantity);
                            OInvOENIssueMasterFromShelf.IssueReturnDetails.Remove(oDeletedDetail);
                        }
                    }
                    else
                    {
                        PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == oDeletedDetail.Fk_Material && p.Pk_PaperStock == oDeletedDetail.Pk_StockID).Single();
                        if ((Convert.ToDecimal(oInv_StockMaster.Quantity) - Convert.ToDecimal(oDeletedDetail.ReturnQuantity)) >= 0)
                        {
                            MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oDeletedDetail.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                            oIssuedetails.ReturnQuantity = Convert.ToDecimal(oIssuedetails.ReturnQuantity - oDeletedDetail.ReturnQuantity);
                            oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity) - Convert.ToDecimal(oDeletedDetail.ReturnQuantity);
                            OInvOENIssueMasterFromShelf.IssueReturnDetails.Remove(oDeletedDetail);
                        }
                    }
                }

                //Add new elements
                foreach (IssueReturnDetails oNewMaterialDetail in NewMaterialsList)
                {
                    OInvOENIssueMasterFromShelf.IssueReturnDetails.Add(oNewMaterialDetail);
                }

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();


            try
            {
                IssueReturn oIssueReturns = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(IssueReturn), omValues) as IssueReturn;
                IssueReturn OInvIndentMasterFromShelf = _oEntities.IssueReturn.Where(p => p.Pk_IssueReturnMasterId == oIssueReturns.Pk_IssueReturnMasterId).Single();
                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                if (omValues.ContainsKey("MaterialData"))
                {
                    string sMaterials = omValues["MaterialData"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {

                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        decimal FkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);
                          
                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material ==FkMaterial).Single();

                        IssueReturnDetails oIssueReturnDetailsFromShelf = _oEntities.IssueReturnDetails.Where(p => p.Fk_IssueReturnMasterId == OInvIndentMasterFromShelf.Pk_IssueReturnMasterId && p.Fk_Material == FkMaterial).Single();
                        int sFkMaterial = Convert.ToInt32(oDataProperties["Fk_Material"]);

                        Stock oInv_Stock = _oEntities.Stocks.Where(p => p.Fk_Material == sFkMaterial && p.Fk_Tanent == OInvIndentMasterFromShelf.MaterialIssue.Fk_Tanent && p.Fk_BranchID == branchid).Single();

                        PaperStock oInv_StockMaster = _oEntities.PaperStock.Where(p => p.Fk_Material == sFkMaterial && p.Pk_PaperStock == oIssueReturnDetailsFromShelf.Pk_StockID).Single();

                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {
                            if ((oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity) >= 0)
                            {
                                MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                                oIssuedetails.ReturnQuantity = (oIssuedetails.ReturnQuantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                                oInv_Stock.Quantity = Convert.ToDecimal(oInv_Stock.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity);


                                _oEntities.DeleteObject(oIssueReturnDetailsFromShelf);

                                _oEntities.SaveChanges();
                            }
                        }

                        else
                        {
                            if ((oInv_StockMaster.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity) >= 0)
                            {
                                MaterialIssueDetail oIssuedetails = _oEntities.MaterialIssueDetails.Where(p => p.Fk_Material == oIssueReturnDetailsFromShelf.Fk_Material && p.Fk_IssueID == oIssueReturns.Fk_IssueId).Single();
                                oIssuedetails.ReturnQuantity = (oIssuedetails.ReturnQuantity - oIssueReturnDetailsFromShelf.ReturnQuantity);
                                oInv_StockMaster.Quantity = Convert.ToDecimal(oInv_StockMaster.Quantity - oIssueReturnDetailsFromShelf.ReturnQuantity);


                                _oEntities.DeleteObject(oIssueReturnDetailsFromShelf);

                                _oEntities.SaveChanges();
                            }

                        }
                    }
                }
                int oIssueReturnDet = _oEntities.IssueReturnDetails.Where(p => p.Fk_IssueReturnMasterId == oIssueReturns.Pk_IssueReturnMasterId).Count();
                if (oIssueReturnDet == 0)
                {
                    OInvIndentMasterFromShelf = _oEntities.IssueReturn.Where(p => p.Pk_IssueReturnMasterId == oIssueReturns.Pk_IssueReturnMasterId).Single();
                    _oEntities.DeleteObject(OInvIndentMasterFromShelf);
                    _oEntities.SaveChanges();
                }
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }


            return oResult;
        }
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oIssueReturns;
            }
            set
            {
                oIssueReturns = value as IssueReturn;
            }
        }
        public decimal ID
        {
            get
            {
                return oIssueReturns.Pk_IssueReturnMasterId;
            }
            set
            {
                oIssueReturns = _oEntities.IssueReturn.Where(p => p.Pk_IssueReturnMasterId == value).Single();
            }
        }
        public object GetRaw()
        {
            return oIssueReturns;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion Methods

    }
}
