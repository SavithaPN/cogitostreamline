﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class InkCert : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private InkCertificate oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Id = null;
            string sMaterialName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<InkCertificate> oColors = null;

            try { sPk_Id = p_Params.Single(p => p.ParameterName == "Pk_Id").ParameterValue; }
            catch { }
            try { sMaterialName = p_Params.Single(p => p.ParameterName == "MaterialName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.InkCertificate;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Id))
                {
                    oColors = oColors.Where(p => p.Pk_Id == decimal.Parse(sPk_Id));
                }
                if (!string.IsNullOrEmpty(sMaterialName))
                {
                    oColors = oColors.Where(p => p.Inv_Material.Name.IndexOf(sMaterialName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderBy(p => p.Pk_Id);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchMaterialName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<InkCertificate> oMaterialName = null;
            oMaterialName = _oEntites.InkCertificate;

            oSearchResult.RecordCount = oMaterialName.Count();
            oMaterialName.OrderBy(p => p.Pk_Id);

            List<EntityObject> oFilteredItem = oMaterialName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                InkCertificate oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(InkCertificate), omValues) as InkCertificate;
                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        InkCertificateDetails oNewPOrderD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(InkCertificateDetails), oDataProperties) as InkCertificateDetails;

                        oNewPOrderD.Fk_Params = Convert.ToDecimal(oDataProperties["Pk_CharID"]);
                        //oNewPOrderD.Amount = (oNewPOrderD.Quantity * oNewPOrderD.Rate);

                        oNewColor.InkCertificateDetails.Add(oNewPOrderD);


                    }


                }
                _oEntites.AddToInkCertificate(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                InkCertificate oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(InkCertificate), omValues) as InkCertificate;
                InkCertificate oColorFromShelf = _oEntites.InkCertificate.Where(p => p.Pk_Id == oColor.Pk_Id).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(InkCertificate), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                InkCertificate oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(InkCertificate), omValues) as InkCertificate;
                InkCertificate oColorFromShelf = _oEntites.InkCertificate.Where(p => p.Pk_Id == oColor.Pk_Id).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as InkCertificate;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_Id;
            }
            set
            {
                oColor = _oEntites.InkCertificate.Where(p => p.Pk_Id == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


