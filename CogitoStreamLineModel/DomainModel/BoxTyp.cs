﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class ItemP
//    {
//    }
//}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class BoxTyp : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private BoxType oBoxT = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_BoxTypeID = null;
            string sName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<BoxType> oBoxTs = null;

            try { sPk_BoxTypeID = p_Params.Single(p => p.ParameterName == "Pk_BoxTypeID").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oBoxTs = _oEntites.BoxType;

            try
            {
                if (!string.IsNullOrEmpty(sPk_BoxTypeID))
                {
                    oBoxTs = oBoxTs.Where(p => p.Pk_BoxTypeID == decimal.Parse(sPk_BoxTypeID));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oBoxTs = oBoxTs.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oBoxTs.Count();

            oBoxTs.OrderBy(p => p.Pk_BoxTypeID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oBoxTs = oBoxTs.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oBoxTs.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                BoxType oNewPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxType), omValues) as BoxType;

                _oEntites.AddToBoxType(oNewPart);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                BoxType oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxType), omValues) as BoxType;
                BoxType oPartFromShelf = _oEntites.BoxType.Where(p => p.Pk_BoxTypeID == oPart.Pk_BoxTypeID).Single();
                object orefItems = oPartFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(BoxType), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                BoxType oPart = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(BoxType), omValues) as BoxType;
                BoxType oPartFromShelf = _oEntites.BoxType.Where(p => p.Pk_BoxTypeID == oPart.Pk_BoxTypeID).Single();
                object orefItems = oPartFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oPartFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oBoxT;

            }
            set
            {
                oBoxT = value as BoxType;
            }
        }

        public decimal ID
        {
            get
            {
                return oBoxT.Pk_BoxTypeID;
            }
            set
            {
                oBoxT = _oEntites.BoxType.Where(p => p.Pk_BoxTypeID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oBoxT;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


