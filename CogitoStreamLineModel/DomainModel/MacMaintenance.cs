﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;

using CogitoStreamLineModel;
namespace CogitoStreamLineModel.DomainModel
{
    public class MacMaintenance : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private MachineMaintenance oMachineMaintenance = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_MachineMaintenance = null;
            string sFk_Machine = null;
            string sFromDate = null;
            string sToDate = null;
            string sDoneBy = null;
            string sComments = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<MachineMaintenance> oMachineMaintenances = null;

            try { sPk_MachineMaintenance = p_Params.Single(p => p.ParameterName == "Pk_MachineMaintenance").ParameterValue; }
            catch { }
            try { sFk_Machine = p_Params.Single(p => p.ParameterName == "Fk_Machine").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }
            try { sDoneBy = p_Params.Single(p => p.ParameterName == "DoneBy").ParameterValue; }
            catch { }
            try { sComments = p_Params.Single(p => p.ParameterName == "Comments").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMachineMaintenances = _oEntites.MachineMaintenances;
            try
            {
                if (!string.IsNullOrEmpty(sPk_MachineMaintenance))
                {
                    oMachineMaintenances = oMachineMaintenances.Where(p => p.Pk_MachineMaintenance == decimal.Parse(sPk_MachineMaintenance));
                }
                //if (!string.IsNullOrEmpty(sStateName))
                //{
                //    //oStates = oStates.Where(p => p.StateName.IndexOf(sStateName, StringComparison.OrdinalIgnoreCase) >= 0);
                //    oStates = oStates.Where(p => p.StateName.ToLower() == (sStateName.ToLower().ToString().Trim()));

                //}
                //if (!string.IsNullOrEmpty(sCountryId))
                //{
                //    oStates = oStates.Where(p => p.Country == decimal.Parse(sCountryId));
                //}
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    oMachineMaintenances = oMachineMaintenances.Where(p => p.FromDate == Convert.ToDateTime(sFromDate));
                }
            }
            catch (System.NullReferenceException)
            {

            }

            oSearchResult.RecordCount = oMachineMaintenances.Count();

            oMachineMaintenances.OrderBy(p => p.Pk_MachineMaintenance);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMachineMaintenances = oMachineMaintenances.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMachineMaintenances.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchMacMaintenanceName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<MachineMaintenance> oMacMaintenanceName = null;
            oMacMaintenanceName = _oEntites.MachineMaintenances;

            oSearchResult.RecordCount = oMacMaintenanceName.Count();
            oMacMaintenanceName.OrderBy(p => p.Pk_MachineMaintenance);

            List<EntityObject> oFilteredItem = oMacMaintenanceName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                MachineMaintenance oNewMacMaintenances = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MachineMaintenance), omValues) as MachineMaintenance;

                _oEntites.AddToMachineMaintenances(oNewMacMaintenances);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                MachineMaintenance oMachineMaintenances = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MachineMaintenance), omValues) as MachineMaintenance;
                MachineMaintenance oMacMaintenacesFromShelf = _oEntites.MachineMaintenances.Where(p => p.Pk_MachineMaintenance == oMachineMaintenances.Pk_MachineMaintenance).Single();
                object orefItems = oMacMaintenacesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(MachineMaintenance), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                MachineMaintenance oMachineMaintenances = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(MachineMaintenance), omValues) as MachineMaintenance;
                MachineMaintenance oMacMaintenacesFromShelf = _oEntites.MachineMaintenances.Where(p => p.Pk_MachineMaintenance == oMachineMaintenances.Pk_MachineMaintenance).Single();
                object orefItems = oMacMaintenacesFromShelf;

                _oEntites.DeleteObject(oMacMaintenacesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oMachineMaintenance;

            }
            set
            {
                oMachineMaintenance = value as MachineMaintenance;
            }
        }

        public decimal ID
        {
            get
            {
                return oMachineMaintenance.Pk_MachineMaintenance;
            }
            set
            {
                oMachineMaintenance = _oEntites.MachineMaintenances.Where(p => p.Pk_MachineMaintenance == value).Single();
            }
        }

        public object GetRaw()
        {
            return oMachineMaintenance;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}



