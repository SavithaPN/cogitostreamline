﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;
using System.IO;

namespace CogitoStreamLineModel.DomainModel
{
    public class Enquiry : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private eq_Enquiry oeq_Enquiry = null;
        private bool disposed = false;
        #endregion Variables

        #region Properties
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oeq_Enquiry;
            }
            set
            {
                oeq_Enquiry = value as eq_Enquiry;
            }
        }
        public decimal ID
        {
            get
            {
                return oeq_Enquiry.Pk_Enquiry;
            }
            set
            {
                oeq_Enquiry = _oEntities.eq_Enquiry.Where(p => p.Pk_Enquiry == value).Single();
            }
        }
        #endregion Properties

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Enquiry = null;
            string sCustomer = null;
            string sEnquiryFromDate = null;
            string sEnquiryToDate = null;
            string sCommunicationType = null;
            string sState = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            string sTanent = null;
            IEnumerable<eq_Enquiry> oEnquires = null;

            try { sPk_Enquiry = p_Params.Single(p => p.ParameterName == "Pk_Enquiry").ParameterValue; }
            catch { }
            try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            catch { }
            try
            { sEnquiryFromDate = p_Params.Single(p => p.ParameterName == "EnquiryFromDate").ParameterValue; }
            catch { }
            try
            { sEnquiryToDate = p_Params.Single(p => p.ParameterName == "EnquiryToDate").ParameterValue; }
            catch { }

            try { sCommunicationType = p_Params.Single(p => p.ParameterName == "CommunicationType").ParameterValue; }
            catch { }
            try { sState = p_Params.Single(p => p.ParameterName == "State").ParameterValue; }
            catch { }
            try{sTanent = HttpContext.Current.Session["Tanent"].ToString();}catch{}
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oEnquires = _oEntities.eq_Enquiry;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Enquiry))
                {
                    oEnquires = oEnquires.Where(p => p.Pk_Enquiry == decimal.Parse(sPk_Enquiry));
                }
                //if (!string.IsNullOrEmpty(sEnquiryDate))
                //{
                //    oEnquires = oEnquires.Where(p => p.Date == Convert.ToDateTime(sEnquiryDate));
                //}

                if (!string.IsNullOrEmpty(sEnquiryFromDate))
                {
                    oEnquires = oEnquires.Where(p => (p.Date >= Convert.ToDateTime(sEnquiryFromDate)));
                }
                if (!string.IsNullOrEmpty(sEnquiryToDate))
                {
                    oEnquires = oEnquires.Where(p => (p.Date <= Convert.ToDateTime(sEnquiryToDate)));
                }
                if (!string.IsNullOrEmpty(sCustomer))
                {
                    oEnquires = oEnquires.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sCommunicationType))
                {
                    oEnquires = oEnquires.Where(p => p.gen_Communication.Type.IndexOf(sCommunicationType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sState))
                {
                    oEnquires = oEnquires.Where(p => p.wfState.State.IndexOf(sState, StringComparison.OrdinalIgnoreCase) >= 0);
                }

                if (!string.IsNullOrEmpty(sTanent))
                {
                    oEnquires = oEnquires.Where(p => p.Fk_Tanent==Convert.ToInt32(sTanent));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEnquires.Count();
            oEnquires = oEnquires.OrderByDescending(p => p.Pk_Enquiry);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEnquires = oEnquires.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredEnquires = oEnquires.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredEnquires;

            return oSearchResult;
        }
        public SearchResult SearchDel(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Enquiry = null;
            //string sCustomer = null;
            //string sEnquiryFromDate = null;
            //string sEnquiryToDate = null;
            //string sCommunicationType = null;
            //string sState = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            //string sTanent = null;
            IEnumerable<Vw_OrdQty> oEnquires = null;

            try { sPk_Enquiry = p_Params.Single(p => p.ParameterName == "Pk_Enquiry").ParameterValue; }
            catch { }
            //try { sCustomer = p_Params.Single(p => p.ParameterName == "Customer").ParameterValue; }
            //catch { }
            //try
            //{ sEnquiryFromDate = p_Params.Single(p => p.ParameterName == "EnquiryFromDate").ParameterValue; }
            //catch { }
            //try
            //{ sEnquiryToDate = p_Params.Single(p => p.ParameterName == "EnquiryToDate").ParameterValue; }
            //catch { }

            //try { sCommunicationType = p_Params.Single(p => p.ParameterName == "CommunicationType").ParameterValue; }
            //catch { }
            //try { sState = p_Params.Single(p => p.ParameterName == "State").ParameterValue; }
            //catch { }
            //try { sTanent = HttpContext.Current.Session["Tanent"].ToString(); }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oEnquires = _oEntities.Vw_OrdQty;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Enquiry))
                {
                    oEnquires = oEnquires.Where(p => p.Fk_Enquiry == decimal.Parse(sPk_Enquiry));
                }
                //if (!string.IsNullOrEmpty(sEnquiryDate))
                //{
                //    oEnquires = oEnquires.Where(p => p.Date == Convert.ToDateTime(sEnquiryDate));
                //}

                //if (!string.IsNullOrEmpty(sEnquiryFromDate))
                //{
                //    oEnquires = oEnquires.Where(p => (p.Date >= Convert.ToDateTime(sEnquiryFromDate)));
                //}
                //if (!string.IsNullOrEmpty(sEnquiryToDate))
                //{
                //    oEnquires = oEnquires.Where(p => (p.Date <= Convert.ToDateTime(sEnquiryToDate)));
                //}
                //if (!string.IsNullOrEmpty(sCustomer))
                //{
                //    oEnquires = oEnquires.Where(p => p.gen_Customer.CustomerName.IndexOf(sCustomer, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sCommunicationType))
                //{
                //    oEnquires = oEnquires.Where(p => p.gen_Communication.Type.IndexOf(sCommunicationType, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sState))
                //{
                //    oEnquires = oEnquires.Where(p => p.wfState.State.IndexOf(sState, StringComparison.OrdinalIgnoreCase) >= 0);
                //}

                //if (!string.IsNullOrEmpty(sTanent))
                //{
                //    oEnquires = oEnquires.Where(p => p.Fk_Tanent == Convert.ToInt32(sTanent));
                //}
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEnquires.Count();
            oEnquires = oEnquires.OrderByDescending(p => p.Fk_Enquiry);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEnquires = oEnquires.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredEnquires = oEnquires.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredEnquires;

            return oSearchResult;
        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            
            try
            {
                eq_Enquiry oNewEnquires = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(eq_Enquiry), omValues) as eq_Enquiry;
                if (omValues.ContainsKey("Documents"))
                {
                    string sDocs = omValues["Documents"].ToString();
                    if (sDocs != "")
                    {
                        object[] Docs = JsonConvert.DeserializeObject<object[]>(sDocs);

                        foreach (object oItem in Docs)
                        {
                            eq_Documents oDocument = new eq_Documents();
                            Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                            oDocument.FileName = oProperties["FileName"];
                            var FN=oProperties["FileName"];
                            oDocument.DocumentLink = HttpContext.Current.Server.MapPath(FN);
                            var filePath = HttpContext.Current.Server.MapPath(FN);



                            //name = Path.GetFileNameWithoutExtension(CogitoStreamLineModel.eq_Enquiry.FN);

                         //   System.IO.File.Move(filePath, HttpContext.Current.Server.MapPath("~/ConvertPDF"));

                         

                            //FileStream fS = new FileStream(filePath1, FileMode.Open, FileAccess.Read);
                            //BinaryReader br = new BinaryReader(fS);
                            //byte[] bytes = br.ReadBytes(Convert.ToInt32(fS.Length));


                            
                            oDocument.FileSize = oProperties["FileSize"];
                            oNewEnquires.eq_Documents.Add(oDocument);
                        }
                    }
                }
                if (omValues.ContainsKey("Sample"))
                {
                    var sample= omValues["Sample"].ToString();
                    if (sample=="True")
                    {
                        oNewEnquires.SampleRecd = "Yes";
                    }
                    else
                    {
                        oNewEnquires.SampleRecd = "No";
                    }
                }
                if (omValues.ContainsKey("Customer"))
                {
                    if (omValues["Customer"].ToString() == "")
                    {
                        oNewEnquires.Customer = 1;
                    }
                    else
                    { oNewEnquires.Customer = Convert.ToDecimal(omValues["Customer"]); }
                }
                else
                {
                    oNewEnquires.Customer = 1;
                }
                if (omValues.ContainsKey("CommunicationType"))
                {
                    oNewEnquires.CommunicationType = Convert.ToDecimal(omValues["CommunicationType"]);
                }
                else
                {
                    oNewEnquires.CommunicationType = 1;
                }
                //if (omValues.ContainsKey("deliverySchedules"))
                //{
                //    string sDeliverySchedules = omValues["deliverySchedules"].ToString();
                //    object[] DeliverySchedules = JsonConvert.DeserializeObject<object[]>(sDeliverySchedules);

                //    foreach (object oSchedule in DeliverySchedules)
                //    {
                //        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oSchedule.ToString());
                //        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                //        gen_DeliverySchedule oDeliverySchedule = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_DeliverySchedule), oDataProperties) as gen_DeliverySchedule;
                //        oDeliverySchedule.DeliveredDate = null;
                //        oNewEnquires.gen_DeliverySchedule.Add(oDeliverySchedule);
                //    }
                //}

                if (omValues.ContainsKey("enquiryChilds"))
                {
                    string sEnquiryChilds = omValues["enquiryChilds"].ToString();
                    object[] EnquiryChilds = JsonConvert.DeserializeObject<object[]>(sEnquiryChilds);

                    foreach (object oEqChild in EnquiryChilds)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oEqChild.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                        eq_EnquiryChild oEnquiryChild = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(eq_EnquiryChild), oDataProperties) as eq_EnquiryChild;

                        oEnquiryChild.Ddate = Convert.ToDateTime(oDataProperties["Ddate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                        oNewEnquires.eq_EnquiryChild.Add(oEnquiryChild);
                    }
                }

              
                oNewEnquires.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                oNewEnquires.Fk_Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                oNewEnquires.Fk_Status = _oEntities.wfStates.Where(p => p.State == "New").Single().Pk_State;
            //    oNewEnquires.Date=Convert.ToDateTime(DateTime.Now.ToString());

//                oNewEnquires.Date = Convert.ToDateTime(omValues["Date"].ToString(),
//System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                oNewEnquires.Date = DateTime.Now;
                _oEntities.AddToeq_Enquiry(oNewEnquires);
                _oEntities.SaveChanges();
                oResult.Success = true;
                oResult.Message = oNewEnquires.Pk_Enquiry.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                eq_Enquiry oEnquires = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(eq_Enquiry), omValues) as eq_Enquiry;
                eq_Enquiry oEnquiresFromShelf = _oEntities.eq_Enquiry.Where(p => p.Pk_Enquiry == oEnquires.Pk_Enquiry).Single();

                oEnquiresFromShelf.eq_Documents.Clear();
                if (omValues.ContainsKey("Documents"))
                {
                    string sDocs = omValues["Documents"].ToString();
                    object[] Docs = JsonConvert.DeserializeObject<object[]>(sDocs);

                    foreach (object oItem in Docs)
                    {
                        eq_Documents oDocument = new eq_Documents();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oDocument.FileName = oProperties["FileName"];
                        //oDocument.DocumentLink = oProperties["DocumentLink"];
                        oDocument.FileSize = oProperties["FileSize"];
                        oEnquiresFromShelf.eq_Documents.Add(oDocument);
                    }
                }

          

                if (omValues.ContainsKey("enquiryChilds"))
                {
                    string sEnquiryChilds = omValues["enquiryChilds"].ToString();
                    object[] EnquiryChilds = JsonConvert.DeserializeObject<object[]>(sEnquiryChilds);
                    List<decimal> UpdatedPk = new List<decimal>();
                    List<eq_EnquiryChild> NewEqChildList = new List<eq_EnquiryChild>();
                    foreach (object oEqChild in EnquiryChilds)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oEqChild.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        if (oDataProperties.ContainsKey("Pk_EnquiryChild") && oDataProperties["Pk_EnquiryChild"] != null && oDataProperties["Pk_EnquiryChild"].ToString() != "")
                        {
                            decimal pkEnquiryChild = decimal.Parse(oDataProperties["Pk_EnquiryChild"].ToString());

                            eq_EnquiryChild oEqChildFromShelf = _oEntities.eq_EnquiryChild.Where(p => p.Pk_EnquiryChild == pkEnquiryChild).Single();

                            object orefEqChild = oEqChildFromShelf;
                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(eq_EnquiryChild), oDataProperties, ref orefEqChild);
                            UpdatedPk.Add(pkEnquiryChild);
                        }
                        else
                        {
                            eq_EnquiryChild oEnquiryChild = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(eq_EnquiryChild), oDataProperties) as eq_EnquiryChild;
                            //oEnquiryChild.DeliveredDate = null;

                            NewEqChildList.Add(oEnquiryChild);
                        }
                    }

                    List<eq_EnquiryChild> oDeletedRecords = oEnquiresFromShelf.eq_EnquiryChild.Where(p => !UpdatedPk.Contains(p.Pk_EnquiryChild)).ToList();
                    foreach (eq_EnquiryChild oDeletedDetail in oDeletedRecords)
                    {
                        oEnquiresFromShelf.eq_EnquiryChild.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (eq_EnquiryChild oNewEnquiryChild in NewEqChildList)
                    {
                        oEnquiresFromShelf.eq_EnquiryChild.Add(oNewEnquiryChild);
                    }

                   
                }

          
                object orefEnquires = oEnquiresFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(eq_Enquiry), omValues, ref orefEnquires);
                _oEntities.SaveChanges();
                oResult.Success = true;
                oResult.Message = oEnquires.Pk_Enquiry.ToString();
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                eq_Enquiry oEnquires = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(eq_Enquiry), omValues) as eq_Enquiry;
                eq_Enquiry oEnquiresFromShelf = _oEntities.eq_Enquiry.Where(p => p.Pk_Enquiry == oEnquires.Pk_Enquiry).Single();
                object orefEnquires = oEnquiresFromShelf;
                _oEntities.DeleteObject(oEnquiresFromShelf);
                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public object GetRaw()
        {
            return oeq_Enquiry;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public Dictionary<int, double> GetMonthWiseEnquiryCount()
        {

            Dictionary<int, double> oResult = new Dictionary<int, double>();

            for (int i = 1; i <= 12; i++)
            {
                int Count = _oEntities.eq_Enquiry.Where(p => p.Date.Month == i).Count();

                oResult.Add(i, Count);
            }

            return oResult;
        }
        public Dictionary<string, string> EnquiryApproval()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int tannent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            IEnumerable<eq_Enquiry> oEnquiry = _oEntities.eq_Enquiry.Where(p => p.wfState.State == "New" && p.Fk_Tanent == tannent);
            int s = 0;
            foreach (eq_Enquiry oEq in oEnquiry)
            {
                oResult.Add(s.ToString(), oEq.Pk_Enquiry + " - " + DateTime.Parse(oEq.Date.ToString()).ToString("dd/MM/yyyy") + " - " + oEq.gen_Customer.CustomerName);
                s++;
            }
            return oResult;
        }
        #endregion Methods
    }
}

