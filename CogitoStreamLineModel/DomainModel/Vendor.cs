﻿//Name          : Vendor Class
//Description   : Contains the Country class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 07/08/2015
//Crh Number    : SL0007
//Modifications : 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data.Objects.DataClasses;
using WebGareCore.CommonObjects;
using CogitoStreamLineModel;
using Newtonsoft.Json;
using System.Web;

namespace CogitoStreamLineModel.DomainModel
{
    public class Vendor : IDomainObject
    {
        #region Variables
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_Vendor ogen_Vendor = null;
        private bool disposed = false;
        #endregion Variables

        #region Properties
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return ogen_Vendor;
            }
            set
            {
                ogen_Vendor = value as gen_Vendor;
            }
        }
        public decimal ID
        {
            get
            {
                return ogen_Vendor.Pk_Vendor;
            }
            set
            {
                ogen_Vendor = _oEntites.gen_Vendor.Where(p => p.Pk_Vendor == value).Single();
            }
        }
        #endregion Properties

        #region Methods
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Vendor = null;
            string sVendorname = null;
            string sVendorType = null;
            string sVendorContact = null;
            string sLandLine = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Vendor> oVendors = null;

            try { sPk_Vendor = p_Params.Single(p => p.ParameterName == "Pk_Vendor").ParameterValue; }
            catch { }
            try { sVendorname = p_Params.Single(p => p.ParameterName == "VendorName").ParameterValue; }
            catch { }
            try { sVendorType = p_Params.Single(p => p.ParameterName == "VendorType").ParameterValue; }
            catch { }
            try { sVendorContact = p_Params.Single(p => p.ParameterName == "VendorContact").ParameterValue; }
            catch { }
            try { sLandLine = p_Params.Single(p => p.ParameterName == "LandLine").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oVendors = _oEntites.gen_Vendor;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Vendor))
                {
                    oVendors = oVendors.Where(p => p.Pk_Vendor == decimal.Parse(sPk_Vendor));
                }
                if (!string.IsNullOrEmpty(sVendorname))
                {
                    oVendors = oVendors.Where(p => p.VendorName.IndexOf(sVendorname, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sVendorType))
                {
                    oVendors = oVendors.Where(p => p.VendorType.IndexOf(sVendorType, StringComparison.OrdinalIgnoreCase) >= 0);
                }
                if (!string.IsNullOrEmpty(sVendorContact))
                {
                    oVendors = oVendors.Where(p => p.OfficeContactNumber == Convert.ToDecimal(sVendorContact));
                }
                if (!string.IsNullOrEmpty(sLandLine))
                {
                    oVendors = oVendors.Where(p => p.LandLine == Convert.ToDecimal(sLandLine));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oVendors.Count();
           
            oVendors = oVendors.OrderBy(p => p.VendorName);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oVendors = oVendors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredUsers = oVendors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredUsers;

            return oSearchResult;
        }
        public SearchResult SearchContacts(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            string sPk_Vendor = null;
            string sContactPersonName = null;
            IEnumerable<vw_VendorDetails> oCustomers = null;

            try { sPk_Vendor = p_Params.Single(p => p.ParameterName == "Pk_Vendor").ParameterValue; }
            catch { }
            try { sContactPersonName = p_Params.Single(p => p.ParameterName == "ContactPersonName").ParameterValue; }
            catch { }

            oCustomers = _oEntites.vw_VendorDetails;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Vendor))
                {
                    oCustomers = oCustomers.Where(p => p.Pk_Vendor == decimal.Parse(sPk_Vendor));
                }
                if (!string.IsNullOrEmpty(sContactPersonName))
                {
                    oCustomers = oCustomers.Where(p => p.ContactPersonName == sContactPersonName);
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oCustomers.Count();
            oCustomers.OrderBy(p => p.Pk_Vendor);

            List<EntityObject> oFilteredCustomers = oCustomers.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredCustomers;

            return oSearchResult;
        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Vendor oNewVendor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Vendor), omValues) as gen_Vendor;

                if (omValues.ContainsKey("Contacts"))
                {
                    string sContacts = omValues["Contacts"].ToString();
                    object[] Contacts = JsonConvert.DeserializeObject<object[]>(sContacts);

                    if (Contacts!=null)
                    {
                    foreach (object oItem in Contacts)
                    {
                        gen_VendorContacts oContact = new gen_VendorContacts();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oContact.Fk_ContactPerson = decimal.Parse(oProperties["id"]);
                        oNewVendor.gen_VendorContacts.Add(oContact);
                    }
                    }
                }

                else
                { }
                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);
                    if (Materials != null)
                    {
                        foreach (object oItem in Materials)
                        {
                            Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                            Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());
                            gen_VendorMaterials oVendorMaterials = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_VendorMaterials), oDataProperties) as gen_VendorMaterials;

                            oNewVendor.gen_VendorMaterials.Add(oVendorMaterials);
                        }
                    }
                }
                else
                {
                }
                oNewVendor.Fk_Tanent = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                _oEntites.AddTogen_Vendor(oNewVendor);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();
            try
            {
                gen_Vendor oVendor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Vendor), omValues) as gen_Vendor;
                gen_Vendor oVendorFromShelf = _oEntites.gen_Vendor.Where(p => p.Pk_Vendor == oVendor.Pk_Vendor).Single();

                oVendorFromShelf.gen_VendorContacts.Clear();

                if (omValues.ContainsKey("Contacts"))
                {
                    string sContacts = omValues["Contacts"].ToString();
                    object[] Contacts = JsonConvert.DeserializeObject<object[]>(sContacts);

                    foreach (object oItem in Contacts)
                    {
                        gen_VendorContacts oContact = new gen_VendorContacts();
                        Dictionary<string, string> oProperties = JsonConvert.DeserializeObject<Dictionary<string, string>>(oItem.ToString());
                        oContact.Fk_ContactPerson = decimal.Parse(oProperties["id"]);
                        oVendorFromShelf.gen_VendorContacts.Add(oContact);
                    }
                }
                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    List<decimal> UpdatedPk = new List<decimal>();
                    List<gen_VendorMaterials> NewVendorMaterialsList = new List<gen_VendorMaterials>();
                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                        if (oDataProperties2["Pk_VendorMaterial"] == null || oDataProperties2["Pk_VendorMaterial"].ToString() == "")
                        {
                            gen_VendorMaterials ogen_VendorMaterials
                                = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_VendorMaterials), oDataProperties2) as gen_VendorMaterials;

                            NewVendorMaterialsList.Add(ogen_VendorMaterials);
                        }
                        else
                        {
                            //Handel Update here
                            decimal dPkVendorMaterialId = Convert.ToDecimal(oDataProperties2["Pk_VendorMaterial"]);

                            gen_VendorMaterials ogen_VendorMaterialsFromShelf = _oEntites.gen_VendorMaterials.Where(p => p.Pk_VendorMaterial == dPkVendorMaterialId).Single();


                            object ogen_VendorMaterials
                                                = _oEntites.gen_VendorMaterials.Where(p => p.Pk_VendorMaterial == dPkVendorMaterialId).First();


                            WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_VendorMaterials), oDataProperties2, ref ogen_VendorMaterials);
                            UpdatedPk.Add(dPkVendorMaterialId);
                        }
                    }

                    if (omValues.ContainsKey("LandLine"))
                    { 
                        
                        if (omValues["LandLine"] == "")
                        {
                            omValues["LandLine"] = 0;           
                        }
                    }
                    if (omValues.ContainsKey("MobileNumber"))
                    {

                        if (omValues["MobileNumber"] == "")
                        {
                            omValues["MobileNumber"] = 0;
                        }
                    }
                    if (omValues.ContainsKey("OfficeContactNumber"))
                    {

                        if (omValues["OfficeContactNumber"] == "")
                        {
                            omValues["OfficeContactNumber"] = 0;
                        }
                    }
                    // Handeling Deleted Records
                    List<gen_VendorMaterials> oDeletedRecords = oVendorFromShelf.gen_VendorMaterials.Where(p => !UpdatedPk.Contains(p.Pk_VendorMaterial)).ToList();

                    foreach (gen_VendorMaterials oDeletedDetail in oDeletedRecords)
                    {
                        oVendorFromShelf.gen_VendorMaterials.Remove(oDeletedDetail);
                    }

                    //Add new elements
                    foreach (gen_VendorMaterials oNewProductDetail in NewVendorMaterialsList)
                    {
                        oVendorFromShelf.gen_VendorMaterials.Add(oNewProductDetail);
                    }
                }
                object orefVendor = oVendorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Vendor), omValues, ref orefVendor);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Vendor oVendor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Vendor), omValues) as gen_Vendor;
                gen_Vendor oVendorFromShelf = _oEntites.gen_Vendor.Where(p => p.Pk_Vendor == oVendor.Pk_Vendor).Single();

                if (omValues.ContainsKey("MaterialData"))
                {
                    string sMaterials = omValues["MaterialData"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        decimal PkVendorMaterial = Convert.ToInt32(oDataProperties["Pk_VendorMaterial"]);
                        gen_VendorMaterials oVendorMaterialsFromShelf = _oEntites.gen_VendorMaterials.Where(p => p.Pk_VendorMaterial == PkVendorMaterial).Single();
                        _oEntites.DeleteObject(oVendorMaterialsFromShelf);
                    }
                }
                List<gen_ContactPerson> ovContacts = oVendorFromShelf.gen_VendorContacts.Select(p => p.gen_ContactPerson).ToList();
                oVendorFromShelf.gen_VendorContacts.Clear();

                foreach (gen_ContactPerson oContcat in ovContacts)
                {
                    _oEntites.DeleteObject(oContcat);
                }
                _oEntites.DeleteObject(oVendorFromShelf);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }
        public object GetRaw()
        {
            return ogen_Vendor;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion Methods
    }
}
