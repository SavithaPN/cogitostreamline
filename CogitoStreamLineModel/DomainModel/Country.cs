﻿
//Name          : Country Class
//Description   : Contains the Country class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0010
//Modifications : 
 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;


namespace CogitoStreamLineModel.DomainModel
{
    public class Country : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private gen_Country oCountry = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_Country = null;
            string sCountryName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Country> oCountrys = null;

            try { sPk_Country = p_Params.Single(p => p.ParameterName == "Pk_Country").ParameterValue; }
            catch { }
            try { sCountryName = p_Params.Single(p => p.ParameterName == "CountryName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oCountrys = _oEntites.gen_Country;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Country))
                {
                    oCountrys = oCountrys.Where(p => p.Pk_Country == decimal.Parse(sPk_Country));
                }
                if (!string.IsNullOrEmpty(sCountryName))
                {
                    //oCountrys = oCountrys.Where(p => p.CountryName.IndexOf(sCountryName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oCountrys = oCountrys.Where(p => p.CountryName.ToLower() == (sCountryName.ToLower().ToString().Trim()));
                }
            }
            catch (System.NullReferenceException)
            {

            }

            oSearchResult.RecordCount = oCountrys.Count();

           oCountrys= oCountrys.OrderBy(p => p.CountryName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oCountrys = oCountrys.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oCountrys.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchCountryName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_Country> oCountryName = null;
            oCountryName = _oEntites.gen_Country;

            oSearchResult.RecordCount = oCountryName.Count();
            oCountryName.OrderBy(p => p.Pk_Country);

            List<EntityObject> oFilteredItem = oCountryName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Country oNewCountrys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Country), omValues) as gen_Country;

                _oEntites.AddTogen_Country(oNewCountrys);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Country oCountrys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Country), omValues) as gen_Country;
                gen_Country oCountrysFromShelf = _oEntites.gen_Country.Where(p => p.Pk_Country == oCountrys.Pk_Country).Single();
                object orefItems = oCountrysFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Country), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Country oCountrys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Country), omValues) as gen_Country;
                gen_Country oCountrysFromShelf = _oEntites.gen_Country.Where(p => p.Pk_Country == oCountrys.Pk_Country).Single();
                object orefItems = oCountrysFromShelf;
                //_oEntites.DeleteObject(oCountrysFromShelf);

                IEnumerable<gen_State> ogenState = _oEntites.gen_State.Where(p => p.Country == oCountrys.Pk_Country);
                if (ogenState.Count() > 0)
                {
                    oResult.Success = false;
                    oResult.Message = "Cannot Delete as this Country is Referenced else where";
                }
                else
                {
                    _oEntites.DeleteObject(oCountrysFromShelf);

                    _oEntites.SaveChanges();
                    oResult.Success = true;
                }
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oCountry;

            }
            set
            {
                oCountry = value as gen_Country;
            }
        }

        public decimal ID
        {
            get
            {
                return oCountry.Pk_Country;
            }
            set
            {
                oCountry = _oEntites.gen_Country.Where(p => p.Pk_Country == value).Single();
            }
        }

        public object GetRaw()
        {
            return oCountry;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


