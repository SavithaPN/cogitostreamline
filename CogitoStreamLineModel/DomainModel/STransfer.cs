﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
using System.Web;


namespace CogitoStreamLineModel.DomainModel
{
    public class STransfer : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private StockTransfer oStockTransfer = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_TransferID = null;
            string sTransferDate = null;
            string sFk_FromBranchID = null;
            string sFk_ToBranchID = null;
            string sTransferQty = null;
            string sFk_Material = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<StockTransfer> oStockTransfers = null;

            try { sPk_TransferID = p_Params.Single(p => p.ParameterName == "Pk_TransferID").ParameterValue; }
            catch { }
            try { sTransferDate = p_Params.Single(p => p.ParameterName == "TransferDate").ParameterValue; }
            catch { }
            try { sFk_FromBranchID = p_Params.Single(p => p.ParameterName == "Fk_FromBranchID").ParameterValue; }
            catch { }
            try { sFk_ToBranchID = p_Params.Single(p => p.ParameterName == "Fk_ToBranchID").ParameterValue; }
            catch { }
            try { sTransferQty = p_Params.Single(p => p.ParameterName == "TransferQty").ParameterValue; }
            catch { }
            try { sFk_Material = p_Params.Single(p => p.ParameterName == "Fk_Material").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oStockTransfers = _oEntites.StockTransfer;
            try
            {
                if (!string.IsNullOrEmpty(sPk_TransferID))
                {
                    oStockTransfers = oStockTransfers.Where(p => p.Pk_TransferID == decimal.Parse(sPk_TransferID));
                }
                if (!string.IsNullOrEmpty(sTransferDate))
                {
                    oStockTransfers = oStockTransfers.Where(p => p.TransferDate == Convert.ToDateTime(sTransferDate));
                }
                if (!string.IsNullOrEmpty(sFk_FromBranchID))
                {
                    oStockTransfers = oStockTransfers.Where(p => p.Fk_FromBranchID == decimal.Parse(sFk_FromBranchID));
                }
                if (!string.IsNullOrEmpty(sFk_ToBranchID))
                {
                    oStockTransfers = oStockTransfers.Where(p => p.Fk_ToBranchID == decimal.Parse(sFk_ToBranchID));
                }
                if (!string.IsNullOrEmpty(sTransferQty))
                {
                    oStockTransfers = oStockTransfers.Where(p => p.TransferQty == decimal.Parse(sTransferQty));
                }
                if (!string.IsNullOrEmpty(sFk_Material))
                {
                    oStockTransfers = oStockTransfers.Where(p => p.Fk_Material == decimal.Parse(sFk_Material));
                }
               
            }
            catch (System.NullReferenceException)
            {

            }

            oSearchResult.RecordCount = oStockTransfers.Count();

            oStockTransfers.OrderBy(p => p.Pk_TransferID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oStockTransfers = oStockTransfers.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oStockTransfers.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }
        public SearchResult SearchST(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            //string CheckDate = "", string Vendor = "", string InvoiceNo = "", string MaterialInward = "", 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_Material = null;
            //string sMaterialIndentDate = null;
            string sDate = null;
            string sVendor = null;
            string sInvoiceNo = null;
            string sMaterialIndent = null;
            string sMaterial = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<vw_StockLedger> oStockCheck = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_QualityCheck").ParameterValue; }
            catch { }
            try { sDate = p_Params.Single(p => p.ParameterName == "DDate").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            catch { }
            try { sInvoiceNo = p_Params.Single(p => p.ParameterName == "Invoice").ParameterValue; }
            catch { }
            try { sMaterialIndent = p_Params.Single(p => p.ParameterName == "MaterialInward").ParameterValue; }
            catch { }
            try { sMaterial = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oStockCheck = _oEntites.vw_StockLedger;

            try
            {
                var tanentid = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
                oStockCheck = oStockCheck.Where(p => p.Fk_BranchID == branchid && p.Fk_Tanent == tanentid);
                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oStockCheck = oStockCheck.Where(p => p.Pk_Material == decimal.Parse(sPk_Material));
                }
              
                if (!string.IsNullOrEmpty(sMaterial))
                {
                    oStockCheck = oStockCheck.Where(p => p.Name.IndexOf(sMaterial, StringComparison.OrdinalIgnoreCase) >= 0);
                }
              
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oStockCheck.Count();
            oStockCheck = oStockCheck.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oStockCheck = oStockCheck.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oStockCheck.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }
        public SearchResult SearchTransferName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<StockTransfer> oStockName = null;
            oStockName = _oEntites.StockTransfer;

            oSearchResult.RecordCount = oStockName.Count();
            oStockName.OrderBy(p => p.Pk_TransferID);

            List<EntityObject> oFilteredItem = oStockName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                StockTransfer oNewStockTransfers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(StockTransfer), omValues) as StockTransfer;
                var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                var frombranch=Convert.ToInt32(omValues["Fk_FromBranchID"]);
                var tobranch=Convert.ToInt32(omValues["Fk_ToBranchID"]);

                Stock oFromStockVal = _oEntites.Stocks.Where(p => p.Fk_Material == oNewStockTransfers.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == frombranch).Single();
                Stock oToStockVal = _oEntites.Stocks.Where(p => p.Fk_Material == oNewStockTransfers.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == tobranch).Single();

                if (Convert.ToInt32(oFromStockVal.Quantity)-Convert.ToInt32(omValues["TransferQty"]) >= 0)
                {
                    oFromStockVal.Quantity=Convert.ToDecimal(oFromStockVal.Quantity)-Convert.ToInt32(omValues["TransferQty"]);
                    oToStockVal.Quantity = Convert.ToDecimal(oToStockVal.Quantity) + Convert.ToInt32(omValues["TransferQty"]);
                }

                _oEntites.AddToStockTransfer(oNewStockTransfers);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                StockTransfer oTransfers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(StockTransfer), omValues) as StockTransfer;
                StockTransfer oTransfersFromShelf = _oEntites.StockTransfer.Where(p => p.Pk_TransferID == oTransfers.Pk_TransferID).Single();
                
                var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                var frombranch = Convert.ToInt32(omValues["Fk_FromBranchID"]);
                var tobranch = Convert.ToInt32(omValues["Fk_ToBranchID"]);

                Stock oFromStockVal = _oEntites.Stocks.Where(p => p.Fk_Material == oTransfersFromShelf.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == frombranch).Single();
                Stock oToStockVal = _oEntites.Stocks.Where(p => p.Fk_Material == oTransfersFromShelf.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == tobranch).Single();

                if ((Convert.ToInt32(oFromStockVal.Quantity)+Convert.ToInt32(oToStockVal.Quantity)) - Convert.ToInt32(omValues["TransferQty"]) >= 0)
                {
                    oFromStockVal.Quantity = Convert.ToDecimal(oFromStockVal.Quantity) + Convert.ToInt32(oTransfersFromShelf.TransferQty);
                    oFromStockVal.Quantity = Convert.ToDecimal(oFromStockVal.Quantity) - Convert.ToInt32(omValues["TransferQty"]);
                    oToStockVal.Quantity = Convert.ToDecimal(oToStockVal.Quantity) - Convert.ToInt32(oTransfersFromShelf.TransferQty);
                    oToStockVal.Quantity = Convert.ToDecimal(oToStockVal.Quantity) + Convert.ToInt32(omValues["TransferQty"]);
                }
                object orefItems = oTransfersFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(StockTransfer), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                StockTransfer oTransfers = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(StockTransfer), omValues) as StockTransfer;
                StockTransfer oTransfersFromShelf = _oEntites.StockTransfer.Where(p => p.Pk_TransferID == oTransfers.Pk_TransferID).Single();
                object orefItems = oTransfersFromShelf;
                //_oEntites.DeleteObject(oTransfersFromShelf);

                _oEntites.DeleteObject(oTransfersFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oStockTransfer;

            }
            set
            {
                oStockTransfer = value as StockTransfer;
            }
        }

        public decimal ID
        {
            get
            {
                return oStockTransfer.Pk_TransferID;
            }
            set
            {
                oStockTransfer = _oEntites.StockTransfer.Where(p => p.Pk_TransferID == value).Single();
            }
        }
        public decimal PendingValue(decimal Branch_ID, decimal Tanent_ID, decimal Fk_Material)
        {
           Stock dPendingValue = _oEntites.Stocks.Where(p => p.Fk_Tanent == Tanent_ID && p.Fk_Material == Fk_Material && p.Fk_BranchID == Branch_ID).First();

            return decimal.Parse(dPendingValue.Quantity.ToString());
        }
        public object GetRaw()
        {
            return oStockTransfer;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


