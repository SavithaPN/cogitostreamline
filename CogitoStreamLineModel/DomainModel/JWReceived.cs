﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class JWReceived : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private JobWorkReceivables oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Color = null;
            string sJCNo = null;
            string sVendor = null;
            string sStartIndex = null;
            string sFromDate = null;
            string sToDate = null;

            string sPageSize = null;
            string sSorting = null;
            IEnumerable<JobWorkReceivables> oColors = null;

            try { sPk_Color = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            try { sJCNo = p_Params.Single(p => p.ParameterName == "JCNo").ParameterValue; }
            catch { }
            try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.JobWorkReceivables;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Color))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_Color));
                }
                if (!string.IsNullOrEmpty(sJCNo))
                {
                    oColors = oColors.Where(p => p.JW_JobCardMaster.Pk_JobCardID == decimal.Parse(sJCNo));
                }
                
                if (!string.IsNullOrEmpty(sFromDate))
                {

                    DateTime SFrom = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oColors = oColors.Where(p => (p.RecdDate >= Convert.ToDateTime(SFrom)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    DateTime STo = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);



                    oColors = oColors.Where(p => (p.RecdDate <= Convert.ToDateTime(STo)));
                }
                if (!string.IsNullOrEmpty(sVendor))
                {
                    oColors = oColors.Where(p => p.JW_JobCardMaster.gen_Vendor.VendorName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchColorName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<JobWorkReceivables> oColorName = null;
            oColorName = _oEntites.JobWorkReceivables;

            oSearchResult.RecordCount = oColorName.Count();
            oColorName.OrderBy(p => p.Pk_ID);

            List<EntityObject> oFilteredItem = oColorName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                JobWorkReceivables oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobWorkReceivables), omValues) as JobWorkReceivables;
                oNewColor.Fk_Status = 1;


                oNewColor.RecdDate = Convert.ToDateTime(omValues["RecdDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                _oEntites.AddToJobWorkReceivables(oNewColor);

                _oEntites.SaveChanges();

                ///////
                var TypePrd = int.Parse(oNewColor.TypeofPrd);
                //var PrdID1 = oNewColor.Inv_MaterialCategory.Inv_Material;
                var prd = oNewColor.Pk_ID;
                var PrdID = oNewColor.PrdName;
                var OutSrcID=oNewColor.Fk_JobCardID;

                var RecdWt=oNewColor.RecdPrdWeight;
                
                decimal LayerWt=0;
                //LayerWt = oNewColor.LayerWt;
                //decimal Len=0;
                //decimal Width=0;
               
                //oNewColor.in
                ///////////
                //int RCount = _oEntites.OutsourcingChild.Where(p => p.Fk_PkSrcID == OutSrcID).Count();
                //if (RCount > 0)
                //{
                //    OutsourcingChild oOutDet = _oEntites.OutsourcingChild.Where(p => p.Fk_PkSrcID == OutSrcID).Single();

                //    LayerWt = Convert.ToDecimal(oOutDet.LayerWt);

                //}

    //            ///////////
    ////          
                decimal ExpNos = 0;
              //  ExpNos = Convert.ToDecimal(RecdWt) / Convert.ToDecimal(LayerWt);

                if (TypePrd == 1)   // semi finished goods
                {
                    //Fk_MatCatID

                    ExpNos = Convert.ToDecimal(omValues["AppNos"]);
                    int RCount1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == PrdID).Count();

                    if (RCount1 == 0)
                    {
                        Semi_FinishedGoodsStock oStockVal = new Semi_FinishedGoodsStock();
                        oStockVal.Fk_MatCatID = PrdID;


                        oStockVal.Stock = Convert.ToDecimal(ExpNos);
                        oStockVal.Fk_JobCardID = OutSrcID;
                        // oStockVal.Quantity = Convert.ToDecimal(oNewPOrderD.Quantity);


                        _oEntites.AddToSemi_FinishedGoodsStock(oStockVal);
                        _oEntites.SaveChanges();
                    }
                    else
                    {
                        int RCount = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == PrdID).Count();

                        if (RCount > 0)
                        {
                            Semi_FinishedGoodsStock OStockVal1 = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_MatCatID == PrdID).FirstOrDefault();

                            OStockVal1.Stock = OStockVal1.Stock + ExpNos;
                            _oEntites.SaveChanges();
                        }
                        else
                        {
                            Semi_FinishedGoodsStock OStockVal1 = new Semi_FinishedGoodsStock();
                            OStockVal1.Fk_MatCatID = PrdID;
                            OStockVal1.Stock = ExpNos;
                            OStockVal1.Fk_JobCardID = OutSrcID;
                        }
                    }
                    //Semi_FinishedGoodsStock OStockVal = _oEntites.Semi_FinishedGoodsStock.Where(p => p.Fk_Material == oOutDetail.Fk_Material && p.RollNo == oOutDetail.ReelNo).Single();
                }
                else

                {

                    ExpNos =Convert.ToDecimal(omValues["AppNos"]);
                    int RCount2 = _oEntites.FinishedGoodsStock.Where(p => p.Fk_BoxID == PrdID).Count();

                    if (RCount2 == 0)
                    {
                        FinishedGoodsStock oStockVal = new FinishedGoodsStock();
                        oStockVal.Fk_BoxID = PrdID;


                        oStockVal.Stock = Convert.ToDecimal(ExpNos);
                        // oStockVal.Quantity = Convert.ToDecimal(oNewPOrderD.Quantity);


                        _oEntites.AddToFinishedGoodsStock(oStockVal);
                        _oEntites.SaveChanges();
                    }
                    else
                    {
                        FinishedGoodsStock OStockVal1 = _oEntites.FinishedGoodsStock.Where(p => p.Fk_BoxID== PrdID).Single();

                        OStockVal1.Stock = OStockVal1.Stock + ExpNos;
                        _oEntites.SaveChanges();
                    }
                }




                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                JobWorkReceivables oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobWorkReceivables), omValues) as JobWorkReceivables;
                JobWorkReceivables oColorFromShelf = _oEntites.JobWorkReceivables.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(JobWorkReceivables), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                JobWorkReceivables oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobWorkReceivables), omValues) as JobWorkReceivables;
                JobWorkReceivables oColorFromShelf = _oEntites.JobWorkReceivables.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as JobWorkReceivables;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_ID;
            }
            set
            {
                oColor = _oEntites.JobWorkReceivables.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


