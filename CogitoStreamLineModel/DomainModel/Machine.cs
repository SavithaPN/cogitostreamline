﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class Machine : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private gen_Machine oMachine = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Machine = null;
            string sLocation = null;
            string sName = null;
            string sCapacity_Length = null;
            string sCapacity_Width = null;
            string sCapacity_Other = null;
            string sAdditional_Information = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_Machine> oMachnines = null;

            try { sPk_Machine = p_Params.Single(p => p.ParameterName == "Pk_Machine").ParameterValue; }
            catch { }
            try { sLocation = p_Params.Single(p => p.ParameterName == "Location").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sCapacity_Length = p_Params.Single(p => p.ParameterName == "Capacity_Length").ParameterValue; }
            catch { }
            try { sCapacity_Width = p_Params.Single(p => p.ParameterName == "Capacity_Width").ParameterValue; }
            catch { }
            try { sCapacity_Other = p_Params.Single(p => p.ParameterName == "Capacity_Other").ParameterValue; }
            catch { }
            try { sAdditional_Information = p_Params.Single(p => p.ParameterName == "Additional_Information").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMachnines = _oEntites.gen_Machine;
            try
            {
                if (!string.IsNullOrEmpty(sPk_Machine))
                {
                    oMachnines = oMachnines.Where(p => p.Pk_Machine == decimal.Parse(sPk_Machine));
                }
                if (!string.IsNullOrEmpty(sLocation))
                {
                    oMachnines = oMachnines.Where(p => p.Location.IndexOf(sLocation, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oMachnines = oMachnines.Where(p => p.Name.IndexOf(sName, StringComparison.OrdinalIgnoreCase) >= 0);

                }
                
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oMachnines.Count();
            oMachnines.OrderBy(p => p.Pk_Machine);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oMachnines = oMachnines.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oMachnines.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchMachineName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_Machine> oMachineName = null;
            oMachineName = _oEntites.gen_Machine;

            oSearchResult.RecordCount = oMachineName.Count();
            oMachineName.OrderBy(p => p.Pk_Machine);

            List<EntityObject> oFilteredItem = oMachineName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Machine oNewMachines = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Machine), omValues) as gen_Machine;

                _oEntites.AddTogen_Machine(oNewMachines);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Machine oMachines = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Machine), omValues) as gen_Machine;
                gen_Machine oMachinesFromShelf = _oEntites.gen_Machine.Where(p => p.Pk_Machine == oMachines.Pk_Machine).Single();
                object orefItems = oMachinesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_Machine), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_Machine oMachines = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_Machine), omValues) as gen_Machine;
                gen_Machine oMachinesFromShelf = _oEntites.gen_Machine.Where(p => p.Pk_Machine == oMachines.Pk_Machine).Single();
                object orefItems = oMachinesFromShelf;
                _oEntites.DeleteObject(oMachinesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oMachine;

            }
            set
            {
                oMachine = value as gen_Machine;
            }
        }

        public decimal ID
        {
            get
            {
                return oMachine.Pk_Machine;
            }
            set
            {
                oMachine = _oEntites.gen_Machine.Where(p => p.Pk_Machine == value).Single();
            }
        }

        public object GetRaw()
        {
            return oMachine;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


