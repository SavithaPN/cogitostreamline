﻿


//Name          : Acoount Head Class
//Description   : Contains the AccountHaad class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : Nagesh V Rao
//Date 	        : 22/10/2015
//Crh Number    : 
//Modifications : 



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
namespace CogitoStreamLineModel.DomainModel
{
    public class AccountHead : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private acc_AccountHeads oHead = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_AccountHeadId = null;
            string sHeadName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<acc_AccountHeads> oHeads = null;

            try { sPk_AccountHeadId = p_Params.Single(p => p.ParameterName == "Pk_AccountHeadId").ParameterValue; }
            catch { }
            try { sHeadName = p_Params.Single(p => p.ParameterName == "HeadName").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oHeads = _oEntites.acc_AccountHeads;
            try
            {
                if (!string.IsNullOrEmpty(sPk_AccountHeadId))
                {
                    oHeads = oHeads.Where(p => p.Pk_AccountHeadId == decimal.Parse(sPk_AccountHeadId));
                }
                if (!string.IsNullOrEmpty(sHeadName))
                {
                    oHeads = oHeads.Where(p => p.HeadName.IndexOf(sHeadName.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0);
                    //oHeadNames = oHeadNames.Where(p => p.PaperTypeName.ToLower() == (sPaperTypeName.ToLower().ToString().Trim()));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oHeads.Count();

            oHeads.OrderBy(p => p.Pk_AccountHeadId);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oHeads = oHeads.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oHeads.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchAccountHeadName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<acc_AccountHeads> oHeadName = null;
            oHeadName = _oEntites.acc_AccountHeads;

            oSearchResult.RecordCount = oHeadName.Count();
            oHeadName.OrderBy(p => p.Pk_AccountHeadId);

            List<EntityObject> oFilteredItem = oHeadName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                acc_AccountHeads oNewHeadNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(acc_AccountHeads), omValues) as acc_AccountHeads;

                _oEntites.AddToacc_AccountHeads(oNewHeadNames);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                acc_AccountHeads oHeadNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(acc_AccountHeads), omValues) as acc_AccountHeads;
                acc_AccountHeads oHeadNamesFromShelf = _oEntites.acc_AccountHeads.Where(p => p.Pk_AccountHeadId == oHeadNames.Pk_AccountHeadId).Single();
                object orefItems = oHeadNamesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(acc_AccountHeads), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                acc_AccountHeads oHeadNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(acc_AccountHeads), omValues) as acc_AccountHeads;
                acc_AccountHeads oHeadNamesFromShelf = _oEntites.acc_AccountHeads.Where(p => p.Pk_AccountHeadId == oHeadNames.Pk_AccountHeadId).Single();
                object orefItems = oHeadNamesFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oHeadNamesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oHead;

            }
            set
            {
                oHead = value as acc_AccountHeads;
            }
        }

        public decimal ID
        {
            get
            {
                return oHead.Pk_AccountHeadId;
            }
            set
            {
                oHead = _oEntites.acc_AccountHeads.Where(p => p.Pk_AccountHeadId == value).Single();
            }
        }

        public object GetRaw()
        {
            return oHead;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}





