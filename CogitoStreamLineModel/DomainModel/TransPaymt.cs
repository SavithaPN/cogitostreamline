﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class TransPaymt : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private Trans_Payment oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_Color = null;
            string sDate = null;
            string sInvno = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Trans_Payment> oColors = null;

            try { sPk_Color = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            try { sDate = p_Params.Single(p => p.ParameterName == "Pay_Date").ParameterValue; }
            catch { }
            try { sInvno = p_Params.Single(p => p.ParameterName == "ChequeNo").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntites.Trans_Payment;

            try
            {
                if (!string.IsNullOrEmpty(sPk_Color))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_Color));
                }
                if (!string.IsNullOrEmpty(sDate))
                {
                    oColors = oColors.Where(p => (p.Pay_Date == Convert.ToDateTime(sDate)));
                }
                if (!string.IsNullOrEmpty(sInvno))
                {
                    oColors = oColors.Where(p => p.ChequeNo.IndexOf(sInvno, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchColorName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<Trans_Payment> oColorName = null;
            oColorName = _oEntites.Trans_Payment;

            oSearchResult.RecordCount = oColorName.Count();
            oColorName.OrderBy(p => p.Pk_ID);

            List<EntityObject> oFilteredItem = oColorName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Trans_Payment oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Trans_Payment), omValues) as Trans_Payment;

                decimal FkTrans = Convert.ToDecimal(omValues["Fk_Transporter"]);
                decimal PdAmt = Convert.ToDecimal(omValues["AmtPaid"]);


                decimal tempVal = 0;
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                List<Trans_Invoice> oPrdListChild = new List<Trans_Invoice>();

                oPrdListChild = dc.Trans_Invoice.Where(p => p.BalAmt > 0 && p.Fk_Transporter==FkTrans).Select(p => p).OfType<Trans_Invoice>().OrderBy(p=>p.Pk_ID).ToList();
                int RCount = oPrdListChild.Count();
                int i = 0;
              decimal tempPaidAmt = 0;
                for (i = 0; i < RCount; i++)
                {
                  
                    var Pk_ID=oPrdListChild.ElementAt(i).Pk_ID;
                    decimal BalVal = Convert.ToDecimal(oPrdListChild.ElementAt(i).BalAmt);
                  
                    if (i != 0)
                    {
                        if (tempPaidAmt > BalVal)
                        {

                        }
                        else if (tempPaidAmt< BalVal)
                        {
                            tempVal = BalVal - tempPaidAmt;
                            Trans_Invoice oTrInv1 = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == Pk_ID).Single();
                            oTrInv1.BalAmt = tempVal;
                            _oEntites.SaveChanges();
                        }
                    }
                    else
                    {

                        if (PdAmt > BalVal)
                        {
                            Trans_Invoice oTrInv1 = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == Pk_ID).Single();
                            tempVal = PdAmt - BalVal;
                            tempPaidAmt = PdAmt - BalVal;
                            oTrInv1.BalAmt = 0;
                            _oEntites.SaveChanges();


                        }
                        else
                        {
                            tempVal = BalVal - PdAmt;

                            if (tempVal > 0)
                            {
                                Trans_Invoice oTrInv = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == Pk_ID).Single();
                                oTrInv.BalAmt = tempVal;
                                _oEntites.SaveChanges();
                                i = RCount;
                            }
                            else
                            {
                                Trans_Invoice oTrInv = _oEntites.Trans_Invoice.Where(p => p.Pk_ID == Pk_ID).Single();
                                oTrInv.BalAmt = 0;
                                _oEntites.SaveChanges();
                                i = RCount;
                            }
                        }
                    }

                }


                //Trans_Inv oNewInv = new Trans_Inv();
                //Gen_OrderChild oGenChild = _oEntites.Gen_OrderChild.Where(p => p.Fk_BoxID == dFk_BoxID && p.Fk_OrderID == dFk_Order).SingleOrDefault();

                _oEntites.AddToTrans_Payment(oNewColor);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Trans_Payment oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Trans_Payment), omValues) as Trans_Payment;
                Trans_Payment oColorFromShelf = _oEntites.Trans_Payment.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(Trans_Payment), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                Trans_Payment oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(Trans_Payment), omValues) as Trans_Payment;
                Trans_Payment oColorFromShelf = _oEntites.Trans_Payment.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oColorFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as Trans_Payment;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_ID;
            }
            set
            {
                oColor = _oEntites.Trans_Payment.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


