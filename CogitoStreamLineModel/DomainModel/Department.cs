﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;
namespace CogitoStreamLineModel.DomainModel
{
    public class Department : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private emp_Department oDept = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_DepartmentId = null;
            string sName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<emp_Department> oDepts = null;

            try { sPk_DepartmentId = p_Params.Single(p => p.ParameterName == "Pk_DepartmentId").ParameterValue; }
            catch { }
            try { sName = p_Params.Single(p => p.ParameterName == "Name").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oDepts = _oEntites.emp_Department;
            try
            {
                if (!string.IsNullOrEmpty(sPk_DepartmentId))
                {
                    oDepts = oDepts.Where(p => p.Pk_DepartmentId == decimal.Parse(sPk_DepartmentId));
                }
                if (!string.IsNullOrEmpty(sName))
                {
                    oDepts = oDepts.Where(p => p.Name.IndexOf(sName.ToLower(), StringComparison.OrdinalIgnoreCase) >= 0);
                    //oDeptNames = oDeptNames.Where(p => p.PaperTypeName.ToLower() == (sPaperTypeName.ToLower().ToString().Trim()));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oDepts.Count();

          oDepts=  oDepts.OrderBy(p => p.Name);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oDepts = oDepts.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oDepts.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchDepartmentName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<emp_Department> oDeptName = null;
            oDeptName = _oEntites.emp_Department;

            oSearchResult.RecordCount = oDeptName.Count();
            oDeptName.OrderBy(p => p.Pk_DepartmentId);

            List<EntityObject> oFilteredItem = oDeptName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_Department oNewDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_Department), omValues) as emp_Department;

                _oEntites.AddToemp_Department(oNewDeptNames);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_Department oDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_Department), omValues) as emp_Department;
                emp_Department oDeptNamesFromShelf = _oEntites.emp_Department.Where(p => p.Pk_DepartmentId == oDeptNames.Pk_DepartmentId).Single();
                object orefItems = oDeptNamesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(emp_Department), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_Department oDeptNames = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_Department), omValues) as emp_Department;
                emp_Department oDeptNamesFromShelf = _oEntites.emp_Department.Where(p => p.Pk_DepartmentId == oDeptNames.Pk_DepartmentId).Single();
                object orefItems = oDeptNamesFromShelf;
                //_oEntites.DeleteObject(oEmployeesFromShelf);

                _oEntites.DeleteObject(oDeptNamesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oDept;

            }
            set
            {
                oDept = value as emp_Department;
            }
        }

        public decimal ID
        {
            get
            {
                return oDept.Pk_DepartmentId;
            }
            set
            {
                oDept = _oEntites.emp_Department.Where(p => p.Pk_DepartmentId == value).Single();
            }
        }

        public object GetRaw()
        {
            return oDept;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
