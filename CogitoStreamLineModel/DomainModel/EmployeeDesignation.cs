﻿//Name          : Employee Designation Class
//Description   : Contains the Employee Designation class definition, every page in the application will be instance of this class with following
//                 1. Communication 2. Data binding 3. Loading preferences
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 21/10/2015
//Crh Number    : 
//Modifications : 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class EmployeeDesignation : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private emp_EmployeeDesignation oemp_EmployeeDesignation = null;

        //public SearchResult Search1(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();
        //    IEnumerable<emp_EmployeeDesignation> oEmployeeDesignations = null;
        //    oEmployeeDesignations = _oEntites.emp_EmployeeDesignation;

        //    oSearchResult.RecordCount = oEmployeeDesignations.Count();
        //    oEmployeeDesignations.OrderBy(p => p.DesignationName);

        //    List<EntityObject> oFilteredVendorRelationShip = oEmployeeDesignations.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredVendorRelationShip;

        //    return oSearchResult;
        //}
        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for EmployeeDesignation based on 
            //Id and DesignationName
            string sPk_EmployeeDesignationId = null;
            string sDesignationName = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<emp_EmployeeDesignation> oEmployeeDesignations = null;

            try { sPk_EmployeeDesignationId = p_Params.Single(p => p.ParameterName == "Pk_EmployeeDesignationId").ParameterValue; }
            catch { }
            try { sDesignationName = p_Params.Single(p => p.ParameterName == "DesignationName").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oEmployeeDesignations = _oEntites.emp_EmployeeDesignation;

            try
            {
                if (!string.IsNullOrEmpty(sPk_EmployeeDesignationId))
                {
                    oEmployeeDesignations = oEmployeeDesignations.Where(p => p.Pk_EmployeeDesignationId == decimal.Parse(sPk_EmployeeDesignationId));
                }
                if (!string.IsNullOrEmpty(sDesignationName))
                {
                    oEmployeeDesignations = oEmployeeDesignations.Where(p => p.DesignationName.IndexOf(sDesignationName, StringComparison.OrdinalIgnoreCase) >= 0);
                }



            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oEmployeeDesignations.Count();
            oEmployeeDesignations.OrderBy(p => p.DesignationName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oEmployeeDesignations = oEmployeeDesignations.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredEmployeeDesignations = oEmployeeDesignations.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredEmployeeDesignations;

            return oSearchResult;

        }
        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }
        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_EmployeeDesignation oNewEmployeeDesignation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_EmployeeDesignation), omValues) as emp_EmployeeDesignation;
                _oEntites.AddToemp_EmployeeDesignation(oNewEmployeeDesignation);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_EmployeeDesignation oEmployeeDesignation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_EmployeeDesignation), omValues) as emp_EmployeeDesignation;
                emp_EmployeeDesignation oEmployeeDesignationFromShelf = _oEntites.emp_EmployeeDesignation.Where(p => p.Pk_EmployeeDesignationId == oEmployeeDesignation.Pk_EmployeeDesignationId).Single();
                object orefEmployeeDesignation = oEmployeeDesignationFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(emp_EmployeeDesignation), omValues, ref orefEmployeeDesignation);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                emp_EmployeeDesignation oEmployeeDesignation = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(emp_EmployeeDesignation), omValues) as emp_EmployeeDesignation;
                emp_EmployeeDesignation oEmployeeDesignationFromShelf = _oEntites.emp_EmployeeDesignation.Where(p => p.Pk_EmployeeDesignationId == oEmployeeDesignation.Pk_EmployeeDesignationId).Single();
                _oEntites.DeleteObject(oEmployeeDesignationFromShelf);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }
        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oemp_EmployeeDesignation;
            }
            set
            {
                oemp_EmployeeDesignation = value as emp_EmployeeDesignation;
            }
        }
        public decimal ID
        {
            get
            {
                return oemp_EmployeeDesignation.Pk_EmployeeDesignationId;
            }
            set
            {
                oemp_EmployeeDesignation = _oEntites.emp_EmployeeDesignation.Where(p => p.Pk_EmployeeDesignationId == value).Single();
            }
        }
        public object GetRaw()
        {
            return oemp_EmployeeDesignation;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
