﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;
using CogitoStreamLineModel;

namespace CogitoStreamLineModel.DomainModel
{
    public class ActivityPermissions : IDomainObject
    {
        //readonly WebGareCoreEntities2  _oEntites = new WebGareCoreEntities2();
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private ideActivityPermission oActivityPermission = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
             
            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_ActivityPermissions = null;
         
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<ideActivityPermission> oActivityPermissions = null;

            try { sPk_ActivityPermissions = p_Params.Single(p => p.ParameterName == "Pk_ActivityPermissions").ParameterValue; }
            catch { }
         
           
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oActivityPermissions = _oEntites.ideActivityPermission;
            try
            {
                if (!string.IsNullOrEmpty(sPk_ActivityPermissions))
                {
                    oActivityPermissions = oActivityPermissions.Where(p => p.Pk_ActivityPermissions == decimal.Parse(sPk_ActivityPermissions));
                }
               
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oActivityPermissions.Count();
            oActivityPermissions.OrderBy(p => p.Pk_ActivityPermissions);
            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oActivityPermissions = oActivityPermissions.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oActivityPermissions.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;
            return oSearchResult;
        }

        public SearchResult SearchCityName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<ideActivityPermission> oActivityPermissionName = null;
            oActivityPermissionName = _oEntites.ideActivityPermission;

            oSearchResult.RecordCount = oActivityPermissionName.Count();
            oActivityPermissionName.OrderBy(p => p.Pk_ActivityPermissions);

            List<EntityObject> oFilteredItem = oActivityPermissionName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideActivityPermission oNewCitys = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideActivityPermission), omValues) as ideActivityPermission;

                _oEntites.AddToideActivityPermission(oNewCitys);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideActivityPermission oActivityPermissions = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideActivityPermission), omValues) as ideActivityPermission;
                ideActivityPermission oActivityPermissionsFromShelf = _oEntites.ideActivityPermission.Where(p => p.Pk_ActivityPermissions == oActivityPermissions.Pk_ActivityPermissions).Single();
                object orefItems = oActivityPermissionsFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ideActivityPermission), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ideActivityPermission oActivityPermissions = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ideActivityPermission), omValues) as ideActivityPermission;
                ideActivityPermission oActivityPermissionsFromShelf = _oEntites.ideActivityPermission.Where(p => p.Pk_ActivityPermissions == oActivityPermissions.Pk_ActivityPermissions).Single();
                object orefItems = oActivityPermissionsFromShelf;
                _oEntites.DeleteObject(oActivityPermissionsFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oActivityPermission;

            }
            set
            {
                oActivityPermission = value as ideActivityPermission;
            }
        }

        public decimal ID
        {
            get
            {
                return oActivityPermission.Pk_ActivityPermissions;
            }
            set
            {
                oActivityPermission = _oEntites.ideActivityPermission.Where(p => p.Pk_ActivityPermissions == value).Single();
            }
        }

        public object GetRaw()
        {
            return oActivityPermission;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}




