﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace CogitoStreamLineModel.DomainModel
//{
//    class PieceWorks
//    {
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class PieceWorks : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private PartJobs oColor = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sCust = null;
            string sToDate = null;
            string sIssDate = null;
            string sFromDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<PartJobs> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            try { sIssDate = p_Params.Single(p => p.ParameterName == "RecdDate").ParameterValue; }
            catch { }
            try { sCust = p_Params.Single(p => p.ParameterName == "Cust").ParameterValue; }
            catch { }
            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntities.PartJobs;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_CharID));
                }

                //if (!string.IsNullOrEmpty(sIssDate))
                //{
                //    oColors = oColors.Where(p => (p.RecdDate == Convert.ToDateTime(sIssDate)));
                //}

                if (!string.IsNullOrEmpty(sFromDate))
                {
                    DateTime SFrom = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);


                    oColors = oColors.Where(p => (p.RecdDate >= Convert.ToDateTime(SFrom)));
                }

                if (!string.IsNullOrEmpty(sToDate))
                {
                    DateTime STo = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oColors = oColors.Where(p => (p.RecdDate <= Convert.ToDateTime(STo)));
                }
                if (!string.IsNullOrEmpty(sCust))
                {
                    oColors = oColors.Where(p => p.gen_Customer.CustomerName.IndexOf(sCust, StringComparison.OrdinalIgnoreCase) >= 0);

                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(s => s.Pk_ID);

            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oColors = oColors.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchTask(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sPk_CharID = null;
            string sDate = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_PartJobs> oColors = null;

            try { sPk_CharID = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            //try { sDate = p_Params.Single(p => p.ParameterName == "RecdDate").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oColors = _oEntities.Vw_PartJobs;

            try
            {
                if (!string.IsNullOrEmpty(sPk_CharID))
                {
                    oColors = oColors.Where(p => p.Pk_ID == decimal.Parse(sPk_CharID));
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oColors.Count();


            oColors = oColors.OrderByDescending(p => p.Pk_ID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oColors = oColors.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            List<EntityObject> oFilteredItems = oColors.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }


        public SearchResult SearchName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<PartJobs> oName = null;
            oName = _oEntities.PartJobs;

            oSearchResult.RecordCount = oName.Count();
            oName.OrderBy(p => p.Pk_ID);

            List<EntityObject> oFilteredItem = oName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PartJobs oNewColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PartJobs), omValues) as PartJobs;
                var CustVal = omValues["Fk_Customer"].ToString();
                decimal fkMat = 0;
                if (omValues.ContainsKey("Materials"))
                {
                    string sIndentDetails = omValues["Materials"].ToString();
                    object[] IndentDetails = JsonConvert.DeserializeObject<object[]>(sIndentDetails);

                    oNewColor.Fk_Status = 1;

                    foreach (object oDtetails in IndentDetails)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDtetails.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        //if (oDataProperties["Fk_Material"].ToString() == "")
                        //{
                        //    oDataProperties["Fk_Material"] = 110011;
                        //    fkMat = 110011;
                        //}
                        //else
                        //{
                        //    fkMat = 0;
                        //}

                        JobWorkChild oOutDetail = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(JobWorkChild), oDataProperties) as JobWorkChild;
                        oOutDetail.Ply = 1;
                        if (oDataProperties.ContainsKey("RecdWeight"))
                        {
                            oOutDetail.Quantity = Convert.ToDecimal(oDataProperties["RecdWeight"]);
                        }
                        if (oDataProperties.ContainsKey("ReelNo"))
                        { oOutDetail.RollNo = oDataProperties["ReelNo"].ToString(); }
                        oNewColor.JobWorkChild.Add(oOutDetail);



                        //////Stock///

                        //var custCode = Convert.ToDecimal(CustVal);
                        //int StkCnt1 = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oOutDetail.Fk_Material && p.Fk_Customer == custCode).Count();

                        //if (StkCnt1 > 0)
                        //{
                        //    JobWorkRMStock oStockVal = _oEntities.JobWorkRMStock.Where(p => p.Fk_Material == oOutDetail.Fk_Material && p.Fk_Customer == custCode).Single();
                        //    oStockVal.StockValue = Convert.ToDecimal(oStockVal.StockValue + oOutDetail.Quantity);
                        //    //oStockVal.RollNo = oNewInwardD.RollNo;
                        //}
                        //else
                        //{
                            JobWorkRMStock oStockVal = new JobWorkRMStock();
                            oStockVal.Fk_Customer = Convert.ToDecimal(CustVal);
                            oStockVal.Fk_Material = oOutDetail.Fk_Material;
                            oStockVal.StockValue = Convert.ToDecimal(oOutDetail.Quantity);
                            oStockVal.RollNo = oOutDetail.RollNo;
                            _oEntities.AddToJobWorkRMStock(oStockVal);

                        //}
                    }


                }
                oNewColor.RecdDate = Convert.ToDateTime(omValues["RecdDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
                _oEntities.AddToPartJobs(oNewColor);

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PartJobs oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PartJobs), omValues) as PartJobs;
                PartJobs oColorFromShelf = _oEntities.PartJobs.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PartJobs), omValues, ref orefItems);
                _oEntities.SaveChanges();
                oResult.Success = true;

            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                PartJobs oColor = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(PartJobs), omValues) as PartJobs;
                PartJobs oColorFromShelf = _oEntities.PartJobs.Where(p => p.Pk_ID == oColor.Pk_ID).Single();
                object orefItems = oColorFromShelf;
                //_oEntities.DeleteObject(oEmployeesFromShelf);

                _oEntities.DeleteObject(oColorFromShelf);

                _oEntities.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oColor;

            }
            set
            {
                oColor = value as PartJobs;
            }
        }

        public decimal ID
        {
            get
            {
                return oColor.Pk_ID;
            }
            set
            {
                oColor = _oEntities.PartJobs.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oColor;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}


