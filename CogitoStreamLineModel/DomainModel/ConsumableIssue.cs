﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebGareCore.CommonObjects;
using System.Reflection;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using System.Web;
using Newtonsoft.Json;


namespace CogitoStreamLineModel.DomainModel
{
    public class ConsumableIssue : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntities = new CogitoStreamLineEntities();
        private Dictionary<string, object> omValues = null;
        private ConsumablesIssue oInward = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 
            //First name, Last name and username
            string sFromDate = null;
            string sToDate = null;

            string sVendor = null;
            string sInwardNo = null;
            string sMaterial = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<ConsumablesIssue> oInwards = null;

            try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            //try { sVendor = p_Params.Single(p => p.ParameterName == "Vendor").ParameterValue; }
            //catch { }
            try { sMaterial = p_Params.Single(p => p.ParameterName == "Material").ParameterValue; }
            catch { }

            try { sFromDate = p_Params.Single(p => p.ParameterName == "FromDate").ParameterValue; }
            catch { }
            try { sToDate = p_Params.Single(p => p.ParameterName == "ToDate").ParameterValue; }
            catch { }

            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }

            oInwards = _oEntities.ConsumablesIssue;

            try
            {
                if (!string.IsNullOrEmpty(sInwardNo))
                {
                    oInwards = oInwards.Where(p => p.Pk_ID == decimal.Parse(sInwardNo));
                }
                //if (!string.IsNullOrEmpty(sVendor))
                //{
                //    oInwards = oInwards.Where(p => p.Inv_MaterialIndentMaster.gen_Vendor.VendorName.IndexOf(sVendor, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                //if (!string.IsNullOrEmpty(sMaterial))
                //{
                //    oInwards = oInwards.Where(p => p.ConsumableIssueDet..Name.IndexOf(sMaterial, StringComparison.OrdinalIgnoreCase) >= 0);
                //}
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    DateTime FrmDate = Convert.ToDateTime(sFromDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oInwards = oInwards.Where(p => (p.IssueDate >= Convert.ToDateTime(FrmDate)));
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    DateTime ToDate = Convert.ToDateTime(sToDate.ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                    oInwards = oInwards.Where(p => (p.IssueDate <= Convert.ToDateTime(ToDate)));
                }
            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }

            oSearchResult.RecordCount = oInwards.Count();
            //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
            oInwards = oInwards.OrderByDescending(p => p.Pk_ID);
            //OrderBy(p => p.Pk_ID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oInwards = oInwards.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        public SearchResult SearchOthersStock(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for Order based on 
            //First name, Last name and OrderName

            string sPk_Material = null;
            string sMatName = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_OthersStock> oOrders = null;

            try { sPk_Material = p_Params.Single(p => p.ParameterName == "Pk_Material").ParameterValue; }
            catch { }
            try { sMatName = p_Params.Single(p => p.ParameterName == "MatName").ParameterValue; }
            catch { }


            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oOrders = _oEntities.Vw_OthersStock;

            try
            {

                if (!string.IsNullOrEmpty(sPk_Material))
                {
                    oOrders = oOrders.Where(p => p.Pk_Material == Convert.ToDecimal(sPk_Material));
                }

                if (!string.IsNullOrEmpty(sMatName))
                {
                    oOrders = oOrders.Where(p => p.MatName.IndexOf(sMatName, StringComparison.OrdinalIgnoreCase) >= 0);
                }

            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oOrders.Count();
            oOrders = oOrders.OrderByDescending(p => p.Pk_Material);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oOrders = oOrders.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredOrders = oOrders.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredOrders;

            return oSearchResult;

        }
        public SearchResult SearchIssueDetails(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();

            //We make a call that Search can only be done for User based on 

            /* Creating a query object with operator , field Ex Quantity >30 to be used for query later*/
            string sPk_MaterialIssueID = null;
            //string sMaterialIndentDate = null;
            string sIssueDate = null;
            string sFk_UserID = null;
            string sPk_MatIssueID = null;
            string sPk_JobCardID = null;
            string sFk_OrderNo = null;
            string sDCNo = null;
            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<Vw_ConsIssueList> oMaterialIssues = null;

            try { sPk_MatIssueID = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
            catch { }
            //try { sPk_MaterialIssueID = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
            //catch { }
            //try { sPk_JobCardID = p_Params.Single(p => p.ParameterName == "Pk_JobCardID").ParameterValue; }
            //catch { }

            //try { sIssueDate = p_Params.Single(p => p.ParameterName == "DateValue").ParameterValue; }
            //catch { }
            //try { sFk_UserID = p_Params.Single(p => p.ParameterName == "Fk_UserID").ParameterValue; }
            //catch { }
            //try { sFk_OrderNo = p_Params.Single(p => p.ParameterName == "Fk_OrderNo").ParameterValue; }
            //catch { }
            //try { sDCNo = p_Params.Single(p => p.ParameterName == "DCNo").ParameterValue; }
            //catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oMaterialIssues = _oEntities.Vw_ConsIssueList;
            // DateTime oDateTime;
            try
            {
                //if (!string.IsNullOrEmpty(sPk_MaterialIssueID))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_Material == decimal.Parse(sPk_MaterialIssueID));
                //}
                //if (!string.IsNullOrEmpty(sPk_JobCardID))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_JobCardID == decimal.Parse(sPk_JobCardID));
                //}
                if (!string.IsNullOrEmpty(sPk_MatIssueID))
                {
                    oMaterialIssues = oMaterialIssues.Where(p => p.Pk_ID == decimal.Parse(sPk_MatIssueID));
                }
                //if (!string.IsNullOrEmpty(sIssueDate))
                //{

                //    oMaterialIssues = oMaterialIssues.Where(p => (p.IssueDate >= Convert.ToDateTime(sIssueDate)));
                //}
                //if (!string.IsNullOrEmpty(sFk_UserID))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_UserID == decimal.Parse(sFk_UserID));
                //}
                //if (!string.IsNullOrEmpty(sFk_OrderNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.Fk_OrderNo == decimal.Parse(sFk_OrderNo));
                //}
                //if (!string.IsNullOrEmpty(sDCNo))
                //{
                //    oMaterialIssues = oMaterialIssues.Where(p => p.DCNo.IndexOf(sDCNo, StringComparison.OrdinalIgnoreCase) >= 0);

                //}


            }
            catch (System.NullReferenceException)
            {
                //possible that some items may not have the required fields set just ignore them
            }
            oSearchResult.RecordCount = oMaterialIssues.Count();
            oMaterialIssues = oMaterialIssues.OrderByDescending(p => p.Pk_ID);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {
                var Nnval = int.Parse(sPageSize);
                if (Nnval == 0)
                {

                    //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
                    //var skip = int.Parse(sPageSize) * (page - 1);

                    //oPOrd = oPOrd.Select(p => p)
                    //                    .Skip(skip)
                    //                    .Take(int.Parse(sPageSize));
                }
                else
                {
                    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                    var skip = int.Parse(sPageSize) * (page - 1);

                    oMaterialIssues = oMaterialIssues.Select(p => p)
                                        .Skip(skip)
                                        .Take(int.Parse(sPageSize));
                }
            }

            //if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            //{

            //    var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
            //    var skip = int.Parse(sPageSize) * (page - 1);

            //    oMaterialIssues = oMaterialIssues.Select(p => p)
            //                        .Skip(skip)
            //                        .Take(int.Parse(sPageSize));
            //}

            List<EntityObject> oFilteredItems = oMaterialIssues.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;
        }

        //public SearchResult SearchInwdDet(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();

        //    //We make a call that Search can only be done for User based on 
        //    //First name, Last name and username

        //    string sInwardNo = null;

        //    string sPk_Mat = null;
        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<Vw_InwardRep> oInwards = null;

        //    try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
        //    catch { }
        //    try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
        //    catch { }


        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }

        //    oInwards = _oEntities.Vw_InwardRep;

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sInwardNo))
        //        {
        //            oInwards = oInwards.Where(p => p.Pk_ID == decimal.Parse(sInwardNo));
        //        }

        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }

        //    oSearchResult.RecordCount = oInwards.Count();
        //    //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
        //    oInwards = oInwards.OrderByDescending(p => p.Pk_ID);
        //    //OrderBy(p => p.Pk_ID);


        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {
        //        var Nnval = int.Parse(sPageSize);
        //        if (Nnval == 0)
        //        {

        //            //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
        //            //var skip = int.Parse(sPageSize) * (page - 1);

        //            //oPOrd = oPOrd.Select(p => p)
        //            //                    .Skip(skip)
        //            //                    .Take(int.Parse(sPageSize));
        //        }
        //        else
        //        {
        //            var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //            var skip = int.Parse(sPageSize) * (page - 1);

        //            oInwards = oInwards.Select(p => p)
        //                                .Skip(skip)
        //                                .Take(int.Parse(sPageSize));
        //        }
        //    }

        //    List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItems;

        //    return oSearchResult;
        //}


        //public SearchResult SearchInwdDetails(List<SearchParameter> p_Params)
        //{
        //    SearchResult oSearchResult = new SearchResult();

        //    //We make a call that Search can only be done for User based on 
        //    //First name, Last name and username

        //    string sInwardNo = null;

        //    string sPk_Mat = null;
        //    string sStartIndex = null;
        //    string sPageSize = null;
        //    string sSorting = null;
        //    IEnumerable<Vw_TotInwdQty> oInwards = null;

        //    try { sInwardNo = p_Params.Single(p => p.ParameterName == "Pk_ID").ParameterValue; }
        //    catch { }
        //    try { sPk_Mat = p_Params.Single(p => p.ParameterName == "Pk_Mat").ParameterValue; }
        //    catch { }


        //    try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
        //    catch { }
        //    try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
        //    catch { }
        //    try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
        //    catch { }

        //    oInwards = _oEntities.Vw_TotInwdQty;

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(sInwardNo))
        //        {
        //            oInwards = oInwards.Where(p => p.Pk_ID == decimal.Parse(sInwardNo));
        //        }
        //        if (!string.IsNullOrEmpty(sPk_Mat))
        //        {
        //            oInwards = oInwards.Where(p => p.Pk_Material == decimal.Parse(sPk_Mat));
        //        }
        //    }
        //    catch (System.NullReferenceException)
        //    {
        //        //possible that some items may not have the required fields set just ignore them
        //    }

        //    oSearchResult.RecordCount = oInwards.Count();
        //    //  oQualityCheck = oQualityCheck.OrderByDescending(p => p.Pk_QualityCheck);
        //    oInwards = oInwards.OrderByDescending(p => p.Pk_ID);
        //    //OrderBy(p => p.Pk_ID);


        //    if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
        //    {
        //        var Nnval = int.Parse(sPageSize);
        //        if (Nnval == 0)
        //        {

        //            //var page = (int.Parse(sStartIndex) / 1) ; // set current page number, must be >= 1
        //            //var skip = int.Parse(sPageSize) * (page - 1);

        //            //oPOrd = oPOrd.Select(p => p)
        //            //                    .Skip(skip)
        //            //                    .Take(int.Parse(sPageSize));
        //        }
        //        else
        //        {
        //            var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
        //            var skip = int.Parse(sPageSize) * (page - 1);

        //            oInwards = oInwards.Select(p => p)
        //                                .Skip(skip)
        //                                .Take(int.Parse(sPageSize));
        //        }
        //    }

        //    List<EntityObject> oFilteredItems = oInwards.Select(p => p).OfType<EntityObject>().ToList();
        //    oSearchResult.ListOfRecords = oFilteredItems;

        //    return oSearchResult;
        //}


        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }


        public ModelManuplationResult CreateNew()
        {

            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                ConsumablesIssue oNewInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumablesIssue), omValues) as ConsumablesIssue;
          
                if (omValues.ContainsKey("Materials"))
                {
                    string sMaterials = omValues["Materials"].ToString();
                    object[] Materials = JsonConvert.DeserializeObject<object[]>(sMaterials);

                    foreach (object oItem in Materials)
                    {
                        Dictionary<string, object> oProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                        Dictionary<string, object> oDataProperties = JsonConvert.DeserializeObject<Dictionary<string, object>>(oProperties["data"].ToString());

                        ConsumableIssueDet oNewInwardD = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumableIssueDet), oDataProperties) as ConsumableIssueDet;

                        
                        oNewInward.ConsumableIssueDet.Add(oNewInwardD);



                        /////////////////stock

                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oNewInwardD.Fk_Material).Single();

                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {

                            int StkCnt = _oEntities.Stocks.Where(p => p.Fk_Material == oNewInwardD.Fk_Material).Count();

                            if (StkCnt > 0)
                            {
                                Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oNewInwardD.Fk_Material).Single();
                                oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oNewInwardD.Quantity);
                            }
                        

                        }


                    }


                }




                oNewInward.IssueDate = Convert.ToDateTime(omValues["IssueDate"].ToString(),
System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);

                _oEntities.AddToConsumablesIssue(oNewInward);

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }



        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {

                ConsumablesIssue oPOrd = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumablesIssue), omValues) as ConsumablesIssue;
                ConsumablesIssue OPOrdFromShelf = _oEntities.ConsumablesIssue.Where(p => p.Pk_ID == oPOrd.Pk_ID).Single();
                object orefPO = OPOrdFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ConsumablesIssue), omValues, ref orefPO);


                string sMaterials = omValues["Materials"].ToString();
                List<object> Materials = JsonConvert.DeserializeObject<List<object>>(sMaterials);

                List<decimal> UpdatedPk = new List<decimal>();
                List<ConsumableIssueDet> NewMaterialsList = new List<ConsumableIssueDet>();
                foreach (object oItem in Materials)
                {
                    Dictionary<string, object> oDataProperties1 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oItem.ToString());
                    Dictionary<string, object> oDataProperties2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(oDataProperties1["data"].ToString());

                    if (oDataProperties2["Pk_ID_Det"] == null || oDataProperties2["Pk_ID_Det"].ToString() == "")
                    {
                        ConsumableIssueDet oNewPOrderD
                            = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumableIssueDet), oDataProperties2) as ConsumableIssueDet;

                        NewMaterialsList.Add(oNewPOrderD);



                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == oNewPOrderD.Fk_Material).Single();

                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {


                            Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oNewPOrderD.Fk_Material).Single();

                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oNewPOrderD.Quantity);
                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oNewPOrderD.Quantity);
                        }
                  
                    }
                    else
                    {
                        //Handel Update here
                        decimal dPkPOrdDet = Convert.ToDecimal(oDataProperties2["Pk_ID_Det"]);
                        decimal dFkMaterial = Convert.ToDecimal(oDataProperties2["Fk_Material"]);
                     
                        Inv_Material oInv_Master = _oEntities.Inv_Material.Where(p => p.Pk_Material == dFkMaterial).Single();

                        ConsumableIssueDet oPOrderDetFromShelf = _oEntities.ConsumableIssueDet.Where(p => p.Pk_ID_Det == dPkPOrdDet && p.Fk_Material == dFkMaterial).Single();

                        object oPOrderDet
                                            = _oEntities.ConsumableIssueDet.Where(p => p.Pk_ID_Det == dPkPOrdDet).First();

                        _oEntities.DeleteObject(oPOrderDet);


                        ConsumableIssueDet oIndentDetails = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumableIssueDet), oDataProperties2) as ConsumableIssueDet;

                        NewMaterialsList.Add(oIndentDetails);
                        UpdatedPk.Add(dPkPOrdDet);


                        if (oInv_Master.Fk_MaterialCategory != 4)
                        {


                            Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oIndentDetails.Fk_Material).Single();

                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oPOrderDetFromShelf.Quantity);
                            oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oIndentDetails.Quantity);
                        }
                        else
                        {

                            //int StkCnt2 = _oEntities.PaperStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oIndentDetails.RollNo).Count();

                            //if (StkCnt2 > 0)
                            //{

                            //    PaperStock oStockVal = _oEntities.PaperStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oIndentDetails.RollNo).Single();

                            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oPOrderDetFromShelf.Quantity);
                            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oIndentDetails.Quantity);

                            //}
                            //else
                            //{
                            //    object oPOrderDet1
                            //            = _oEntities.PaperStock.Where(p => p.Fk_Material == oIndentDetails.Fk_Material && p.RollNo == oPOrderDetFromShelf.RollNo).First();

                            //    _oEntities.DeleteObject(oPOrderDet1);

                            //    PaperStock oStockVal1 = new PaperStock();

                            //    oStockVal1.Fk_Material = dFkMaterial;
                            //    oStockVal1.Quantity = Convert.ToDecimal(oDataProperties2["Quantity"]);
                            //    oStockVal1.RollNo = oDataProperties2["RollNo"].ToString();
                            //    _oEntities.AddToPaperStock(oStockVal1);
                            //}
                        }
                    }


                }
                // Handeling Deleted Records

                List<ConsumableIssueDet> oDeletedRecords = OPOrdFromShelf.ConsumableIssueDet.Where(p => !UpdatedPk.Contains(p.Pk_ID_Det)).ToList();



                //Add new elements
                foreach (ConsumableIssueDet oNewMaterialDetail in NewMaterialsList)
                {
                    OPOrdFromShelf.ConsumableIssueDet.Add(oNewMaterialDetail);
                }

                //object orefPurchaseM = OPOrdFromShelf;
                //WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(PurchaseOrderM), omValues, ref orefPurchaseM);

                //OPOrdFromShelf.GrandTotal = gtot;

                _oEntities.SaveChanges();

                oResult.Success = true;
            }
            //    ConsumablesIssue oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumablesIssue), omValues) as ConsumablesIssue;
            //    ConsumablesIssue oInwardFromShelf = _oEntities.ConsumablesIssue.Where(p => p.Pk_ID == oInward.Pk_ID).Single();
            //    object orefItems = oInwardFromShelf;

            //    var TanentID=Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
            //    var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            //    Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material && p.Fk_Tanent == TanentID && p.Fk_BranchID == branchid).Single();

            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInwardFromShelf.Quantity);
            //    oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity + oInward.Quantity);

            //    PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded)-Convert.ToDecimal(oInwardFromShelf.Quantity);
            //    oPendingNew.AlreadyInwarded = Convert.ToDecimal(oPendingNew.AlreadyInwarded) + Convert.ToDecimal(omValues["Quantity"]);
            //    ConsumablesIssue oIwd = _oEntities.ConsumablesIssue.Where(p => p.Fk_Material == oInwardFromShelf.Fk_Material && p.Pk_ID == oInward.Pk_ID).Single();
            //    //QualityCheck oQualitycheckPending = _oEntities.QualityChecks.Where(p => p.Pk_QualityCheck == oInward.Fk_QualityCheck && p.Fk_Material == oInward.Fk_Material).Single();
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending + oIwd.Quantity);
            //    //oQualitycheckPending.Pending = Convert.ToDecimal(oQualitycheckPending.Pending - oInward.Quantity);

            //    WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(ConsumablesIssue), omValues, ref orefItems);

            //    //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();







            //       decimal InwdFkMaterial = 0;
            //       InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);



            //       //oPendingNew.Quantity = Convert.ToDecimal(oQCheck.Quantity);29.12
            //       //oPendingNew.AlreadyInwarded = Convert.ToDecimal(oInward.Quantity);29.12


            // //      oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oInward.Quantity);



            //    _oEntities.SaveChanges();
            //    oResult.Success = true;
            //}


            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }
            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                //        ConsumablesIssue oInward = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(ConsumablesIssue), omValues) as ConsumablesIssue;
                //        ConsumablesIssue oInwardFromShelf = _oEntities.MaterialInwardMs.Where(p => p.Pk_ID == oInward.Pk_ID).Single();
                //        object orefItems = oInwardFromShelf;
                //        //QualityCheck oQCheck = _oEntities.QualityChecks.Where(p => p.Fk_IndentNumber == oInward.Fk_Indent && p.Fk_Material == oInward.Fk_Material).Single();



                //        var TanentID = Convert.ToInt32(HttpContext.Current.Session["Tanent"]);
                //        var branchid = Convert.ToInt32(HttpContext.Current.Session["Branch"]);

                //        Stock oStockVal = _oEntities.Stocks.Where(p => p.Fk_Material == oInward.Fk_Material ).Single();

                //        oStockVal.Quantity = Convert.ToDecimal(oStockVal.Quantity - oInward.Quantity);



                //        decimal InwdFkMaterial = 0;
                //        InwdFkMaterial = Convert.ToDecimal(oInward.Fk_Material);

                //        PendingTrack oPendingNew = _oEntities.PendingTracks.Where(p => p.Fk_Indent == oInward.Fk_Indent && p.Fk_Material == InwdFkMaterial).Single();


                //        oPendingNew.AlreadyInwarded = oPendingNew.Quantity - Convert.ToDecimal(oInward.Quantity);
                //       // oPendingNew.Pending = Convert.ToDecimal(oQCheck.Quantity) - Convert.ToDecimal(oPendingNew.AlreadyInwarded);



                //        _oEntities.DeleteObject(oInwardFromShelf);

                //         _oEntities.SaveChanges();

                //        oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oInward;

            }
            set
            {
                oInward = value as ConsumablesIssue;
            }
        }

        public decimal ID
        {
            get
            {
                return oInward.Pk_ID;
            }
            set
            {
                oInward = _oEntities.ConsumablesIssue.Where(p => p.Pk_ID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oInward;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntities.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Dictionary<string, string> InwardStock()
        {
            Dictionary<string, string> oResult = new Dictionary<string, string>();
            int Branch = Convert.ToInt32(HttpContext.Current.Session["Branch"]);
            IEnumerable<ConsumablesIssue> oMaterialInwardMs = _oEntities.ConsumablesIssue;
            oMaterialInwardMs.OrderByDescending(ss => ss.Pk_ID);
            int s = 0;
            foreach (ConsumablesIssue oMatInw in oMaterialInwardMs)
            {
                oResult.Add(s.ToString(), oMatInw.Pk_ID + " - " + DateTime.Parse(oMatInw.IssueDate.ToString()).ToString("dd/MM/yyyy"));
                s++;
            }
            return oResult;
        }
    }
}



