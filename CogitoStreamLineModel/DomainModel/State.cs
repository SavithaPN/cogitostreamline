﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects.DataClasses;
using WebGareCore;
using WebGareCore.CommonObjects;
using System.Data;

using CogitoStreamLineModel;
namespace CogitoStreamLineModel.DomainModel
{
    public class State : IDomainObject
    {
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        private Dictionary<string, object> omValues = null;
        private gen_State oState = null;


        public SearchResult Search(List<SearchParameter> p_Params)
        {
            SearchResult oSearchResult = new SearchResult();
            string sPk_StateID = null;
            string sStateName = null;
            string sCountryId = null;

            string sStartIndex = null;
            string sPageSize = null;
            string sSorting = null;
            IEnumerable<gen_State> oStates = null;

            try { sPk_StateID = p_Params.Single(p => p.ParameterName == "Pk_StateID").ParameterValue; }
            catch { }
            try { sStateName = p_Params.Single(p => p.ParameterName == "StateName").ParameterValue; }
            catch { }
            try { sCountryId = p_Params.Single(p => p.ParameterName == "CountryId").ParameterValue; }
            catch { }
            try { sStartIndex = p_Params.Single(p => p.ParameterName == "StartIndex").ParameterValue; }
            catch { }
            try { sPageSize = p_Params.Single(p => p.ParameterName == "PageSize").ParameterValue; }
            catch { }
            try { sSorting = p_Params.Single(p => p.ParameterName == "Sorting").ParameterValue; }
            catch { }


            oStates = _oEntites.gen_State;
            try
            {
                if (!string.IsNullOrEmpty(sPk_StateID))
                {
                    oStates = oStates.Where(p => p.Pk_StateID == decimal.Parse(sPk_StateID));
                }
                if (!string.IsNullOrEmpty(sStateName))
                {
                    //oStates = oStates.Where(p => p.StateName.IndexOf(sStateName, StringComparison.OrdinalIgnoreCase) >= 0);
                    oStates = oStates.Where(p => p.StateName.ToLower() == (sStateName.ToLower().ToString().Trim()));

                }
                if (!string.IsNullOrEmpty(sCountryId))
                {
                    oStates = oStates.Where(p => p.Country == decimal.Parse(sCountryId));
                }
            }
            catch (System.NullReferenceException)
            {
                
            }

            oSearchResult.RecordCount = oStates.Count();

           oStates= oStates.OrderBy(p => p.StateName);


            if (!string.IsNullOrEmpty(sStartIndex) && !string.IsNullOrEmpty(sPageSize))
            {

                var page = (int.Parse(sStartIndex) / int.Parse(sPageSize)) + 1; // set current page number, must be >= 1
                var skip = int.Parse(sPageSize) * (page - 1);

                oStates = oStates.Select(p => p)
                                    .Skip(skip)
                                    .Take(int.Parse(sPageSize));
            }

            List<EntityObject> oFilteredItems = oStates.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItems;

            return oSearchResult;

        }

        public SearchResult SearchStateName(List<SearchParameter> p_Params)
        {

            SearchResult oSearchResult = new SearchResult();
            IEnumerable<gen_State> oStateName = null;
            oStateName = _oEntites.gen_State;

            oSearchResult.RecordCount = oStateName.Count();
            oStateName.OrderBy(p => p.Pk_StateID);

            List<EntityObject> oFilteredItem = oStateName.Select(p => p).OfType<EntityObject>().ToList();
            oSearchResult.ListOfRecords = oFilteredItem;

            return oSearchResult;
        }

        public void SetValues(Dictionary<string, object> oValues)
        {
            omValues = oValues;
        }
        public List<System.Data.Objects.DataClasses.EntityObject> Sort(List<SearchParameter> p_Params)
        {
            throw new NotImplementedException();
        }

        public ModelManuplationResult CreateNew()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_State oNewStates = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_State), omValues) as gen_State;

                _oEntites.AddTogen_State(oNewStates);

                _oEntites.SaveChanges();

                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Update()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_State oStates = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_State), omValues) as gen_State;
                gen_State oStatesFromShelf = _oEntites.gen_State.Where(p => p.Pk_StateID == oStates.Pk_StateID).Single();
                object orefItems = oStatesFromShelf;
                WebGareCore.CommonObjects.Utilities.MapDataToObject(typeof(gen_State), omValues, ref orefItems);
                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public ModelManuplationResult Delete()
        {
            ModelManuplationResult oResult = new ModelManuplationResult();

            try
            {
                gen_State oStates = WebGareCore.CommonObjects.Utilities.MapDataToType(typeof(gen_State), omValues) as gen_State;
                gen_State oStatesFromShelf = _oEntites.gen_State.Where(p => p.Pk_StateID == oStates.Pk_StateID).Single();
                object orefItems = oStatesFromShelf;

                _oEntites.DeleteObject(oStatesFromShelf);

                _oEntites.SaveChanges();
                oResult.Success = true;
            }
            catch (Exception e)
            {
                oResult.Success = false;
                oResult.Message = e.Message;
                oResult.Exception = e;
            }

            return oResult;
        }

        public System.Data.Objects.DataClasses.EntityObject DAO
        {
            get
            {
                return oState;

            }
            set
            {
                oState = value as gen_State;
            }
        }

        public decimal ID
        {
            get
            {
                return oState.Pk_StateID;
            }
            set
            {
                oState = _oEntites.gen_State.Where(p => p.Pk_StateID == value).Single();
            }
        }

        public object GetRaw()
        {
            return oState;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _oEntites.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}



