USE [BalajiTesting]
GO
/****** Object:  Table [dbo].[emp_EmployeeDesignation]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emp_EmployeeDesignation](
	[Pk_EmployeeDesignationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DesignationName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_emp_EmployeeDesignation] PRIMARY KEY CLUSTERED 
(
	[Pk_EmployeeDesignationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[emp_Department]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emp_Department](
	[Pk_DepartmentId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_emp_Department] PRIMARY KEY CLUSTERED 
(
	[Pk_DepartmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_EstimateTax]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_EstimateTax](
	[PK_EstimationTaxes] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_DetailedEstimation] [numeric](18, 0) NULL,
	[VAT_Percentage] [numeric](18, 2) NULL,
	[VAT_Value] [numeric](18, 2) NULL,
	[Exercise_Percentage] [numeric](18, 2) NULL,
	[Exercise_Value] [numeric](18, 2) NULL,
	[SalesTax_Percentage] [numeric](18, 2) NULL,
	[Sales_Value] [numeric](18, 2) NULL,
 CONSTRAINT [PK_est_EstimateTax] PRIMARY KEY CLUSTERED 
(
	[PK_EstimationTaxes] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_EstimateAdditionalCost]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_EstimateAdditionalCost](
	[PK_EstimationAddtionalCosts] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_DetailedEstimation] [numeric](18, 0) NULL,
	[ConversionRatePercentage] [numeric](18, 0) NULL,
	[ConversionValue] [numeric](18, 0) NULL,
	[WestagePercentage] [numeric](18, 0) NULL,
	[WestageValue] [numeric](18, 0) NULL,
	[TransportCosts] [numeric](18, 0) NULL,
	[HandlingCosts] [numeric](18, 0) NULL,
	[RejectionPercentage] [numeric](18, 0) NULL,
	[RejectionValue] [numeric](18, 0) NULL,
	[MarginPercentage] [numeric](18, 0) NULL,
	[MarginValue] [numeric](18, 0) NULL,
	[PrintingCosts] [numeric](18, 0) NULL,
	[PastingCosts] [numeric](18, 0) NULL,
	[PinCosts] [numeric](18, 0) NULL,
	[ElectricityCosts] [numeric](18, 0) NULL,
	[ToolingCosts] [numeric](18, 0) NULL,
	[VarnishCosts] [numeric](18, 0) NULL,
 CONSTRAINT [PK_est_EstimateAdditionalCost] PRIMARY KEY CLUSTERED 
(
	[PK_EstimationAddtionalCosts] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_BoxEstimation]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_BoxEstimation](
	[Pk_BoxEstimation] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[GrandTotal] [numeric](18, 0) NULL,
 CONSTRAINT [PK_est_BoxEstimation] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxEstimation] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Communication]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_Communication](
	[Pk_Communication] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](200) NULL,
 CONSTRAINT [PK_gen_Communication] PRIMARY KEY CLUSTERED 
(
	[Pk_Communication] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Color]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_Color](
	[Pk_Color] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ColorName] [nvarchar](30) NULL,
 CONSTRAINT [PK_gen_Color] PRIMARY KEY CLUSTERED 
(
	[Pk_Color] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[Pk_AccountTypeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AccTypeName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[Pk_AccountTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[acc_AccountHeads]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acc_AccountHeads](
	[Pk_AccountHeadId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[HeadName] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_acc_AccountHeads] PRIMARY KEY CLUSTERED 
(
	[Pk_AccountHeadId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[att_RegisterMain]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[att_RegisterMain](
	[Pk_RegisterMain] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CMonth] [int] NOT NULL,
	[CYear] [int] NOT NULL,
 CONSTRAINT [PK_att_RegisterMain] PRIMARY KEY CLUSTERED 
(
	[Pk_RegisterMain] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BarCodeT]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BarCodeT](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Fk_PoNo] [numeric](18, 0) NULL,
	[Fk_JobCardNo] [numeric](18, 0) NULL,
	[JCDate] [datetime] NULL,
	[AssStkQty] [numeric](18, 0) NULL,
	[PODate] [datetime] NULL,
	[InwdQty] [numeric](18, 0) NULL,
	[POQty] [numeric](18, 0) NULL,
	[PendingQty] [numeric](18, 0) NULL,
	[BName] [varchar](50) NULL,
	[MName] [varchar](50) NULL,
	[ExStk] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BoxType]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxType](
	[Pk_BoxTypeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BoxType] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BoxChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxChild](
	[Pk_BoxCID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BoxChild] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxCID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COA_Parameters]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COA_Parameters](
	[Pk_ParaID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ParaName] [nvarchar](50) NULL,
 CONSTRAINT [PK_COA_Parameters] PRIMARY KEY CLUSTERED 
(
	[Pk_ParaID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Mill]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_Mill](
	[Pk_Mill] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MillName] [nvarchar](150) NULL,
	[Address] [varchar](100) NULL,
	[ContactNumber] [numeric](18, 0) NULL,
	[Email] [varchar](50) NULL,
	[ContactPerson1] [varchar](20) NULL,
	[Website] [varchar](50) NULL,
	[FkTenantID] [numeric](18, 0) NOT NULL,
	[LandLine] [numeric](18, 0) NULL,
	[SName] [nvarchar](50) NULL,
 CONSTRAINT [PK_gen_Mill] PRIMARY KEY CLUSTERED 
(
	[Pk_Mill] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_Machine]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_Machine](
	[Pk_Machine] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Location] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Capacity_Length] [decimal](18, 0) NULL,
	[Capacity_Width] [decimal](18, 0) NULL,
	[Capacity_Other] [decimal](18, 0) NULL,
	[AdditionalInformation] [nvarchar](150) NULL,
 CONSTRAINT [PK_gen_Machine] PRIMARY KEY CLUSTERED 
(
	[Pk_Machine] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FluteType]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FluteType](
	[Pk_FluteID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FluteName] [varchar](50) NULL,
	[TKFactor] [numeric](18, 2) NULL,
	[Height] [numeric](18, 2) NULL,
 CONSTRAINT [PK_FluteType] PRIMARY KEY CLUSTERED 
(
	[Pk_FluteID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_Country]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_Country](
	[Pk_Country] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CountryName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_gen_Country] PRIMARY KEY CLUSTERED 
(
	[Pk_Country] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_PaperType]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_PaperType](
	[Pk_PaperType] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PaperTypeName] [nvarchar](50) NULL,
	[PaperDescription] [nvarchar](500) NULL,
 CONSTRAINT [PK_gen_PaperType] PRIMARY KEY CLUSTERED 
(
	[Pk_PaperType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Unit]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_Unit](
	[Pk_Unit] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[UnitName] [nvarchar](50) NULL,
	[UnitType] [nvarchar](50) NULL,
	[Description] [nvarchar](300) NULL,
 CONSTRAINT [PK_gen_Unit] PRIMARY KEY CLUSTERED 
(
	[Pk_Unit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inv_MaterialCategory]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inv_MaterialCategory](
	[Pk_MaterialCategory] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_Inv_MaterialCategory] PRIMARY KEY CLUSTERED 
(
	[Pk_MaterialCategory] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GenReports]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenReports](
	[Pk_ReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](50) NULL,
 CONSTRAINT [PK_GenReports] PRIMARY KEY CLUSTERED 
(
	[Pk_ReportID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ideRole]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ideRole](
	[Pk_Roles] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](250) NULL,
	[Description] [varchar](250) NULL,
 CONSTRAINT [PK_ideRoles] PRIMARY KEY CLUSTERED 
(
	[Pk_Roles] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[idePermission]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[idePermission](
	[Pk_Permissions] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PermissionName] [varchar](250) NULL,
	[PermissionCode] [varchar](50) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_idePermissions] PRIMARY KEY CLUSTERED 
(
	[Pk_Permissions] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InkChar]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InkChar](
	[Pk_CharID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_InkChar] PRIMARY KEY CLUSTERED 
(
	[Pk_CharID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TonnValues]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TonnValues](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ExStock] [numeric](18, 2) NULL,
	[IssStock] [numeric](18, 2) NULL,
	[IssRetStock] [numeric](18, 2) NULL,
	[WastageVal] [numeric](18, 2) NULL,
	[ProductionVal] [numeric](18, 2) NULL,
 CONSTRAINT [PK_TonnValues] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tax]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tax](
	[PkTax] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TaxName] [varchar](50) NULL,
	[TaxValue] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Tax] PRIMARY KEY CLUSTERED 
(
	[PkTax] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransporterM]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransporterM](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TransporterName] [nvarchar](100) NULL,
	[Address1] [nvarchar](100) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[LLine] [nvarchar](50) NULL,
 CONSTRAINT [PK_TransporterM] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityCheck]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QualityCheck](
	[Pk_QualityCheck] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[InvoiceNumber] [nvarchar](50) NULL,
	[CheckDate] [datetime] NULL,
	[Comments] [varchar](500) NULL,
	[DCNo] [nchar](10) NULL,
	[Fk_PONo] [numeric](18, 0) NULL,
 CONSTRAINT [PK_QualityCheck] PRIMARY KEY CLUSTERED 
(
	[Pk_QualityCheck] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProcessMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcessMaster](
	[Pk_Process] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ProcessName] [nvarchar](150) NULL,
 CONSTRAINT [PK_ProcessMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_Process] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaperChar]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaperChar](
	[Pk_CharID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[UOM] [nvarchar](50) NULL,
	[TestMethod] [nvarchar](50) NULL,
 CONSTRAINT [PK_PaperChar] PRIMARY KEY CLUSTERED 
(
	[Pk_CharID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PartMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartMaster](
	[Pk_Part] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PartName] [nvarchar](50) NULL,
 CONSTRAINT [PK_PartMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_Part] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wgTenant](
	[Pk_Tanent] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TanentName] [nvarchar](500) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreationChannel] [nvarchar](50) NULL,
	[CreatedBy] [numeric](18, 0) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_wgTenant] PRIMARY KEY CLUSTERED 
(
	[Pk_Tanent] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfTriggers]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfTriggers](
	[Pk_Triggers] [numeric](18, 0) NOT NULL,
	[Fk_Path] [numeric](18, 0) NULL,
	[TriggerType] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfTriggerParameters]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfTriggerParameters](
	[Pk_TriggerParameters] [numeric](18, 0) NOT NULL,
	[Fk_Triggers] [numeric](18, 0) NULL,
	[ParameterName] [nvarchar](500) NULL,
	[Template] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfTransactions]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfTransactions](
	[Pk_Transaction] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Path] [numeric](18, 0) NULL,
	[Comment] [nvarchar](500) NULL,
	[Entity] [numeric](18, 0) NULL,
	[Date] [datetime] NULL,
	[Fk_User] [numeric](18, 0) NULL,
 CONSTRAINT [PK_wfTransactions] PRIMARY KEY CLUSTERED 
(
	[Pk_Transaction] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfStates]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfStates](
	[Pk_State] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[State] [nvarchar](255) NULL,
 CONSTRAINT [PK_wfStates] PRIMARY KEY CLUSTERED 
(
	[Pk_State] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WireChar]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WireChar](
	[Pk_CharID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[StdVal] [nvarchar](50) NULL,
 CONSTRAINT [PK_WireChar] PRIMARY KEY CLUSTERED 
(
	[Pk_CharID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfWorkflowRoles]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfWorkflowRoles](
	[Pk_WfRole] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_wfWorkflowRoles] PRIMARY KEY CLUSTERED 
(
	[Pk_WfRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfWorkflowPaths]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfWorkflowPaths](
	[Pk_Paths] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[InitialState] [bit] NULL,
	[CurrentState] [numeric](18, 0) NULL,
	[NextState] [numeric](18, 0) NULL,
	[PathName] [nvarchar](255) NULL,
	[AskComment] [bit] NULL,
	[ActionName] [nvarchar](100) NULL,
	[IconClass] [nvarchar](50) NULL,
	[BelongsTo] [nvarchar](255) NULL,
	[Fk_WfRole] [numeric](18, 0) NULL,
 CONSTRAINT [PK_wfWorkflowPaths] PRIMARY KEY CLUSTERED 
(
	[Pk_Paths] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Voucher]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Voucher](
	[Pk_ExpensesID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[Description] [varchar](500) NULL,
	[Fk_AccountHeadId] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Voucher] PRIMARY KEY CLUSTERED 
(
	[Pk_ExpensesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MachineMaintenance]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[MachineMaintenance](
	[Pk_MachineMaintenance] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Machine] [numeric](18, 0) NOT NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[DoneBy] [varchar](50) NULL,
	[Comments] [varchar](500) NULL,
 CONSTRAINT [PK_MachineMaintenance] PRIMARY KEY CLUSTERED 
(
	[Pk_MachineMaintenance] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OD_ID]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OD_ID](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PlyValue] [numeric](18, 0) NULL,
	[Length] [numeric](18, 0) NULL,
	[Width] [numeric](18, 0) NULL,
	[Height] [numeric](18, 0) NULL,
	[Fk_FluteID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_OD_ID] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QC_Documents]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QC_Documents](
	[Pk_Documents] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_QualityCheck] [numeric](18, 0) NULL,
	[DocumentLink] [nvarchar](500) NULL,
	[FileName] [nvarchar](255) NULL,
	[FileSize] [nvarchar](50) NULL,
 CONSTRAINT [PK_QC_Documents] PRIMARY KEY CLUSTERED 
(
	[Pk_Documents] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityDocuments]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityDocuments](
	[Pk_Document] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_QualityCheck] [numeric](18, 0) NULL,
	[DocumentLink] [nvarchar](500) NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FileSize] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QualityDocuments] PRIMARY KEY CLUSTERED 
(
	[Pk_Document] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trans_Payment]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trans_Payment](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Transporter] [numeric](18, 0) NULL,
	[AmtPaid] [numeric](18, 0) NULL,
	[InvNo] [nvarchar](50) NULL,
	[ChequeNo] [nvarchar](50) NULL,
	[ChequeDate] [smalldatetime] NULL,
	[Pay_Date] [smalldatetime] NULL,
 CONSTRAINT [PK_Trans_Payment] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trans_Invoice]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trans_Invoice](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[VehicleNo] [nvarchar](50) NULL,
	[VehicleName] [nvarchar](100) NULL,
	[DriverName] [nvarchar](70) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[Pickup_Point] [nvarchar](100) NULL,
	[Drop_Point] [nvarchar](100) NULL,
	[Invno] [nvarchar](50) NULL,
	[InvDate] [smalldatetime] NULL,
	[Amount] [numeric](18, 0) NULL,
	[CustomerName] [nvarchar](50) NULL,
	[Fk_Transporter] [numeric](18, 0) NULL,
	[BalAmt] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Trans_Invoice] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ideActivity]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ideActivity](
	[Pk_Activities] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[ActivityName] [varchar](250) NULL,
	[Link] [varchar](500) NULL,
	[NavigationParameters] [varchar](500) NULL,
	[ImageName] [varchar](500) NULL,
	[Fk_ParentActivity] [numeric](18, 0) NULL,
	[OrderBy] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ideActivities] PRIMARY KEY CLUSTERED 
(
	[Pk_Activities] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_State]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_State](
	[Pk_StateID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[StateName] [varchar](50) NOT NULL,
	[Country] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_State] PRIMARY KEY CLUSTERED 
(
	[Pk_StateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Branch](
	[Pk_BranchID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[BranchName] [varchar](50) NULL,
	[Fk_Tanent] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[Pk_BranchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bank](
	[Pk_BankID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_AccountTypeID] [numeric](18, 0) NOT NULL,
	[AccountName] [nvarchar](50) NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[BranchName] [nvarchar](50) NOT NULL,
	[BankName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[Pk_BankID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_City]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_City](
	[Pk_CityID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_StateID] [numeric](18, 0) NULL,
	[CityName] [varchar](50) NOT NULL,
	[Country] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_City] PRIMARY KEY CLUSTERED 
(
	[Pk_CityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ideActivityPermission]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ideActivityPermission](
	[Pk_ActivityPermissions] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Activities] [numeric](18, 0) NULL,
	[Fk_Permissions] [numeric](18, 0) NULL,
	[Fk_Roles] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ideActivityPermissions] PRIMARY KEY CLUSTERED 
(
	[Pk_ActivityPermissions] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ideUser]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ideUser](
	[Pk_Users] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[Fk_Branch] [numeric](18, 0) NULL,
	[UserName] [nvarchar](200) NULL,
	[Password] [nvarchar](200) NULL,
	[FirstName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[Office] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [smalldatetime] NULL,
 CONSTRAINT [PK_ideUsers] PRIMARY KEY CLUSTERED 
(
	[Pk_Users] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[wfWorkflowRoleUser]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wfWorkflowRoleUser](
	[Pk_wfRoleUser] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Role] [numeric](18, 0) NULL,
	[Fk_User] [numeric](18, 0) NULL,
 CONSTRAINT [PK_wfWorkflowRoleUser] PRIMARY KEY CLUSTERED 
(
	[Pk_wfRoleUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ideShortCut]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ideShortCut](
	[Pk_Shortcuts] [numeric](18, 0) NOT NULL,
	[ShortCut] [varchar](500) NULL,
	[Fk_Module] [numeric](18, 0) NULL,
	[Fk_User] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ideRoleUser]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ideRoleUser](
	[Pk_RoleUsers] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Roles] [numeric](18, 0) NULL,
	[Fk_Users] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ideRoleUsers] PRIMARY KEY CLUSTERED 
(
	[Pk_RoleUsers] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[emp_EmployeeMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emp_EmployeeMaster](
	[Pk_EmployeeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EmployeeId] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[FatherName] [nvarchar](50) NULL,
	[MotherName] [nvarchar](50) NULL,
	[DOB] [datetime] NULL,
	[Address] [nvarchar](250) NULL,
	[Fk_City] [numeric](18, 0) NOT NULL,
	[Fk_State] [numeric](18, 0) NOT NULL,
	[Pincode] [numeric](18, 0) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Fk_DepartmentId] [numeric](18, 0) NOT NULL,
	[Fk_EmployeeDesignation] [numeric](18, 0) NOT NULL,
	[IsActive] [bit] NULL,
	[AadharCardNo] [nvarchar](100) NULL,
	[PanNo] [nvarchar](100) NULL,
	[AcNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_emp_EmployeeMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_EmployeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_Vendor](
	[Fk_Tanent] [numeric](18, 0) NOT NULL,
	[Pk_Vendor] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[VendorName] [char](50) NOT NULL,
	[VendorType] [char](30) NOT NULL,
	[OfficeContactNumber] [numeric](30, 0) NOT NULL,
	[Email] [nvarchar](30) NOT NULL,
	[Address_No_Street] [nvarchar](100) NOT NULL,
	[Fk_State] [numeric](18, 0) NOT NULL,
	[Fk_Country] [numeric](18, 0) NOT NULL,
	[Fk_City] [numeric](18, 0) NOT NULL,
	[PinCode] [numeric](18, 0) NOT NULL,
	[LandLine] [numeric](18, 0) NULL,
	[MobileNumber] [numeric](18, 0) NULL,
	[Product] [nchar](10) NULL,
 CONSTRAINT [PK_gen_Vendor] PRIMARY KEY CLUSTERED 
(
	[Pk_Vendor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gen_Customer](
	[Pk_Customer] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CustomerName] [char](100) NOT NULL,
	[CustomerType] [char](50) NOT NULL,
	[CustomerAddress] [nvarchar](500) NOT NULL,
	[Fk_City] [numeric](18, 0) NOT NULL,
	[Fk_State] [numeric](18, 0) NOT NULL,
	[Fk_Country] [numeric](18, 0) NOT NULL,
	[PinCode] [numeric](18, 0) NOT NULL,
	[CustomerContact] [numeric](18, 0) NOT NULL,
	[LandLine] [numeric](18, 0) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[ContactPerson] [char](50) NOT NULL,
	[Fk_Tanent] [numeric](18, 0) NOT NULL,
	[CreatedDateOn] [smalldatetime] NOT NULL,
	[CustomerShort] [nchar](10) NULL,
	[VAT] [nvarchar](50) NULL,
	[PAN] [nvarchar](50) NULL,
	[Regn] [nvarchar](50) NULL,
	[CreditLimit] [nchar](10) NULL,
	[CreditAmt] [numeric](18, 2) NULL,
 CONSTRAINT [PK_gen_Customer] PRIMARY KEY CLUSTERED 
(
	[Pk_Customer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gen_ContactPerson]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_ContactPerson](
	[Pk_ContactPersonId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ContactPersonName] [nvarchar](100) NOT NULL,
	[ContactPersonDesignation] [nvarchar](100) NULL,
	[ContactPersonNumber] [numeric](18, 0) NOT NULL,
	[ContactPersonEmailId] [nvarchar](50) NOT NULL,
	[Address_No_Street] [nvarchar](100) NOT NULL,
	[Address_State] [numeric](18, 0) NOT NULL,
	[Address_City] [numeric](18, 0) NOT NULL,
	[Address_PinCode] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_gen_ContactPerson] PRIMARY KEY CLUSTERED 
(
	[Pk_ContactPersonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_CustomerShippingDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_CustomerShippingDetails](
	[Pk_CustomerShippingId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_CustomerId] [numeric](18, 0) NULL,
	[ShippingCustomerName] [nvarchar](150) NULL,
	[ShippingAddress] [nvarchar](500) NULL,
	[ShippingCountry] [nvarchar](50) NULL,
	[ShippingState] [nvarchar](50) NULL,
	[ShippingCity] [nvarchar](50) NULL,
	[ShippingPincode] [numeric](18, 0) NULL,
	[ShippingMobile] [numeric](18, 0) NULL,
	[ShippingLandLine] [numeric](18, 0) NULL,
	[ShippingEmailId] [nvarchar](50) NULL,
 CONSTRAINT [PK_gen_CustomerShippingDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_CustomerShippingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_CustomerContacts]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_CustomerContacts](
	[Pk_CustomerContacts] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Customer] [numeric](18, 0) NULL,
	[Fk_ContactPerson] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_CustomerContacts] PRIMARY KEY CLUSTERED 
(
	[Pk_CustomerContacts] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_VendorContacts]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_VendorContacts](
	[Pk_VendorContact] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
	[Fk_ContactPerson] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_VendorContacts] PRIMARY KEY CLUSTERED 
(
	[Pk_VendorContact] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_MillContacts]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_MillContacts](
	[Pk_MillContacts] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Mill] [numeric](18, 0) NULL,
	[Fk_ContactPerson] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_MillContacts] PRIMARY KEY CLUSTERED 
(
	[Pk_MillContacts] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DispatchMast]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DispatchMast](
	[Pk_Dispatch] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Dis_Date] [smalldatetime] NULL,
	[Fk_Customer] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DispatchMast] PRIMARY KEY CLUSTERED 
(
	[Pk_Dispatch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[emp_EmployeeImage]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emp_EmployeeImage](
	[Pk_EmployeeImageId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Employee] [numeric](18, 0) NULL,
	[ImageLink] [nvarchar](50) NULL,
	[ImageName] [nvarchar](50) NULL,
	[ImageSize] [nvarchar](50) NULL,
 CONSTRAINT [PK_emp_EmployeeImage] PRIMARY KEY CLUSTERED 
(
	[Pk_EmployeeImageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[eq_Enquiry]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[eq_Enquiry](
	[Pk_Enquiry] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CommunicationType] [numeric](18, 0) NULL,
	[Customer] [numeric](18, 0) NULL,
	[Description] [nvarchar](50) NULL,
	[Date] [smalldatetime] NOT NULL,
	[SpecialRequirements] [nvarchar](50) NULL,
	[EqComments] [nvarchar](50) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[DocumentLink] [varchar](500) NULL,
	[Fk_Branch] [numeric](18, 0) NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[RefNo] [nchar](50) NULL,
	[SampleRecd] [nchar](10) NULL,
 CONSTRAINT [PK_eq_Enquiry] PRIMARY KEY CLUSTERED 
(
	[Pk_Enquiry] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BoxMaster](
	[Pk_BoxID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Customer] [numeric](18, 0) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Fk_BoxType] [numeric](18, 0) NULL,
	[PartNo] [varchar](50) NULL,
	[Fk_FluteType] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BoxMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[att_RegisterAttendance]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[att_RegisterAttendance](
	[Pk_RegisterAttendance] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_RegisterMain] [numeric](18, 0) NOT NULL,
	[EmployeeName] [nvarchar](50) NULL,
	[Fk_EmployeeId] [numeric](18, 0) NOT NULL,
	[Day_1] [bit] NULL,
	[Day_2] [bit] NULL,
	[Day_3] [bit] NULL,
	[Day_4] [bit] NULL,
	[Day_5] [bit] NULL,
	[Day_6] [bit] NULL,
	[Day_7] [bit] NULL,
	[Day_8] [bit] NULL,
	[Day_9] [bit] NULL,
	[Day_10] [bit] NULL,
	[Day_11] [bit] NULL,
	[Day_12] [bit] NULL,
	[Day_13] [bit] NULL,
	[Day_14] [bit] NULL,
	[Day_15] [bit] NULL,
	[Day_16] [bit] NULL,
	[Day_17] [bit] NULL,
	[Day_18] [bit] NULL,
	[Day_19] [bit] NULL,
	[Day_20] [bit] NULL,
	[Day_21] [bit] NULL,
	[Day_22] [bit] NULL,
	[Day_23] [bit] NULL,
	[Day_24] [bit] NULL,
	[Day_25] [bit] NULL,
	[Day_26] [bit] NULL,
	[Day_27] [bit] NULL,
	[Day_28] [bit] NULL,
	[Day_29] [bit] NULL,
	[Day_30] [bit] NULL,
	[Day_31] [bit] NULL,
 CONSTRAINT [PK_att_RegisterAttendance] PRIMARY KEY CLUSTERED 
(
	[Pk_RegisterAttendance] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inv_MaterialIndentMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inv_MaterialIndentMaster](
	[Pk_MaterialOrderMasterId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[MaterialIndentDate] [datetime] NULL,
	[Fk_VendorId] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_Branch] [numeric](18, 0) NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[Comments] [nchar](10) NULL,
	[Fk_UserID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MaterialIndent] PRIMARY KEY CLUSTERED 
(
	[Pk_MaterialOrderMasterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inv_Material](
	[Pk_Material] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_MaterialCategory] [numeric](18, 0) NULL,
	[Name] [nvarchar](250) NULL,
	[Fk_Mill] [numeric](18, 0) NULL,
	[Fk_PaperType] [numeric](18, 0) NULL,
	[Fk_Color] [numeric](18, 0) NULL,
	[Deckle] [numeric](20, 2) NULL,
	[GSM] [numeric](20, 0) NULL,
	[BF] [numeric](20, 0) NULL,
	[Length] [numeric](20, 0) NULL,
	[Width] [numeric](20, 0) NULL,
	[Height] [numeric](20, 0) NULL,
	[GSM2] [numeric](20, 0) NULL,
	[PPly] [numeric](20, 0) NULL,
	[FPly] [numeric](20, 0) NULL,
	[Weight] [numeric](20, 0) NULL,
	[Size] [numeric](20, 0) NULL,
	[VendorIdNumber] [numeric](18, 0) NULL,
	[Company] [nvarchar](20) NULL,
	[Brand] [nvarchar](20) NULL,
	[MaterialType] [nvarchar](20) NULL,
	[Description] [nvarchar](100) NULL,
	[Fk_UnitId] [numeric](18, 0) NULL,
	[Moisture] [numeric](20, 0) NULL,
	[Min_Value] [numeric](18, 0) NULL,
	[Max_Value] [numeric](18, 0) NULL,
	[GSM3] [numeric](18, 0) NULL,
	[PartNo] [nvarchar](50) NULL,
	[BF2] [numeric](18, 0) NULL,
	[BF3] [numeric](18, 0) NULL,
	[BF4] [numeric](18, 0) NULL,
	[BF5] [numeric](18, 0) NULL,
	[GSM4] [numeric](18, 0) NULL,
	[GSM5] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Inv_Material] PRIMARY KEY CLUSTERED 
(
	[Pk_Material] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SampleBoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SampleBoxMaster](
	[Pk_BoxID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Customer] [numeric](18, 0) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Fk_BoxType] [numeric](18, 0) NULL,
	[PartNo] [varchar](50) NULL,
	[Fk_FluteType] [numeric](18, 0) NULL,
	[AsPer] [nvarchar](50) NULL,
 CONSTRAINT [PK_SampleBoxMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Receipt]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipt](
	[Pk_ReceiptID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Customer] [numeric](18, 0) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[ChequeNo] [nvarchar](50) NULL,
	[ChequeDate] [datetime] NULL,
	[Description] [nvarchar](500) NULL,
	[BankName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Receipt] PRIMARY KEY CLUSTERED 
(
	[Pk_ReceiptID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Outsourcing]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Outsourcing](
	[Pk_SrcID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
	[Description] [nvarchar](max) NULL,
	[IssueDate] [smalldatetime] NULL,
	[Fk_Status] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Outsource] PRIMARY KEY CLUSTERED 
(
	[Pk_SrcID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialQuotationMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaterialQuotationMaster](
	[Pk_QuotationRequest] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
	[Requests] [varchar](250) NULL,
	[RequestDate] [datetime] NULL,
	[CreatedBy] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MaterialQuotationMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_QuotationRequest] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartJobs]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartJobs](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RecdDate] [smalldatetime] NULL,
	[Description] [nvarchar](max) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_Customer] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PartJobs] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Payment](
	[Pk_PaymentID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Amount] [numeric](18, 2) NOT NULL,
	[Description] [varchar](500) NULL,
	[Fk_Vendor] [numeric](18, 0) NOT NULL,
	[ChequeNo] [varchar](50) NULL,
	[ChequeDate] [datetime] NULL,
	[Fk_Bank] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED 
(
	[Pk_PaymentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WireCertificate]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WireCertificate](
	[Pk_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Invno] [nvarchar](50) NULL,
	[InvDate] [smalldatetime] NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Tested] [nvarchar](50) NULL,
	[Approved] [nvarchar](50) NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
 CONSTRAINT [PK_WireCertificate] PRIMARY KEY CLUSTERED 
(
	[Pk_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PartJobsReturns]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PartJobsReturns](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Ret_Date] [smalldatetime] NULL,
	[TypeofPrd] [nchar](10) NULL,
	[Fk_PJID] [numeric](18, 0) NULL,
	[RetPrdWeight] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[PrdName] [numeric](18, 0) NULL,
	[RecdJobStatus] [nchar](10) NULL,
	[WtDiff] [numeric](18, 2) NULL,
 CONSTRAINT [PK_PartJobsReturns] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaperStock]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaperStock](
	[Pk_PaperStock] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[RollNo] [varchar](50) NULL,
	[Quantity] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PaperStock] PRIMARY KEY CLUSTERED 
(
	[Pk_PaperStock] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OutsourcingChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OutsourcingChild](
	[Pk_SrcDetID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PkSrcID] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[IssueWeight] [numeric](18, 2) NULL,
	[ReelNo] [nvarchar](50) NULL,
	[Length] [numeric](18, 0) NULL,
	[Width] [numeric](18, 0) NULL,
	[Height] [numeric](18, 0) NULL,
	[Ply] [numeric](18, 0) NULL,
	[AppNos] [int] NULL,
	[LayerWt] [numeric](18, 2) NULL,
 CONSTRAINT [PK_OursourcingChild] PRIMARY KEY CLUSTERED 
(
	[Pk_SrcDetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialQuotationDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaterialQuotationDetails](
	[Pk_QuotationMaterial] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_QuotationRequest] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NOT NULL,
	[Quantity] [numeric](18, 2) NULL,
	[SpecialInformation] [varchar](500) NULL,
 CONSTRAINT [PK_MaterialQuotationDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_QuotationMaterial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobWorkChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobWorkChild](
	[PkJobDetID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[RecdWeight] [numeric](18, 0) NULL,
	[Fk_JobID] [numeric](18, 0) NULL,
	[ReelNo] [nvarchar](50) NULL,
	[Length] [numeric](18, 0) NULL,
	[Width] [numeric](18, 0) NULL,
	[Height] [numeric](18, 0) NULL,
	[Ply] [numeric](18, 0) NULL,
	[AppNos] [int] NULL,
	[LayerWt] [numeric](18, 2) NULL,
 CONSTRAINT [PK_JobWorkChild] PRIMARY KEY CLUSTERED 
(
	[PkJobDetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobWorkRMStock]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobWorkRMStock](
	[Pk_StockID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[StockValue] [numeric](18, 0) NULL,
	[RollNo] [varchar](50) NULL,
	[Fk_Customer] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JobWorkRMStock] PRIMARY KEY CLUSTERED 
(
	[Pk_StockID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QualityChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityChild](
	[Pk_QualityChild] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FkQualityCheck] [numeric](18, 0) NULL,
	[POQty] [numeric](18, 0) NULL,
	[AccQty] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[PendingQty] [numeric](18, 0) NULL,
	[ReelNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_QualityChild] PRIMARY KEY CLUSTERED 
(
	[Pk_QualityChild] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PendingTrack]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PendingTrack](
	[Pk_IndentTrack] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Indent] [numeric](18, 0) NOT NULL,
	[Quantity] [numeric](18, 2) NOT NULL,
	[AlreadyInwarded] [numeric](18, 2) NOT NULL,
	[Pending] [numeric](18, 2) NOT NULL,
	[Fk_Material] [numeric](18, 0) NOT NULL,
	[QC_Qty] [numeric](18, 2) NULL,
 CONSTRAINT [PK_PendingTrack] PRIMARY KEY CLUSTERED 
(
	[Pk_IndentTrack] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuotationMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationMaster](
	[Pk_Quotation] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Grandtotal] [numeric](18, 2) NULL,
	[Description] [nvarchar](max) NULL,
	[QuotationDate] [datetime] NULL,
	[Fk_Enquiry] [numeric](18, 0) NULL,
	[Terms] [nvarchar](max) NULL,
 CONSTRAINT [PK_QuotationMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_Quotation] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseOrderM]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseOrderM](
	[Pk_PONo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PODate] [datetime] NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[TaxType] [nchar](10) NULL,
	[ED] [numeric](18, 0) NULL,
	[GrandTotal] [numeric](18, 0) NULL,
	[NetValue] [numeric](18, 0) NULL,
	[Fk_Indent] [numeric](18, 0) NULL,
	[CGST] [numeric](18, 2) NULL,
	[SGST] [numeric](18, 2) NULL,
	[DeliveryAdd] [nvarchar](max) NULL,
 CONSTRAINT [PK_PurchaseOrderM] PRIMARY KEY CLUSTERED 
(
	[Pk_PONo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StockTransfer]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StockTransfer](
	[Pk_TransferID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TransferDate] [datetime] NULL,
	[Fk_FromBranchID] [numeric](18, 0) NOT NULL,
	[Fk_ToBranchID] [numeric](18, 0) NOT NULL,
	[TransferQty] [numeric](18, 0) NOT NULL,
	[Fk_Material] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_StockTransfer] PRIMARY KEY CLUSTERED 
(
	[Pk_TransferID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stocks]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stocks](
	[Pk_Stock] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 2) NULL,
	[SafeValue] [numeric](18, 2) NULL,
	[Fk_Tanent] [numeric](18, 0) NOT NULL,
	[Fk_BranchID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Stocks] PRIMARY KEY CLUSTERED 
(
	[Pk_Stock] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sample_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sample_JobCardMaster](
	[Pk_JobCardID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[JDate] [datetime] NULL,
	[Fk_Order] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Printing] [nvarchar](50) NULL,
	[Calico] [bit] NULL,
	[Others] [varchar](500) NULL,
	[Invno] [varchar](50) NULL,
	[Corrugation] [varchar](50) NULL,
	[TopPaperQty] [numeric](18, 0) NULL,
	[TwoPlyQty] [numeric](18, 0) NULL,
	[TwoPlyWt] [numeric](18, 0) NULL,
	[PastingQty] [numeric](18, 0) NULL,
	[PastingWstQty] [numeric](18, 0) NULL,
	[RotaryQty] [numeric](18, 0) NULL,
	[RotaryWstQty] [numeric](18, 0) NULL,
	[PunchingQty] [numeric](18, 0) NULL,
	[PunchingWstQty] [numeric](18, 0) NULL,
	[SlotingQty] [numeric](18, 0) NULL,
	[SlotingWstQty] [numeric](18, 0) NULL,
	[PinningQty] [numeric](18, 0) NULL,
	[PinningWstQty] [numeric](18, 0) NULL,
	[FinishingQty] [numeric](18, 0) NULL,
	[FinishingWstQty] [numeric](18, 0) NULL,
	[TotalQty] [numeric](18, 0) NOT NULL,
	[TotalWstQty] [numeric](18, 0) NULL,
	[Machine] [numeric](18, 0) NULL,
	[StartTime] [nvarchar](50) NULL,
	[EndTime] [nvarchar](50) NULL,
	[ChkQuality] [numeric](18, 0) NULL,
	[PColor] [nvarchar](50) NULL,
	[PDetails] [nvarchar](150) NULL,
	[MFG] [nvarchar](50) NULL,
	[DPRSc] [nvarchar](50) NULL,
	[ECCQty] [numeric](18, 0) NULL,
	[EccWstQty] [numeric](18, 0) NULL,
	[GummingQty] [numeric](18, 0) NULL,
	[GummingWstQty] [numeric](18, 0) NULL,
	[Fk_Schedule] [numeric](18, 0) NULL,
	[Stitching] [bit] NULL,
 CONSTRAINT [Sample_PK_JobCardMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_JobCardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SampleBoxSpecs]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleBoxSpecs](
	[Pk_BoxSpecID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OuterShellYes] [bit] NULL,
	[CapYes] [bit] NULL,
	[LenghtPartationYes] [bit] NULL,
	[WidthPartationYes] [bit] NULL,
	[PlateYes] [bit] NULL,
	[OuterShell] [int] NULL,
	[Cap] [int] NULL,
	[LenghtPartation] [int] NULL,
	[WidthPartation] [int] NULL,
	[Plate] [int] NULL,
	[TotalWeight] [numeric](18, 3) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[TopYes] [bit] NULL,
	[BottomYes] [bit] NULL,
	[TopVal] [int] NULL,
	[BottVal] [int] NULL,
	[SleeveYes] [bit] NULL,
	[Sleeve] [int] NULL,
 CONSTRAINT [SamplePk_BoxSpecID] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxSpecID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GlueCertificate]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GlueCertificate](
	[Pk_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Invno] [nvarchar](50) NULL,
	[InvDate] [smalldatetime] NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Tested] [nvarchar](50) NULL,
	[Approved] [nvarchar](50) NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
 CONSTRAINT [PK_GlueCertificate] PRIMARY KEY CLUSTERED 
(
	[Pk_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_VendorMaterials]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_VendorMaterials](
	[Pk_VendorMaterial] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Rate] [numeric](18, 2) NULL,
 CONSTRAINT [PK_gen_VendorMaterials] PRIMARY KEY CLUSTERED 
(
	[Pk_VendorMaterial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InkCertificate]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InkCertificate](
	[Pk_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[BatchNo] [nvarchar](50) NULL,
	[Description] [nvarchar](150) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Tested] [nvarchar](50) NULL,
	[Approved] [nvarchar](50) NULL,
 CONSTRAINT [PK_InkCertificate] PRIMARY KEY CLUSTERED 
(
	[Pk_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BoxSpecs]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxSpecs](
	[Pk_BoxSpecID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OuterShellYes] [bit] NULL,
	[CapYes] [bit] NULL,
	[LenghtPartationYes] [bit] NULL,
	[WidthPartationYes] [bit] NULL,
	[PlateYes] [bit] NULL,
	[OuterShell] [int] NULL,
	[Cap] [int] NULL,
	[LenghtPartation] [int] NULL,
	[WidthPartation] [int] NULL,
	[Plate] [int] NULL,
	[TotalWeight] [numeric](18, 3) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[TopYes] [bit] NULL,
	[BottomYes] [bit] NULL,
	[TopVal] [int] NULL,
	[BottVal] [int] NULL,
	[SleeveYes] [bit] NULL,
	[Sleeve] [int] NULL,
 CONSTRAINT [Pk_BoxSpecID] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxSpecID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BoxPart]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxPart](
	[Pk_BoxPartID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Fk_PartID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BoxPart] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxPartID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BoardDesc]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BoardDesc](
	[Pk_Value] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoardID] [numeric](18, 0) NULL,
	[LayerID] [numeric](18, 0) NULL,
	[GSM] [numeric](18, 0) NULL,
	[BF] [numeric](18, 0) NULL,
	[Color] [varchar](50) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BoardDesc_1] PRIMARY KEY CLUSTERED 
(
	[Pk_Value] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[eq_Documents]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eq_Documents](
	[Pk_Documents] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Enquiry] [numeric](18, 0) NULL,
	[DocumentLink] [nvarchar](500) NULL,
	[FileName] [nvarchar](255) NULL,
	[FileSize] [nvarchar](50) NULL,
 CONSTRAINT [PK_ideeq_Documents] PRIMARY KEY CLUSTERED 
(
	[Pk_Documents] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DispatchChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DispatchChild](
	[Pk_DispDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_DispMast] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Weight] [numeric](18, 2) NULL,
	[Bundle] [nvarchar](50) NULL,
	[NosInBundle] [int] NULL,
	[NoOfRows] [int] NULL,
	[NoOfColumns] [int] NULL,
 CONSTRAINT [PK_DispatchChild] PRIMARY KEY CLUSTERED 
(
	[Pk_DispDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Day_WiseStock]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Day_WiseStock](
	[Pk_ID] [numeric](18, 0) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[TDate] [datetime2](7) NULL,
	[Qty] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Ton_WiseStock] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[eq_EnquiryChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eq_EnquiryChild](
	[Pk_EnquiryChild] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Fk_Enquiry] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Ddate] [datetime] NULL,
	[Amount] [numeric](18, 0) NULL,
	[ConvValue] [numeric](18, 0) NULL,
	[Estimated] [bit] NULL,
 CONSTRAINT [PK_eq_EnquiryChild_1] PRIMARY KEY CLUSTERED 
(
	[Pk_EnquiryChild] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_Estimation]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_Estimation](
	[Pk_Estimate] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Enquiry] [numeric](18, 0) NULL,
	[Name] [nvarchar](500) NULL,
	[PlateYes] [bit] NULL,
	[WidthPartationYes] [bit] NULL,
	[LenghtPartationYes] [bit] NULL,
	[OuterShellYes] [bit] NULL,
	[CapYes] [bit] NULL,
	[OuterShell] [int] NULL,
	[Plate] [int] NULL,
	[WidthPartation] [int] NULL,
	[LenghtPartation] [int] NULL,
	[Cap] [int] NULL,
	[ConvRate] [decimal](18, 0) NULL,
	[ConvValue] [numeric](18, 0) NULL,
	[GMarginPercentage] [int] NULL,
	[GMarginValue] [numeric](18, 0) NULL,
	[TaxesPercntage] [int] NULL,
	[TaxesValue] [numeric](18, 0) NULL,
	[TransportValue] [numeric](18, 0) NULL,
	[WeightHValue] [numeric](18, 0) NULL,
	[HandlingChanrgesValue] [numeric](18, 0) NULL,
	[PackingChargesValue] [numeric](18, 0) NULL,
	[RejectionPercentage] [int] NULL,
	[RejectionValue] [numeric](18, 0) NULL,
	[TotalWeight] [numeric](18, 0) NULL,
	[TotalPrice] [numeric](18, 0) NULL,
 CONSTRAINT [PK_est_Estimation_1] PRIMARY KEY CLUSTERED 
(
	[Pk_Estimate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_Order](
	[Pk_Order] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[OrderDate] [smalldatetime] NOT NULL,
	[Fk_Enquiry] [numeric](18, 0) NULL,
	[Fk_Customer] [numeric](18, 0) NULL,
	[Fk_Branch] [numeric](18, 0) NULL,
	[Product] [nvarchar](500) NULL,
	[ShippingAddress] [nvarchar](max) NULL,
	[ShippingInstruction] [nvarchar](max) NULL,
	[SpecialInstructions] [nvarchar](max) NULL,
	[Quantity] [int] NULL,
	[Price] [numeric](18, 2) NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[Fk_ShippingId] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Cust_PO] [nvarchar](50) NULL,
 CONSTRAINT [PK_gen_Order] PRIMARY KEY CLUSTERED 
(
	[Pk_Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FinishedGoodsStock]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinishedGoodsStock](
	[Pk_StockId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Stock] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_FinishedGoodsStock] PRIMARY KEY CLUSTERED 
(
	[Pk_StockId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_EstimationPart]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_EstimationPart](
	[Pk_DetailedEstimationPart] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_DetailedEstimation] [numeric](18, 0) NULL,
	[Length] [numeric](18, 0) NULL,
	[Width] [numeric](18, 0) NULL,
	[Height] [numeric](18, 0) NULL,
	[BoardArea] [numeric](18, 0) NULL,
	[Deckle] [numeric](18, 0) NULL,
	[CuttingSize] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[NoBoards] [numeric](18, 0) NULL,
	[Weight] [numeric](18, 0) NULL,
	[Rate] [numeric](18, 0) NULL,
 CONSTRAINT [PK_est_EstimationPart_1] PRIMARY KEY CLUSTERED 
(
	[Pk_DetailedEstimationPart] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_BoxEstimationChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_BoxEstimationChild](
	[Pk_BoxEstimationChild] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxEstimation] [numeric](18, 0) NULL,
	[Fk_EnquiryChild] [numeric](18, 0) NULL,
	[ConvRate] [numeric](18, 2) NULL,
	[ConvValue] [numeric](18, 2) NULL,
	[GMarginPercentage] [numeric](18, 2) NULL,
	[GMarginValue] [numeric](18, 2) NULL,
	[TaxesPercntage] [numeric](18, 2) NULL,
	[TaxesValue] [numeric](18, 2) NULL,
	[TransportValue] [numeric](18, 2) NULL,
	[WeightHValue] [numeric](18, 2) NULL,
	[HandlingChanrgesValue] [numeric](18, 2) NULL,
	[PackingChargesValue] [numeric](18, 2) NULL,
	[RejectionPercentage] [numeric](18, 2) NULL,
	[RejectionValue] [numeric](18, 2) NULL,
	[TotalWeight] [numeric](18, 2) NULL,
	[TotalPrice] [numeric](18, 2) NULL,
	[Status] [bit] NULL,
	[VATPercentage] [numeric](18, 2) NULL,
	[VATValue] [numeric](18, 2) NULL,
	[CSTPercentage] [numeric](18, 2) NULL,
	[CSTValue] [numeric](18, 2) NULL,
	[TransportPercentage] [numeric](18, 2) NULL,
 CONSTRAINT [PK_est_BoxEstimationChild] PRIMARY KEY CLUSTERED 
(
	[Pk_BoxEstimationChild] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerRejections]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerRejections](
	[Pk_RejectionId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RejectionDate] [datetime] NULL,
	[Fk_OrderNo] [numeric](18, 0) NOT NULL,
	[Fk_Customer] [numeric](18, 0) NOT NULL,
	[Rejected_Quantity] [numeric](18, 0) NOT NULL,
	[Reason] [nvarchar](500) NULL,
 CONSTRAINT [PK_CustomerRejections] PRIMARY KEY CLUSTERED 
(
	[Pk_RejectionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerComplaints]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerComplaints](
	[Pk_ComplaintID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[ComplaintDate] [datetime] NOT NULL,
	[Fk_CustomerID] [numeric](18, 0) NULL,
	[Fk_OrderNo] [numeric](18, 0) NULL,
	[Fk_EnquiryNo] [numeric](18, 0) NULL,
	[Description] [nvarchar](500) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_User] [numeric](18, 0) NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CustomerComplaints] PRIMARY KEY CLUSTERED 
(
	[Pk_ComplaintID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inv_Billing]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Inv_Billing](
	[Pk_Invoice] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Invno] [varchar](50) NULL,
	[InvDate] [smalldatetime] NULL,
	[Fk_OrderNo] [numeric](18, 0) NOT NULL,
	[Fk_Customer] [numeric](18, 0) NULL,
	[PayMode] [nvarchar](50) NULL,
	[GrandTotal] [numeric](18, 2) NULL,
	[COA] [nvarchar](50) NULL,
	[ESUGAM] [nvarchar](50) NULL,
	[NETVALUE] [numeric](18, 2) NULL,
	[FORM_CT3] [bit] NULL,
	[CST] [bit] NULL,
	[FORM_H] [bit] NULL,
	[TaxType] [char](10) NULL,
	[ED] [numeric](18, 0) NULL,
	[Buyers_OrderNo] [nvarchar](50) NULL,
	[BOrderDated] [datetime2](7) NULL,
	[DNTimeofInv] [datetime2](7) NULL,
	[MVehicleNo] [nvarchar](50) NULL,
	[DNTimeofRemoval] [datetime2](7) NULL,
	[DeliveryName] [nvarchar](150) NULL,
	[DeliveryAdd1] [nvarchar](150) NULL,
	[DeliveryAdd2] [nvarchar](150) NULL,
	[StateCode] [nvarchar](50) NULL,
	[Cancel] [bit] NULL,
 CONSTRAINT [PK_Inv_Billing] PRIMARY KEY CLUSTERED 
(
	[Pk_Invoice] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GlueCertificateDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GlueCertificateDetails](
	[Pk_IdDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PkID] [numeric](18, 0) NULL,
	[PCode] [nvarchar](150) NULL,
	[MixingRatio] [nvarchar](150) NULL,
	[GelTemp] [nchar](10) NULL,
	[Specification1] [nvarchar](150) NULL,
	[Specification2] [nvarchar](150) NULL,
	[Results1] [nvarchar](150) NULL,
	[Results2] [nvarchar](150) NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_GlueCertificateDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_IdDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inv_MaterialIndentDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inv_MaterialIndentDetails](
	[Pk_MaterialOrderDetailsId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_MaterialOrderMasterId] [numeric](18, 0) NULL,
	[RequiredDate] [datetime] NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Fk_CustomerOrder] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 3) NULL,
	[Fk_Mill] [numeric](18, 0) NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MaterialIndentDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_MaterialOrderDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InkCertificateDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InkCertificateDetails](
	[Pk_IdDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_ID] [numeric](18, 0) NULL,
	[Fk_Params] [numeric](18, 0) NULL,
	[Spec_Target] [nvarchar](150) NULL,
	[Spec_Tolerance] [nvarchar](150) NULL,
	[Results] [nvarchar](50) NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_InkCertificateDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_IdDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemPartProperty](
	[Pk_PartPropertyID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxSpecsID] [numeric](18, 0) NOT NULL,
	[PName] [nvarchar](50) NULL,
	[Length] [int] NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[Weight] [numeric](18, 3) NULL,
	[Quantity] [int] NULL,
	[NoBoards] [numeric](18, 2) NULL,
	[BoardArea] [numeric](18, 2) NULL,
	[Deckle] [numeric](18, 2) NULL,
	[CuttingSize] [numeric](18, 2) NULL,
	[Rate] [numeric](18, 2) NULL,
	[AddBLength] [numeric](18, 2) NULL,
	[TakeUpFactor] [numeric](18, 2) NULL,
	[PlyVal] [numeric](18, 0) NULL,
	[OLength1] [int] NULL,
	[OWidth1] [int] NULL,
	[OHeight1] [int] NULL,
	[BoardGSM] [numeric](18, 2) NULL,
	[BoardBS] [numeric](18, 2) NULL,
 CONSTRAINT [PK_ItemPartyProperty] PRIMARY KEY CLUSTERED 
(
	[Pk_PartPropertyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SampleItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleItemPartProperty](
	[Pk_PartPropertyID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxSpecsID] [numeric](18, 0) NOT NULL,
	[PName] [nvarchar](50) NULL,
	[Length] [int] NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[Weight] [numeric](18, 3) NULL,
	[Quantity] [int] NULL,
	[NoBoards] [numeric](18, 2) NULL,
	[BoardArea] [numeric](18, 2) NULL,
	[Deckle] [numeric](18, 2) NULL,
	[CuttingSize] [numeric](18, 2) NULL,
	[Rate] [numeric](18, 2) NULL,
	[AddBLength] [numeric](18, 2) NULL,
	[TakeUpFactor] [numeric](18, 2) NULL,
	[PlyVal] [numeric](18, 0) NULL,
	[OLength1] [int] NULL,
	[OWidth1] [int] NULL,
	[OHeight1] [int] NULL,
	[BoardGSM] [numeric](18, 2) NULL,
	[BoardBS] [numeric](18, 2) NULL,
 CONSTRAINT [SamplePK_ItemPartyProperty] PRIMARY KEY CLUSTERED 
(
	[Pk_PartPropertyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sample_JobCardDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sample_JobCardDetails](
	[Pk_JobCardDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_JobCardID] [numeric](18, 0) NULL,
	[Fk_PaperStock] [numeric](18, 0) NULL,
	[QtyConsumed] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[RM_Consumed] [numeric](18, 0) NULL,
 CONSTRAINT [Sample_PK_JobCardDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_JobCardDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PurchaseOrderD]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PurchaseOrderD](
	[Pk_PODet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PONo] [numeric](18, 0) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Rate] [numeric](18, 0) NULL,
	[Amount] [numeric](18, 2) NULL,
	[InwdQty] [numeric](18, 0) NULL,
	[Fk_IndentVal] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PurchaseOrderD] PRIMARY KEY CLUSTERED 
(
	[Pk_PODet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuotationDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationDetails](
	[Pk_QuotationDetID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_QuotationID] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[CostPerBox] [numeric](18, 2) NULL,
	[BSValue] [numeric](18, 2) NULL,
	[Fk_EstimationID] [numeric](18, 0) NULL,
	[BoxID] [numeric](18, 0) NULL,
	[BName] [nvarchar](50) NULL,
	[Dimension] [nvarchar](100) NULL,
 CONSTRAINT [PK_QuotationDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_QuotationDetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobWorkReceivables]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobWorkReceivables](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RecdDate] [smalldatetime] NULL,
	[TypeofPrd] [nchar](10) NULL,
	[Fk_OutSrcID] [numeric](18, 0) NULL,
	[RecdPrdWeight] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[PrdName] [numeric](18, 0) NULL,
	[OutSourcedTaskStatus] [nchar](10) NULL,
	[WtDiff] [numeric](18, 2) NULL,
	[Fk_OutSrcDetID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JobWorkReceivables] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialInwardM]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialInwardM](
	[Pk_Inward] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Inward_Date] [datetime] NULL,
	[Fk_InwardBy] [numeric](18, 0) NOT NULL,
	[Fk_QC] [numeric](18, 0) NULL,
	[Fk_Indent] [numeric](18, 0) NULL,
	[PONo] [numeric](18, 0) NULL,
	[Pur_InvNo] [nvarchar](50) NULL,
	[Pur_InvDate] [datetime] NULL,
 CONSTRAINT [PK_MaterialInwardM] PRIMARY KEY CLUSTERED 
(
	[Pk_Inward] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaperCertificate]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaperCertificate](
	[Pk_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Invno] [nvarchar](50) NULL,
	[InvDate] [smalldatetime] NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Tested] [nvarchar](50) NULL,
	[Approved] [nvarchar](50) NULL,
	[Fk_Vendor] [numeric](18, 0) NULL,
	[Fk_PoNo] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PaperCertificate] PRIMARY KEY CLUSTERED 
(
	[Pk_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[POReturnMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POReturnMaster](
	[Pk_PoRetID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PoRetDate] [datetime] NULL,
	[Fk_PoNo] [numeric](18, 0) NULL,
 CONSTRAINT [PK_POReturnMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_PoRetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WireCertificateDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WireCertificateDetails](
	[Pk_IdDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_ID] [numeric](18, 0) NULL,
	[Fk_Params] [numeric](18, 0) NULL,
	[Observed] [nvarchar](150) NULL,
	[Remarks] [nvarchar](150) NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_WireCertificateDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_IdDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SampleItems_Layers]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleItems_Layers](
	[Pk_LayerID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PartId] [numeric](18, 0) NULL,
	[GSM] [numeric](18, 0) NULL,
	[BF] [numeric](18, 0) NULL,
	[Color] [nvarchar](50) NULL,
	[Rate] [numeric](18, 2) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Weight] [numeric](18, 3) NULL,
	[Tk_Factor] [numeric](18, 2) NULL,
 CONSTRAINT [SamplePK_Items_Part] PRIMARY KEY CLUSTERED 
(
	[Pk_LayerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[POReturnDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POReturnDetails](
	[Pk_PoRetDetID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PoRetID] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Rate] [numeric](18, 2) NULL,
	[Amount] [numeric](18, 2) NULL,
 CONSTRAINT [PK_POReturnDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_PoRetDetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaperCertificateDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaperCertificateDetails](
	[Pk_IdDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PkID] [numeric](18, 0) NULL,
	[Fk_Characteristics] [numeric](18, 0) NULL,
	[Target] [nvarchar](150) NULL,
	[Acceptance] [nvarchar](150) NULL,
	[Result1] [nchar](10) NULL,
	[Result2] [nchar](10) NULL,
	[Result3] [nchar](10) NULL,
	[Result4] [nchar](10) NULL,
	[Result5] [nchar](10) NULL,
	[Result6] [nchar](10) NULL,
	[MinVal] [nchar](10) NULL,
	[MaxVal] [nchar](10) NULL,
	[AvgVal] [nchar](10) NULL,
	[Remarks] [nchar](150) NULL,
	[TQty] [numeric](18, 0) NULL,
	[ReelNo] [nchar](10) NULL,
	[Status] [nchar](10) NULL,
 CONSTRAINT [PK_PaperCertificateDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_IdDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialInwardD]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialInwardD](
	[Pk_InwardDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Inward] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Price] [numeric](18, 0) NULL,
	[AccQty] [numeric](18, 0) NULL,
	[RollNo] [nvarchar](50) NULL,
	[Fk_Mill] [numeric](18, 0) NULL,
	[QCNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_MaterialInwardD] PRIMARY KEY CLUSTERED 
(
	[Pk_InwardDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaymentTrack]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PaymentTrack](
	[Pk_PaymtID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Paymt_Date] [datetime] NOT NULL,
	[Fk_Invno] [numeric](18, 0) NOT NULL,
	[ChequeNo] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[TransactionID] [varchar](50) NULL,
	[PaidAmt] [numeric](18, 2) NULL,
	[BalanceAmt] [numeric](18, 2) NULL,
	[BillAmt] [numeric](18, 2) NULL,
 CONSTRAINT [PK_PaymentTrack] PRIMARY KEY CLUSTERED 
(
	[Pk_PaymtID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Inv_BillingDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inv_BillingDetails](
	[Pk_Inv_Details] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Invoice] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Price] [numeric](18, 2) NULL,
	[Fk_TaxID] [numeric](18, 0) NULL,
	[Discount] [numeric](18, 2) NULL,
	[NetAmount] [numeric](18, 2) NULL,
	[Description] [nvarchar](500) NULL,
	[TotalAmount] [numeric](18, 2) NULL,
	[TaxValue] [numeric](18, 2) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Fk_PartID] [numeric](18, 0) NULL,
	[HsnCode] [nvarchar](50) NULL,
	[UnitVal] [nchar](10) NULL,
 CONSTRAINT [PK_Inv_BillingDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_Inv_Details] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Items_Layers]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items_Layers](
	[Pk_LayerID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_PartId] [numeric](18, 0) NULL,
	[GSM] [numeric](18, 0) NULL,
	[BF] [numeric](18, 0) NULL,
	[Color] [nvarchar](50) NULL,
	[Rate] [numeric](18, 2) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[Weight] [numeric](18, 3) NULL,
	[Tk_Factor] [numeric](18, 2) NULL,
 CONSTRAINT [PK_Items_Part] PRIMARY KEY CLUSTERED 
(
	[Pk_LayerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inv_PaymentDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Inv_PaymentDetails](
	[Pk_PaymtNo] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_InvoiceNo] [numeric](18, 0) NOT NULL,
	[CardNo] [numeric](18, 0) NULL,
	[CName] [varchar](50) NULL,
	[BankName] [varchar](50) NULL,
	[CardType] [varchar](50) NULL,
	[ChequeNo] [varchar](50) NULL,
	[ChequeDate] [datetime] NULL,
	[CBankName] [varchar](50) NULL,
	[TransNo] [varchar](50) NULL,
	[GateWay] [varchar](50) NULL,
	[TBankName] [varchar](50) NULL,
	[TargetedAccount] [varchar](50) NULL,
	[CashDenominations] [varchar](500) NULL,
 CONSTRAINT [PK_Inv_PaymentDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_PaymtNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COA]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COA](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[RDate] [datetime] NULL,
	[Fk_Invno] [numeric](18, 0) NULL,
 CONSTRAINT [PK_COA] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BoxStock]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxStock](
	[Pk_StockID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Fk_PartID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BoxStock] PRIMARY KEY CLUSTERED 
(
	[Pk_StockID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gen_OrderChild]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gen_OrderChild](
	[Pk_OrderChild] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_OrderID] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[OrdQty] [numeric](18, 0) NULL,
	[EnqQty] [numeric](18, 0) NULL,
	[Ddate] [datetime] NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_PartID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Gen_OrderChild] PRIMARY KEY CLUSTERED 
(
	[Pk_OrderChild] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_DeliverySchedule]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_DeliverySchedule](
	[Pk_DeliverySechedule] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[DeliveryDate] [datetime] NOT NULL,
	[Quantity] [numeric](18, 0) NULL,
	[Comments] [nvarchar](500) NULL,
	[DeliveryCompleted] [bit] NULL,
	[DeliveredDate] [datetime] NULL,
	[DeliveryCancelled] [bit] NULL,
	[Fk_Order] [numeric](18, 0) NULL,
	[OrdQty] [numeric](18, 0) NULL,
	[Fk_PartID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_DeliverySchedule] PRIMARY KEY CLUSTERED 
(
	[Pk_DeliverySechedule] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[est_Layer]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[est_Layer](
	[Pk_Layer] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_Estimation] [numeric](18, 0) NULL,
	[Gsm] [numeric](18, 0) NULL,
	[Bf] [numeric](18, 0) NULL,
	[Color] [nvarchar](150) NULL,
	[Rate] [numeric](18, 0) NULL,
 CONSTRAINT [PK_est_Layer_1] PRIMARY KEY CLUSTERED 
(
	[Pk_Layer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gen_Events]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gen_Events](
	[Pk_Events] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[EventSource] [numeric](18, 0) NOT NULL,
	[SourceType] [nvarchar](50) NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[StartTime] [nvarchar](50) NULL,
	[EndTime] [nvarchar](50) NULL,
	[EventDescription] [nvarchar](500) NULL,
	[Value] [numeric](18, 0) NULL,
 CONSTRAINT [PK_gen_Events] PRIMARY KEY CLUSTERED 
(
	[Pk_Events] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COADetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COADetails](
	[Pk_IDDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_MastID] [numeric](18, 0) NULL,
	[Fk_ParaID] [numeric](18, 0) NULL,
	[UOM] [nvarchar](50) NULL,
	[Specification] [nvarchar](150) NULL,
	[Result] [nvarchar](150) NULL,
	[Remarks] [nvarchar](150) NULL,
 CONSTRAINT [PK_COADetails] PRIMARY KEY CLUSTERED 
(
	[Pk_IDDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobCardPartsMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobCardPartsMaster](
	[Pk_JobCardPartsID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[JDate] [datetime] NULL,
	[Fk_Order] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_PartID] [numeric](18, 0) NULL,
	[Printing] [bit] NULL,
	[Calico] [bit] NULL,
	[Others] [varchar](500) NULL,
	[Invno] [varchar](50) NULL,
	[Corrugation] [varchar](50) NULL,
	[TopPaperQty] [numeric](18, 0) NULL,
	[TwoPlyQty] [numeric](18, 0) NULL,
	[TwoPlyWt] [numeric](18, 0) NULL,
	[PastingQty] [numeric](18, 0) NULL,
	[PastingWstQty] [numeric](18, 0) NULL,
	[RotaryQty] [numeric](18, 0) NULL,
	[RotaryWstQty] [numeric](18, 0) NULL,
	[PunchingQty] [numeric](18, 0) NULL,
	[PunchingWstQty] [numeric](18, 0) NULL,
	[SlotingQty] [numeric](18, 0) NULL,
	[SlotingWstQty] [numeric](18, 0) NULL,
	[PinningQty] [numeric](18, 0) NULL,
	[PinningWstQty] [numeric](18, 0) NULL,
	[FinishingQty] [numeric](18, 0) NULL,
	[FinishingWstQty] [numeric](18, 0) NULL,
	[TotalQty] [numeric](18, 0) NULL,
	[TotalWstQty] [numeric](18, 0) NULL,
	[Fk_Schedule] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JobCardPartsMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_JobCardPartsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobCardMaster](
	[Pk_JobCardID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[JDate] [datetime] NULL,
	[Fk_Order] [numeric](18, 0) NULL,
	[Fk_Status] [numeric](18, 0) NULL,
	[Fk_BoxID] [numeric](18, 0) NULL,
	[Printing] [nvarchar](50) NULL,
	[Calico] [bit] NULL,
	[Others] [varchar](500) NULL,
	[Invno] [varchar](50) NULL,
	[Corrugation] [varchar](50) NULL,
	[TopPaperQty] [numeric](18, 0) NULL,
	[TwoPlyQty] [numeric](18, 0) NULL,
	[TwoPlyWt] [numeric](18, 0) NULL,
	[PastingQty] [numeric](18, 0) NULL,
	[PastingWstQty] [numeric](18, 0) NULL,
	[RotaryQty] [numeric](18, 0) NULL,
	[RotaryWstQty] [numeric](18, 0) NULL,
	[PunchingQty] [numeric](18, 0) NULL,
	[PunchingWstQty] [numeric](18, 0) NULL,
	[SlotingQty] [numeric](18, 0) NULL,
	[SlotingWstQty] [numeric](18, 0) NULL,
	[PinningQty] [numeric](18, 0) NULL,
	[PinningWstQty] [numeric](18, 0) NULL,
	[FinishingQty] [numeric](18, 0) NULL,
	[FinishingWstQty] [numeric](18, 0) NULL,
	[TotalQty] [numeric](18, 0) NOT NULL,
	[TotalWstQty] [numeric](18, 0) NULL,
	[Machine] [numeric](18, 0) NULL,
	[StartTime] [nvarchar](50) NULL,
	[EndTime] [nvarchar](50) NULL,
	[ChkQuality] [numeric](18, 0) NULL,
	[PColor] [nvarchar](50) NULL,
	[PDetails] [nvarchar](150) NULL,
	[MFG] [nvarchar](50) NULL,
	[DPRSc] [nvarchar](50) NULL,
	[ECCQty] [numeric](18, 0) NULL,
	[EccWstQty] [numeric](18, 0) NULL,
	[GummingQty] [numeric](18, 0) NULL,
	[GummingWstQty] [numeric](18, 0) NULL,
	[Fk_Schedule] [numeric](18, 0) NULL,
	[Stitching] [bit] NULL,
	[CutLength] [numeric](18, 2) NULL,
	[UpsVal] [numeric](18, 2) NULL,
	[Corr] [nchar](10) NULL,
	[TopP] [nchar](10) NULL,
	[Pasting] [nchar](10) NULL,
	[Rotary] [nchar](10) NULL,
	[PrintingP] [nchar](10) NULL,
	[Punching] [nchar](10) NULL,
	[Slotting] [nchar](10) NULL,
	[Finishing] [nchar](10) NULL,
	[Pinning] [nchar](10) NULL,
	[Gumming] [nchar](10) NULL,
	[Bundling] [nchar](10) NULL,
	[Bundles] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JobCardMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_JobCardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobCardDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobCardDetails](
	[Pk_JobCardDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_JobCardID] [numeric](18, 0) NULL,
	[Fk_PaperStock] [numeric](18, 0) NULL,
	[QtyConsumed] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[RM_Consumed] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JobCardDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_JobCardDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JC_Documents]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JC_Documents](
	[Pk_Documents] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_JCNo] [numeric](18, 0) NULL,
	[FileName] [nvarchar](255) NULL,
	[FileSize] [nvarchar](50) NULL,
 CONSTRAINT [PK_Documents] PRIMARY KEY CLUSTERED 
(
	[Pk_Documents] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobCardPartsDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobCardPartsDetails](
	[Pk_JobCardPartsDet] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_JobCardPartsID] [numeric](18, 0) NULL,
	[Fk_PaperStock] [numeric](18, 0) NULL,
	[QtyConsumed] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[RM_Consumed] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JobCardPartsDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_JobCardPartsDet] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Semi_FinishedGoodsStock]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Semi_FinishedGoodsStock](
	[Pk_StkID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_MatCatID] [numeric](18, 0) NULL,
	[Stock] [numeric](18, 0) NULL,
	[Fk_JobCardID] [numeric](18, 0) NULL,
	[Description] [nvarchar](150) NULL,
 CONSTRAINT [PK_Semi_FinishedGoodsStock] PRIMARY KEY CLUSTERED 
(
	[Pk_StkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JProcess]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JProcess](
	[Pk_JProcessID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_ProcessID] [numeric](18, 0) NULL,
	[Fk_JobCardID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_JProcess] PRIMARY KEY CLUSTERED 
(
	[Pk_JProcessID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobProcess]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobProcess](
	[Pk_ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_ProcessID] [numeric](18, 0) NULL,
	[Fk_JobCardID] [numeric](18, 0) NULL,
	[Quantity] [numeric](18, 0) NULL,
	[WastageQty] [numeric](18, 2) NULL,
	[ProcessDate] [datetime] NULL,
	[PQuantity] [numeric](18, 0) NULL,
	[PStartTime] [datetime2](7) NULL,
	[PEndTime] [datetime2](7) NULL,
	[ReasonWastage] [nvarchar](250) NULL,
	[RemainingQty] [numeric](18, 0) NULL,
	[WQty] [numeric](18, 2) NULL,
 CONSTRAINT [PK_JobProcess] PRIMARY KEY CLUSTERED 
(
	[Pk_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MaterialIssue]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MaterialIssue](
	[Pk_MaterialIssueID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IssueDate] [datetime] NULL,
	[Fk_UserID] [numeric](18, 0) NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[Fk_OrderNo] [numeric](18, 0) NULL,
	[DCNo] [varchar](50) NULL,
	[Fk_Branch] [numeric](18, 0) NULL,
	[Fk_JobCardID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MaterialIssue] PRIMARY KEY CLUSTERED 
(
	[Pk_MaterialIssueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MaterialIssueDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaterialIssueDetails](
	[Pk_MaterialIssueDetailsID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_IssueID] [numeric](18, 0) NOT NULL,
	[Fk_Material] [numeric](18, 0) NOT NULL,
	[Quantity] [decimal](18, 0) NOT NULL,
	[ReturnQuantity] [decimal](18, 0) NULL,
	[Weight] [decimal](18, 0) NULL,
	[Pk_StockID] [numeric](18, 0) NULL,
	[RollNo] [nvarchar](50) NULL,
	[Fk_Mill] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MaterialIssueDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_MaterialIssueDetailsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IssueReturn]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IssueReturn](
	[Pk_IssueReturnMasterId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[IssueReturnDate] [datetime] NULL,
	[Fk_EnteredUserId] [numeric](18, 0) NULL,
	[Fk_IssueId] [numeric](18, 0) NULL,
	[IssueDate] [datetime] NULL,
	[Fk_Tanent] [numeric](18, 0) NULL,
	[Fk_Branch] [numeric](18, 0) NULL,
 CONSTRAINT [PK_IssueReturnMaster] PRIMARY KEY CLUSTERED 
(
	[Pk_IssueReturnMasterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IssueReturnDetails]    Script Date: 11/23/2018 12:14:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IssueReturnDetails](
	[Pk_IssueReturnDetailsId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fk_IssueReturnMasterId] [numeric](18, 0) NULL,
	[Fk_Material] [numeric](18, 0) NULL,
	[ReturnQuantity] [numeric](18, 0) NULL,
	[Pk_StockID] [numeric](18, 0) NULL,
	[RollNo] [nvarchar](50) NULL,
 CONSTRAINT [PK_IssueReturnDetails] PRIMARY KEY CLUSTERED 
(
	[Pk_IssueReturnDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 2) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_BoxStock_Quantity]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxStock] ADD  CONSTRAINT [DF_BoxStock_Quantity]  DEFAULT ((0)) FOR [Quantity]
GO
/****** Object:  ForeignKey [FK_att_RegisterAttendance_att_RegisterMain]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[att_RegisterAttendance]  WITH CHECK ADD  CONSTRAINT [FK_att_RegisterAttendance_att_RegisterMain] FOREIGN KEY([Fk_RegisterMain])
REFERENCES [dbo].[att_RegisterMain] ([Pk_RegisterMain])
GO
ALTER TABLE [dbo].[att_RegisterAttendance] CHECK CONSTRAINT [FK_att_RegisterAttendance_att_RegisterMain]
GO
/****** Object:  ForeignKey [FK_att_RegisterAttendance_emp_EmployeeMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[att_RegisterAttendance]  WITH CHECK ADD  CONSTRAINT [FK_att_RegisterAttendance_emp_EmployeeMaster] FOREIGN KEY([Fk_EmployeeId])
REFERENCES [dbo].[emp_EmployeeMaster] ([Pk_EmployeeId])
GO
ALTER TABLE [dbo].[att_RegisterAttendance] CHECK CONSTRAINT [FK_att_RegisterAttendance_emp_EmployeeMaster]
GO
/****** Object:  ForeignKey [FK_Bank_AccountType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Bank]  WITH CHECK ADD  CONSTRAINT [FK_Bank_AccountType] FOREIGN KEY([Fk_AccountTypeID])
REFERENCES [dbo].[AccountType] ([Pk_AccountTypeID])
GO
ALTER TABLE [dbo].[Bank] CHECK CONSTRAINT [FK_Bank_AccountType]
GO
/****** Object:  ForeignKey [FK_BoardDesc_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoardDesc]  WITH CHECK ADD  CONSTRAINT [FK_BoardDesc_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[BoardDesc] CHECK CONSTRAINT [FK_BoardDesc_Inv_Material]
GO
/****** Object:  ForeignKey [FK_BoxMaster_BoxType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxMaster]  WITH CHECK ADD  CONSTRAINT [FK_BoxMaster_BoxType] FOREIGN KEY([Fk_BoxType])
REFERENCES [dbo].[BoxType] ([Pk_BoxTypeID])
GO
ALTER TABLE [dbo].[BoxMaster] CHECK CONSTRAINT [FK_BoxMaster_BoxType]
GO
/****** Object:  ForeignKey [FK_BoxMaster_FluteType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxMaster]  WITH CHECK ADD  CONSTRAINT [FK_BoxMaster_FluteType] FOREIGN KEY([Fk_FluteType])
REFERENCES [dbo].[FluteType] ([Pk_FluteID])
GO
ALTER TABLE [dbo].[BoxMaster] CHECK CONSTRAINT [FK_BoxMaster_FluteType]
GO
/****** Object:  ForeignKey [FK_BoxMaster_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxMaster]  WITH CHECK ADD  CONSTRAINT [FK_BoxMaster_gen_Customer] FOREIGN KEY([Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[BoxMaster] CHECK CONSTRAINT [FK_BoxMaster_gen_Customer]
GO
/****** Object:  ForeignKey [FK_BoxPart_BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxPart]  WITH CHECK ADD  CONSTRAINT [FK_BoxPart_BoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[BoxPart] CHECK CONSTRAINT [FK_BoxPart_BoxMaster]
GO
/****** Object:  ForeignKey [FK_BoxPart_PartMaster1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxPart]  WITH CHECK ADD  CONSTRAINT [FK_BoxPart_PartMaster1] FOREIGN KEY([Fk_PartID])
REFERENCES [dbo].[PartMaster] ([Pk_Part])
GO
ALTER TABLE [dbo].[BoxPart] CHECK CONSTRAINT [FK_BoxPart_PartMaster1]
GO
/****** Object:  ForeignKey [FK_BoxSpecs_BoxMast]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxSpecs]  WITH CHECK ADD  CONSTRAINT [FK_BoxSpecs_BoxMast] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[BoxSpecs] CHECK CONSTRAINT [FK_BoxSpecs_BoxMast]
GO
/****** Object:  ForeignKey [FK_BoxStock_BoxStock]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxStock]  WITH CHECK ADD  CONSTRAINT [FK_BoxStock_BoxStock] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[BoxStock] CHECK CONSTRAINT [FK_BoxStock_BoxStock]
GO
/****** Object:  ForeignKey [FK_BoxStock_ItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[BoxStock]  WITH CHECK ADD  CONSTRAINT [FK_BoxStock_ItemPartProperty] FOREIGN KEY([Fk_PartID])
REFERENCES [dbo].[ItemPartProperty] ([Pk_PartPropertyID])
GO
ALTER TABLE [dbo].[BoxStock] CHECK CONSTRAINT [FK_BoxStock_ItemPartProperty]
GO
/****** Object:  ForeignKey [FK_Branch_wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Branch]  WITH CHECK ADD  CONSTRAINT [FK_Branch_wgTenant] FOREIGN KEY([Fk_Tanent])
REFERENCES [dbo].[wgTenant] ([Pk_Tanent])
GO
ALTER TABLE [dbo].[Branch] CHECK CONSTRAINT [FK_Branch_wgTenant]
GO
/****** Object:  ForeignKey [FK_COA_Inv_Billing]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[COA]  WITH CHECK ADD  CONSTRAINT [FK_COA_Inv_Billing] FOREIGN KEY([Fk_Invno])
REFERENCES [dbo].[Inv_Billing] ([Pk_Invoice])
GO
ALTER TABLE [dbo].[COA] CHECK CONSTRAINT [FK_COA_Inv_Billing]
GO
/****** Object:  ForeignKey [FK_COADetails_COA]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[COADetails]  WITH CHECK ADD  CONSTRAINT [FK_COADetails_COA] FOREIGN KEY([Fk_MastID])
REFERENCES [dbo].[COA] ([Pk_ID])
GO
ALTER TABLE [dbo].[COADetails] CHECK CONSTRAINT [FK_COADetails_COA]
GO
/****** Object:  ForeignKey [FK_COADetails_COA_Parameters]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[COADetails]  WITH CHECK ADD  CONSTRAINT [FK_COADetails_COA_Parameters] FOREIGN KEY([Fk_ParaID])
REFERENCES [dbo].[COA_Parameters] ([Pk_ParaID])
GO
ALTER TABLE [dbo].[COADetails] CHECK CONSTRAINT [FK_COADetails_COA_Parameters]
GO
/****** Object:  ForeignKey [FK_CustomerComplaints_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[CustomerComplaints]  WITH CHECK ADD  CONSTRAINT [FK_CustomerComplaints_gen_Customer] FOREIGN KEY([Fk_CustomerID])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[CustomerComplaints] CHECK CONSTRAINT [FK_CustomerComplaints_gen_Customer]
GO
/****** Object:  ForeignKey [FK_CustomerComplaints_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[CustomerComplaints]  WITH CHECK ADD  CONSTRAINT [FK_CustomerComplaints_gen_Order] FOREIGN KEY([Fk_OrderNo])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[CustomerComplaints] CHECK CONSTRAINT [FK_CustomerComplaints_gen_Order]
GO
/****** Object:  ForeignKey [FK_CustomerRejections_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[CustomerRejections]  WITH CHECK ADD  CONSTRAINT [FK_CustomerRejections_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[CustomerRejections] CHECK CONSTRAINT [FK_CustomerRejections_gen_Customer]
GO
/****** Object:  ForeignKey [FK_CustomerRejections_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[CustomerRejections]  WITH CHECK ADD  CONSTRAINT [FK_CustomerRejections_gen_Order] FOREIGN KEY([Fk_OrderNo])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[CustomerRejections] CHECK CONSTRAINT [FK_CustomerRejections_gen_Order]
GO
/****** Object:  ForeignKey [FK_Ton_WiseStock_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Day_WiseStock]  WITH CHECK ADD  CONSTRAINT [FK_Ton_WiseStock_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[Day_WiseStock] CHECK CONSTRAINT [FK_Ton_WiseStock_Inv_Material]
GO
/****** Object:  ForeignKey [FK_DispatchChild_BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[DispatchChild]  WITH CHECK ADD  CONSTRAINT [FK_DispatchChild_BoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[DispatchChild] CHECK CONSTRAINT [FK_DispatchChild_BoxMaster]
GO
/****** Object:  ForeignKey [FK_DispatchChild_DispatchMast]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[DispatchChild]  WITH CHECK ADD  CONSTRAINT [FK_DispatchChild_DispatchMast] FOREIGN KEY([Fk_DispMast])
REFERENCES [dbo].[DispatchMast] ([Pk_Dispatch])
GO
ALTER TABLE [dbo].[DispatchChild] CHECK CONSTRAINT [FK_DispatchChild_DispatchMast]
GO
/****** Object:  ForeignKey [FK_DispatchMast_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[DispatchMast]  WITH CHECK ADD  CONSTRAINT [FK_DispatchMast_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[DispatchMast] CHECK CONSTRAINT [FK_DispatchMast_gen_Customer]
GO
/****** Object:  ForeignKey [FK_emp_EmployeeImage_emp_EmployeeMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[emp_EmployeeImage]  WITH CHECK ADD  CONSTRAINT [FK_emp_EmployeeImage_emp_EmployeeMaster] FOREIGN KEY([Fk_Employee])
REFERENCES [dbo].[emp_EmployeeMaster] ([Pk_EmployeeId])
GO
ALTER TABLE [dbo].[emp_EmployeeImage] CHECK CONSTRAINT [FK_emp_EmployeeImage_emp_EmployeeMaster]
GO
/****** Object:  ForeignKey [FK_emp_EmployeeMaster_emp_Department]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[emp_EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_emp_EmployeeMaster_emp_Department] FOREIGN KEY([Fk_DepartmentId])
REFERENCES [dbo].[emp_Department] ([Pk_DepartmentId])
GO
ALTER TABLE [dbo].[emp_EmployeeMaster] CHECK CONSTRAINT [FK_emp_EmployeeMaster_emp_Department]
GO
/****** Object:  ForeignKey [FK_emp_EmployeeMaster_emp_EmployeeDesignation]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[emp_EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_emp_EmployeeMaster_emp_EmployeeDesignation] FOREIGN KEY([Fk_EmployeeDesignation])
REFERENCES [dbo].[emp_EmployeeDesignation] ([Pk_EmployeeDesignationId])
GO
ALTER TABLE [dbo].[emp_EmployeeMaster] CHECK CONSTRAINT [FK_emp_EmployeeMaster_emp_EmployeeDesignation]
GO
/****** Object:  ForeignKey [FK_emp_EmployeeMaster_gen_City]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[emp_EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_emp_EmployeeMaster_gen_City] FOREIGN KEY([Fk_City])
REFERENCES [dbo].[gen_City] ([Pk_CityID])
GO
ALTER TABLE [dbo].[emp_EmployeeMaster] CHECK CONSTRAINT [FK_emp_EmployeeMaster_gen_City]
GO
/****** Object:  ForeignKey [FK_emp_EmployeeMaster_gen_State]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[emp_EmployeeMaster]  WITH CHECK ADD  CONSTRAINT [FK_emp_EmployeeMaster_gen_State] FOREIGN KEY([Fk_State])
REFERENCES [dbo].[gen_State] ([Pk_StateID])
GO
ALTER TABLE [dbo].[emp_EmployeeMaster] CHECK CONSTRAINT [FK_emp_EmployeeMaster_gen_State]
GO
/****** Object:  ForeignKey [FK_eq_Documents_eq_Enquiry]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_Documents]  WITH CHECK ADD  CONSTRAINT [FK_eq_Documents_eq_Enquiry] FOREIGN KEY([Fk_Enquiry])
REFERENCES [dbo].[eq_Enquiry] ([Pk_Enquiry])
GO
ALTER TABLE [dbo].[eq_Documents] CHECK CONSTRAINT [FK_eq_Documents_eq_Enquiry]
GO
/****** Object:  ForeignKey [FK_eq_Enquiry_Branch]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_Enquiry]  WITH CHECK ADD  CONSTRAINT [FK_eq_Enquiry_Branch] FOREIGN KEY([Fk_Branch])
REFERENCES [dbo].[Branch] ([Pk_BranchID])
GO
ALTER TABLE [dbo].[eq_Enquiry] CHECK CONSTRAINT [FK_eq_Enquiry_Branch]
GO
/****** Object:  ForeignKey [FK_eq_Enquiry_gen_Communication]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_Enquiry]  WITH CHECK ADD  CONSTRAINT [FK_eq_Enquiry_gen_Communication] FOREIGN KEY([CommunicationType])
REFERENCES [dbo].[gen_Communication] ([Pk_Communication])
GO
ALTER TABLE [dbo].[eq_Enquiry] CHECK CONSTRAINT [FK_eq_Enquiry_gen_Communication]
GO
/****** Object:  ForeignKey [FK_eq_Enquiry_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_Enquiry]  WITH CHECK ADD  CONSTRAINT [FK_eq_Enquiry_gen_Customer] FOREIGN KEY([Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[eq_Enquiry] CHECK CONSTRAINT [FK_eq_Enquiry_gen_Customer]
GO
/****** Object:  ForeignKey [FK_eq_Enquiry_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_Enquiry]  WITH CHECK ADD  CONSTRAINT [FK_eq_Enquiry_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[eq_Enquiry] CHECK CONSTRAINT [FK_eq_Enquiry_wfStates]
GO
/****** Object:  ForeignKey [FK_eq_Enquiry_wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_Enquiry]  WITH CHECK ADD  CONSTRAINT [FK_eq_Enquiry_wgTenant] FOREIGN KEY([Fk_Tanent])
REFERENCES [dbo].[wgTenant] ([Pk_Tanent])
GO
ALTER TABLE [dbo].[eq_Enquiry] CHECK CONSTRAINT [FK_eq_Enquiry_wgTenant]
GO
/****** Object:  ForeignKey [FK_eq_EnquiryChild_BoxMaster1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_EnquiryChild]  WITH CHECK ADD  CONSTRAINT [FK_eq_EnquiryChild_BoxMaster1] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[eq_EnquiryChild] CHECK CONSTRAINT [FK_eq_EnquiryChild_BoxMaster1]
GO
/****** Object:  ForeignKey [FK_eq_EnquiryChild_eq_Enquiry1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[eq_EnquiryChild]  WITH CHECK ADD  CONSTRAINT [FK_eq_EnquiryChild_eq_Enquiry1] FOREIGN KEY([Fk_Enquiry])
REFERENCES [dbo].[eq_Enquiry] ([Pk_Enquiry])
GO
ALTER TABLE [dbo].[eq_EnquiryChild] CHECK CONSTRAINT [FK_eq_EnquiryChild_eq_Enquiry1]
GO
/****** Object:  ForeignKey [FK_est_BoxEstimationChild_eq_EnquiryChild]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[est_BoxEstimationChild]  WITH CHECK ADD  CONSTRAINT [FK_est_BoxEstimationChild_eq_EnquiryChild] FOREIGN KEY([Fk_EnquiryChild])
REFERENCES [dbo].[eq_EnquiryChild] ([Pk_EnquiryChild])
GO
ALTER TABLE [dbo].[est_BoxEstimationChild] CHECK CONSTRAINT [FK_est_BoxEstimationChild_eq_EnquiryChild]
GO
/****** Object:  ForeignKey [FK_est_BoxEstimationChild_est_BoxEstimation]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[est_BoxEstimationChild]  WITH CHECK ADD  CONSTRAINT [FK_est_BoxEstimationChild_est_BoxEstimation] FOREIGN KEY([Fk_BoxEstimation])
REFERENCES [dbo].[est_BoxEstimation] ([Pk_BoxEstimation])
GO
ALTER TABLE [dbo].[est_BoxEstimationChild] CHECK CONSTRAINT [FK_est_BoxEstimationChild_est_BoxEstimation]
GO
/****** Object:  ForeignKey [FK_est_Estimation_eq_Enquiry]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[est_Estimation]  WITH CHECK ADD  CONSTRAINT [FK_est_Estimation_eq_Enquiry] FOREIGN KEY([Fk_Enquiry])
REFERENCES [dbo].[eq_Enquiry] ([Pk_Enquiry])
GO
ALTER TABLE [dbo].[est_Estimation] CHECK CONSTRAINT [FK_est_Estimation_eq_Enquiry]
GO
/****** Object:  ForeignKey [FK_est_EstimationPart_est_Estimation1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[est_EstimationPart]  WITH CHECK ADD  CONSTRAINT [FK_est_EstimationPart_est_Estimation1] FOREIGN KEY([Fk_DetailedEstimation])
REFERENCES [dbo].[est_Estimation] ([Pk_Estimate])
GO
ALTER TABLE [dbo].[est_EstimationPart] CHECK CONSTRAINT [FK_est_EstimationPart_est_Estimation1]
GO
/****** Object:  ForeignKey [FK_est_Layer_est_EstimationPart1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[est_Layer]  WITH CHECK ADD  CONSTRAINT [FK_est_Layer_est_EstimationPart1] FOREIGN KEY([Fk_Estimation])
REFERENCES [dbo].[est_EstimationPart] ([Pk_DetailedEstimationPart])
GO
ALTER TABLE [dbo].[est_Layer] CHECK CONSTRAINT [FK_est_Layer_est_EstimationPart1]
GO
/****** Object:  ForeignKey [FK_FinishedGoodsStock_BoxMast]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[FinishedGoodsStock]  WITH CHECK ADD  CONSTRAINT [FK_FinishedGoodsStock_BoxMast] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[FinishedGoodsStock] CHECK CONSTRAINT [FK_FinishedGoodsStock_BoxMast]
GO
/****** Object:  ForeignKey [FK_gen_City_gen_Country]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_City]  WITH CHECK ADD  CONSTRAINT [FK_gen_City_gen_Country] FOREIGN KEY([Country])
REFERENCES [dbo].[gen_Country] ([Pk_Country])
GO
ALTER TABLE [dbo].[gen_City] CHECK CONSTRAINT [FK_gen_City_gen_Country]
GO
/****** Object:  ForeignKey [FK_gen_City_gen_State]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_City]  WITH CHECK ADD  CONSTRAINT [FK_gen_City_gen_State] FOREIGN KEY([Fk_StateID])
REFERENCES [dbo].[gen_State] ([Pk_StateID])
GO
ALTER TABLE [dbo].[gen_City] CHECK CONSTRAINT [FK_gen_City_gen_State]
GO
/****** Object:  ForeignKey [FK_gen_ContactPerson_gen_City]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_ContactPerson]  WITH CHECK ADD  CONSTRAINT [FK_gen_ContactPerson_gen_City] FOREIGN KEY([Address_City])
REFERENCES [dbo].[gen_City] ([Pk_CityID])
GO
ALTER TABLE [dbo].[gen_ContactPerson] CHECK CONSTRAINT [FK_gen_ContactPerson_gen_City]
GO
/****** Object:  ForeignKey [FK_gen_ContactPerson_gen_State]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_ContactPerson]  WITH CHECK ADD  CONSTRAINT [FK_gen_ContactPerson_gen_State] FOREIGN KEY([Address_State])
REFERENCES [dbo].[gen_State] ([Pk_StateID])
GO
ALTER TABLE [dbo].[gen_ContactPerson] CHECK CONSTRAINT [FK_gen_ContactPerson_gen_State]
GO
/****** Object:  ForeignKey [FK_gen_Customer_gen_City]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Customer]  WITH CHECK ADD  CONSTRAINT [FK_gen_Customer_gen_City] FOREIGN KEY([Fk_City])
REFERENCES [dbo].[gen_City] ([Pk_CityID])
GO
ALTER TABLE [dbo].[gen_Customer] CHECK CONSTRAINT [FK_gen_Customer_gen_City]
GO
/****** Object:  ForeignKey [FK_gen_Customer_gen_Country]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Customer]  WITH CHECK ADD  CONSTRAINT [FK_gen_Customer_gen_Country] FOREIGN KEY([Fk_Country])
REFERENCES [dbo].[gen_Country] ([Pk_Country])
GO
ALTER TABLE [dbo].[gen_Customer] CHECK CONSTRAINT [FK_gen_Customer_gen_Country]
GO
/****** Object:  ForeignKey [FK_gen_Customer_gen_State]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Customer]  WITH CHECK ADD  CONSTRAINT [FK_gen_Customer_gen_State] FOREIGN KEY([Fk_State])
REFERENCES [dbo].[gen_State] ([Pk_StateID])
GO
ALTER TABLE [dbo].[gen_Customer] CHECK CONSTRAINT [FK_gen_Customer_gen_State]
GO
/****** Object:  ForeignKey [FK_gen_CustomerContacts_gen_ContactPerson]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_CustomerContacts]  WITH CHECK ADD  CONSTRAINT [FK_gen_CustomerContacts_gen_ContactPerson] FOREIGN KEY([Fk_ContactPerson])
REFERENCES [dbo].[gen_ContactPerson] ([Pk_ContactPersonId])
GO
ALTER TABLE [dbo].[gen_CustomerContacts] CHECK CONSTRAINT [FK_gen_CustomerContacts_gen_ContactPerson]
GO
/****** Object:  ForeignKey [FK_gen_CustomerContacts_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_CustomerContacts]  WITH CHECK ADD  CONSTRAINT [FK_gen_CustomerContacts_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[gen_CustomerContacts] CHECK CONSTRAINT [FK_gen_CustomerContacts_gen_Customer]
GO
/****** Object:  ForeignKey [FK_gen_CustomerShippingDetails_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_CustomerShippingDetails]  WITH CHECK ADD  CONSTRAINT [FK_gen_CustomerShippingDetails_gen_Customer] FOREIGN KEY([Fk_CustomerId])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[gen_CustomerShippingDetails] CHECK CONSTRAINT [FK_gen_CustomerShippingDetails_gen_Customer]
GO
/****** Object:  ForeignKey [FK_gen_DeliverySchedule_BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_DeliverySchedule]  WITH CHECK ADD  CONSTRAINT [FK_gen_DeliverySchedule_BoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[gen_DeliverySchedule] CHECK CONSTRAINT [FK_gen_DeliverySchedule_BoxMaster]
GO
/****** Object:  ForeignKey [FK_gen_DeliverySchedule_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_DeliverySchedule]  WITH CHECK ADD  CONSTRAINT [FK_gen_DeliverySchedule_gen_Order] FOREIGN KEY([Fk_Order])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[gen_DeliverySchedule] CHECK CONSTRAINT [FK_gen_DeliverySchedule_gen_Order]
GO
/****** Object:  ForeignKey [FK_gen_DeliverySchedule_ItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_DeliverySchedule]  WITH CHECK ADD  CONSTRAINT [FK_gen_DeliverySchedule_ItemPartProperty] FOREIGN KEY([Fk_PartID])
REFERENCES [dbo].[ItemPartProperty] ([Pk_PartPropertyID])
GO
ALTER TABLE [dbo].[gen_DeliverySchedule] CHECK CONSTRAINT [FK_gen_DeliverySchedule_ItemPartProperty]
GO
/****** Object:  ForeignKey [FK_gen_Events_gen_DeliverySchedule]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Events]  WITH CHECK ADD  CONSTRAINT [FK_gen_Events_gen_DeliverySchedule] FOREIGN KEY([EventSource])
REFERENCES [dbo].[gen_DeliverySchedule] ([Pk_DeliverySechedule])
GO
ALTER TABLE [dbo].[gen_Events] CHECK CONSTRAINT [FK_gen_Events_gen_DeliverySchedule]
GO
/****** Object:  ForeignKey [FK_gen_MillContacts_gen_ContactPerson]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_MillContacts]  WITH CHECK ADD  CONSTRAINT [FK_gen_MillContacts_gen_ContactPerson] FOREIGN KEY([Fk_ContactPerson])
REFERENCES [dbo].[gen_ContactPerson] ([Pk_ContactPersonId])
GO
ALTER TABLE [dbo].[gen_MillContacts] CHECK CONSTRAINT [FK_gen_MillContacts_gen_ContactPerson]
GO
/****** Object:  ForeignKey [FK_gen_MillContacts_gen_Mill]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_MillContacts]  WITH CHECK ADD  CONSTRAINT [FK_gen_MillContacts_gen_Mill] FOREIGN KEY([Fk_Mill])
REFERENCES [dbo].[gen_Mill] ([Pk_Mill])
GO
ALTER TABLE [dbo].[gen_MillContacts] CHECK CONSTRAINT [FK_gen_MillContacts_gen_Mill]
GO
/****** Object:  ForeignKey [FK_gen_Order_eq_Enquiry1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Order]  WITH CHECK ADD  CONSTRAINT [FK_gen_Order_eq_Enquiry1] FOREIGN KEY([Fk_Enquiry])
REFERENCES [dbo].[eq_Enquiry] ([Pk_Enquiry])
GO
ALTER TABLE [dbo].[gen_Order] CHECK CONSTRAINT [FK_gen_Order_eq_Enquiry1]
GO
/****** Object:  ForeignKey [FK_gen_Order_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Order]  WITH CHECK ADD  CONSTRAINT [FK_gen_Order_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[gen_Order] CHECK CONSTRAINT [FK_gen_Order_gen_Customer]
GO
/****** Object:  ForeignKey [FK_gen_Order_gen_CustomerShippingDetails]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Order]  WITH CHECK ADD  CONSTRAINT [FK_gen_Order_gen_CustomerShippingDetails] FOREIGN KEY([Fk_ShippingId])
REFERENCES [dbo].[gen_CustomerShippingDetails] ([Pk_CustomerShippingId])
GO
ALTER TABLE [dbo].[gen_Order] CHECK CONSTRAINT [FK_gen_Order_gen_CustomerShippingDetails]
GO
/****** Object:  ForeignKey [FK_Gen_OrderChild_BoxMaster1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Gen_OrderChild]  WITH CHECK ADD  CONSTRAINT [FK_Gen_OrderChild_BoxMaster1] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[Gen_OrderChild] CHECK CONSTRAINT [FK_Gen_OrderChild_BoxMaster1]
GO
/****** Object:  ForeignKey [FK_Gen_OrderChild_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Gen_OrderChild]  WITH CHECK ADD  CONSTRAINT [FK_Gen_OrderChild_gen_Order] FOREIGN KEY([Fk_OrderID])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[Gen_OrderChild] CHECK CONSTRAINT [FK_Gen_OrderChild_gen_Order]
GO
/****** Object:  ForeignKey [FK_Gen_OrderChild_ItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Gen_OrderChild]  WITH CHECK ADD  CONSTRAINT [FK_Gen_OrderChild_ItemPartProperty] FOREIGN KEY([Fk_PartID])
REFERENCES [dbo].[ItemPartProperty] ([Pk_PartPropertyID])
GO
ALTER TABLE [dbo].[Gen_OrderChild] CHECK CONSTRAINT [FK_Gen_OrderChild_ItemPartProperty]
GO
/****** Object:  ForeignKey [FK_gen_State_gen_Country]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_State]  WITH CHECK ADD  CONSTRAINT [FK_gen_State_gen_Country] FOREIGN KEY([Country])
REFERENCES [dbo].[gen_Country] ([Pk_Country])
GO
ALTER TABLE [dbo].[gen_State] CHECK CONSTRAINT [FK_gen_State_gen_Country]
GO
/****** Object:  ForeignKey [FK_gen_Vendor_gen_City]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Vendor]  WITH CHECK ADD  CONSTRAINT [FK_gen_Vendor_gen_City] FOREIGN KEY([Fk_City])
REFERENCES [dbo].[gen_City] ([Pk_CityID])
GO
ALTER TABLE [dbo].[gen_Vendor] CHECK CONSTRAINT [FK_gen_Vendor_gen_City]
GO
/****** Object:  ForeignKey [FK_gen_Vendor_gen_Country]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Vendor]  WITH CHECK ADD  CONSTRAINT [FK_gen_Vendor_gen_Country] FOREIGN KEY([Fk_Country])
REFERENCES [dbo].[gen_Country] ([Pk_Country])
GO
ALTER TABLE [dbo].[gen_Vendor] CHECK CONSTRAINT [FK_gen_Vendor_gen_Country]
GO
/****** Object:  ForeignKey [FK_gen_Vendor_gen_State]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_Vendor]  WITH CHECK ADD  CONSTRAINT [FK_gen_Vendor_gen_State] FOREIGN KEY([Fk_State])
REFERENCES [dbo].[gen_State] ([Pk_StateID])
GO
ALTER TABLE [dbo].[gen_Vendor] CHECK CONSTRAINT [FK_gen_Vendor_gen_State]
GO
/****** Object:  ForeignKey [FK_gen_VendorContacts_gen_ContactPerson]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_VendorContacts]  WITH CHECK ADD  CONSTRAINT [FK_gen_VendorContacts_gen_ContactPerson] FOREIGN KEY([Fk_ContactPerson])
REFERENCES [dbo].[gen_ContactPerson] ([Pk_ContactPersonId])
GO
ALTER TABLE [dbo].[gen_VendorContacts] CHECK CONSTRAINT [FK_gen_VendorContacts_gen_ContactPerson]
GO
/****** Object:  ForeignKey [FK_gen_VendorContacts_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_VendorContacts]  WITH CHECK ADD  CONSTRAINT [FK_gen_VendorContacts_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[gen_VendorContacts] CHECK CONSTRAINT [FK_gen_VendorContacts_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_gen_VendorMaterials_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_VendorMaterials]  WITH CHECK ADD  CONSTRAINT [FK_gen_VendorMaterials_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[gen_VendorMaterials] CHECK CONSTRAINT [FK_gen_VendorMaterials_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_gen_VendorMaterials_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[gen_VendorMaterials]  WITH CHECK ADD  CONSTRAINT [FK_gen_VendorMaterials_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[gen_VendorMaterials] CHECK CONSTRAINT [FK_gen_VendorMaterials_Inv_Material]
GO
/****** Object:  ForeignKey [FK_GlueCertificate_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[GlueCertificate]  WITH CHECK ADD  CONSTRAINT [FK_GlueCertificate_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[GlueCertificate] CHECK CONSTRAINT [FK_GlueCertificate_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_GlueCertificate_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[GlueCertificate]  WITH CHECK ADD  CONSTRAINT [FK_GlueCertificate_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[GlueCertificate] CHECK CONSTRAINT [FK_GlueCertificate_Inv_Material]
GO
/****** Object:  ForeignKey [FK_GlueCertificateDetails_GlueCertificate]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[GlueCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_GlueCertificateDetails_GlueCertificate] FOREIGN KEY([Fk_PkID])
REFERENCES [dbo].[GlueCertificate] ([Pk_Id])
GO
ALTER TABLE [dbo].[GlueCertificateDetails] CHECK CONSTRAINT [FK_GlueCertificateDetails_GlueCertificate]
GO
/****** Object:  ForeignKey [FK_ideActivity_wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideActivity]  WITH CHECK ADD  CONSTRAINT [FK_ideActivity_wgTenant] FOREIGN KEY([Fk_Tanent])
REFERENCES [dbo].[wgTenant] ([Pk_Tanent])
GO
ALTER TABLE [dbo].[ideActivity] CHECK CONSTRAINT [FK_ideActivity_wgTenant]
GO
/****** Object:  ForeignKey [FK_ideActivityPermission_ideActivity]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideActivityPermission]  WITH CHECK ADD  CONSTRAINT [FK_ideActivityPermission_ideActivity] FOREIGN KEY([Fk_Activities])
REFERENCES [dbo].[ideActivity] ([Pk_Activities])
GO
ALTER TABLE [dbo].[ideActivityPermission] CHECK CONSTRAINT [FK_ideActivityPermission_ideActivity]
GO
/****** Object:  ForeignKey [FK_ideActivityPermission_idePermission]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideActivityPermission]  WITH CHECK ADD  CONSTRAINT [FK_ideActivityPermission_idePermission] FOREIGN KEY([Fk_Permissions])
REFERENCES [dbo].[idePermission] ([Pk_Permissions])
GO
ALTER TABLE [dbo].[ideActivityPermission] CHECK CONSTRAINT [FK_ideActivityPermission_idePermission]
GO
/****** Object:  ForeignKey [FK_ideActivityPermission_ideRole]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideActivityPermission]  WITH CHECK ADD  CONSTRAINT [FK_ideActivityPermission_ideRole] FOREIGN KEY([Fk_Roles])
REFERENCES [dbo].[ideRole] ([Pk_Roles])
GO
ALTER TABLE [dbo].[ideActivityPermission] CHECK CONSTRAINT [FK_ideActivityPermission_ideRole]
GO
/****** Object:  ForeignKey [FK_ideRoleUser_ideRole]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideRoleUser]  WITH CHECK ADD  CONSTRAINT [FK_ideRoleUser_ideRole] FOREIGN KEY([Fk_Roles])
REFERENCES [dbo].[ideRole] ([Pk_Roles])
GO
ALTER TABLE [dbo].[ideRoleUser] CHECK CONSTRAINT [FK_ideRoleUser_ideRole]
GO
/****** Object:  ForeignKey [FK_ideRoleUser_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideRoleUser]  WITH CHECK ADD  CONSTRAINT [FK_ideRoleUser_ideUser] FOREIGN KEY([Fk_Users])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[ideRoleUser] CHECK CONSTRAINT [FK_ideRoleUser_ideUser]
GO
/****** Object:  ForeignKey [FK_ideShortCut_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideShortCut]  WITH CHECK ADD  CONSTRAINT [FK_ideShortCut_ideUser] FOREIGN KEY([Fk_User])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[ideShortCut] CHECK CONSTRAINT [FK_ideShortCut_ideUser]
GO
/****** Object:  ForeignKey [FK_ideUser_Branch]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideUser]  WITH CHECK ADD  CONSTRAINT [FK_ideUser_Branch] FOREIGN KEY([Fk_Branch])
REFERENCES [dbo].[Branch] ([Pk_BranchID])
GO
ALTER TABLE [dbo].[ideUser] CHECK CONSTRAINT [FK_ideUser_Branch]
GO
/****** Object:  ForeignKey [FK_ideUser_wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ideUser]  WITH CHECK ADD  CONSTRAINT [FK_ideUser_wgTenant] FOREIGN KEY([Fk_Tanent])
REFERENCES [dbo].[wgTenant] ([Pk_Tanent])
GO
ALTER TABLE [dbo].[ideUser] CHECK CONSTRAINT [FK_ideUser_wgTenant]
GO
/****** Object:  ForeignKey [FK_InkCertificate_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[InkCertificate]  WITH CHECK ADD  CONSTRAINT [FK_InkCertificate_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[InkCertificate] CHECK CONSTRAINT [FK_InkCertificate_Inv_Material]
GO
/****** Object:  ForeignKey [FK_InkCertificateDetails_InkCertificate]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[InkCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_InkCertificateDetails_InkCertificate] FOREIGN KEY([Fk_ID])
REFERENCES [dbo].[InkCertificate] ([Pk_Id])
GO
ALTER TABLE [dbo].[InkCertificateDetails] CHECK CONSTRAINT [FK_InkCertificateDetails_InkCertificate]
GO
/****** Object:  ForeignKey [FK_InkCertificateDetails_InkChar]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[InkCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_InkCertificateDetails_InkChar] FOREIGN KEY([Fk_Params])
REFERENCES [dbo].[InkChar] ([Pk_CharID])
GO
ALTER TABLE [dbo].[InkCertificateDetails] CHECK CONSTRAINT [FK_InkCertificateDetails_InkChar]
GO
/****** Object:  ForeignKey [FK_Inv_Billing_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Billing]  WITH CHECK ADD  CONSTRAINT [FK_Inv_Billing_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[Inv_Billing] CHECK CONSTRAINT [FK_Inv_Billing_gen_Customer]
GO
/****** Object:  ForeignKey [FK_Inv_Billing_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Billing]  WITH CHECK ADD  CONSTRAINT [FK_Inv_Billing_gen_Order] FOREIGN KEY([Fk_OrderNo])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[Inv_Billing] CHECK CONSTRAINT [FK_Inv_Billing_gen_Order]
GO
/****** Object:  ForeignKey [FK_Inv_BillingDetails_BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_BillingDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_BillingDetails_BoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[Inv_BillingDetails] CHECK CONSTRAINT [FK_Inv_BillingDetails_BoxMaster]
GO
/****** Object:  ForeignKey [FK_Inv_BillingDetails_Inv_Billing]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_BillingDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_BillingDetails_Inv_Billing] FOREIGN KEY([Fk_Invoice])
REFERENCES [dbo].[Inv_Billing] ([Pk_Invoice])
GO
ALTER TABLE [dbo].[Inv_BillingDetails] CHECK CONSTRAINT [FK_Inv_BillingDetails_Inv_Billing]
GO
/****** Object:  ForeignKey [FK_Inv_BillingDetails_ItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_BillingDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_BillingDetails_ItemPartProperty] FOREIGN KEY([Fk_PartID])
REFERENCES [dbo].[ItemPartProperty] ([Pk_PartPropertyID])
GO
ALTER TABLE [dbo].[Inv_BillingDetails] CHECK CONSTRAINT [FK_Inv_BillingDetails_ItemPartProperty]
GO
/****** Object:  ForeignKey [FK_Inv_Material_gen_Color]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Material]  WITH NOCHECK ADD  CONSTRAINT [FK_Inv_Material_gen_Color] FOREIGN KEY([Fk_Color])
REFERENCES [dbo].[gen_Color] ([Pk_Color])
GO
ALTER TABLE [dbo].[Inv_Material] CHECK CONSTRAINT [FK_Inv_Material_gen_Color]
GO
/****** Object:  ForeignKey [FK_Inv_Material_gen_Mill]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Material]  WITH NOCHECK ADD  CONSTRAINT [FK_Inv_Material_gen_Mill] FOREIGN KEY([Fk_Mill])
REFERENCES [dbo].[gen_Mill] ([Pk_Mill])
GO
ALTER TABLE [dbo].[Inv_Material] CHECK CONSTRAINT [FK_Inv_Material_gen_Mill]
GO
/****** Object:  ForeignKey [FK_Inv_Material_gen_PaperType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Material]  WITH NOCHECK ADD  CONSTRAINT [FK_Inv_Material_gen_PaperType] FOREIGN KEY([Fk_PaperType])
REFERENCES [dbo].[gen_PaperType] ([Pk_PaperType])
GO
ALTER TABLE [dbo].[Inv_Material] CHECK CONSTRAINT [FK_Inv_Material_gen_PaperType]
GO
/****** Object:  ForeignKey [FK_Inv_Material_gen_Unit]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Material]  WITH NOCHECK ADD  CONSTRAINT [FK_Inv_Material_gen_Unit] FOREIGN KEY([Fk_UnitId])
REFERENCES [dbo].[gen_Unit] ([Pk_Unit])
GO
ALTER TABLE [dbo].[Inv_Material] CHECK CONSTRAINT [FK_Inv_Material_gen_Unit]
GO
/****** Object:  ForeignKey [FK_Inv_Material_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Material]  WITH NOCHECK ADD  CONSTRAINT [FK_Inv_Material_gen_Vendor] FOREIGN KEY([VendorIdNumber])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[Inv_Material] CHECK CONSTRAINT [FK_Inv_Material_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_Inv_Material_Inv_MaterialCategory]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_Material]  WITH NOCHECK ADD  CONSTRAINT [FK_Inv_Material_Inv_MaterialCategory] FOREIGN KEY([Fk_MaterialCategory])
REFERENCES [dbo].[Inv_MaterialCategory] ([Pk_MaterialCategory])
GO
ALTER TABLE [dbo].[Inv_Material] CHECK CONSTRAINT [FK_Inv_Material_Inv_MaterialCategory]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentDetails_gen_Mill]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Mill] FOREIGN KEY([Fk_Mill])
REFERENCES [dbo].[gen_Mill] ([Pk_Mill])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentDetails] CHECK CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Mill]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentDetails_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentDetails] CHECK CONSTRAINT [FK_Inv_MaterialIndentDetails_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentDetails] CHECK CONSTRAINT [FK_Inv_MaterialIndentDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_MaterialIndentDetails_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIndentDetails_gen_Order] FOREIGN KEY([Fk_CustomerOrder])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentDetails] CHECK CONSTRAINT [FK_MaterialIndentDetails_gen_Order]
GO
/****** Object:  ForeignKey [FK_MaterialIndentDetails_MaterialIndent]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIndentDetails_MaterialIndent] FOREIGN KEY([Fk_MaterialOrderMasterId])
REFERENCES [dbo].[Inv_MaterialIndentMaster] ([Pk_MaterialOrderMasterId])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentDetails] CHECK CONSTRAINT [FK_MaterialIndentDetails_MaterialIndent]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentMaster_Branch]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentMaster_Branch] FOREIGN KEY([Fk_Branch])
REFERENCES [dbo].[Branch] ([Pk_BranchID])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentMaster] CHECK CONSTRAINT [FK_Inv_MaterialIndentMaster_Branch]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentMaster_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentMaster_ideUser] FOREIGN KEY([Fk_UserID])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentMaster] CHECK CONSTRAINT [FK_Inv_MaterialIndentMaster_ideUser]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentMaster_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentMaster_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentMaster] CHECK CONSTRAINT [FK_Inv_MaterialIndentMaster_wfStates]
GO
/****** Object:  ForeignKey [FK_Inv_MaterialIndentMaster_wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]  WITH CHECK ADD  CONSTRAINT [FK_Inv_MaterialIndentMaster_wgTenant] FOREIGN KEY([Fk_Tanent])
REFERENCES [dbo].[wgTenant] ([Pk_Tanent])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentMaster] CHECK CONSTRAINT [FK_Inv_MaterialIndentMaster_wgTenant]
GO
/****** Object:  ForeignKey [FK_MaterialIndent_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_MaterialIndentMaster]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIndent_gen_Vendor] FOREIGN KEY([Fk_VendorId])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[Inv_MaterialIndentMaster] CHECK CONSTRAINT [FK_MaterialIndent_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_Inv_PaymentDetails_Inv_Billing]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Inv_PaymentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Inv_PaymentDetails_Inv_Billing] FOREIGN KEY([Fk_InvoiceNo])
REFERENCES [dbo].[Inv_Billing] ([Pk_Invoice])
GO
ALTER TABLE [dbo].[Inv_PaymentDetails] CHECK CONSTRAINT [FK_Inv_PaymentDetails_Inv_Billing]
GO
/****** Object:  ForeignKey [FK_IssueReturn_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[IssueReturn]  WITH CHECK ADD  CONSTRAINT [FK_IssueReturn_ideUser] FOREIGN KEY([Fk_EnteredUserId])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[IssueReturn] CHECK CONSTRAINT [FK_IssueReturn_ideUser]
GO
/****** Object:  ForeignKey [FK_IssueReturnMaster_MaterialIssue]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[IssueReturn]  WITH CHECK ADD  CONSTRAINT [FK_IssueReturnMaster_MaterialIssue] FOREIGN KEY([Fk_IssueId])
REFERENCES [dbo].[MaterialIssue] ([Pk_MaterialIssueID])
GO
ALTER TABLE [dbo].[IssueReturn] CHECK CONSTRAINT [FK_IssueReturnMaster_MaterialIssue]
GO
/****** Object:  ForeignKey [FK_IssueReturnDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[IssueReturnDetails]  WITH CHECK ADD  CONSTRAINT [FK_IssueReturnDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[IssueReturnDetails] CHECK CONSTRAINT [FK_IssueReturnDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_IssueReturnDetails_IssueReturnMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[IssueReturnDetails]  WITH CHECK ADD  CONSTRAINT [FK_IssueReturnDetails_IssueReturnMaster] FOREIGN KEY([Fk_IssueReturnMasterId])
REFERENCES [dbo].[IssueReturn] ([Pk_IssueReturnMasterId])
GO
ALTER TABLE [dbo].[IssueReturnDetails] CHECK CONSTRAINT [FK_IssueReturnDetails_IssueReturnMaster]
GO
/****** Object:  ForeignKey [FK_ItemPartProperty_BoxSpecs]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[ItemPartProperty]  WITH CHECK ADD  CONSTRAINT [FK_ItemPartProperty_BoxSpecs] FOREIGN KEY([Fk_BoxSpecsID])
REFERENCES [dbo].[BoxSpecs] ([Pk_BoxSpecID])
GO
ALTER TABLE [dbo].[ItemPartProperty] CHECK CONSTRAINT [FK_ItemPartProperty_BoxSpecs]
GO
/****** Object:  ForeignKey [FK_Items_Layers]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Items_Layers]  WITH CHECK ADD  CONSTRAINT [FK_Items_Layers] FOREIGN KEY([Fk_PartId])
REFERENCES [dbo].[ItemPartProperty] ([Pk_PartPropertyID])
GO
ALTER TABLE [dbo].[Items_Layers] CHECK CONSTRAINT [FK_Items_Layers]
GO
/****** Object:  ForeignKey [FK_JC_Documents_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JC_Documents]  WITH CHECK ADD  CONSTRAINT [FK_JC_Documents_JobCardMaster] FOREIGN KEY([Fk_JCNo])
REFERENCES [dbo].[JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[JC_Documents] CHECK CONSTRAINT [FK_JC_Documents_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_JobCardDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardDetails]  WITH CHECK ADD  CONSTRAINT [FK_JobCardDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[JobCardDetails] CHECK CONSTRAINT [FK_JobCardDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_JobCardDetails_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardDetails]  WITH CHECK ADD  CONSTRAINT [FK_JobCardDetails_JobCardMaster] FOREIGN KEY([Fk_JobCardID])
REFERENCES [dbo].[JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[JobCardDetails] CHECK CONSTRAINT [FK_JobCardDetails_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_JobCardDetails_PaperStock]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardDetails]  WITH CHECK ADD  CONSTRAINT [FK_JobCardDetails_PaperStock] FOREIGN KEY([Fk_PaperStock])
REFERENCES [dbo].[PaperStock] ([Pk_PaperStock])
GO
ALTER TABLE [dbo].[JobCardDetails] CHECK CONSTRAINT [FK_JobCardDetails_PaperStock]
GO
/****** Object:  ForeignKey [FK_JobCardMaster_BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardMaster_BoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[JobCardMaster] CHECK CONSTRAINT [FK_JobCardMaster_BoxMaster]
GO
/****** Object:  ForeignKey [FK_JobCardMaster_gen_DeliverySchedule]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardMaster_gen_DeliverySchedule] FOREIGN KEY([Fk_Schedule])
REFERENCES [dbo].[gen_DeliverySchedule] ([Pk_DeliverySechedule])
GO
ALTER TABLE [dbo].[JobCardMaster] CHECK CONSTRAINT [FK_JobCardMaster_gen_DeliverySchedule]
GO
/****** Object:  ForeignKey [FK_JobCardMaster_gen_Order1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardMaster_gen_Order1] FOREIGN KEY([Fk_Order])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[JobCardMaster] CHECK CONSTRAINT [FK_JobCardMaster_gen_Order1]
GO
/****** Object:  ForeignKey [FK_JobCardMaster_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardMaster_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[JobCardMaster] CHECK CONSTRAINT [FK_JobCardMaster_wfStates]
GO
/****** Object:  ForeignKey [FK_JobCardPartsDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardPartsDetails]  WITH CHECK ADD  CONSTRAINT [FK_JobCardPartsDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[JobCardPartsDetails] CHECK CONSTRAINT [FK_JobCardPartsDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_JobCardPartsDetails_JobCardPartsMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardPartsDetails]  WITH CHECK ADD  CONSTRAINT [FK_JobCardPartsDetails_JobCardPartsMaster] FOREIGN KEY([Fk_JobCardPartsID])
REFERENCES [dbo].[JobCardPartsMaster] ([Pk_JobCardPartsID])
GO
ALTER TABLE [dbo].[JobCardPartsDetails] CHECK CONSTRAINT [FK_JobCardPartsDetails_JobCardPartsMaster]
GO
/****** Object:  ForeignKey [FK_JobCardPartsDetails_PaperStock]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardPartsDetails]  WITH CHECK ADD  CONSTRAINT [FK_JobCardPartsDetails_PaperStock] FOREIGN KEY([Fk_PaperStock])
REFERENCES [dbo].[PaperStock] ([Pk_PaperStock])
GO
ALTER TABLE [dbo].[JobCardPartsDetails] CHECK CONSTRAINT [FK_JobCardPartsDetails_PaperStock]
GO
/****** Object:  ForeignKey [FK_JobCardPartsMaster_gen_DeliverySchedule]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardPartsMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardPartsMaster_gen_DeliverySchedule] FOREIGN KEY([Fk_Schedule])
REFERENCES [dbo].[gen_DeliverySchedule] ([Pk_DeliverySechedule])
GO
ALTER TABLE [dbo].[JobCardPartsMaster] CHECK CONSTRAINT [FK_JobCardPartsMaster_gen_DeliverySchedule]
GO
/****** Object:  ForeignKey [FK_JobCardPartsMaster_gen_Order1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardPartsMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardPartsMaster_gen_Order1] FOREIGN KEY([Fk_Order])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[JobCardPartsMaster] CHECK CONSTRAINT [FK_JobCardPartsMaster_gen_Order1]
GO
/****** Object:  ForeignKey [FK_JobCardPartsMaster_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobCardPartsMaster]  WITH CHECK ADD  CONSTRAINT [FK_JobCardPartsMaster_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[JobCardPartsMaster] CHECK CONSTRAINT [FK_JobCardPartsMaster_wfStates]
GO
/****** Object:  ForeignKey [FK_JobProcess_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobProcess]  WITH CHECK ADD  CONSTRAINT [FK_JobProcess_JobCardMaster] FOREIGN KEY([Fk_JobCardID])
REFERENCES [dbo].[JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[JobProcess] CHECK CONSTRAINT [FK_JobProcess_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_JobProcess_ProcessMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobProcess]  WITH CHECK ADD  CONSTRAINT [FK_JobProcess_ProcessMaster] FOREIGN KEY([Fk_ProcessID])
REFERENCES [dbo].[ProcessMaster] ([Pk_Process])
GO
ALTER TABLE [dbo].[JobProcess] CHECK CONSTRAINT [FK_JobProcess_ProcessMaster]
GO
/****** Object:  ForeignKey [FK_JobWorkChild_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkChild]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkChild_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[JobWorkChild] CHECK CONSTRAINT [FK_JobWorkChild_Inv_Material]
GO
/****** Object:  ForeignKey [FK_JobWorkChild_JobWorkChild]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkChild]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkChild_JobWorkChild] FOREIGN KEY([Fk_JobID])
REFERENCES [dbo].[PartJobs] ([Pk_ID])
GO
ALTER TABLE [dbo].[JobWorkChild] CHECK CONSTRAINT [FK_JobWorkChild_JobWorkChild]
GO
/****** Object:  ForeignKey [FK_JobWorkReceivables_Inv_MaterialCategory]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkReceivables]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkReceivables_Inv_MaterialCategory] FOREIGN KEY([Fk_OutSrcDetID])
REFERENCES [dbo].[OutsourcingChild] ([Pk_SrcDetID])
GO
ALTER TABLE [dbo].[JobWorkReceivables] CHECK CONSTRAINT [FK_JobWorkReceivables_Inv_MaterialCategory]
GO
/****** Object:  ForeignKey [FK_JobWorkReceivables_JobWorkReceivables]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkReceivables]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkReceivables_JobWorkReceivables] FOREIGN KEY([Fk_OutSrcID])
REFERENCES [dbo].[Outsourcing] ([Pk_SrcID])
GO
ALTER TABLE [dbo].[JobWorkReceivables] CHECK CONSTRAINT [FK_JobWorkReceivables_JobWorkReceivables]
GO
/****** Object:  ForeignKey [FK_JobWorkReceivables_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkReceivables]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkReceivables_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[JobWorkReceivables] CHECK CONSTRAINT [FK_JobWorkReceivables_wfStates]
GO
/****** Object:  ForeignKey [FK_JobWorkRMStock_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkRMStock]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkRMStock_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[JobWorkRMStock] CHECK CONSTRAINT [FK_JobWorkRMStock_gen_Customer]
GO
/****** Object:  ForeignKey [FK_JobWorkRMStock_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JobWorkRMStock]  WITH CHECK ADD  CONSTRAINT [FK_JobWorkRMStock_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[JobWorkRMStock] CHECK CONSTRAINT [FK_JobWorkRMStock_Inv_Material]
GO
/****** Object:  ForeignKey [FK_JProcess_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JProcess]  WITH CHECK ADD  CONSTRAINT [FK_JProcess_JobCardMaster] FOREIGN KEY([Fk_JobCardID])
REFERENCES [dbo].[JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[JProcess] CHECK CONSTRAINT [FK_JProcess_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_JProcess_ProcessMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[JProcess]  WITH CHECK ADD  CONSTRAINT [FK_JProcess_ProcessMaster] FOREIGN KEY([Fk_ProcessID])
REFERENCES [dbo].[ProcessMaster] ([Pk_Process])
GO
ALTER TABLE [dbo].[JProcess] CHECK CONSTRAINT [FK_JProcess_ProcessMaster]
GO
/****** Object:  ForeignKey [FK_MachineMaintenance_gen_Machine]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MachineMaintenance]  WITH CHECK ADD  CONSTRAINT [FK_MachineMaintenance_gen_Machine] FOREIGN KEY([Fk_Machine])
REFERENCES [dbo].[gen_Machine] ([Pk_Machine])
GO
ALTER TABLE [dbo].[MachineMaintenance] CHECK CONSTRAINT [FK_MachineMaintenance_gen_Machine]
GO
/****** Object:  ForeignKey [FK_MaterialInwardD_gen_Mill]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialInwardD]  WITH CHECK ADD  CONSTRAINT [FK_MaterialInwardD_gen_Mill] FOREIGN KEY([Fk_Mill])
REFERENCES [dbo].[gen_Mill] ([Pk_Mill])
GO
ALTER TABLE [dbo].[MaterialInwardD] CHECK CONSTRAINT [FK_MaterialInwardD_gen_Mill]
GO
/****** Object:  ForeignKey [FK_MaterialInwardD_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialInwardD]  WITH CHECK ADD  CONSTRAINT [FK_MaterialInwardD_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[MaterialInwardD] CHECK CONSTRAINT [FK_MaterialInwardD_Inv_Material]
GO
/****** Object:  ForeignKey [FK_MaterialInwardD_MaterialInwardM]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialInwardD]  WITH CHECK ADD  CONSTRAINT [FK_MaterialInwardD_MaterialInwardM] FOREIGN KEY([Fk_Inward])
REFERENCES [dbo].[MaterialInwardM] ([Pk_Inward])
GO
ALTER TABLE [dbo].[MaterialInwardD] CHECK CONSTRAINT [FK_MaterialInwardD_MaterialInwardM]
GO
/****** Object:  ForeignKey [FK_MaterialInwardM_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialInwardM]  WITH CHECK ADD  CONSTRAINT [FK_MaterialInwardM_ideUser] FOREIGN KEY([Fk_InwardBy])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[MaterialInwardM] CHECK CONSTRAINT [FK_MaterialInwardM_ideUser]
GO
/****** Object:  ForeignKey [FK_MaterialInwardM_Inv_MaterialIndentMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialInwardM]  WITH CHECK ADD  CONSTRAINT [FK_MaterialInwardM_Inv_MaterialIndentMaster] FOREIGN KEY([Fk_Indent])
REFERENCES [dbo].[Inv_MaterialIndentMaster] ([Pk_MaterialOrderMasterId])
GO
ALTER TABLE [dbo].[MaterialInwardM] CHECK CONSTRAINT [FK_MaterialInwardM_Inv_MaterialIndentMaster]
GO
/****** Object:  ForeignKey [FK_MaterialInwardM_PurchaseOrderM]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialInwardM]  WITH CHECK ADD  CONSTRAINT [FK_MaterialInwardM_PurchaseOrderM] FOREIGN KEY([PONo])
REFERENCES [dbo].[PurchaseOrderM] ([Pk_PONo])
GO
ALTER TABLE [dbo].[MaterialInwardM] CHECK CONSTRAINT [FK_MaterialInwardM_PurchaseOrderM]
GO
/****** Object:  ForeignKey [FK_MaterialIssue_Branch]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssue]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssue_Branch] FOREIGN KEY([Fk_Branch])
REFERENCES [dbo].[Branch] ([Pk_BranchID])
GO
ALTER TABLE [dbo].[MaterialIssue] CHECK CONSTRAINT [FK_MaterialIssue_Branch]
GO
/****** Object:  ForeignKey [FK_MaterialIssue_gen_Order]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssue]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssue_gen_Order] FOREIGN KEY([Fk_OrderNo])
REFERENCES [dbo].[gen_Order] ([Pk_Order])
GO
ALTER TABLE [dbo].[MaterialIssue] CHECK CONSTRAINT [FK_MaterialIssue_gen_Order]
GO
/****** Object:  ForeignKey [FK_MaterialIssue_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssue]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssue_ideUser] FOREIGN KEY([Fk_UserID])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[MaterialIssue] CHECK CONSTRAINT [FK_MaterialIssue_ideUser]
GO
/****** Object:  ForeignKey [FK_MaterialIssue_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssue]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssue_JobCardMaster] FOREIGN KEY([Fk_JobCardID])
REFERENCES [dbo].[JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[MaterialIssue] CHECK CONSTRAINT [FK_MaterialIssue_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_MaterialIssue_wgTenant]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssue]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssue_wgTenant] FOREIGN KEY([Fk_Tanent])
REFERENCES [dbo].[wgTenant] ([Pk_Tanent])
GO
ALTER TABLE [dbo].[MaterialIssue] CHECK CONSTRAINT [FK_MaterialIssue_wgTenant]
GO
/****** Object:  ForeignKey [FK_MaterialIssueDetails_gen_Mill]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssueDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssueDetails_gen_Mill] FOREIGN KEY([Fk_Mill])
REFERENCES [dbo].[gen_Mill] ([Pk_Mill])
GO
ALTER TABLE [dbo].[MaterialIssueDetails] CHECK CONSTRAINT [FK_MaterialIssueDetails_gen_Mill]
GO
/****** Object:  ForeignKey [FK_MaterialIssueDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssueDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssueDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[MaterialIssueDetails] CHECK CONSTRAINT [FK_MaterialIssueDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_MaterialIssueDetails_MaterialIssue]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialIssueDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialIssueDetails_MaterialIssue] FOREIGN KEY([Fk_IssueID])
REFERENCES [dbo].[MaterialIssue] ([Pk_MaterialIssueID])
GO
ALTER TABLE [dbo].[MaterialIssueDetails] CHECK CONSTRAINT [FK_MaterialIssueDetails_MaterialIssue]
GO
/****** Object:  ForeignKey [FK_MaterialQuotationDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialQuotationDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialQuotationDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[MaterialQuotationDetails] CHECK CONSTRAINT [FK_MaterialQuotationDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_MaterialQuotationDetails_MaterialQuotationMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialQuotationDetails]  WITH CHECK ADD  CONSTRAINT [FK_MaterialQuotationDetails_MaterialQuotationMaster] FOREIGN KEY([Fk_QuotationRequest])
REFERENCES [dbo].[MaterialQuotationMaster] ([Pk_QuotationRequest])
GO
ALTER TABLE [dbo].[MaterialQuotationDetails] CHECK CONSTRAINT [FK_MaterialQuotationDetails_MaterialQuotationMaster]
GO
/****** Object:  ForeignKey [FK_MaterialQuotationMaster_MaterialQuotationMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[MaterialQuotationMaster]  WITH CHECK ADD  CONSTRAINT [FK_MaterialQuotationMaster_MaterialQuotationMaster] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[MaterialQuotationMaster] CHECK CONSTRAINT [FK_MaterialQuotationMaster_MaterialQuotationMaster]
GO
/****** Object:  ForeignKey [FK_OD_ID_FluteType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[OD_ID]  WITH CHECK ADD  CONSTRAINT [FK_OD_ID_FluteType] FOREIGN KEY([Fk_FluteID])
REFERENCES [dbo].[FluteType] ([Pk_FluteID])
GO
ALTER TABLE [dbo].[OD_ID] CHECK CONSTRAINT [FK_OD_ID_FluteType]
GO
/****** Object:  ForeignKey [FK_Outsource_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Outsourcing]  WITH CHECK ADD  CONSTRAINT [FK_Outsource_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[Outsourcing] CHECK CONSTRAINT [FK_Outsource_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_Outsourcing_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Outsourcing]  WITH CHECK ADD  CONSTRAINT [FK_Outsourcing_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[Outsourcing] CHECK CONSTRAINT [FK_Outsourcing_wfStates]
GO
/****** Object:  ForeignKey [FK_OursourcingChild_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[OutsourcingChild]  WITH CHECK ADD  CONSTRAINT [FK_OursourcingChild_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[OutsourcingChild] CHECK CONSTRAINT [FK_OursourcingChild_Inv_Material]
GO
/****** Object:  ForeignKey [FK_OutsourcingChild_Outsourcing]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[OutsourcingChild]  WITH CHECK ADD  CONSTRAINT [FK_OutsourcingChild_Outsourcing] FOREIGN KEY([Fk_PkSrcID])
REFERENCES [dbo].[Outsourcing] ([Pk_SrcID])
GO
ALTER TABLE [dbo].[OutsourcingChild] CHECK CONSTRAINT [FK_OutsourcingChild_Outsourcing]
GO
/****** Object:  ForeignKey [FK_PaperCertificate_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaperCertificate]  WITH CHECK ADD  CONSTRAINT [FK_PaperCertificate_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[PaperCertificate] CHECK CONSTRAINT [FK_PaperCertificate_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_PaperCertificate_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaperCertificate]  WITH CHECK ADD  CONSTRAINT [FK_PaperCertificate_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[PaperCertificate] CHECK CONSTRAINT [FK_PaperCertificate_Inv_Material]
GO
/****** Object:  ForeignKey [FK_PaperCertificate_PurchaseOrderM]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaperCertificate]  WITH CHECK ADD  CONSTRAINT [FK_PaperCertificate_PurchaseOrderM] FOREIGN KEY([Fk_PoNo])
REFERENCES [dbo].[PurchaseOrderM] ([Pk_PONo])
GO
ALTER TABLE [dbo].[PaperCertificate] CHECK CONSTRAINT [FK_PaperCertificate_PurchaseOrderM]
GO
/****** Object:  ForeignKey [FK_PaperCertificateDetails_PaperCertificate]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaperCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_PaperCertificateDetails_PaperCertificate] FOREIGN KEY([Fk_PkID])
REFERENCES [dbo].[PaperCertificate] ([Pk_Id])
GO
ALTER TABLE [dbo].[PaperCertificateDetails] CHECK CONSTRAINT [FK_PaperCertificateDetails_PaperCertificate]
GO
/****** Object:  ForeignKey [FK_PaperCertificateDetails_PaperChar]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaperCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_PaperCertificateDetails_PaperChar] FOREIGN KEY([Fk_Characteristics])
REFERENCES [dbo].[PaperChar] ([Pk_CharID])
GO
ALTER TABLE [dbo].[PaperCertificateDetails] CHECK CONSTRAINT [FK_PaperCertificateDetails_PaperChar]
GO
/****** Object:  ForeignKey [FK_PaperStock_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaperStock]  WITH NOCHECK ADD  CONSTRAINT [FK_PaperStock_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[PaperStock] CHECK CONSTRAINT [FK_PaperStock_Inv_Material]
GO
/****** Object:  ForeignKey [FK_PartJobs_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PartJobs]  WITH CHECK ADD  CONSTRAINT [FK_PartJobs_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[PartJobs] CHECK CONSTRAINT [FK_PartJobs_gen_Customer]
GO
/****** Object:  ForeignKey [FK_PartJobs_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PartJobs]  WITH CHECK ADD  CONSTRAINT [FK_PartJobs_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[PartJobs] CHECK CONSTRAINT [FK_PartJobs_wfStates]
GO
/****** Object:  ForeignKey [FK_PartJobsReturns_Inv_MaterialCategory]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PartJobsReturns]  WITH CHECK ADD  CONSTRAINT [FK_PartJobsReturns_Inv_MaterialCategory] FOREIGN KEY([PrdName])
REFERENCES [dbo].[Inv_MaterialCategory] ([Pk_MaterialCategory])
GO
ALTER TABLE [dbo].[PartJobsReturns] CHECK CONSTRAINT [FK_PartJobsReturns_Inv_MaterialCategory]
GO
/****** Object:  ForeignKey [FK_PartJobsReturns_PartJobs]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PartJobsReturns]  WITH CHECK ADD  CONSTRAINT [FK_PartJobsReturns_PartJobs] FOREIGN KEY([Fk_PJID])
REFERENCES [dbo].[PartJobs] ([Pk_ID])
GO
ALTER TABLE [dbo].[PartJobsReturns] CHECK CONSTRAINT [FK_PartJobsReturns_PartJobs]
GO
/****** Object:  ForeignKey [FK_PartJobsReturns_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PartJobsReturns]  WITH CHECK ADD  CONSTRAINT [FK_PartJobsReturns_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[PartJobsReturns] CHECK CONSTRAINT [FK_PartJobsReturns_wfStates]
GO
/****** Object:  ForeignKey [FK_Payment_Bank]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_Bank] FOREIGN KEY([Fk_Bank])
REFERENCES [dbo].[Bank] ([Pk_BankID])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_Bank]
GO
/****** Object:  ForeignKey [FK_Payment_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Payment]  WITH CHECK ADD  CONSTRAINT [FK_Payment_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[Payment] CHECK CONSTRAINT [FK_Payment_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_PaymentTrack_Inv_Billing]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PaymentTrack]  WITH CHECK ADD  CONSTRAINT [FK_PaymentTrack_Inv_Billing] FOREIGN KEY([Fk_Invno])
REFERENCES [dbo].[Inv_Billing] ([Pk_Invoice])
GO
ALTER TABLE [dbo].[PaymentTrack] CHECK CONSTRAINT [FK_PaymentTrack_Inv_Billing]
GO
/****** Object:  ForeignKey [FK_PendingTrack_Inv_MaterialIndentMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PendingTrack]  WITH CHECK ADD  CONSTRAINT [FK_PendingTrack_Inv_MaterialIndentMaster] FOREIGN KEY([Fk_Indent])
REFERENCES [dbo].[Inv_MaterialIndentMaster] ([Pk_MaterialOrderMasterId])
GO
ALTER TABLE [dbo].[PendingTrack] CHECK CONSTRAINT [FK_PendingTrack_Inv_MaterialIndentMaster]
GO
/****** Object:  ForeignKey [FK_POReturnDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[POReturnDetails]  WITH CHECK ADD  CONSTRAINT [FK_POReturnDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[POReturnDetails] CHECK CONSTRAINT [FK_POReturnDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_POReturnDetails_POReturnMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[POReturnDetails]  WITH CHECK ADD  CONSTRAINT [FK_POReturnDetails_POReturnMaster] FOREIGN KEY([Fk_PoRetID])
REFERENCES [dbo].[POReturnMaster] ([Pk_PoRetID])
GO
ALTER TABLE [dbo].[POReturnDetails] CHECK CONSTRAINT [FK_POReturnDetails_POReturnMaster]
GO
/****** Object:  ForeignKey [FK_POReturnMaster_PurchaseOrderM]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[POReturnMaster]  WITH CHECK ADD  CONSTRAINT [FK_POReturnMaster_PurchaseOrderM] FOREIGN KEY([Fk_PoNo])
REFERENCES [dbo].[PurchaseOrderM] ([Pk_PONo])
GO
ALTER TABLE [dbo].[POReturnMaster] CHECK CONSTRAINT [FK_POReturnMaster_PurchaseOrderM]
GO
/****** Object:  ForeignKey [FK_PurchaseOrderD_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PurchaseOrderD]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderD_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[PurchaseOrderD] CHECK CONSTRAINT [FK_PurchaseOrderD_Inv_Material]
GO
/****** Object:  ForeignKey [FK_PurchaseOrderD_PurchaseOrderM1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PurchaseOrderD]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderD_PurchaseOrderM1] FOREIGN KEY([Fk_PONo])
REFERENCES [dbo].[PurchaseOrderM] ([Pk_PONo])
GO
ALTER TABLE [dbo].[PurchaseOrderD] CHECK CONSTRAINT [FK_PurchaseOrderD_PurchaseOrderM1]
GO
/****** Object:  ForeignKey [FK_PurchaseOrderM_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PurchaseOrderM]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderM_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[PurchaseOrderM] CHECK CONSTRAINT [FK_PurchaseOrderM_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_PurchaseOrderM_Inv_MaterialIndentMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PurchaseOrderM]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderM_Inv_MaterialIndentMaster] FOREIGN KEY([Fk_Indent])
REFERENCES [dbo].[Inv_MaterialIndentMaster] ([Pk_MaterialOrderMasterId])
GO
ALTER TABLE [dbo].[PurchaseOrderM] CHECK CONSTRAINT [FK_PurchaseOrderM_Inv_MaterialIndentMaster]
GO
/****** Object:  ForeignKey [FK_PurchaseOrderM_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[PurchaseOrderM]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderM_wfStates] FOREIGN KEY([Fk_Status])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[PurchaseOrderM] CHECK CONSTRAINT [FK_PurchaseOrderM_wfStates]
GO
/****** Object:  ForeignKey [FK_QC_Documents_QualityCheck]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QC_Documents]  WITH CHECK ADD  CONSTRAINT [FK_QC_Documents_QualityCheck] FOREIGN KEY([Fk_QualityCheck])
REFERENCES [dbo].[QualityCheck] ([Pk_QualityCheck])
GO
ALTER TABLE [dbo].[QC_Documents] CHECK CONSTRAINT [FK_QC_Documents_QualityCheck]
GO
/****** Object:  ForeignKey [FK_QualityChild_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QualityChild]  WITH CHECK ADD  CONSTRAINT [FK_QualityChild_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[QualityChild] CHECK CONSTRAINT [FK_QualityChild_Inv_Material]
GO
/****** Object:  ForeignKey [FK_QualityChild_QualityCheck]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QualityChild]  WITH CHECK ADD  CONSTRAINT [FK_QualityChild_QualityCheck] FOREIGN KEY([FkQualityCheck])
REFERENCES [dbo].[QualityCheck] ([Pk_QualityCheck])
GO
ALTER TABLE [dbo].[QualityChild] CHECK CONSTRAINT [FK_QualityChild_QualityCheck]
GO
/****** Object:  ForeignKey [FK_QualityDocuments_QualityCheck]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QualityDocuments]  WITH NOCHECK ADD  CONSTRAINT [FK_QualityDocuments_QualityCheck] FOREIGN KEY([Fk_QualityCheck])
REFERENCES [dbo].[QualityCheck] ([Pk_QualityCheck])
GO
ALTER TABLE [dbo].[QualityDocuments] CHECK CONSTRAINT [FK_QualityDocuments_QualityCheck]
GO
/****** Object:  ForeignKey [FK_QuotationDetails_BoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QuotationDetails]  WITH CHECK ADD  CONSTRAINT [FK_QuotationDetails_BoxMaster] FOREIGN KEY([BoxID])
REFERENCES [dbo].[BoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[QuotationDetails] CHECK CONSTRAINT [FK_QuotationDetails_BoxMaster]
GO
/****** Object:  ForeignKey [FK_QuotationDetails_est_BoxEstimation]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QuotationDetails]  WITH CHECK ADD  CONSTRAINT [FK_QuotationDetails_est_BoxEstimation] FOREIGN KEY([Fk_EstimationID])
REFERENCES [dbo].[est_BoxEstimation] ([Pk_BoxEstimation])
GO
ALTER TABLE [dbo].[QuotationDetails] CHECK CONSTRAINT [FK_QuotationDetails_est_BoxEstimation]
GO
/****** Object:  ForeignKey [FK_QuotationDetails_QuotationMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QuotationDetails]  WITH CHECK ADD  CONSTRAINT [FK_QuotationDetails_QuotationMaster] FOREIGN KEY([Fk_QuotationID])
REFERENCES [dbo].[QuotationMaster] ([Pk_Quotation])
GO
ALTER TABLE [dbo].[QuotationDetails] CHECK CONSTRAINT [FK_QuotationDetails_QuotationMaster]
GO
/****** Object:  ForeignKey [FK_QuotationMaster_eq_Enquiry]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[QuotationMaster]  WITH CHECK ADD  CONSTRAINT [FK_QuotationMaster_eq_Enquiry] FOREIGN KEY([Fk_Enquiry])
REFERENCES [dbo].[eq_Enquiry] ([Pk_Enquiry])
GO
ALTER TABLE [dbo].[QuotationMaster] CHECK CONSTRAINT [FK_QuotationMaster_eq_Enquiry]
GO
/****** Object:  ForeignKey [FK_Receipt_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Receipt]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_gen_Customer] FOREIGN KEY([Fk_Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[Receipt] CHECK CONSTRAINT [FK_Receipt_gen_Customer]
GO
/****** Object:  ForeignKey [FK_Sample_JobCardDetails_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Sample_JobCardDetails]  WITH CHECK ADD  CONSTRAINT [FK_Sample_JobCardDetails_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[Sample_JobCardDetails] CHECK CONSTRAINT [FK_Sample_JobCardDetails_Inv_Material]
GO
/****** Object:  ForeignKey [FK_Sample_JobCardDetails_Sample_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Sample_JobCardDetails]  WITH CHECK ADD  CONSTRAINT [FK_Sample_JobCardDetails_Sample_JobCardMaster] FOREIGN KEY([Fk_JobCardID])
REFERENCES [dbo].[Sample_JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[Sample_JobCardDetails] CHECK CONSTRAINT [FK_Sample_JobCardDetails_Sample_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_Sample_JobCardMaster_SampleBoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Sample_JobCardMaster]  WITH CHECK ADD  CONSTRAINT [FK_Sample_JobCardMaster_SampleBoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[SampleBoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[Sample_JobCardMaster] CHECK CONSTRAINT [FK_Sample_JobCardMaster_SampleBoxMaster]
GO
/****** Object:  ForeignKey [FK_SampleBoxMaster_BoxType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleBoxMaster]  WITH CHECK ADD  CONSTRAINT [FK_SampleBoxMaster_BoxType] FOREIGN KEY([Fk_BoxType])
REFERENCES [dbo].[BoxType] ([Pk_BoxTypeID])
GO
ALTER TABLE [dbo].[SampleBoxMaster] CHECK CONSTRAINT [FK_SampleBoxMaster_BoxType]
GO
/****** Object:  ForeignKey [FK_SampleBoxMaster_FluteType]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleBoxMaster]  WITH CHECK ADD  CONSTRAINT [FK_SampleBoxMaster_FluteType] FOREIGN KEY([Fk_FluteType])
REFERENCES [dbo].[FluteType] ([Pk_FluteID])
GO
ALTER TABLE [dbo].[SampleBoxMaster] CHECK CONSTRAINT [FK_SampleBoxMaster_FluteType]
GO
/****** Object:  ForeignKey [FK_SampleBoxMaster_gen_Customer]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleBoxMaster]  WITH CHECK ADD  CONSTRAINT [FK_SampleBoxMaster_gen_Customer] FOREIGN KEY([Customer])
REFERENCES [dbo].[gen_Customer] ([Pk_Customer])
GO
ALTER TABLE [dbo].[SampleBoxMaster] CHECK CONSTRAINT [FK_SampleBoxMaster_gen_Customer]
GO
/****** Object:  ForeignKey [FK_SampleBoxSpecs_SampleBoxMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleBoxSpecs]  WITH CHECK ADD  CONSTRAINT [FK_SampleBoxSpecs_SampleBoxMaster] FOREIGN KEY([Fk_BoxID])
REFERENCES [dbo].[SampleBoxMaster] ([Pk_BoxID])
GO
ALTER TABLE [dbo].[SampleBoxSpecs] CHECK CONSTRAINT [FK_SampleBoxSpecs_SampleBoxMaster]
GO
/****** Object:  ForeignKey [FK_SampleItemPartProperty_SampleBoxSpecs]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleItemPartProperty]  WITH CHECK ADD  CONSTRAINT [FK_SampleItemPartProperty_SampleBoxSpecs] FOREIGN KEY([Fk_BoxSpecsID])
REFERENCES [dbo].[SampleBoxSpecs] ([Pk_BoxSpecID])
GO
ALTER TABLE [dbo].[SampleItemPartProperty] CHECK CONSTRAINT [FK_SampleItemPartProperty_SampleBoxSpecs]
GO
/****** Object:  ForeignKey [FK_SampleItems_Layers_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleItems_Layers]  WITH CHECK ADD  CONSTRAINT [FK_SampleItems_Layers_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[SampleItems_Layers] CHECK CONSTRAINT [FK_SampleItems_Layers_Inv_Material]
GO
/****** Object:  ForeignKey [FK_SampleItems_Layers_SampleItemPartProperty]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[SampleItems_Layers]  WITH CHECK ADD  CONSTRAINT [FK_SampleItems_Layers_SampleItemPartProperty] FOREIGN KEY([Fk_PartId])
REFERENCES [dbo].[SampleItemPartProperty] ([Pk_PartPropertyID])
GO
ALTER TABLE [dbo].[SampleItems_Layers] CHECK CONSTRAINT [FK_SampleItems_Layers_SampleItemPartProperty]
GO
/****** Object:  ForeignKey [FK_Semi_FinishedGoodsStock_Inv_MaterialCategory]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Semi_FinishedGoodsStock]  WITH CHECK ADD  CONSTRAINT [FK_Semi_FinishedGoodsStock_Inv_MaterialCategory] FOREIGN KEY([Fk_MatCatID])
REFERENCES [dbo].[Inv_MaterialCategory] ([Pk_MaterialCategory])
GO
ALTER TABLE [dbo].[Semi_FinishedGoodsStock] CHECK CONSTRAINT [FK_Semi_FinishedGoodsStock_Inv_MaterialCategory]
GO
/****** Object:  ForeignKey [FK_Semi_FinishedGoodsStock_JobCardMaster]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Semi_FinishedGoodsStock]  WITH CHECK ADD  CONSTRAINT [FK_Semi_FinishedGoodsStock_JobCardMaster] FOREIGN KEY([Fk_JobCardID])
REFERENCES [dbo].[JobCardMaster] ([Pk_JobCardID])
GO
ALTER TABLE [dbo].[Semi_FinishedGoodsStock] CHECK CONSTRAINT [FK_Semi_FinishedGoodsStock_JobCardMaster]
GO
/****** Object:  ForeignKey [FK_Stocks_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Stocks]  WITH CHECK ADD  CONSTRAINT [FK_Stocks_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[Stocks] CHECK CONSTRAINT [FK_Stocks_Inv_Material]
GO
/****** Object:  ForeignKey [FK_StockTransfer_Branch]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[StockTransfer]  WITH CHECK ADD  CONSTRAINT [FK_StockTransfer_Branch] FOREIGN KEY([Fk_FromBranchID])
REFERENCES [dbo].[Branch] ([Pk_BranchID])
GO
ALTER TABLE [dbo].[StockTransfer] CHECK CONSTRAINT [FK_StockTransfer_Branch]
GO
/****** Object:  ForeignKey [FK_StockTransfer_Branch1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[StockTransfer]  WITH CHECK ADD  CONSTRAINT [FK_StockTransfer_Branch1] FOREIGN KEY([Fk_ToBranchID])
REFERENCES [dbo].[Branch] ([Pk_BranchID])
GO
ALTER TABLE [dbo].[StockTransfer] CHECK CONSTRAINT [FK_StockTransfer_Branch1]
GO
/****** Object:  ForeignKey [FK_StockTransfer_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[StockTransfer]  WITH CHECK ADD  CONSTRAINT [FK_StockTransfer_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[StockTransfer] CHECK CONSTRAINT [FK_StockTransfer_Inv_Material]
GO
/****** Object:  ForeignKey [FK_Trans_Invoice_TransporterM]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Trans_Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Trans_Invoice_TransporterM] FOREIGN KEY([Fk_Transporter])
REFERENCES [dbo].[TransporterM] ([Pk_ID])
GO
ALTER TABLE [dbo].[Trans_Invoice] CHECK CONSTRAINT [FK_Trans_Invoice_TransporterM]
GO
/****** Object:  ForeignKey [FK_Trans_Payment_TransporterM]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Trans_Payment]  WITH CHECK ADD  CONSTRAINT [FK_Trans_Payment_TransporterM] FOREIGN KEY([Fk_Transporter])
REFERENCES [dbo].[TransporterM] ([Pk_ID])
GO
ALTER TABLE [dbo].[Trans_Payment] CHECK CONSTRAINT [FK_Trans_Payment_TransporterM]
GO
/****** Object:  ForeignKey [FK_Voucher_acc_AccountHeads]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[Voucher]  WITH CHECK ADD  CONSTRAINT [FK_Voucher_acc_AccountHeads] FOREIGN KEY([Fk_AccountHeadId])
REFERENCES [dbo].[acc_AccountHeads] ([Pk_AccountHeadId])
GO
ALTER TABLE [dbo].[Voucher] CHECK CONSTRAINT [FK_Voucher_acc_AccountHeads]
GO
/****** Object:  ForeignKey [FK_wfWorkflowPaths_wfStates]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[wfWorkflowPaths]  WITH CHECK ADD  CONSTRAINT [FK_wfWorkflowPaths_wfStates] FOREIGN KEY([CurrentState])
REFERENCES [dbo].[wfStates] ([Pk_State])
GO
ALTER TABLE [dbo].[wfWorkflowPaths] CHECK CONSTRAINT [FK_wfWorkflowPaths_wfStates]
GO
/****** Object:  ForeignKey [FK_wfWorkflowPaths_wfWorkflowRoles1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[wfWorkflowPaths]  WITH CHECK ADD  CONSTRAINT [FK_wfWorkflowPaths_wfWorkflowRoles1] FOREIGN KEY([Fk_WfRole])
REFERENCES [dbo].[wfWorkflowRoles] ([Pk_WfRole])
GO
ALTER TABLE [dbo].[wfWorkflowPaths] CHECK CONSTRAINT [FK_wfWorkflowPaths_wfWorkflowRoles1]
GO
/****** Object:  ForeignKey [FK_wfWorkflowRoleUser_ideUser]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[wfWorkflowRoleUser]  WITH CHECK ADD  CONSTRAINT [FK_wfWorkflowRoleUser_ideUser] FOREIGN KEY([Fk_User])
REFERENCES [dbo].[ideUser] ([Pk_Users])
GO
ALTER TABLE [dbo].[wfWorkflowRoleUser] CHECK CONSTRAINT [FK_wfWorkflowRoleUser_ideUser]
GO
/****** Object:  ForeignKey [FK_wfWorkflowRoleUser_wfWorkflowRoles1]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[wfWorkflowRoleUser]  WITH CHECK ADD  CONSTRAINT [FK_wfWorkflowRoleUser_wfWorkflowRoles1] FOREIGN KEY([Fk_Role])
REFERENCES [dbo].[wfWorkflowRoles] ([Pk_WfRole])
GO
ALTER TABLE [dbo].[wfWorkflowRoleUser] CHECK CONSTRAINT [FK_wfWorkflowRoleUser_wfWorkflowRoles1]
GO
/****** Object:  ForeignKey [FK_WireCertificate_gen_Vendor]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[WireCertificate]  WITH CHECK ADD  CONSTRAINT [FK_WireCertificate_gen_Vendor] FOREIGN KEY([Fk_Vendor])
REFERENCES [dbo].[gen_Vendor] ([Pk_Vendor])
GO
ALTER TABLE [dbo].[WireCertificate] CHECK CONSTRAINT [FK_WireCertificate_gen_Vendor]
GO
/****** Object:  ForeignKey [FK_WireCertificate_Inv_Material]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[WireCertificate]  WITH CHECK ADD  CONSTRAINT [FK_WireCertificate_Inv_Material] FOREIGN KEY([Fk_Material])
REFERENCES [dbo].[Inv_Material] ([Pk_Material])
GO
ALTER TABLE [dbo].[WireCertificate] CHECK CONSTRAINT [FK_WireCertificate_Inv_Material]
GO
/****** Object:  ForeignKey [FK_WireCertificateDetails_WireCertificate]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[WireCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_WireCertificateDetails_WireCertificate] FOREIGN KEY([Fk_ID])
REFERENCES [dbo].[WireCertificate] ([Pk_Id])
GO
ALTER TABLE [dbo].[WireCertificateDetails] CHECK CONSTRAINT [FK_WireCertificateDetails_WireCertificate]
GO
/****** Object:  ForeignKey [FK_WireCertificateDetails_WireChar]    Script Date: 11/23/2018 12:14:02 ******/
ALTER TABLE [dbo].[WireCertificateDetails]  WITH CHECK ADD  CONSTRAINT [FK_WireCertificateDetails_WireChar] FOREIGN KEY([Fk_Params])
REFERENCES [dbo].[WireChar] ([Pk_CharID])
GO
ALTER TABLE [dbo].[WireCertificateDetails] CHECK CONSTRAINT [FK_WireCertificateDetails_WireChar]
GO
