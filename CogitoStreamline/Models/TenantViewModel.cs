﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CogitoStreamline.Models
{
    public class TenantViewModel
    {
        public List<TenantDto> Tenants { get; set; }
    }

    public class TenantDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public bool Active { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid LastActionBy { get; set; }
        public DateTime LastAction { get; set; }
    }
}