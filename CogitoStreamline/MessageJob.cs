﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CogitoStreamline
{
    [PersistJobDataAfterExecution, DisallowConcurrentExecution]
    internal class MessageJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine("Fired @ {0}", context.FireTimeUtc);

            Console.WriteLine("MyKey: {0}", MyKey);
            Console.WriteLine("MyTriggerKey: {0}", MyTriggerKey);

            if (!context.MergedJobDataMap.Contains("MyNewVal"))
            {
                context.JobDetail.JobDataMap.Add("MyNewVal", "MyVal");
                Console.WriteLine("ValueAdded");
            }

            Console.WriteLine(
                        "JobDetails DataCount: {0}, Trigger DataCount:{1}, Merged Count: {2}",
                        context.JobDetail.JobDataMap.Count,
                        context.Trigger.JobDataMap.Count,
                        context.MergedJobDataMap.Count);
        }

        public string MyKey { get; set; }

        public string MyTriggerKey { get; set; }
    }
}
