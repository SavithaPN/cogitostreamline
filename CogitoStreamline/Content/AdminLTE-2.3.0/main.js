//Initialize Enabler
if (Enabler.isInitialized()) {
    init();
} else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, init);
}
//Run when Enabler is ready
function init(){
    //politeInit();
    //Load in Javascript

    if(Enabler.isPageLoaded()){
        politeInit();
    }else{
        Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, politeInit);
    }
}

function politeInit(){

    console.log ("!!! start polite load");

    LoadScriptList (['./vendor','./animation'],scriptsLoadedCallback.bind (this) )

}


var canvas, stage, exportRoot;

function scriptsLoadedCallback (e) {

    console.log ("!!! scripts loaded callback");

    canvas = document.getElementById("canvas");
    images = images||{};


    if (lib.properties.manifest.length >0) {

        var loader = new createjs.LoadQueue(false,null, "Anonymous");

        loader.addEventListener("complete", handleComplete);
        loader.loadManifest(lib.properties.manifest);
    }else {


        handleComplete();

    }




}


function handleComplete(evt) {

    console.log ();
    exportRoot = new lib.GCP_Networking_en_300x250_HTML();

    stage = new createjs.Stage(canvas);
    stage.addChild(exportRoot);
    stage.enableMouseOver(24);
    stage.update();

    createjs.Ticker.setFPS(lib.properties.fps);
    createjs.Ticker.addEventListener("tick", stage);
}