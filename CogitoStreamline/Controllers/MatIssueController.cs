﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamline.Controllers;
using Microsoft.Reporting.WebForms;
using System.IO;
using CrystalDecisions.Shared;
using System.Drawing.Printing;
using System.Data;

namespace CogitoStreamline.Controllers
{
    public class MatIssueController : CommonController
    {
        //
        // GET: /MaterialIndent/
        //Customer oCustomer = new Customer();
        WebGareCore.DomainModel.Tenants oTanent = new WebGareCore.DomainModel.Tenants();
        Issue oIssue = new Issue();

        public MatIssueController()
        {
            oDoaminObject = new Issue();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Material Issue";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public ActionResult IssueRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_MaterialIssueID"]);

                List<Vw_JcIssues> BillList = new List<Vw_JcIssues>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_JcIssues.Where(x => x.Pk_MaterialIssueID == sInvno).Select(x => x).OfType<Vw_JcIssues>().ToList();
 //                SELECT "Vw_JcIssues"."Pk_JobCardDet", "Vw_JcIssues"."RollNo", "Vw_JcIssues"."Name", "Vw_JcIssues"."Pk_Material", 
 //               "Vw_JcIssues"."Fk_BoxID", "Vw_JcIssues"."Pk_JobCardID", "Vw_JcIssues"."Expr1", "Vw_JcIssues"."JDate",
 //               "Vw_JcIssues"."CustomerName", "Vw_JcIssues"."Pk_MaterialIssueID", "Vw_JcIssues"."IssueDate", "Vw_JcIssues"."IssueQty"
 //FROM   "Balaji"."dbo"."Vw_JcIssues" "Vw_JcIssues"
 //ORDER BY "Vw_JcIssues"."Pk_MaterialIssueID", "Vw_JcIssues"."Pk_Material"




               
                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("JDate");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Pk_MaterialIssueID");
                    dt.Columns.Add("IssueDate");
                    dt.Columns.Add("IssueQty");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("Expr1");

                    foreach (Vw_JcIssues entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row["JDate"] = entity.JDate;
                        row["CustomerName"] = entity.CustomerName;
                        row["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                        row["IssueQty"] = entity.IssueQty;
                        row["IssueDate"] = entity.IssueDate;
                        row["ColorName"] = entity.ColorName;
                        row["MillName"] = entity.MillName;
                        row["Expr1"] = entity.Expr1;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.Issue orp = new CogitoStreamline.Report.Issue();

                    orp.Load("@\\Report\\Issue.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Issue" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult IssueDevRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_MaterialIssueID"]);

                List<Vw_IssuesDev> BillList = new List<Vw_IssuesDev>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_IssuesDev.Where(x => x.Pk_MaterialIssueID == sInvno).Select(x => x).OfType<Vw_IssuesDev>().ToList();
                //                SELECT "Vw_IssuesDev"."Pk_JobCardDet", "Vw_IssuesDev"."RollNo", "Vw_IssuesDev"."Name", "Vw_IssuesDev"."Pk_Material", 
                //               "Vw_IssuesDev"."Fk_BoxID", "Vw_IssuesDev"."Pk_JobCardID", "Vw_IssuesDev"."Expr1", "Vw_IssuesDev"."JDate",
                //               "Vw_IssuesDev"."CustomerName", "Vw_IssuesDev"."Pk_MaterialIssueID", "Vw_IssuesDev"."IssueDate", "Vw_IssuesDev"."IssueQty"
                //FROM   "Balaji"."dbo"."Vw_IssuesDev" "Vw_IssuesDev"
                //ORDER BY "Vw_IssuesDev"."Pk_MaterialIssueID", "Vw_IssuesDev"."Pk_Material"





                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    //dt.Columns.Add("RollNo");
                    dt.Columns.Add("IssueMat");
                    dt.Columns.Add("Pk_LayerID");
                    dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("BoxMat");
                    dt.Columns.Add("Pk_BoxID");
                    dt.Columns.Add("Pk_MaterialIssueID");
                    dt.Columns.Add("Pk_MaterialIssueDetailsID");
                    dt.Columns.Add("IssuePk");
                    dt.Columns.Add("BoxPk");
                    dt.Columns.Add("Name");
                    //dt.Columns.Add("Expr1");

                    foreach (Vw_IssuesDev entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["IssuePk"] = entity.IssuePk;
                        row["IssueMat"] = entity.IssueMat;
                        row["Pk_LayerID"] = entity.Pk_LayerID;
                        row["Pk_JobCardID"] = entity.Pk_JobCardID;
                        row["BoxMat"] = entity.BoxMat;
                        row["Pk_BoxID"] = entity.Pk_BoxID;
                        row["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                        row["Pk_MaterialIssueDetailsID"] = entity.Pk_MaterialIssueDetailsID;
                        row["BoxPk"] = entity.BoxPk;
                        row["Name"] = entity.Name;
                        //row["MillName"] = entity.MillName;
                        //row["Expr1"] = entity.Expr1;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.MatIssueDeviations orp = new CogitoStreamline.Report.MatIssueDeviations();

                    orp.Load("@\\Report\\MatIssueDeviations.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "IssueDev" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();
                    //////////////////////
                   
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult IssueDevRepForIssue(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_MaterialIssueID"]);

                List<Vw_JcIssues> BillList = new List<Vw_JcIssues>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_JcIssues.Where(x => x.Pk_MaterialIssueID == sInvno).Select(x => x).OfType<Vw_JcIssues>().ToList();
                //                SELECT "Vw_JcIssues"."Pk_JobCardDet", "Vw_JcIssues"."RollNo", "Vw_JcIssues"."Name", "Vw_JcIssues"."Pk_Material", 
                //               "Vw_JcIssues"."Fk_BoxID", "Vw_JcIssues"."Pk_JobCardID", "Vw_JcIssues"."Expr1", "Vw_JcIssues"."JDate",
                //               "Vw_JcIssues"."CustomerName", "Vw_JcIssues"."Pk_MaterialIssueID", "Vw_JcIssues"."IssueDate", "Vw_JcIssues"."IssueQty"
                //FROM   "Balaji"."dbo"."Vw_JcIssues" "Vw_JcIssues"
                //ORDER BY "Vw_JcIssues"."Pk_MaterialIssueID", "Vw_JcIssues"."Pk_Material"





                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    //dt.Columns.Add("IssueMat");
                    //dt.Columns.Add("Pk_LayerID");
                    dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("Pk_MaterialIssueID");
                    //dt.Columns.Add("BoxMat");
                    dt.Columns.Add("Fk_BoxID");
                    dt.Columns.Add("Pk_Material");
                    //dt.Columns.Add("Pk_MaterialIssueDetailsID");
                    //dt.Columns.Add("IssuePk");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("Expr1");

                    foreach (Vw_JcIssues entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                        row["Fk_BoxID"] = entity.Fk_BoxID;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Name"] = entity.Name;
                        row["Pk_JobCardID"] = entity.Pk_JobCardID;
                        //row["BoxMat"] = entity.BoxMat;
                        //row["Pk_BoxID"] = entity.Pk_BoxID;
                        //row["Pk_MaterialIssueID"] = entity.Pk_MaterialIssueID;
                        //row["Pk_MaterialIssueDetailsID"] = entity.Pk_MaterialIssueDetailsID;
                        //row["BoxPk"] = entity.BoxPk;
                        row["ColorName"] = entity.ColorName;
                        row["MillName"] = entity.MillName;
                        row["Expr1"] = entity.Expr1;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.MatIssueDeviations1 orp = new CogitoStreamline.Report.MatIssueDeviations1();

                    orp.Load("@\\Report\\MatIssueDeviations1.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "IssueDevIss"  + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();
                    //////////////////////

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
   
        //public ActionResult IssReport(string data = "")
        //{
        //    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    int sIssno = Convert.ToInt32(oValues["Pk_MaterialIssueID"]);


        //    var id = "PDF";
        //    //List<string> c  ;
        //    var reportStream = System.Reflection.Assembly.GetExecutingAssembly();
        //    // GetManifestResourceStream("ReportName.rdlc");
        //    // localReport.LoadReportDefinition(reportStream); 
        //    //ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
        //    LocalReport lr = new LocalReport();
        //    string path = Path.Combine(Server.MapPath("~/Report"), "Issue.rdlc");
        //    if (System.IO.File.Exists(path))
        //    {
        //        lr.ReportPath = path;
        //    }
        //    else
        //    {
        //        return View("Index");
        //    }
        //    List<Vw_Issue> cn = new List<Vw_Issue>();
        //    using (CogitoStreamLineModel.CogitoStreamLineEntities CL = new CogitoStreamLineModel.CogitoStreamLineEntities())
        //    {
        //        cn = CL.Vw_Issue.Where(x => x.Pk_MaterialIssueID == sIssno).Select(x => x).OfType<Vw_Issue>().ToList();

        //    }

        //    ReportDataSource rd = new ReportDataSource("DataSet1", cn);
        //    lr.DataSources.Add(rd);
        //    string reportType = id;
        //    string mimeType;
        //    string encoding;
        //    //string fileNameExtension;


        //    //string deviceInfo =
        //    //    "<DeviceInfo>" +
        //    //    "<OutputFormat>" + id + "</OutputFormat>" +
        //    //    "<PageWidth>8.5in</PageWidth>" +
        //    //    "<PageHeight>11in</PageHeight>" +
        //    //    "<MarginTop>0.5in</MarginTop>" +
        //    //    "<MarginLeft>1in</MarginLeft>" +
        //    //    "<MarginRight>1in</MarginRight>" +
        //    //    "<MarginBottom>0.5in</MarginBottom>" +
        //    //    "</DeviceInfo>";

        //    Warning[] warnings;
        //    //string[] streams;
        //    //byte[] renderedBytes;

        //    string pdfPath = Server.MapPath("~/ConvertPDF/" + "MaterialIssue" + ".pdf");
        //    FileInfo file = new FileInfo(pdfPath);
        //    if (file.Exists)
        //    {
        //        file.Delete();
        //    }

        //    string[] streamids;
        //    //string mimeType;
        //    //string encoding;
        //    string filenameExtension;
        //    ReportViewer reportViewer = new ReportViewer();
        //    reportViewer.LocalReport.ReportPath = path;
        //    reportViewer.LocalReport.DataSources.Add(rd);
        //    byte[] bytes = reportViewer.LocalReport.Render(
        //        "PDF", null, out mimeType, out encoding, out filenameExtension,
        //        out streamids, out warnings);

        //    using (FileStream fs = new FileStream(pdfPath, FileMode.Create))
        //    {
        //        fs.Write(bytes, 0, bytes.Length);
        //    }


        //    // renderedBytes = "";
        //    //renderedBytes = lr.Render(
        //    //    reportType, deviceInfo,
        //    //    out mimeType,
        //    //    out encoding,
        //    //    out fileNameExtension,
        //    //    out streams,
        //    //    out warnings);

        //    return null;

        //}



        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Issue oMaterialIssue = oDoaminObject as Issue;
                oMaterialIssue.ID = decimal.Parse(pId);

                MaterialIssue oAtualObject = oMaterialIssue.DAO as MaterialIssue;

                int i = -1;

                var oMaterialIssueDetails = oAtualObject.MaterialIssueDetails.Select(p => new
                {
                    Pk_MaterialIssueDetailsID = p.Pk_MaterialIssueDetailsID,
                    slno = ++i,
                    Quantity = p.Quantity,

                    //RequiredDate = p.RequiredDate != null ? DateTime.Parse(p.RequiredDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_Material = p.Fk_Material,
                    txtFk_Material = p.Inv_Material.Name,
                    Fk_IssueID = p.Fk_IssueID,
                    Pk_StockID=p.Pk_StockID,
                    RollNo=p.RollNo,
                    Color = p.Inv_Material.gen_Color.ColorName,
                    Mill = p.Inv_Material.gen_Mill.MillName,

                });


                //Create a anominious object here to break the circular reference
                var oMaterialIssueToDisplay = new
                {
                    Pk_MaterialIssueID = oAtualObject.Pk_MaterialIssueID,
                    IssueDate = DateTime.Parse(oAtualObject.IssueDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_UserID = oAtualObject.Fk_UserID,
                    Fk_OrderNo = oAtualObject.Fk_OrderNo,
                    Fk_Tanent = oAtualObject.Fk_Tanent,
                    DCNo = oAtualObject.DCNo,
                    Fk_JobCardID = oAtualObject.Fk_JobCardID,
                    MaterialIssueDetails = Json(oMaterialIssueDetails).Data
                    //Fk_VendorId,
                };

                return Json(new { success = true, data = oMaterialIssueToDisplay });
            }
            else
            {
                var oMaterialIssueToDisplay = new MaterialIssue();
                return Json(new { success = true, data = oMaterialIssueToDisplay });
            }
        }

        [HttpPost]
        public JsonResult MaterialIssueListByFiter(string Pk_MaterialIssueID = null, string IssueDate = "",  string Fk_UserID = "", string Fk_Tanent = "", string Fk_OrderNo = "", string DCNo = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialIssueID", Pk_MaterialIssueID));
                oSearchParams.Add(new SearchParameter("IssueDate", IssueDate));
                oSearchParams.Add(new SearchParameter("Fk_UserID", Fk_UserID));
                oSearchParams.Add(new SearchParameter("Fk_Tanent", Fk_Tanent));
                oSearchParams.Add(new SearchParameter("Fk_OrderNo", Fk_OrderNo));
                oSearchParams.Add(new SearchParameter("DCNo", DCNo));
            
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialIssuesToDisplayObjects = oSearchResult.ListOfRecords;
                List<MaterialIssue> oMaterialIssueObjects = oMaterialIssuesToDisplayObjects.Select(p => p).OfType<MaterialIssue>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialIssuesToDisplay = oMaterialIssueObjects.Select(p => new
                {

                    Pk_MaterialIssueID = p.Pk_MaterialIssueID,
                    IssueDate = p.IssueDate != null ? DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_OrderNo = p.Fk_OrderNo,
                    productVal=p.gen_Order.Product,
                    Fk_UserID = p.Fk_UserID,
                    //StateName = p.wfState.State,
                    Fk_Tanent = p.Fk_Tanent != null ? p.wgTenant.TanentName : "",
                    DCNo = p.DCNo,
                    JobCardID=p.Fk_JobCardID,
                    CustName=p.gen_Order.gen_Customer.CustomerName,
                    BoxID=p.JobCardMaster.BoxMaster.Name,
                    PName=p.JobCardMaster.gen_DeliverySchedule.ItemPartProperty.PName,
                    //Branch = p.Fk_Tanent != null ? p.gen_Branch.BranchName : ""
                    //   Branch = p.Fk_Tanent != null ? p.wgTanent.TenantName : ""
                }).ToList();

                return Json(new { Result = "OK", Records = oMaterialIssuesToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult IssueGetRec(string Pk_MaterialIssueID = "", string Pk_JobCardID = "", string Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialIssueID", Pk_MaterialIssueID));
                oSearchParams.Add(new SearchParameter("Pk_JobCardID", Pk_JobCardID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oIssue.SearchIssueDetails(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PapDistIssueList> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_PapDistIssueList>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_MaterialIssueID=p.Pk_MaterialIssueID,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    RollNo = p.RollNo,
                    Pk_JobCardID = p.Pk_JobCardID,
                    Fk_Order = p.Fk_Order,
                    Color=p.ColorName,
                    Mill=p.MillName,
                    Pk_BoxID=p.Pk_BoxID,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }




        [HttpPost]
        public JsonResult JCAssignedGetRec(string Pk_MaterialIssueID = "", string Pk_JobCardID = "", string Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MaterialIssueID", Pk_MaterialIssueID));
                oSearchParams.Add(new SearchParameter("Pk_JobCardID", Pk_JobCardID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oIssue.SearchAssDetails(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_AssignedStock> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_AssignedStock>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                 Pk_PaperStock = p.Pk_PaperStock,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    RollNo = p.RollNo,
                    Pk_JobCardID = p.Pk_JobCardID,
                 Pk_Material = p.Pk_Material,
                 Color = p.ColorName,
                 Mill = p.MillName,
                 PKMill=p.Pk_Mill,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }


        [HttpPost]
        public override JsonResult Save(string data)
        {
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);



            data = JsonConvert.SerializeObject(values);
            //this.CurrentObject = null;
            return base.Save(data);


        }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}

