﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class ODIDController : CommonController
    {
        //
        // GET: /Mill/
        ODID oMaterial = new ODID();
        ODID ID_Val = new ODID();
        FType oFType = new FType();

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        public ODIDController()
        {
            oDoaminObject = new ODID();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "ID To OD Conversions";
            return base.Index();
        }


      

        [HttpPost]
        public JsonResult getFluteType(string pId = "")
        {
            List<FluteType> oListOfBoxType = oFType.Search(null).ListOfRecords.OfType<FluteType>().ToList();

            var oBoxToDisplay = oListOfBoxType.Select(p => new
            {
                Name = p.FluteName,
                Id = p.Pk_FluteID

            });

            return Json(oBoxToDisplay);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                ODID oMaterial = oDoaminObject as ODID;
                oMaterial.ID = decimal.Parse(pId);
                OD_ID oAtualObject = oMaterial.DAO as OD_ID;
                //Create a anominious object here to break the circular reference

                var oMaterialToDisplay = new
                {

                    Pk_ID=oAtualObject.Pk_ID,
                    Fk_FluteID=oAtualObject.Fk_FluteID,
                    Length=oAtualObject.Length,
                    Width=oAtualObject.Width,
                    Height=oAtualObject.Height,
                    PlyValue=oAtualObject.PlyValue

                    
                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new OD_ID();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }


        //[HttpPost]
        //public JsonResult CapDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sName = oValues["Name"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Name", sName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oBoardToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<OD_ID> oBoardObjects = oBoardToDisplayObjects.Select(p => p).OfType<OD_ID>().ToList();

        //        var oBoardToDisplay = oBoardObjects.Select(p => new
        //        {
        //            Pk_Material = p.Pk_Material,
        //            Name = p.Name
        //        }).ToList().OrderBy(s => s.Name);

        //        return Json(new { Success = true, Records = oBoardToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult ODIDListByFiter(string Length = "", string Height = "", string Width = "", string ID = "", string PlyV = "", string FTypeVal = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Height", Height));
                oSearchParams.Add(new SearchParameter("Length", Length));
                oSearchParams.Add(new SearchParameter("Width", Width));
                oSearchParams.Add(new SearchParameter("ID", ID));
                oSearchParams.Add(new SearchParameter("PlyV", PlyV));
                oSearchParams.Add(new SearchParameter("FTypeVal", FTypeVal));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialToDisplayObjects = oSearchResult.ListOfRecords;
                List<OD_ID> oMaterialObjects = oMaterialToDisplayObjects.Select(p => p).OfType<OD_ID>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = oMaterialObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    Fk_FluteID = p.FluteType.FluteName,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    PlyValue = p.PlyValue


                }).ToList().OrderBy(s => s.Pk_ID);

                return Json(new { Result = "OK", Records = oMaterialToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult LWHDetails(string data="")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string FluteType = oValues["FluteType"];
                string Ply = oValues["Ply"];

                //decimal eId1 = Convert.ToDecimal(oValues["BoxId"]);
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("FluteType", FluteType));
                oSearchParams.Add(new SearchParameter("Ply", Ply));

                //oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                //oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = ID_Val.SearchValueName(oSearchParams);

                List<EntityObject> oEnquirysToDisplayObjects = oSearchResult.ListOfRecords;
                List<OD_ID> oEnquiryObjects = oEnquirysToDisplayObjects.Select(p => p).OfType<OD_ID>().ToList();

                var oEnquiryBoxToDisplay = oEnquiryObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    Fk_FluteID = p.FluteType.FluteName,
                    Length = p.Length,
                    Width = p.Width,
                    Height = p.Height,
                    PlyValue = p.PlyValue
                }).ToList();



                return Json(new { Result = "OK", Records = oEnquiryBoxToDisplay, TotalRecordCount = oEnquiryBoxToDisplay.Count() });
            }
            catch (Exception ex)
            {
                //return Json(new { Result = "ERROR", Message = ex.Message });
                return Json(new { Result = "ERROR", Message = "Select Enquiry No. & Proceed" });
            }
        }

            }
          
        

    }
//}
