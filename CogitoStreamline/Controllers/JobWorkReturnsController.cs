﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;
using CrystalDecisions.Shared;
using System.Drawing.Printing;
using System.Data;

namespace CogitoStreamLine.Controllers
{

    public class JobWorkReturnsController : CommonController
    {
        Mat_Category oMatCat = new Mat_Category();
        Country oCountry = new Country();
        public JobWorkReturnsController()
        {
            oDoaminObject = new PieceWorkReturns();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Job Work-In Delivery";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oMatCat.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                PieceWorkReturns oCity = oDoaminObject as PieceWorkReturns;
                oCity.ID = decimal.Parse(pId);
                PartJobsReturns oAtualObject = oCity.DAO as PartJobsReturns;
                //Create a anominious object here to break the circular reference

                var oCityToDisplay = new
                {
                    Pk_ID = oAtualObject.Pk_ID,
                    //CityName = oAtualObject.CityName,
                    //Fk_StateID = oAtualObject.Fk_StateID,
                    //Country = oAtualObject.Country

                };

                return Json(new { success = true, data = oCityToDisplay });
            }
            else
            {
                var oCityToDisplay = new PartJobsReturns();
                return Json(new { success = true, data = oCityToDisplay });
            }
        }


        [HttpPost]
        public JsonResult getMatCategory(string pId = "")
        {
            List<Inv_MaterialCategory> oListOfState = oMatCat.Search(null).ListOfRecords.OfType<Inv_MaterialCategory>().ToList();

            var oStateToDisplay = oListOfState.Where(p => p.Pk_MaterialCategory > 4 && p.Pk_MaterialCategory < 16).Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_MaterialCategory
            });

            return Json(oStateToDisplay);
        }

        [HttpPost]
        public JsonResult getCountry(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }

        [HttpPost]
        public JsonResult JobsReturnsListByFiter(string Pk_ID = "", string FromDate = "", string ToDate = "", string CityName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("CityName", CityName));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCityToDisplayObjects = oSearchResult.ListOfRecords;
                List<PartJobsReturns> oCityObjects = oCityToDisplayObjects.Select(p => p).OfType<PartJobsReturns>().ToList();

                //Create a anominious object here to break the circular reference
                var oCityToDisplay = oCityObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,

                    Fk_OutSrcID = p.Fk_PJID,
                    Ret_Date = DateTime.Parse(p.Ret_Date.ToString()).ToString("dd/MM/yyyy"),
                    PrdName = p.Inv_MaterialCategory.Name,
                    Customer=p.PartJobs.gen_Customer.CustomerName,
                    ProductName=p.ProductName,
                    RetPrdWeight=p.RetPrdWeight,
                    //p.PODate != null ? DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy") : "",


                    TypeofPrd = p.TypeofPrd == "1" ? "Finished" : "Semi-Finished",




                }).ToList().OrderByDescending(s => s.Pk_ID);

                return Json(new { Result = "OK", Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public ActionResult JobInDelRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_ID"]);

                List<Vw_JWInDelv> BillList = new List<Vw_JWInDelv>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_JWInDelv.Where(x => x.DelvID == sInvno).Select(x => x).OfType<Vw_JWInDelv>().ToList();


                //SELECT     dbo.JW_RM_RetMast.Pk_Inward, dbo.Inv_Material.Pk_Material, dbo.Inv_Material.Name, dbo.JWMat_InwdD.Quantity,
                //dbo.JW_RM_RetMast.Inward_Date, dbo.JW_RM_RetMast.Fk_QC, 
                //                      dbo.JW_RM_RetMast.Fk_Indent, dbo.JW_RM_RetMast.PONo, dbo.JWMat_InwdD.RollNo
                //FROM         dbo.JW_RM_RetMast INNER JOIN
                //                      dbo.JWMat_InwdD ON dbo.JW_RM_RetMast.Pk_Inward = dbo.JWMat_InwdD.Fk_Inward INNER JOIN
                //                      dbo.Inv_Material ON dbo.JWMat_InwdD.Fk_Material = dbo.Inv_Material.Pk_Material


                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Ret_Date");
                    dt.Columns.Add("ProductName");
                    dt.Columns.Add("DCNo");
                    dt.Columns.Add("RetPrdWeight");
                    dt.Columns.Add("Pk_ID");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("DelvID");
                    //dt.Columns.Add("ColorName");
                    //dt.Columns.Add("MillName");
                    foreach (Vw_JWInDelv entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Ret_Date"] = entity.Ret_Date;
                        row["ProductName"] = entity.ProductName;
                        row["CustomerName"] = entity.CustomerName;
                        row["DCNo"] = entity.DCNo;
                        row["RetPrdWeight"] = entity.RetPrdWeight;
                        row["Description"] = entity.Description;
                        row["DCNo"] = entity.DCNo;
                        row["Pk_ID"] = entity.Pk_ID;
                        row["Description"] = entity.Description;
                        row["DelvID"] = entity.DelvID;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.JobInDelRep orp = new CogitoStreamline.Report.JobInDelRep();

                    orp.Load("@\\Report\\JobInDelRep.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "JobInDelRep" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}

