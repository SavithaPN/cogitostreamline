﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace CogitoStreamLine.Controllers
{

    public class BoxStockListController : CommonController
    {
        StocksList oPStk = new StocksList();
        Order oOB = new Order();
        //Country oCountry = new Country();
        public BoxStockListController()
        {
            oDoaminObject = new StocksList();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Box Stock";
            return base.Index();
        }
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        protected override void Dispose(bool disposing)
        {
            //oState.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                StocksList oStock = oDoaminObject as StocksList;
                oStock.ID = decimal.Parse(pId);
                Vw_BoxStock oAtualObject = oStock.DAO as Vw_BoxStock;
                //Create a anominious object here to break the circular reference

                var oStockToDisplay = new
                {
                    Pk_Material = oAtualObject.Pk_BoxID,
                    MaterialName = oAtualObject.Name,
                    //RollNo = oAtualObject.RollNo,
                    Quantity = oAtualObject.Quantity
                };

                return Json(new { success = true, data = oStockToDisplay });
            }
            else
            {
                var oStockToDisplay = new Vw_BoxStock();
                return Json(new { success = true, data = oStockToDisplay });
            }
        }


        //[HttpPost]
        //public JsonResult BoxStockListByFiter(string Pk_Stock = "", string MaterialCategory = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Pk_Stock", Pk_Stock));
        //        oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
        //        oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Stock> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Stock>().ToList();

        //        //Create a anominious object here to break the circular reference
        //        var oStockToDisplay = oStockObjects.Select(p => new
        //        {
        //            Pk_Stock = p.Pk_Stock,
        //            MaterialName = p.Inv_Material.Name,
        //            Quantity = p.Quantity,

        //        }).ToList().OrderBy(s => s.Pk_Stock);

        //        return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}
        [HttpPost]
        public JsonResult BoxStockListByFiter(string Pk_BoxID = "", string Name = "", string CustName = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BoxID", Pk_BoxID));
                oSearchParams.Add(new SearchParameter("Pk_Order", Pk_Order));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("CustName", CustName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPStk.SearchBoxStk(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxStock> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Vw_BoxStock>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                    Pk_Material = p.Pk_BoxID,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    CustName = p.CustomerName,
                    PartName=p.PName,
                    StockID=p.Pk_StockID,
                    PartID=p.Pk_PartPropertyID,

                }).ToList().OrderBy(s => s.Pk_Material);

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult BillingBoxes(string Fk_OrderID = "", string BName = "", string PName = "",string FkCust = "")
        {
            try
            {
                //Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //string Fk_OrderID = oValues["Fk_OrderID"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();


                oSearchParams.Add(new SearchParameter("Fk_OrderID", Fk_OrderID));
                oSearchParams.Add(new SearchParameter("BName", BName));
                oSearchParams.Add(new SearchParameter("FkCust", FkCust));
                oSearchParams.Add(new SearchParameter("PName", PName));
                SearchResult oSearchResult = oOB.SearchBillOrdDet(oSearchParams);
                List<EntityObject> oOrdersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BillingBoxStocks> oOrderObjects = oOrdersToDisplayObjects.Select(p => p).OfType<Vw_BillingBoxStocks>().ToList();

                var oDeliveryScheduleToDisplay = oOrderObjects.Where(p=>p.Fk_Status==1 && p.Quantity>0).Select(p => new
                {
                   // Pk_OrderChild = p.Pk_OrderChild,
                  //  EnqQty = p.EnqQty,
                //    OrdQty = p.OrdQty,
                    BoxID = p.Pk_BoxID,
                   // PartNo = p.PartNo,
                    BName = p.Name,
                    OrdNo = p.Pk_Order,
                    Product = p.Product,
                    //EnqChild=p.Pk_EnquiryChild,
                    PName = p.PName,
                   PartID = p.Pk_PartPropertyID,
                StockID=p.Pk_StockID,
                    Quantity = p.Quantity,
                 //   Status = p.Fk_Status,
                    CustomerName = p.CustomerName,
                    Pk_Order=p.Pk_Order,
                    OrdQty=p.OrdQty,
                    Wt=p.Weight,
                  // OrderDate = DateTime.Parse(p.OrderDate.ToString()).ToString("dd/MM/yyyy"),

                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        public ActionResult BoxStockRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<Vw_BoxStock> PaperList = new List<Vw_BoxStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.Vw_BoxStock.Where(x => x.Quantity > 0).Select(x => x).OfType<Vw_BoxStock>().OrderBy(p => p.CustomerName).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("BoxStock");

                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    //dt.Columns.Add("Pk_Material");
                    //dt.Columns.Add("Pk_PaperStock");
                    //dt.Columns.Add("GSM");
                    dt.Columns.Add("Expr1");
                    dt.Columns.Add("PName");
                    //dt.Columns.Add("Deckle");
                    //dt.Columns.Add("MillName");
                    //dt.Columns.Add("SName");


                    foreach (Vw_BoxStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["CustomerName"] = entity.CustomerName;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Expr1"] = entity.Expr1;
                        row["PName"] = entity.PName;
                        //row["GSM"] = entity.GSM;
                        //row["BF"] = entity.BF;
                        //row["ColorName"] = entity.ColorName;
                        //row["Deckle"] = entity.Deckle;
                        //row["MillName"] = entity.MillName;
                        //row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.BoxStock orp = new CogitoStreamline.Report.BoxStock();

                    orp.Load("@\\Report\\BoxStock.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "BoxStock" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult DeleteRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int BoxID = Convert.ToInt32(oValues["BoxID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
                int StockID = Convert.ToInt32(oValues["StockID"]);
                int PartId = Convert.ToInt32(oValues["PartId"]);

                //List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
          

                BoxStock oBoxList = _oEntites.BoxStock.Where(x => x.Pk_StockID == StockID).Single();


                //oPaperList.
                _oEntites.DeleteObject(oBoxList);

                _oEntites.SaveChanges();



                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult UpdateRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                int BoxID = Convert.ToInt32(oValues["BoxID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
                int StockID = Convert.ToInt32(oValues["StockID"]);
                int PartId = Convert.ToInt32(oValues["PartId"]);

                //List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                if (StockID > 0)
                {
                    BoxStock oBoxList = _oEntites.BoxStock.Where(x => x.Pk_StockID == StockID).Single();

                    //var ExQty = oBoxList.Quantity;
                    //var TotQty = ExQty + StockVal;
                    oBoxList.Quantity = StockVal;
 
                    _oEntites.SaveChanges();
                }
              
                 
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult UpdateStk(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                int BoxID = Convert.ToInt32(oValues["BoxID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
                //int StockID = Convert.ToInt32(oValues["StockID"]);
                //int PartId = Convert.ToInt32(oValues["PartId"]);

                //List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                if (BoxID > 0)
                {
                    int RCount1 = _oEntites.BoxStock.Where(p => p.Fk_BoxID == BoxID).Count();

                    if(RCount1==1)
                    {
                    BoxStock oBoxList = _oEntites.BoxStock.Where(x => x.Fk_BoxID == BoxID).Single();

                    //var ExQty = oBoxList.Quantity;
                    //var TotQty = ExQty + StockVal;
                    oBoxList.Quantity = StockVal;
                        _oEntites.SaveChanges();
                    }
                    else if(RCount1==0)
                    {

                        Vw_BoxDet oBoxList = _oEntites.Vw_BoxDet.Where(x => x.Pk_BoxID == BoxID).FirstOrDefault();
                        BoxStock oBoxStock = new BoxStock();

                        oBoxStock.Fk_BoxID = BoxID;
                        oBoxStock.Quantity = StockVal;
                        oBoxStock.Fk_PartID = oBoxList.Pk_PartPropertyID;
                        _oEntites.AddToBoxStock(oBoxStock);
                        _oEntites.SaveChanges();
                    }

                }
                  

        
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult InsertRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
               

                int BoxID = Convert.ToInt32(oValues["BoxID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
              
                List<Vw_BoxDet> BoxList = new List<Vw_BoxDet>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                Vw_BoxDet oBoxList = _oEntites.Vw_BoxDet.Where(x => x.Pk_BoxID == BoxID).FirstOrDefault();
                BoxStock oBoxStock = new BoxStock();

                oBoxStock.Fk_BoxID = BoxID;                
                oBoxStock.Quantity = StockVal;
                oBoxStock.Fk_PartID = oBoxList.Pk_PartPropertyID;
                _oEntites.AddToBoxStock(oBoxStock);
                _oEntites.SaveChanges();


                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


    }
}
