﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class GlueCertificateController : Controller
//    {
//        //
//        // GET: /GlueCertificate/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;
using Common.Logging.Configuration;
using System.Collections.Specialized;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Drawing.Printing;

namespace CogitoStreamline.Controllers
{

    public class GlueCertificateController : CommonController
    {
        GlueCert oColor = new GlueCert();

        public GlueCertificateController()
        {
            oDoaminObject = new GlueCert();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Glue Quality Certificate";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        //[HttpPost]
        //public JsonResult getChar(string pId = "")
        //{
        //    List<InkParams> oListOfBoxType = oColor.Search(null).ListOfRecords.OfType<InkParams>().ToList();

        //    var oBoxToDisplay = oListOfBoxType.Select(p => new
        //    {
        //        Name = p.Name,
        //        Id = p.Pk_CharID

        //    });

        //    return Json(oBoxToDisplay);
        //}
        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                GlueCert oPOrders = oDoaminObject as GlueCert;
                // PurchaseOrderMs oPOrders = oDoaminObject as PurchaseOrderM;
                oPOrders.ID = decimal.Parse(pId);
                GlueCertificate oAtualObject = oPOrders.DAO as GlueCertificate;

                int i = -1;

                var oMaterials = oAtualObject.GlueCertificateDetails.Select(p => new
                {
                    slno = ++i,
                    Pk_IdDet = p.Pk_IdDet,
                    PCode = p.PCode,
                    MixingRatio = p.MixingRatio,
                    GelTemp = p.GelTemp,
                    Specification1 = p.Specification1,
                    Specification2 = p.Specification2,
                    Results1 = p.Results1,
                    Results2 = p.Results2,

                });

                var oPOrderDisplay = new
                {
                    Pk_Id = oAtualObject.Pk_Id,
                    Fk_Material = oAtualObject.Fk_Material,
                    Invno = oAtualObject.Invno,
                    InvDate = DateTime.Parse(oAtualObject.InvDate.ToString()).ToString("dd/MM/yyyy"),
                    Quantity = oAtualObject.Quantity,
                    Tested = oAtualObject.Tested,
                    Approved = oAtualObject.Approved,
                    Fk_Vendor = oAtualObject.Fk_Vendor,
                    
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oPOrderDisplay });
            }
            else
            {
                var oReturnGoodFromSiteDisplay = new PaperCertificate();
                return Json(new { success = true, data = oReturnGoodFromSiteDisplay });
            }
        }





        [HttpPost]
        public JsonResult GlueCertListByFiter(string Name = "", string Pk_Id = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Id", Pk_Id));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<GlueCertificate> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<GlueCertificate>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Id = p.Pk_Id,
                    Fk_Material = p.Fk_Material,
                    Name=p.Inv_Material.Name,
                    Invno = p.Invno,
                    InvDate = DateTime.Parse(p.InvDate.ToString()).ToString("dd/MM/yyyy"),
                    Quantity = p.Quantity,
                    Tested = p.Tested,
                    Approved = p.Approved,
                    Fk_Vendor = p.Fk_Vendor,
                    VendorName=p.gen_Vendor.VendorName,
                    // InvDate = DateTime.Parse(p.InvDate.ToString()).ToString("dd/MM/yyyy"),


                }).ToList().OrderBy(s => s.Pk_Id);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        public ActionResult GlueCert(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);

                List<Vw_GlueCertificate> BillList = new List<Vw_GlueCertificate>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_GlueCertificate.Where(x => x.Pk_Id == sInvno).Select(x => x).OfType<Vw_GlueCertificate>().ToList();

                //            SELECT Vw_PaperCertificate.Pk_Id, Vw_PaperCertificate.Invno, Vw_PaperCertificate.InvDate, Vw_PaperCertificate.Name,
                //               Vw_PaperCertificate.TestMethod, Vw_PaperCertificate.Target, Vw_PaperCertificate.Acceptance, Vw_PaperCertificate.Result1,
                //               Vw_PaperCertificate.Result2, Vw_PaperCertificate.Result3, Vw_PaperCertificate.Result4, Vw_PaperCertificate.Result5,
                //               Vw_PaperCertificate.Result6, Vw_PaperCertificate.MinVal, Vw_PaperCertificate.MaxVal, Vw_PaperCertificate.AvgVal, 
                //               Vw_PaperCertificate.Remarks, Vw_PaperCertificate.Quantity, Vw_PaperCertificate.Tested, Vw_PaperCertificate.Approved,
                //               Vw_PaperCertificate.VendorName, Vw_PaperCertificate.UOM, Vw_PaperCertificate.Pk_CharID, Vw_PaperCertificate.Pk_IdDet,
                //               Vw_PaperCertificate.Fk_Material
                //FROM   Balaji.dbo.Vw_PaperCertificate Vw_PaperCertificate
                //ORDER BY Vw_PaperCertificate.Pk_Id, Vw_PaperCertificate.Pk_IdDet



                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_Id");
                    dt.Columns.Add("Invno");
                      dt.Columns.Add("InvDate");
                    dt.Columns.Add("Name");
                     dt.Columns.Add("MixingRatio");
                     dt.Columns.Add("PCode");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Tested");
                    dt.Columns.Add("Approved");
                    dt.Columns.Add("Specification1");
                    dt.Columns.Add("Specification2");
                    dt.Columns.Add("Results1");
                    dt.Columns.Add("Results2");
                    dt.Columns.Add("GelTemp");
                    dt.Columns.Add("Fk_Material");
                    dt.Columns.Add("VendorName");
                    dt.Columns.Add("Pk_IdDet");

                    foreach (Vw_GlueCertificate entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_Id"] = entity.Pk_Id;
                        row["Invno"] = entity.Invno;
                        row["InvDate"] = entity.InvDate;
                        row["Name"] = entity.Name;
                        row["Specification1"] = entity.Specification1;
                        row["Specification2"] = entity.Specification2;
                        row["Results1"] = entity.Results1;
                        row["Quantity"] = entity.Quantity;
                        row["Tested"] = entity.Tested;
                        row["Approved"] = entity.Approved;
                        row["VendorName"] = entity.VendorName;
                        row["Results2"] = entity.Results2;
                        row["MixingRatio"] = entity.MixingRatio;
                        row["PCode"] = entity.PCode;
                        row["Fk_Material"] = entity.Fk_Material;
                        row["GelTemp"] = entity.GelTemp;
                        row["Pk_IdDet"] = entity.Pk_IdDet;
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.GlueCertificate orp = new CogitoStreamline.Report.GlueCertificate();

                    orp.Load("@\\Report\\GlueCertificate.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;


                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "GCertificate" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }

}

