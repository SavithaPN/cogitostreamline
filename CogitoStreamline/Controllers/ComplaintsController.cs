﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Collections.Generic;
using System;
namespace CogitoStreamLine.Controllers
{
    public class ComplaintsController : CommonController
    {

        Customer oCustomer = new Customer();

        public ComplaintsController()
        {
            oDoaminObject = new CustComplaints();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Complaints";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oCustomer.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                CustComplaints oComplaint = oDoaminObject as CustComplaints;
                oComplaint.ID = decimal.Parse(pId);

                CustomerComplaint oAtualObject = oComplaint.DAO as CustomerComplaint;
                //Create a anominious object here to break the circular reference

                var oCmpToDisplay = new
                {
                    Pk_ComplaintID = oAtualObject.Pk_ComplaintID,
                    ComplaintDate = DateTime.Parse(oAtualObject.ComplaintDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_CustomerID = oAtualObject.Fk_CustomerID,
                    Fk_OrderNo = oAtualObject.Fk_OrderNo,
                    Fk_EnquiryNo = oAtualObject.Fk_EnquiryNo,
                    Description = oAtualObject.Description,
                    Fk_Status = oAtualObject.Fk_Status,
                    Fk_User = oAtualObject.Fk_User
                };

                return Json(new { success = true, data = oCmpToDisplay });
            }
            else
            {
                var oCmpToDisplay = new CustomerComplaint();
                return Json(new { success = true, data = oCmpToDisplay });
            }
        }

        [HttpPost]
        public JsonResult getCustomer(string pId = "")
        {
            List<gen_Customer> oListOfCustomer = oCustomer.Search(null).ListOfRecords.OfType<gen_Customer>().ToList();

            var oCustomerToDisplay = oListOfCustomer.Select(p => new
            {
                Name = p.CustomerName,
                Id = p.Pk_Customer
            });

            return Json(oCustomerToDisplay);
        }


        [HttpPost]
        public JsonResult CmpListByFiter(string Pk_ComplaintID = "", string ComplaintDate = "", string Fk_CustomerID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ComplaintID", Pk_ComplaintID));
                oSearchParams.Add(new SearchParameter("ComplaintDate", ComplaintDate));
                oSearchParams.Add(new SearchParameter("Fk_CustomerID", Fk_CustomerID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCmpToDisplayObjects = oSearchResult.ListOfRecords;
                List<CustomerComplaint> oComplaintObjects = oCmpToDisplayObjects.Select(p => p).OfType<CustomerComplaint>().ToList();

                //Create a anominious object here to break the circular reference
                var oCmpToDisplay = oComplaintObjects.Select(p => new
                {
                    Pk_ComplaintID = p.Pk_ComplaintID,
                    ComplaintDate = DateTime.Parse(p.ComplaintDate.ToString()).ToString("dd/MM/yyyy"),
                    CustomerID = p.gen_Customer.CustomerName,
                    Fk_OrderNo = p.Fk_OrderNo,
                    Fk_EnquiryNo = p.Fk_EnquiryNo,
                    Description = p.Description,
                    Fk_Status = p.Fk_Status,
                    Fk_User = p.Fk_User
                }).ToList().OrderByDescending(p => p.Pk_ComplaintID);

                return Json(new { Result = "OK", Records = oCmpToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

