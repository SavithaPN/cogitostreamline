﻿
//Name            : Tenants Controller Class  
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
//3. Tenants duplicatechecking for checking the duplicate entry of the  Tenants name
//Author          :   Shantha
//Date            :   14/8/2015
//CRH Number      :   SL0003
//Modifications   :



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class WTenantController : CommonController
    {
        Tenants oTenant = new Tenants();

        public WTenantController()
        {
            oDoaminObject = new Tenants();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Tenants";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oTenant.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Tenants oTenant = oDoaminObject as Tenants;
                oTenant.ID = decimal.Parse(pId);
                WebGareCore.wgTenant oAtualObject = oTenant.DAO as WebGareCore.wgTenant;
                //Create a anominious object here to break the circular reference

                var oTanentToDisplay = new
                {

                    Pk_Tanent = oAtualObject.Pk_Tanent,
                    TanentName = oAtualObject.TanentName
                  //  Description = oAtualObject.Description
                };

                return Json(new { success = true, data = oTanentToDisplay });
            }
            else
            {
                var oTanentToDisplay = new CogitoStreamLineModel.wgTenant();
                return Json(new { success = true, data = oTanentToDisplay });
            }
        }



        [HttpPost]
        public JsonResult TanentDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sTanentName = oValues["TanentName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("TanentName", sTanentName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oTanentToDisplayObjects = oSearchResult.ListOfRecords;
                List<WebGareCore.wgTenant> oTanentObjects = oTanentToDisplayObjects.Select(p => p).OfType<WebGareCore.wgTenant>().ToList();

                var oTanentToDisplay = oTanentObjects.Select(p => new
                {
                    Pk_Tanent = p.Pk_Tanent,
                    TanentName = p.TanentName
                }).ToList().OrderBy(s => s.TanentName);

                return Json(new { Success = true, Records = oTanentToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult TenantListByFiter(string TanentName = "", string Pk_Tanent = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Tanent", Pk_Tanent));
                oSearchParams.Add(new SearchParameter("TanentName", TanentName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oTanentToDisplayObjects = oSearchResult.ListOfRecords;
                List<WebGareCore.wgTenant> oTanentObjects = oTanentToDisplayObjects.Select(p => p).OfType<WebGareCore.wgTenant>().ToList();

                //Create a anominious object here to break the circular reference
                var oTanentToDisplay = oTanentObjects.Select(p => new
                {
                    Pk_Tanent = p.Pk_Tanent,
                    TanentName = p.TanentName
                }).ToList();


                return Json(new { Result = "OK", Records = oTanentToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

