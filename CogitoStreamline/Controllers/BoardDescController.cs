﻿//using System;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using ManufacturingModel;
using ManufacturingModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.Data.SqlClient;
using System.Data;


namespace ManufacturingERP.Controllers
{
    public class BoardDescController : CommonController
    {
        //
        // GET: /FlexEst/

        BoardD oEst;
        public BoardDescController()
        {
            oDoaminObject = new Mat_Category();
            //oDoaminObject1 = new BoardD();
            oEst = new BoardD();

        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        public override ActionResult Index()
        {
            return base.Index();
        }


        public override JsonResult Load(string pId = "")
        {
            return null;
        }

        [HttpPost]
   
        public JsonResult LoadBySpecID(string data = "")
        {

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

            string eId = oValues["BoxId"];
            string eCat = oValues["MatCat"];
            decimal eId1 = Convert.ToDecimal(oValues["BoxId"]);
            if (eId != "")
            {

                Mat_Category oBoxSp = oDoaminObject as Mat_Category;
                //Estimation oEstimation = oDoaminObject as Estimation;


                if (oBoxSp.SetSpecId(decimal.Parse(eCat)))
                {
                    Inv_MaterialCategory oAtualObject1 = oBoxSp.DAO as Inv_MaterialCategory;

                    //let us get parts of the estimation
                    var oParts = oAtualObject1.Inv_Material.Where(p => p.Pk_Material == eId1).Select(p => new
                    {
                        id = p.Pk_Material,
                        layers = Json(p.BoardDesc.Where(x => x.Fk_BoardID == eId1).Select(x => new
                        {
                            id = x.Pk_Value,
                            gsm = x.GSM,
                            bf = x.BF,
                            color = x.Color,
                            Fk_Material = x.Fk_Material,
                         
                        })).Data,


                        length = p.Length,
                        width = p.Width,
                        height = p.Height,
                      
                    });

                    var oEstimateToDisplay = new object();
                    

                    oEstimateToDisplay = new
                    {
                        
                        parts = Json(oParts).Data,
                        
                    };

                    return Json(new { success = true, data = oEstimateToDisplay });
                }
                else
                {
                    return Json(new { success = false, data = "" });
                }
            }
            else
            {
                //var oEstimateToDisplay = new gen_Estimate();
                //return Json(new { success = true, data = oEstimateToDisplay });
                return null;
            }
        }
    }
}
