﻿
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using WebGareCore.CommonObjects;
//using WebGareCore.DomainModel;
//using System.Data.Objects.DataClasses;
//using WebGareCore;
//using TugberkUg.MVC.Helpers;
//using Newtonsoft.Json;
//using CogitoStreamLineModel.DomainModel;
//using CogitoStreamLineModel;
//using System.IO;
//using System.Data;
//using CrystalDecisions.ReportAppServer.ReportDefModel;
//using System.Drawing.Printing;
//using CrystalDecisions.Shared;
//using WebGareCore.Controls;


//namespace CogitoStreamline.Controllers
//{
//    public class CheckQualityController : CommonController
//    {

//        #region Variables
//        public WgFileUpload oFileUpload = new WgFileUpload("documents", "uploads");
       
//        #endregion Variables

//        Quality oQC = new Quality();

//        // GET: /MaterialInward/
//      //  Customer oCustomer = new Customer();
//        WebGareCore.DomainModel.Tenants oTanent = new WebGareCore.DomainModel.Tenants();
//        //Quality oQuality = new Quality();
//        [HttpPost]
//        public JsonResult FileUpload()
//        {
//            return Json(oFileUpload.SaveFile(Request.Files));
//        }
//        public CheckQualityController()
//        {
//            oDoaminObject = new Quality();
//        }

//        public override ActionResult Index()
//        {
//            ViewBag.Header = "QualityCheck";
//            return base.Index();
//        }

//        protected override void Dispose(bool disposing)
//        {
//            base.Dispose(disposing);
//        }
//        [HttpPost]
//        public ActionResult Upload(string data = "")
//        {
//            var file = Request.Files[0];

//            if (file != null && file.ContentLength > 0)
//            {
//                var fileName = Path.GetFileName(file.FileName);
//                var path = Path.Combine(Server.MapPath("~/uploads/"), fileName);
//                file.SaveAs(path);
//                Session["DocumentLink"] = path.ToString();
//            }
//            return RedirectToAction("Index");
//        }
       


//        [HttpPost]
//        public override JsonResult Load(string data = "")
//        {
//            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
//            string pId = oValues["Id"];
//            if (pId != "")
//            {
//                Quality oCheckQuality = oDoaminObject as Quality;
//                oCheckQuality.ID = decimal.Parse(pId);
//                QualityCheck oAtualObject = oCheckQuality.DAO as QualityCheck;

//                int i = -1;
//                var oDocuments = oAtualObject.QualityDocuments.Select(p => new
//                {
//                    Pk_Documents = p.Pk_Document,
//                    FileName = p.FileName,
//                    DocumentLink = p.DocumentLink,
//                    FileSize = p.FileSize
//                });
//                //var oQualityDoc = oAtualObject.QualityDocuments.Select(p => new
//                //{
//                //    Pk_Document = p.Pk_Document,
//                //    slno = ++i,
//                //    DocumentLink = p.DocumentLink,
//                //    FileName = p.FileName,
//                //    FileSize = p.FileSize
//                //});
//                var oMaterials = oAtualObject.QualityChild.Select(p => new
//                {
//                    slno = ++i,
//                    POQty = p.POQty,
//                    Fk_Material = p.Fk_Material,
//                     Name = p.Inv_Material.Name,
//                    Pk_QualityChild = p.Pk_QualityChild,
//                    AccQty = p.AccQty,
//                    ReelNo = p.ReelNo,
//                });

//                //decimal dPendingValue = oCheckQuality.PendingValue(decimal.Parse(oAtualObject.Fk_IndentNumber.ToString()), decimal.Parse(oAtualObject.Fk_Material.ToString()));
//                //dPendingValue = dPendingValue + decimal.Parse(oAtualObject.Quantity.ToString());
//                ////Create a anominious object here to break the circular reference
//                var oQualityCheckToDisplay = new
//                {
//                    Pk_QualityCheck = oAtualObject.Pk_QualityCheck,
//                    InvoiceNumber = oAtualObject.InvoiceNumber,
//                    CheckDate = DateTime.Parse(oAtualObject.CheckDate.ToString()).ToString("dd/MM/yyyy"),
//                    DCNo = oAtualObject.DCNo,
//                    Fk_PONo = oAtualObject.Fk_PONo,
//                    //txtFk_Vendor = oAtualObject.F

//                //    Fk_Vendor = oAtualObject.Fk_Vendor,
//                //    Fk_Material = oAtualObject.Fk_Material,
//                //    Fk_IndentNumber = oAtualObject.Fk_IndentNumber,
//                //    //Board_Length = oAtualObject.Length,
//                //    //Board_Width = oAtualObject.Width,
//                //    Ply = oAtualObject.Ply,
//                //    FluteType = oAtualObject.FluteType,
//                //    //Board_GSM = oAtualObject.GSM,
//                //    //Board_BS = oAtualObject.BS,
//                //    Moisture = oAtualObject.Moisture,
//                //    Printing = oAtualObject.Printing,
//                //    Quantity = oAtualObject.Quantity,
//                //    Result = oAtualObject.Result,
//                //    Comments = oAtualObject.Comments,
//                //    Pending = dPendingValue,


//                    MaterialData = Json(oMaterials).Data
//                //    //Fk_VendorId,
//                };

//                return Json(new { success = true, data = oQualityCheckToDisplay });
//            }
//            else
//            {
//                var oQualityCheckToDisplay = new QualityCheck();
//                return Json(new { success = true, data = oQualityCheckToDisplay });
            
//        }}

//        [HttpPost]
//        public JsonResult QualityList(string DDate = "", string Invno = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {
//            try
//            {
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("DDate", DDate));
//                oSearchParams.Add(new SearchParameter("Invoice", Invno));
//                oSearchParams.Add(new SearchParameter("Name", MaterialName));

//                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
//                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
//                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
//                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oQC.SearchQC(oSearchParams);


//                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
//                List<Vw_Inwd> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_Inwd>().ToList();

//                //Create a anominious object here to break the circular reference
//                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

//                var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new
//                {
//                    Pk_QualityCheck = p.Pk_QualityCheck,
//                    CheckDate=p.CheckDate,
//                    //Vendor=p.VendorName,
//                    //InvoiceNo=p.InvoiceNumber,
//                    PONo=p.Fk_PONo,
//                    Indent=p.Fk_Indent
//                    //Mat_Name=p.Name,
//                  //  PendQty=p.QCPQty,
//                  //  TanentName = p.TanentName,
//                  //SQuantity=p.SQuantity,
//                  //  UnitName = p.UnitName

//                }).ToList();

//                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = ex.Message });
//            }
//        }
//        [HttpPost]
//        public JsonResult QualityCheckListByFiter(string QCNo = "", string CheckDate = "", string Vendor = "", string MaterialName = "", string InvoiceNo = "", string MaterialInward = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {
//            try
//            {
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("Pk_QualityCheck", QCNo));
//                oSearchParams.Add(new SearchParameter("CheckDate", CheckDate));
//                oSearchParams.Add(new SearchParameter("InvoiceNo", InvoiceNo));
//                oSearchParams.Add(new SearchParameter("Vendor", Vendor));
//                oSearchParams.Add(new SearchParameter("Fk_Material", MaterialName));
//                oSearchParams.Add(new SearchParameter("MaterialInward", MaterialInward));
//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

//                List<EntityObject> oQualityCheckToDisplayObjects = oSearchResult.ListOfRecords;
//                List<QualityCheck> oQualityCheckObjects = oQualityCheckToDisplayObjects.Select(p => p).OfType<QualityCheck>().ToList();

                
//                //Quality oCheckQuality = oDoaminObject as Quality;
//                //QualityCheck oAtualObject = oCheckQuality.DAO as QualityCheck;
//                //decimal dPendingValue = oCheckQuality.PendingValue(decimal.Parse(oAtualObject.Fk_IndentNumber.ToString()), decimal.Parse(oAtualObject.Fk_Material.ToString()));
//                //dPendingValue = dPendingValue + decimal.Parse(oAtualObject.Quantity.ToString());
          
//                //PendingTrack opending=new PendingTrack();
//                //Quality oQuality=new Quality();

//                //decimal dPendingValue = oQuality.PendingValue(decimal.Parse(oQuality.Fk_IndentNumber.ToString()), decimal.Parse(oAtualObject.Fk_Material.ToString()));
//                //dPendingValue = dPendingValue + decimal.Parse(oAtualObject.Quantity.ToString());



//                //Create a anominious object here to break the circular reference
//                var oQualityCheckToDisplay = oQualityCheckObjects.Select(p => new
//                {

//                    Pk_QualityCheck = p.Pk_QualityCheck,
//                    InvoiceNumber = p.InvoiceNumber,
//                    CheckDate =  p.CheckDate != null ? DateTime.Parse(p.CheckDate.ToString()).ToString("dd/MM/yyyy") : "",
//                    Fk_PONo = p.Fk_PONo,
//                    DCNo = p.DCNo,
//                    VName=p.PurchaseOrderM.gen_Vendor.VendorName

//                }).ToList();

//                return Json(new { Result = "OK", Records = oQualityCheckToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = ex.Message });
//            }
//        }

//        public JsonResult getMaterials(string FkQualityCheck = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {

//            try
//            {
//                QualityChild oIssueDetails = new QualityChild();
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("FkQualityCheck", FkQualityCheck));
//                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oQC.SearchFKQC(oSearchParams);

//                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
//                List<QualityChild> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<QualityChild>().ToList();

//                //Create a anominious object here to break the circular reference
//                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
//                {
//                    Pk_Material = p.Inv_Material.Pk_Material,
//                    Name = p.Inv_Material.Name,
//                    Quantity = p.AccQty!=null?p.POQty:0,
//                  Fk_Mill=p.Inv_Material.Fk_Mill,
//                    SName = p.Inv_Material.gen_Mill.MillName,
//                    ReelNo= p.ReelNo!=null ? p.ReelNo:"",
//                    Color=p.Inv_Material.gen_Color.ColorName,
//                    Mill=p.Inv_Material.gen_Mill.MillName,
//                }).ToList();

//                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
//            }
//        }

//        [HttpPost]


//        public JsonResult POList(string Pk_PONo = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {
//            try
//            {
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
//                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));

//                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
//                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
//                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
//                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oQC.SearchPO(oSearchParams);


//                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
//                List<Vw_PO> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_PO>().ToList();

//                //Create a anominious object here to break the circular reference
//                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

//                var oVMaterialsToDisplay = oVMaterialObjects.Where(p => p.Pk_PONo == decimal.Parse(Pk_PONo)).Select(p => new
//                {
//                    Pk_PONo = p.Pk_PONo,
//                    Pk_Material = p.Pk_Material,
//                    Mat_Name = p.Name,                    
//                    Quantity = p.Quantity,
//                    Rate=p.Rate,
//                    Amount=p.Amount,
//                    VendorName=p.VendorName,
//                    Color=p.ColorName,
//                    Mill=p.MillName,
//                }).ToList();

//                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = "Select PO No. & Proceed" });
//            }
//        }


//        public JsonResult POCertList(string Pk_PONo = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {
//            try
//            {
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
//                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));

//                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
//                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
//                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
//                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oQC.SearchPO(oSearchParams);


//                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
//                List<Vw_PO> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_PO>().ToList();

//                //Create a anominious object here to break the circular reference
//                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

//                var oVMaterialsToDisplay = oVMaterialObjects.Where(p => p.Fk_Status == 1).Select(p => new
//                {
//                    Pk_PONo = p.Pk_PONo,
//                    Pk_Material = p.Pk_Material,
//                    Mat_Name = p.Name,
//                    Quantity = p.Quantity,
//                    Rate = p.Rate,
//                    Amount = p.Amount,
//                    Pk_Vendor=p.Pk_Vendor,
//                    VendorName = p.VendorName,
//                    GSM=p.GSM,
//                    BF=p.BF,
//                }).ToList();

//                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = "Select PO No. & Proceed" });
//            }
//        }


//        public JsonResult POPendingList(string Pk_PONo = "", string MaterialName = "", string VendorName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {
//            try
//            {

//                int StrStatus = 1;
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("Pk_PONo", Pk_PONo));
//                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
//                oSearchParams.Add(new SearchParameter("VendorName", VendorName));

//                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
//                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
//                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
//                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oQC.SearchPO(oSearchParams);


//                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
//                List<Vw_PO> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_PO>().ToList();

//                //Create a anominious object here to break the circular reference
//                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

//                var oVMaterialsToDisplay = oVMaterialObjects.Where(x => x.Fk_Status == StrStatus && x.Quantity-x.InwdQty>0 ).Select(p => new
//                {
//                    Pk_PONo = p.Pk_PONo,
//                    Pk_Material = p.Pk_Material,
//                    Mat_Name = p.Name,
//                    Quantity = p.Quantity,
//                    Rate = p.Rate,
//                    Amount = p.Amount,
//                    VendorName = p.VendorName,
//                    INQty=p.InwdQty,
//                    PODate = p.PODate != null ? DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy") : "",
//                    PQuantity = (p.Quantity-p.InwdQty)

//                }).ToList();

//                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = "Select PO No. & Proceed" });
//            }
//        }

//        public JsonResult QualityGetRec(string Pk_QualityCheck = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
//        {

//            try
//            {
//              //  QualityChild oIssueDetails = new QualityChild();
//                List<SearchParameter> oSearchParams = new List<SearchParameter>();
//                oSearchParams.Add(new SearchParameter("Pk_QualityCheck", Pk_QualityCheck));
//                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
//                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
//                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

//                SearchResult oSearchResult = oQC.SearchQCDet(oSearchParams);

//                List<EntityObject> oIssueDetailsToDisplayObjects = oSearchResult.ListOfRecords;
//                List<Vw_Quality> oIssueObjects = oIssueDetailsToDisplayObjects.Select(p => p).OfType<Vw_Quality>().ToList();

//                //Create a anominious object here to break the circular reference
//                var oIssueDetailsToDisplay = oIssueObjects.Select(p => new
//                {
                    
//                    Name = p.Name,
//                    Quantity = p.AccQty,
//                   POQty = p.POQty,
//                   Color=p.ColorName,
//                   Mill=p.MillName,

//                }).ToList();

//                return Json(new { Result = "OK", Records = oIssueDetailsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
//            }
//            catch (Exception ex)
//            {
//                return Json(new { Result = "ERROR", Message = "Select Quality Check No. and Proceed" });
//            }
//        }



//        public ActionResult QCRep(string data = "")
//        {
//            try
//            {
//                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
//                int sInvno = Convert.ToInt32(oValues["Pk_QC"]);

//                List<Vw_Quality> BillList = new List<Vw_Quality>();
//                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

//                //BillList = dc.VwInvoices.ToList();
//                BillList = dc.Vw_Quality.Where(x => x.Pk_QualityCheck == sInvno).Select(x => x).OfType<Vw_Quality>().ToList();

////        SELECT     dbo.QualityCheck.Pk_QualityCheck, dbo.QualityCheck.CheckDate, dbo.QualityCheck.Fk_PONo,
//                //dbo.Inv_Material.Name, dbo.QualityChild.POQty, dbo.QualityChild.AccQty, 
////                      dbo.QualityCheck.InvoiceNumber
////FROM         dbo.QualityCheck INNER JOIN
////                      dbo.QualityChild ON dbo.QualityCheck.Pk_QualityCheck = dbo.QualityChild.FkQualityCheck INNER JOIN
////                      dbo.Inv_Material ON dbo.QualityChild.Fk_Material = dbo.Inv_Material.Pk_Material

//                if (BillList.Count > 0)
//                {
//                    DataTable dt = new DataTable();

//                    dt.Columns.Add("Name");
//                    dt.Columns.Add("Fk_PONo");
//                    dt.Columns.Add("Pk_QualityCheck");
//                    dt.Columns.Add("CheckDate");
//                    dt.Columns.Add("POQty");
//                    dt.Columns.Add("AccQty");
//                    dt.Columns.Add("InvoiceNumber");
//                    dt.Columns.Add("ColorName");
//                    dt.Columns.Add("MillName");

//                    foreach (Vw_Quality entity in BillList)
//                    {
//                        DataRow row = dt.NewRow();

//                        row["Name"] = entity.Name;
//                        row["Fk_PONo"] = entity.Fk_PONo;
//                        row["Pk_QualityCheck"] = entity.Pk_QualityCheck;
//                        row["CheckDate"] = entity.CheckDate;
//                        row["POQty"] = entity.POQty;
//                        row["AccQty"] = entity.AccQty;
//                        row["InvoiceNumber"] = entity.InvoiceNumber;
//                        row["ColorName"] = entity.ColorName;
//                        row["MillName"] = entity.MillName;

//                        dt.Rows.Add(row);
//                    }


//                    DataSet ds = new DataSet();
//                    ds.Tables.Add(dt);
//                    CogitoStreamline.Report.QualityCheck orp = new CogitoStreamline.Report.QualityCheck();

//                    orp.Load("@\\Report\\QualityCheck.rpt");
//                    orp.SetDataSource(dt.DefaultView);

//                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "QualityCheck" + sInvno + ".pdf");
//                    FileInfo file = new FileInfo(pdfPath);
//                    if (file.Exists)
//                    {
//                        file.Delete();
//                    }
//                    var pd = new PrintDocument();


//                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
//                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
//                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
//                    objDiskOpt.DiskFileName = pdfPath;

//                    orp.ExportOptions.DestinationOptions = objDiskOpt;
//                    orp.Export();

//                }
//                return null;
//            }
//            catch (Exception ex)
//            {
//                return null;

//            }
//        }
//    }
//}
