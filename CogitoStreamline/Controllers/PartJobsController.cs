﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
namespace CogitoStreamLine.Controllers
{
    public class PartJobsController : CommonController
    {

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        PieceWorks oItemPart = new PieceWorks();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public PartJobsController()
        {
            oDoaminObject = new PieceWorks();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Job Work-In--Material Inward";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oItemPart.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                PieceWorks oItemPart = oDoaminObject as PieceWorks;
                oItemPart.ID = decimal.Parse(pId);
                PartJobs oAtualObject = oItemPart.DAO as PartJobs;
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_ID = oAtualObject.Pk_ID,
                    //Name = oAtualObject.Name

                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new PartJobs();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        public ActionResult StatusUpdate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_ID"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                PartJobs oPurM = _oEntites.PartJobs.Where(p => p.Pk_ID == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oPurM.Fk_Status = 4;
                _oEntites.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult OpenStatus(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_ID"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                PartJobs oPurM = _oEntites.PartJobs.Where(p => p.Pk_ID == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oPurM.Fk_Status = 1;
                _oEntites.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        [HttpPost]
        public JsonResult PJobsListByFiter(string Name = "", string Pk_ID = "", string Cust = "", string FromDate = "", string ToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("Cust", Cust));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<PartJobs> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<PartJobs>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    Fk_Customer = p.gen_Customer.CustomerName,
                    RecdDate = DateTime.Parse(p.RecdDate.ToString()).ToString("dd/MM/yyyy"),
                    Description = p.Description,


                }).ToList().OrderByDescending(s => s.Pk_ID);
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult TaskGetRec(string Pk_ID = "", string Customer = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oItemPart.SearchTask(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PartJobs> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_PartJobs>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    MaterialName = p.Name,
                    CustomerName = p.CustomerName,
                    Weight = p.Quantity,
                    Width = p.Width,
                    Length = p.Length,
                    LayerWt = p.LayerWt,
                    Description = p.Description,
                    ExpectedNos = p.AppNos,
                    ReelNo = p.RollNo,
                    RecdDate = DateTime.Parse(p.RecdDate.ToString()).ToString("dd/MM/yyyy"),
                }).ToList().OrderByDescending(s => s.Pk_ID);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }




        public ActionResult JobRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_ID"]);


                //                SELECT "Vw_PartJobs"."Pk_Material", "Vw_PartJobs"."Name", "Vw_PartJobs"."CustomerName", "Vw_PartJobs"."Pk_ID", "Vw_PartJobs"."RecdWeight", 
                //               "Vw_PartJobs"."ReelNo", "Vw_PartJobs"."Length", "Vw_PartJobs"."Width", "Vw_PartJobs"."LayerWt", "Vw_PartJobs"."AppNos", "Vw_PartJobs"."RecdDate", 
                //               "Vw_PartJobs"."Description"
                //FROM   "Balaji"."dbo"."Vw_PartJobs" "Vw_PartJobs"
                //ORDER BY "Vw_PartJobs"."Pk_ID", "Vw_PartJobs"."Pk_Material"




                List<Vw_PartJobs> BillList = new List<Vw_PartJobs>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_PartJobs.Where(x => x.Pk_ID == sInvno).Select(x => x).OfType<Vw_PartJobs>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_ID");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("LayerWt");
                    dt.Columns.Add("AppNos");
                    dt.Columns.Add("RecdDate");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Pk_Material");
                    //dt.Columns.Add("FORM_H");
                    //dt.Columns.Add("FORM_CT3");

                    foreach (Vw_PartJobs entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Pk_ID"] = entity.Pk_ID;
                        row["Quantity"] = entity.Quantity;
                        row["RollNo"] = entity.RollNo;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["CustomerName"] = entity.CustomerName;
                        row["LayerWt"] = entity.LayerWt;
                        row["AppNos"] = entity.AppNos;
                        row["RecdDate"] = entity.RecdDate;
                        row["Description"] = entity.Description;
                        row["Pk_Material"] = entity.Pk_Material;
                        //row["FORM_H"] = entity.FORM_H;
                        //row["FORM_CT3"] = entity.FORM_CT3;
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.PartJobs orp = new CogitoStreamline.Report.PartJobs();

                    orp.Load("@\\Report\\PartJobs.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "JobWork" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }




        //       public ActionResult JobRepDates(string data = "")
        //       {
        //           try
        //           {
        //               Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //               //DateTime= DateTime.ParseExact(oValues["FromDate"].ToString());
        //               //int sInvno = Convert.ToInt32(oValues["Pk_ID"]);
        //               //int sInvno = Convert.ToInt32(oValues["Pk_ID"]);

        ////                SELECT "Vw_PartJobs"."Pk_Material", "Vw_PartJobs"."Name", "Vw_PartJobs"."CustomerName", "Vw_PartJobs"."Pk_ID", "Vw_PartJobs"."RecdWeight", 
        ////               "Vw_PartJobs"."ReelNo", "Vw_PartJobs"."Length", "Vw_PartJobs"."Width", "Vw_PartJobs"."LayerWt", "Vw_PartJobs"."AppNos", "Vw_PartJobs"."RecdDate", 
        ////               "Vw_PartJobs"."Description"
        ////FROM   "Balaji"."dbo"."Vw_PartJobs" "Vw_PartJobs"
        ////ORDER BY "Vw_PartJobs"."Pk_ID", "Vw_PartJobs"."Pk_Material"


        //               //BillList = dc.VW_Invoice.Where(x => x.InvDate.Value.Month == dmonth && x.InvDate.Value.Year == dyear).Select(x => x).OfType<VW_Invoice>().ToList();

        //               List<Vw_PartJobs> BillList = new List<Vw_PartJobs>();
        //               CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //               //BillList = dc.VwInvoices.ToList();
        //               BillList = dc.Vw_PartJobs.Where(x => x.RecdDate).Select(x => x).OfType<Vw_PartJobs>().ToList();

        //               if (BillList.Count > 0)
        //               {
        //                   DataTable dt = new DataTable();

        //                   dt.Columns.Add("Name");
        //                   dt.Columns.Add("Pk_ID");
        //                   dt.Columns.Add("RecdWeight");
        //                   dt.Columns.Add("ReelNo");
        //                   dt.Columns.Add("Length");
        //                   dt.Columns.Add("Width");
        //                   dt.Columns.Add("CustomerName");
        //                   dt.Columns.Add("LayerWt");
        //                   dt.Columns.Add("AppNos");
        //                   dt.Columns.Add("RecdDate");
        //                   dt.Columns.Add("Description");
        //                   dt.Columns.Add("Pk_Material");
        //                   //dt.Columns.Add("FORM_H");
        //                   //dt.Columns.Add("FORM_CT3");

        //                   foreach (Vw_PartJobs entity in BillList)
        //                   {
        //                       DataRow row = dt.NewRow();

        //                       row["Name"] = entity.Name;
        //                       row["Pk_ID"] = entity.Pk_ID;
        //                       row["RecdWeight"] = entity.RecdWeight;
        //                       row["ReelNo"] = entity.ReelNo;
        //                       row["Length"] = entity.Length;
        //                       row["Width"] = entity.Width;
        //                       row["CustomerName"] = entity.CustomerName;
        //                       row["LayerWt"] = entity.LayerWt;
        //                       row["AppNos"] = entity.AppNos;
        //                       row["RecdDate"] = entity.RecdDate;
        //                       row["Description"] = entity.Description;
        //                       row["Pk_Material"] = entity.Pk_Material;
        //                       //row["FORM_H"] = entity.FORM_H;
        //                       //row["FORM_CT3"] = entity.FORM_CT3;
        //                       dt.Rows.Add(row);
        //                   }


        //                   DataSet ds = new DataSet();
        //                   ds.Tables.Add(dt);
        //                   CogitoStreamline.Report.JobWork orp = new CogitoStreamline.Report.JobWork();

        //                   orp.Load("@\\Report\\JobWork.rpt");
        //                   orp.SetDataSource(dt.DefaultView);

        //                   string pdfPath = Server.MapPath("~/ConvertPDF/" + "JobWork" + sInvno + ".pdf");
        //                   FileInfo file = new FileInfo(pdfPath);
        //                   if (file.Exists)
        //                   {
        //                       file.Delete();
        //                   }
        //                   var pd = new PrintDocument();


        //                   orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //                   orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //                   DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //                   objDiskOpt.DiskFileName = pdfPath;

        //                   orp.ExportOptions.DestinationOptions = objDiskOpt;
        //                   orp.Export();

        //               }
        //               return null;
        //           }
        //           catch (Exception ex)
        //           {
        //               return null;

        //           }
        //       }

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}

