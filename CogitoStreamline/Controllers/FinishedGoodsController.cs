﻿//Name          : FinishedGoods Controller
//Description   : Contains the FinishedGoods controller, every page in the application will be instance of this class with following
//                 1. Load Method(during Edit) 2. ListByFiltermethod----For displaying jTable  3. Get Customer Details 4. File Upload 5. Get Comunication Type 6. Material Specifications 7. Delivery Schedule
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 08/09/2015
//Crh Number    : SL00060
//Modifications : 




using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class FinishedGoodsController : CommonController
    {
        //
        // GET: /FinishedGoods/

        #region Variables
        Customer oCustomer = new Customer();
        #endregion Variables

        #region Constractor
        public FinishedGoodsController()
        {
            oDoaminObject = new FinishedGoods();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Finished Goods";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCustomer.Dispose();
            base.Dispose(disposing);
        }
        
        [HttpPost]
        public JsonResult getCustomers(string pId = "")
        {
            List<gen_Customer> oListOfCustomers = oCustomer.Search(null).ListOfRecords.OfType<gen_Customer>().ToList();
            var oCustomersToDisplay = oListOfCustomers.Select(p => new
            {
                Name = p.CustomerName,
                Id = p.Pk_Customer
            });

            return Json(oCustomersToDisplay);
        }
        //[HttpPost]
        //public JsonResult getCommunicationType(string pId = "")
        //{
        //    List<gen_Communication> oListOfCommunicationTypes = oCommunication.Search(null).ListOfRecords.OfType<gen_Communication>().ToList();
        //    var oContriesToDisplay = oListOfCommunicationTypes.Select(p => new
        //    {
        //        Name = p.Type,
        //        Id = p.Pk_Communication
        //    });

        //    return Json(oContriesToDisplay);
        //}
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                FinishedGoods oFinishedGoods = oDoaminObject as FinishedGoods;
                oFinishedGoods.ID = decimal.Parse(pId);

                FinishedGoodsMaster oAtualObject = oFinishedGoods.DAO as FinishedGoodsMaster;

                int i = -1;
                var oDelivery = oAtualObject.gen_Order.gen_DeliverySchedule.Select(p => new
                {
                    Pk_DeliverySchedule = p.Pk_DeliverySechedule,
                    slno = ++i,
                    Quantity = p.Quantity,
                    DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                    Comments = p.Comments
                });

                var oShipping = oAtualObject.FinishedGoodsShippingDetails.Select(s => new
                {
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    Address = s.Address,
                    Pincode = s.Pincode,
                    City = s.City,
                    State = s.State,
                    Country = s.Country,
                    Mobile = s.Mobile,
                    Telephone = s.Telephone,
                    Email = s.Email,
                    Fax = s.Fax
                });

                var oFinishedGoodsToDisplay = new
                {
                    Pk_FinishedGoodsId = oAtualObject.Pk_FinishedGoodsId,
                    Fk_Customer = oAtualObject.Fk_Customer,
                    Fk_Order = oAtualObject.Fk_Order,
                    FinishedDate = DateTime.Parse(oAtualObject.FinishedDate.ToString()).ToString("dd/MM/yyyy"),
                    Product = oAtualObject.gen_Order.Product,
                    Quantity = oAtualObject.Quantity,
                    shippingDetails = Json(oShipping).Data,
                    
                     deliverySchedule = Json(oDelivery).Data
                };

                return Json(new { success = true, data = oFinishedGoodsToDisplay });
            }
            else
            {
                var oFinishedGoodsToDisplay = new FinishedGoodsMaster();
                return Json(new { success = true, data = oFinishedGoodsToDisplay });
            }
        }
        [HttpPost]
        public JsonResult FinishedGoodsListByFiter(string Pk_FinishedGoodsId = "", string FinishedDate = "", string Customer = "", string Fk_Order = "", string State = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_FinishedGoodsId", Pk_FinishedGoodsId));
                oSearchParams.Add(new SearchParameter("FinishedDate", FinishedDate));
                oSearchParams.Add(new SearchParameter("Customer", Customer));
                oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oFinishedGoodssToDisplayObjects = oSearchResult.ListOfRecords;
                List<FinishedGoodsMaster> oFinishedGoodsObjects = oFinishedGoodssToDisplayObjects.Select(p => p).OfType<FinishedGoodsMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oFinishedGoodsToDisplay = oFinishedGoodsObjects.Select(p => new
                {
                    Pk_FinishedGoodsId = p.Pk_FinishedGoodsId,
                    CustomerName = p.gen_Customer.CustomerName,
                    Fk_Customer = p.Fk_Customer,
                    FinishedDate = DateTime.Parse(p.FinishedDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Order = p.Fk_Order

                }).ToList();

                return Json(new { Result = "OK", Records = oFinishedGoodsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public ActionResult Upload(string data = "")
        {
            var file = Request.Files[0];

            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/FinishedGoodsDocuments/"), fileName);
                file.SaveAs(path);
                Session["DocumentLink"] = path.ToString();
            }
            return RedirectToAction("Index");
        }
        #endregion Methods
    }
}
