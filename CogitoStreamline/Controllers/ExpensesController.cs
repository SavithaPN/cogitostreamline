﻿//Name            : Expenses Controller Class
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
//  
//Author          :   Nagesh
//Date            :  22/10/2015
//CRH Number      :   
//Modifications   :

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class ExpensesController : CommonController
    {
        AccountHead oAccount = new AccountHead();
        //Country oCountry = new Country();
        public ExpensesController()
        {
            oDoaminObject = new Expenses();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Voucher";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oAccount.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Expenses oExp = oDoaminObject as Expenses;
                oExp.ID = decimal.Parse(pId);
                Voucher oAtualObject = oExp.DAO as Voucher;
                //Create a anominious object here to break the circular reference

                var oExpToDisplay = new
                {
                    Pk_ExpensesID = oAtualObject.Pk_ExpensesID,
                    Amount = oAtualObject.Amount,
                    Description = oAtualObject.Description,
                    Fk_AccountHeadId = oAtualObject.Fk_AccountHeadId

                };

                return Json(new { success = true, data = oExpToDisplay });
            }
            else
            {
                var oExpToDisplay = new Voucher();
                return Json(new { success = true, data = oExpToDisplay });
            }
        }

        //[HttpPost]
        //public JsonResult CityDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sCountry = oValues["Country"];
        //        string sFk_StateID = oValues["Fk_StateID"];
        //        string sCityName = oValues["CityName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Country", sCountry));
        //        oSearchParams.Add(new SearchParameter("StateId", sFk_StateID));
        //        oSearchParams.Add(new SearchParameter("CityName", sCityName));


        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oExpToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_City> oExpObjects = oExpToDisplayObjects.Select(p => p).OfType<gen_City>().ToList();

        //        var oExpToDisplay = oExpObjects.Select(p => new
        //        {

        //            Pk_CityID = p.Pk_CityID,
        //            CityName = p.CityName,
        //            Fk_StateID = p.Fk_StateID


        //        }).ToList().OrderBy(s => s.CityName);

        //        return Json(new { Success = true, Records = oExpToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult getAccount(string pId = "")
        {
            List<acc_AccountHeads> oListOfState = oAccount.Search(null).ListOfRecords.OfType<acc_AccountHeads>().ToList();

            var oExpToDisplay = oListOfState.Select(p => new
            {
                Name = p.HeadName,
                Id = p.Pk_AccountHeadId
            });

            return Json(oExpToDisplay);
        }

        //[HttpPost]
        //public JsonResult getCountry(string pId = "")
        //{
        //    List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

        //    var oCountryToDisplay = oListOfCountry.Select(p => new
        //    {
        //        Name = p.CountryName,
        //        Id = p.Pk_Country
        //    });

        //    return Json(oCountryToDisplay);
        //}

        [HttpPost]
        public JsonResult ExpenseListByFiter(string Pk_ExpensesID = "", string Amount = "", string Description = "", string Fk_AccountHeadId = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ExpensesID", Pk_ExpensesID));
                oSearchParams.Add(new SearchParameter("Amount", Amount));
                oSearchParams.Add(new SearchParameter("Description", Description));
                oSearchParams.Add(new SearchParameter("Fk_AccountHeadId", Fk_AccountHeadId));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oExpToDisplayObjects = oSearchResult.ListOfRecords;
                List<Voucher> oExpObjects = oExpToDisplayObjects.Select(p => p).OfType<Voucher>().ToList();

                //Create a anominious object here to break the circular reference
                var oExpToDisplay = oExpObjects.Select(p => new
                {
                    Pk_ExpensesID = p.Pk_ExpensesID,
                    Amount = p.Amount,
                    Description = p.Description,
                    AccountHead = p.acc_AccountHeads.HeadName
                }).ToList().OrderBy(s => s.Pk_ExpensesID);

                return Json(new { Result = "OK", Records = oExpToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ExpenseListByAccount(string data = "")
        {
            try
            {
                Expenses oExp = new Expenses();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sAccountHeadId = oValues["AccountHeadId"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("AccountHeadId", sAccountHeadId));

                SearchResult oSearchResult = oExp.Search(oSearchParams);

                List<EntityObject> oExpsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Voucher> oExpObjects = oExpsToDisplayObjects.Select(p => p).OfType<Voucher>().ToList();

                var oExpToDisplay = oExpObjects.Select(p => new
                {
                    Pk_ExpensesID = p.Pk_ExpensesID,
                    Amount = p.Amount
                }).ToList();

                return Json(new { Success = true, Records = oExpToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
    }
}

