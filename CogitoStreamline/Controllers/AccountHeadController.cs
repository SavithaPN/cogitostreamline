﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class AccountHeadController : CommonController
    {
        AccountHead oCountry = new AccountHead();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public AccountHeadController()
        {
            oDoaminObject = new AccountHead();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Account Head";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCountry.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                AccountHead oHead = oDoaminObject as AccountHead;
                oHead.ID = decimal.Parse(pId);
                acc_AccountHeads oAtualObject = oHead.DAO as acc_AccountHeads;
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_AccountHeadId = oAtualObject.Pk_AccountHeadId,
                    HeadName = oAtualObject.HeadName,
                    Description = oAtualObject.Description
                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new acc_AccountHeads();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        [HttpPost]
        public JsonResult HeadDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sHeadName = oValues["HeadName"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("HeadName", sHeadName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<acc_AccountHeads> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<acc_AccountHeads>().ToList();
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_AccountHeadId = p.Pk_AccountHeadId,
                    HeadName = p.HeadName
                }).ToList().OrderBy(s => s.HeadName);
                return Json(new { Success = true, Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult HeadNameListByFiter(string HeadName = "", string Pk_AccountHeadId = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("HeadName", HeadName));
                oSearchParams.Add(new SearchParameter("Pk_AccountHeadId", Pk_AccountHeadId));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<acc_AccountHeads> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<acc_AccountHeads>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_AccountHeadId = p.Pk_AccountHeadId,
                    HeadName = p.HeadName,
                    Description = p.Description
                }).ToList().OrderBy(s => s.HeadName);
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

