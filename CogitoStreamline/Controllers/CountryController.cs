﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class CountryController : CommonController
    {
        Country oCountry = new Country();
        public CountryController()
        {
            oDoaminObject = new Country();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Country";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCountry.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Country oCountry = oDoaminObject as Country;
                oCountry.ID = decimal.Parse(pId);
                gen_Country oAtualObject = oCountry.DAO as gen_Country;
                //Create a anominious object here to break the circular reference
                var oCountryToDisplay = new
                {
                    Pk_Country = oAtualObject.Pk_Country,
                    CountryName = oAtualObject.CountryName
                };
                return Json(new { success = true, data = oCountryToDisplay });
            }
            else
            {
                var oCountryToDisplay = new gen_Country();
                return Json(new { success = true, data = oCountryToDisplay });
            }
        }
        [HttpPost]
        public JsonResult CountryDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sCountryName = oValues["CountryName"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("CountryName", sCountryName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oCountryToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Country> oCountryObjects = oCountryToDisplayObjects.Select(p => p).OfType<gen_Country>().ToList();
                var oCountryToDisplay = oCountryObjects.Select(p => new
                {
                    Pk_Country = p.Pk_Country,
                    CountryName = p.CountryName
                }).ToList().OrderBy(s => s.CountryName);
                return Json(new { Success = true, Records = oCountryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult CountryListByFiter(string Pk_Country = "", string CountryName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Country", Pk_Country));
                oSearchParams.Add(new SearchParameter("CountryName", CountryName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oCountryToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Country> oCountryObjects = oCountryToDisplayObjects.Select(p => p).OfType<gen_Country>().ToList();
                //Create a anominious object here to break the circular reference
                var oCountryToDisplay = oCountryObjects.Select(p => new
                {
                    Pk_Country = p.Pk_Country,
                    CountryName = p.CountryName
                }).ToList().OrderBy(s=>s.CountryName);
                return Json(new { Result = "OK", Records = oCountryToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}


