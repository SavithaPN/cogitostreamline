﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using ManufacturingModel;
using ManufacturingModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamLineModel.DomainModel;


namespace ManufacturingERP.Controllers
{
    public class EnqEstimateController : CommonController
    {
        //
        // GET: /FlexEst/

        public EnqEstimateController()
        {
            oDoaminObject = new Estimation();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        public override ActionResult Index()
        {
            return base.Index();
        }


        public override JsonResult Load(string pId = "")
        {
            return null;
        }

        [HttpPost]
        public override JsonResult Save(string data)
        {
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);



            data = JsonConvert.SerializeObject(values);
            //this.CurrentObject = null;
            return base.Save(data);


        }

    }
}
