﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;


namespace CogitoStreamline.Controllers
{
    public class CustomerController : CommonController
    {
        //
        // GET: /Customer/

        #region Variables
        Customer oCustomer = new Customer();
        City oCity = new City();
        State oState = new State();
        Country oCountry = new Country();
        #endregion Variables

        #region Constractor
        public CustomerController()
        {
            oDoaminObject = new Customer();
        }
        #endregion Constractor

        #region Methods
        protected override void Dispose(bool disposing)
        {
            oCustomer.Dispose();
            oState.Dispose();
            oCity.Dispose();
            oCountry.Dispose();
            base.Dispose(disposing);
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Customer";
            return base.Index();
        }
        [HttpPost]
        public JsonResult getState(string pId = "")
        {
            List<gen_State> oListOfState = oState.Search(null).ListOfRecords.OfType<gen_State>().ToList();
            var ostateToDisplay = oListOfState.Select(p => new
            {
                Name = p.StateName,
                Id = p.Pk_StateID
            });

            return Json(ostateToDisplay);
        }
        [HttpPost]
        public JsonResult getCity(string pId = "")
        {
            List<gen_City> oListOfCity = oCity.Search(null).ListOfRecords.OfType<gen_City>().ToList();
            var oCityToDisplay = oListOfCity.Select(p => new
            {
                Name = p.CityName,
                Id = p.Pk_CityID
            });

            return Json(oCityToDisplay);
        }
        [HttpPost]
        public JsonResult getCountry(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();
            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }

        [HttpPost]
        public JsonResult getCountryPop(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();
            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Customer oCustomer = oDoaminObject as Customer;
                oCustomer.ID = decimal.Parse(pId);
                gen_Customer oAtualObject = oCustomer.DAO as gen_Customer;

                var oContacts = oAtualObject.gen_CustomerContacts.Select(p => new
                {
                    id = p.gen_ContactPerson.Pk_ContactPersonId,
                    Name = p.gen_ContactPerson.ContactPersonName,
                    Details = "Number:" + p.gen_ContactPerson.ContactPersonNumber +
                              ";eMail:" + p.gen_ContactPerson.ContactPersonEmailId +
                              ";State:" + p.gen_ContactPerson.gen_State.StateName +
                              ";City:" + p.gen_ContactPerson.gen_City.CityName +
                              ";Street:" + p.gen_ContactPerson.Address_No_Street +
                              ";Pin:" + p.gen_ContactPerson.Address_PinCode
                });


                int i = -1;

                var oShippingDetails = oAtualObject.gen_CustomerShippingDetails.Select(p => new
                {
                    Pk_CustomerShippingId = p.Pk_CustomerShippingId,
                    slno = ++i,
                    Fk_CustomerId = p.Fk_CustomerId,
                    ShippingCustomerName = p.ShippingCustomerName,
                    ShippingAddress = p.ShippingAddress,
                    ShippingCountry = p.ShippingCountry,
                    ShippingState = p.ShippingState,
                    ShippingCity = p.ShippingCity,
                    ShippingPincode = p.ShippingPincode,
                    ShippingMobile = p.ShippingMobile,
                    ShippingLandLine = p.ShippingLandLine,
                    ShippingEmailId = p.ShippingEmailId
                });

                //Create a anominious object here to break the circular reference
                var oCustomerToDisplay = new
                {
                    Pk_Customer = oAtualObject.Pk_Customer,
                    CustomerName = oAtualObject.CustomerName,
                    CustomerType = oAtualObject.CustomerType,
                    CustomerContact = oAtualObject.CustomerContact,
                    CustomerAddress = oAtualObject.CustomerAddress,
                    Fk_State = oAtualObject.Fk_State,
                    Fk_City = oAtualObject.Fk_City,
                    Fk_Country = oAtualObject.Fk_Country,
                    PinCode = oAtualObject.PinCode,
                    LandLine = oAtualObject.LandLine,
                    ContactPerson = oAtualObject.ContactPerson,
                    Email = oAtualObject.Email,
                    VAT = oAtualObject.VAT,
                    Fk_Tanent = oAtualObject.Fk_Tanent,
                    CustomerShort=oAtualObject.CustomerShort,
                    Contacts = Json(oContacts).Data,
                    ShippingDet = Json(oShippingDetails).Data

                };

                return Json(new { success = true, data = oCustomerToDisplay });
            }
            else
            {
                var oCustomerToDisplay = new gen_Customer();
                return Json(new { success = true, data = oCustomerToDisplay });
            }
        }
        [HttpPost]
        public JsonResult CustomerListByFiter(string Pk_Customer = "", string CustomerName = "", string CustomerType = "", string CustomerContact = "", string LandLine = "", string Pk_CustomerShippingId = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_CustomerShippingId", Pk_CustomerShippingId));
                oSearchParams.Add(new SearchParameter("Pk_Customer", Pk_Customer));
                oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
                oSearchParams.Add(new SearchParameter("CustomerType", CustomerType));
                oSearchParams.Add(new SearchParameter("CustomerContact", CustomerContact));
                oSearchParams.Add(new SearchParameter("LandLine", LandLine));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCustomersToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Customer> oCustomerObjects = oCustomersToDisplayObjects.Select(p => p).OfType<gen_Customer>().ToList();

                //int i = -1;

                var oCustomersToDisplay = oCustomerObjects.Select(p => new
                {
                    Pk_Customer = p.Pk_Customer,
                    CustomerName = p.CustomerName,
                    CustomerAddress = p.CustomerAddress,
                    City = p.gen_City.CityName,
                    State = p.gen_State.StateName,
                    Country = p.gen_Country.CountryName,
                    PinCode = p.PinCode,
                    CustomerContact = p.CustomerContact,
                    CustomerType = p.CustomerType,
                    LandLine = p.LandLine,
                    ContactPerson = p.ContactPerson,
                    Email = p.Email
                }).ToList().OrderBy(p => p.CustomerName);

                return Json(new { Result = "OK", Records = oCustomersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult CustomerDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sCustomerName = oValues["CustomerName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("CustomerName", sCustomerName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCustomerToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Customer> oCustomerObjects = oCustomerToDisplayObjects.Select(p => p).OfType<gen_Customer>().ToList();

                var oCustomersToDisplay = oCustomerObjects.Select(p => new
                {
                    Pk_Customer = p.Pk_Customer,
                    CustomerName = p.CustomerName
                }).ToList().OrderBy(s => s.CustomerName);

                return Json(new { Success = true, Records = oCustomersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult CustomerContacts(string Pk_Customer = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Customer oCustomer = oDoaminObject as Customer;
                oCustomer.ID = decimal.Parse(Pk_Customer);
                gen_Customer oCustomerObjects = oCustomer.DAO as gen_Customer;

                var oCustomerContactsToDisplay = oCustomerObjects.gen_CustomerContacts.Select(p => new
                {
                    Pk_CustomerContacts = p.Pk_CustomerContacts,
                    ContactPersonName = p.gen_ContactPerson.ContactPersonName,
                    ContactPersonDesignation = p.gen_ContactPerson.ContactPersonDesignation,
                    ContactPersonNumber = p.gen_ContactPerson.ContactPersonNumber,
                    ContactPersonEmailId = p.gen_ContactPerson.ContactPersonEmailId,
                    City = p.gen_ContactPerson.gen_City.CityName

                }).ToList();

                return Json(new { Result = "OK", Records = oCustomerContactsToDisplay, TotalRecordCount = oCustomerContactsToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }




        [HttpPost]
        public JsonResult CustomerShipping(string Pk_Customer = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Customer oCustomer = oDoaminObject as Customer;
                oCustomer.ID = decimal.Parse(Pk_Customer);
                gen_Customer oCustomerObjects = oCustomer.DAO as gen_Customer;

                var oCustomerContactsToDisplay = oCustomerObjects.gen_CustomerShippingDetails.Select(p => new
                {
                    Fk_CustomerId = p.Fk_CustomerId,
                    ShippingCustomerName = p.ShippingCustomerName,
                    ShippingAddress = p.ShippingAddress,
                    ShippingCountry = p.ShippingCountry,
                    ShippingState = p.ShippingState,
                    ShippingCity = p.ShippingCity,
                    ShippingPincode = p.ShippingPincode,
                    ShippingMobile = p.ShippingMobile,
                    ShippingLandLine = p.ShippingLandLine,
                    ShippingEmailId = p.ShippingEmailId

                }).ToList();

                return Json(new { Result = "OK", Records = oCustomerContactsToDisplay, TotalRecordCount = oCustomerContactsToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
       
        
        //[HttpPost]
        //public JsonResult CustomerShipping(string CustCode = "", string CustomerName = "", string SCity = "", string SState = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    ///include pid here
        //    ///
        //    try
        //    {

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("CustCode", CustCode));
        //        oSearchParams.Add(new SearchParameter("CustomerName", CustomerName));
        //        oSearchParams.Add(new SearchParameter("SCity", SCity));
        //        oSearchParams.Add(new SearchParameter("SState", SState));



        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oCustomer.SearchPK(oSearchParams);


        //        List<EntityObject> oCustomerShippingsToDisplay = oSearchResult.ListOfRecords;
        //        List<Vw_CustShip> oCustObjects = oCustomerShippingsToDisplay.Select(p => p).OfType<Vw_CustShip>().ToList();


        //        var oCustomerShippingsToDisplayObj = oCustObjects.Where(p => p.Fk_CustomerId == decimal.Parse(CustCode)).Select(p => new
        //        {
        //            Pk_CustomerShippingId = p.Pk_CustomerShippingId,
        //            Fk_CustomerId = p.Fk_CustomerId,
        //            ShippingCustomerName = p.ShippingCustomerName,
        //            ShippingAddress = p.ShippingAddress,
        //            ShippingCountry = p.ShippingCountry,
        //            ShippingState = p.ShippingState,
        //            ShippingCity = p.ShippingCity,
        //            ShippingPincode = p.ShippingPincode,
        //            ShippingMobile = p.ShippingMobile,
        //            ShippingLandLine = p.ShippingLandLine,
        //            ShippingEmailId = p.ShippingEmailId
        //        }).ToList();
        //        return Json(new { Result = "OK", Records = oCustomerShippingsToDisplayObj, TotalRecordCount = oCustomerShippingsToDisplayObj.Count() });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}
       
        
        
        
        [HttpPost]
        public JsonResult ContactDuplicateChecking(string data = "")
        {
            try
            {
                Customer oCustomer = new Customer();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sContactPersonName = oValues["ContactPersonName"];
                string sPk_Customer = oValues["Pk_Customer"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("ContactPersonName", sContactPersonName));
                oSearchParams.Add(new SearchParameter("Pk_Customer", sPk_Customer));

                SearchResult oSearchResult = oCustomer.SearchContacts(oSearchParams);

                List<EntityObject> oContactToDisplayObjects = oSearchResult.ListOfRecords;
                List<vw_CustomerDetails> oContactObjects = oContactToDisplayObjects.Select(p => p).OfType<vw_CustomerDetails>().ToList();

                var oContactToDisplay = oContactObjects.Select(p => new
                {
                    Pk_Customer = p.Pk_Customer,
                    ContactPersonName = p.ContactPersonName,
                }).ToList().OrderBy(s => s.Pk_Customer);

                return Json(new { Success = true, Records = oContactToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        #endregion Methods
    }
}

