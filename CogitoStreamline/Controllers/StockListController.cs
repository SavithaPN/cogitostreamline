﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;
using System.Data;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace CogitoStreamLine.Controllers
{

    public class StockListController : CommonController
    {
        StocksList oPStk = new StocksList();
        Material oMaterial = new Material();
        public StockListController()
        {
            oDoaminObject = new StocksList();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Consumable Stock";
            return base.Index();
        }
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        protected override void Dispose(bool disposing)
        {
            //oState.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                StocksList oStock = oDoaminObject as StocksList;
                oStock.ID = decimal.Parse(pId);
                Stock oAtualObject = oStock.DAO as Stock;
                //Create a anominious object here to break the circular reference

                var oStockToDisplay = new
                {
                    Pk_Stock = oAtualObject.Pk_Stock,
                    Pk_Material = oAtualObject.Inv_Material.Pk_Material,
                    Name = oAtualObject.Inv_Material.Name,
                    Quantity = oAtualObject.Quantity,
                    Min_Value = oAtualObject.Inv_Material.Min_Value,
                    Max_Value = oAtualObject.Inv_Material.Max_Value

                };

                return Json(new { success = true, data = oStockToDisplay });
            }
            else
            {
                var oStockToDisplay = new Stock();
                return Json(new { success = true, data = oStockToDisplay });
            }
        }

        //[HttpPost]
        //public override JsonResult Load(string data = "")
        //{
        //    Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //    string pId = oValues["Id"];
        //    if (pId != "")
        //    {
        //        StocksList oMaterial = oDoaminObject as StocksList;
        //        oMaterial.ID = decimal.Parse(pId);
        //        StocksList oAtualObject = oMaterial.DAO as StocksList;
        //        //Create a anominious object here to break the circular reference

        //        var oMaterialToDisplay = new
        //        {

        //            Pk_Material = oAtualObject.Pk_Material,
        //            Name = oAtualObject.Name,
        //            //Brand = oAtualObject.Brand,
        //            //MaterialType = oAtualObject.MaterialType != null ? oAtualObject.MaterialType : "",
        //            ////   Fk_UnitId = oAtualObject.Fk_UnitId,
        //            //Size = oAtualObject.Size,
        //            ////   Description = oAtualObject.Description,
        //            //PartNo = oAtualObject.PartNo,
        //            Min_Value = oAtualObject.Min_Value,
        //            Max_Value = oAtualObject.Max_Value
        //        };

        //        return Json(new { success = true, data = oMaterialToDisplay });
        //    }
        //    else
        //    {
        //        var oMaterialToDisplay = new Inv_Material();
        //        return Json(new { success = true, data = oMaterialToDisplay });
        //    }
        //}
        [HttpPost]
        public JsonResult StockListByFiter(string Pk_Stock = "", string MaterialCategory = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Stock", Pk_Stock));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_ConsStockList> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Vw_ConsStockList>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                    Pk_Stock = p.Pk_Stock,
                    MaterialName = p.MatName,
                    Quantity     = p.Quantity,
                    CatName=p.CatName,
                    Pk_Material=p.Pk_Material,

                }).ToList().OrderBy(s => s.Pk_Stock);

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        //[HttpPost]
        //public JsonResult PaperStockListByFiter(string Pk_Material = "",   string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //    try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
        //        oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
             
        //        oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oPStk.SearchpStk(oSearchParams);

        //        List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<vw_PaperRollStock> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<vw_PaperRollStock>().ToList();

        //        //Create a anominious object here to break the circular reference
        //        var oStockToDisplay = oStockObjects.Select(p => new
        //        {
        //            Pk_Material = p.Pk_Material,
        //            MaterialName = p.Name,
        //            Quantity = p.Quantity,

        //        }).ToList().OrderBy(s => s.Pk_Material);

        //        return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        //[HttpPost]
        //public JsonResult CityListByState(string data = "")
        //{
        //    try
        //    {
        //        City oStock = new City();
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sStateId = oValues["StateId"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("StateId", sStateId));

        //        SearchResult oSearchResult = oStock.Search(oSearchParams);

        //        List<EntityObject> oStocksToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_City> oStockObjects = oStocksToDisplayObjects.Select(p => p).OfType<gen_City>().ToList();

        //        var oStockToDisplay = oStockObjects.Select(p => new
        //        {
        //            Pk_Stock = p.Pk_Stock,
        //            CityName = p.CityName
        //        }).ToList();

        //        return Json(new { Success = true, Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}
        public ActionResult StockList(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //  int sInvno = Convert.ToInt32(oValues["Pk_EQID"]);

                List<Vw_ConsStockList> BillList = new List<Vw_ConsStockList>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_ConsStockList.Select(x => x).OfType<Vw_ConsStockList>().OrderBy(x => x.CatName).ThenBy(x=>x.MatName).ToList();


                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("MatName");
                    dt.Columns.Add("Pk_Stock");
                    dt.Columns.Add("CatName");
                    dt.Columns.Add("Fk_MaterialCategory");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_Material");
                    


                    foreach (Vw_ConsStockList entity in BillList)
                    {
                        DataRow row = dt.NewRow();
                        row["Fk_MaterialCategory"] = entity.Fk_MaterialCategory;
                        row["MatName"] = entity.MatName;
                        row["Pk_Stock"] = entity.Pk_Stock;
                        row["CatName"] = entity.CatName;
                   
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Quantity"] = entity.Quantity;
                         

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.ConsStockList orp = new CogitoStreamline.Report.ConsStockList();

                    orp.Load("@\\Report\\ConsStockList.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "ConsStockList" + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult DeleteRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int MatID = Convert.ToInt32(oValues["MatID"]);
                //int StockVal = Convert.ToInt32(oValues["StockVal"]);
                int StockID = Convert.ToInt32(oValues["StockID"]);
                //int PartId = Convert.ToInt32(oValues["PartId"]);

                //List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


               Stock oBoxList = _oEntites.Stocks.Where(x => x.Pk_Stock == StockID).Single();


                //oPaperList.
                _oEntites.DeleteObject(oBoxList);

                _oEntites.SaveChanges();



                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult UpdateRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                int MatID = Convert.ToInt32(oValues["MatID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
                int StockID = Convert.ToInt32(oValues["StockID"]);
                //int PartId = Convert.ToInt32(oValues["PartId"]);

                //List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                if (StockID > 0)
                {
                    Stock oBoxList = _oEntites.Stocks.Where(x => x.Pk_Stock == StockID).Single();

                    oBoxList.Quantity = StockVal;
                    //oBoxList.Quantity = StockVal;
                    //oBoxList.Fk_Material = pkmat;

                    _oEntites.SaveChanges();
                }
                //else
                //{
                //    PaperStock oPaperList1 = _oEntites.PaperStock.Where(x => x.Pk_PaperStock == sInvno).Single();
                //    oPaperList1.RollNo = rollNo;
                //    oPaperList1.Quantity = StockVal;
                //    oPaperList1.Fk_Material = pkmat;

                //    _oEntites.SaveChanges();
                //}



                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult InsertRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //int sInvno =
                //_comLayer.parameters.add("MatID", MatID);
                //_comLayer.parameters.add("StockVal", StockVal);
                //_comLayer.parameters.add("StockID", StockID);
                //_comLayer.parameters.add("PartId", PartId);


                int MatID = Convert.ToInt32(oValues["MatID"]);
                int StockVal = Convert.ToInt32(oValues["StockVal"]);
                //int StockID = Convert.ToInt32(oValues["StockID"]);
                //int PartId = Convert.ToInt32(oValues["PartId"]);

                List<Vw_ConsStockList> BoxList = new List<Vw_ConsStockList>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                int PCount1 = _oEntites.Stocks.Where(x => x.Fk_Material == MatID).Count();

                //Vw_ConsStockList oBoxList = _oEntites.Vw_ConsStockList.Where(x => x.Pk_Material == MatID).FirstOrDefault();

                if (PCount1 == 0)
                {
                    Stock oBoxStock = new Stock();

                    oBoxStock.Fk_Material = MatID;
                    oBoxStock.Quantity = StockVal;
                    oBoxStock.Fk_BranchID = 1;
                    oBoxStock.Fk_Tanent = 1;
                    //oBoxStock.Fk_PartID = oBoxList.Pk_PartPropertyID;
                    _oEntites.AddToStocks(oBoxStock);
                    _oEntites.SaveChanges();
                }

                else if (PCount1 == 1)
                {
                    Stock oStockList = _oEntites.Stocks.Where(x => x.Fk_Material == MatID).Single();

                    oStockList.Quantity = StockVal;
                    //oBoxList.Quantity = StockVal;
                    //oBoxList.Fk_Material = pkmat;

                    _oEntites.SaveChanges();
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

    }
}
