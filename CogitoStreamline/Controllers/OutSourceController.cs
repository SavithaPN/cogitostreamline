﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
namespace CogitoStreamLine.Controllers
{
    public class OutSourceController : CommonController
    {

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        OutSource oItemPart = new OutSource();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public OutSourceController()
        {
            oDoaminObject = new OutSource();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Job Work Out-Work Order";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oItemPart.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                OutSource oItemPart = oDoaminObject as OutSource;
                oItemPart.ID = decimal.Parse(pId);
                Outsourcing oAtualObject = oItemPart.DAO as Outsourcing;
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_SrcID = oAtualObject.Pk_SrcID,
                    //Name = oAtualObject.Name

                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new Outsourcing();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        public ActionResult StatusUpdate(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_SrcID"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                Outsourcing oPurM = _oEntites.Outsourcing.Where(p => p.Pk_SrcID == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oPurM.Fk_Status = 4;
                _oEntites.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult OpenStatus(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_SrcID"]);

                // List<Vw_POReport> BillList = new List<Vw_POReport>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                Outsourcing oPurM = _oEntites.Outsourcing.Where(p => p.Pk_SrcID == sInvno).Single();
                //Purchase_Order oPOrder = new Purchase_Order();

                oPurM.Fk_Status = 1;
                _oEntites.SaveChanges();

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        
        
        [HttpPost]
        public JsonResult OutSourceListByFiter(string Name = "", string Pk_SrcID = "",string FromDate = "", string ToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Pk_SrcID", Pk_SrcID));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<Outsourcing> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<Outsourcing>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_SrcID = p.Pk_SrcID,
                    Fk_Vendor = p.gen_Vendor.VendorName,
                    IssueDate = DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy"),
                    Description = p.Description,


                }).ToList().OrderByDescending(s => s.Pk_SrcID);
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult TaskGetRec(string Pk_SrcID = "", string Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {


            try
            {

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_SrcID", Pk_SrcID));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oItemPart.SearchTask(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_OSTasks> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Vw_OSTasks>().ToList();


                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_SrcID=p.Pk_SrcID,
                    MaterialName = p.Name,
                    VendorName=p.VendorName,
                    Weight = p.IssueWeight,
                    Width=p.Width,
                    Length = p.Length,
                    LayerWt = p.LayerWt,
                    Description=p.Description,
                    ExpectedNos = p.AppNos,
                    ReelNo=p.ReelNo,
                    IssueDate = DateTime.Parse(p.IssueDate.ToString()).ToString("dd/MM/yyyy"),
                    Pk_SrcDetID=p.Pk_SrcDetID,
                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });

            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }


        }




        public ActionResult JobRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["Pk_SrcID"]);


 //                SELECT "Vw_OSTasks"."Pk_Material", "Vw_OSTasks"."Name", "Vw_OSTasks"."VendorName", "Vw_OSTasks"."Pk_SrcID", "Vw_OSTasks"."IssueWeight", 
 //               "Vw_OSTasks"."ReelNo", "Vw_OSTasks"."Length", "Vw_OSTasks"."Width", "Vw_OSTasks"."LayerWt", "Vw_OSTasks"."AppNos", "Vw_OSTasks"."IssueDate", 
 //               "Vw_OSTasks"."Description"
 //FROM   "Balaji"."dbo"."Vw_OSTasks" "Vw_OSTasks"
 //ORDER BY "Vw_OSTasks"."Pk_SrcID", "Vw_OSTasks"."Pk_Material"




                List<Vw_OSTasks> BillList = new List<Vw_OSTasks>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.Vw_OSTasks.Where(x => x.Pk_SrcID == sInvno).Select(x => x).OfType<Vw_OSTasks>().ToList();

                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_SrcID");
                    dt.Columns.Add("IssueWeight");
                    dt.Columns.Add("ReelNo");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("VendorName");
                    dt.Columns.Add("LayerWt");
                    dt.Columns.Add("AppNos");
                    dt.Columns.Add("IssueDate");
                    dt.Columns.Add("Description");
                    dt.Columns.Add("Pk_Material");
                    //dt.Columns.Add("FORM_H");
                    //dt.Columns.Add("FORM_CT3");

                    foreach (Vw_OSTasks entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["Pk_SrcID"] = entity.Pk_SrcID;
                        row["IssueWeight"] = entity.IssueWeight;
                        row["ReelNo"] = entity.ReelNo;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["VendorName"] = entity.VendorName;
                        row["LayerWt"] = entity.LayerWt;
                        row["AppNos"] = entity.AppNos;
                        row["IssueDate"] = entity.IssueDate;
                        row["Description"] = entity.Description;
                        row["Pk_Material"] = entity.Pk_Material;
                        //row["FORM_H"] = entity.FORM_H;
                        //row["FORM_CT3"] = entity.FORM_CT3;
                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.JobWork orp = new CogitoStreamline.Report.JobWork();

                    orp.Load("@\\Report\\JobWork.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "WO" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }




 //       public ActionResult JobRepDates(string data = "")
 //       {
 //           try
 //           {
 //               Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
 //               //DateTime= DateTime.ParseExact(oValues["FromDate"].ToString());
 //               //int sInvno = Convert.ToInt32(oValues["Pk_SrcID"]);
 //               //int sInvno = Convert.ToInt32(oValues["Pk_SrcID"]);

 ////                SELECT "Vw_OSTasks"."Pk_Material", "Vw_OSTasks"."Name", "Vw_OSTasks"."VendorName", "Vw_OSTasks"."Pk_SrcID", "Vw_OSTasks"."IssueWeight", 
 ////               "Vw_OSTasks"."ReelNo", "Vw_OSTasks"."Length", "Vw_OSTasks"."Width", "Vw_OSTasks"."LayerWt", "Vw_OSTasks"."AppNos", "Vw_OSTasks"."IssueDate", 
 ////               "Vw_OSTasks"."Description"
 ////FROM   "Balaji"."dbo"."Vw_OSTasks" "Vw_OSTasks"
 ////ORDER BY "Vw_OSTasks"."Pk_SrcID", "Vw_OSTasks"."Pk_Material"


 //               //BillList = dc.VW_Invoice.Where(x => x.InvDate.Value.Month == dmonth && x.InvDate.Value.Year == dyear).Select(x => x).OfType<VW_Invoice>().ToList();

 //               List<Vw_OSTasks> BillList = new List<Vw_OSTasks>();
 //               CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

 //               //BillList = dc.VwInvoices.ToList();
 //               BillList = dc.Vw_OSTasks.Where(x => x.IssueDate).Select(x => x).OfType<Vw_OSTasks>().ToList();

 //               if (BillList.Count > 0)
 //               {
 //                   DataTable dt = new DataTable();

 //                   dt.Columns.Add("Name");
 //                   dt.Columns.Add("Pk_SrcID");
 //                   dt.Columns.Add("IssueWeight");
 //                   dt.Columns.Add("ReelNo");
 //                   dt.Columns.Add("Length");
 //                   dt.Columns.Add("Width");
 //                   dt.Columns.Add("VendorName");
 //                   dt.Columns.Add("LayerWt");
 //                   dt.Columns.Add("AppNos");
 //                   dt.Columns.Add("IssueDate");
 //                   dt.Columns.Add("Description");
 //                   dt.Columns.Add("Pk_Material");
 //                   //dt.Columns.Add("FORM_H");
 //                   //dt.Columns.Add("FORM_CT3");

 //                   foreach (Vw_OSTasks entity in BillList)
 //                   {
 //                       DataRow row = dt.NewRow();

 //                       row["Name"] = entity.Name;
 //                       row["Pk_SrcID"] = entity.Pk_SrcID;
 //                       row["IssueWeight"] = entity.IssueWeight;
 //                       row["ReelNo"] = entity.ReelNo;
 //                       row["Length"] = entity.Length;
 //                       row["Width"] = entity.Width;
 //                       row["VendorName"] = entity.VendorName;
 //                       row["LayerWt"] = entity.LayerWt;
 //                       row["AppNos"] = entity.AppNos;
 //                       row["IssueDate"] = entity.IssueDate;
 //                       row["Description"] = entity.Description;
 //                       row["Pk_Material"] = entity.Pk_Material;
 //                       //row["FORM_H"] = entity.FORM_H;
 //                       //row["FORM_CT3"] = entity.FORM_CT3;
 //                       dt.Rows.Add(row);
 //                   }


 //                   DataSet ds = new DataSet();
 //                   ds.Tables.Add(dt);
 //                   CogitoStreamline.Report.JobWork orp = new CogitoStreamline.Report.JobWork();

 //                   orp.Load("@\\Report\\JobWork.rpt");
 //                   orp.SetDataSource(dt.DefaultView);

 //                   string pdfPath = Server.MapPath("~/ConvertPDF/" + "JobWork" + sInvno + ".pdf");
 //                   FileInfo file = new FileInfo(pdfPath);
 //                   if (file.Exists)
 //                   {
 //                       file.Delete();
 //                   }
 //                   var pd = new PrintDocument();


 //                   orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
 //                   orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
 //                   DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
 //                   objDiskOpt.DiskFileName = pdfPath;

 //                   orp.ExportOptions.DestinationOptions = objDiskOpt;
 //                   orp.Export();

 //               }
 //               return null;
 //           }
 //           catch (Exception ex)
 //           {
 //               return null;

 //           }
 //       }

    }
}

