﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using System.Data;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace CogitoStreamLine.Controllers
{

    public class PaperStockListController : CommonController
    {
        StocksList oPStk = new StocksList();
        //Country oCountry = new Country();
        public PaperStockListController()
        {
            oDoaminObject = new StocksList();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Paper Stock";
            return base.Index();
        }
        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();
        protected override void Dispose(bool disposing)
        {
            //oState.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                StocksList oStock = oDoaminObject as StocksList;
                oStock.ID = decimal.Parse(pId);
                vw_PaperRollStock oAtualObject = oStock.DAO as vw_PaperRollStock;
                //Create a anominious object here to break the circular reference

                var oStockToDisplay = new
                {
                    Pk_Material = oAtualObject.Pk_Material,
                    MaterialName = oAtualObject.Name,
                    RollNo=oAtualObject.RollNo,
                    Quantity = oAtualObject.Quantity
                };

                return Json(new { success = true, data = oStockToDisplay });
            }
            else
            {
                var oStockToDisplay = new vw_PaperRollStock();
                return Json(new { success = true, data = oStockToDisplay });
            }
        }


        [HttpPost]
        public JsonResult StockListByFiter(string Pk_Stock = "", string MaterialCategory = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Stock", Pk_Stock));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("MaterialCategory", MaterialCategory));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Stock> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Stock>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                    Pk_Stock = p.Pk_Stock,
                    MaterialName = p.Inv_Material.Name,
                    Quantity = p.Quantity,

                }).ToList().OrderBy(s => s.Pk_Stock);

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult JCPaperList(string Fk_BoxID = "", string PartID = "", string MaterialName = "", string MaterialJCName = "", string GSM = "", string BF = "", string Deckle = "", string Mill = "", string Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PartID", PartID));
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                //oSearchParams.Add(new SearchParameter("Mill", Mill));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("Color", Color));
                oSearchParams.Add(new SearchParameter("Deckle", Deckle));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPStk.SearchpLoadStk(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_PaperStock> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Vw_PaperStock>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                   // Pk_PaperStock = p.Pk_PaperStock,
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                    //Quantity = p.Quantity,
                    //RollNo = p.RollNo,
                    GSM = p.GSM,
                    Deckle = p.Deckle,
                    BF = p.BF,
                    Color = p.ColorName,
                    SName = p.MillName,

                }).ToList();

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult PaperStockListByFiter(string Pk_Material = "", string RollNoP = "", string Deckle = "", string MaterialName = "", string MaterialJCName = "", string GSM = "", string BF = "", string Mill = "", string RollNo = "", string Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("MaterialJCName", MaterialJCName));
                oSearchParams.Add(new SearchParameter("Mill", Mill));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("RollNo", RollNo));
                oSearchParams.Add(new SearchParameter("RollNoP", RollNoP));
                oSearchParams.Add(new SearchParameter("Color", Color));
                oSearchParams.Add(new SearchParameter("Deckle", Deckle));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPStk.SearchpStk(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<vw_PaperRollStock> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<vw_PaperRollStock>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Where(p => p.Quantity > 0).Select(p => new
                {
                    Pk_PaperStock=p.Pk_PaperStock,
                    Pk_Material = p.Pk_Material,
                    Fk_JobCardID = p.Fk_JobCardID,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    RollNo=p.RollNo,
                    GSM = p.GSM,
                    Deckle=p.Deckle,
                    BF = p.BF,
                    Color = p.ColorName,
                    SName=p.SName,  
                

                }).ToList();

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult PaperStockListAge(string Pk_Material = "", string Deckle = "", string MaterialName = "", string MaterialJCName = "", string GSM = "", string BF = "", string Mill = "", string RollNo = "", string Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                oSearchParams.Add(new SearchParameter("MaterialJCName", MaterialJCName));
                oSearchParams.Add(new SearchParameter("Mill", Mill));
                oSearchParams.Add(new SearchParameter("GSM", GSM));
                oSearchParams.Add(new SearchParameter("BF", BF));
                oSearchParams.Add(new SearchParameter("RollNo", RollNo));
                oSearchParams.Add(new SearchParameter("Color", Color));
                oSearchParams.Add(new SearchParameter("Deckle", Deckle));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPStk.SearchpStkAge(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Age> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Vw_Age>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                    Pk_PaperStock = p.Pk_PaperStock,
                    Pk_Material = p.Pk_Material,
                    MaterialName = p.Name,
                    Quantity = p.Quantity,
                    RollNo = p.RollNo,
                    GSM = p.GSM,
                    Deckle = p.Deckle,
                    BF = p.BF,
                    Color = p.ColorName,
                    SName = p.MillName,
                    Age=p.Age,

                }).ToList().OrderByDescending(p => p.Age);  

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult MatIDStock(string Pk_Material = "", string Deckle = "", string MaterialName = "", string GSM = "", string BF = "", string Mill = "", string Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPStk.SearchpSummStk(oSearchParams);

                List<EntityObject> oStockToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_TotPaperStockList> oStockObjects = oStockToDisplayObjects.Select(p => p).OfType<Vw_TotPaperStockList>().ToList();

                //Create a anominious object here to break the circular reference
                var oStockToDisplay = oStockObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    StkQty = p.TQty,
                  

                }).ToList();

                return Json(new { Result = "OK", Records = oStockToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public ActionResult StockList(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
              //  int sInvno = Convert.ToInt32(oValues["Pk_EQID"]);

                List<vw_PaperRollStock> BillList = new List<vw_PaperRollStock>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //BillList = dc.VwInvoices.ToList();
                BillList = dc.vw_PaperRollStock.Select(x => x).OfType<vw_PaperRollStock>().OrderBy(x => x.GSM).ToList();


                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("SName");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("RollNo");


                    foreach (vw_PaperRollStock entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Name"] = entity.Name;
                        row["SName"] = entity.SName;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["Deckle"] = entity.Deckle;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Quantity"] = entity.Quantity;
                        row["ColorName"] = entity.ColorName;
                        row["RollNo"] = entity.RollNo;

                        dt.Rows.Add(row);
                    }


                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.PaperStock orp = new CogitoStreamline.Report.PaperStock();

                    orp.Load("@\\Report\\PaperStock.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "PaperStock"+ ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult PaperRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity>0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(p=>p.GSM).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
               
                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                       

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.PaperStk orp = new CogitoStreamline.Report.PaperStk();

                    orp.Load("@\\Report\\PaperStk.rpt");
                    orp.SetDataSource(dt.DefaultView);



                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "PaperStk" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;                 
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult MillWiseRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(p => p.GSM).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("SName");


                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.MillWise orp = new CogitoStreamline.Report.MillWise();

                    orp.Load("@\\Report\\MillWise.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 
                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "MillWise" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult GSMWiseRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(p => p.GSM).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("SName");


                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.GsmWise orp = new CogitoStreamline.Report.GsmWise();

                    orp.Load("@\\Report\\GsmWise.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "GsmWise" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult StockRep(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(x => x.Deckle).ThenByDescending(x => x.GSM).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("SName");


                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.StockList orp = new CogitoStreamline.Report.StockList();

                    orp.Load("@\\Report\\StockList.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "StockList" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();



                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult BFWiseRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(p => p.BF).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("SName");


                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.BFWise orp = new CogitoStreamline.Report.BFWise();

                    orp.Load("@\\Report\\BFWise.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "BFWise" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult DeckelWiseRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(p => p.BF).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("SName");


                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.DeckelWise orp = new CogitoStreamline.Report.DeckelWise();

                    orp.Load("@\\Report\\DeckelWise.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "DeckelWise" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult ShadeWiseRep(string data = "")
        {
            try
            {


                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);


                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(p => p.BF).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("SName");


                    foreach (vw_PaperRollStock entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["SName"] = entity.SName;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.ShadeWise orp = new CogitoStreamline.Report.ShadeWise();

                    orp.Load("@\\Report\\ShadeWise.rpt");
                    orp.SetDataSource(dt.DefaultView);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "ShadeWise" + ".pdf");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();


                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult StockRepGYT(string data = "")
        {
            try
            {
                string Repfor = " Stock List  -  GYT ";
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                string CN = "GYT";
              //  List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();
                List<Vw_Age> PaperList = new List<Vw_Age>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                //PaperList = dc.vw_PaperRollStock.Where(x => x.Quantity > 0 && x.ColorName == CN).Select(x => x).OfType<vw_PaperRollStock>().OrderBy(x => x.Deckle).ThenByDescending(x => x.GSM).ToList();
                PaperList = dc.Vw_Age.Where(x => x.Quantity > 0 && x.ColorName == CN).Select(x => x).OfType<Vw_Age>().OrderBy(x => x.Deckle).ThenByDescending(x => x.GSM).ToList();
                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("Age");


                    foreach (Vw_Age entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["Age"] = entity.Age;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.StockColor orp = new CogitoStreamline.Report.StockColor();

                    orp.Load("@\\Report\\StockColor.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    ParameterFieldDefinitions crParameterFieldDefinitions1;
                    ParameterFieldDefinition crParameterFieldDefinition1;
                    ParameterValues crParameterValues1 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                    crParameterDiscreteValue1.Value = Repfor;
                    crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition1 = crParameterFieldDefinitions1["Title"];
                    crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                    crParameterValues1.Clear();
                    crParameterValues1.Add(crParameterDiscreteValue1);
                    crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);


                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 

                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Stock_GYT" + ".xlsx");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    //objDiskOpt.DiskFileName = pdfPath;

                    //orp.ExportOptions.DestinationOptions = objDiskOpt;
                    //orp.Export();

                    ExportOptions rptOptions = new ExportOptions();
                    ExcelDataOnlyFormatOptions pdffprnat = new ExcelDataOnlyFormatOptions();
                    //PdfFormatOptions pdffprnat = new PdfFormatOptions();
                    // pdffprnat.CreateBookmarksFromGroupTree = true;
                    rptOptions.ExportFormatOptions = pdffprnat;
                    rptOptions.ExportFormatType = ExportFormatType.ExcelWorkbook;
                    rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                    dest.DiskFileName = pdfPath;
                    rptOptions.ExportDestinationOptions = dest;
                    orp.Export(rptOptions);
                   

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


        public ActionResult StockRepNat(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string Repfor = " Stock List  -  Natural ";
                string CN = "Natural";
                List<Vw_Age> PaperList = new List<Vw_Age>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperList = dc.Vw_Age.Where(x => x.Quantity > 0 && x.ColorName == CN).Select(x => x).OfType<Vw_Age>().OrderBy(x => x.Deckle).ThenByDescending(x => x.GSM).ToList();

                if (PaperList.Count > 0)
                {
                    DataTable dt = new DataTable("PaperRollStock");

                    dt.Columns.Add("RollNo");
                    dt.Columns.Add("Quantity");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Pk_Material");
                    dt.Columns.Add("Pk_PaperStock");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("Age");


                    foreach (Vw_Age entity in PaperList)
                    {
                        DataRow row = dt.NewRow();

                        row["RollNo"] = entity.RollNo;
                        row["Quantity"] = entity.Quantity;
                        row["Name"] = entity.Name;
                        row["Pk_Material"] = entity.Pk_Material;
                        row["Pk_PaperStock"] = entity.Pk_PaperStock;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["ColorName"] = entity.ColorName;
                        row["Deckle"] = entity.Deckle;
                        row["MillName"] = entity.MillName;
                        row["Age"] = entity.Age;

                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.StockColor orp = new CogitoStreamline.Report.StockColor();

                    orp.Load("@\\Report\\StockColor.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    ParameterFieldDefinitions crParameterFieldDefinitions1;
                    ParameterFieldDefinition crParameterFieldDefinition1;
                    ParameterValues crParameterValues1 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
                    crParameterDiscreteValue1.Value = Repfor;
                    crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition1 = crParameterFieldDefinitions1["Title"];
                    crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

                    crParameterValues1.Clear();
                    crParameterValues1.Add(crParameterDiscreteValue1);
                    crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);


                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders(); 


                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "Stock_NAT" + ".xlsx");

                    FileInfo file = new FileInfo(pdfPath);

                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    //orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    //orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    //DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    //objDiskOpt.DiskFileName = pdfPath;

                    //orp.ExportOptions.DestinationOptions = objDiskOpt;
                    //orp.Export();

                    ExportOptions rptOptions = new ExportOptions();
                    ExcelDataOnlyFormatOptions pdffprnat = new ExcelDataOnlyFormatOptions();
                    //PdfFormatOptions pdffprnat = new PdfFormatOptions();
                    // pdffprnat.CreateBookmarksFromGroupTree = true;
                    rptOptions.ExportFormatOptions = pdffprnat;
                    rptOptions.ExportFormatType = ExportFormatType.ExcelWorkbook;
                    rptOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions dest = new DiskFileDestinationOptions();
                    dest.DiskFileName = pdfPath;
                    rptOptions.ExportDestinationOptions = dest;
                    orp.Export(rptOptions);
                   

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }



        public ActionResult DeleteRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                   int sInvno = Convert.ToInt32(oValues["fkBranch"]);
                   string rollNo = (oValues["RollNo"].ToString());
                
                  
                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

                PaperStock oPaperList = _oEntites.PaperStock.Where(x => x.Pk_PaperStock == sInvno && x.RollNo == rollNo).Single();

                
                    //oPaperList.
                    _oEntites.DeleteObject(oPaperList);

                    _oEntites.SaveChanges();


                 
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public ActionResult UpdateRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
               int sInvno = Convert.ToInt32(oValues["fkBranch"]);
                string rollNo = (oValues["RollNo"].ToString());
                int StockVal = Convert.ToInt32(oValues["Stock"]);
                int pkmat = Convert.ToInt32(oValues["pkmat"]);
                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
                if (rollNo.Length > 0)
                {
                    PaperStock oPaperList = _oEntites.PaperStock.Where(x => x.Pk_PaperStock == sInvno).Single();

                    oPaperList.RollNo = rollNo;
                    oPaperList.Quantity = StockVal;
                    oPaperList.Fk_Material = pkmat;

                    _oEntites.SaveChanges();
                }
                else
                {
                    PaperStock oPaperList1 = _oEntites.PaperStock.Where(x => x.Pk_PaperStock == sInvno).Single();
                    oPaperList1.RollNo = rollNo;
                    oPaperList1.Quantity = StockVal;
                    oPaperList1.Fk_Material = pkmat;

                    _oEntites.SaveChanges();
                }
               

                
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
     
        
        
        public ActionResult InsertRow(string data = "")
        {
            try
            {

                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                //int sInvno =
                string rollNo = (oValues["RollNo"].ToString());
                int StockVal = Convert.ToInt32(oValues["fkBranch"]);
                int pkmat = Convert.ToInt32(oValues["MatID"]);
                List<vw_PaperRollStock> PaperList = new List<vw_PaperRollStock>();

                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

              //  PaperStock oPaperList = _oEntites.PaperStock.Where(x => x.Pk_PaperStock == sInvno && x.RollNo == rollNo).Single();
                PaperStock oPaperList = new PaperStock();


                oPaperList.RollNo = rollNo;
                oPaperList.Quantity = StockVal;
                oPaperList.Fk_Material = pkmat;
                _oEntites.AddToPaperStock(oPaperList);
         _oEntites.SaveChanges();


                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }


    }
}
