﻿
//Name            : Payment Controller Class
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
//Author          :   Shantha
//Date            :   20/10/2015
//CRH Number      :   SL0002
//Modifications   :

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class PaymtController : CommonController
    {
        Paymt oPaymt = new Paymt();
        BankN oBank = new BankN();
        Vendor oVendor = new Vendor();


        public PaymtController()
        {
            oDoaminObject = new Paymt();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Payment";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oPaymt.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Paymt oPaymt = oDoaminObject as Paymt;
                oPaymt.ID = decimal.Parse(pId);
                Payment oAtualObject = oPaymt.DAO as Payment;
                //Create a anominious object here to break the circular reference

                var oPaymentToDisplay = new
                {
                    Fk_Vendor=oAtualObject.Fk_Vendor,
                    Pk_PaymentID = oAtualObject.Pk_PaymentID,
                    Amount=oAtualObject.Amount,
                    Description = oAtualObject.Description,
                    ChequeNo=oAtualObject.ChequeNo,
                    ChequeDate = oAtualObject.ChequeDate != null ? DateTime.Parse(oAtualObject.ChequeDate.ToString()).ToString("dd/MM/yyyy") : "",
                     Fk_Bank=oAtualObject.Fk_Bank
                };

                return Json(new { success = true, data = oPaymentToDisplay });
            }
            else
            {
                var oPaymentToDisplay = new Payment();
                return Json(new { success = true, data = oPaymentToDisplay });
            }
        }

        [HttpPost]
        public JsonResult getVendor(string pId = "")
        {
            List<gen_Vendor> oListOfVendor = oVendor.Search(null).ListOfRecords.OfType<gen_Vendor>().ToList();
            var oVendorToDisplay = oListOfVendor.Select(p => new
            {
                Name = p.VendorName,
                Id = p.Pk_Vendor
            });

            return Json(oVendorToDisplay);
        }


        [HttpPost]
        public JsonResult getBank(string pId = "")
        {
            List<Bank> oListOfBank = oBank.Search(null).ListOfRecords.OfType<Bank>().ToList();
            var oBankToDisplay = oListOfBank.Select(p => new
            {
                Name = p.BankName,
                Id = p.Pk_BankID
            });

            return Json(oBankToDisplay);
        }


        //[HttpPost]
        //public JsonResult ColorDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sColorName = oValues["ColorName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("ColorName", sColorName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_Color> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<gen_Color>().ToList();

        //        var oColorToDisplay = oColorObjects.Select(p => new
        //        {
        //            Pk_Color = p.Pk_Color,
        //            ColorName = p.ColorName
        //        }).ToList().OrderBy(s => s.ColorName);

        //        return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult PaymentListByFiter(string Pk_PaymentID = "", string ChequeNo = "", string ChequeDate = "", string Fk_Vendor = "", string Description = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();

                oSearchParams.Add(new SearchParameter("Pk_PaymentID", Pk_PaymentID));
                oSearchParams.Add(new SearchParameter("Fk_Vendor", Fk_Vendor));
                oSearchParams.Add(new SearchParameter("ChequeNo", ChequeNo));
                oSearchParams.Add(new SearchParameter("Description", Description));
                oSearchParams.Add(new SearchParameter("ChequeDate", ChequeDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaymentToDisplayObjects = oSearchResult.ListOfRecords;
                List<Payment> oPaymentObjects = oPaymentToDisplayObjects.Select(p => p).OfType<Payment>().ToList();

                //Create a anominious object here to break the circular reference
                var oPaymentToDisplay = oPaymentObjects.Select(p => new
                {
                    Pk_PaymentID = p.Pk_PaymentID,
                    Description= p.Description,
                    ChequeNo=p.ChequeNo,
                    ChequeDate=p.ChequeDate != null ? DateTime.Parse(p.ChequeDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_Vendor =p.gen_Vendor.VendorName,
                    Amount=p.Amount

                }).ToList().OrderBy(s => s.Pk_PaymentID);

                return Json(new { Result = "OK", Records = oPaymentToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
