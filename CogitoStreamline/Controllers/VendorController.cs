﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
namespace CogitoStreamline.Controllers
{
    public class VendorController : CommonController
    {
        //
        // GET: /Vendor/

        #region Variables
        Country oCountry = new Country();
        City oCity = new City();
        State oState = new State();
        Vendor oVendor = new Vendor();
        #endregion Variables

        #region Constractor
        public VendorController()
        {
            oDoaminObject = new Vendor();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Vendor";
            return base.Index();
        }
        [HttpPost]
        public JsonResult getCountry(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();
            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }
        [HttpPost]
        public JsonResult getState(string pId = "")
        {
            List<gen_State> oListOfState = oState.Search(null).ListOfRecords.OfType<gen_State>().ToList();
            var ostateToDisplay = oListOfState.Select(p => new
            {
                Name = p.StateName,
                Id = p.Pk_StateID
            });

            return Json(ostateToDisplay);
        }
        [HttpPost]
        public JsonResult getCity(string pId = "")
        {
            List<gen_City> oListOfCity = oCity.Search(null).ListOfRecords.OfType<gen_City>().ToList();
            var oCityToDisplay = oListOfCity.Select(p => new
            {
                Name = p.CityName,
                Id = p.Pk_CityID
            });

            return Json(oCityToDisplay);
        }
        protected override void Dispose(bool disposing)
        {
            oVendor.Dispose();
            oCountry.Dispose();
            oState.Dispose();
            oCity.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Vendor oVendor = oDoaminObject as Vendor;
                oVendor.ID = decimal.Parse(pId);
                gen_Vendor oAtualObject = oVendor.DAO as gen_Vendor;
                int i = -1;
                var oMaterials = oAtualObject.gen_VendorMaterials.Select(p => new
                {
                    Pk_VendorMaterial = p.Pk_VendorMaterial,
                    slno = ++i,
                    Fk_Material = p.Fk_Material,
                    MaterialName = p.Inv_Material.Name,
                    Rate=p.Rate
                });

                var oContacts = oAtualObject.gen_VendorContacts.Select(p => new
                {
                    id = p.gen_ContactPerson.Pk_ContactPersonId,
                    Name = p.gen_ContactPerson.ContactPersonName,
                    Details = "Number:" + p.gen_ContactPerson.ContactPersonNumber +
                              ";eMail:" + p.gen_ContactPerson.ContactPersonEmailId +
                             ";State:" + p.gen_ContactPerson.gen_State.StateName +
                              ";City:" + p.gen_ContactPerson.gen_City.CityName +
                              ";Street:" + p.gen_ContactPerson.Address_No_Street +
                              ";Pin:" + p.gen_ContactPerson.Address_PinCode
                });


                //Create a anominious object here to break the circular reference
                var oVendorToDisplay = new
                {
                    VendorName = oAtualObject.VendorName,
                    VendorType = oAtualObject.VendorType,
                    OfficeContactNumber = oAtualObject.OfficeContactNumber,
                    Email = oAtualObject.Email,
                    //BusinessType = oAtualObject.BusinessType,
                    Address_No_Street = oAtualObject.Address_No_Street,
                    Fk_State = oAtualObject.Fk_State,
                    Fk_City = oAtualObject.Fk_City,
                    Fk_Country=oAtualObject.Fk_Country,
                    PinCode = oAtualObject.PinCode,
                    Product=oAtualObject.Product,
                    Pk_Vendor = oAtualObject.Pk_Vendor,
                    LandLine=oAtualObject.LandLine,
                    MobileNumber = oAtualObject.MobileNumber,
                    Contacts = Json(oContacts).Data,
                    MaterialData = Json(oMaterials).Data
                };

                return Json(new { success = true, data = oVendorToDisplay });
            }
            else
            {
                var oVendorToDisplay = new gen_Vendor();
                return Json(new { success = true, data = oVendorToDisplay });
            }
        }
        [HttpPost]
        public JsonResult VendorListByFiter(string VendorName = null, string Pk_Vendor = "",string VendorType="", string VendorContact = "", string LandLine = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("VendorName", VendorName));
                oSearchParams.Add(new SearchParameter("Pk_Vendor", Pk_Vendor.ToString()));
                oSearchParams.Add(new SearchParameter("VendorType", VendorType.ToString()));
                oSearchParams.Add(new SearchParameter("VendorContact", VendorContact));
                oSearchParams.Add(new SearchParameter("LandLine", LandLine));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oVendorsToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Vendor> oVendorObjects = oVendorsToDisplayObjects.Select(p => p).OfType<gen_Vendor>().ToList();

                //Create a anominious object here to break the circular reference
                var oVendorsToDisplay = oVendorObjects.Select(p => new
                {
                    VendorName = p.VendorName,
                    OfficeContactNumber = p.OfficeContactNumber,
                   LandLine = p.LandLine,
                    City=p.gen_City.CityName,
                    Pk_Vendor = p.Pk_Vendor,
                    VendorType = p.VendorType,
                    Email=p.Email
                }).ToList().OrderBy(p => p.VendorName); 

                return Json(new { Result = "OK", Records = oVendorsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult VendorContacts(string Pk_Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Vendor oVendor = oDoaminObject as Vendor;
                oVendor.ID = decimal.Parse(Pk_Vendor);
                gen_Vendor oVendorObjects = oVendor.DAO as gen_Vendor;

                var oVendorsContactsToDisplay = oVendorObjects.gen_VendorContacts.Select(p => new
                {
                    Pk_VendorContacts = p.Pk_VendorContact,
                    ContactPersonName = p.gen_ContactPerson.ContactPersonName,
                    ContactPersonDesignation = p.gen_ContactPerson.ContactPersonDesignation,
                    ContactPersonNumber = p.gen_ContactPerson.ContactPersonNumber,
                    ContactPersonEmailId = p.gen_ContactPerson.ContactPersonEmailId,
                    City = p.gen_ContactPerson.gen_City.CityName
                }).ToList();

                return Json(new { Result = "OK", Records = oVendorsContactsToDisplay, TotalRecordCount = oVendorsContactsToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult VendorMaterials(string Pk_Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                Vendor oVendor = oDoaminObject as Vendor;
                oVendor.ID = decimal.Parse(Pk_Vendor);
                gen_Vendor oVendorObjects = oVendor.DAO as gen_Vendor;

                var oVendorMaterialsToDisplay = oVendorObjects.gen_VendorMaterials.Select(p => new
                {
                    Pk_VendorMaterial = p.Pk_VendorMaterial,
                    Name = p.Inv_Material.Name,
                    ColorName = p.Inv_Material.Fk_Color != null ? p.Inv_Material.gen_Color.ColorName : null,
                    MillName = p.Inv_Material.Fk_Mill!=null?p.Inv_Material.gen_Mill.MillName:null,
                    PaperTypeName = p.Inv_Material.Fk_PaperType!=null?p.Inv_Material.gen_PaperType.PaperTypeName:null
                }).ToList();

                return Json(new { Result = "OK", Records = oVendorMaterialsToDisplay, TotalRecordCount = oVendorMaterialsToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult VendorDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sVendorName = oValues["VendorName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("VendorName", sVendorName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oVendorToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Vendor> oVendorObjects = oVendorToDisplayObjects.Select(p => p).OfType<gen_Vendor>().ToList();

                var oVendorsToDisplay = oVendorObjects.Select(p => new
                {
                    Pk_Vendor = p.Pk_Vendor,
                    VendorName = p.VendorName
                }).ToList().OrderBy(s => s.VendorName);

                return Json(new { Success = true, Records = oVendorsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ContactDuplicateChecking(string data = "")
        {
            try
            {
                Vendor oVendor = new Vendor();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sContactPersonName = oValues["ContactPersonName"];
                string sPk_Vendor = oValues["Pk_Vendor"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("ContactPersonName", sContactPersonName));
                oSearchParams.Add(new SearchParameter("Pk_Vendor", sPk_Vendor));

                SearchResult oSearchResult = oVendor.SearchContacts(oSearchParams);

                List<EntityObject> oContactToDisplayObjects = oSearchResult.ListOfRecords;
                List<vw_VendorDetails> oContactObjects = oContactToDisplayObjects.Select(p => p).OfType<vw_VendorDetails>().ToList();

                var oContactToDisplay = oContactObjects.Select(p => new
                {
                    Pk_Vendor = p.Pk_Vendor,
                    ContactPersonName = p.ContactPersonName,
                }).ToList().OrderBy(s => s.Pk_Vendor);

                return Json(new { Success = true, Records = oContactToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        #endregion Methods
    }
}

