﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class BrnchController : Controller
//    {
//        //
//        // GET: /Brnch/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class BrnchController : CommonController
    {
        Brnch oBranch = new Brnch();
        public BrnchController()
        {
            oDoaminObject = new Brnch();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Branch";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oBranch.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Brnch oBranch = oDoaminObject as Brnch;
                oBranch.ID = decimal.Parse(pId);
                Branch oAtualObject = oBranch.DAO as Branch;
                //Create a anominious object here to break the circular reference
                var oBranchToDisplay = new
                {
                    Pk_BranchID = oAtualObject.Pk_BranchID,
                    BranchName = oAtualObject.BranchName,
                    Fk_Tanent = oAtualObject.Fk_Tanent
                };
                return Json(new { success = true, data = oBranchToDisplay });
            }
            else
            {
                var oBranchToDisplay = new Branch();
                return Json(new { success = true, data = oBranchToDisplay });
            }
        }
        [HttpPost]
        public JsonResult BranchDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sBranchName = oValues["BranchName"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("BranchName", sBranchName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oBranchToDisplayObjects = oSearchResult.ListOfRecords;
                List<Branch> oBranchObjects = oBranchToDisplayObjects.Select(p => p).OfType<Branch>().ToList();
                var oBranchToDisplay = oBranchObjects.Select(p => new
                {
                    Pk_BranchID = p.Pk_BranchID,
                    BranchName = p.BranchName,
                    Fk_Tanent = p.Fk_Tanent
                }).ToList().OrderBy(s => s.BranchName);
                return Json(new { Success = true, Records = oBranchToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BranchListByFiter(string Pk_BranchID = "", string BranchName = "", string Fk_Tanent = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BranchID", Pk_BranchID));
                oSearchParams.Add(new SearchParameter("BranchName", BranchName));
                oSearchParams.Add(new SearchParameter("Fk_Tanent", Fk_Tanent));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oBranchToDisplayObjects = oSearchResult.ListOfRecords;
                List<Branch> oBranchObjects = oBranchToDisplayObjects.Select(p => p).OfType<Branch>().ToList();
                //Create a anominious object here to break the circular reference
                var oBranchToDisplay = oBranchObjects.Select(p => new
                {
                    Pk_BranchID = p.Pk_BranchID,
                    BranchName = p.BranchName,
                    Fk_Tanent = p.Fk_Tanent
                }).ToList().OrderBy(s => s.BranchName);
                return Json(new { Result = "OK", Records = oBranchToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

