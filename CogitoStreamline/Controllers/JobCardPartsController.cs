﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing.Printing;
using System.IO;

namespace CogitoStreamline.Controllers
{
    public class JobCardPartsController : CommonController
    {
        //
        // GET: /Order/
        //Customer oCustomer = new Customer();
        ////Product oProduct = new Product();
        JobCardParts oPaperStock = new JobCardParts();

        public JobCardPartsController()
        {
            oDoaminObject = new JobCardParts();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "JobCardParts";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        //[HttpPost]
        //public JsonResult getCustomer(string pId = "")
        //{
        //    List<gen_Customer> oListOfOrderRelationShips = oCustomer.Search(null).ListOfRecords.OfType<gen_Customer>().ToList();
        //    var oContriesToDisplay = oListOfOrderRelationShips.Select(p => new
        //    {
        //        Name = p.CustomerName,
        //        Id = p.Pk_Customer
        //    });

        //    return Json(oContriesToDisplay);
        //}


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                JobCardParts oJob = oDoaminObject as JobCardParts;
                oJob.ID = decimal.Parse(pId);
                JobCardPartsMaster oAtualObject = oJob.DAO as JobCardPartsMaster;

                int i = -1;

                var oJobDet = oAtualObject.JobCardPartsDetails.Select(p => new
                {

                    Pk_JobCardPartsDet = p.Pk_JobCardPartsDet,
                    slno = ++i,
                    Fk_JobCardPartsID = p.Fk_JobCardPartsID,
                    Fk_PaperStock = p.Fk_PaperStock,
                    Name = p.PaperStock.Inv_Material.Name,
                    RollNo=p.PaperStock.RollNo,
                    Quantity = p.PaperStock.Quantity,
                    QtyConsumed = p.QtyConsumed
                    //DeliveryDate = DateTime.Parse(p.DeliveryDate.ToString()).ToString("dd/MM/yyyy"),
                });


                //Create a anominious object here to break the circular reference
                var oJobToDisplay = new
                {
                    Pk_JobCardPartsID = oAtualObject.Pk_JobCardPartsID,
                    Corrugation = oAtualObject.Corrugation,
                    Fk_Order = oAtualObject.Fk_Order,
                    Fk_Status = oAtualObject.Fk_Status,
                    Fk_PartID = oAtualObject.Fk_PartID,
                    Printing = oAtualObject.Printing,
                    Calico = oAtualObject.Calico,
                    Others = oAtualObject.Others,
                    Invno = oAtualObject.Invno,
                    JDate = DateTime.Parse(oAtualObject.JDate.ToString()).ToString("dd/MM/yyyy"),
                    TopPaperQty = oAtualObject.TopPaperQty,
                    TwoPlyQty = oAtualObject.TwoPlyQty,
                    TwoPlyWt = oAtualObject.TwoPlyWt,
                    PastingQty = oAtualObject.PastingQty,
                    PastingWstQty = oAtualObject.PastingWstQty,
                    RotaryQty = oAtualObject.RotaryQty,
                    RotaryWstQty = oAtualObject.RotaryWstQty,
                    PunchingQty = oAtualObject.PunchingQty,
                    PunchingWstQty = oAtualObject.PunchingWstQty,
                    SlotingQty = oAtualObject.SlotingQty,
                    SlotingWstQty = oAtualObject.SlotingWstQty,
                    PinningQty = oAtualObject.PinningQty,
                    PinningWstQty = oAtualObject.PinningWstQty,
                    FinishingQty = oAtualObject.FinishingQty,
                    FinishingWstQty = oAtualObject.FinishingWstQty,
                    TotalQty = oAtualObject.TotalQty,
                    TotalWstQty = oAtualObject.TotalWstQty,
                    MaterialData = Json(oJobDet).Data,
                    State = oAtualObject.wfStates.State
                  
                };

                return Json(new { success = true, data = oJobToDisplay });
            }
            else
            {
                var oJobToDisplay = new gen_Order();
                return Json(new { success = true, data = oJobToDisplay });
            }
        }
        [HttpPost]
        public JsonResult PaperStockList(string Fk_BoxID = "", string Fk_Enquiry = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_BoxID", Fk_BoxID));
                oSearchParams.Add(new SearchParameter("Fk_Enquiry", Fk_Enquiry));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchPaperStock(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_BoxPaperStock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<Vw_BoxPaperStock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Fk_BoxID = p.Fk_BoxID,
                    //Quantity = p.Expr1,
                    //Fk_Enquiry = p.Fk_Enquiry

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult JCPaperStockList(string Pk_PaperStock = "", string Fk_Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PaperStock", Pk_PaperStock));
                oSearchParams.Add(new SearchParameter("Fk_Material", Fk_Material));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchJCPaperStock(oSearchParams);

                List<EntityObject> oPapersToDisplayObjects = oSearchResult.ListOfRecords;
                List<PaperStock> oPaperObjects = oPapersToDisplayObjects.Select(p => p).OfType<PaperStock>().ToList();

                var oPapersToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_PaperStock = p.Pk_PaperStock,
                    Name = p.Inv_Material.Name,
                    Fk_Material = p.Fk_Material,
                    RollNo = p.RollNo,
                    Quantity = p.Quantity

                }).ToList();

                return Json(new { Result = "OK", Records = oPapersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult MaterialPartsList(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                //oSearchParams.Add(new SearchParameter("Name", Name));
                //oSearchParams.Add(new SearchParameter("CatName", Fk_MaterialCategory));
                //oSearchParams.Add(new SearchParameter("Fk_Mill", Fk_Mill));
                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchParts(oSearchParams);

                List<EntityObject> oMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_MaterialParts> oMaterialObjects = oMaterialsToDisplayObjects.Select(p => p).OfType<Vw_MaterialParts>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialsToDisplay = oMaterialObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    PartNo = p.PartNo,
                    //MillName = p.Fk_Mill != null ? p.gen_Mill.MillName : "",
                    //GSM = p.GSM,
                    //BF = p.BF,
                    //Length = p.Length,
                    //Width = p.Width

                }).ToList();

                return Json(new { Result = "OK", Records = oMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult JCPartsListByFiter(string Pk_JobCardPartsID = "", string JDate = "", string Fk_Order = "", string OnlyPending = "", string FromJDate = "", string ToJDate = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("Pk_JobCardPartsID", Pk_JobCardPartsID));
                oSearchParams.Add(new SearchParameter("JDate", JDate));
                oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<JobCardPartsMaster> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<JobCardPartsMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Select(p => new
                {
                    Pk_JobCardPartsID = p.Pk_JobCardPartsID,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Order = p.Fk_Order,
                    Fk_Status = p.wfStates.State
                   
                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult JCPartsList(string Pk_JobCardPartsID = "", string JDate = "", string Fk_Order = "", string OnlyPending = "", string FromJDate = "", string ToJDate = "", string Pk_Order = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("OnlyPending", OnlyPending));
                oSearchParams.Add(new SearchParameter("Pk_JobCardPartsID", Pk_JobCardPartsID));
                oSearchParams.Add(new SearchParameter("JDate", JDate));
                oSearchParams.Add(new SearchParameter("Fk_Order", Fk_Order));
                oSearchParams.Add(new SearchParameter("FromJDate", FromJDate));
                oSearchParams.Add(new SearchParameter("ToJDate", ToJDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oPaperStock.SearchJCPart(oSearchParams);

                List<EntityObject> oJobsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_JCPartDet> oJobObjects = oJobsToDisplayObjects.Select(p => p).OfType<Vw_JCPartDet>().ToList();

                //Create a anominious object here to break the circular reference
                var oJobsToDisplay = oJobObjects.Select(p => new
                {
                    Pk_JobCardPartsID = p.Pk_JobCardPartsID,
                    JDate = DateTime.Parse(p.JDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Order = p.Fk_Order,
                    BoxName=p.BoxName,
                    PartName=p.PName,
                   // Fk_Status = p.wfStates.State





                }).ToList();

                return Json(new { Result = "OK", Records = oJobsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult InvDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["invno"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("InvNo", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oJCPToDisplayObjects = oSearchResult.ListOfRecords;
                List<JobCardPartsMaster> oJCPObjects = oJCPToDisplayObjects.Select(p => p).OfType<JobCardPartsMaster>().ToList();

                var oJCPToDisplay = oJCPObjects.Select(p => new
                {
                    Pk_JobCardPartsID = p.Pk_JobCardPartsID,
                    Invno = p.Invno

                }).ToList().OrderBy(s => s.Invno);

                return Json(new { Success = true, Records = oJCPToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }



        public ActionResult JCRep(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                int sInvno = Convert.ToInt32(oValues["JCNO"]);

                List<Vw_JCpartsRep> BillList = new List<Vw_JCpartsRep>();
                CogitoStreamLineEntities dc = new CogitoStreamLineEntities();


                BillList = dc.Vw_JCpartsRep.Where(x => x.Pk_JobCardPartsID == sInvno).Select(x => x).OfType<Vw_JCpartsRep>().ToList();

                //               SELECT "JobCardMaster"."Pk_JobCardID", "JobCardMaster"."JDate", "BoxMaster"."Name", "Inv_Material"."Name", 
                //              "JobCardMaster"."Fk_Order", "JobCardMaster"."Invno", "JobCardMaster"."Corrugation", "JobCardMaster"."TopPaperQty", 
                //              "JobCardMaster"."TwoPlyQty", "JobCardMaster"."TwoPlyWt", "JobCardMaster"."PastingQty", "JobCardMaster"."PastingWstQty",
                //              "JobCardMaster"."RotaryQty", "JobCardMaster"."RotaryWstQty", "JobCardMaster"."PunchingQty",
                //              "JobCardMaster"."PunchingWstQty", "JobCardMaster"."SlotingQty", "JobCardMaster"."SlotingWstQty",
                //              "JobCardMaster"."PinningQty", "JobCardMaster"."PinningWstQty", "JobCardMaster"."FinishingQty", 
                //              "JobCardMaster"."FinishingWstQty", "JobCardMaster"."TotalQty", "JobCardMaster"."TotalWstQty", 
                //              "gen_Customer"."CustomerName", "gen_Machine"."Name"
                //FROM   (((("ERP"."dbo"."JobCardDetails" "JobCardDetails" INNER JOIN "ERP"."dbo"."JobCardMaster" "JobCardMaster" 
                //              ON "JobCardDetails"."Fk_JobCardID"="JobCardMaster"."Pk_JobCardID") INNER JOIN "ERP"."dbo"."Inv_Material"
                //              "Inv_Material" ON "JobCardDetails"."Fk_Material"="Inv_Material"."Pk_Material") INNER JOIN "ERP"."dbo"."BoxMaster"
                //              "BoxMaster" ON "JobCardMaster"."Fk_BoxID"="BoxMaster"."Pk_BoxID") INNER JOIN "ERP"."dbo"."gen_Machine" "gen_Machine"
                //              ON "JobCardMaster"."Machine"="gen_Machine"."Pk_Machine") INNER JOIN "ERP"."dbo"."gen_Customer" "gen_Customer" ON 
                //              "BoxMaster"."Customer"="gen_Customer"."Pk_Customer"
                //ORDER BY "Inv_Material"."Name"



                decimal NoOfPapers = 0;
                decimal PlySheets = 0;

                decimal QtyReq = 0;
                decimal TkUpFactor = 0;
                decimal OrderQty = 0;
                decimal PVal = 0;
                decimal calcUps = 0;
                decimal DeckelVal;
                if (BillList.Count > 0)
                {
                    DataTable dt = new DataTable();

                    dt.Columns.Add("Pk_JobCardPartsID");
                    dt.Columns.Add("JDate");
                    dt.Columns.Add("BName");
                    dt.Columns.Add("CustomerName");
                    dt.Columns.Add("CuttingSize");
                    dt.Columns.Add("Rate");

                    dt.Columns.Add("PartTakeup");
                    dt.Columns.Add("BoardGSM");
                    dt.Columns.Add("BoardBS");
                    dt.Columns.Add("PlyVal");
                    //dt.Columns.Add("Pk_JobCardID");
                    dt.Columns.Add("Deckle");
                    dt.Columns.Add("PColor");
                    //dt.Columns.Add("PDetails");
                    //dt.Columns.Add("MFG");
                    //dt.Columns.Add("DPRSc");
                    dt.Columns.Add("GSM");
                    dt.Columns.Add("BF");
                    dt.Columns.Add("TotalQty");
                    dt.Columns.Add("Invno");

                    dt.Columns.Add("Printing");
                    dt.Columns.Add("OrdQty");
                    dt.Columns.Add("FluHt");
                    dt.Columns.Add("PName");
                    dt.Columns.Add("FluteName");
                    dt.Columns.Add("Weight");
                    dt.Columns.Add("DeliveryDate");
                    dt.Columns.Add("Tk_Factor");
                    dt.Columns.Add("Length");
                    dt.Columns.Add("Width");
                    dt.Columns.Add("Height");
                    dt.Columns.Add("ReturnQuantity");
                    dt.Columns.Add("MillName");
                    dt.Columns.Add("ColorName");
                    dt.Columns.Add("SchQty");


                    foreach (Vw_JCpartsRep entity in BillList)
                    {
                        DataRow row = dt.NewRow();

                        row["Pk_JobCardPartsID"] = entity.Pk_JobCardPartsID;
                        row["JDate"] = entity.JDate;
                        row["BName"] = entity.BName;
                        row["CustomerName"] = entity.CustomerName;
                        row["CuttingSize"] = entity.CuttingSize;
                        row["Rate"] = entity.PartRate;
                        row["PartTakeup"] = entity.PartTakeup;
                        row["BoardGSM"] = entity.BoardGSM;
                        row["BoardBS"] = entity.BoardBS;
                        row["PlyVal"] = entity.PlyVal;
                        //row["PColor"] = entity.PColor;
                        //row["PDetails"] = entity.PDetails;
                        //row["MFG"] = entity.MFG;
                        row["PName"] = entity.PName;
                        row["GSM"] = entity.GSM;
                        row["BF"] = entity.BF;
                        row["TotalQty"] = entity.TotalQty;
                        //row["OrdQty"] = entity.OrdQty;
                        row["Printing"] = entity.Printing;
                        row["FluHt"] = entity.FluteHt;
                        row["FluteName"] = entity.FluteName;
                        row["Weight"] = entity.Weight;
                        row["DeliveryDate"] = entity.DeliveryDate;
                        row["Tk_Factor"] = entity.Tk_Factor;
                        row["Length"] = entity.Length;
                        row["Width"] = entity.Width;
                        row["Height"] = entity.Height;
                        row["Deckle"] = entity.Deckle;
                        row["ColorName"] = entity.ColorName;
                        row["MillName"] = entity.MillName;
                        row["SchQty"] = entity.SchQty;
                        row["Invno"] = entity.Invno;

                        //    row["ReturnQuantity"] = entity.ReturnQuantity;

                        TkUpFactor = Convert.ToDecimal(entity.Invno);
                        DeckelVal = Convert.ToDecimal(entity.Deckle);
                        OrderQty = Convert.ToDecimal(entity.SchQty);                 ////earlier it was order qty, it should be schqty, after linking production schedule
                        PVal = Convert.ToDecimal(entity.PlyVal);
                        // DCl = Convert.ToDecimal(entity.Rate);
                        //     GSMVal  = Convert.ToDecimal(entity.GSM);

                        //var DecVal = (DeckelVal * 2);

                        //if (DecVal < 132)
                        //{

                        //}

                        if (TkUpFactor > 0)     ///FOR SGST
                        {
                            NoOfPapers = Convert.ToDecimal(OrderQty / TkUpFactor);

                            //PlySheets = Convert.ToDecimal(NoOfPapers * TkUpFactor);

                            if (PVal == 3)
                            {
                                PlySheets = Convert.ToDecimal(NoOfPapers * 1);
                            }
                            else
                                if (PVal == 5)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 2);
                                }

                                else if (PVal == 7)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 3);
                                }
                                else if (PVal == 9)
                                {
                                    PlySheets = Convert.ToDecimal(NoOfPapers * 4);
                                }
                        }

                        //  QtyReq = (NoOfPapers * TkUpFactor * CutLen / 100 * DCl / 100 * GSMVal / 100);
                        dt.Rows.Add(row);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    CogitoStreamline.Report.JCPartsRep orp = new CogitoStreamline.Report.JCPartsRep();

                    orp.Load("@\\Report\\JCPartsRep.rpt");
                    orp.SetDataSource(dt.DefaultView);


                    CrystalDecisions.CrystalReports.Engine.ReportDocument repDoc = orp;

                    ParameterFieldDefinitions crParameterFieldDefinitions3;
                    ParameterFieldDefinition crParameterFieldDefinition3;
                    ParameterValues crParameterValues3 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
                    crParameterDiscreteValue3.Value = NoOfPapers;
                    crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition3 = crParameterFieldDefinitions3["NoPly"];
                    crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

                    crParameterValues3.Clear();
                    crParameterValues3.Add(crParameterDiscreteValue3);
                    crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



                    ParameterFieldDefinitions crParameterFieldDefinitions4;
                    ParameterFieldDefinition crParameterFieldDefinition4;
                    ParameterValues crParameterValues4 = new ParameterValues();
                    ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
                    crParameterDiscreteValue4.Value = PlySheets;
                    crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
                    crParameterFieldDefinition4 = crParameterFieldDefinitions4["PlySh"];
                    crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

                    crParameterValues4.Clear();
                    crParameterValues4.Add(crParameterDiscreteValue4);
                    crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


                    string pdfPath = Server.MapPath("~/ConvertPDF/" + "JCPartsRep" + sInvno + ".pdf");
                    FileInfo file = new FileInfo(pdfPath);
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                    var pd = new PrintDocument();


                    orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
                    objDiskOpt.DiskFileName = pdfPath;

                    orp.ExportOptions.DestinationOptions = objDiskOpt;
                    orp.Export();

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;

            }
        }
  
    }
}
