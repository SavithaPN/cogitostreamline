﻿//Name            : Unit Controller Class  
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
//3. unit duplicatechecking for checking the duplicate entry of the  unit name
//Author          :   Shantha
//Date            :   4/8/2015
//CRH Number      :   SL0003
//Modifications   :




using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class UnitController : CommonController
    {
        Unit oUnit = new Unit();

        public UnitController()
        {
            oDoaminObject = new Unit();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Unit";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oUnit.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Unit oUnit = oDoaminObject as Unit;
                oUnit.ID = decimal.Parse(pId);
                gen_Unit oAtualObject = oUnit.DAO as gen_Unit;
                //Create a anominious object here to break the circular reference

                var oUnitToDisplay = new
                {

                    Pk_Unit = oAtualObject.Pk_Unit,
                    UnitName = oAtualObject.UnitName,
                    UnitType = oAtualObject.UnitType,
                    Description=oAtualObject.Description
                };

                return Json(new { success = true, data = oUnitToDisplay });
            }
            else
            {
                var oUnitToDisplay = new Inv_MaterialCategory();
                return Json(new { success = true, data = oUnitToDisplay });
            }
        }



        [HttpPost]
        public JsonResult UnitDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sUnitName = oValues["UnitName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("UnitName", sUnitName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oUnitToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Unit> oUnitObjects = oUnitToDisplayObjects.Select(p => p).OfType<gen_Unit>().ToList();

                var oUnitToDisplay = oUnitObjects.Select(p => new
                {
                    Pk_Unit = p.Pk_Unit,
                    UnitName = p.UnitName
                }).ToList().OrderBy(s => s.UnitName);

                return Json(new { Success = true, Records = oUnitToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UnitListByFiter(string UnitName = "", string Pk_Unit = "", string UnitType = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Unit", Pk_Unit));
                oSearchParams.Add(new SearchParameter("UnitName", UnitName));
                oSearchParams.Add(new SearchParameter("UnitType", UnitType));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oUnitToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Unit> oUnitObjects = oUnitToDisplayObjects.Select(p => p).OfType<gen_Unit>().ToList();

                //Create a anominious object here to break the circular reference
                var oUnitToDisplay = oUnitObjects.Select(p => new
                {
                    Pk_Unit = p.Pk_Unit,
                    UnitName = p.UnitName,
                    UnitType = p.UnitType

                }).ToList().OrderBy(s=>s.UnitName);

                return Json(new { Result = "OK", Records = oUnitToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

