﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;
using System.Data;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using WebGareCore.Controls;

namespace CogitoStreamline.Controllers
{

    public class StudController : CommonController
    {
        Stud oColor = new Stud();

        public StudController()
        {
            oDoaminObject = new Stud();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Inv_Material";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }
        public ActionResult Import()
        {
            return View();
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Stud oColor = oDoaminObject as Stud;
                oColor.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oColor.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                    Pk_Material = oAtualObject.Pk_Material,
                    //studentname = oAtualObject.studentname

                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new Inv_Material();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }



        [HttpPost]
        public JsonResult NameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sBranchName = oValues["studentname"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("studentname", sBranchName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    //studentname = p.studentname

                }).ToList();

                return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult StudentListByFiter(string studentname = "", string Pk_Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Material", Pk_Material));
                oSearchParams.Add(new SearchParameter("studentname", studentname));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {

                    Pk_Material = p.Pk_Material,
                    //studentname = p.studentname

                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

             [HttpPost]
        public ActionResult Importexcel()
        {


            if (Request.Files[" fileupload1"].ContentLength > 0)
            {
                string extension = System.IO.Path.GetExtension(Request.Files["FileUpload1"].FileName);
                string path1 = string.Format("{0}/{1}", Server.MapPath("~/uploads"), Request.Files["FileUpload1"].FileName);
                if (System.IO.File.Exists(path1))
                    System.IO.File.Delete(path1);

                Request.Files["FileUpload1"].SaveAs(path1);
                string sqlConnectionString = @"data Source=sql6005.site4now.net;Database=AstraPack;Trusted_Connection=true;Persist Security Info=True";


                //Create connection string to Excel work book
                string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                //Create Connection to Excel work book
                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                //Create OleDbCommand to fetch data from Excel
                OleDbCommand cmd = new OleDbCommand("Select [pk_paperstock],[fk_material],[quantity],[RollNo] from [ReelStock]", excelConnection);

                excelConnection.Open();
                OleDbDataReader dReader;
                dReader = cmd.ExecuteReader();

                SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnectionString);
                //Give your Destination table name
                sqlBulk.DestinationTableName = "Paper_Stock";
                sqlBulk.WriteToServer(dReader);
                excelConnection.Close();

                // SQL Server Connection String


            }

            return RedirectToAction("New");
        }

        //public ActionResult BrandRep(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

        //        //int sInvno = Convert.ToInt32(oValues["fkbox"]);

        //        List<Inv_Material> ColorList = new List<Inv_Material>();
        //        EyeSenseEntities dc = new EyeSenseEntities();

        //        //ColorList = dc.VwInvoices.ToList();
        //        ColorList = dc.Inv_Material.Select(x => x).OfType<Inv_Material>().ToList();

        //        if (ColorList.Count > 0)
        //        {
        //            DataTable dt = new DataTable();

        //            dt.Columns.Add("Pk_Material");
        //            dt.Columns.Add("studentname");




        //            foreach (Inv_Material entity in ColorList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["Pk_Material"] = entity.Pk_Material;
        //                row["studentname"] = entity.studentname;


        //                dt.Rows.Add(row);
        //            }


        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            Reports.BrandR orp = new Reports.BrandR();

        //            orp.Load("@\\Reports\\BrandR.rpt");
        //            orp.SetDataSource(dt.DefaultView);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "BrandList" + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;

        //    }
        //}



             public ActionResult Upload(string data)
             {

                 Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                 string sInvno = oValues["fkBranch"];
                 if (Request.ContentLength > 0)
                 {
                     string fName = System.IO.Path.GetFileName(sInvno);
                     string extension = System.IO.Path.GetExtension(sInvno);
                     string path1 = string.Format("{0}\\{1}", Server.MapPath("~\\uploads"), fName);
                     //if (System.IO.File.Exists(path1))
                     //    System.IO.File.Delete(path1);

                     //Request.Files["FileUpload1"].SaveAs(path1);
                     string sqlConnectionString = @"data Source=sql6005.site4now.net;Database=AstraPack;Trusted_Connection=true;Persist Security Info=True";


                     //Create connection string to Excel work book
                     string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                     //Create Connection to Excel work book
                     OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                     //Create OleDbCommand to fetch data from Excel
                     OleDbCommand cmd = new OleDbCommand("Select [Pk_PaperStock], [fk_material],[RollNo] ,[quantity]from [Sheet2$]", excelConnection);
                     //select Convert(nvarchar(50),GSM)+'GSM'+Convert(nvarchar(50),BF)+'BF' as SS from [Inv_Material]
                     
//select Convert(nvarchar(50),GSM)+'GSM' + Convert(nvarchar(50),BF)+'BF ' + MillName as SS from [Inv_Material],GEN_MILL
//WHERE INV_MATERIAL.FK_MILL=GEN_MILL.PK_MILL

                     excelConnection.Open();
                     OleDbDataReader dReader;
                     dReader = cmd.ExecuteReader();

                     SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnectionString);
                     //Give your Destination table name
                     sqlBulk.DestinationTableName = "PaperStock";
                     sqlBulk.WriteToServer(dReader);
                     excelConnection.Close();

                     // SQL Server Connection String


                 }

                 return RedirectToAction("New");
             }


             public ActionResult StockUpload(string data)
             {

                 Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                 string sInvno = oValues["fkBranch"];
                 if (Request.ContentLength > 0)
                 {
                     string fName = System.IO.Path.GetFileName(sInvno);
                     string extension = System.IO.Path.GetExtension(sInvno);
                     string path1 = string.Format("{0}\\{1}", Server.MapPath("~\\uploads"), fName);
                     //if (System.IO.File.Exists(path1))
                     //    System.IO.File.Delete(path1);

                     //Request.Files["FileUpload1"].SaveAs(path1);
                     string sqlConnectionString = @"data Source=sql6005.site4now.net;Database=AstraPack;Trusted_Connection=true;Persist Security Info=True";


                     //Create connection string to Excel work book
                     string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                     //Create Connection to Excel work book
                     OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                     //Create OleDbCommand to fetch data from Excel
                     OleDbCommand cmd = new OleDbCommand("Select [Pk_PaperStock], [Fk_Material],	[RollNo],	[Quantity] from [Sheet5$]", excelConnection);
                     //select Convert(nvarchar(50),GSM)+'GSM'+Convert(nvarchar(50),BF)+'BF' as SS from [Inv_Material]

                     //select Convert(nvarchar(50),GSM)+'GSM' + Convert(nvarchar(50),BF)+'BF ' + MillName as SS from [Inv_Material],GEN_MILL
                     //WHERE INV_MATERIAL.FK_MILL=GEN_MILL.PK_MILL

                     excelConnection.Open();
                     OleDbDataReader dReader;
                     dReader = cmd.ExecuteReader();

                     SqlBulkCopy sqlBulk = new SqlBulkCopy(sqlConnectionString);
                     //Give your Destination table name
                     sqlBulk.DestinationTableName = "PaperStock";
                     sqlBulk.WriteToServer(dReader);
                     excelConnection.Close();

                     // SQL Server Connection String


                 }

                 return RedirectToAction("New");
             }
    }
}
