﻿//Name            : Material Controller Class  (Common  )
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
//Author          :   Ravi
//Date            :   4/8/2015
//CRH Number      :   SL0005
//Modifications   :




using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class MaterialController : CommonController
    {
        //

        Material oMat=new Material();





        public MaterialController()
        {
            oDoaminObject = new Material();
          
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Material";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }



        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oMaterial = oDoaminObject as Material;
                oMaterial.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oMaterial.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = new
                {
                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name


                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new Inv_Material();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }


        //[HttpPost]
        //public JsonResult MaterialPending(string IndentNo = "", string VendorName = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        //{
        //   try
        //    {
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("IndentNo", IndentNo));
        //        oSearchParams.Add(new SearchParameter("VendorName", VendorName));
        //        oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
        //        //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
        //        //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
        //        //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

        //        //oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
        //        //oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<PendingTrack> oMaterialObjects = oMaterialsToDisplayObjects.Select(p => p).OfType<PendingTrack>().ToList();

        //        //Create a anominious object here to break the circular reference
                
        //        var oMaterialsToDisplay = oMaterialObjects.Where(p => p.Pending > 0 ).Select(p => new


        //        {
        //            Pk_Material = p.Pk_Material,
        //            Name = p.Name,
        //            MillName = p.Fk_Mill != null ? p.gen_Mill.MillName : "",
        //            GSM = p.GSM,
        //            BF = p.BF,
        //            Length = p.Length,
        //            Width = p.Width
        //        }).ToList();

        //        return Json(new { Result = "OK", Records = oMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Result = "ERROR", Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult MaterialListByFiter(string Name = "", string Fk_MaterialCategory = "", decimal Length = 0, decimal Width = 0, decimal GSM = 0, decimal BF = 0, string Fk_Mill = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("CatName", Fk_MaterialCategory));
                oSearchParams.Add(new SearchParameter("Fk_Mill", Fk_Mill));
                oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
                oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
                oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
                oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oMaterialObjects = oMaterialsToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialsToDisplay = oMaterialObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    MillName = p.Fk_Mill != null ? p.gen_Mill.MillName : "",
                    GSM = p.GSM,
                    BF = p.BF,
                    Length = p.Length,
                    Width = p.Width

                }).ToList();

                return Json(new { Result = "OK", Records = oMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult MaterialList(string IndentNo = "", string Name = "", string VendorName = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("IndentNo", IndentNo));
                oSearchParams.Add(new SearchParameter("VendorName", VendorName));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("MaterialName", MaterialName));
                //oSearchParams.Add(new SearchParameter("Length", Length.ToString()));
                //oSearchParams.Add(new SearchParameter("Width", Width.ToString()));
                //oSearchParams.Add(new SearchParameter("GSM", GSM.ToString()));
                //oSearchParams.Add(new SearchParameter("BF", BF.ToString()));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oMat.SearchIndent(oSearchParams);




                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Vw_Material> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<Vw_Material>().ToList();

                //Create a anominious object here to break the circular reference
                //var oVMaterialsToDisplay = oVMaterialObjects.Select(p => new

                var oVMaterialsToDisplay = oVMaterialObjects.Where(p => p.Pending > 0).Select(p => new
                {
                    Fk_Material = p.Fk_Material,
                    Name = p.Name,
                    VendorName = p.VendorName,
                    Quantity=p.Quantity,
                    Pk_MaterialOrderMasterId=p.Pk_MaterialOrderMasterId,
                    Pk_Vendor=p.Pk_Vendor,
                    Pending=p.Pending,
                    QC_Qty=p.QC_Qty
                    //BF = p.BF,
                    //Length = p.Length,
                    //Width = p.Width
                }).ToList();

                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

