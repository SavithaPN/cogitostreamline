﻿//Name          : ideActivityPermission Controller
//Description   : Contains the ideActivityPermission controller, every page in the application will be instance of this class with following
//                 1. Load Method(during Edit) 2. ListByFiltermethod----For displaying jTable  
//Author        : CH.V.N.Ravi Kumar
//Date 	        : 06/11/2015
//Crh Number    : 
//Modifications : 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
using WebGareCore.Controls;
using WebGareCore.CommonObjects.WorkFlow;

namespace CogitoStreamline.Controllers
{
    public class ActivityPermissionController : CommonController
    {
        //
        // GET: /ideActivityPermission/

        #region Variables
      
        #endregion Variables

        #region Constractor
        public ActivityPermissionController()
        {
            oDoaminObject = new ActivityPermissions();
        }
        #endregion Constractor

        #region Methods
        public override ActionResult Index()
        {
            ViewBag.Header = "Activity Permission";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            //oCustomer.Dispose();
           // oCommunication.Dispose();
            base.Dispose(disposing);
        }
       
       
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
               ActivityPermissions oideActivityPermission = oDoaminObject as ActivityPermissions;
                oideActivityPermission.ID = decimal.Parse(pId);
                CogitoStreamLineModel.ideActivityPermission oAtualObject = oideActivityPermission.DAO as CogitoStreamLineModel.ideActivityPermission;

                int i = -1;

                //get Documents
               


                //Create a anominious object here to break the circular reference
                var oideActivityPermissionToDisplay = new
                {
                    Pk_ActivityPermissions = oAtualObject.Pk_ActivityPermissions,
                  //  CommunicationType = oAtualObject.CommunicationType,
                  
                   
                };

                return Json(new { success = true, data = oideActivityPermissionToDisplay });
            }
            else
            {
                var oideActivityPermissionToDisplay = new CogitoStreamLineModel.ideActivityPermission();
                return Json(new { success = true, data = oideActivityPermissionToDisplay });
            }
        }
        [HttpPost]
        public JsonResult ideActivityPermissionListByFiter(string Pk_ActivityPermissions = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ActivityPermissions", Pk_ActivityPermissions));
             
            
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oideActivityPermissionsToDisplayObjects = oSearchResult.ListOfRecords;
                List<CogitoStreamLineModel.ideActivityPermission> oideActivityPermissionObjects = oideActivityPermissionsToDisplayObjects.Select(p => p).OfType<CogitoStreamLineModel.ideActivityPermission>().ToList();

                //Create a anominious object here to break the circular reference
                var oideActivityPermissionToDisplay = oideActivityPermissionObjects.Select(p => new
                {
                    Pk_ActivityPermissions = p.Pk_ActivityPermissions
                 
                   

                }).ToList();

                return Json(new { Result = "OK", Records = oideActivityPermissionToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        
        #endregion Methods
    }
}
