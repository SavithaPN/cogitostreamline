﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class FTypeController : CommonController
    {
        //
        // GET: /Mill/
        FType oMaterial = new FType();

        readonly CogitoStreamLineEntities _oEntites = new CogitoStreamLineEntities();

        public FTypeController()
        {
            oDoaminObject = new FType();
        }

        protected override void Dispose(bool disposing)
        {
      
           
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Flute Type";
            return base.Index();
        }





        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                FType oMaterial = oDoaminObject as FType;
                oMaterial.ID = decimal.Parse(pId);
                FluteType oAtualObject = oMaterial.DAO as FluteType;
                //Create a anominious object here to break the circular reference

                var oMaterialToDisplay = new
                {

                    Pk_FluteID = oAtualObject.Pk_FluteID,
                    FluteName = oAtualObject.FluteName,
                    TKFactor=oAtualObject.TKFactor
                   

                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new FluteType();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }


        [HttpPost]
        public JsonResult FTypeDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oBoardToDisplayObjects = oSearchResult.ListOfRecords;
                List<FluteType> oBoardObjects = oBoardToDisplayObjects.Select(p => p).OfType<FluteType>().ToList();

                var oBoardToDisplay = oBoardObjects.Select(p => new
                {
                    Pk_FluteID = p.Pk_FluteID,
                    FluteName = p.FluteName
                }).ToList().OrderBy(s => s.Pk_FluteID);

                return Json(new { Success = true, Records = oBoardToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult FluteListByFiter(string FluteID = "", string Name = "", string TakeUpFactor="",int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("FluteID", FluteID));
                oSearchParams.Add(new SearchParameter("TakeUpFactor", TakeUpFactor));

                //oSearchParams.Add(new SearchParameter("Category", "Cap"));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialToDisplayObjects = oSearchResult.ListOfRecords;
                List<FluteType> oMaterialObjects = oMaterialToDisplayObjects.Select(p => p).OfType<FluteType>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = oMaterialObjects.Select(p => new
                {
                    Pk_FluteID = p.Pk_FluteID,
                    FluteName = p.FluteName,
                    TKFactor=p.TKFactor
        


                }).ToList().OrderBy(s => s.Pk_FluteID);

                return Json(new { Result = "OK", Records = oMaterialToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult FindVal(string data)
        {
            decimal? CCode = 0;
            int i = 0;
            decimal TempVal;
            decimal WtBox;


            if (data != "{}")
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);

                decimal pId = Convert.ToDecimal(oValues["FTVal"]);
                TempVal = 0;
                WtBox = 0;


                int RCount = _oEntites.FluteType.Where(p => p.Pk_FluteID == pId).Count();
                //decimal? tempQty = 0;
                if (RCount > 0)
                {
                    FluteType oQC = _oEntites.FluteType.Where(p => p.Pk_FluteID == pId).Single();

                    CCode = oQC.TKFactor;
                }
                else
                { CCode = 0; }

              //  CogitoStreamLineEntities dc = new CogitoStreamLineEntities();
               // List<FluteType> oPrdListChild = new List<FluteType>();
               //// oPrdListChild = dc.FluteType.Where(p => p.TKFactor == pId).Select(p => p).OfType<FluteType>().ToList()
                 
               // //oPrdListChild = dc.FluteType.Where(p => p.TKFactor==pId).Select(p => p).OfType<FluteType>().Single();

                //int RCount = _oEntites.FluteType.Where(p => p.TKFactor == pId).Count();
                //if (RCount > 0)
                //{

                //    //Vw_BoxSpec oQC = _oEntites.Vw_BoxSpec.Where(p => p.Pk_BoxID == pId).Single();

                //    for (i = 0; i < RCount; i++)
                //    {
                //        TempVal = Convert.ToDecimal(oPrdListChild.ElementAt(i).Weight);

                //        WtBox = WtBox + TempVal;
                //    }

                //    CCode = WtBox;
                //}

                //else
                //{ CCode = 0; }

            }
            return Json(CCode);
        }
    
    }
}
