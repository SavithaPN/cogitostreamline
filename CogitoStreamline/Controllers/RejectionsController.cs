﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class RejectionsController : CommonController
    {
        Rejection oRej = new Rejection();
        public RejectionsController()
        {
            oDoaminObject = new Rejection();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Goods Returns";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oRej.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Rejection oRejection = oDoaminObject as Rejection;
                oRejection.ID = decimal.Parse(pId);
                CustomerRejection oAtualObject = oRejection.DAO as CustomerRejection;
                //Create a anominious object here to break the circular reference

                var oRejToDisplay = new
                {
                    Pk_RejectionId = oAtualObject.Pk_RejectionId,
                    RejectionDate = DateTime.Parse(oAtualObject.RejectionDate.ToString()).ToString("dd/MM/yyyy"),
                    //Paymt_Date = oAtualObject.Paymt_Date,
                    Fk_OrderNo = oAtualObject.Fk_OrderNo,
                    Fk_Customer = oAtualObject.Fk_Customer,
                  
                    Rejected_Quantity = oAtualObject.Rejected_Quantity,
                    Reason = oAtualObject.Reason
                   

                };

                return Json(new { success = true, data = oRejToDisplay });
            }
            else
            {
                var oRejToDisplay = new CustomerRejection();
                return Json(new { success = true, data = oRejToDisplay });
            }
        }

        //[HttpPost]
        //public JsonResult StateDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sStateName = oValues["StateName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("StateName", sStateName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oRejToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_State> oRejObjects = oRejToDisplayObjects.Select(p => p).OfType<gen_State>().ToList();

        //        var oStateToDisplay = oRejObjects.Select(p => new
        //        {
        //            Pk_StateID = p.Pk_StateID,
        //            StateName = p.StateName,
        //            Country = p.Country

        //        }).ToList().OrderBy(s => s.StateName);

        //        return Json(new { Success = true, Records = oStateToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}


        //[HttpPost]
        //public JsonResult getCountry(string pId = "")
        //{
        //    List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

        //    var oCountryToDisplay = oListOfCountry.Select(p => new
        //    {
        //        Name = p.CountryName,
        //        Id = p.Pk_Country
        //    });

        //    return Json(oCountryToDisplay);
        //}

        [HttpPost]
        public JsonResult RejectionListByFiter(string Pk_RejectionId = "", string RejectionDate = "", string Fk_OrderNo = "", string Fk_Customer = "", string Rejected_Quantity = "", string Reason = "",  int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_RejectionId", Pk_RejectionId));
                oSearchParams.Add(new SearchParameter("RejectionDate", RejectionDate));
                oSearchParams.Add(new SearchParameter("Fk_OrderNo", Fk_OrderNo));
                oSearchParams.Add(new SearchParameter("Fk_Customer", Fk_Customer));
                oSearchParams.Add(new SearchParameter("Rejected_Quantity", Rejected_Quantity));
                oSearchParams.Add(new SearchParameter("Reason", Reason));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oRejToDisplayObjects = oSearchResult.ListOfRecords;
                List<CustomerRejection> oRejObjects = oRejToDisplayObjects.Select(p => p).OfType<CustomerRejection>().ToList();

                //Create a anominious object here to break the circular reference
                var oRejToDisplay = oRejObjects.Select(p => new
                {
                    Pk_RejectionId = p.Pk_RejectionId,
                    RejectionDate = DateTime.Parse(p.RejectionDate.ToString()).ToString("dd/MM/yyyy"),
                    //Paymt_Date = oAtualObject.Paymt_Date,
                    Fk_OrderNo = p.Fk_OrderNo,
                    //Fk_Customer = p.Fk_Customer,
                    Fk_Customer = p.gen_Customer.CustomerName,
                    Rejected_Quantity = p.Rejected_Quantity,
                    Reason = p.Reason



                }).ToList().OrderByDescending(p => p.Pk_RejectionId);

                return Json(new { Result = "OK", Records = oRejToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        //[HttpPost]
        //public JsonResult StateListByCountry(string data = "")
        //{
        //    try
        //    {
        //        State oState = new State();
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sParent = oValues["CountryId"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("CountryId", sParent));

        //        SearchResult oSearchResult = oState.Search(oSearchParams);

        //        List<EntityObject> oStatesToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_State> oRejObjects = oStatesToDisplayObjects.Select(p => p).OfType<gen_State>().ToList();

        //        var oStatesToDisplay = oRejObjects.Select(p => new
        //        {
        //            Pk_StateID = p.Pk_StateID,
        //            StateName = p.StateName


        //        }).ToList();

        //        return Json(new { Success = true, Records = oStatesToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}

