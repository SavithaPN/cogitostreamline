﻿//using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class JWRecdController : CommonController
    {
        Mat_Category oMatCat = new Mat_Category();
        Country oCountry = new Country();
        public JWRecdController()
        {
            oDoaminObject = new JWReceived();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Job Work Out - Product Inward";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oMatCat.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                JWReceived oCity = oDoaminObject as JWReceived;
                oCity.ID = decimal.Parse(pId);
                JobWorkReceivables oAtualObject = oCity.DAO as JobWorkReceivables;
                //Create a anominious object here to break the circular reference

                var oCityToDisplay = new
                {
                    Pk_ID = oAtualObject.Pk_ID,
                    //CityName = oAtualObject.CityName,
                    //Fk_StateID = oAtualObject.Fk_StateID,
                    //Country = oAtualObject.Country

                };

                return Json(new { success = true, data = oCityToDisplay });
            }
            else
            {
                var oCityToDisplay = new JobWorkReceivables();
                return Json(new { success = true, data = oCityToDisplay });
            }
        }

        //[HttpPost]
        //public JsonResult CityDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sCountry = oValues["Country"];
        //        string sFk_StateID = oValues["Fk_StateID"];
        //        string sCityName = oValues["CityName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Country", sCountry));
        //        oSearchParams.Add(new SearchParameter("StateId", sFk_StateID));
        //        oSearchParams.Add(new SearchParameter("CityName", sCityName));


        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oCityToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<JobWorkReceivables> oCityObjects = oCityToDisplayObjects.Select(p => p).OfType<JobWorkReceivables>().ToList();

        //        var oCityToDisplay = oCityObjects.Select(p => new
        //        {

        //            Pk_ID = p.Pk_ID,
        //            CityName = p.CityName,
        //            Fk_StateID = p.Fk_StateID


        //        }).ToList().OrderBy(s => s.CityName);

        //        return Json(new { Success = true, Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult getMatCategory(string pId = "")
        {
            List<Inv_MaterialCategory> oListOfState = oMatCat.Search(null).ListOfRecords.OfType<Inv_MaterialCategory>().ToList();

            var oStateToDisplay = oListOfState.Where(p=>p.Pk_MaterialCategory>4 && p.Pk_MaterialCategory<18).Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_MaterialCategory
            });

            return Json(oStateToDisplay);
        }

        [HttpPost]
        public JsonResult getCountry(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }

        [HttpPost]
        public JsonResult JobsListByFiter(string JCNo = "", string FromDate = "", string ToDate = "", string Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("JCNo", JCNo));
                oSearchParams.Add(new SearchParameter("Vendor", Vendor));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCityToDisplayObjects = oSearchResult.ListOfRecords;
                List<JobWorkReceivables> oCityObjects = oCityToDisplayObjects.Select(p => p).OfType<JobWorkReceivables>().ToList();

                //Create a anominious object here to break the circular reference
                var oCityToDisplay = oCityObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,

                   DCNo=p.DcNo,
                    Fk_JobCardID=p.Fk_JobCardID,
                    RecdDate = DateTime.Parse(p.RecdDate.ToString()).ToString("dd/MM/yyyy"),
                    PrdName = p.Name,
                    Vendor =p.JW_JobCardMaster.gen_Vendor.VendorName,
                   //Name=p.Name,
                    Qty=p.RecdPrdWeight!=null?p.RecdPrdWeight:0,
                  
                   //p.PODate != null ? DateTime.Parse(p.PODate.ToString()).ToString("dd/MM/yyyy") : "",
                    
                    
                    TypeofPrd = p.TypeofPrd == "1" ?"Finished"  : "Semi-Finished",


                  

                }).ToList().OrderByDescending(s => s.Pk_ID);

                return Json(new { Result = "OK", Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //[HttpPost]
        //public JsonResult CityListByState(string data = "")
        //{
        //    try
        //    {
        //        JWReceived oCity = new JWReceived();
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sStateId = oValues["StateId"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("StateId", sStateId));

        //        SearchResult oSearchResult = oCity.Search(oSearchParams);

        //        List<EntityObject> oCitysToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<JobWorkReceivables> oCityObjects = oCitysToDisplayObjects.Select(p => p).OfType<JobWorkReceivables>().ToList();

        //        var oCityToDisplay = oCityObjects.Select(p => new
        //        {
        //            Pk_ID = p.Pk_ID,
        //            CityName = p.CityName
        //        }).ToList();

        //        return Json(new { Success = true, Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]

        public JsonResult SaveNew([System.Web.Http.FromBody]string data)
        {
            data = data.TrimStart('=');
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            data = JsonConvert.SerializeObject(values);
            return base.Save(data);


        }
    }
}

