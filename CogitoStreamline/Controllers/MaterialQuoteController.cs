﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamline.Controllers;

namespace ManufacturingERP.Controllers
{
    public class MaterialQuoteController : CommonController
    {
        //
        // GET: /MaterialIndent/
        Vendor oVendor = new Vendor();
        

        public MaterialQuoteController()
        {
            oDoaminObject = new MaterialQuote();
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Material Quatation";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                MaterialQuote oMaterialQuote = oDoaminObject as MaterialQuote;
                oMaterialQuote.ID = decimal.Parse(pId);
                MaterialQuotationMaster oAtualObject = oMaterialQuote.DAO as MaterialQuotationMaster;

                int i = -1;

                var oMaterialQuoteDetails = oAtualObject.MaterialQuotationDetails.Select(p => new
                {
                    Pk_QuotationMaterial = p.Pk_QuotationMaterial,
                    slno = ++i,
                    Quantity = p.Quantity,

                    //RequestDate = p.RequestDate != null ? DateTime.Parse(p.RequestDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_Material = p.Fk_Material,
                    txtFk_Material = p.Inv_Material.Name,
                    SpecialInformation = p.SpecialInformation,
                    //Fk_User = oAtualObject.CreatedBy
                });


                //Create a anominious object here to break the circular reference
                var oMaterialQuoteToDisplay = new
                {
                    Pk_QuotationRequest = oAtualObject.Pk_QuotationRequest,
                    RequestDate = DateTime.Parse(oAtualObject.RequestDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Vendor = oAtualObject.Fk_Vendor,
                    CreatedBy = oAtualObject.CreatedBy,
                    Requests = oAtualObject.Requests,
                    MaterialDetails = Json(oMaterialQuoteDetails).Data
                    //Comments = oAtualObject.Comments,
                    //MaterialIndentDetails = Json(oMaterialQuoteDetails).Data
                    //Fk_VendorId,
                };

                return Json(new { success = true, data = oMaterialQuoteToDisplay });
            }
            else
            {
                var oMaterialQuoteToDisplay = new MaterialQuotationMaster();
                return Json(new { success = true, data = oMaterialQuoteToDisplay });
            }
        }

        [HttpPost]
        public JsonResult MaterialQuoteListByFiter(string Pk_QuotationRequest = null, string FromRequestDate = "", string ToRequestDate = "", string Fk_Vendor = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();

                oSearchParams.Add(new SearchParameter("Pk_QuotationRequest", Pk_QuotationRequest));
               
                oSearchParams.Add(new SearchParameter("FromRequestDate", FromRequestDate));
                oSearchParams.Add(new SearchParameter("ToRequestDate", ToRequestDate));
                oSearchParams.Add(new SearchParameter("Fk_Vendor", Fk_Vendor));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialQuotesToDisplayObjects = oSearchResult.ListOfRecords;
                List<MaterialQuotationMaster> oMaterialQuoteObjects = oMaterialQuotesToDisplayObjects.Select(p => p).OfType<MaterialQuotationMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialQuotesToDisplay = oMaterialQuoteObjects.Select(p => new
                {

                    Pk_QuotationRequest = p.Pk_QuotationRequest,
                    RequestDate = DateTime.Parse(p.RequestDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_Vendor = p.gen_Vendor.VendorName,
                    CreatedBy = p.CreatedBy,
                    Requests = p.Requests
                }).ToList();

                return Json(new { Result = "OK", Records = oMaterialQuotesToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}
