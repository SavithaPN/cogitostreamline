﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class STransferController : CommonController
    {
        Brnch oBranch = new Brnch();
        STransfer oSTransfer = new STransfer();
       // Country oCountry = new Country();
        public STransferController()
        {
            oDoaminObject = new STransfer();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Stock Transfer";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oBranch.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                STransfer oTransfer = oDoaminObject as STransfer;
                oTransfer.ID = decimal.Parse(pId);
                StockTransfer oAtualObject = oTransfer.DAO as StockTransfer;
                //Create a anominious object here to break the circular reference
                var TanentID = Convert.ToInt32(Session["Tanent"]);
              
                decimal dPendingValue = oTransfer.PendingValue(decimal.Parse(oAtualObject.Fk_FromBranchID.ToString()), decimal.Parse(TanentID.ToString()), decimal.Parse(oAtualObject.Fk_Material.ToString()));
                dPendingValue = dPendingValue + decimal.Parse(oAtualObject.TransferQty.ToString());

                var oTransferToDisplay = new
                {
                    Pk_TransferID = oAtualObject.Pk_TransferID,
                    TransferDate = DateTime.Parse(oAtualObject.TransferDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_FromBranchID = oAtualObject.Fk_FromBranchID,
                    Fk_ToBranchID = oAtualObject.Fk_ToBranchID,
                    TransferQty = oAtualObject.TransferQty,
                    Fk_Material = oAtualObject.Fk_Material,
                    TxtQuantity = dPendingValue
                };

                return Json(new { success = true, data = oTransferToDisplay });
            }
            else
            {
                var oTransferToDisplay = new StockTransfer();
                return Json(new { success = true, data = oTransferToDisplay });
            }
        }

 

        [HttpPost]
        public JsonResult getFromBranch(string pId = "")
        {
            List<Branch> oListOfState = oBranch.Search(null).ListOfRecords.OfType<Branch>().ToList();
            var Fk_Tanent = Convert.ToInt32(Session["Tanent"]);
            var oTransferToDisplay = oListOfState.Where(s=>s.Fk_Tanent==Fk_Tanent).Select(p => new
            {
                Name = p.BranchName,
                Id = p.Pk_BranchID
            });

            return Json(oTransferToDisplay);
        }


        [HttpPost]
        public JsonResult getToBranch(string pId = "")
        {
            List<Branch> oListOfState = oBranch.Search(null).ListOfRecords.OfType<Branch>().ToList();
            var Fk_Tanent = Convert.ToInt32(Session["Tanent"]);
            var oTransferToDisplay = oListOfState.Where(s => s.Fk_Tanent == Fk_Tanent).Select(p => new
            {
                Name = p.BranchName,
                Id = p.Pk_BranchID
            });

            return Json(oTransferToDisplay);
        }

        [HttpPost]
        public JsonResult TransferListByFiter(string Pk_TransferID = "", string TransferDate = "", string Fk_FromBranchID = "", string Fk_ToBranchID = "", string TransferQty = "", string Fk_Material = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_TransferID", Pk_TransferID));
                oSearchParams.Add(new SearchParameter("TransferDate", TransferDate));
                oSearchParams.Add(new SearchParameter("Fk_FromBranchID", Fk_FromBranchID));
                oSearchParams.Add(new SearchParameter("Fk_ToBranchID", Fk_ToBranchID));
                oSearchParams.Add(new SearchParameter("TransferQty", TransferQty));
                oSearchParams.Add(new SearchParameter("Fk_Material", Fk_Material));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oTransferToDisplayObjects = oSearchResult.ListOfRecords;
                List<StockTransfer> oTransferObjects = oTransferToDisplayObjects.Select(p => p).OfType<StockTransfer>().ToList();

                //Create a anominious object here to break the circular reference
                var oTransferToDisplay = oTransferObjects.Select(p => new
                {
                    Pk_TransferID = p.Pk_TransferID,
                    TransferDate = DateTime.Parse(p.TransferDate.ToString()).ToString("dd/MM/yyyy"),
                    Fk_FromBranchID = p.Branch.BranchName,
                    Fk_ToBranchID = p.Branch1.BranchName,
                    TransferQty = p.TransferQty,
                    //Fk_Material = p.Inv_Material.Name

                }).ToList().OrderBy(s => s.Pk_TransferID);

                return Json(new { Result = "OK", Records = oTransferToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult TransferList(string DDate = "", string Invno = "", string MaterialName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("DDate", DDate));
                oSearchParams.Add(new SearchParameter("Invoice", Invno));
                oSearchParams.Add(new SearchParameter("Name", MaterialName));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oSTransfer.SearchST(oSearchParams);


                List<EntityObject> oVMaterialsToDisplayObjects = oSearchResult.ListOfRecords;
                List<vw_StockLedger> oVMaterialObjects = oVMaterialsToDisplayObjects.Select(p => p).OfType<vw_StockLedger>().ToList();


                var oVMaterialsToDisplay = oVMaterialObjects.Where(p => p.StockQty > 0 ).Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    StockQty = p.StockQty


                }).ToList();

                return Json(new { Result = "OK", Records = oVMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult StockListByBranch(string data = "")
        {
            try
            {
                STransfer oTransfer = new STransfer();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sBranchId = oValues["BranchId"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("StateId", sBranchId));

                SearchResult oSearchResult = oTransfer.Search(oSearchParams);

                List<EntityObject> oTransfersToDisplayObjects = oSearchResult.ListOfRecords;
                List<StockTransfer> oTransferObjects = oTransfersToDisplayObjects.Select(p => p).OfType<StockTransfer>().ToList();

                var oTransferToDisplay = oTransferObjects.Select(p => new
                {
                    Pk_TransferID = p.Pk_TransferID,
                    //BranchName = p.BranchName
                }).ToList();

                return Json(new { Success = true, Records = oTransferToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
    }
}

