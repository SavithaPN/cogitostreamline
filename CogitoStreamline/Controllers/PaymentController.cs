﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class PaymentController : CommonController
    {
        PayTrack oPay = new PayTrack();
        public PaymentController()
        {
            oDoaminObject = new PayTrack();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Payment Track";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oPay.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                PayTrack oPayment = oDoaminObject as PayTrack;
                oPayment.ID = decimal.Parse(pId);
                PaymentTrack oAtualObject = oPayment.DAO as PaymentTrack;
                //Create a anominious object here to break the circular reference

                var oPayToDisplay = new
                {
                    Pk_PaymtID = oAtualObject.Pk_PaymtID,

                    Paymt_Date = oAtualObject.Paymt_Date != null ? DateTime.Parse(oAtualObject.Paymt_Date.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_Invno = oAtualObject.Fk_Invno,
                    ChequeNo = oAtualObject.ChequeNo,
                    BankName = oAtualObject.BankName,
                    TransactionID = oAtualObject.TransactionID,
                    PaidAmt = oAtualObject.PaidAmt,
                    BalanceAmt = oAtualObject.BalanceAmt,
                    BillAmt = oAtualObject.BillAmt

                };

                return Json(new { success = true, data = oPayToDisplay });
            }
            else
            {
                var oPayToDisplay = new PaymentTrack();
                return Json(new { success = true, data = oPayToDisplay });
            }
        }

       
        [HttpPost]
        public JsonResult PaymentListByFiter(string Pk_PaymtID = "", string Paymt_Date = "", string Fk_Invno = "", string ChequeNo = "", string BankName = "", string TransactionID = "", string PaidAmt = "", string BalanceAmt = "", string BillAmt = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_PaymtID", Pk_PaymtID));
                oSearchParams.Add(new SearchParameter("Paymt_Date", Paymt_Date));
                oSearchParams.Add(new SearchParameter("Fk_Invno", Fk_Invno));
                oSearchParams.Add(new SearchParameter("ChequeNo", ChequeNo));
                oSearchParams.Add(new SearchParameter("BankName", BankName));
                oSearchParams.Add(new SearchParameter("TransactionID", TransactionID));
                oSearchParams.Add(new SearchParameter("PaidAmt", PaidAmt));
                oSearchParams.Add(new SearchParameter("BalanceAmt", BalanceAmt));
                oSearchParams.Add(new SearchParameter("BillAmt", BillAmt));
              
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPayToDisplayObjects = oSearchResult.ListOfRecords;
                List<PaymentTrack> oPayObjects = oPayToDisplayObjects.Select(p => p).OfType<PaymentTrack>().ToList();

                //Create a anominious object here to break the circular reference
                var oPayToDisplay = oPayObjects.Select(p => new
                {
                    Pk_PaymtID = p.Pk_PaymtID,
                    Paymt_Date = p.Paymt_Date != null ? DateTime.Parse(p.Paymt_Date.ToString()).ToString("dd/MM/yyyy") : "",
                    Fk_Invno = p.Fk_Invno,
                    ChequeNo = p.ChequeNo,
                    BankName = p.BankName,
                    TransactionID = p.TransactionID,
                    PaidAmt = p.PaidAmt,
                    BalanceAmt = p.BalanceAmt,
                    BillAmt = p.BillAmt

                }).ToList();

                return Json(new { Result = "OK", Records = oPayToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

