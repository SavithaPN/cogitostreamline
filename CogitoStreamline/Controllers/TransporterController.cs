﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.IO;


namespace CogitoStreamline.Controllers
{

    public class TransporterController : CommonController
    {
        Transporter oColor = new Transporter();

        public TransporterController()
        {
            oDoaminObject = new Transporter();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Transporter";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Transporter oColor = oDoaminObject as Transporter;
                oColor.ID = decimal.Parse(pId);
                TransporterM oAtualObject = oColor.DAO as TransporterM;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                    Pk_ID = oAtualObject.Pk_ID,
                    TransporterName = oAtualObject.TransporterName,                      
                    Address1=oAtualObject.Address1,
                     MobileNo=oAtualObject.MobileNo,
                    LLine=oAtualObject.LLine,
                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new TransporterM();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }



        [HttpPost]
        public JsonResult TNameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sColorName = oValues["TransporterName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("TransporterName", sColorName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<TransporterM> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<TransporterM>().ToList();

                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    TransporterName = p.TransporterName

                }).ToList().OrderBy(s => s.TransporterName);

                return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult TransporterListByFiter(string TransporterName = "", string Pk_ID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ID", Pk_ID));
                oSearchParams.Add(new SearchParameter("TransporterName", TransporterName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<TransporterM> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<TransporterM>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_ID = p.Pk_ID,
                    TransporterName = p.TransporterName,
                    Address1=p.Address1,
                     MobileNo=p.MobileNo,
                    LLine=p.LLine,


                }).ToList();

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
