﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CogitoStreamline.Controllers
{
    public class HelloJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine("HelloJob is executing.");
        }
    }
}