﻿//Name            : Employee Controller Class  (one of the material )
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
// 3. duplicatechecking for checking the duplicate entry of the  name
//Author          :   CH.V.N.Ravi Kumar
//Date            :   21/10/2015
//CRH Number      :   
//Modifications   :

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using System.Data;
using System.IO;
using System.Drawing.Printing;
using WebGareCore.Controls;


namespace PowerEquations.Controllers
{
    public class EmployeeController : CommonController
    {
        //
        // GET: /Employee/
        public WgFileUpload oFileUpload = new WgFileUpload("documents", "EmployeeImages");
        Employee oEmployee = new Employee();
        State oState = new State();
        City oCity = new City();
        EmployeeDesignation oEmployeeDesignation = new EmployeeDesignation();
        Department oDepartment = new Department();

        public EmployeeController()
        {
            oDoaminObject = new Employee();
        }


        public override ActionResult Index()
        {
            ViewBag.Header = "Employee";
            return base.Index();
        }
        [HttpPost]
        public JsonResult ImageUpload()
        {
            return Json(oFileUpload.SaveFile(Request.Files));
        }
        protected override void Dispose(bool disposing)
        {
            oState.Dispose();
            oCity.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult getEmployeeDesignation(string pId = "")
        {
            List<emp_EmployeeDesignation> oListOfEmployeeDesignationRelationShips = oEmployeeDesignation.Search(null).ListOfRecords.OfType<emp_EmployeeDesignation>().ToList();
            var oEmployeeDesignationToDisplay = oListOfEmployeeDesignationRelationShips.Select(p => new
            {
                Name = p.DesignationName,
                Id = p.Pk_EmployeeDesignationId
            });

            return Json(oEmployeeDesignationToDisplay);
        }

        [HttpPost]
        public JsonResult getEmployeeDepartment(string pId = "")
        {
            List<emp_Department> oListOfEmployeeDepartmentRelationShips = oDepartment.Search(null).ListOfRecords.OfType<emp_Department>().ToList();
            var oEmployeeDepartmentToDisplay = oListOfEmployeeDepartmentRelationShips.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_DepartmentId
            });

            return Json(oEmployeeDepartmentToDisplay);
        }

        [HttpPost]
        public JsonResult getState(string pId = "")
        {
            List<gen_State> oListOfState = oState.Search(null).ListOfRecords.OfType<gen_State>().ToList();
            var ostateToDisplay = oListOfState.Select(p => new
            {
                Name = p.StateName,
                Id = p.Pk_StateID
            });

            return Json(ostateToDisplay);
        }

        [HttpPost]
        public JsonResult getCity(string pId = "")
        {
            List<gen_City> oListOfCity = oCity.Search(null).ListOfRecords.OfType<gen_City>().ToList();
            var oCityToDisplay = oListOfCity.Select(p => new
            {
                Name = p.CityName,
                Id = p.Pk_CityID
            });

            return Json(oCityToDisplay);
        }

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Employee oEmployee = oDoaminObject as Employee;
                oEmployee.ID = decimal.Parse(pId);
                emp_EmployeeMaster oAtualObject = oEmployee.DAO as emp_EmployeeMaster;

                var oImages = oAtualObject.emp_EmployeeImage.Select(p => new
                {
                    FileName = p.ImageName,
                    FileSize = p.ImageSize
                });

                //Create a anominious object here to break the circular reference
                var oEmployeeToDisplay = new
                {
                    Pk_EmployeeId = oAtualObject.Pk_EmployeeId,
                    FirstName = oAtualObject.FirstName,
                    LastName = oAtualObject.LastName,
                    FatherName = oAtualObject.FatherName,
                    MotherName = oAtualObject.MotherName,
                    DOB = DateTime.Parse(oAtualObject.DOB.ToString()).ToString("dd/MM/yyyy"),
                    Address=oAtualObject.Address,
                    Fk_City = oAtualObject.Fk_City,
                    Fk_State = oAtualObject.Fk_State,
                    Mobile=oAtualObject.Mobile,
                    Phone=oAtualObject.Phone,
                    Email=oAtualObject.Email,
                    Pincode=oAtualObject.Pincode,
                    Fk_EmployeeDesignation = oAtualObject.Fk_EmployeeDesignation,
                    Fk_DepartmentId=oAtualObject.Fk_DepartmentId,
                   IsActive=oAtualObject.IsActive,
                   Documents = Json(oImages).Data
                };

                return Json(new { success = true, data = oEmployeeToDisplay });
            }
            else
            {
                var oEmployeeToDisplay = new emp_EmployeeMaster();
                return Json(new { success = true, data = oEmployeeToDisplay });
            }
        }

        [HttpPost]
        public JsonResult EmployeeListByFiter(string Pk_EmployeeId=null, string FirstName = null,string LastName=null,string Mobile=null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_EmployeeId", Pk_EmployeeId));
                oSearchParams.Add(new SearchParameter("FirstName", FirstName));
                oSearchParams.Add(new SearchParameter("LastName", LastName));
                oSearchParams.Add(new SearchParameter("Mobile", Mobile));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oEmployeesToDisplayObjects = oSearchResult.ListOfRecords;
                List<emp_EmployeeMaster> oEmployeeObjects = oEmployeesToDisplayObjects.Select(p => p).OfType<emp_EmployeeMaster>().ToList();

                //Create a anominious object here to break the circular reference
                var oEmployeesToDisplay = oEmployeeObjects.Select(p => new
                {
                    Pk_EmployeeId = p.Pk_EmployeeId,
                    FirstName = p.FirstName,
                    LastName=p.LastName,
                   FatherName=p.FatherName,
                   City=p.gen_City.CityName,
                   DesignationName =p.emp_EmployeeDesignation.DesignationName,
                   Address=p.Address,
                   DepartmentName=p.emp_Department.Name,
                   State=p.gen_State.StateName,
                   Mobile=p.Mobile,
                    Email = p.Email
                }).ToList().OrderBy(s => s.FirstName);

                return Json(new { Result = "OK", Records = oEmployeesToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

       
    }
}

