﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class RecptController : CommonController
    {
        Customer oCust = new Customer();
        //Country oCountry = new Country();
        public RecptController()
        {
            oDoaminObject = new Recpt();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Receipt";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oCust.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {

            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Recpt oRec = oDoaminObject as Recpt;
                oRec.ID = decimal.Parse(pId);
                Receipt oAtualObject = oRec.DAO as Receipt;
                //Create a anominious object here to break the circular reference

                var oRecToDisplay = new
                {
                    Pk_ReceiptID = oAtualObject.Pk_ReceiptID,
                    Fk_CustomerId = oAtualObject.Fk_Customer,
                    Amount = oAtualObject.Amount,
                    ChequeNo = oAtualObject.ChequeNo,
                    //ChequeDate = oAtualObject.ChequeDate,
                    ChequeDate = oAtualObject.ChequeDate != null ? DateTime.Parse(oAtualObject.ChequeDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Description = oAtualObject.Description,
                    BankName = oAtualObject.BankName


                };

                return Json(new { success = true, data = oRecToDisplay });
            }
            else
            {
                var oRecToDisplay = new Receipt();
                return Json(new { success = true, data = oRecToDisplay });
            }
        }

        //[HttpPost]
        //public JsonResult CityDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sCountry = oValues["Country"];
        //        string sFk_StateID = oValues["Fk_StateID"];
        //        string sCityName = oValues["CityName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Country", sCountry));
        //        oSearchParams.Add(new SearchParameter("StateId", sFk_StateID));
        //        oSearchParams.Add(new SearchParameter("CityName", sCityName));


        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oRecToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<gen_City> oRecObjects = oRecToDisplayObjects.Select(p => p).OfType<gen_City>().ToList();

        //        var oRecToDisplay = oRecObjects.Select(p => new
        //        {

        //            Pk_CityID = p.Pk_CityID,
        //            CityName = p.CityName,
        //            Fk_StateID = p.Fk_StateID


        //        }).ToList().OrderBy(s => s.CityName);

        //        return Json(new { Success = true, Records = oRecToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult getCustomer(string pId = "")
        {
            List<gen_Customer> oListOfCust = oCust.Search(null).ListOfRecords.OfType<gen_Customer>().ToList();

            var oRecToDisplay = oListOfCust.Select(p => new
            {
                Name = p.CustomerName,
                Id = p.Pk_Customer
            });

            return Json(oRecToDisplay);
        }

        //[HttpPost]
        //public JsonResult getCountry(string pId = "")
        //{
        //    List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

        //    var oCountryToDisplay = oListOfCountry.Select(p => new
        //    {
        //        Name = p.CountryName,
        //        Id = p.Pk_Country
        //    });

        //    return Json(oCountryToDisplay);
        //}

        [HttpPost]
        public JsonResult ReceiptListByFiter(string Pk_ReceiptID = "", string Fk_Customer = "", string Amount = "", string ChequeNo = "", string ChequeDate = "", string Description = "", string BankName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_ReceiptID", Pk_ReceiptID));
                oSearchParams.Add(new SearchParameter("Fk_Customer", Fk_Customer));
                oSearchParams.Add(new SearchParameter("Amount", Amount));
                oSearchParams.Add(new SearchParameter("ChequeNo", ChequeNo));
                oSearchParams.Add(new SearchParameter("ChequeDate", ChequeDate));
                oSearchParams.Add(new SearchParameter("Description", Description));
                oSearchParams.Add(new SearchParameter("BankName", BankName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oRecToDisplayObjects = oSearchResult.ListOfRecords;
                List<Receipt> oRecObjects = oRecToDisplayObjects.Select(p => p).OfType<Receipt>().ToList();

                //Create a anominious object here to break the circular reference
                var oRecToDisplay = oRecObjects.Select(p => new
                {
                    Pk_ReceiptID = p.Pk_ReceiptID,
                    Fk_Customer = p.gen_Customer.CustomerName,
                    Amount = p.Amount,
                    ChequeNo = p.ChequeNo,
                    // ChequeDate = p.ChequeDate,
                    ChequeDate = p.ChequeDate != null ? DateTime.Parse(p.ChequeDate.ToString()).ToString("dd/MM/yyyy") : "",
                    Description = p.Description,
                    BankName = p.BankName
                }).ToList().OrderBy(s => s.Pk_ReceiptID);

                return Json(new { Result = "OK", Records = oRecToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ReceiptListByCustomer(string data = "")
        {
            try
            {
                Recpt oRec = new Recpt();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sFk_Customer = oValues["Fk_Customer"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Fk_Customer", sFk_Customer));

                SearchResult oSearchResult = oRec.Search(oSearchParams);

                List<EntityObject> oRecsToDisplayObjects = oSearchResult.ListOfRecords;
                List<Receipt> oRecObjects = oRecsToDisplayObjects.Select(p => p).OfType<Receipt>().ToList();

                var oRecToDisplay = oRecObjects.Select(p => new
                {
                    Pk_ReceiptID = p.Pk_ReceiptID,
                    Amount = p.Amount
                }).ToList();

                return Json(new { Success = true, Records = oRecToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
    }
}

