﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class SampleSampleSampleItemPartPropertyController : Controller
//    {
//        //
//        // GET: /SampleSampleItemPartProperty/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class SampleItemPartPropertyController : CommonController
    {
        SampleItemPropertiesOld oItemPartP = new SampleItemPropertiesOld();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public SampleItemPartPropertyController()
        {
            oDoaminObject = new SampleItemPropertiesOld();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "SampleItemPartProperty";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oItemPartP.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                SampleItemPropertiesOld oItemPartP = oDoaminObject as SampleItemPropertiesOld;
                oItemPartP.ID = decimal.Parse(pId);
                SampleItemPartProperty oAtualObject = oItemPartP.DAO as SampleItemPartProperty;
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_PartPropertyID = oAtualObject.Pk_PartPropertyID
                    //    Name = oAtualObject.Name

                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new BoxType();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        //[HttpPost]
        //public JsonResult NameDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sName = oValues["Name"];
        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("Name", sName));
        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
        //        List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<BoxType> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxType>().ToList();
        //        var oHeadToDisplay = oHeadObjects.Select(p => new
        //        {
        //            Pk_BoxTypeID = p.Pk_BoxTypeID,
        //            Name = p.Name
        //        }).ToList().OrderBy(s => s.Name);
        //        return Json(new { Success = true, Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}
        [HttpPost]
        public JsonResult ItemPropertyListByFiter(string Pk_PartPropertyID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                //   oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Pk_PartPropertyID", Pk_PartPropertyID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<SampleItemPartProperty> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<SampleItemPartProperty>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_PartPropertyID = p.Pk_PartPropertyID
                    // Name = p.Name

                }).ToList().OrderBy(s => s.Pk_PartPropertyID);
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }



        [HttpPost]
        public JsonResult LayerDet(string Pk_PartPropertyID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {


                SampleItemPropertiesOld oItemPartP = oDoaminObject as SampleItemPropertiesOld;
                oItemPartP.ID = decimal.Parse(Pk_PartPropertyID);
                SampleItemPartProperty oEnquiryObjects = oItemPartP.DAO as SampleItemPartProperty;
                var oDeliveryScheduleToDisplay = oEnquiryObjects.SampleItems_Layers.Select(p => new
                {
                    Pk_LayerID = p.Pk_LayerID,
                    GSM = p.GSM,
                    BF = p.BF,
                    Weight = p.Weight


                }).ToList();

                return Json(new { Result = "OK", Records = oDeliveryScheduleToDisplay, TotalRecordCount = oDeliveryScheduleToDisplay.Count() });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}



