﻿//Name            : Bank Controller Class
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable

//Author          :   Shantha
//Date            :  26/10/2015
//CRH Number      :   SL0002
//Modifications   :

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class BankController : CommonController
    {
        BankN oBank = new BankN();
        AccType oAcc = new AccType();

        public BankController()
        {
            oDoaminObject = new BankN();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Bank Accounts Names";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oBank.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public JsonResult getAccount(string pId = "")
        {
            List<AccountType> oListOfAcc = oAcc.Search(null).ListOfRecords.OfType<AccountType>().ToList();
            var oAccToDisplay = oListOfAcc.Select(p => new
            {
                Name = p.AccTypeName,
                Id = p.Pk_AccountTypeID
            });

            return Json(oAccToDisplay);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                BankN oBank = oDoaminObject as BankN;
                oBank.ID = decimal.Parse(pId);
                Bank oAtualObject = oBank.DAO as Bank;
                //Create a anominious object here to break the circular reference

                var oBankToDisplay = new
                {

                    Pk_BankID = oAtualObject.Pk_BankID,
                    AccountName = oAtualObject.AccountName,
                    AccountNumber = oAtualObject.AccountNumber,
                    BranchName=oAtualObject.BranchName,
                    Fk_AccountTypeID=oAtualObject.AccountType.AccTypeName,
                    BankName=oAtualObject.BankName


                };

                return Json(new { success = true, data = oBankToDisplay });
            }
            else
            {
                var oBankToDisplay = new Bank();
                return Json(new { success = true, data = oBankToDisplay });
            }
        }



        //[HttpPost]
        //public JsonResult AccountDuplicateChecking(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        string sAccountName = oValues["AccTypeName"];

        //        List<SearchParameter> oSearchParams = new List<SearchParameter>();
        //        oSearchParams.Add(new SearchParameter("AccTypeName", sAccountName));

        //        SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

        //        List<EntityObject> oBankToDisplayObjects = oSearchResult.ListOfRecords;
        //        List<Bank> oBankObjects = oBankToDisplayObjects.Select(p => p).OfType<Bank>().ToList();

        //        var oBankToDisplay = oBankObjects.Select(p => new
        //        {
        //            Pk_AccountTypeID = p.Pk_AccountTypeID,
        //            AccTypeName = p.AccTypeName
        //        }).ToList().OrderBy(s => s.AccTypeName);

        //        return Json(new { Success = true, Records = oBankToDisplay, TotalRecordCount = oSearchResult.RecordCount });
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { Success = false, Message = ex.Message });
        //    }
        //}

        [HttpPost]
        public JsonResult BankListByFiter(string Pk_BankID = "", string BankName = "", string AccountName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_BankID", Pk_BankID));
                oSearchParams.Add(new SearchParameter("BankName", BankName));
                oSearchParams.Add(new SearchParameter("AccountName", AccountName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oBankToDisplayObjects = oSearchResult.ListOfRecords;
                List<Bank> oBankObjects = oBankToDisplayObjects.Select(p => p).OfType<Bank>().ToList();

                //Create a anominious object here to break the circular reference
                var oBankToDisplay = oBankObjects.Select(p => new
                {
                    Pk_BankID = p.Pk_BankID,
                    BankName = p.BankName,
                    AccountName = p.AccountName,
                    AccountNumber=p.AccountNumber,
                    BranchName=p.BranchName,
                    Fk_AccountTypeID=p.AccountType.AccTypeName


                }).ToList().OrderBy(s => s.BankName);

                return Json(new { Result = "OK", Records = oBankToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
