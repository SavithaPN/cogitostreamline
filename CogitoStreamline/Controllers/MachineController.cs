﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class MachineController : CommonController
    {
         Country oCountry = new Country();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public MachineController()
        {
            oDoaminObject = new Machine();
        }
        public override ActionResult Index()
        {

            ViewBag.Header = "Machine";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCountry.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Machine oMachine = oDoaminObject as Machine;
                oMachine.ID = decimal.Parse(pId);
                gen_Machine oAtualObject = oMachine.DAO as gen_Machine;
                //Create a anominious object here to break the circular reference

                var oMacToDisplay = new
                {
                    Pk_Machine = oAtualObject.Pk_Machine,
                    Location = oAtualObject.Location,
                    Name = oAtualObject.Name,
                    Capacity_Length = oAtualObject.Capacity_Length,
                    Capacity_Width = oAtualObject.Capacity_Width,
                    Capacity_Other = oAtualObject.Capacity_Other,
                    AdditionalInformation = oAtualObject.AdditionalInformation
                   

                };

                return Json(new { success = true, data = oMacToDisplay });
            }
            else
            {
                var oMacToDisplay = new gen_Machine();
                return Json(new { success = true, data = oMacToDisplay });
            }
        }
        [HttpPost]
        public JsonResult MachineNameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oMachineToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Machine> oMachineObjects = oMachineToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();
                var oMachineToDisplay = oMachineObjects.Select(p => new
                {
                    Pk_Machine = p.Pk_Machine,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Success = true, Records = oMachineToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult MachineLocationDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sLocation = oValues["Location"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Location", sLocation));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oMachineToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Machine> oMachineObjects = oMachineToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();
                var oMachineToDisplay = oMachineObjects.Select(p => new
                {
                    Pk_Machine = p.Pk_Machine,
                    Location = p.Location
                }).ToList().OrderBy(s => s.Location);
                return Json(new { Success = true, Records = oMachineToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult MachineListByFiter(string Pk_Machine = "", string Name = "", string Location = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Machine", Pk_Machine));
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Location", Location));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMacToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Machine> oMachineObjects = oMacToDisplayObjects.Select(p => p).OfType<gen_Machine>().ToList();

                //Create a anominious object here to break the circular reference
                var oMacToDisplay = oMachineObjects.Select(p => new
                {

                    Pk_Machine = p.Pk_Machine,
                    Location = p.Location,
                    Name = p.Name,
                    Capacity_Length = p.Capacity_Length,
                    Capacity_Width = p.Capacity_Width,
                    Capacity_Other = p.Capacity_Other,
                    AdditionalInformation = p.AdditionalInformation

                }).ToList().OrderBy(s => s.Name);

                return Json(new { Result = "OK", Records = oMacToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

