﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class CityController : CommonController
    {
        State oState = new State();
        Country oCountry = new Country();
        public CityController()
        {
            oDoaminObject = new City();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "City";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oState.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                City oCity = oDoaminObject as City;
                oCity.ID = decimal.Parse(pId);
                gen_City oAtualObject = oCity.DAO as gen_City;
                //Create a anominious object here to break the circular reference

                var oCityToDisplay = new
                {
                    Pk_CityID = oAtualObject.Pk_CityID,
                    CityName = oAtualObject.CityName,
                    Fk_StateID = oAtualObject.Fk_StateID,
                    Country = oAtualObject.Country

                };

                return Json(new { success = true, data = oCityToDisplay });
            }
            else
            {
                var oCityToDisplay = new gen_City();
                return Json(new { success = true, data = oCityToDisplay });
            }
        }

        [HttpPost]
        public JsonResult CityDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sCountry = oValues["Country"];
                string sFk_StateID = oValues["Fk_StateID"];
                string sCityName = oValues["CityName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Country", sCountry));
                oSearchParams.Add(new SearchParameter("StateId", sFk_StateID));
                oSearchParams.Add(new SearchParameter("CityName", sCityName));


                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCityToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_City> oCityObjects = oCityToDisplayObjects.Select(p => p).OfType<gen_City>().ToList();

                var oCityToDisplay = oCityObjects.Select(p => new
                {

                    Pk_CityID = p.Pk_CityID,
                    CityName = p.CityName,
                    Fk_StateID = p.Fk_StateID


                }).ToList().OrderBy(s => s.CityName);

                return Json(new { Success = true, Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult getState(string pId = "")
        {
            List<gen_State> oListOfState = oState.Search(null).ListOfRecords.OfType<gen_State>().ToList();

            var oStateToDisplay = oListOfState.Select(p => new
            {
                Name = p.StateName,
                Id = p.Pk_StateID
            });

            return Json(oStateToDisplay);
        }

        [HttpPost]
        public JsonResult getCountry(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }

        [HttpPost]
        public JsonResult CityListByFiter(string Pk_CityID = "", string CityName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_CityID", Pk_CityID));
                oSearchParams.Add(new SearchParameter("CityName", CityName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oCityToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_City> oCityObjects = oCityToDisplayObjects.Select(p => p).OfType<gen_City>().ToList();

                //Create a anominious object here to break the circular reference
                var oCityToDisplay = oCityObjects.Select(p => new
                {
                    Pk_CityID = p.Pk_CityID,
                    CityName = p.CityName,
                    State = p.gen_State.StateName,
                    Country = p.gen_Country.CountryName
                }).ToList().OrderBy(s => s.CityName);

                return Json(new { Result = "OK", Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CityListByState(string data = "")
        {
            try
            {
                City oCity = new City();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sStateId = oValues["StateId"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("StateId", sStateId));

                SearchResult oSearchResult = oCity.Search(oSearchParams);

                List<EntityObject> oCitysToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_City> oCityObjects = oCitysToDisplayObjects.Select(p => p).OfType<gen_City>().ToList();

                var oCityToDisplay = oCityObjects.Select(p => new
                {
                    Pk_CityID = p.Pk_CityID,
                    CityName = p.CityName
                }).ToList();

                return Json(new { Success = true, Records = oCityToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
    }
}

