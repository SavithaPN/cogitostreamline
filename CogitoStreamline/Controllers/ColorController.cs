﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class ColorController : CommonController
    {
        Color oColor = new Color();

        public ColorController()
        {
            oDoaminObject = new Color();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Color";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oColor.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Color oColor = oDoaminObject as Color;
                oColor.ID = decimal.Parse(pId);
                gen_Color oAtualObject = oColor.DAO as gen_Color;
                //Create a anominious object here to break the circular reference

                var oColorToDisplay = new
                {

                    Pk_Color = oAtualObject.Pk_Color,
                    ColorName = oAtualObject.ColorName
                };

                return Json(new { success = true, data = oColorToDisplay });
            }
            else
            {
                var oColorToDisplay = new gen_Color();
                return Json(new { success = true, data = oColorToDisplay });
            }
        }



        [HttpPost]
        public JsonResult ColorDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sColorName = oValues["ColorName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("ColorName", sColorName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Color> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<gen_Color>().ToList();

                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Color = p.Pk_Color,
                    ColorName = p.ColorName
                }).ToList().OrderBy(s => s.ColorName);

                return Json(new { Success = true, Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ColorListByFiter(string ColorName = "", string Pk_Color = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Color", Pk_Color));
                oSearchParams.Add(new SearchParameter("ColorName", ColorName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oColorToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_Color> oColorObjects = oColorToDisplayObjects.Select(p => p).OfType<gen_Color>().ToList();

                //Create a anominious object here to break the circular reference
                var oColorToDisplay = oColorObjects.Select(p => new
                {
                    Pk_Color = p.Pk_Color,
                    ColorName = p.ColorName

                }).ToList().OrderBy(s => s.ColorName);

                return Json(new { Result = "OK", Records = oColorToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
