﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;

namespace CogitoStreamline.Controllers
{
    public class UserController : CommonController
    {
       //Module oModule = new Module();
        Tenants oTenants = new Tenants();

        public UserController()
        {
            oDoaminObject = new User();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "User";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }


        [HttpPost]
        public JsonResult getTenants(string pId = "")
        {
            List<wgTenant> oListOfContries = oTenants.Search(null).ListOfRecords.OfType<wgTenant>().ToList();
            var oModulesToDisplay = oListOfContries.Select(p => new
            {
                Name = p.TanentName,
                Id = p.Pk_Tanent
            });

            return Json(oModulesToDisplay);
        }

        //[HttpPost]
        //public JsonResult getModules(string pId = "")
        //{
        //    List<ideModule> oListOfContries = oModule.Search(null).ListOfRecords.OfType<ideModule>().ToList();
        //    var oModulesToDisplay = oListOfContries.Select(p => new
        //    {
        //        Name= p.ModuleName,
        //        Id= p.Pk_Modules
        //    });

        //    return Json(oModulesToDisplay);
        //}

        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                User oUser = oDoaminObject as User;
                oUser.ID = decimal.Parse(pId);
                ideUser oAtualObject = oUser.DAO as ideUser;
                //Create a anominious object here to break the circular reference
               // int i = -1;
                 
                var oUserToDisplay = new
                {
                    UserName = oAtualObject.UserName,
                    FirstName = oAtualObject.FirstName,
                    LastName = oAtualObject.LastName,
                    Company = oAtualObject.wgTenant.TanentName,
                    Fk_Tanent = oAtualObject.Fk_Tanent,
                    Office = oAtualObject.Office,
                    CreatedDate = DateTime.Parse(oAtualObject.CreatedDate.ToString()).ToString("d"),
                    IsActive = oAtualObject.IsActive,
                    Pk_Users = oAtualObject.Pk_Users,
                };

                return Json(new { success = true, data = oUserToDisplay });
            }
            else
            {
                var oUserToDisplay = new ideUser();
                return Json(new { success = true, data = oUserToDisplay });
            }
        }


      
        [HttpPost]
        public JsonResult UserListByFiter(string firstName = "", string lastName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("FirstName", firstName));
                oSearchParams.Add(new SearchParameter("LastName", lastName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oUsersToDisplayObjects = oSearchResult.ListOfRecords;
                List<ideUser> oUserObjects = oUsersToDisplayObjects.Select(p => p).OfType<ideUser>().ToList();

                //Create a anominious object here to break the circular reference
                var oUsersToDisplay = oUserObjects.Select(p => new
                {
                    UserName = p.UserName,
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    Company = p.wgTenant.TanentName,
                    Office = p.Office,
                    Pk_Users = p.Pk_Users
                }).ToList();

                return Json(new { Result = "OK", Records = oUsersToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
