﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class MachineMaintenanceController : CommonController
    {
        Machine oMachine = new Machine();
        public MachineMaintenanceController()
        {
            oDoaminObject = new MacMaintenance();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Machine Maintenance";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oMachine.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                MacMaintenance oMachineMaintenance = oDoaminObject as MacMaintenance;
                oMachineMaintenance.ID = decimal.Parse(pId);
                MachineMaintenance oAtualObject = oMachineMaintenance.DAO as MachineMaintenance;
                //Create a anominious object here to break the circular reference

                var oMaintenanceToDisplay = new
                {
                    Pk_MachineMaintenance = oAtualObject.Pk_MachineMaintenance,
                    Fk_Machine = oAtualObject.Fk_Machine,
                    //FromDate = oAtualObject.FromDate,
                    //ToDate = oAtualObject.ToDate,
                    FromDate = DateTime.Parse(oAtualObject.FromDate.ToString()).ToString("dd/MM/yyyy"),
                    ToDate = DateTime.Parse(oAtualObject.ToDate.ToString()).ToString("dd/MM/yyyy"),
                    DoneBy = oAtualObject.DoneBy,
                    Comments = oAtualObject.Comments

                };

                return Json(new { success = true, data = oMaintenanceToDisplay });
            }
            else
            {
                var oMaintenanceToDisplay = new MachineMaintenance();
                return Json(new { success = true, data = oMaintenanceToDisplay });
            }
        }
        [HttpPost]
        public JsonResult getMachine(string pId = "")
        {
            List<gen_Machine> oListOfMachine = oMachine.Search(null).ListOfRecords.OfType<gen_Machine>().ToList();

            var oMacMaintenanceToDisplay = oListOfMachine.Select(p => new
            {
                Name = p.Name,
                Id = p.Pk_Machine
            });

            return Json(oMacMaintenanceToDisplay);
        }
        [HttpPost]
        public JsonResult MachineMaintenanceListByFiter(string Pk_MachineMaintenance = "", string FromDate = "", string ToDate = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_MachineMaintenance", Pk_MachineMaintenance));
                oSearchParams.Add(new SearchParameter("FromDate", FromDate));
                oSearchParams.Add(new SearchParameter("ToDate", ToDate));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaintenanceToDisplayObjects = oSearchResult.ListOfRecords;
                List<MachineMaintenance> oMaintenanceObjects = oMaintenanceToDisplayObjects.Select(p => p).OfType<MachineMaintenance>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaintenanceToDisplay = oMaintenanceObjects.Select(p => new
                {
                    Pk_MachineMaintenance = p.Pk_MachineMaintenance,
                    Fk_Machine = p.gen_Machine.Name,
                    FromDate = p.FromDate!= null ? DateTime.Parse(p.FromDate.ToString()).ToString("dd/MM/yyyy") : "",
                    ToDate = p.ToDate!= null ? DateTime.Parse(p.ToDate.ToString()).ToString("dd/MM/yyyy") : "",
                    DoneBy = p.DoneBy,
                    Comments = p.Comments


                }).ToList();

                return Json(new { Result = "OK", Records = oMaintenanceToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

