﻿//Name            : Account Type Controller Class
//Description     :   Contains the page class definition, every page in the application will be instance of this class with the following
//                    1. Load Method --- Used during Editing 2. List by filter method -- used for data loading in the jtable
//Author          :   Shantha
//Date            :  26/10/2015
//CRH Number      :   SL0002
//Modifications   :



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

using System.IO;

namespace CogitoStreamline.Controllers
{

    public class AccountTypeController : CommonController
    {
        AccType oAcc = new AccType();

        public AccountTypeController()
        {
            oDoaminObject = new AccType();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Account Type";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oAcc.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                AccType oAcc = oDoaminObject as AccType;
                oAcc.ID = decimal.Parse(pId);
                AccountType oAtualObject = oAcc.DAO as AccountType;
                //Create a anominious object here to break the circular reference

                var oAccToDisplay = new
                {

                    Pk_AccountTypeID = oAtualObject.Pk_AccountTypeID,
                    AccTypeName = oAtualObject.AccTypeName,
                    Description=oAtualObject.Description

                };

                return Json(new { success = true, data = oAccToDisplay });
            }
            else
            {
                var oAccToDisplay = new AccountType();
                return Json(new { success = true, data = oAccToDisplay });
            }
        }



        [HttpPost]
        public JsonResult AccountDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sAccountName = oValues["AccTypeName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("AccTypeName", sAccountName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oAccToDisplayObjects = oSearchResult.ListOfRecords;
                List<AccountType> oAccObjects = oAccToDisplayObjects.Select(p => p).OfType<AccountType>().ToList();

                var oAccToDisplay = oAccObjects.Select(p => new
                {
                    Pk_AccountTypeID = p.Pk_AccountTypeID,
                    AccTypeName = p.AccTypeName
                }).ToList().OrderBy(s => s.AccTypeName);

                return Json(new { Success = true, Records = oAccToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AccountListByFiter(string AccTypeName = "", string Pk_AccountTypeID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_AccountTypeID", Pk_AccountTypeID));
                oSearchParams.Add(new SearchParameter("AccTypeName", AccTypeName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oAccToDisplayObjects = oSearchResult.ListOfRecords;
                List<AccountType> oAccObjects = oAccToDisplayObjects.Select(p => p).OfType<AccountType>().ToList();

                //Create a anominious object here to break the circular reference
                var oAccToDisplay = oAccObjects.Select(p => new
                {
                    Pk_AccountTypeID = p.Pk_AccountTypeID,
                    AccTypeName = p.AccTypeName,
                    Description=p.Description

                }).ToList().OrderBy(s => s.AccTypeName);

                return Json(new { Result = "OK", Records = oAccToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
