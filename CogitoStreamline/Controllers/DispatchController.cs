﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class DispatchController : Controller
//    {
//        //
//        // GET: /Dispatch/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using WebGareCore;
using CogitoStreamline.Controllers;
//using iTextSharp;
using System.IO;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
using System.Data;
using Microsoft.Reporting.WebForms;
using CogitoStreamline;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing.Printing;


namespace CogitoStreamline.Controllers
{
    public class DispatchController : CommonController
    {

        DispatchM oBilling = new DispatchM();
        //Taxx oTax = new Taxx();


        public DispatchController()
        {
            oDoaminObject = new DispatchM();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Product Dispatch";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

     
        //public ActionResult InvReport(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Invno"]);

        //        List<VwInvoice> BillList = new List<VwInvoice>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //BillList = dc.VwInvoices.ToList();
        //        BillList = dc.VwInvoice.Where(x => x.Pk_Dispatch == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

        //        if (BillList.Count > 0)
        //        {
        //            var CtrVal = BillList.Count;
        //            DataTable dt = new DataTable("InvoiceBill");

        //            dt.Columns.Add("CustomerName");
        //            dt.Columns.Add("CustomerAddress");
        //            dt.Columns.Add("CityName");
        //            dt.Columns.Add("StateName");
        //            dt.Columns.Add("PinCode");
        //            dt.Columns.Add("Pk_Dispatch");
        //            dt.Columns.Add("Dis_Date");
        //            dt.Columns.Add("Fk_OrderNo");
        //            dt.Columns.Add("TaxValue");
        //            dt.Columns.Add("Quantity");
        //            dt.Columns.Add("Price");
        //            dt.Columns.Add("Fk_JobCardID");
        //            dt.Columns.Add("NetAmount");
        //            dt.Columns.Add("Name");
        //            dt.Columns.Add("Description");
        //            dt.Columns.Add("Pk_DispDet");
        //            dt.Columns.Add("Esugam");
        //            dt.Columns.Add("COA");
        //            dt.Columns.Add("CreditLimit");
        //            dt.Columns.Add("HsnCode");
        //            //dt.Columns.Add("PAN");
        //            dt.Columns.Add("VAT");
        //            //dt.Columns.Add("");
        //            dt.Columns.Add("TaxType");

        //            dt.Columns.Add("DeliveryName");
        //            dt.Columns.Add("DeliveryAdd1");
        //            dt.Columns.Add("DeliveryAdd2");

        //            dt.Columns.Add("PName");
        //            dt.Columns.Add("Length");
        //            dt.Columns.Add("Width");
        //            dt.Columns.Add("Height");


        //            dt.Columns.Add("Buyers_OrderNo");
        //            dt.Columns.Add("BOrderDated");
        //            dt.Columns.Add("DNTimeofInv");
        //            dt.Columns.Add("MVehicleNo");
        //            dt.Columns.Add("DNTimeofRemoval");
        //            dt.Columns.Add("Pk_PartPropertyID");



        //            decimal NetValue = 0;
        //            decimal cgval = 0;
        //            decimal ExValue = 0;
        //            decimal VatVal = 0;
        //            string ttype = "";
        //            string Ttax = "";
        //            string fct3 = "";
        //            //string fh = "";

        //            foreach (VwInvoice entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["CustomerName"] = entity.CustomerName;
        //                row["CustomerAddress"] = entity.CustomerAddress;
        //                row["CityName"] = entity.CityName;
        //                row["StateName"] = entity.StateName;
        //                row["PinCode"] = entity.PinCode;
        //                row["Pk_Dispatch"] = entity.Pk_Dispatch;
        //                row["Dis_Date"] = entity.Dis_Date;
        //                row["Fk_OrderNo"] = entity.Fk_OrderNo;
        //                row["TaxValue"] = entity.TaxValue;
        //                row["Quantity"] = entity.Quantity;
        //                row["Price"] = entity.Price;
        //                row["Fk_JobCardID"] = entity.Fk_JobCardID;
        //                row["NetAmount"] = entity.NetAmount;
        //                NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
        //                row["Name"] = entity.Name;
        //                row["HsnCode"] = entity.HsnCode;
        //                row["Pk_DispDet"] = entity.Pk_DispDet;
        //                row["Esugam"] = entity.ESUGAM;
        //                row["COA"] = entity.COA;
        //                row["CreditLimit"] = entity.CreditLimit;
        //                row["HsnCode"] = entity.HsnCode;
        //                row["Description"] = entity.Description;

        //                row["DeliveryName"] = entity.DeliveryName;
        //                row["DeliveryAdd1"] = entity.DeliveryAdd1;
        //                row["DeliveryAdd2"] = entity.DeliveryAdd2;


        //                row["PName"] = entity.PName;
        //                row["Length"] = entity.Length;
        //                row["Width"] = entity.Width;
        //                row["Height"] = entity.Height;


        //                row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
        //                row["BOrderDated"] = entity.BOrderDated;
        //                row["DNTimeofInv"] = entity.DNTimeofInv;
        //                row["MVehicleNo"] = entity.MVehicleNo;
        //                row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
        //                row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;




        //                row["TaxType"] = entity.TaxType;

        //                if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
        //                {
        //                    Ttax = "V";
        //                    ttype = "SGST @ 6 % & CGST @ 6%";


        //                }
        //                else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
        //                {
        //                    Ttax = "C";
        //                    ttype = "IGST @ 12 % ";
        //                }





        //                dt.Rows.Add(row);
        //            }
        //            NetValue = Convert.ToDecimal(NetValue / CtrVal);
        //            if (fct3.Length > 0)
        //            {
        //                ExValue = 0;
        //            }
        //            else
        //            {
        //                ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
        //            }

        //            if (Ttax == "V")
        //            {
        //                VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
        //                cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
        //            }
        //            else if (Ttax == "C")
        //            {
        //                VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
        //                cgval = 0;
        //            }






        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

        //            orp.Load("@\\Report\\Bill1.rpt");
        //            orp.SetDataSource(dt.DefaultView);



        //            ReportDocument repDoc = orp;

        //            ParameterFieldDefinitions crParameterFieldDefinitions3;
        //            ParameterFieldDefinition crParameterFieldDefinition3;
        //            ParameterValues crParameterValues3 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue3.Value = NetValue;
        //            crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
        //            crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

        //            crParameterValues3.Clear();
        //            crParameterValues3.Add(crParameterDiscreteValue3);
        //            crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);



        //            ParameterFieldDefinitions crParameterFieldDefinitions4;
        //            ParameterFieldDefinition crParameterFieldDefinition4;
        //            ParameterValues crParameterValues4 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue4.Value = "Original Copy";
        //            crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
        //            crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

        //            crParameterValues4.Clear();
        //            crParameterValues4.Add(crParameterDiscreteValue4);
        //            crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);


        //            //ParameterFieldDefinitions crParameterFieldDefinitions5;
        //            //ParameterFieldDefinition crParameterFieldDefinition5;
        //            //ParameterValues crParameterValues5 = new ParameterValues();
        //            //ParameterDiscreteValue crParameterDiscreteValue5 = new ParameterDiscreteValue();
        //            //crParameterDiscreteValue5.Value = fct3;
        //            //crParameterFieldDefinitions5 = orp.DataDefinition.ParameterFields;
        //            //crParameterFieldDefinition5 = crParameterFieldDefinitions4["StrValuefct3"];
        //            //crParameterValues5 = crParameterFieldDefinition5.CurrentValues;

        //            //crParameterValues5.Clear();
        //            //crParameterValues5.Add(crParameterDiscreteValue5);
        //            //crParameterFieldDefinition5.ApplyCurrentValues(crParameterValues5);




        //            //ParameterFieldDefinitions crParameterFieldDefinitions6;
        //            //ParameterFieldDefinition crParameterFieldDefinition6;
        //            //ParameterValues crParameterValues6 = new ParameterValues();
        //            //ParameterDiscreteValue crParameterDiscreteValue6 = new ParameterDiscreteValue();
        //            //crParameterDiscreteValue6.Value = fh;
        //            //crParameterFieldDefinitions6 = orp.DataDefinition.ParameterFields;
        //            //crParameterFieldDefinition6 = crParameterFieldDefinitions6["StrValueh"];
        //            //crParameterValues6 = crParameterFieldDefinition6.CurrentValues;

        //            //crParameterValues6.Clear();
        //            //crParameterValues6.Add(crParameterDiscreteValue6);
        //            //crParameterFieldDefinition6.ApplyCurrentValues(crParameterValues6);


        //            ParameterFieldDefinitions crParameterFieldDefinitions7;
        //            ParameterFieldDefinition crParameterFieldDefinition7;
        //            ParameterValues crParameterValues7 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue7.Value = ExValue;
        //            crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
        //            crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

        //            crParameterValues7.Clear();
        //            crParameterValues7.Add(crParameterDiscreteValue7);
        //            crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




        //            ParameterFieldDefinitions crParameterFieldDefinitions1;
        //            ParameterFieldDefinition crParameterFieldDefinition1;
        //            ParameterValues crParameterValues1 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue1.Value = VatVal;
        //            crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
        //            crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

        //            crParameterValues1.Clear();
        //            crParameterValues1.Add(crParameterDiscreteValue1);
        //            crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

        //            //ttype


        //            ParameterFieldDefinitions crParameterFieldDefinitions12;
        //            ParameterFieldDefinition crParameterFieldDefinition12;
        //            ParameterValues crParameterValues12 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue12.Value = ttype;
        //            crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
        //            crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

        //            crParameterValues12.Clear();
        //            crParameterValues12.Add(crParameterDiscreteValue12);
        //            crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "BillOriginal-" + " " + sInvno + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();

        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //        //  Response.Redirect("Index");
        //    }
        //}





        //public ActionResult InvReportDuplicate(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Invno"]);

        //        List<VwInvoice> BillList = new List<VwInvoice>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //BillList = dc.VwInvoices.ToList();
        //        BillList = dc.VwInvoice.Where(x => x.Pk_Dispatch == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

        //        if (BillList.Count > 0)
        //        {
        //            var CtrVal = BillList.Count;
        //            DataTable dt = new DataTable("InvoiceBill");
        //            dt.Columns.Add("CustomerName");
        //            dt.Columns.Add("CustomerAddress");
        //            dt.Columns.Add("CityName");
        //            dt.Columns.Add("StateName");
        //            dt.Columns.Add("PinCode");
        //            dt.Columns.Add("Pk_Dispatch");
        //            dt.Columns.Add("Dis_Date");
        //            dt.Columns.Add("Fk_OrderNo");
        //            dt.Columns.Add("TaxValue");
        //            dt.Columns.Add("Quantity");
        //            dt.Columns.Add("Price");
        //            dt.Columns.Add("Fk_JobCardID");
        //            dt.Columns.Add("NetAmount");
        //            dt.Columns.Add("Name");
        //            //dt.Columns.Add("Pk_Dispatch");
        //            dt.Columns.Add("Pk_DispDet");
        //            dt.Columns.Add("Esugam");
        //            dt.Columns.Add("COA");
        //            dt.Columns.Add("CreditLimit");
        //            dt.Columns.Add("HsnCode");
        //            //dt.Columns.Add("PAN");
        //            dt.Columns.Add("VAT");
        //            //dt.Columns.Add("");
        //            dt.Columns.Add("TaxType");

        //            dt.Columns.Add("DeliveryName");
        //            dt.Columns.Add("DeliveryAdd1");
        //            dt.Columns.Add("DeliveryAdd2");

        //            dt.Columns.Add("PName");
        //            dt.Columns.Add("Length");
        //            dt.Columns.Add("Width");
        //            dt.Columns.Add("Height");


        //            dt.Columns.Add("Buyers_OrderNo");
        //            dt.Columns.Add("BOrderDated");
        //            dt.Columns.Add("DNTimeofInv");
        //            dt.Columns.Add("MVehicleNo");
        //            dt.Columns.Add("DNTimeofRemoval");
        //            dt.Columns.Add("Pk_PartPropertyID");



        //            decimal NetValue = 0;
        //            decimal cgval = 0;
        //            decimal ExValue = 0;
        //            decimal VatVal = 0;
        //            string ttype = "";
        //            string Ttax = "";
        //            string fct3 = "";
        //            //string fh = "";

        //            foreach (VwInvoice entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["CustomerName"] = entity.CustomerName;
        //                row["CustomerAddress"] = entity.CustomerAddress;
        //                row["CityName"] = entity.CityName;
        //                row["StateName"] = entity.StateName;
        //                row["PinCode"] = entity.PinCode;
        //                row["Pk_Dispatch"] = entity.Pk_Dispatch;
        //                row["Dis_Date"] = entity.Dis_Date;
        //                row["Fk_OrderNo"] = entity.Fk_OrderNo;
        //                row["TaxValue"] = entity.TaxValue;
        //                row["Quantity"] = entity.Quantity;
        //                row["Price"] = entity.Price;
        //                row["Fk_JobCardID"] = entity.Fk_JobCardID;
        //                row["NetAmount"] = entity.NetAmount;
        //                NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
        //                row["Name"] = entity.Name;
        //                row["HsnCode"] = entity.HsnCode;
        //                row["Pk_DispDet"] = entity.Pk_DispDet;
        //                row["Esugam"] = entity.ESUGAM;
        //                row["COA"] = entity.COA;
        //                row["CreditLimit"] = entity.CreditLimit;
        //                row["HsnCode"] = entity.HsnCode;

        //                row["DeliveryName"] = entity.DeliveryName;
        //                row["DeliveryAdd1"] = entity.DeliveryAdd1;
        //                row["DeliveryAdd2"] = entity.DeliveryAdd2;


        //                row["PName"] = entity.PName;
        //                row["Length"] = entity.Length;
        //                row["Width"] = entity.Width;
        //                row["Height"] = entity.Height;


        //                row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
        //                row["BOrderDated"] = entity.BOrderDated;
        //                row["DNTimeofInv"] = entity.DNTimeofInv;
        //                row["MVehicleNo"] = entity.MVehicleNo;
        //                row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
        //                row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;




        //                row["TaxType"] = entity.TaxType;

        //                if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
        //                {
        //                    Ttax = "V";
        //                    ttype = "SGST @ 6 % & CGST @ 6%";


        //                }
        //                else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
        //                {
        //                    Ttax = "C";
        //                    ttype = "IGST @ 12 % ";
        //                }





        //                dt.Rows.Add(row);
        //            }
        //            NetValue = Convert.ToDecimal(NetValue / CtrVal);
        //            if (fct3.Length > 0)
        //            {
        //                ExValue = 0;
        //            }
        //            else
        //            {
        //                ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
        //            }

        //            if (Ttax == "V")
        //            {
        //                VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
        //                cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
        //            }
        //            else if (Ttax == "C")
        //            {
        //                VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
        //                cgval = 0;
        //            }






        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

        //            orp.Load("@\\Report\\Bill1.rpt");
        //            orp.SetDataSource(dt.DefaultView);



        //            ReportDocument repDoc = orp;


        //            ParameterFieldDefinitions crParameterFieldDefinitions3;
        //            ParameterFieldDefinition crParameterFieldDefinition3;
        //            ParameterValues crParameterValues3 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue3.Value = NetValue;
        //            crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
        //            crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

        //            crParameterValues3.Clear();
        //            crParameterValues3.Add(crParameterDiscreteValue3);
        //            crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);




        //            ParameterFieldDefinitions crParameterFieldDefinitions4;
        //            ParameterFieldDefinition crParameterFieldDefinition4;
        //            ParameterValues crParameterValues4 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue4.Value = "DC";
        //            crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
        //            crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

        //            crParameterValues4.Clear();
        //            crParameterValues4.Add(crParameterDiscreteValue4);
        //            crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);









        //            ParameterFieldDefinitions crParameterFieldDefinitions7;
        //            ParameterFieldDefinition crParameterFieldDefinition7;
        //            ParameterValues crParameterValues7 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue7.Value = ExValue;
        //            crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
        //            crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

        //            crParameterValues7.Clear();
        //            crParameterValues7.Add(crParameterDiscreteValue7);
        //            crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




        //            ParameterFieldDefinitions crParameterFieldDefinitions1;
        //            ParameterFieldDefinition crParameterFieldDefinition1;
        //            ParameterValues crParameterValues1 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue1.Value = VatVal;
        //            crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
        //            crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

        //            crParameterValues1.Clear();
        //            crParameterValues1.Add(crParameterDiscreteValue1);
        //            crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

        //            //ttype


        //            ParameterFieldDefinitions crParameterFieldDefinitions12;
        //            ParameterFieldDefinition crParameterFieldDefinition12;
        //            ParameterValues crParameterValues12 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue12.Value = ttype;
        //            crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
        //            crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

        //            crParameterValues12.Clear();
        //            crParameterValues12.Add(crParameterDiscreteValue12);
        //            crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "DC-" + " " + sInvno + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();


        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //        //  Response.Redirect("Index");
        //    }
        //}








        //public ActionResult InvReportExtra(string data = "")
        //{
        //    try
        //    {
        //        Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        //        int sInvno = Convert.ToInt32(oValues["Invno"]);

        //        List<VwInvoice> BillList = new List<VwInvoice>();
        //        CogitoStreamLineEntities dc = new CogitoStreamLineEntities();

        //        //BillList = dc.VwInvoices.ToList();
        //        BillList = dc.VwInvoice.Where(x => x.Pk_Dispatch == sInvno).Select(x => x).OfType<VwInvoice>().ToList();

        //        if (BillList.Count > 0)
        //        {
        //            var CtrVal = BillList.Count;
        //            DataTable dt = new DataTable("InvoiceBill");

        //            dt.Columns.Add("CustomerName");
        //            dt.Columns.Add("CustomerAddress");
        //            dt.Columns.Add("CityName");
        //            dt.Columns.Add("StateName");
        //            dt.Columns.Add("PinCode");
        //            dt.Columns.Add("Pk_Dispatch");
        //            dt.Columns.Add("Dis_Date");
        //            dt.Columns.Add("Fk_OrderNo");
        //            dt.Columns.Add("TaxValue");
        //            dt.Columns.Add("Quantity");
        //            dt.Columns.Add("Price");
        //            dt.Columns.Add("Fk_JobCardID");
        //            dt.Columns.Add("NetAmount");
        //            dt.Columns.Add("Name");
        //            //dt.Columns.Add("Pk_Dispatch");
        //            dt.Columns.Add("Pk_DispDet");
        //            dt.Columns.Add("Esugam");
        //            dt.Columns.Add("COA");
        //            dt.Columns.Add("CreditLimit");
        //            dt.Columns.Add("HsnCode");
        //            //dt.Columns.Add("PAN");
        //            dt.Columns.Add("VAT");
        //            //dt.Columns.Add("");
        //            dt.Columns.Add("TaxType");

        //            dt.Columns.Add("DeliveryName");
        //            dt.Columns.Add("DeliveryAdd1");
        //            dt.Columns.Add("DeliveryAdd2");

        //            dt.Columns.Add("PName");
        //            dt.Columns.Add("Length");
        //            dt.Columns.Add("Width");
        //            dt.Columns.Add("Height");


        //            dt.Columns.Add("Buyers_OrderNo");
        //            dt.Columns.Add("BOrderDated");
        //            dt.Columns.Add("DNTimeofInv");
        //            dt.Columns.Add("MVehicleNo");
        //            dt.Columns.Add("DNTimeofRemoval");
        //            dt.Columns.Add("Pk_PartPropertyID");



        //            decimal NetValue = 0;
        //            decimal cgval = 0;
        //            decimal ExValue = 0;
        //            decimal VatVal = 0;
        //            string ttype = "";
        //            string Ttax = "";
        //            string fct3 = "";
        //            //string fh = "";

        //            foreach (VwInvoice entity in BillList)
        //            {
        //                DataRow row = dt.NewRow();

        //                row["CustomerName"] = entity.CustomerName;
        //                row["CustomerAddress"] = entity.CustomerAddress;
        //                row["CityName"] = entity.CityName;
        //                row["StateName"] = entity.StateName;
        //                row["PinCode"] = entity.PinCode;
        //                row["Pk_Dispatch"] = entity.Pk_Dispatch;
        //                row["Dis_Date"] = entity.Dis_Date;
        //                row["Fk_OrderNo"] = entity.Fk_OrderNo;
        //                row["TaxValue"] = entity.TaxValue;
        //                row["Quantity"] = entity.Quantity;
        //                row["Price"] = entity.Price;
        //                row["Fk_JobCardID"] = entity.Fk_JobCardID;
        //                row["NetAmount"] = entity.NetAmount;
        //                NetValue = Convert.ToDecimal(NetValue + entity.NetAmount);
        //                row["Name"] = entity.Name;
        //                row["HsnCode"] = entity.HsnCode;
        //                row["Pk_DispDet"] = entity.Pk_DispDet;
        //                row["Esugam"] = entity.ESUGAM;
        //                row["COA"] = entity.COA;
        //                row["CreditLimit"] = entity.CreditLimit;
        //                row["HsnCode"] = entity.HsnCode;

        //                row["DeliveryName"] = entity.DeliveryName;
        //                row["DeliveryAdd1"] = entity.DeliveryAdd1;
        //                row["DeliveryAdd2"] = entity.DeliveryAdd2;


        //                row["PName"] = entity.PName;
        //                row["Length"] = entity.Length;
        //                row["Width"] = entity.Width;
        //                row["Height"] = entity.Height;


        //                row["Buyers_OrderNo"] = entity.Buyers_OrderNo;
        //                row["BOrderDated"] = entity.BOrderDated;
        //                row["DNTimeofInv"] = entity.DNTimeofInv;
        //                row["MVehicleNo"] = entity.MVehicleNo;
        //                row["DNTimeofRemoval"] = entity.DNTimeofRemoval;
        //                row["Pk_PartPropertyID"] = entity.Pk_PartPropertyID;




        //                row["TaxType"] = entity.TaxType;

        //                if (entity.TaxType.TrimEnd() == "SGST")     ///FOR SGST
        //                {
        //                    Ttax = "V";
        //                    ttype = "SGST @ 6 % & CGST @ 6%";


        //                }
        //                else if (entity.TaxType.TrimEnd() == "IGST")   ///FOR IGST
        //                {
        //                    Ttax = "C";
        //                    ttype = "IGST @ 12 % ";
        //                }





        //                dt.Rows.Add(row);
        //            }
        //            NetValue = Convert.ToDecimal(NetValue / CtrVal);
        //            if (fct3.Length > 0)
        //            {
        //                ExValue = 0;
        //            }
        //            else
        //            {
        //                ExValue = (Convert.ToDecimal(NetValue) * Convert.ToDecimal(0.06));
        //            }

        //            if (Ttax == "V")
        //            {
        //                VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
        //                cgval = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.06);
        //            }
        //            else if (Ttax == "C")
        //            {
        //                VatVal = (Convert.ToDecimal(NetValue)) * Convert.ToDecimal(0.12);
        //                cgval = 0;
        //            }






        //            DataSet ds = new DataSet();
        //            ds.Tables.Add(dt);
        //            CogitoStreamline.Report.Bill1 orp = new CogitoStreamline.Report.Bill1();

        //            orp.Load("@\\Report\\Bill1.rpt");
        //            orp.SetDataSource(dt.DefaultView);



        //            ReportDocument repDoc = orp;

        //            ParameterFieldDefinitions crParameterFieldDefinitions3;
        //            ParameterFieldDefinition crParameterFieldDefinition3;
        //            ParameterValues crParameterValues3 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue3 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue3.Value = NetValue;
        //            crParameterFieldDefinitions3 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition3 = crParameterFieldDefinitions3["TotAmt"];
        //            crParameterValues3 = crParameterFieldDefinition3.CurrentValues;

        //            crParameterValues3.Clear();
        //            crParameterValues3.Add(crParameterDiscreteValue3);
        //            crParameterFieldDefinition3.ApplyCurrentValues(crParameterValues3);






        //            ParameterFieldDefinitions crParameterFieldDefinitions4;
        //            ParameterFieldDefinition crParameterFieldDefinition4;
        //            ParameterValues crParameterValues4 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue4 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue4.Value = "ExtraCopy";
        //            crParameterFieldDefinitions4 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition4 = crParameterFieldDefinitions4["CopyName"];
        //            crParameterValues4 = crParameterFieldDefinition4.CurrentValues;

        //            crParameterValues4.Clear();
        //            crParameterValues4.Add(crParameterDiscreteValue4);
        //            crParameterFieldDefinition4.ApplyCurrentValues(crParameterValues4);



        //            ParameterFieldDefinitions crParameterFieldDefinitions7;
        //            ParameterFieldDefinition crParameterFieldDefinition7;
        //            ParameterValues crParameterValues7 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue7 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue7.Value = ExValue;
        //            crParameterFieldDefinitions7 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition7 = crParameterFieldDefinitions7["cgst"];
        //            crParameterValues7 = crParameterFieldDefinition7.CurrentValues;

        //            crParameterValues7.Clear();
        //            crParameterValues7.Add(crParameterDiscreteValue7);
        //            crParameterFieldDefinition7.ApplyCurrentValues(crParameterValues7);




        //            ParameterFieldDefinitions crParameterFieldDefinitions1;
        //            ParameterFieldDefinition crParameterFieldDefinition1;
        //            ParameterValues crParameterValues1 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue1 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue1.Value = VatVal;
        //            crParameterFieldDefinitions1 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition1 = crParameterFieldDefinitions1["vat5"];
        //            crParameterValues1 = crParameterFieldDefinition1.CurrentValues;

        //            crParameterValues1.Clear();
        //            crParameterValues1.Add(crParameterDiscreteValue1);
        //            crParameterFieldDefinition1.ApplyCurrentValues(crParameterValues1);

        //            //ttype


        //            ParameterFieldDefinitions crParameterFieldDefinitions12;
        //            ParameterFieldDefinition crParameterFieldDefinition12;
        //            ParameterValues crParameterValues12 = new ParameterValues();
        //            ParameterDiscreteValue crParameterDiscreteValue12 = new ParameterDiscreteValue();
        //            crParameterDiscreteValue12.Value = ttype;
        //            crParameterFieldDefinitions12 = orp.DataDefinition.ParameterFields;
        //            crParameterFieldDefinition12 = crParameterFieldDefinitions1["TAXT"];
        //            crParameterValues12 = crParameterFieldDefinition12.CurrentValues;

        //            crParameterValues12.Clear();
        //            crParameterValues12.Add(crParameterDiscreteValue12);
        //            crParameterFieldDefinition12.ApplyCurrentValues(crParameterValues12);

        //            string pdfPath = Server.MapPath("~/ConvertPDF/" + "BillExtra-" + " " + sInvno + ".pdf");
        //            FileInfo file = new FileInfo(pdfPath);
        //            if (file.Exists)
        //            {
        //                file.Delete();
        //            }
        //            var pd = new PrintDocument();


        //            orp.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //            orp.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //            DiskFileDestinationOptions objDiskOpt = new DiskFileDestinationOptions();
        //            objDiskOpt.DiskFileName = pdfPath;

        //            orp.ExportOptions.DestinationOptions = objDiskOpt;
        //            orp.Export();


        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //        //  Response.Redirect("Index");
        //    }
        //}

        //[HttpPost]
        //public JsonResult getTax(string pId = "")
        //{
        //    List<Tax> oListOfTax = oTax.Search(null).ListOfRecords.OfType<Tax>().ToList();
        //    var oTaxToDisplay = oListOfTax.Select(p => new
        //    {
        //        Name = p.TaxName,
        //        Id = p.PkTax
        //    });

        //    return Json(oTaxToDisplay);
        //}
    
        
        
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                DispatchM oBilling = oDoaminObject as DispatchM;
                oBilling.ID = decimal.Parse(pId);
                DispatchMast oAtualObject = oBilling.DAO as DispatchMast;

                int i = -1;

                var oInvBillDetails = oAtualObject.DispatchChild.Select(p => new
                {
                    Pk_DispDet = p.Pk_DispDet,
                    slno = ++i,
                    Quantity = p.Quantity,
                    Fk_BoxID = p.Fk_BoxID,
                    BoxID = p.Fk_BoxID,

                    //HsnCode = p.HsnCode,
                    Name = p.BoxMaster.Name,
                    //Fk_JobCardID = p.Fk_JobCardID,
                    //Price = p.Price,
                    //TotalAmount = p.TotalAmount,
                    //NetAmount = p.NetAmount,
                    //Description = p.Description

                });


                //Create a anominious object here to break the circular reference
                var oInvBillToDisplay = new
                {
                    Pk_Dispatch = oAtualObject.Pk_Dispatch,
                    Dis_Date = DateTime.Parse(oAtualObject.Dis_Date.ToString()).ToString("dd/MM/yyyy"),
                    //Invno = oAtualObject.Invno,
                    Fk_Customer = oAtualObject.Fk_Customer,
                    //GrandTotal = oAtualObject.GrandTotal,
                    //PayMode = oAtualObject.PayMode,
                    //COA = oAtualObject.COA,
                    //ESUGAM = oAtualObject.ESUGAM,
                    //NETVALUE = oAtualObject.NETVALUE,
                    //DeliveryName = oAtualObject.DeliveryName,
                    //DeliveryAdd1 = oAtualObject.DeliveryAdd1,
                    //DeliveryAdd2 = oAtualObject.DeliveryAdd2,
                    // FORM_CT3=oAtualObject.FORM_CT3,
                    // FORM_H=oAtualObject.FORM_H,
                    //TaxType = oAtualObject.TaxType.TrimEnd(),
                    //ED = oAtualObject.ED,
                    //Cancel = oAtualObject.Cancel,
                    DispatchChild = Json(oInvBillDetails).Data
                    //Fk_VendorId,
                };

                return Json(new { success = true, data = oInvBillToDisplay });
            }
            else
            {
                var oInvBillToDisplay = new DispatchMast();
                return Json(new { success = true, data = oInvBillToDisplay });
            }
        }
        [HttpPost]
        public JsonResult DispatchListByFiter(string Pk_Dispatch = null, string Invno = "", string FromInvDate = "", string Customer = "", string TxtFromDate = "", string TxtToDate = "", string OrderNo = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_Dispatch", Pk_Dispatch));
                oSearchParams.Add(new SearchParameter("Fk_Customer", Customer));
                oSearchParams.Add(new SearchParameter("FromInvDate", FromInvDate));
                oSearchParams.Add(new SearchParameter("TxtFromDate", TxtFromDate));
                oSearchParams.Add(new SearchParameter("TxtToDate", TxtToDate));
                //oSearchParams.Add(new SearchParameter("OrderNo", OrderNo));
                //oSearchParams.Add(new SearchParameter("Invno", Invno));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oInvBillToDisplayObjects = oSearchResult.ListOfRecords;
                List<DispatchMast> oInvBillObjects = oInvBillToDisplayObjects.Select(p => p).OfType<DispatchMast>().ToList();

                //Create a anominious object here to break the circular reference
                var oInvBillToDisplay = oInvBillObjects.Select(p => new
                {

                    Pk_Dispatch = p.Pk_Dispatch,
                    Dis_Date = p.Dis_Date != null ? DateTime.Parse(p.Dis_Date.ToString()).ToString("dd/MM/yyyy") : "",
                    //Invno = p.Invno,
                    Fk_Customer = p.Fk_Customer != null ? p.gen_Customer.CustomerName : "",
                    //GrandTotal = p.GrandTotal,
                    //NETVALUE = p.NETVALUE

                }).ToList();

                return Json(new { Result = "OK", Records = oInvBillToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
