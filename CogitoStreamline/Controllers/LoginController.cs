﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Objects.DataClasses;
using Newtonsoft.Json;
//using WebGareCore.DomainModel;
using WebGareCore.CommonObjects;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;
using WebGareCore.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class LoginController : Controller
    {
       // CogitoStreamLineEntities oEntities = new CogitoStreamLineEntities();
        public ActionResult Index()
        {
            return View();
        }

        Users oUser = new Users();

        protected override void Dispose(bool disposing)
        {
            oUser.Dispose();
            base.Dispose(disposing);
        }
                
        [HttpPost]
        public JsonResult Authenticate(string Credentials)
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(Credentials);
            List<SearchParameter> oSearchParams = new List<SearchParameter>();
            AuthenticationResult oAResult = oUser.Authenticate(oValues["UserName"], oValues["Password"]);
            if (oAResult.Success)
            {
                List<EntityObject> oUsersToDisplayObjects = oAResult.ListOfRecords;
                ideUser oUserObject = oUsersToDisplayObjects.First() as ideUser;
                Session["UserName"] = oUserObject.UserName;
                Session["User"] = oUserObject.FirstName + "-" + oUserObject.LastName;
                Session["UserID"] = oUserObject.Pk_Users;
                Session["Tanent"] = oUserObject.Fk_Tanent;
                Session["Branch"] = oUserObject.Fk_Branch;
                Session["UserRole"] = oUserObject.ideRoleUser.First().Fk_Roles;
                //ViewBag.UserRole = oUserObject.ideRoleUsers.SourceRoleName;
            }
            return Json(new { Success = oAResult.Success, Message = oAResult.Message });
        }
    }
}
