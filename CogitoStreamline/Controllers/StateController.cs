﻿
//Name          : State Controller
//Description   : Contains the State controller, every page in the application will be instance of this class with following
//                 1. Load Method(during Edit) 2. ListByFiltermethod----For displaying jTable  3. State Name Duplicate Checking 4.getCountry(to fill cmb)
//Author        : Nagesh V Rao
//Date 	        : 11/08/2015
//Crh Number    : SL0011
//Modifications : 
 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;

namespace CogitoStreamLine.Controllers
{

    public class StateController : CommonController
    {
        Country oCountry = new Country();
        public StateController()
        {
            oDoaminObject = new State();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "State";
            return base.Index();
        }

        protected override void Dispose(bool disposing)
        {
            oCountry.Dispose();
            base.Dispose(disposing);
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                State oState = oDoaminObject as State;
                oState.ID = decimal.Parse(pId);
                gen_State oAtualObject = oState.DAO as gen_State;
                //Create a anominious object here to break the circular reference

                var oStateToDisplay = new
                {
                    Pk_StateID = oAtualObject.Pk_StateID,
                    StateName = oAtualObject.StateName,
                  Country=oAtualObject.Country

                };

                return Json(new { success = true, data = oStateToDisplay });
            }
            else
            {
                var oStateToDisplay = new gen_State();
                return Json(new { success = true, data = oStateToDisplay });
            }
        }

        [HttpPost]
        public JsonResult StateDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sCountry = oValues["Country"];
                string sStateName = oValues["StateName"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("CountryId", sCountry));
                oSearchParams.Add(new SearchParameter("StateName", sStateName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oStateToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_State> oStateObjects = oStateToDisplayObjects.Select(p => p).OfType<gen_State>().ToList();

                var oStateToDisplay = oStateObjects.Select(p => new
                {
                    Pk_StateID = p.Pk_StateID,
                    StateName = p.StateName,
                    Country = p.Country

                }).ToList().OrderBy(s => s.StateName);

                return Json(new { Success = true, Records = oStateToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public JsonResult getCountry(string pId = "")
        {
            List<gen_Country> oListOfCountry = oCountry.Search(null).ListOfRecords.OfType<gen_Country>().ToList();

            var oCountryToDisplay = oListOfCountry.Select(p => new
            {
                Name = p.CountryName,
                Id = p.Pk_Country
            });

            return Json(oCountryToDisplay);
        }

        [HttpPost]
        public JsonResult StateListByFiter(string Pk_StateID = "", string StateName = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Pk_StateID", Pk_StateID));
                oSearchParams.Add(new SearchParameter("StateName", StateName));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oStateToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_State> oStateObjects = oStateToDisplayObjects.Select(p => p).OfType<gen_State>().ToList();

                //Create a anominious object here to break the circular reference
                var oStateToDisplay = oStateObjects.Select(p => new
                {
                    Pk_StateID = p.Pk_StateID,
                    StateName = p.StateName,
                    Country=p.gen_Country.CountryName

                }).ToList().OrderBy(s=>s.StateName);

                return Json(new { Result = "OK", Records = oStateToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult StateListByCountry(string data = "")
        {
            try
            {
                State oState = new State();
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sParent = oValues["CountryId"];
                
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("CountryId", sParent));

                SearchResult oSearchResult = oState.Search(oSearchParams);

                List<EntityObject> oStatesToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_State> oStateObjects = oStatesToDisplayObjects.Select(p => p).OfType<gen_State>().ToList();

                var oStatesToDisplay = oStateObjects.Select(p => new
                {
                    Pk_StateID = p.Pk_StateID,
                    StateName = p.StateName


                }).ToList();

                return Json(new { Success = true, Records = oStatesToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

    }
}


