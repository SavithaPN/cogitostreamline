﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;


using System.IO;


namespace CogitoStreamline.Controllers
{
    public class HomeController : Controller
    {
         ActivitiesRepository _oActivityRepository = new ActivitiesRepository();
        public ActionResult Index()
        {
            ViewBag.UserRole = Session["UserRole"].ToString();
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            ViewBag.Activities = _oActivityRepository.GetHomePageActivities(Session["UserName"].ToString());
            ViewBag.Repository = _oActivityRepository;
            string[] sUser = Session["User"].ToString().Split('-');
            ViewBag.FirstName = sUser[0];
            ViewBag.LastName = sUser[1];
            return View();
        }
       
    }
}
