﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class CustShippingController : CommonController
    {
        CustomerShipping oCustShip = new CustomerShipping();
        State oState = new State();
        City oCity = new City();

        public CustShippingController()
        {
            oDoaminObject = new CustomerShipping();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Customer Shipping";
            return base.Index();
        }
        [HttpPost]
        public JsonResult getState(string pId = "")
        {
            List<gen_State> oListOfState = oState.Search(null).ListOfRecords.OfType<gen_State>().ToList();

            var ostateToDisplay = oListOfState.Select(p => new
            {
                Name = p.StateName,
                Id = p.Pk_StateID
            });

            return Json(ostateToDisplay);
        }


        [HttpPost]
        public JsonResult getCity(string pId = "")
        {
            List<gen_City> oListOfCity = oCity.Search(null).ListOfRecords.OfType<gen_City>().ToList();
            var oCityToDisplay = oListOfCity.Select(p => new
            {
                Name = p.CityName,
                Id = p.Pk_CityID
            });

            return Json(oCityToDisplay);
        }

                
        protected override void Dispose(bool disposing)
        {
            oCustShip.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                CustomerShipping oCustShip = oDoaminObject as CustomerShipping;
                oCustShip.ID = decimal.Parse(pId);
                gen_CustomerShippingDetails oAtualObject = oCustShip.DAO as gen_CustomerShippingDetails;
                //Create a anominious object here to break the circular reference
                var oBranchToDisplay = new
                {
                    Pk_CustomerShippingId = oAtualObject.Pk_CustomerShippingId,
                    ShippingCustomerName = oAtualObject.ShippingCustomerName,
                    ShippingCountry = oAtualObject.ShippingCountry,
                    ShippingState = oAtualObject.ShippingState
                };
                return Json(new { success = true, data = oBranchToDisplay });
            }
            else
            {
                var oBranchToDisplay = new gen_CustomerShippingDetails();
                return Json(new { success = true, data = oBranchToDisplay });
            }
        }
        [HttpPost]
        public JsonResult ShippingListByFiter(string Pk_CustomerShippingId = "", string ShippingCustomerName = "", string ShippingCity = "", string ShippingCountry = "", string ShippingState = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
             
                oSearchParams.Add(new SearchParameter("Pk_CustomerShippingId", Pk_CustomerShippingId));
                // oSearchParams.Add(new SearchParameter("Fk_CustomerId", Fk_CustomerId));
                oSearchParams.Add(new SearchParameter("ShippingCustomerName", ShippingCustomerName));
                oSearchParams.Add(new SearchParameter("ShippingCountry", ShippingCountry));
                oSearchParams.Add(new SearchParameter("ShippingState", ShippingState));
                oSearchParams.Add(new SearchParameter("ShippingCity", ShippingCity));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oBranchToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_CustomerShippingDetails> oBranchObjects = oBranchToDisplayObjects.Select(p => p).OfType<gen_CustomerShippingDetails>().ToList();
                //Create a anominious object here to break the circular reference
                var oBranchToDisplay = oBranchObjects.Select(p => new
                {
                    Pk_CustomerShippingId = p.Pk_CustomerShippingId,
                    ShippingCustomerName = p.ShippingCustomerName,
                    ShippingCountry = p.ShippingCountry,
                    ShippingState = p.ShippingState

                }).ToList().OrderBy(s => s.ShippingCustomerName);
                return Json(new { Result = "OK", Records = oBranchToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

