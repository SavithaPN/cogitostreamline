﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel;
using CogitoStreamLineModel.DomainModel;

namespace CogitoStreamline.Controllers
{
    public class WireController : CommonController
    {
        //
        // GET: /Mill/
        Material oMaterial = new Material();


        Unit oUnit = new Unit();


        public WireController()
        {
            oDoaminObject = new Material();
        }

        protected override void Dispose(bool disposing)
        {
            oUnit.Dispose();
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Wire";
            return base.Index();
        }



        [HttpPost]
        public JsonResult getUnit(string pId = "")
        {
            List<gen_Unit> oListOfUnit = oUnit.Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
            var oUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.UnitName,
                Id = p.Pk_Unit
            });

            return Json(oUnitToDisplay);
        }


        [HttpPost]
        public JsonResult WireDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oGumToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oGumObjects = oGumToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oGumToDisplay = oGumObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oGumToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oMaterial = oDoaminObject as Material;
                oMaterial.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oMaterial.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference

                var oMaterialToDisplay = new
                {

                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name,
                    Brand = oAtualObject.Brand,
                    MaterialType = oAtualObject.MaterialType != null ? oAtualObject.MaterialType : "",
                    Fk_UnitId = oAtualObject.Fk_UnitId,
                    //Size = oAtualObject.Size,
                    //Description = oAtualObject.Description,
                    //PartNo = oAtualObject.PartNo,
                    Min_Value = oAtualObject.Min_Value,
                    Max_Value = oAtualObject.Max_Value

                };

                return Json(new { success = true, data = oMaterialToDisplay });
            }
            else
            {
                var oMaterialToDisplay = new Inv_Material();
                return Json(new { success = true, data = oMaterialToDisplay });
            }
        }

        [HttpPost]
        public JsonResult WireListByFiter(string Name = "", string Brand = "", string MaterialType = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Brand", Brand));
                oSearchParams.Add(new SearchParameter("MaterialType", MaterialType));
                oSearchParams.Add(new SearchParameter("Category", "Wire"));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oMaterialToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oMaterialObjects = oMaterialToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oMaterialToDisplay = oMaterialObjects.Select(p => new
                {

                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Unit = p.gen_Unit.UnitName,
                    Brand = p.Brand,
                    MaterialType = p.MaterialType


                }).ToList().OrderBy(s => s.Name);

                return Json(new { Result = "OK", Records = oMaterialToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

    }
}

