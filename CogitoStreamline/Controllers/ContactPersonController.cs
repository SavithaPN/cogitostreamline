﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;

namespace CogitoStreamline.Controllers
{
    public class ContactPersonController : CommonController
    {
        //
        // GET: /ContactPerson/


        ContactPerson oContactPerson = new ContactPerson();
        State oState = new State();
        City oCity = new City();

        public ContactPersonController()
        {
            oDoaminObject = new ContactPerson();
        }

        protected override void Dispose(bool disposing)
        {
            oState.Dispose();
            oCity.Dispose();
            oContactPerson.Dispose();
            base.Dispose(disposing);
        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Contact Person";
            return base.Index();
        }

        [HttpPost]
        public JsonResult getState(string pId = "")
        {
            List<gen_State> oListOfState = oState.Search(null).ListOfRecords.OfType<gen_State>().ToList();
            
            var ostateToDisplay = oListOfState.Select(p => new
            {
                Name = p.StateName,
                Id = p.Pk_StateID
            });

            return Json(ostateToDisplay);
        }


        [HttpPost]
        public JsonResult getCity(string pId = "")
        {
            List<gen_City> oListOfCity = oCity.Search(null).ListOfRecords.OfType<gen_City>().ToList();
            var oCityToDisplay = oListOfCity.Select(p => new
            {
                Name = p.CityName,
                Id = p.Pk_CityID
            });

            return Json(oCityToDisplay);
        }

                
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                ContactPerson oContactPerson = oDoaminObject as ContactPerson;
                oContactPerson.ID = decimal.Parse(pId);
                gen_ContactPerson oAtualObject = oContactPerson.DAO as gen_ContactPerson;
                //Create a anominious object here to break the circular reference
                var oContactPersonToDisplay = new
                {
                    Pk_ContactPersonId = oAtualObject.Pk_ContactPersonId,
                    ContactPersonName = oAtualObject.ContactPersonName,
                    ContactPersonEmailId = oAtualObject.ContactPersonEmailId,
                    ContactPersonDesignation = oAtualObject.ContactPersonDesignation,
                    Address_No_Street = oAtualObject.Address_No_Street,
                    Address_StateName = oAtualObject.gen_State.StateName,
                    Address_CityName = oAtualObject.gen_City.CityName,
                    Address_State = oAtualObject.Address_State,
                    Address_City = oAtualObject.Address_City,
                    Address_PinCode = oAtualObject.Address_PinCode,
                    ContactPersonNumber = oAtualObject.ContactPersonNumber
                   
                };

                return Json(new { success = true, data = oContactPersonToDisplay });
            }
            else
            {
                var oContactPersonToDisplay = new gen_ContactPerson();
                return Json(new { success = true, data = oContactPersonToDisplay });
            }
        }

        [HttpPost]
        public JsonResult ContactPersonListByFiter(string ContactPersonName = null, string Mobile = null, int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("ContactPersonName", ContactPersonName));

                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oContactPersonsToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_ContactPerson> oContactPersonObjects = oContactPersonsToDisplayObjects.Select(p => p).OfType<gen_ContactPerson>().ToList();

                //Create a anominious object here to break the circular reference
                var oContactPersonsToDisplay = oContactPersonObjects.Select(p => new
                {
                    Pk_ContactPersonId = p.Pk_ContactPersonId,
                    ContactPersonName = p.ContactPersonName,
                    ContactPersonEmailId = p.ContactPersonEmailId,
                    ContactPersonDesignation = p.ContactPersonDesignation,
                    Address_No_Street=p.Address_No_Street,
                    Address_State=p.Address_State,
                    Address_City=p.Address_City,
                    Address_PinCode=p.Address_PinCode,
                    ContactPersonNumber = p.ContactPersonNumber
                  
                }).ToList();

                return Json(new { Result = "OK", Records = oContactPersonsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

                       








    }
}

