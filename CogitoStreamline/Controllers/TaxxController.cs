﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace CogitoStreamline.Controllers
//{
//    public class TaxxController : Controller
//    {
//        //
//        // GET: /Tax/

//        public ActionResult Index()
//        {
//            return View();
//        }

//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class TaxxController : CommonController
    {
        Taxx oTax = new Taxx();
        public TaxxController()
        {
            oDoaminObject = new Taxx();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Tax";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oTax.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Taxx oTax = oDoaminObject as Taxx;
                oTax.ID = decimal.Parse(pId);
                Tax oAtualObject = oTax.DAO as Tax;
                //Create a anominious object here to break the circular reference
                var oTaxToDisplay = new
                {
                    PkTax = oAtualObject.PkTax,
                    TaxName = oAtualObject.TaxName,
                    TaxValue = oAtualObject.TaxValue
                };
                return Json(new { success = true, data = oTaxToDisplay });
            }
            else
            {
                var oTaxToDisplay = new Tax();
                return Json(new { success = true, data = oTaxToDisplay });
            }
        }
        [HttpPost]
        public JsonResult TaxDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sTaxName = oValues["TaxName"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("TaxName", sTaxName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oTaxToDisplayObjects = oSearchResult.ListOfRecords;
                List<Tax> oTaxObjects = oTaxToDisplayObjects.Select(p => p).OfType<Tax>().ToList();
                var oTaxToDisplay = oTaxObjects.Select(p => new
                {
                    PkTax = p.PkTax,
                    TaxName = p.TaxName
                }).ToList().OrderBy(s => s.TaxName);
                return Json(new { Success = true, Records = oTaxToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult TaxListByFiter(string PkTax = "", string TaxName = "", string TaxValue = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PkTax", PkTax));
                oSearchParams.Add(new SearchParameter("TaxName", TaxName));
                oSearchParams.Add(new SearchParameter("TaxValue", TaxValue));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oTaxToDisplayObjects = oSearchResult.ListOfRecords;
                List<Tax> oTaxObjects = oTaxToDisplayObjects.Select(p => p).OfType<Tax>().ToList();
                //Create a anominious object here to break the circular reference
                var oTaxToDisplay = oTaxObjects.Select(p => new
                {
                    PkTax = p.PkTax,
                    TaxName = p.TaxName,
                    TaxValue = p.TaxValue
                }).ToList().OrderBy(s=>s.TaxName);
                return Json(new { Result = "OK", Records = oTaxToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
