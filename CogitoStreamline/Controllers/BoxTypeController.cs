﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class BoxTypeController : CommonController
    {
        BoxTyp oItemPart = new BoxTyp();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public BoxTypeController()
        {
            oDoaminObject = new BoxTyp();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Box Type";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oItemPart.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                BoxTyp oItemPart = oDoaminObject as BoxTyp;
                oItemPart.ID = decimal.Parse(pId);
                BoxType oAtualObject = oItemPart.DAO as BoxType;
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = new
                {
                    Pk_BoxTypeID = oAtualObject.Pk_BoxTypeID,
                    Name = oAtualObject.Name
                  
                };
                return Json(new { success = true, data = oHeadToDisplay });
            }
            else
            {
                var oHeadToDisplay = new BoxType();
                return Json(new { success = true, data = oHeadToDisplay });
            }
        }
        [HttpPost]
        public JsonResult NameDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<BoxType> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxType>().ToList();
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxTypeID = p.Pk_BoxTypeID,
                    Name = p.Name
                }).ToList().OrderBy(s => s.Name);
                return Json(new { Success = true, Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult BoxTypeListByFiter(string Name = "", string Pk_BoxTypeID = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Pk_BoxTypeID", Pk_BoxTypeID));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oHeadToDisplayObjects = oSearchResult.ListOfRecords;
                List<BoxType> oHeadObjects = oHeadToDisplayObjects.Select(p => p).OfType<BoxType>().ToList();
                //Create a anominious object here to break the circular reference
                var oHeadToDisplay = oHeadObjects.Select(p => new
                {
                    Pk_BoxTypeID = p.Pk_BoxTypeID,
                    Name = p.Name

                }).ToList().OrderBy(s => s.Pk_BoxTypeID);
                return Json(new { Result = "OK", Records = oHeadToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

