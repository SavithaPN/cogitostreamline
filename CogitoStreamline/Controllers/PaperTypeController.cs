﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;
using System.IO;
namespace CogitoStreamLine.Controllers
{
    public class PaperTypeController : CommonController
    {
        PaperType oCountry = new PaperType();
        //Module oModule = new Module();
        //Role oRole = new Role();

        public PaperTypeController()
        {
            oDoaminObject = new PaperType();
        }
        public override ActionResult Index()
        {
            ViewBag.Header = "Paper Type";
            return base.Index();
        }
        protected override void Dispose(bool disposing)
        {
            oCountry.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                PaperType oPapertype = oDoaminObject as PaperType;
                oPapertype.ID = decimal.Parse(pId);
                gen_PaperType oAtualObject = oPapertype.DAO as gen_PaperType;
                //Create a anominious object here to break the circular reference
                var oPaperToDisplay = new
                {
                    Pk_PaperType = oAtualObject.Pk_PaperType,
                    PaperTypeName = oAtualObject.PaperTypeName,
                    PaperDescription = oAtualObject.PaperDescription
                };
                return Json(new { success = true, data = oPaperToDisplay });
            }
            else
            {
                var oPaperToDisplay = new gen_PaperType();
                return Json(new { success = true, data = oPaperToDisplay });
            }
        }
        [HttpPost]
        public JsonResult PaperDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sPaperTypeName = oValues["PaperTypeName"];
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PaperTypeName", sPaperTypeName));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_PaperType> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<gen_PaperType>().ToList();
                var oPaperToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_PaperType = p.Pk_PaperType,
                    PaperTypeName = p.PaperTypeName
                }).ToList().OrderBy(s => s.PaperTypeName);
                return Json(new { Success = true, Records = oPaperToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult PaperListByFiter(string PaperTypeName = "", string Pk_PaperType = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("PaperTypeName", PaperTypeName));
                oSearchParams.Add(new SearchParameter("Pk_PaperType", Pk_PaperType));
                oSearchParams.Add(new SearchParameter("StartIndex", jtStartIndex.ToString()));
                oSearchParams.Add(new SearchParameter("PageSize", jtPageSize.ToString()));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);
                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<gen_PaperType> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<gen_PaperType>().ToList();
                //Create a anominious object here to break the circular reference
                var oPaperToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_PaperType = p.Pk_PaperType,
                    PaperTypeName = p.PaperTypeName,
                      PaperDescription = p.PaperDescription
                }).ToList().OrderBy(s => s.PaperTypeName);
                return Json(new { Result = "OK", Records = oPaperToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}

