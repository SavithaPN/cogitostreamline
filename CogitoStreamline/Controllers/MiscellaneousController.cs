﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGareCore.CommonObjects;
using WebGareCore.DomainModel;
using System.Data.Objects.DataClasses;
using WebGareCore;
using TugberkUg.MVC.Helpers;
using Newtonsoft.Json;
using CogitoStreamLineModel.DomainModel;
using CogitoStreamLineModel;

namespace CogitoStreamline.Controllers
{
    public class MiscellaneousController : CommonController
    {
        /*In controller has no Paper object, but it will use the Materials object Inv_Material table to store Information */

        Material oMat = new Material();

        public MiscellaneousController()
        {
            oDoaminObject = new Material();

        }

        public override ActionResult Index()
        {
            ViewBag.Header = "Miscellaneous Items";

            return base.Index();
        }

        [HttpPost]
        public JsonResult getUnit(string pId = "")
        {
            List<gen_Unit> oListOfUnit = new Unit().Search(null).ListOfRecords.OfType<gen_Unit>().ToList();
            var oUnitToDisplay = oListOfUnit.Select(p => new
            {
                Name = p.UnitName,
                Id = p.Pk_Unit
            });

            return Json(oUnitToDisplay);
        }

        [HttpPost]
        public JsonResult MiscellaneousDuplicateChecking(string data = "")
        {
            try
            {
                Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
                string sName = oValues["Name"];

                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", sName));

                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                var oPaperToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name

                }).ToList().OrderBy(s => s.Name);

                return Json(new { Success = true, Records = oPaperToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }



        [HttpPost]
        public override JsonResult Load(string data = "")
        {
            /* Pick Paper relevat fields from Material and create a structure */
            Dictionary<string, string> oValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            string pId = oValues["Id"];
            if (pId != "")
            {
                Material oPaper = oDoaminObject as Material;
                oPaper.ID = decimal.Parse(pId);
                Inv_Material oAtualObject = oPaper.DAO as Inv_Material;
                //Create a anominious object here to break the circular reference
                var oPaperToDisplay = new
                {
                    Pk_Material = oAtualObject.Pk_Material,
                    Name = oAtualObject.Name,
                    Description = oAtualObject.Description,



                };

                return Json(new { success = true, data = oPaperToDisplay });
            }
            else
            {
                var oPaperToDisplay = new Material();
                return Json(new { success = true, data = oPaperToDisplay });
            }
        }


        [HttpPost]
        public JsonResult MiscListByFiter(string Name = "", string Description = "", string Deckle = "", string Mill = "", string Fk_Color = "", string Brand = "", string BF = "", int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                List<SearchParameter> oSearchParams = new List<SearchParameter>();
                oSearchParams.Add(new SearchParameter("Name", Name));
                oSearchParams.Add(new SearchParameter("Description", Description));
                oSearchParams.Add(new SearchParameter("Category", "Misc"));
                SearchResult oSearchResult = oDoaminObject.Search(oSearchParams);

                List<EntityObject> oPaperToDisplayObjects = oSearchResult.ListOfRecords;
                List<Inv_Material> oPaperObjects = oPaperToDisplayObjects.Select(p => p).OfType<Inv_Material>().ToList();

                //Create a anominious object here to break the circular reference
                var oRawMaterialsToDisplay = oPaperObjects.Select(p => new
                {
                    Pk_Material = p.Pk_Material,
                    Name = p.Name,
                    Description = p.Description,

                }).ToList();

                return Json(new { Result = "OK", Records = oRawMaterialsToDisplay, TotalRecordCount = oSearchResult.RecordCount });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }






    }
}