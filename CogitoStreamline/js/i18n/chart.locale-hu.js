(function ($) {
   
    $.jqChartDateFormat = {
        dayNames: [
            "Va", "Hé", "Ke", "Sze", "Csü", "Pé", "Szo",
            "Vasárnap", "Hétfő", "Kedd", "Szerda", "Csütörtök", "Péntek", "Szombat"
        ],
        monthNames: [
            "Jan", "Feb", "Már", "Ápr", "Máj", "Jún", "Júl", "Aug", "Szep", "Okt", "Nov", "Dec",
            "Január", "Február", "Március", "Áprili", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"
        ],
        amPm: ["de", "du", "DE", "DU"],
        s: function (j) { return '.-ik'; },
        masks: {
            shortDate: "yyyy/d/m",
            shortTime: "tt h:MM",
            longTime: "tt h:MM:ss"
        }
    };
})(jQuery);
