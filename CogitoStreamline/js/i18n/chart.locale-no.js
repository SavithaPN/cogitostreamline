(function ($) {
   
    $.jqChartDateFormat = {
        dayNames: [
            "s�.", "ma.", "ti.", "on.", "to.", "fr.", "l�.",
            "S�ndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "L�rdag"
        ],
        monthNames: [
            "jan.", "feb.", "mars", "april", "mai", "juni", "juli", "aug.", "sep.", "okt.", "nov.", "des.",
            "januar", "februar", "mars", "april", "mai", "juni", "juli", "august", "september", "oktober", "november", "desember"
        ],
        amPm: ["", "", "", ""],
        s: function (b) { return "." },
        masks: {
            shortDate: "d.m.yyyy",
            shortTime: "HH:MM",
            longTime: "HH:MM:ss"
        }
    };
})(jQuery);
