(function ($) {
   
    $.jqChartDateFormat = {
        dayNames: [
            "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб",
            "Воскресение", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"
        ],
        monthNames: [
            "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек",
            "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
        ],
        amPm: ["am", "pm", "AM", "PM"],
        s: function (j) { return j < 11 || j > 13 ? ['st', 'nd', 'rd', 'th'][Math.min((j - 1) % 10, 3)] : 'th' },
        masks: {
            shortDate: "m.d.yyyy",
            shortTime: "H:MM",
            longTime: "H:MM:ss"
        }
    };
})(jQuery);
