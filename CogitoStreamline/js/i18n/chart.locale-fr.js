(function ($) {
  
    $.jqChartDateFormat = {
        dayNames: [
            "Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam",
            "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"
        ],
        monthNames: [
            "Jan", "Fév", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Déc",
            "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"
        ],
        amPm: ["am", "pm", "AM", "PM"],
        s: function (j) { return j == 1 ? 'er' : 'e'; },
        masks: {
            shortDate: "m/d/yyyy",
            shortTime: "h:MM TT",
            longTime: "h:MM:ss TT"
        }
    };
})(jQuery);
