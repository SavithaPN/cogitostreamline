(function ($) {
 
    $.jqChartDateFormat = {
        dayNames: [
            "Zo", "Ma", "Di", "Wo", "Do", "Vr", "Za",
            "Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"
        ],
        monthNames: [
            "Jan", "Feb", "Maa", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
            "Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "October", "November", "December"
        ],
        amPm: ["am", "pm", "AM", "PM"],
        s: function (b) {
            return b < 11 || b > 13 ? ["st", "nd", "rd", "th"][Math.min((b - 1) % 10, 3)] : "th"
        },
        masks: {
            shortDate: "m/d/yyyy",
            shortTime: "H:MM",
            longTime: "H:MM:ss"
        }
    };
})(jQuery);