﻿
var BoardID = 0;
var ply = 0;
function initialCRUDLoad() {
   
    $('#MainSearchContainer').jtable({
        title: 'Board List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Board/BoardListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width:'5%'
            }
            ,
            
            Name: {
                title: 'Name'
            },

            Length: {
                title: 'Cut Len'
            },
            Width: {
                title: 'Deckle'
            },
            //GSM: {
            //    title: 'GSM',
            //    width:'5%'
            //},
            PPly: {
                title: 'PLY',
                width: '5%'
            },
            //Weight: {
            //    title: 'Weight'
            //},
    //        ViewSpec: {
    //    title: 'B_Spec',
    //    width: '2%',
    //    listClass: 'child-opener-image-column',
    //    display: function (row) {
                
    //        var button1 = $("<button class='bnt'>View Specs</button>");
    //        button1.click(function (e) {
    //            fkbox = row.record.Pk_Material;
    //            ply = row.record.PPly;
    //            //TypeId=1;
    //            //_comLayer.parameters.add("fkbox", fkbox);
    //            //_comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
    //            window.open("/BoardDesc?BID=" + fkbox + "," + "Ply=" + ply);
    //            //window.open("/BoxSpec?BoxMaster=" + fkbox );
                        
    //        });
    //        return button1;
    //    }
            
    //},
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtBoardName').val(),
            MillName: $('#TxtMillName').val()
            
         
        });

    });

    $('#TxtBoardName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtBoardName').val(),
                MillName: $('#TxtMillName').val()
            });
        }

    });



    $('#TxtMillName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtBoardName').val(),
                MillName: $('#TxtMillName').val()
            });
        }

    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });



  
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Mill", "getMill", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Mill", "getMill", null);
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Color", "getColor", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Color", "getColor", null);
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);


}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Weight"] = $('#Weight').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Weight"] = $('#Weight').val();
}
function afterModelSaveEx(result) {
    //e.preventDefault();
    //alert(result.Message);
    //if (result.Message != null) {
    //    var bstr = result.Message;
    //    var lenb = bstr.length;
    //    var bstr1 = bstr.search("-");
    //    var bstr2 = bstr.substr(0, bstr1);
    //    var bstr3 = bstr.substr((bstr1+1),lenb);
    // //   var btypeval = bstr.search("=");
    //    //   fkbox1 = document.getElementById('hidPkBox').value;
    //    BoardID = bstr2;
    //    ply = bstr3;
    //}
    //else {
    //    //fkbox1 = document.getElementById('hidPkBox').value;

    //}
    ////window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);
  
    ////var BType = document.getElementById('Fk_BoxType').value;
    //window.open("/BoardDesc?BID=" + BoardID+ "," + "Ply=" + ply );
};
function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Board";

    $(".input-large").focusout(function () {
        try {
            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            var height = document.getElementById('Height').value;
            var BF = document.getElementById('BF').value;
            var GSM = document.getElementById('GSM').value;
            var GSM2 = document.getElementById('GSM2').value;
            var PPly = document.getElementById('PPly').value;
            var FPly = document.getElementById('FPly').value;
            var cal1 = Number(length) + Number(width) + 50;
            cal1 = (Math.round(Number(cal1) / 100)) * 100;
            var cal2 = (Math.round(Number(height) / 100)) * 100;
            var cal3 = Number(FPly) * 1.5;
            var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(cal3) * Number(PPly) * Number(GSM2) * 8;
            document.getElementById('Weight').value = Number(weight) / 1000000;
        }
        catch (e) { }

    });





}
function afterEditShow(viewObject) {
    
    $(".input-large").focusout(function () {
        try {
            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            var height = document.getElementById('Height').value;
            var BF = document.getElementById('BF').value;
            var GSM = document.getElementById('GSM').value;
            var GSM2 = document.getElementById('GSM2').value;
            var PPly = document.getElementById('PPly').value;
            var FPly = document.getElementById('FPly').value;
            var cal1 = Number(length) + Number(width) + 50;
            cal1 = (Math.round(Number(cal1) / 100)) * 100;
            var cal2 = (Math.round(Number(height) / 100)) * 100;
            var cal3 = Number(FPly) * 1.5;
            var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(cal3) * Number(PPly) * Number(GSM2) * 8;
            document.getElementById('Weight').value = Number(weight) / 1000000;
        }
        catch (e) { }
    });
}
function CheckBoardDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("Board/BoardDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Board Name Already added";

}
