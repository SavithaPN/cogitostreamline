﻿$.fn.estBoxChild = function (options) {

    this.options = $.extend({
        viewTag: "",
        title: "",
        id: "",
        outershell: false,
        cap: false,
        rate: false,
        model: new part(),
        loadFromModel: false
    }, options);

    this.model = this.options.model;

    _comLayer.parameters.clear();

    if (this.options.outershell) {
        _comLayer.parameters.add("ViewName", "_OuterShell");

    }
    else if (this.options.cap) {
        _comLayer.parameters.add("ViewName", "_CapPart");
    }
    else if (this.options.rate) {
        _comLayer.parameters.add("ViewName", "_Rate");
    }
    else {
        _comLayer.parameters.add("ViewName", "_GenericPart");
    }
    var sPath = _comLayer.buildURL(this.options.viewTag, "LoadViewLayout");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    if (oResult.success == true) {
        $(this).empty();
        $(this).html(oResult.data);
    }

    $(this).find('.partHeader').text(this.options.title);
    $(this).find('.mainPartContainer').attr("id", this.options.id);

    var that = this;

    this.getModel = function () {
        return this.model;
    }

    if (!this.options.rate) {
        $(this).find('.mainPartContainer').find(".smalltile").click(function () {

            var cntr = $($(this).parent()).parent();

            var ix = $(this).attr("value");

            displayLayers(cntr, ix, that);
        });
    }
    else {
        var cntr = $($(this).parent()[0]);

        ko.applyBindings(that.options.estimationModel, $(this).parent()[0]);

        that = this;

        $(cntr).find('.input-mini').change(function () {
            that.options.rateChanged(that.options.estimationModel);
        });
    }

    //check if are loading from model
    if (this.options.loadFromModel) {
        displayLayers($($(this).parent()[0]), this.model.layers.length, that);
    }

    return this;
}