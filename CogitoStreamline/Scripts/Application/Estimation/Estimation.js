﻿

//viewObject.viewModel.data["boxEstimationChilds"] = ko.observableArray();

function layer(gsm, bf, color, rate, weight, tk_factor,Rate) {
    this.id = null;
    this.gsm = ko.observable(gsm);
    this.bf = ko.observable(bf);
    this.color = ko.observable(color);
    this.rate = ko.observable(rate);
    this.weight = ko.observable(weight);
    this.tk_factor = ko.observable(tk_factor);
    
    this.rate = ko.observable(Rate);
    //this.Rate = ko.observable(Rate);        //for label display but final papercost will not be calculated
 
}
function part() {
    this.id = null;
    this.layers = Array();
    this.length = ko.observable(0);
    this.width = ko.observable(0);
    this.height = ko.observable(0);
    this.PName = ko.observable(0);
    this.boardArea = ko.observable(0);
    this.deckle = ko.observable(0);
    this.cuttingSize = ko.observable(0);
    this.quantity = ko.observable(1);
    this.noBoards = ko.observable(0);
    this.weight = ko.observable(0);
    this.rate = ko.observable(0);
    this.AddBLength = ko.observable(0)
    this.TakeUpFactor = ko.observable(0)
  

    this.addNewChild = function (sChildType) {
        if (sChildType == "layers") {
            var lyr = new layer(0, 0, "N", 0);
            this.addLayer(lyr);
            return lyr;
        }

        return null;
    }

    this.addLayer = function (layer) {
        this.layers.push(layer);
    }
}

function Estimation() {
    this.id = null;
    this.estID = null;
    this.plateYes = ko.observable();
    this.widthPartationYes = ko.observable();
    this.lenghtPartationYes = ko.observable();
    this.outerShellYes = ko.observable();
    this.capYes = ko.observable();
    this.outerShell = ko.observable();
    this.plate = ko.observable();
    this.widthPartation = ko.observable();
    this.lenghtPartation = ko.observable();
    this.cap = ko.observable();
    this.topval = ko.observable();
    this.bottval = ko.observable();
    this.sleeve = ko.observable();
    this.topYes = ko.observable();
    this.bottomYes = ko.observable();
    this.sleeveYes = ko.observable();
    this.parts = Array();
    this.Fk_Enquiry = null;
    this.convRate = ko.observable(0);
    this.convValue = ko.observable(0);
    this.gMarginPercentage = ko.observable(0);
    this.gMarginValue = ko.observable(0);
    this.taxesPercntage = ko.observable(0);
    this.taxesValue = ko.observable(0);
    this.VATPercentage = ko.observable(0);
    this.VATValue = ko.observable(0);
    this.CSTPercentage = ko.observable(0);
    this.CSTValue = ko.observable(0);
    this.transportValue = ko.observable(0);
    this.weightHValue = ko.observable(0);
    this.handlingChanrgesValue = ko.observable(0);
    this.packingChargesValue = ko.observable(0);
    this.rejectionPercentage = ko.observable(0);
    this.rejectionValue = ko.observable(0);
    this.totalWeight = ko.observable(0);
    this.totRate = ko.observable(0);
    this.totalPrice = ko.observable(0);
    this.VATPercentage = ko.observable(0);
    this.CSTPercentage = ko.observable(0);
    this.TransportPercentage = ko.observable(0);
    this.totalAddPrice = ko.observable(0);
    this.partsCopy = null;
    //this.EnqChild=ko.observable(0);



    this.addParts = function (part) {
        this.parts.push(part);
    };

    this.addNewChild = function (sChildType) {
        if (sChildType == "parts") {
            var prt = new part();
            this.addParts(prt);
            return prt;
        }

        return null;
    }

    this.save = function () {

        var ToPass = ko.toJSON(this);
        var sPath = "";
        var pass = false;

        if (document.getElementById('ORate').value > 0 && document.getElementById('ConvRate').value > 0) {

            //if ($("#frmHost").validationEngine('validate')) {
            _util.showMessageDialog("", false);
            //this.estID = 29;
            //alert(estID);

            //alert(this.modelName);


            if (this.estID == null) {
                sPath = _comLayer.buildURL(this.modelName, "FlexEst", "Save");
            }
            else { sPath = _comLayer.buildURL(this.modelName, "FlexEst", "Update"); }

            _comLayer.parameters.clear();
            _comLayer.parameters.setJSON(ToPass);

            var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

            if (oResult.Success) {
                //_util.showSuccess("Data Saved successfully", function () { document.location.reload(true); });
                _util.showSuccess("Data Saved successfully", function () { document.location.href = "/Estimation"; });
                pass = true;

            }
            else {
                _util.showError("Unable save please contact administrator", function () { });
            }

        }
        if (pass == false) {
            alert("Rate/Conv Rate Cannot be Blank");
        }

    };




    this.deleteRecord = function () {
        if (_util.fireSubscribedHooks(this.hookSubscribers, 'beforeModelDelete', { Name: this.modelName, Tag: this.modelTag })) {
            var ToPass = ko.toJSON(this.data);
            var sPath = "";

            //if (this.id != null) {
            if (this.ID != null) {
                sPath = _comLayer.buildURL(this.modelName, this.modelTag, "Delete");
                _comLayer.parameters.clear();
                _comLayer.parameters.setJSON(ToPass);

                var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

                _util.fireSubscribedHooks(this.hookSubscribers, 'afterModelDelete', { result: oResult });
            }

        };
    };

    this.fetch = function () {
        if (this.Fk_BoxID == null || this.Fk_BoxID == "") return;
        var oResult = this._getDataFromSource(null);

        if (oResult.success == true) {
            _util.deepObjectCopy(oResult.data, this);
            return true;
        }

        return false;
    };

    this._getDataFromSource = function (filters) {
        //if filters are set just go with the filters
        if (filters != null) {
            _comLayer.parameters = filters;
        }
        else {
            _comLayer.parameters.clear();
            _comLayer.parameters.add("BoxId", this.Fk_BoxID);
            _comLayer.parameters.add("EnqChild", this.EnqChild);
            _comLayer.parameters.add("BT", this.BT);
            _comLayer.parameters.add("BoxEstimation", this.BoxEstimation);
            _comLayer.parameters.add("BoxEstimationChild", this.BoxEstimationChild);
        }

        var sPath = _comLayer.buildURL(this.modelName, "BoxSpec", "LoadBySpecID");
        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);




        var oResultObject = oResult;

        return oResultObject;

    };


}