﻿


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Color List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Color/ColorListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_Color: {
                title: 'Id',
                key: true,
            },
            ColorName: {
                title: 'Name'
            }
           
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Color: $('#TxtPkColor').val(),
            ColorName: $('#TxtColorName').val()
         });

    });
    $('#TxtPkColor').keypress(function (e) {
        if (e.KeyCode==13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Color: $('#TxtPkColor').val(),
                ColorName: $('#TxtColorName').val()
            });
        }

    });

    $('#TxtColorName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Color: $('#TxtPkColor').val(),
                ColorName: $('#TxtColorName').val()
            });
        }

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
   }


function CheckColorDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("ColorName", $('#ColorName').val());
    oResult = _comLayer.executeSyncAction("Color/ColorDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Color Name Already added";

}


