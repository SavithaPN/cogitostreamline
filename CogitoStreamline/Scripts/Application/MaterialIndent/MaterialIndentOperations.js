﻿var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var gMaterials = "";
var VendorID = "";
var OrdID = "";
var MatID = "";
var checkedVal;

function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Material Indent List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialIndent/MaterialIndentListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true,
                width: '2%'
            },
            MaterialIndentDate: {
                title: 'Indent Date',
                width: '3%'
            },
            StateName: {
                title: 'Status',
                width: '2%'
            },
            Comments: {
                title: 'Reasons',
                width: '2%'
            },
            Select_A: {
                title: "Close Indent Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Close Indent</button>");
                    $(button1).click(function () {
                        MatInd = row.record.Pk_MaterialOrderMasterId;
                        _comLayer.parameters.add("MatID", MatInd);
                        oResult = _comLayer.executeSyncAction("MaterialIndent/StatusUpdate", _comLayer.parameters);
                        if (oResult = true)
                        { alert("Indent Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Select_B: {
                title: "Open Indent Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Open Indent</button>");
                    $(button1).click(function () {
                        MatInd = row.record.Pk_MaterialOrderMasterId;
                        _comLayer.parameters.add("MatID", MatInd);
                        oResult = _comLayer.executeSyncAction("MaterialIndent/OpenStatus", _comLayer.parameters);
                        if (oResult = true)
                        { alert("Indent Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_MID = row.record.Pk_MaterialOrderMasterId;
                        _comLayer.parameters.add("Pk_MID", Pk_MID);
                        _comLayer.executeSyncAction("MaterialIndent/IndentRep", _comLayer.parameters);
                        var strval = "ConvertPDF/MIndent" + Pk_MID + ".pdf"

                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/MIndent" + Pk_MID + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');

                            return true;
                        }


                        /////////////////////////


                    });
                    return button;
                }
            },

            ////////////////////////////////////////////////

            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Indent List',
                                        actions: {
                                            listAction: '/MaterialIndent/IndentGetRec?Pk_MaterialIndent=' + data.record.Pk_MaterialOrderMasterId

                                        },
                                        fields: {

                                            //Customer: {
                                            //    title: 'Customer'
                                            //},

                                            PoNo: {
                                                title: 'PoNo.'
                                            },
                                            //VendorName: {
                                            //    title: 'VendorName'
                                            //},


                                            //MillName: {
                                            //    title: 'MillName'
                                            //},

                                            MaterialName: {
                                                title: 'MaterialName'
                                            },
                                            Color: {
                                                title: 'Shade'
                                            },
                                            Quantity: {
                                                title: 'Quantity',
                                                key: true,
                                            },


                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },


            //////////////////////////////////////////////////
        }
    });


    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_MaterialIndent: $('#IndentNumber').val(),
            Vendor: $('#VendorName').val(),
            Branch: $('#BranchName').val(),
            FromIndentDate: $('#TxtFromDate').val(),
            ToIndentDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Branch", "getBranch", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Branch", "getBranch", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("MatCat", "getMatCat", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("MatCat", "getMatCat", null);

    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });
    $('#dtIndentDate').datepicker({
        autoclose: true
    });


    //curViewModel = viewObject.viewModel;

    $('#divDeliveryschedule').jtable({
        title: 'Job Card Details--3',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/JobAssignedStock',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Pk_JobCardID: {
                title: 'JCard No',
                list: true
            },
            JDate: {
                title: 'JCard Date',
                key: false
            },
            Customer: {
                title: 'Customer',
                list: true
            },
            MName: {
                title: 'Material',
                list: true

            },
            BoxName: {
                title: 'box',
                list: true
            },

            Pk_JobCardID: {
                title: 'JobCardID',
            },

            Quantity: {
                title: 'Quantity'

            }
        }
    });
    setUpVendorSearch();
}
function resetOneToManyForm(property) {
    $("#dtRequiredDate").val("");
    $("#txtMaterial").val("");
    $("#txtCustomerOrder").val("");
    $("#Quantity").val("");
    $("#txtMill").val("");
    $("#txtVendor").val("");

}
function afterNewShow(viewObject) {

    //bEditContext = false;
  
    //document.getElementById('Pk_MaterialOrderMasterId').value = Number($('#Pk_MaterialOrderMasterId').val()) + 1;
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('dtPIndentDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#dtPIndentDate').datepicker({ autoclose: true });

    //curViewModel = viewObject.viewModel;

    $('#divMaterialDetail').jtable({
        title: 'Material Indent',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialIndent/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            RequiredDate: {
                title: 'Date',
                key: false
            },
            Fk_Material: {
                title: 'Material',
                list: false
            },
            txtFk_Material: {
                title: 'Material Name'

            },
            Fk_CustomerOrder: {
                title: 'Cust.Order'

            },
            //Fk_Vendor: {
            //    title: 'Vendor',
            //    list: false

            //},
            //Fk_Mill: {
            //    title: 'Mill',
            //    list: false

            //},
            //txtFk_Vendor: {
            //    title: 'V Name',
            //},
            //txtFk_Mill: {
            //    title: 'Mill Name',

            //},
            SQty: {
                title: 'Stk.Qty'

            },
            RQty: {
                title: 'Req.Qty'

            },
            Quantity: {
                title: 'Indented Qty'

            },
            
        }
    });
    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");


    $('#cmdCreate').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
        //$(this).tab('show')
        //alert($('.nav-tabs .active').text());
        if ($(this).attr("href") == "#tab1") {
            //$('input:submit').show();
            $('#cmdCreate').hide();
        }
        else if 
            ($(this).attr("href") == "#tab2") {
            $('#cmdCreate').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
    })



    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();

        $("#searchDialog").modal("show");
    });

    //if (document.getElementById('Paper').checked == true) {
    //    alert("You have selected paper");
    //} 
    
    
    //var chx = document.getElementsByTagName('Others')
    //if (chx.type == 'radio' && chx.checked) {
    //    return true;
    //    alert("You have selected others");
    //}

}

function afterOneToManyDialogShow() {
    //$('#dtRequiredDate').datepicker({
    //    autoclose:true
    //});

    //var dNow = new Date();
    //document.getElementById('dtRequiredDate').value = dNow.getDate() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getFullYear();
    $('#dtRequiredDate').datepicker({ autoclose: true });


    $("#cmdMaterialSearch").click(function (e) {
        e.preventDefault();
        _util.displayView("MaterialIndent", "_AddMaterialIndent", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").modal("show");


        $("#dataDialog").width(1000);
        $("#dataDialog").css("top", "50px");
        $("#dataDialog").css("left", "300px");


        setUpMaterialSearch();
        //$("#searchMaterialDialog").modal("show");

    });


    $("#cmdVendorSearch").click(function (e) {
        e.preventDefault();
        //   _util.displayView("MaterialIndent", "_AddMaterialIndent", "dataDialogArea");
        //    $('#dataDialogHeading').text("Search");
        $("#searchDialog").modal("show");
        setUpVendorSearch();
        //$("#searchMaterialDialog").modal("show");

    });


    $("#cmdVendorMSearch").click(function (e) {
        e.preventDefault();
        _util.displayView("MaterialIndent", "_AddVendor", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").modal("show");
        setUpVendorMSearch();

    });

    $("#cmdMillMSearch").click(function (e) {
        e.preventDefault();
        _util.displayView("MaterialIndent", "_AddMill", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").modal("show");
        setUpMillSearch();

    });

    $('#cmdCustomerOrderSearch').click(function (e) {
        e.preventDefault();
        _util.displayView("MaterialIndent", "_AddCustomerOrder", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        //$("#dataDialog").left(80);
        $("#dataDialog").width(1000);
        $("#dataDialog").modal("show");
        setUpCustomerSearch();
        //$("#searchCustomerDialog").modal("show");

    });



}

function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }


    if ($("#dtRequiredDate").val() == "") {
        $("#dtRequiredDate").validationEngine('showPrompt', 'Enter Required Date', 'error', true)
        bValidation = false;
    }

    if ($("#txtMaterial").val() == "") {
        $("#txtMaterial").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

function showOnlyPending() {
    $('#MainSearchContainer').jtable('load', {
        CustomerName: $('#CustomerName').val(),
        ProductName: $('#ProductName').val(),
        OnlyPending: document.getElementById('chkonlyPending').checked
    });
}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;


    $('#dtPIndentDate').datepicker({
        autoclose: true
    });

    $('#cmdCreate').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
        //$(this).tab('show')
        //alert($('.nav-tabs .active').text());
        if ($(this).attr("href") == "#tab1") {
            //$('input:submit').show();
            $('#cmdCreate').hide();
        }
        else if
            ($(this).attr("href") == "#tab2") {
            $('#cmdCreate').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
    })

    $('#divMaterialDetail').jtable({
        title: 'Material Indent',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialIndent/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            RequiredDate: {
                title: 'Date',
                key: false
            },
            Fk_Material: {
                title: 'Material',
                list: false
            },
            txtFk_Material: {
                title: 'Material Name'

            },
            Fk_CustomerOrder: {
                title: 'Cust.Order'

            },
            //Fk_Vendor: {
            //    title: 'Vendor'

            //},
            //Fk_Mill: {
            //    title: 'Mill'

            //},
            //SQty: {
            //    title: 'Stk.Qty'

            //},
            RQty: {
                title: 'Req.Qty'

            },
            Quantity: {
                title: 'Indented Qty'

            }
        }
    });


    var oSCuts = viewObject.viewModel.data.MaterialIndentDetails();
    viewObject.viewModel.data["IndentDetails"] = ko.observableArray();
    var i = 0;
    //Unit, SQty, RQty, Fk_Vendor, Fk_Mill, txtFk_Mill, txtFk_Vendor
    while (oSCuts[i]) {
        var oCut = new MaterialIndent();
        //oCut.load(oSCuts[i].Pk_MaterialOrderDetailsId, oSCuts[i].RequiredDate, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].Fk_CustomerOrder, oSCuts[i].Quantity, oSCuts[i].Unit, oSCuts[i].SQty, oSCuts[i].RQty, oSCuts[i].Fk_Vendor, oSCuts[i].Fk_Mill, oSCuts[i].txtFk_Mill, oSCuts[i].txtFk_Vendor);
        oCut.load(oSCuts[i].Pk_MaterialOrderDetailsId, oSCuts[i].RequiredDate, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].Fk_CustomerOrder, oSCuts[i].Quantity, oSCuts[i].Unit, oSCuts[i].SQty, oSCuts[i].RQty);
        viewObject.viewModel.data["IndentDetails"].push(oCut);
        i++;
    }

    //  configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "_AddMaterialIndent", function () { return new MaterialIndent() });
    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");

    bEditContext = true;

    $('#wfTransDisplay').wgWorkFlowHistory({
        wgTag: "Order",
        viewModel: viewObject.viewModel
    });

    $('#wfButtons').wgWorkFlowButtons({
        wgTag: "MaterialIndent",
        viewModel: viewObject.viewModel,
        stateField: "Fk_Status",
        belongsTo: "MaterialIndent",
        pkField: "Pk_MaterialOrderMasterId"
    });
}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {

    $('#txtFk_VendorId').wgReferenceField({
        keyProperty: "Fk_VendorId",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewObject.viewModel
    });
    cleanSearchDialog();
}


function setUpMaterialSearch() {
    //Branch
    OrdID = document.getElementById('txtCustomerOrder').value;
    //alert( document.getElementById('Others').value)
    //cleanSearchDialog();

    $("#srchMHeader").text("Material Search");

    //Adding Search fields  "modelControl validate[required,funcCall[checkDuplicate]]"
    var txtFieldGSM = "<input type='text' id='txtdlgGSM' placeholder='GSM' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldBF = "<input type='text' id='txtdlgBF' placeholder='BF' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldD = "<input type='text' id='txtdlgD' placeholder='Deckle' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldMill = "<input type='text' id='txtdlgMill' placeholder='Others-Name' class='input-large search-query'/>&nbsp;&nbsp;";

   // $("#dlgMaterialSearchFields").append(txtFieldMaterialName);
    $("#dlgMaterialSearchFields").append(txtFieldGSM);
    $("#dlgMaterialSearchFields").append(txtFieldBF);
    $("#dlgMaterialSearchFields").append(txtFieldD);
    $("#dlgMaterialSearchFields").append(txtFieldMill);

    //gMaterials = "Paper";
    //if (checkedVal == "Others")
    //    gMaterials = "Others";
    //    else
    //{
    //    gMaterials = "Paper";
    //}
    if (OrdID == '') {
        $('#MaterialSearchContainer').empty();
        $('#MaterialSearchContainer').jtable({
            title: 'Material List',
            paging: true,
            pageSize: 8,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/MaterialCategory/MaterialIndentSearch?MaterialCategory=' + gMaterials
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdMatDone').click();
                });
            },

            fields: {
                Pk_Material: {
                    title: 'Material Id',
                    key: true
                },
                Name: {
                    title: 'Material Name',
                    edit: false
                },
                //Quantity: {
                //    title: 'Ex.Stk.Qty',
                //    edit: false
                //},
                Color: {
                    title: 'Shade'
                },
                Category: {
                    title: 'Cat.'
                },
                Desc: {
                    title: 'Desc',
                    list: true
                },
                Mill: {
                    title: 'Mill',
                    list: true
                },
                Pk_mill: {
                    title: 'Mill',
                    list:false
                },
                UnitName: {
                    title: 'UnitName',
                    list: true
                },
            }
        });
    }
    else {

        $('#MaterialSearchContainer').empty();
        $('#MaterialSearchContainer').jtable({
            title: 'Material List',
            paging: true,
            pageSize: 8,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/Order/OrderIndentDetails?Pk_Order=' + OrdID
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdMatDone').click();
                });
            },

            fields: {
                Pk_Material: {
                    title: 'PkMaterial',
                    key: true,
                    list: false
                },
                BName: {
                    title: 'Box Name',
                    key: false
                },
                MName: {
                    title: 'M.Name',
                    edit: false
                },
                OrdQty: {
                    title: 'OrdQty',
                    edit: false
                },
                PaperReq: {
                    title: 'Paper Req.'
                },
                Color: {
                    title: 'Shade'
                },
                Mill: {
                    title: 'Mill'
                },

            }
        });

    }
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();
       
        //if (checkedVal == "Others")

        $('#MaterialSearchContainer').jtable('load', {
            //MaterialName: $('#txtdlgMaterialName').val(),
            GSM: $('#txtdlgGSM').val(),
            BF: $('#txtdlgBF').val(),
            Deckle: $('#txtdlgD').val(),
            MaterialName: $('#txtdlgMill').val(),
            MaterialCategory: $('#Selected').val(),
        });
    });



    $('#cmdMatDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $("#dataDialog").modal("hide");
        $('#txtMaterial').wgReferenceField("setData", rows[0].keyValue);

        MatID = rows[0].data.Pk_Material;
        gMaterials = rows[0].data.Pk_Material;
        var RQty = rows[0].data.PaperReq;

        //Shade.style.visibility = 'visible';
        //document.getElementById('Shade').innerHTML = 
        _comLayer.parameters.add("Pk_Material", MatID);
        oResult1 = _comLayer.executeSyncAction("MaterialIndent/FindStock", _comLayer.parameters);
        document.getElementById('SQty').value = oResult1;
        document.getElementById('RQty').value = RQty;
      
        
        document.getElementById('Shade').value = rows[0].data.Color;
        //document.getElementById('txtMill').value = rows[0].data.Mill;
        //document.getElementById('Fk_Mill').value = rows[0].data.Pk_mill;
        
        var StockQty = document.getElementById('SQty').value;
        var ReqQty = document.getElementById('RQty').value;

        if (Number(ReqQty) > Number(StockQty)) {
            document.getElementById('ShrtQty').type = 'text';
            document.getElementById('ShrtQty').value = Number(ReqQty) - Number(StockQty);
            Qty.style.visibility = 'visible';
            document.getElementById('Qty').innerHTML = 'Shortage!';
        }
        else if (Number(StockQty) > Number(ReqQty)) {

            document.getElementById('ShrtQty').type = 'text';
            document.getElementById('ShrtQty').value = Number(StockQty) - Number(ReqQty);
            Qty.style.visibility = 'visible';
            document.getElementById('Qty').innerHTML = 'Excess!';
        }
        else {
            document.getElementById('ShrtQty').type = 'hidden';
        }
        oResult1 = _comLayer.executeSyncAction("MaterialIndent/FindPendingPO", _comLayer.parameters);

        document.getElementById('POQty').value = oResult1;
        var PQty = document.getElementById('POQty').value;
        if (PQty > 0) {
            fillPODet();
        }
        checkDuplicate();

        ////document.getElementById('Unit').value = rows[0].data.UnitName;
        //document.getElementById('Unit').value = "Kgs"
        //document.getElementById('SQty').value = rows[0].data.PaperStkQty;

        //if (rows[0].data.PaperReq > 0)
        //{ var ReqQty = (rows[0].data.PaperReq / 1000) }


        cleanSearchDialog();
        FillJobCardAssignedStockList();
        //cleanSearchDialog();
        //$('#divDeliveryschedule').jtable('load', {
        //    Pk_Material: MatID
        //});
        $("#txtvendor").focus();
    });
}



function fillPODet() {
    var MatName = document.getElementById('txtMaterial').value;
    $('#divPODet').jtable({
        title: 'Pending PO Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialIndent/POPendingList?MaterialName=' + MatName,
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Pk_PONo: {
                title: 'PO No',
                list: true
            },
            PODate: {
                title: 'PO Date',
                key: false
            },
            VendorName: {
                title: 'Vendor',
                list: true,
                width: '5%'
            },
            Mat_Name: {
                title: 'Material',
                list: true,
                width: '5%'

            },
            PQuantity: {
                title: 'PQuantity',
                list: true
            },

            //Pk_JobCardID: {
            //    title: 'JobCardID',
            //    list: false
            //},

            //Quantity: {
            //    title: 'Assigned Qty'


            //},
            //StockQty: {
            //    title: 'Ex.Stk.Qty'

            //},
        }
    });
    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    $('#divPODet').jtable('load');

    //divPODet
}
function FillJobCardAssignedStockList() {
    cleanSearchDialog();
    var MatID = gMaterials;
    //$('#divDeliveryschedule').empty();

    if (MatID == '') {
        //$('#divDeliveryschedule').empty();

        $('#divDeliveryschedule').jtable({
            title: 'Job Card Details',
            paging: true,
            pageSize: 10,
            defaultSorting: 'Name ASC',
            actions: {
                listAction: '/JobCard/JobAssignedStock',
                //updateAction: ''
            },
            fields: {
                slno: {
                    title: 'slno',
                    key: true,
                    list: false
                },
                //Pk_JobCardID: {
                //    title: 'JCard No',
                //    list: true
                //},
                JDate: {
                    title: 'JCard Date',
                    key: false
                },
                Customer: {
                    title: 'Customer',
                    list: true
                },
                MName: {
                    title: 'Material',
                    list: true

                },
                BoxName: {
                    title: 'BoxName',
                    list: true
                },

                Pk_JobCardID: {
                    title: 'JobCardID',
                    list: true
                },

                Quantity: {
                    title: 'Assigned Qty'


                },
                //StockQty: {
                //    title: 'Ex.Stk.Qty'

                //},
            }
        });
        //$('#LoadRecordsButton').click(function (e) {
        //    e.preventDefault();
        $('#divDeliveryschedule').jtable('load');

        //});

        //$('#LoadRecordsButton').click();
    }
    else {
        var MatID = gMaterials;
        //alert("/JobCard/JobAssignedStock?Pk_Material=" + MatID);
        $('#divDeliveryschedule').jtable({
            title: 'Job Card Details',
            paging: true,
            pageSize: 10,
            defaultSorting: 'Name ASC',
            actions: {
                listAction: '/JobCard/JobAssignedStock?Pk_Material=' + MatID,

                //updateAction: ''
            },
            fields: {
                //slno: {
                //    title: 'slno',

                //    list: false
                //},
                Pk_JobCardID: {
                    title: 'JCNo',
                    width: '1%'
                },
                JDate: {
                    title: 'JC Date',
                    width: '1%'
                },
                Customer: {
                    title: 'Customer',
                    list: true,
                    width: '1%'
                },
                MName: {
                    title: 'Material',
                    list: true,
                    width: '2%'

                },

                BoxName: {
                    title: 'BoxName',
                    list: true,
                    width: '1%'
                },

                //Pk_JobCardID: {
                //    title: 'JobCardID',
                //    list:false
                //},

                Quantity: {
                    title: 'Ass.Qty.',
                    width: '1%'

                },
                //StockQty:                    {
                //        title: 'Ex.Stk.Qty',
                //        width: '1%'
                //    },
            }
        });
        //$('#LoadRecordsButton').click(function (e) {
        //    e.preventDefault();
        $('#divDeliveryschedule').jtable('load', {
            Pk_Material: MatID
        });
        //});

        //$('#LoadRecordsButton').click();
    }
}
function setUpCustomerSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchCHeader").text("Customer Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";


    $("#dlgCustomerSearchFields").append(txtFieldCustomerName);
    $("#dlgCustomerSearchFields").append(txtFieldMaterialName);
    $("#dlgCustomerSearchFields").append(txtFieldBoxName);

    $('#CustSearchContainer').jtable({
        title: 'Cust.Order List',
        paging: true,
        pageSize:5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderIndentDetails'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdCustDone').click();
            });
        },
        fields: {
            Pk_Order: {
                title: 'Ord.No',
                key: true,
                width: '2%'
            },
            PONo: {
                title: 'Cust.PONo',
                width: '2%'
            },
            OrderDate: {
                title: 'P.Ord.Dt',
                width: '2%'
            },
            CustomerName: {
                title: 'Cust.Name'
            },
            DelDate: {
                title: 'Del.Dt',
                width: '2%'
            },
            Pk_Material: {
                title: 'MaterialID',
                list: false
            },
            MName: {
                title: 'Mat.Name',
                width: '2%'
            },
            Pk_BoxID: {
                title: 'BoxID',
                list:false
            },
            BName: {
                title: 'BName',
                width: '2%'
            },
            OrdQty: {
                title: 'OrdQty',
                width: '2%'

            },
            PaperReq: {
                title: 'PaperReq',
                width: '2%'
            },
            Color: {
                title: 'Color',
                width: '2%'
            },
            Mill: {
                title: 'Mill',
                width: '2%'
            },
            MillID: {
                title: 'MillID',
                width: '2%',
                list:false

            },
                           
        }
    });
    $('#CustSearchContainer').jtable('load');
    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    //    $('#CustSearchContainer').jtable('load', {
    //        CustomerName: $('#txtdlgCustomerName').val()
    //    });
    //});

    //$('#LoadRecordsButton').click();


    $('#cmdCustSearch').click(function (e) {
        e.preventDefault();

        $('#CustSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
           MaterialName: $('#txtdlgMaterialName').val(),
            BoxName: $('#txtdlgBoxName').val(),
        });
    });

    $('#cmdCustDone').click(function (e) {
        e.preventDefault();
        var rows = $('#CustSearchContainer').jtable('selectedRows');
        //$("#dataDialog").modal("hide");
        $("#dataDialog").modal("hide");
        $('#txtCustomerOrder').wgReferenceField("setData", rows[0].keyValue);
        //$("#dtRequiredDate").focus();
        /////////////////////////

  

        MatID = rows[0].data.Pk_Material;
        gMaterials = rows[0].data.Pk_Material;
        var RQty = rows[0].data.PaperReq;
        $('#txtMaterial').wgReferenceField("setData", rows[0].data.Pk_Material);
        document.getElementById('dtDelDate').value = rows[0].data.DelDate;
        document.getElementById('Fk_Mill').value = rows[0].data.MillID;
        //Shade.style.visibility = 'visible';
        //document.getElementById('Shade').innerHTML = 
        _comLayer.parameters.add("Pk_Material", MatID);
        oResult1 = _comLayer.executeSyncAction("MaterialIndent/FindStock", _comLayer.parameters);
        document.getElementById('SQty').value = oResult1;
        document.getElementById('RQty').value = RQty;

        document.getElementById('Shade').value = rows[0].data.Color;
        document.getElementById('txtMill').value = rows[0].data.Mill;

        var StockQty = document.getElementById('SQty').value;
        var ReqQty = document.getElementById('RQty').value;

        if (Number(ReqQty) > Number(StockQty)) {
            document.getElementById('ShrtQty').type = 'text';
            document.getElementById('ShrtQty').value = Number(ReqQty) - Number(StockQty);
            Qty.style.visibility = 'visible';
            document.getElementById('Qty').innerHTML = 'Shortage!';
        }
        else if (Number(StockQty) > Number(ReqQty)) {

            document.getElementById('ShrtQty').type = 'text';
            document.getElementById('ShrtQty').value = Number(StockQty) - Number(ReqQty);
            Qty.style.visibility = 'visible';
            document.getElementById('Qty').innerHTML = 'Excess!';
        }
        else {
            document.getElementById('ShrtQty').type = 'hidden';
        }
        oResult1 = _comLayer.executeSyncAction("MaterialIndent/FindPendingPO", _comLayer.parameters);

        document.getElementById('POQty').value = oResult1;
        var PQty = document.getElementById('POQty').value;
        if (PQty > 0) {
            fillPODet();
        }
        checkDuplicate();

        //document.getElementById('Unit').value = rows[0].data.UnitName;
        document.getElementById('Unit').value = "Kgs"
        //document.getElementById('SQty').value = rows[0].data.PaperStkQty;

        //if (rows[0].data.PaperReq > 0)
        //{ var ReqQty = (rows[0].data.PaperReq / 1000) }


        cleanSearchDialog();
        FillJobCardAssignedStockList();
        //cleanSearchDialog();
        //$('#divDeliveryschedule').jtable('load', {
        //    Pk_Material: MatID
        //});
        $("#txtvendor").focus();

        ///////////////////////////
    });

}

function setUpVendorSearch() {
    //Enquiry

    //cleanSearchDialog();
    $("#dlgSearchFields").empty();
    $("#srchHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgVendorSearchFields").append(txtFieldVendorName);


    $('#MainSearhContainer').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MainSearhContainer').jtable('selectedRows');
        $("#searchDialog").modal("hide");
        //$('#txtFk_VendorId').wgReferenceField("setData", rows[0].keyValue);
        //VendorID = rows[0].keyValue;

    });

}


function setUpMillSearch() {
    //Enquiry

    //cleanSearchDialog();
    $("#dlgSearchFields").empty();
    $("#srchHeader").text("Mill Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgMillName' placeholder='Mill Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMillSearchFields").append(txtFieldVendorName);


    $('#MillSearchContainer').jtable({
        title: 'Mill List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Mill/MillListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMillDone').click();
            });
        },
        fields: {
            Pk_Mill: {
                title: 'Id',
                key: true,
                list: true,
                width: '5%'
            },
            MillName: {
                title: 'Name'
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MillSearchContainer').jtable('load', {
            MillName: $('#txtdlgMillName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMillSearch').click(function (e) {
        e.preventDefault();
        $('#MillSearchContainer').jtable('load', {
            MillName: $('#txtdlgMillName').val()
        });
    });

    $('#cmdMillDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MillSearchContainer').jtable('selectedRows');
        $("#dataDialog").modal("hide");
        $('#txtMill').wgReferenceField("setData", rows[0].keyValue);
        //$("#txtMaterial").focus();

    });

}



function setUpVendorMSearch() {
    //Enquiry

    //cleanSearchDialog();
    $("#dlgSearchFields").empty();
    $("#srchHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgVendorSearchFields").append(txtFieldVendorName);


    $('#VendorSearchContainer').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdVendorDone').click();
            });
        },
        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#VendorSearchContainer').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        $('#VendorSearchContainer').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdVendorDone').click(function (e) {
        e.preventDefault();
        var rows = $('#VendorSearchContainer').jtable('selectedRows');
        $("#dataDialog").modal("hide");
        $('#txtVendor').wgReferenceField("setData", rows[0].keyValue);
        //$("#txtMaterial").focus();

    });

}



function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["MaterialIndentDate"] = $('#dtPIndentDate').val();
    //viewModel.data["Fk_VendorId"] = $('#Fk_VendorId').val();
    //viewModel.data["Fk_VendorId"] = VendorID;
    //viewModel.data["Fk_Mill"] = $('#MillID').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["MaterialIndentDate"] = $('#dtPIndentDate').val();
    //viewModel1.data["Fk_VendorId"] = $('#Fk_VendorId').val();
    //viewModel1.data["Fk_VendorId"] = VendorID;
    //viewModel1.data["Fk_Mill"] = $('#MillID').val();
}





function ReferenceFieldNotInitilized(viewModel) {

    $('#txtVendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel

    });

    $('#txtCustomerOrder').wgReferenceField({
        keyProperty: "Pk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewModel
    });
    //$('#txtMill').wgReferenceField({
    //    keyProperty: "Fk_Mill",
    //    displayProperty: "MillName",
    //    loadPath: "Mill/Load",
    //    viewModel: viewModel
    //});
    //$('#txtFk_UserID').wgReferenceField({
    //    keyProperty: "Fk_UserID",
    //    displayProperty: "UserName",
    //    loadPath: "User/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        $('#txtMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_CustomerOrder"]);
        $('#txtFk_VendorId').wgReferenceField("setData", viewModel.data["Fk_VendorId"]);
        $('#txtVendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
        $('#txtMill').wgReferenceField("setData", viewModel.data["Fk_Mill"]);
    }
}

//function cleanSearchDialog() {

//    //Clean the search dialog of previous bindings and prepare for this selected dialog
//    $('#MainSearhContainer').empty();
//    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
//    $("#dlgSearchFields").empty();
//    $('#cmdSearch').off();
//    $('#cmdDone').off();
//}
function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();
    $('#divDeliveryschedule').empty();
    $('#CustSearchContainer').empty();
    $('#dlgCustomerSearchFields').empty();
    $('#dlgSearchFields').empty();

}



function orderDateValidate(field, rules, i, options) {
    if (bFromSave) {
        var isDateValid = _util.isDate(field.val());

        if (isDateValid.valid == false) {
            return "* " + isDateValid.Message;
        }

        if (!_util.dateGraterThanToday(field.val())) {
            return "* Order date cannot be greater than today";
        }
    }
    bFromSave = false;
}
function checkDuplicate() {
    //curViewModel = viewObject.viewModel;

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["IndentDetails"]()[i]) {
            //if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material
            if (MatID == curViewModel.data["IndentDetails"]()[i].data.Fk_Material) {
                alert("Item Already added");
                return "* " + "Item Already added";

            }
            i++;
        }
    }
}
function deliveryDateValidate(field, rules, i, options) {

    var isDateValid = _util.isDate(field.val());

    if (isDateValid.valid == false) {
        return "* " + isDateValid.Message;
    }

    if (field.val() == undefined || curViewModel.data["OrderDate"] == undefined) {
        return "* Please select the order date before adding the delivery schedule";
    }

    var orderDate = curViewModel.data["OrderDate"];

    try {
        orderDate = curViewModel.data["OrderDate"]();
    } catch (e) { }

    if (_util.dateGraterThan(field.val(), orderDate)) {
        return "* Delivery date cannot be less than order date";
    }

}
function showOnlyBoard() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Board').val()
    });
}
function showOnlyGum() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Gum').val()
    });
}
function showOnlyInk() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Ink').val()
    });
}

function showOnlyPaper() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Paper').val()
    });
    document.getElementById("Others").checked = false;
    document.getElementById("Paper").checked = true;
    document.getElementById("Selected").value = "Paper";
}

function showOnlyHoneycomb() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Honeycomb').val()
    });
}


function showOthers() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Others').val()
         
    });
    document.getElementById("Others").checked = true;
    document.getElementById("Paper").checked = false;
    document.getElementById("Selected").value = "Others";
   // checkedVal = "Others";
}



