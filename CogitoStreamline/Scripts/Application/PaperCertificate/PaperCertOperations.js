﻿/// <reference path="PaperCertOperations.js" />
var curViewModel = null;
var curEdit = false;
var GSMVal;
var CharID;
var RNo;
var BFVal;
var MatName;
var arrSubstance;
var arrBF;

//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Certification List',
        paging: true,
        pageSize: 8,
        actions: {
            listAction: 'PaperCertificate/PCertListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''

        },
        fields: {
            Pk_Id: {
                title: 'Cert.No',
                key: true,
                list: true
            },
            Name: {
                title: 'Mat.Name',
                width: '15%'
            },
            Invno: {
                title: 'Invno',
                width: '5%'
            },
            InvDate: {
                title: 'InvDate',
                width: '5%',
                list: true
            },
            PoNoDisplay: {
                title: 'PoNo',
                width: '8%'
            },
            VendorName: {
                title: 'VName',
                width: '25%'
            },

            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        JCNO = row.record.Pk_Id;
                        _comLayer.parameters.add("JCNO", JCNO);
                        _comLayer.executeSyncAction("PaperCertificate/PCert", _comLayer.parameters);
                        var strval = "ConvertPDF/PCertificate" + JCNO + ".pdf"

                        window.open(strval, '_blank ', 'width=700,height=250');
                        //return true;
                        ///////////////////////////
                    });
                    return button;
                }

            },

        },
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Id: $('#txtCust').val(),
            Name: $('#txtName').val(),
            VName: $('#txtVendorName').val(),
            PoNoDisplay: $('#txtPONo').val(),
            
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');


    });
 
    $('#cmdConsolidate').click(function (e) {
        e.preventDefault();

        var PONOVal = document.getElementById('txtPONo').value;
        if(PONOVal.length>0)
        {
            PoNO = PONOVal;
            _comLayer.parameters.add("PoNO", PoNO);
            _comLayer.executeSyncAction("PaperCertificate/PCert", _comLayer.parameters);
        }


    });

}

function afterNewShow(viewObject) {
        curViewModel = viewObject.viewModel;
        //var ss = $("#tlbMaterials").find("tr").length;
        //alert(ss);
    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Characteristic List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PaperCertificate/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {

            Pk_CharID: {
                title: 'Pk_CharID',
                key: true,
                list: false
            },
            ReelNo: {
                title: 'ReelNo',
                list: true
            },
            //TQty: {
            //    title: 'TQty',
            //    list: true
            //},
            Fk_Characteristics: {
                title: 'Char.Name',                
                list: true
            },
          
            TestMethod: {
                title: 'TestMethod'
            },
            Target: {
                title: 'Target',
                width: '1%'            
        },

        Acceptance: {
            title: 'Acceptance',
            width: '1%'
        },
        Result1: {
            title: 'R1',
            width: '1%'

        },
        Result2: {
            title: 'R2',
            width: '1%'

        },
        Result3: {
            title: 'R3',
            width: '1%'
        },
        Result4: {
            title: 'R4',
            width: '1%'
        },
        Result5: {
            title: 'R5',
            width: '1%'
        },
        Result6: {
            title: 'R6',
            width: '1%'
        },
        MinVal: {
            title: 'MinVal',
            width: '1%'
        },
        MaxVal: {
            title: 'MaxVal',
            width: '1%'
        },
       AvgVal: {
           title: 'AvgVal',
            width: '1%'
       },
       Remarks: {
           title: 'Remarks',
           width: '1%'
       },
       Status: {
           title: 'Status',
           width: '1%'
       },
        }
    });

   
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PaperCertificate", "_AddMaterial", function () { return new IssuedMaterial() });
    //var ss = $("#tlbMaterials").find("tr").length;
    //alert(ss);
  
    //Calc();
    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(900);
        $("#searchDialog").height(500);

    });

    $('#cmdMaterialMSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        $("#searchIndentDialog").modal("show");
      
        $("#searchIndentDialog").css("top", "100px");
        $("#searchIndentDialog").css("left", "350px");
    });
  
}

function afterOneToManyDialogShow(property) {

}
function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchHeader1").text("Paper Search");

    //Adding Search fields
    //var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    //$("#dlgMaterialSearchFields").append(txtFieldMaterialName);

    $('#IndentSearhContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            Name: {
                title: 'Material Name',
                edit: false
            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
            //Deckle: {
            //    title: 'Deckle',
            //    edit: false
            //},


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgIndent').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgIndent').val()
        });
    });



    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');



        $("#searchIndentDialog").modal("hide");
        $('#txtFkMaterial').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('txtGSM').value = rows[0].data.GSM;
        GSMVal = rows[0].data.GSM;
        BFVal = rows[0].data.BF;
        $("#searchIndentDialog").modal("hide");
     

    });
}


function Clear()
{
    document.getElementById('TestMethod').value = "";
    document.getElementById('Target').value = "";

    document.getElementById('Min').value = "";

    document.getElementById('Max').value = "";
    document.getElementById('MinVal').value = "";
    document.getElementById('MaxVal').value = "";
    document.getElementById('AvgVal').value = "";
    document.getElementById('Acceptance').value = "";
    document.getElementById('Result1').value = "";
    document.getElementById('Result2').value = "";
    document.getElementById('Result3').value = "";
    document.getElementById('Result4').value = "";
    document.getElementById('Result5').value = "";
    document.getElementById('Result6').value = "";
    document.getElementById('Remarks').value = "";
    document.getElementById('Status').value = "";
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["InvDate"] = $('#dtDate').val();
    viewModel.data["Fk_Vendor"] = $('#txtVendor').val();
    viewModel.data["Fk_Material"] = $('#txtMid').val();
    viewModel.data["Fk_PoNo"] = $('#Fk_PoNo').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["InvDate"] = $('#dtDate').val();
    viewModel1.data["Fk_Vendor"] = $('#txtVendor').val();
    viewModel1.data["Fk_Material"] = $('#txtMid').val();
    viewModel1.data["Fk_PoNo"] = $('#Fk_PoNo').val();

  
}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;
    var TaxVal =  Number(GTot * 0.06);
    document.getElementById('CGST').value = TaxVal;
    document.getElementById('SGST').value = TaxVal;

    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    MatName = viewObject.viewModel.data["MatName"]();
    //RNo = viewObject.viewModel.data["ReelNo"]();
    BFVal = viewObject.viewModel.data["BfVal"]();
    GSMVal = viewObject.viewModel.data["GSMval"]();
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;


    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PaperCertificate/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {

            Pk_CharID: {
                title: 'Pk_CharID',
                key: true,
                list: false
            },
            ReelNo: {
                title: 'ReelNo',
                list: true
            },
            //TQty: {
            //    title: 'TQty',
            //    list: true
            //},
            Fk_Characteristics: {
                title: 'Char.Name',
                list: true
            },

            TestMethod: {
                title: 'TestMethod'
            },
            Target: {
                title: 'Target',
                width: '1%'
            },

            Acceptance: {
                title: 'Acceptance',
                width: '1%'
            },
            Result1: {
                title: 'R1',
                width: '1%'

            },
            Result2: {
                title: 'R2',
                width: '1%'

            },
            Result3: {
                title: 'R3',
                width: '1%'
            },
            Result4: {
                title: 'R4',
                width: '1%'
            },
            Result5: {
                title: 'R5',
                width: '1%'
            },
            Result6: {
                title: 'R6',
                width: '1%'
            },
            MinVal: {
                title: 'MinVal',
                width: '1%'
            },
            MaxVal: {
                title: 'MaxVal',
                width: '1%'
            },
            AvgVal: {
                title: 'AvgVal',
                width: '1%'
            },
            Remarks: {
                title: 'Remarks',
                width: '1%'
            },
            Status: {
                title: 'Status',
                width: '1%'
            },
        }
    });


    //if ($.trim($('#TaxType').val()) == "VAT") {
    //    $("#VAT").prop("checked", true);
    //}
    //else if ($.trim($('#TaxType').val()) == "CST") {
    //    $("#CST").prop("checked", true);

        

        
    

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        //    this.load = function (Pk_IdDet, ReelNo,  Pk_CharID, Fk_Characteristics, txtFMaterial, TestMethod, Target, Acceptance, Result1, Result2, Result3, Result4, Result5, Result6, MinVal, MaxVal, AvgVal, Remarks, Status) {

      
        oCut.load(oSCuts[i].Pk_IdDet, oSCuts[i].ReelNo, oSCuts[i].Pk_CharID, oSCuts[i].Fk_Characteristics, oSCuts[i].txtFMaterial, oSCuts[i].TestMethod, oSCuts[i].Target, oSCuts[i].Acceptance, oSCuts[i].Result1, oSCuts[i].Result2, oSCuts[i].Result3, oSCuts[i].Result4, oSCuts[i].Result5, oSCuts[i].Result6, oSCuts[i].MinVal, oSCuts[i].MaxVal, oSCuts[i].AvgVal, oSCuts[i].Remarks, oSCuts[i].Status);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }
  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PaperCertificate", "_AddMaterial", function () { return new IssuedMaterial() });


 
}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-medium search-query'/>&nbsp;&nbsp;";
    var txtFieldPONO = "<input type='text' id='txtdlgPONO' placeholder='PONO' class='input-medium search-query'/>&nbsp;&nbsp;";
    var txtFieldGSM = "<input type='text' id='txtdlgGSM' placeholder='GSM' class='input-medium search-query'/>&nbsp;&nbsp;";
    var txtFieldBF = "<input type='text' id='txtdlgBF' placeholder='BF' class='input-medium search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldPONO);
    $("#dlgSearchFields").append(txtFieldVendorName);
    $("#dlgSearchFields").append(txtFieldGSM);
    $("#dlgSearchFields").append(txtFieldBF);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/POCertList',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },

        //Pk_PONo = p.Pk_PONo,
        //Pk_Material = p.Pk_Material,
        //Mat_Name = p.Name,
        //Quantity = p.Quantity,
        //Rate = p.Rate,
        //Amount = p.Amount,
        //VendorName = p.VendorName


        fields: {
            Pk_PONo: {
                title: 'PoNo',
                key: true,
                width: '2%',
                list:false
            },
            PoNoDisp: {
                title: 'PoNo',               
                width: '2%',
               
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            },
            Mat_Name: {
                title: 'Material Name',
                edit: false,
                width: '3%'
            },
            GSM: {
                title: 'GSM',
                edit: false,
                width: '2%'
            },
            BF: {
                title: 'BF',
                edit: false,
                width: '2%'
            },
            Color
                : {
                    title: 'Shade',
                    edit: false,
                    width: '2%'
                },
            PoDet
              : {
                  title: 'PoDet',
                  list: false,
                
              },
          
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val(),
            GSM: $('#txtdlgGSM').val(),
            BF: $('#txtdlgBF').val(),
            PoNoDisp: $('#txtdlgPONO').val(),
            
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val(),
            GSM: $('#txtdlgGSM').val(),
            BF: $('#txtdlgBF').val(),
            PoNoDisp: $('#txtdlgPONO').val(),
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        //$('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        //$('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        
        document.getElementById('txtVendor').value = rows[0].data.Pk_Vendor;
        document.getElementById('txtVName').value = rows[0].data.VendorName;
        VendorID = rows[0].keyValue;
        document.getElementById('Fk_PoNo').value = VendorID;
        //  $('#txtFkMaterial').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('txtMid').value = rows[0].data.Pk_Material;
        document.getElementById('txtMName').value = rows[0].data.Mat_Name;

        document.getElementById('txtGSM').value = rows[0].data.GSM;
        GSMVal = rows[0].data.GSM;
        BFVal = rows[0].data.BF;
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Characteristics List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PaperChar/PCharListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
      
        fields: {
            Pk_CharID: {
                title: 'Id',
                key: true
            },
            Name: {
                title: 'Name'
            },
            UOM: {
                title: "UOM"
            },
            TestMethod:{
                title: "TestMethod"
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtSMaterial').val(),
            //Name: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtSMaterial').val(),
            //Name: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();

        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        //var aa = viewModel.data["Materials"]()[0];
        //tlbMaterials
        //  var oTable = $('#tlbMaterials').jtable();
        //var ss = $("#tlbMaterials").find("tr").length;
        //alert(ss);
        document.getElementById('txtFk_Material').value = rows[0].data["Name"];
        document.getElementById('Pk_CharID').value = rows[0].keyValue;
        document.getElementById('TestMethod').value = rows[0].data.TestMethod;
        var ss = $("#tlbMaterials").find("tr").length;
        CharID = rows[0].keyValue;

        if (Number(CharID) > 1)
        { document.getElementById('ReelNo').value = RNo; }

        if (curEdit == true)
        {
            document.getElementById('txtMName1').value = MatName;
        }
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
       


        CalcTarget();
 $("#ReelNo").focus();
    });



}

function ReelSave()
{
       RNo =document.getElementById('ReelNo').value  ;
}

function CalcBF()
{
    var BSVal =document.getElementById('BS').value;

    if (BSVal != "" || BSVal != undefined || BSVal != 0)
    {
     
        GSMVal = this.GSMVal;

        var BF = (Number(BSVal) * 1000) / Number(GSMVal)
        document.getElementById('BF').value = BF.toFixed(2);
    }
}
function CalcTarget() {

    
    if (curEdit == true)
    {
        BFVal = this.BFVal;
        GSMVal = this.GSMVal;
       
    }

    if (curEdit == false) {
        var MatName = document.getElementById('txtMName').value;
        document.getElementById('txtMName1').value = MatName;
    }
    if (CharID == 1) {
        document.getElementById('Target').value=Number(GSMVal);
        var TargetVal = Number(GSMVal) * .05
        document.getElementById('Acceptance').value = "+/- 5%";

        document.getElementById('Min').value =  (Number(GSMVal) - Number(TargetVal)) ;
        document.getElementById('Max').value =  (Number(GSMVal) + Number(TargetVal));
    }

        ///////BF
    else if (CharID == 2) {


       // alert(arrBF);
      document.getElementById('Target').value = Number(BFVal);
       // var TargetVal = Number(BFVal) * .05
        document.getElementById('Acceptance').value = " > or = " + Number(BFVal);
       document.getElementById('Min').value = Number(BFVal);
       document.getElementById('Max').value =1000;
      for (var i = 0; i < arrSubstance.length; i++) {
          if (arrSubstance[i] > 0)
          {
              if (arrBF[i] > 0)
              {
                  var Numerator = Number(arrBF[i]);
                  var Denom = Number(arrSubstance[i]);
                  //var R1 = document.getElementById('Result1').value;
                  var ResVal = (i+1);
                  var Res = (Number(Numerator) * 1000 / Number(Denom));
                  if (ResVal == 1) {

                      document.getElementById('Result1').value = Res.toFixed(2);
                  }
                  else if (ResVal == 2) {

                      document.getElementById('Result2').value = Res.toFixed(2);
                  }
                  else if (ResVal == 3) {

                      document.getElementById('Result3').value = Res.toFixed(2);
                  }
                  else if (ResVal == 4) {

                      document.getElementById('Result4').value = Res.toFixed(2);
                  }
                  else if (ResVal == 5) {

                      document.getElementById('Result5').value = Res.toFixed(2);
                  }
                  else if (ResVal == 6) {

                      document.getElementById('Result6').value = Res.toFixed(2);
                  }
              }
          }
          CheckAccept();
          // total += parseFloat(arr2[i]);
      }


      
    }
       ///////RCT MD
   else if (CharID == 3) {
      
       //var TargetVal = Number(GSMVal) * .05
       

       
       document.getElementById('Max').value = "20"


       /////////////////
       var IndexVal;
     
       if(BFVal==14)
       { IndexVal = 3.50;}

       else if(BFVal==16)
       {IndexVal = 4.50;}
       
       else if(BFVal==18)
       {IndexVal = 5.50;}
       
       else if(BFVal==20){
           IndexVal = 7.00;}
       
       else if(BFVal==22)
       {     IndexVal = 7.25;}
       
       else if(BFVal==24)
       {IndexVal = 8.25;}
       
       else if(BFVal==25)
       {    IndexVal = 9.50;}
       
       else if(BFVal==35)
       {  
           IndexVal = 11.50;
       }
       else if(BFVal==45)
            {   IndexVal = 13.00;
            
       }
       var RCTVal = (Number(IndexVal) * Number(GSMVal)) / 1000;

       document.getElementById('Target').value = RCTVal;
       var SetMinVal =  Number(RCTVal)-(Number(RCTVal)*0.05)
       document.getElementById('Min').value = SetMinVal.toFixed(3);
       document.getElementById('Acceptance').value = " > " + SetMinVal.toFixed(3);
       
       /////////////////




   }
       ///////RCT CD
   else if (CharID == 4) {
      // document.getElementById('Target').value = 1.2;
       //var TargetVal = Number(GSMVal) * .05
       //document.getElementById('Acceptance').value = " > or = 1.2";

       //document.getElementById('Min').value = "1.2";
       document.getElementById('Max').value = "20"


       var IndexVal;

       if (BFVal == 14)
       { IndexVal = 3.50; }

       else if (BFVal == 16)
       { IndexVal = 4.50; }

       else if (BFVal == 18)
       { IndexVal = 5.50; }

       else if (BFVal == 20) {
           IndexVal = 7.00;
       }

       else if (BFVal == 22)
       { IndexVal = 7.25; }

       else if (BFVal == 24)
       { IndexVal = 8.25; }

       else if (BFVal == 25)
       { IndexVal = 9.50; }

       else if (BFVal == 35) {
           IndexVal = 11.50;
       }
       else if (BFVal == 45) {
           IndexVal = 13.00;

       }
       var RCTVal = (Number(IndexVal) * Number(GSMVal)) / 1000;

       document.getElementById('Target').value = RCTVal;
       var SetMinVal = Number(RCTVal) - (Number(RCTVal) * 0.05)
       document.getElementById('Min').value = SetMinVal.toFixed(3);
       document.getElementById('Acceptance').value = " > " + SetMinVal.toFixed(3);
       ////////////
   }
       ///////MOISTURE
   else if (CharID == 5) {
       document.getElementById('Target').value = ">7 & <9";
       //var TargetVal = Number(GSMVal) * .05
       document.getElementById('Acceptance').value = " > or = 7 & < or = 9";

       document.getElementById('Min').value = "7";
       document.getElementById('Max').value = "9"
   }

       ///COBB Rough    --- only one result
   else if (CharID == 6) {
       document.getElementById('Target').value = ">40&<60";
       //var TargetVal = Number(GSMVal) * .05
       document.getElementById('Acceptance').value = " > or = 40  & < or = 60";

       document.getElementById('Min').value = "40";
       document.getElementById('Max').value = "60"
   }
       ///COBB Smooth--- only one result
   else if (CharID == 7) {
       document.getElementById('Target').value = ">30&<50";
       //var TargetVal = Number(GSMVal) * .05
       document.getElementById('Acceptance').value = " > or = 30 & < or = 50";

       document.getElementById('Min').value = "30";
       document.getElementById('Max').value = "50"
   }
       ///////CALIPER
   else if (CharID == 8) {
      
       var TargetVal = Number(GSMVal) * 1.3;
       document.getElementById('TestMethod').value = "V2 Standard";
       document.getElementById('Target').value = TargetVal;
       document.getElementById('Acceptance').value = " > or = " + TargetVal;

       document.getElementById('Min').value = TargetVal;
       document.getElementById('Max').value = "1000"
   }
       //////////Mfg Defects

   else if (CharID == 9) {


     
       //document.getElementById('TestMethod').value = " ";
       document.getElementById('Target').value = " ";
       document.getElementById('Acceptance').value = " ";
       document.getElementById('Min').value = " ";
       document.getElementById('Max').value = " ";
       document.getElementById('Result1').value =0;
       document.getElementById('Result2').value = 0;
       document.getElementById('Result3').value = 0;
       document.getElementById('Result4').value = 0;
       document.getElementById('Result5').value = 0;
       document.getElementById('Result6').value = 0;


       document.getElementById('MinVal').value = " ";
       document.getElementById('MaxVal').value = " ";
       document.getElementById('AvgVal').value = " ";
             
   }
   else if (CharID == 10) {
      
       var TargetVal = (Number(GSMVal) * Number(BFVal))/1000;
       //  document.getElementById('TestMethod').value = "V2 Standard";
       document.getElementById('Target').value =  Number(TargetVal);
       document.getElementById('BS').value = TargetVal;
       document.getElementById('Acceptance').value = " > or = " + TargetVal;

       document.getElementById('Min').value = TargetVal;
       document.getElementById('Max').value = "1000"
  
   }
}

function CheckAccept() {

    //if (CharID == 1)
    //alert(CharID);
    var arrctr = 0;
    var R1 = document.getElementById('Result1').value;
    if (R1 == "") {
        R1 = 0;
    }
    else {
        arrctr = arrctr + 1;
    }
    var R2 = document.getElementById('Result2').value;
    if (R2 == "") {
        R2 = 0;
    }
    else {
        arrctr = arrctr + 1;
    }
    var R3 = document.getElementById('Result3').value;
    if (R3 == "") {
        R3 = 0;
    }
    else {
        arrctr = arrctr + 1;
    }
    var R4 = document.getElementById('Result4').value;
    if (R4 == "") {
        R4 = 0;
    }
    else {
        arrctr = arrctr + 1;
    }
    var R5 = document.getElementById('Result5').value;
    if (R5 == "") {
        R5 = 0;
    }
    else {
        arrctr = arrctr + 1;
    }
    var R6 = document.getElementById('Result6').value;
    if (R6 == "") {
        R6 = 0;
    }
    else {
        arrctr = arrctr + 1;
    }
    var numbers = [Number(R1), Number(R2), Number(R3), Number(R4), Number(R5), Number(R6)];
    var sortVal = numbers.sort();

    var arr = Math.max(R1, R2, R3, R4, R5, R6);
    if (arrctr == 1) {
        var arr1 = numbers[numbers.length - 1];
    }
    else if (arrctr == 2) {
        var arr1 = numbers[numbers.length - 2];
    }
    else if (arrctr == 3) {
        var arr1 = numbers[numbers.length - 3];
    }
    else if (arrctr == 4) {
        var arr1 = numbers[numbers.length - 4];
    }
    else if (arrctr == 5) {
        var arr1 = numbers[numbers.length - 5];
    }
    else if (arrctr == 6) {
        var arr1 = numbers[numbers.length - 6];
    }
    // var arr1 = Math.min(Number(R1), Number(R2), Number(R3), Number(R4), Number(R5), Number(R6));
    if (arr1 == 0) {
        arr1 = arr;
    }
    var arr2 = [R1, R2, R3, R4, R5, R6];

     
    //alert(arr2);
    if (CharID == 1)
    {
        arrSubstance=arr2
    }
    else if (CharID == 10)
    {
        arrBF = arr2;
    }


    var total = 0;
    for (var i = 0; i < arr2.length; i++) {
        total += parseFloat(arr2[i]);
    }
    //var avg = Math.round(Number(total) / Number(arr2.length));
    var RndVal = 0;
    if (CharID == 1 || CharID == 6) {
        var avg = Number(total) / Number(arrctr);

        RndVal = avg.toFixed(1);
    }
    else if (CharID == 2 || CharID == 3 || CharID == 4) {
        var avg = Number(total) / Number(arrctr);
        RndVal = avg.toFixed(2);
    }
    else {
        var avg = Math.round(Number(total) / Number(arrctr));
        RndVal = avg;
    }

    //alert("Avg " + avg);

    document.getElementById('MinVal').value = arr1;
    document.getElementById('MaxVal').value = arr;
    document.getElementById('AvgVal').value = RndVal;



    var MaxRange = document.getElementById('Max').value;
    var MinRange = document.getElementById('Min').value;
    if (Number(MaxRange) > 0 && Number(MinRange) > 0) {

        if (CharID == 2) {

            if (Number(RndVal >= MinRange)) {
                document.getElementById('Status').value = "Yes";
                $("#Fk_Yes").prop("checked", true);

            }
            else {
                document.getElementById('Status').value = "No";
                $("#Fk_No").prop("checked", true);
            }
        }

        else {
            var j = [numbers.length - arrctr]
            for (i = 0; i < arrctr; i++) {
                if (Number(numbers[j] > 0)) {
                    //   alert("value of arrctr = " + arrctr);
                    //alert("value of j = " + j);
                    //alert(Number(numbers[j]));

                    if (Number(numbers[j]) > Number(MaxRange) || Number(numbers[j]) < Number(MinRange)) {
                        document.getElementById('Status').value = "No";
                        $("#Fk_No").prop("checked", true);
                        break;
                    }
                    else {
                        document.getElementById('Status').value = "Yes";
                        $("#Fk_Yes").prop("checked", true);
                    }
                }
                j = j - 1;
            }
        }
    }
   
     
}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        //var ss = $("#tlbMaterials").find("tr").length;
        //alert(ss);
        e.preventDefault();
        Clear();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFkMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });



    if (viewModel.data != null) {
        $('#txtFkMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
     //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    //$('#cmdDone').off();


}



function radioClass(str1) {
    if (str1 == "Yes") {
        document.getElementById('Status').value = str1;
    }
    else if (str1 == "No") {
        document.getElementById('Status').value = str1;
    }
    
}



function ValDup() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("ReelNo", $('#ReelNo').val());
    _comLayer.parameters.add("Invno", $('#txtInvNo').val());
    oResult = _comLayer.executeSyncAction("PaperCertificate/ValDuplicate", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Certificate Already added";

}