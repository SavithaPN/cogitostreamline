﻿var ssum = 0;
var samt = 0;
function IssuedMaterial() {
    this.data = new Object();
    this.data["Pk_CharID"] = "";
    this.data["Fk_Characteristics"] = "";
    this.data["txtFk_Material"] = "";
    this.data["TestMethod"] = "";
    this.data["Target"] = "";
    this.data["Acceptance"] = "";
    this.data["Result1"] = "";
    this.data["Result2"] = "";
    this.data["Result3"] = "";
    this.data["Result4"] = "";
    this.data["Result5"] = "";
    this.data["Result6"] = "";
    this.data["MinVal"] = "";
    this.data["MaxVal"] = "";
    this.data["AvgVal"] = "";
    this.data["Remarks"] = "";
    //this.data["TQty"] = "";
    this.data["ReelNo"] = "";
    this.data["Status"] = "";


    this.updateValues = function () {
        this.data["Pk_CharID"] = $('#Pk_CharID').val();
        this.data["Fk_Characteristics"] = $('#txtFk_Material').val();
        this.data["TestMethod"] = $('#TestMethod').val();
        this.data["Target"] = $('#Target').val();
        this.data["Acceptance"] = $('#Acceptance').val();
        this.data["Result1"] = $('#Result1').val();
        this.data["Result2"] = $('#Result2').val();
        this.data["Result3"] = $('#Result3').val();
        this.data["Result4"] = $('#Result4').val();
        this.data["Result5"] = $('#Result5').val();
        this.data["Result6"] = $('#Result6').val();
        this.data["MinVal"] = $('#MinVal').val();
        this.data["MaxVal"] = $('#MaxVal').val();
        this.data["AvgVal"] = $('#AvgVal').val();
        this.data["Remarks"] = $('#Remarks').val();
        this.data["ReelNo"] = $('#ReelNo').val();
        //this.data["TQty"] = $('#TQty').val();
        this.data["Status"] = $('#Status').val();
       
    };
    this.load = function (Pk_IdDet, ReelNo,  Pk_CharID, Fk_Characteristics, txtFMaterial, TestMethod, Target, Acceptance, Result1, Result2, Result3, Result4, Result5, Result6, MinVal, MaxVal, AvgVal, Remarks, Status) {
   
        this.data["Pk_IdDet"] = Pk_IdDet;
        this.data["ReelNo"] = ReelNo;
        this.data["Pk_CharID"] = Pk_CharID;
            this.data["Fk_Characteristics"] = Fk_Characteristics;
            this.data["txtFMaterial"] = txtFMaterial;
            this.data["TestMethod"] = TestMethod;
            this.data["Target"] =Target;
            this.data["Acceptance"] = Acceptance;
            this.data["Result1"] = Result1;
            this.data["Result2"] = Result2;
            this.data["Result3"] = Result3;
            this.data["Result4"] = Result4;
            this.data["Result5"] = Result5;
            this.data["Result6"] =Result6;
            this.data["MinVal"] = MinVal;
            this.data["MaxVal"] = MaxVal;
            this.data["AvgVal"] = AvgVal;
            this.data["Remarks"] = Remarks;
            this.data["Status"] = Status;
    };
}
