﻿$.fn.slPart = function (options) {

    this.options = $.extend({
        viewTag: "",
        title: "",
        id: "",
        outershell: false,
        cap: false,
        rate: false,
        model: new part(),
        loadFromModel: false
    }, options);

    this.model = this.options.model;

    _comLayer.parameters.clear();

    if (this.options.outershell) {
        _comLayer.parameters.add("ViewName", "_OuterShell");

    }
    else if (this.options.cap) {
        _comLayer.parameters.add("ViewName", "_CapPart");
    }
    else if (this.options.rate) {
        _comLayer.parameters.add("ViewName", "_Rate");
    }
    else {
        _comLayer.parameters.add("ViewName", "_GenericPart");
    }
    var sPath = _comLayer.buildURL(this.options.viewTag, "LoadViewLayout");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    if (oResult.success == true) {
    //oResult.success == true
        $(this).empty();
        $(this).html(oResult.data);
    }

    $(this).find('.partHeader').text(this.options.title);
    $(this).find('.mainPartContainer').attr("id", this.options.id);

    var that = this;

    this.getModel = function () {
        return this.model;
    }

    if (!this.options.rate) {
        $(this).find('.mainPartContainer').find(".smalltile").click(function () {

            var cntr = $($(this).parent()).parent();

            var ix = $(this).attr("value");

            displayLayers(cntr, ix, that);
        });
    }
    else {
        var cntr = $($(this).parent()[0]);

        ko.applyBindings(that.options.estimationModel, $(this).parent()[0]);

        that = this;

        $(cntr).find('.input-mini').change(function () {
            that.options.rateChanged(that.options.estimationModel);
        });
    }

    //check if are loading from model
    if (this.options.loadFromModel) {
        displayLayers($($(this).parent()[0]), this.model.layers.length, that);
    }
    
    return this;
}

function copyLayerValues(model, iSelected) {

    var i = 0;

    while (model.layers[i]) {

        var chk = "#chkLayer_" + i;

        if ($(chk).is(':checked')) {
            model.layers[i].gsm(model.layers[iSelected].gsm());
            model.layers[i].bf(model.layers[iSelected].bf());
            model.layers[i].color(model.layers[iSelected].color());
            model.layers[i].rate(model.layers[iSelected].rate());
        }

        i++;
    }
}

function copySelection(model, type) {

    var i = 0;

    while (model.layers[i]) {
        var chk = "#chkLayer_" + i;
        $(chk).prop('checked', false);

        if (type == "Fluted") {
            if ((i % 2) != 0) {
                $(chk).prop('checked', true);
            }
        }
        else if (type == "NonFluted") {
            if ((i % 2) == 0) {
                $(chk).prop('checked', true);
            }
        }
        else {
            $(chk).prop('checked', true);
        }
                
        i++;
    }

}

function displayLayers(cntr, ix, that) {
    $(cntr).find('.selPly').hide();
    var lar = null;
    for (i = 0; i < ix; i++) {
        if (!that.options.loadFromModel) {
            var lar = new layer(0, 0, "N", 0);
            that.model.addLayer(lar);
        }
        else {
            lar = that.model.layers[i];
        }
        var lblGSm = $("<table><tr><td><label>GSM</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].gsm'></label></td></tr></table>");
        var lblbf = $("<table><tr><td><label>BF</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].bf'></label></td></tr></table>");
        var lblcolor = $("<table><tr><td><label>Color</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].color'></label></td></tr></table>");
        var lblrate = $("<table><tr><td><label>Rate</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].rate'></label></td></tr></table>");
        var dv = $("<div class='selector' style='height:100px; width:35px;' layerId ='" + i + "' ></div>");

        if (i % 2 != 0) {
            dv.addClass("flt");
        }

        dv.append(lblGSm);
        dv.append(lblbf);
        dv.append(lblcolor);
        dv.append(lblrate);
        var tr = $("<td></td>");
        tr.append(dv);
        $(cntr).find(".row").append(tr);

    }

    $(cntr).find('.selector').click(function () {
        ko.cleanNode(document.getElementById("estDialog"));

        var iCount = $(this).attr("layerId");
        var model = that.model.layers[iCount];

        ko.applyBindings(model, document.getElementById("estDialog"));

        var layerNumber = Number(iCount) + 1;

        var header = "Details of Layer " + layerNumber;

        $("#lyrHeader").text(header);

        var i = 0;

        $("#dlgCopyOptions").empty();

        while (that.model.layers[i]) {
            var chk = $("<td><label class='checkbox inline'><input type='checkbox' value='" + i + "' id='chkLayer_" + i + "' /><span class='metro-checkbox'>" + (i + 1) + "</span></label><td>");
            $("#dlgCopyOptions").append(chk);
            i++;
        }

        $("#cmdCopyAll").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "");
        });

        $("#cmdCopyFluted").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "Fluted");
        });

        $("#cmdNonFluted").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "NonFluted");
        });

        $("#cmdSaveLayers").click(function (e) {
            e.preventDefault();
            copyLayerValues(that.model, iCount);
            $("#estDialog").modal("hide");
        });


        $("#estDialog").find('.input-mini').change(function () {
            that.options.layerChanged(that.model, model, that.options.outershell, that.options.cap, that.options.plate);
        });

        $("#estDialog").modal("show");
    });

    $(cntr).find('.selElements').show();

    //ko.applyBindings(that.model, $(that).find('.mainPartContainer')[0]);
    ko.applyBindings(that.model, $(that).parent()[0]);

    $(cntr).find('.input-mini').change(function () {
        that.options.partChanged(that.model, that.options.outershell, that.options.cap, that.options.plate);
    });

}


