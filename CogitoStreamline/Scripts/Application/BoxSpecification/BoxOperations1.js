﻿var Estimation = new Estimation();
var bLoadFromModel = false;
var prtSequence = -1;

$(document).ready(function () {

    $('#cmdPublish').click(function (e) {
        e.preventDefault();
        $("#Basic").removeClass("active");
        $("#Details").addClass("active");
        $("#pivot").pivot(1);
        createFormParts();
    });

    $('#cmdNext').click(function (e) {
        e.preventDefault();
        $("#pivot").pivot(1);
    });

    $(".ckhPart").click(function (e) {
        if (e.currentTarget.checked) {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](1);
           
        }
        else {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](0);
          
        }
    });

    ko.applyBindings(Estimation, document.getElementById("shellSelection"));   

    var BoxNumber = _util.getParameterByName("BoxMaster");
    //BoxS.Fk_BoxID = BoxNumber;
    Estimation.Fk_BoxID = BoxNumber;

    if (Estimation.fetch()) {
        bLoadFromModel = true;
        createFormParts();
        $("#Basic").removeClass("active");
        $("#Details").addClass("active");
        $("#pivot").pivot(1);
        
    }
  
});

function createFormParts() {

    $("#prtList").empty();

    //Check Outershell
    addPart($('#chkOuterShell')[0].checked, $('#txtOuterShell').val(), "Outer Shell", true, false, false);

    //Check Plate
    addPart($('#chkPlate')[0].checked, $('#txtPlate').val(), "Plate", false, false, true);

    //Check LengthPartation
    addPart($('#chkLengthPartation')[0].checked, $('#txtLPartation').val(), "Length Partation", false, false, false);

    //Check WidthPartation
    addPart($('#chkWidthPartation')[0].checked, $('#txtWidthPartation').val(), "Width Partation", false, false, false);

    //Check Cap
    addPart($('#chkCap')[0].checked, $('#txtCap').val(), "Cap", false, true);

    //Now add one rate part to this
    addRatePart();

    //Load the existing model if available

}

function addRatePart() {
    var divId = "rateContainer";
    var rowMain = $("<tr></tr>");
    var colMain = $("<td></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var lblTotalWeight = $("<label>Total Weight</label>");
    var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalWeight' type='text' placeholder='Weight' style='width: 70px' readonly />");
    var colTotalWeight = $("<td valign='bottom'></td>");

    colTotalWeight.append(lblTotalWeight);
    colTotalWeight.append(txtTotalWeight);
    rowMain.append(colTotalWeight);

    //Create a row for column for total rate
    var lblTotalRate = $("<label>Total Price</label>");
    var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalPrice' type='text' placeholder='Price' style='width: 70px' readonly />");
    var colTotalRate = $("<td valign='bottom'></td>");

    colTotalRate.append(lblTotalRate);
    colTotalRate.append(txtTotalRate);
    rowMain.append(colTotalRate);

    var control = $(colMain).slPart({
        viewTag: "BoxSpec",
        title: "Addtional Costs",
        id: divId,
        estimationModel: Estimation,
        rate: true,
        rateChanged: rateChanged
    });

    var divId = "save";
    var rowMain = $("<tr id='saveBlock'></tr>");
    var colMain = $("<td colspan='3' align='center'></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var cmdCreate = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdCreate' >Save</button>");
        
    colMain.append(cmdCreate);
    rowMain.append(colMain);
    
    ko.applyBindings(Estimation, document.getElementById("saveBlock"));
}

function addPart(bConsider, iValue, sTitle, outherShell, cap, plate) {

    var localModel = null;
    if (bConsider) {

        for (i = 0; i < iValue; i++) {
            //let us sort out the model
            if (bLoadFromModel) {
                 prtSequence++;
                 localModel = Estimation.parts[prtSequence];
            }
            else
            {
                localModel = new part();
                Estimation.addParts(localModel);
            }

            var divId = "prtContainer" + i;
            var rowMain = $("<tr></tr>");
            var colMain = $("<td></td>");

            rowMain.append(colMain);
            $("#prtList").append(rowMain);

            //Create a row for column for total weight
            var lblTotalWeight = $("<label>Total Weight</label>");
            //var txtTotalWeight = $("<input class='input-mini weight validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' style='width: 70px' readonly />");
            var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' style='width: 70px' readonly />");
            var colTotalWeight = $("<td></td>");

            colTotalWeight.append(lblTotalWeight);
            colTotalWeight.append(txtTotalWeight);
            rowMain.append(colTotalWeight);

            //Create a row for column for total rate
            var lblTotalRate = $("<label>Total Rate</label>");
            //var txtTotalRate = $("<input class='input-mini rate validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' style='width: 70px' readonly />");
            var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' style='width: 70px' readonly />");
            var colTotalRate = $("<td></td>");

            colTotalRate.append(lblTotalRate);
            colTotalRate.append(txtTotalRate);
            rowMain.append(colTotalRate);

          var sNewTitle = sTitle + "-" + (i + 1);

          var control = $(colMain).slPart({
              viewTag: "BoxSpec",
                    title: sNewTitle,
                    id: divId,
                    outershell: outherShell,
                    cap: cap,
                    plate: plate,
                    model: localModel,
                    loadFromModel: bLoadFromModel,
                    partChanged: calculatePartChanged,
                    layerChanged: calculateLayerChanged
                });
                
        }
    }

}

function calculatePartChanged(model, outerShell, cap, plate) {

    if (outerShell) {
        outerShellCalculation(model)
    }
    else if (cap || plate) {
        plateCapCalculation(model);
    }
    else {
        partationCalculation(model);
    }

    rateChanged(Estimation);
}

function calculateLayerChanged(mainModel, model, outerShell, cap, plate)
{
    if (outerShell) {
        outerShellCalculation(mainModel)
    }
    else if (cap || plate) {
        plateCapCalculation(mainModel);
    }
    else {
        partationCalculation(mainModel);
    }

    rateChanged(Estimation);
}


////Outershell
function outerShellCalculation(model) {
    var length = model.length();
    var width = model.width();
    var height = model.height();
    //var noBoards = model.noBoards();

    //Board Size
    var boardArea = ((Number(length) * Number(width) * 4) + Number(50)) / 1000;
    model.boardArea(boardArea);

    //Cuttingsize
    var cuttingsize = Math.round((Number(length) * 2 + Number(width) * 2 + Number(50)) / 100) * 100;
    model.cuttingSize(cuttingsize);

    //Deckle
    var deckel = Math.round((Number(width) + Number(height) + 20) / 100) * 100;
    model.deckle(deckel);

    var i = 0;

    var weight = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = cuttingsize * deckel * 1.5 * model.layers[i].gsm();
            weight = weight + layerWeight;
        }
        else {
            var layerWeight = cuttingsize * deckel * model.layers[i].gsm();
            weight = weight + layerWeight;
        }

        i++;
    }

    weight = ((weight / 1000) * noBoards).toFixed(3);

    model.weight(weight);

}



////partationcalculation
function partationCalculation(model) {

    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();


    var adjustedLength = (Number(length) + Number(10)) / 10;
    var adjustedWidth = Number(Number(width) + Number(20)) / 10;

    var boardArea = (Number(adjustedWidth) * Number(adjustedLength)) / 1000;
    model.boardArea(boardArea);

    //Cuttingsize
    var cuttingSize = adjustedLength + adjustedWidth + 50;
    model.cuttingSize(cuttingSize);

    //Deckle
    var deckel = Math.round((Number(width) + 20) / 100) * 100;
    model.deckle(deckel);

    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = 1.5 * model.layers[i].gsm();
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    model.weight(weight);
}



//platecalculation
function plateCapCalculation(model) {

    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();
  

    var adjustedLength = (Number(length) + Number(240));
    var adjustedWidth = Number(Number(width) + Number(240));

    var boardArea = (Number(adjustedWidth) * Number(adjustedLength))/1000;
    model.boardArea(boardArea);

    //Cuttingsize
    var cuttingSize = adjustedLength + adjustedWidth + 50;
    model.cuttingSize(cuttingSize);

    //Deckle
    var deckel = Math.round((Number(width) + 20) / 100) * 100;
    model.deckle(deckel);

    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = 1.5 * model.layers[i].gsm();
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    model.weight(weight);
}

//Final Rate calculation
function rateChanged(estModel) {
    var i = 0;
    var totalWeight = 0;

    while (estModel.parts[i]) {
        totalWeight = Number(totalWeight) + Number(estModel.parts[i].weight());
        i++;
    }

    estModel.totalWeight(totalWeight);

    //Now rate calculation
    //Declaring variables

    var convRate = estModel.convRate();
    var convValue = 0;
    var gMarginPercentage = estModel.gMarginPercentage();
    var gMarginValue = 0;
    var taxesPercntage = estModel.taxesPercntage();
    var taxesValue = 0;
    var transportValue = estModel.transportValue();
    var weightHValue = estModel.weightHValue();
    var handlingChanrgesValue = estModel.handlingChanrgesValue();
    var packingChargesValue = estModel.packingChargesValue();
    var rejectionPercentage = estModel.rejectionPercentage();
    var rejectionValue = 0;
    var totalPrice = 0;

    //Calculations
    convValue = (convRate * totalWeight) / 100;
    gMarginValue = (convValue * gMarginPercentage) / 100;
    taxesValue = (convValue * taxesPercntage) / 100;
    rejectionValue = (convValue * rejectionPercentage) / 100;

    estModel.convValue(convValue);
    estModel.gMarginValue(gMarginValue);
    estModel.taxesValue(taxesValue);
    estModel.rejectionValue(rejectionValue);

    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  Number(handlingChanrgesValue) +
                  Number(packingChargesValue) +
                  Number(rejectionValue));
 
    estModel.totalPrice(totalPrice);
}