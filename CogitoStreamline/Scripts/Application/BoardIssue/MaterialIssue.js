﻿
//Name          : material indent Operations ----javascript files
//Description   : Contains the  materials and its quantity data Operations  
//                 1. Jtable Loading and Columns Descriptions 
//Author        : shantha
//Date            :   21/08/2015
//CRH Number      :   SL0038-SL0040
//Modifications : 




function MaterialIssue() {
    this.data = new Object();
    this.data["Pk_MaterialIssueDetailsID"] = "";
    this.data["Fk_IssueID"] = "";
    this.data["Fk_Material"] = "";
    this.data["txtFk_Material"] = "";
    this.data["Quantity"] = "";
    this.data["RollNo"] = "";

    this.updateValues = function () {
        //var i = 0;

        //while (gMaterials[i]) {
        //    if (gMaterials[i].Id == this.data["Fk_Material"]) {

        //        this.data["MaterialName"] = gMaterials.Name;
        //        i++;
        //    }

        //}
        //,
        //Pk_StockID: {
        //        title: 'Stkid'

        //},
        //this.data["RequiredDate"] = $('#dtRequiredDate').val();
        this.data["txtFk_Material"] = $('#txtMaterial').val();
        //this.data["Fk_CustomerOrder"] = $('#txtCustomerOrder').val();
        this.data["Quantity"] = $('#Quantity').val();
        this.data["Pk_StockID"] = $('#Pk_StockID').val();
        this.data["RollNo"] = $('#RollNo').val();
        
    };

    this.load = function (Pk_MaterialIssueDetailsID, Pk_StockID, Fk_Material, txtFk_Material, Quantity, RollNo) {
        this.data["Pk_MaterialIssueDetailsID"] = Pk_MaterialIssueDetailsID;
        
        this.data["Fk_Material"] = Fk_Material;
        this.data["txtFk_Material"] = txtFk_Material;
        this.data["Pk_StockID"] = Pk_StockID;
        this.data["Quantity"] = Quantity;
        this.data["RollNo"] = RollNo;

        //this.updateValues();
    };
}


