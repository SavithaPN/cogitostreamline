﻿var ssum = 0;
var samt = 0;
function IssuedMaterial() {
    this.data = new Object();
    this.data["PCode"] = "";
    this.data["MixingRatio"] = "";
    this.data["GelTemp"] = "";
    this.data["Specification1"] = "";
    this.data["Specification2"] = "";
    this.data["Results1"] = "";
    this.data["Results2"] = "";
    this.data["Status"] = "";
    
    this.updateValues = function () {
        this.data["PCode"] = $('#PCode').val();
        this.data["MixingRatio"] = $('#MixingRatio').val();
        this.data["GelTemp"] = $('#GelTemp').val();
        this.data["Specification1"] = $('#Specification1').val();
        this.data["Specification2"] = $('#Specification2').val();
        // this.data["Fk_Characteristics"] = $('#txtFk_Material').val();
        this.data["Results1"] = $('#Results1').val();
        this.data["Results2"] = $('#Results2').val();
        this.data["Status"] = $('#Status').val();

    };
    this.load = function (Pk_IdDet, PCode, MixingRatio, GelTemp, Specification1, Specification2, Results1, Results2, Status) {
        this.data["PCode"] = PCode;
        this.data["MixingRatio"] = MixingRatio;
        this.data["GelTemp"] = GelTemp;
        this.data["Specification1"] = Specification1;
        this.data["Specification2"] = Specification2;
        this.data["Results1"] = Results1;
        this.data["Results2"] = Results2;
        this.data["Status"] = Status;

    };
}
