﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Certification List',
        paging: true,
        pageSize: 8,
        actions: {
            listAction: 'GlueCertificate/GlueCertListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''

        },
        fields: {
            Pk_Id: {
                title: 'Cert.Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Mat.Name',
                width: '25%'
            },
            Invno: {
                title: 'Invno',
                width: '25%'
            },
            InvDate: {
                title: 'InvDate',
                width: '25%',
                list: false
            },
            VendorName: {
                title: 'VName',
                width: '25%'
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        JCNO = row.record.Pk_Id;
                        _comLayer.parameters.add("JCNO", JCNO);
                        _comLayer.executeSyncAction("GlueCertificate/GlueCert", _comLayer.parameters);
                        var strval = "ConvertPDF/GCertificate" + JCNO + ".pdf"

                        window.open(strval, '_blank ', 'width=700,height=250');
                        //return true;
                        ///////////////////////////


                    });
                    return button;
                }

            },

        },
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Id: $('#txtCust').val(),
            Name: $('#txtName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');


    });
 

}

function afterNewShow(viewObject) {
    //var dNow = new Date();
    //document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Findings',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/GlueCertificate/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {

            Pk_IdDet: {
                title: 'Pk_IdDet',
                key: true,
                list: false
            },
            PCode: {
                title: 'PCode',                
                list: true
            },
          
            PCode: {
                title: 'Prd.Code',
                list: true,
                width: '2%'
            },

            MixingRatio: {
                title: 'MixingRatio',
                width: '1%'
            },
            GelTemp: {
                title: 'GelTemp',
                width: '1%'
            },
            Specification1: {
                title: 'Vis.Spec',
                width: '3%'
            },

            Results1: {
                title: 'Vis.Result',
                width: '1%'
            },
            Specification2: {
                title: 'Solid Spec.',
                width: '3%'

            },

            Results2: {
                title: 'Solid Result',
                width: '3%'
            },
            Status: {
                title: 'Status',
                width: '1%'
            },
        }
    });

   
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "GlueCertificate", "_AddMaterial", function () { return new IssuedMaterial() });

 

    //Calc();
    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });

    $('#cmdMaterialMSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        $("#searchIndentDialog").modal("show");
      
        $("#searchIndentDialog").css("top", "100px");
        $("#searchIndentDialog").css("left", "350px");
    });
  
}

function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchHeader1").text("Glue Search");

    //Adding Search fields
    //var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    //$("#dlgMaterialSearchFields").append(txtFieldMaterialName);

    $('#IndentSearhContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Gum/GumListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name'
            }
            ,
            Fk_UnitId: {
                title: 'Unit'
            }
            ,

            Type: {
                title: 'Type'
            }
            ,
            Brand: {
                title: 'Brand'
            }


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgIndent').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgIndent').val()
        });
    });



    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');



        $("#searchIndentDialog").modal("hide");
        $('#txtFkMaterial').wgReferenceField("setData", rows[0].keyValue);


        $("#searchIndentDialog").modal("hide");
     

    });
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["InvDate"] = $('#dtDate').val();
    viewModel.data["Fk_Vendor"] = VendorID;
   

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["InvDate"] = $('#dtDate').val();
    viewModel1.data["Fk_Vendor"] = VendorID;
   

  
}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;
    var TaxVal =  Number(GTot * 0.06);
    document.getElementById('CGST').value = TaxVal;
    document.getElementById('SGST').value = TaxVal;

    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;


    $('#tlbMaterials').jtable({
        title: 'Findings',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/GlueCertificate/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {

            Pk_IdDet: {
                title: 'Pk_IdDet',
                key: true,
                list: false
            },
            PCode: {
                title: 'Prd.Code',
                list: true,
                width: '2%'
            },

            MixingRatio: {
                title: 'MixingRatio',
                width: '1%'
            },
            GelTemp: {
                title: 'GelTemp',
                width: '1%'
            },
            Specification1: {
                title: 'Vis.Spec',
                width: '3%'
            },

            Results1: {
                title: 'Vis.Result',
                width: '1%'
            },
            Specification2: {
                title: 'Solid Spec.',
                width: '3%'

            },

            Results2: {
                title: 'Solid Result',
                width: '3%'
            },

          
        }
    });

    //if ($.trim($('#TaxType').val()) == "VAT") {
    //    $("#VAT").prop("checked", true);
    //}
    //else if ($.trim($('#TaxType').val()) == "CST") {
    //    $("#CST").prop("checked", true);

        

        
    

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();
      
        //Pk_IdDet, Pk_CharID, Fk_Characteristics, txtFMaterial, Spec_Target, Spec_Tolerance, Results
        oCut.load(oSCuts[i].Pk_IdDet, oSCuts[i].PCode, oSCuts[i].MixingRatio, oSCuts[i].GelTemp, oSCuts[i].Specification1, oSCuts[i].Specification2, oSCuts[i].Results1, oSCuts[i].Results2);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }
  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "GlueCertificate", "_AddMaterial", function () { return new IssuedMaterial() });


 
}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true,
                width:'2%'
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Parameters List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InkParams/InkCharListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
      
        fields: {
            Pk_CharID: {
                title: 'Id',
                key: true
            },
            Name: {
                title: 'Name'
            },
            //StdVal: {
            //    title: "StdVal"
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtSMaterial').val(),
            //Name: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtSMaterial').val(),
            //Name: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        document.getElementById('txtFk_Material').value = rows[0].data["Name"];
        document.getElementById('Pk_CharID').value = rows[0].keyValue;
        
        //document.getElementById('StdVal').value = rows[0].data.StdVal;
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Spec_Target").focus();
    });



}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFkMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });



    if (viewModel.data != null) {
        $('#txtFkMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
     //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();


}


function radioClass(str1) {
    if (str1 == "Yes") {
        document.getElementById('Status').value = str1;
        $("#Fk_Yes").prop("checked", true);
    }
    else if (str1 == "No") {
        document.getElementById('Status').value = str1;
        $("#Fk_No").prop("checked", true);
    }

}

