﻿
//Name          : State Operations ---javascript file
//Description   : Contains the  State Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckStateDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0011
//Modifications :  

var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'State List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/State/StateListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_StateID: {
                title: 'Id',
                key: true

            },
            Country: {
                title: 'Country',
                width: '25%'
            },
            StateName: {
                title: 'Name',
                width: '25%',
            }
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_StateID: $('#txtStateID').val(),
            StateName: $('#txtStateName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Country", "getCountry", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Country", "getCountry", null);
}
function CheckStateDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Country", $('#Country').val());
    _comLayer.parameters.add("StateName", $('#StateName').val());
    oResult = _comLayer.executeSyncAction("State/StateDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "State Already added";
}









