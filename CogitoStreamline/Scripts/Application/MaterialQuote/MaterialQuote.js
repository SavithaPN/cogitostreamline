﻿function MaterialQuote() {
    this.data = new Object();
    this.data["Pk_QuotationMaterial"] = "";
    this.data["Fk_QuotationRequest"] = "";
    this.data["Fk_Material"] = "";
    this.data["txtFk_Material"] = "";
    this.data["SpecialInformation"] = "";
    this.data["Quantity"] = "";

    this.updateValues = function () {
        //var i = 0;

        //while (gMaterials[i]) {
        //    if (gMaterials[i].Id == this.data["Fk_Material"]) {

        //        this.data["MaterialName"] = gMaterials.Name;
        //        i++;
        //    }

        //}
        //this.data["RequiredDate"] = $('#dtRequiredDate').val();
        this.data["txtFk_Material"] = $('#txtFk_Material').val();
        this.data["SpecialInformation"] = $('#SpecialInformation').val();
        this.data["Quantity"] = $('#Quantity').val();
    };

    this.load = function (Pk_QuotationMaterial, txtFk_Material, Fk_Material, Quantity, SpecialInformation) {
        this.data["Pk_QuotationMaterial"] = Pk_QuotationMaterial;
        this.data["Fk_Material"] = Fk_Material;
        this.data["txtFk_Material"] = txtFk_Material;
        this.data["SpecialInformation"] = SpecialInformation;
        this.data["Quantity"] = Quantity;
        //this.updateValues();
    };
}

