﻿var objShortCut = null;
var VendorID = "";
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Material Quotation List',
        paging: true,
        pageSize: 8,
        sorting: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialQuote/MaterialQuoteListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {

            Pk_QuotationRequest: {
                title: 'Request ID',
                width: '10%',
                key: true
            },
            RequestDate: {
                title: ' Date',
                width: '5%'
            },
            Fk_Vendor: {
                title: 'Vendor Name',
                width: '10%'
            }
           



        }
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            
            Pk_QuotationRequest: $('#txtQuotationRequest').val(),
            Fk_Vendor: $('#txtVendorName').val(),
            FromRequestDate: $('#TxtFromRequestDate').val(),
            ToRequestDate: $('#TxtToRequestDate').val()

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
    setUpVendorSearch();
}


function afterNewShow(viewObject) {
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('RequestDate').value = (((dNow.getDate() + 1) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#RequestDate').datepicker({ autoclose: true });


    //var dNow = new Date();
    //document.getElementById('Inwd_date').value = (dNow.getMonth() + 1) + '/' + dNow.getDate() + '/' + dNow.getFullYear();

    objCurrstate = false;
    curViewModel = viewObject.viewModel;

    $('#tlbProducts').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialQuote/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Material Id',
                key: false,
                list: false
            },
            txtFk_Material: {
                title: 'Material'
            },
            Quantity: {
                title: 'Quantity'

            },
            SpecialInformation: {
                title: 'Special Information',
                width: '2%'
            }
            
        }


    });

   


    //$("#Inwd_date").val($.datepicker.formatDate("mm/dd/yy", new Date()));

    // $('#cmdCreate').attr('disabled', true);
    configureOne2Many("#cmdAddProduct", '#tlbProducts', "#cmdSaveProduct", viewObject, "QuoteDetails", "MaterialQuote", "_AddMaterial", function () { return new MaterialQuote() });

    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        //setUpCustomerSearch();
        $("#searchDialogVendor").modal("show");
    });

}

function ReferenceFieldNotInitilized(viewModel) {

    $('#txtFk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });
    $('#txtCustomerOrder').wgReferenceField({
        keyProperty: "Pk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewModel
    });

    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_CustomerOrder"]);
        $('#txtFk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
    }

    if (objContextEdit == true) {
        $('#cmdMaterialSearch').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdMaterialSearch').attr('disabled', false);
    }
}


function setUpVendorSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldVendorNo = "<input type='text' id='txtdlgVendorNo' placeholder='Vendor No.' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendorname = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFieldsVendor").append(txtFieldVendorNo);
    $("#dlgSearchFieldsVendor").append(txtFieldVendorname);

    $('#SearchVendorContainer').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        fields: {
            Pk_Vendor: {
                title: 'VendorID',
                key: true
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false
            }
            //OrderDate: {
            //    title: 'OrderDate'
            //},
            //Fk_Status: {
            //    title: 'Status'
            //}
        }
    });


    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    //    $('#SearchVendorContainer').jtable('load', {
    //        Pk_Vendor: $('#txtdlgVendorNo').val()
    //    });

    //});

    ////Load all records when page is first shown
    //$('#LoadRecordsButton').click();

    //$('#cmdVendSearch').click(function (e) {
    //    e.preventDefault();
    //    $('#SearchVendorContainer').jtable('load', {
    //        Pk_Vendor: $('#txtdlgVendorNo').val()
    //    });
    //});

    //$('#cmdVendorDone').click(function (e) {
    //    e.preventDefault();
    //    var rows = $('#SearchVendorContainer').jtable('selectedRows');
    //    $("#Fk_Vendor").val(rows[0].data.Pk_Vendor);
    //    $("#searchDialogVendor").modal("hide");
    //});

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchVendorContainer').jtable('load', {
            Pk_Vendor: $('#txtdlgVendorNo').val(),
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdVendSearch').click(function (e) {
        e.preventDefault();
        $('#SearchVendorContainer').jtable('load', {
            Pk_Vendor: $('#txtdlgVendorNo').val(),
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdVendorDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchVendorContainer').jtable('selectedRows');
        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        VendorID = rows[0].keyValue;
        $("#searchDialogVendor").modal("hide");
    });
}

function beforeModelSaveEx() {
    //   bFromSave = true;

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["RequestDate"] = $('#RequestDate').val();
    viewModel.data["Fk_Material"] = $('#Fk_Material').val();
    //viewModel.data["Fk_Vendor"] = $('#txtFk_Vendor').val();
    viewModel.data["Fk_Vendor"] = VendorID;




    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["RequestDate"] = $('#RequestDate').val();
    viewModel1.data["Fk_Vendor"] = $('#Fk_Vendor').val();
    viewModel1.data["Fk_Material"] = $('#Fk_Material').val();
    //viewModel.data["Fk_EnquiryNo"] = $('#Fk_EnquiryNo').val();
    viewModel1.data["Fk_Vendor"] = VendorID;

}


function afterOneToManyDialogShow(property) {
    
    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        _util.setDivPosition("searchMaterial", "block");
        _util.setDivPosition("divCreateProduct", "none");
    });
    
}

    function setUpMaterialSearch() {
        //Enquiry

        //cleanSearchDialog();

        //Adding Search fields
        var txtFieldMaterialNo = "<input type='text' id='txtdlgMaterialNo' placeholder='MaterialNo.' class='input-large search-query'/>&nbsp;&nbsp;";
        var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";
        $("#dlgSearchFieldsMaterial").append(txtFieldMaterialNo);
        $("#dlgSearchFieldsMaterial").append(txtFieldMaterialName);

        $('#SearchMaterialContainer').jtable({
            title: 'Material List',
            paging: true,
            pageSize: 8,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            defaultSorting: 'Name ASC',
            actions: {
                listAction: '/Material/MaterialListByFiter',
            },
            fields: {
                Pk_Material: {
                    title: 'MaterialID',
                    key: true
                },
                Name: {
                    title: 'Material Name',
                    edit: false
                }
               
            }
        });


          
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#SearchMaterialContainer').jtable('load', {
                Pk_Material: $('#txtdlgMaterialNo').val(),
                Name: $('#txtdlgMaterialName').val()
            });
        });
        $('#LoadRecordsButton').click();

        $('#cmdMatSearch').click(function (e) {
            e.preventDefault();
            $('#SearchMaterialContainer').jtable('load', {
                Pk_Material: $('#txtdlgMaterialNo').val(),
                Name: $('#txtdlgMaterialName').val()
            });
        });

        $('#cmdMatDone').click(function (e) {
            e.preventDefault();
            var rows = $('#SearchMaterialContainer').jtable('selectedRows');
            $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
            //VendorID = rows[0].keyValue;
            _util.setDivPosition("searchMaterial", "none");
            _util.setDivPosition("divCreateProduct", "block");
        });

 

    }

    function afterEditShow(viewObject) {
        curViewModel = viewObject.viewModel;
        var dNow = new Date();
        document.getElementById('RequestDate').value = (((dNow.getDate() + 1) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
        $('#RequestDate').datepicker({ autoclose: true });


        //var dNow = new Date();
        //document.getElementById('Inwd_date').value = (dNow.getMonth() + 1) + '/' + dNow.getDate() + '/' + dNow.getFullYear();

        objCurrstate = false;
        curViewModel = viewObject.viewModel;

        $('#tlbProducts').jtable({
            title: 'Material List',
            paging: true,
            pageSize: 8,
            defaultSorting: 'Name ASC',
            actions: {
                listAction: '/MaterialQuote/Bounce',
                deleteAction: '',
                updateAction: ''
            },
            fields: {
                slno: {
                    title: 'slno',
                    key: true,
                    list: false
                },
                Fk_Material: {
                    title: 'Material Id',
                    key: false,
                    list: true
                },
                txtFk_Material: {
                    title: 'Material'
                },
                Quantity: {
                    title: 'Quantity'

                },
                SpecialInformation: {
                    title: 'Special Information',
                    width: '2%'
                }

            }


        });
        var oSCuts = viewObject.viewModel.data.MaterialDetails();
        viewObject.viewModel.data["QuoteDetails"] = ko.observableArray();
        var i = 0;

        while (oSCuts[i]) {
            var oCut = new MaterialQuote();
            oCut.load(oSCuts[i].Pk_QuotationMaterial, oSCuts[i].txtFk_Material, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].SpecialInformation);
            viewObject.viewModel.data["QuoteDetails"].push(oCut);
            //$('#txtFk_Product').wgReferenceField("setData", oSCuts[i].Fk_Product);
            i++;
        }



        //$("#Inwd_date").val($.datepicker.formatDate("mm/dd/yy", new Date()));

        // $('#cmdCreate').attr('disabled', true);
        configureOne2Many("#cmdAddProduct", '#tlbProducts', "#cmdSaveProduct", viewObject, "QuoteDetails", "MaterialQuote", "_AddMaterial", function () { return new MaterialQuote() });

        //$('#cmdVendorSearch').click(function (e) {
        //    e.preventDefault();
        //    //setUpCustomerSearch();
        //    $("#searchDialogVendor").modal("show");
        //});

    }



