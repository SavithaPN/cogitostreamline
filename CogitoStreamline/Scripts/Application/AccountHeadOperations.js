﻿//Name          : AccountHead Operations ----javascript files 
//Description   : Contains the  AccountHead Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckPaperDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 22/10/2015
//Crh Number    : SL0009
//Modifications : 



var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Account Head List',
        paging: true,
        pageSize: 8,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/AccountHead/HeadNameListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_AccountHeadId: {
                title: 'Id',
                key: true,
                list: true
            },
            HeadName: {
                title: 'Name',
                width: '25%'
            },
            Description: {
                title: 'Description',
                width: '25%'
            }

        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_AccountHeadId: $('#txtHeadID').val(),
            HeadName: $('#txtHeadName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckHeadDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("HeadName", $('#HeadName').val());
    oResult = _comLayer.executeSyncAction("AccountHead/HeadDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Head Name Already added";
}









