﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Delivered Products List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobWorkReturns/JobsReturnsListByFiter',
            //deleteAction: '',            
            //createAction: '',
            //updateAction: ''
        },
        fields: {

            //Fk_OutSrcID = p.Fk_OutSrcID,
            //RecdDate = DateTime.Parse(p.RecdDate.ToString()).ToString("dd/MM/yyyy"),
            //PrdName = p.PrdName,
            //TypeofPrd=p.TypeofPrd,


            Pk_ID: {
                title: 'JW Delv.No.',
                key: true,
                list: false,
                width: '4%'
            },
            Customer: {
                title: 'Customer',
                width: '4%'
            },
            Ret_Date: {
                title: 'Delv.Date',
                width: '4%'
            },
            Pk_ID: {
                title: 'DCNo',
                width: '4%'
            },
            Fk_OutSrcID: {
                title: 'Job-In No.', 
                width: '4%'
            },
            PrdName: {
                title: 'Prd.Name',
                width: '10%'
            },
            TypeofPrd: {
                title: 'Type of Prd.',
                width: '10%'
            },

            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_ID = row.record.Pk_ID;
                        _comLayer.parameters.add("Pk_ID", Pk_ID);
                        _comLayer.executeSyncAction("JobWorkReturns/JobInDelRep", _comLayer.parameters);
                        var strval = "ConvertPDF/JobInDelRep" + Pk_ID + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////



                        /////////////////////////


                    });
                    return button;
                }
            },
            //////////////////////////////////////////////////
            //Details: {
            //    title: 'Details',
            //    width: '5%',
            //    sorting: false,
            //    edit: false,
            //    create: false,
            //    listClass: 'child-opener-image-column',
            //    display: function (data) {
            //        //Create an image that will be used to open child table
            //        // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
            //        var $img = $("<i class='icon-users'></i>");
            //        //Open child table when user clicks the image
            //        $img.click(function () {
            //            $('#MainSearchContainer').jtable('openChildTable',
            //                        $img.closest('tr'),
            //                        {
            //                            title: 'Materials List',
            //                            actions: {
            //                                listAction: '/PartJobs/TaskGetRec?Pk_ID=' + data.record.Pk_ID

            //                            },
            //                            fields: {
            //                                MaterialName: {
            //                                    title: 'MaterialName'
            //                                },

            //                                ReelNo: {
            //                                    title: 'ReelNo',
            //                                    key: true,
            //                                },
            //                                Weight: {
            //                                    title: 'Issued Qty.',
            //                                    key: true,
            //                                },
            //                                Length: {
            //                                    title: 'Length',
            //                                    key: true,
            //                                },
            //                                Width: {
            //                                    title: 'Width',
            //                                    key: true,
            //                                },
            //                                LayerWt: {
            //                                    title: 'LayerWt',
            //                                    key: true,
            //                                    edit: true,
            //                                },

            //                            },
            //                            formClosed: function (event, data) {
            //                                data.form.validationEngine('hide');
            //                                data.form.validationEngine('detach');
            //                            }
            //                        }, function (data) { //opened handler
            //                            data.childTable.jtable('load');
            //                        });
            //        });
            //        //Return image to show on the person row
            //        return $img;
            //    }
            //},

            ///////////////////////////////////////////////
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ID: $('#txtPkPONo').val(),
            FromDate: $('#TxtFromDate').val(),
            ToDate: $('#TxtToDate').val(),
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#TxtFromDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            FromDate: $('#TxtFromDate').val()
        });
    });
    $('#TxtFromDate').datepicker({
        autoclose: true
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtDate').datepicker({autoclose: true });


    $('#Pk_ID').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            $('#MainSearchContainer').jtable('load', {
                Pk_ID: $('#txtPkPONo').val()
            });
        }
    });

    $('#dtDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            FromDate: $('#TxtFromDate').val()
        });
    });

    $('#cmdPrint').click(function (e) {
        e.preventDefault();
        var FromDate = document.getElementById('TxtFromDate').value;
        var ToDate = document.getElementById('TxtFromDate').value;

        _comLayer.parameters.add("FromDate", FromDate);
        _comLayer.parameters.add("ToDate", ToDate);
        _comLayer.executeSyncAction("OutSource/JobRepDates", _comLayer.parameters);
        var strval = "ConvertPDF/JobWork" + Pk_ID + ".pdf"
        window.open(strval, '_blank ', 'width=700,height=250');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("MatCategory", "getMatCategory", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("MatCategory", "getMatCategory", null);


}


function afterNewShow(viewObject) {


    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });


    $('#cmdVendorMSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(800);
        $("#searchDialog").height(500);

    });

    $('#TypeofPrd').change(function () {
        if ($('#TypeofPrd').val() != '') {
            loadComboProduct('#PrdName', $('#TypeofPrd').val());
            $('#PrdName').change();
        }
    });

    $('#PrdName').change(function () {
        if ($('#PrdName').val() != '') {
            //loadComboCity('#Fk_City', $('#PrdName').val());
            //$('#Fk_City').change();
           // alert('hhh');
        }
    });

}
function loadComboProduct(combo, parent) {
    var oResult = getProdTypeComboValues(parent);
    $(combo).empty();

    var option = $("<option value=''>Select</option>");
    $(combo).append(option);

    if (oResult.Result="OK") {
        var i = 0;

        while (oResult.Records[i]) {

            var option = $("<option value='" + oResult.Records[i].Pk_MaterialCategory + "'>" + oResult.Records[i].Name + "</option>");
            $(combo).append(option);
            i++;
        }
    }
}
function getProdTypeComboValues(parent) {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("Pk_MaterialCategory", parent);

    if (parent != 2) {
        var sPath = _comLayer.buildURL("MaterialCategory", null, "MCatListByFiter");
        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

        var oResultObject = oResult;

        return oResultObject;
    }
}
function StatusChange(selectObj) {
    var selectIndex = selectObj.selectedIndex;
    var selectValue = selectObj.options[selectIndex].text;
    document.getElementById('FkStatus1').value = selectIndex;

}

function PrdTypeChange(selectObj) {
    var selectIndex = selectObj.selectedIndex;
    var selectValue = selectObj.options[selectIndex].text;
    document.getElementById('TypeofPrd1').value = selectIndex;

}
function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;


    viewModel.data["Ret_Date"] = $('#dtDate').val();
    viewModel.data["TypeofPrd"] = $('#TypeofPrd1').val();
    viewModel.data["Fk_PJID"] = $('#Fk_PJID').val();
    viewModel.data["LayerWeight"] = $('#LayerWeight').val();
    viewModel.data["WtDiff"] = $('#WtDiff').val();
    viewModel.data["RecdJobStatus"] = $('#FkStatus1').val();


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Ret_Date"] = $('#dtDate').val();
    viewModel1.data["TypeofPrd"] = $('#TypeofPrd1').val();
    viewModel1.data["Fk_PJID"] = $('#Fk_PJID').val();
    viewModel1.data["LayerWeight"] = $('#LayerWeight').val();
    viewModel1.data["WtDiff"] = $('#WtDiff').val();
    viewModel1.data["RecdJobStatus"] = $('#FkStatus1').val();

}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;
    var TaxVal = Number(GTot * 0.06);
    document.getElementById('CGST').value = TaxVal;
    document.getElementById('SGST').value = TaxVal;

    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;


    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/OutSource/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            }
        }
    });

    if ($.trim($('#TaxType').val()) == "VAT") {
        $("#VAT").prop("checked", true);
    }
    else if ($.trim($('#TaxType').val()) == "CST") {
        $("#CST").prop("checked", true);




    }

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "OutSource", "_AddMaterial", function () { return new IssuedMaterial() });


    //var POstatus = viewObject.viewModel.data["Fk_Status"]();

    //if (POstatus == "4") {
    //    $("#chkCloseJC").attr('checked', true);
    //}
}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Jobs-In Search");

    //Adding Search fields
    var txtFieldJCID = "<input type='text' id='txtdlgJCID' placeholder='JW NO' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldJCID);
    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Jobs-In List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PartJobs/TaskGetRec',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },

        //Pk_ID = p.Pk_ID,
        //MaterialName = p.Name,
        //CustomerName = p.CustomerName,
        //Weight = p.RecdWeight,
        //Width = p.Width,
        //Length = p.Length,
        //LayerWt = p.LayerWt,
        //Description = p.Description,
        //ExpectedNos = p.AppNos,
        //ReelNo = p.ReelNo,
        //RecdDate = DateTime.Parse(p.RecdDate.ToString()).ToString("dd/MM/yyyy"),
        fields: {
            Pk_ID: {
                title: 'Job Work ID',
                key: true,
                width: '5%'
            },
            CustomerName: {
                title: 'Customer',
                edit: false,
                width: '7%'
            },
            RecdDate: {
                title: 'RecdDate ',
                width: '3%'
            },
            MaterialName: {
                title: 'Issued Mat.',
                width: '5%'
            },
            Description: {
                title: 'Job Desc.',
                width: '7%'
            },
            ExpectedNos: {
                title: 'Exptd Output Nos.',
                width: '7%'
            },
            Weight: {
                title: 'Recd.Wt',
                width: '7%'
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            Pk_ID: $('#txtdlgJCID').val(),            
            Customer: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            Pk_ID: $('#txtdlgJCID').val(),
            Customer: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        //$('#Fk_OutSrcID').wgReferenceField("setData", rows[0].keyValue);
        //$('#Fk_OutSrcID').wgReferenceField("setData", rows[0].data.Pk_ID);
        document.getElementById('Fk_PJID').value = rows[0].data.Pk_ID;

        document.getElementById('AppNos').value = rows[0].data.ExpectedNos;

        //document.getElementById('RecdWeight').value = rows[0].data.Weight;

        document.getElementById('Description').value = rows[0].data.Description;

        $("#searchDialog").modal("hide");
        $("#RecdPrdWeight").focus();

    });

}

function setUpIssueMaterialSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter?MaterialCategory=Paper',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            Pk_PaperStock: {
                title: 'PKStock',
                key: false,
                width: '2%',
                list: false
            },
            RollNo: {
                title: 'Reel No',
                edit: false,
                width: '2%'
            },
            Name: {
                title: 'Material Name',
                edit: false
            },
            Quantity: {
                title: 'Ex.Stk.Qty',
                edit: false,
                width: '2%'
            },
            GSM: {
                title: 'GSM',

                list: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('IssueWeight').value = rows[0].data["Quantity"];

        document.getElementById('ReelNo').value = rows[0].data["RollNo"];
        document.getElementById('GSM').value = rows[0].data["GSM"];
        //document.getElementById('Ply').value = 1;


        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Length").focus();
    });



}

//var addl = Math.Round(Convert.ToDecimal(oPart.Length) * Convert.ToDecimal(oPart.Width), 2);
//layerWeight = Convert.ToDecimal((addl * Convert.ToDecimal(oLayerProperties["gsm"]) / 100) * Convert.ToDecimal(oLayerProperties["rate"]));    //////////////////rate is takeup factor value

function CheckWt() {
    //var RecdWeight = document.getElementById('RecdWeight').value;
    var RetPrdWeight = document.getElementById('RetPrdWeight').value;
    //var GSM = document.getElementById('GSM').value;
    //var RWt = document.getElementById('IssueWeight').value;

    //var addl = Math.round(Number(Length) * Number(Width), 2);
    //var WeightDiff = Number(RecdWeight) - Number(RetPrdWeight);



    //document.getElementById('WtDiff').value = WeightDiff;
}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_OutSrcID').wgReferenceField({
        keyProperty: "Fk_OutSrcID",
        displayProperty: "Pk_ID",
        loadPath: "OutSource/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    //$('#Fk_Indent').wgReferenceField({
    //    keyProperty: "Fk_Indent",
    //    displayProperty: "Fk_Indent",
    //    loadPath: "MaterialIndent/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        $('#Fk_OutSrcID').wgReferenceField("setData", viewModel.data["Fk_OutSrcID"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
        //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {

            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.ReelNo == curViewModel.data["Materials"]()[i].data.ReelNo) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}
function setUpIndentSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader1").text("Indent Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Indent List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialIndent/MaterialIndentPOList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true
            },

            MaterialIndentDate: {
                title: 'Indent Date'
            },
            //CustomerName=p.CustomerName,
            //MaterialName=p.Name,
            CustomerName: {
                title: 'CustomerName'
            },
            MaterialName: {
                title: 'MaterialName'
            },
            Quantity: {
                title: 'Qty'
            },
            //StateName: {
            //    title: 'Status'
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val(),
            Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_Indent').value = rows[0].keyValue;
        IndentNo = rows[0].keyValue;

        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}
function CalcAmt() {
    //  curViewModel = viewObject.viewModel;
    if (objContextEdit == false) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));


        }

        else {
        }
    } else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
        }
        else {
        }



    }
}

function radioClass1(intval) {
    if (intval == 1) {


        document.getElementById('TaxType').value = "VAT";
        if (FORM_CT3.checked == true)
        { document.getElementById('ED').value = Number($('#GrandTotal').val()); }
        else {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
        }

        FORM_CT3.style.visibility = 'visible';
        FORM_H.style.visibility = 'visible';

        //    fct3.style.visibility = 'hidden';
        fct3.style.visibility = 'visible';
        fh.style.visibility = 'visible';

        if (Number($('#ED').val()) > 0) {
            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
            //  NETVALUE
        }

    }
    else if (intval == 2) {

        document.getElementById('TaxType').value = "CST";

        FORM_H.style.visibility = 'hidden';

        fh.style.visibility = 'hidden';


        if (Number($('#ED').val()) > 0) {

            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
        }

    }


}

