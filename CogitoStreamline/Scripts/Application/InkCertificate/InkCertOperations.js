﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
var CharID;


//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Certification List',
        paging: true,
        pageSize: 8,
        actions: {
            listAction: 'InkCertificate/InkCertListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''

        },
        fields: {
            Pk_Id: {
                title: 'Cert.Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Mat.Name',
                width: '25%'
            },
            BatchNo: {
                title: 'BatchNo',
                width: '25%'
            },
            //InvDate: {
            //    title: 'InvDate',
            //    width: '25%',
            //    list: false
            //},
            //VendorName: {
            //    title: 'VName',
            //    width: '25%'
            //},
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        JCNO = row.record.Pk_Id;
                        _comLayer.parameters.add("JCNO", JCNO);
                        _comLayer.executeSyncAction("InkCertificate/InkCert", _comLayer.parameters);
                        var strval = "ConvertPDF/ICertificate" + JCNO + ".pdf"

                        window.open(strval, '_blank ', 'width=700,height=250');
                        //return true;
                        ///////////////////////////


                    });
                    return button;
                }

            },

        },
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Id: $('#txtCust').val(),
            Name: $('#txtName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');


    });
 

}

function afterNewShow(viewObject) {
    //var dNow = new Date();
    //document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Parameters List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InkCertificate/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {

            Pk_CharID: {
                title: 'Pk_CharID',
                key: true,
                list: false
            },
            Fk_Characteristics: {
                title: 'Param.Name',                
                list: true
            },
          
            Spec_Target: {
                title: 'Target'
            },
            Spec_Tolerance: {
                title: 'Result'
            },
            Results: {
                title: 'Remarks',
                width: '1%'            
        },

            Status: {
                title: 'Status',
                width: '1%'
            },
        }
    });

   
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "InkCertificate", "_AddMaterial", function () { return new IssuedMaterial() });

 

    //Calc();
    $('#cmdVendorSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });

    $('#cmdMaterialMSearch').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        $("#searchIndentDialog").modal("show");
      
        $("#searchIndentDialog").css("top", "100px");
        $("#searchIndentDialog").css("left", "350px");
    });
  
}

function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchHeader1").text("Ink Search");

    //Adding Search fields
    //var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    //$("#dlgMaterialSearchFields").append(txtFieldMaterialName);

    $('#IndentSearhContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Ink/InkListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name'
            }
            ,
            Fk_UnitId: {
                title: 'Unit'
            }
            ,

            Fk_Color: {
                title: 'Color'
            }
            ,
            Brand: {
                title: 'Brand'
            }


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgIndent').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Name: $('#txtdlgIndent').val()
        });
    });



    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');



        $("#searchIndentDialog").modal("hide");
        $('#txtFkMaterial').wgReferenceField("setData", rows[0].keyValue);


        $("#searchIndentDialog").modal("hide");
     

    });
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["InvDate"] = $('#dtDate').val();
    viewModel.data["Fk_Vendor"] = VendorID;
    

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["InvDate"] = $('#dtDate').val();
    viewModel1.data["Fk_Vendor"] = VendorID;
   

  
}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;
    var TaxVal =  Number(GTot * 0.06);
    document.getElementById('CGST').value = TaxVal;
    document.getElementById('SGST').value = TaxVal;

    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;


    $('#tlbMaterials').jtable({
        title: 'Parameters List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InkCertificate/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {

            Pk_CharID: {
                title: 'Pk_CharID',
                key: true,
                list: false
            },
            txtFMaterial:{
                title: 'Param.Name',
                list: true
            },

            Spec_Target: {
                title: 'Target'
            },
            Spec_Tolerance: {
                title: 'Tolerance'
            },
            Results: {
                title: 'Result',
                width: '1%'
            },


        }
    });

    //if ($.trim($('#TaxType').val()) == "VAT") {
    //    $("#VAT").prop("checked", true);
    //}
    //else if ($.trim($('#TaxType').val()) == "CST") {
    //    $("#CST").prop("checked", true);

        

        
    

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        //Pk_IdDet, Pk_CharID, Fk_Characteristics, txtFMaterial, Spec_Target, Spec_Tolerance, Results
        oCut.load(oSCuts[i].Pk_IdDet, oSCuts[i].Pk_CharID, oSCuts[i].Fk_Characteristics, oSCuts[i].txtFMaterial, oSCuts[i].Spec_Target, oSCuts[i].Spec_Tolerance, oSCuts[i].Results);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }
  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "InkCertificate", "_AddMaterial", function () { return new IssuedMaterial() });


 
}

function Clear() {
    //document.getElementById('TestMethod').value = "";
    document.getElementById('Spec_Target').value = "";

    //document.getElementById('Tolerance').value = "";

    document.getElementById('Spec_Tolerance').value = "";
    //document.getElementById('MinVal').value = "";
    //document.getElementById('MaxVal').value = "";
    //document.getElementById('AvgVal').value = "";
    //document.getElementById('Acceptance').value = "";
    document.getElementById('Results').value = "";
    //document.getElementById('Result2').value = "";
    //document.getElementById('Result3').value = "";
    //document.getElementById('Result4').value = "";
    //document.getElementById('Result5').value = "";
    //document.getElementById('Result6').value = "";
    //document.getElementById('Remarks').value = "";
    document.getElementById('Status').value = "";
}


function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true,
                width:'2%'
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Parameters List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InkParams/InkCharListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
      
        fields: {
            Pk_CharID: {
                title: 'Id',
                key: true
            },
            Name: {
                title: 'Name'
            },
            //StdVal: {
            //    title: "StdVal"
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtSMaterial').val(),
            //Name: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtSMaterial').val(),
            //Name: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        document.getElementById('txtFk_Material').value = rows[0].data["Name"];
        document.getElementById('Pk_CharID').value = rows[0].keyValue;
        CharID = rows[0].keyValue;
        //document.getElementById('StdVal').value = rows[0].data.StdVal;
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");

        CalcTarget();
        $("#Spec_Tolerance").focus();

    });
   


}

function CalcTarget() {

    if (CharID == 1) {
        document.getElementById('Spec_Target').value = "20";
    
    }
         
    else if (CharID == 2) {
        document.getElementById('Spec_Target').value = "Should match with Approved Shade ";
        document.getElementById('Spec_Tolerance').value = "Approved Shade ";
      
    }
    
    else if (CharID == 3) {
        document.getElementById('Spec_Target').value = "Neutral Ph=7 ~ > 6 && < 8  ";
    }
       
    else if (CharID == 4) {
        document.getElementById('Spec_Target').value = "As Per Standard";
        document.getElementById('Spec_Tolerance').value = "As Per Standard";
    }
    else if (CharID == 5) {
        document.getElementById('Spec_Target').value = "Received or Not";
        document.getElementById('Spec_Tolerance').value = "Received or Not";
    }
}


function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        Clear();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFkMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });



    if (viewModel.data != null) {
        $('#txtFkMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
     //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();


}

function CheckAccept() {

    //var R1 = document.getElementById('Result1').value;
    //var R2 = document.getElementById('Result2').value;
    //var R3 = document.getElementById('Result3').value;
    //var R4 = document.getElementById('Result4').value;
    //var R5 = document.getElementById('Result5').value;
    //var R6 = document.getElementById('Result6').value;

    //var arr = Math.max(R1, R2, R3, R4, R5, R6);
    //var arr1 = Math.min(R1, R2, R3, R4, R5, R6);
    //var arr2 = [R1, R2, R3, R4, R5, R6];


    ////alert("Max " + arr);
    ////alert("Min " + arr1);


    //var total = 0;
    //for (var i = 0; i < arr2.length; i++) {
    //    total += parseInt(arr2[i]);
    //}
    //var avg = Math.round(Number(total) / Number(arr2.length));
    ////alert("Avg " + avg);

    //document.getElementById('MinVal').value = arr1;
    //document.getElementById('MaxVal').value = arr;
    //document.getElementById('AvgVal').value = avg;



    var MaxRange = document.getElementById('Spec_Target').value;
    var R1 = document.getElementById('Spec_Tolerance').value;  //// result value
    CharID = document.getElementById('Pk_CharID').value;

    if (CharID == 1) {

        if (Number(R1) > 0) {

            if (Number(R1) > 0) {
                if (Number(R1) < Number(MaxRange)) {
                    document.getElementById('Status').value = "No";
                    $("#Fk_No").prop("checked", true);
                    $("#Results").focus();

                }

                else {
                    document.getElementById('Status').value = "Yes";
                    $("#Fk_Yes").prop("checked", true);
                    $("#Results").focus();

                }
            }

        }
    }

    if (CharID == 3) {
        if (Number(R1) > 0) {

            if (Number(R1) < 6 || Number(R1) > 8) {
                    
                document.getElementById('Status').value = "No";
                $("#Fk_No").prop("checked", true);
                $("#Results").focus();

            }

            else {
                document.getElementById('Status').value = "Yes";
                $("#Fk_Yes").prop("checked", true);
                $("#Results").focus();

            }
        }

    }
        
    }



function radioClass(str1) {
    if (str1 == "Yes") {
        document.getElementById('Status').value = str1;
    }
    else if (str1 == "No") {
        document.getElementById('Status').value = str1;
    }
}