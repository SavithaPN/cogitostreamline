﻿var ssum = 0;
var samt = 0;
function IssuedMaterial() {
    this.data = new Object();
    this.data["Pk_CharID"] = "";
    this.data["Fk_Characteristics"] = "";
    this.data["txtFk_Material"] = "";
    this.data["Spec_Target"] = "";
    this.data["Spec_Tolerance"] = ""
    this.data["Results"] = "";
    this.data["Status"] = $('#Status').val();

    this.updateValues = function () {
        this.data["Pk_CharID"] = $('#Pk_CharID').val();
        this.data["Fk_Characteristics"] = $('#txtFk_Material').val();
        this.data["Spec_Tolerance"] = $('#Spec_Tolerance').val();
        this.data["Spec_Target"] = $('#Spec_Target').val();
        this.data["Results"] = $('#Results').val();
        this.data["Status"] = $('#Status').val();

    };
    this.load = function (Pk_IdDet, Pk_CharID, Fk_Characteristics, txtFMaterial, Spec_Target, Spec_Tolerance, Results,Status) {
        this.data["Pk_CharID"] = Pk_CharID;
        this.data["Fk_Characteristics"] = Fk_Characteristics;
        this.data["txtFMaterial"] = txtFMaterial;
        this.data["Spec_Target"] = Spec_Target;
        this.data["Spec_Tolerance"] = Spec_Tolerance;
        this.data["Results"] = Results;
        this.data["Status"] = Status;
    };
}
