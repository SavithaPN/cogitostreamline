﻿//Name          : Activity Permission Operations ----javascript files
//Description   : Contains the  Activity Permission Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckActivity PermissionDuplicate(duplication checking) .
//Author        : CH. V. N. Ravi Kumar
//Date 	        : 07/11/2015
//Crh Number    : 
//Modifications : 

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Activity Permission List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/ActivityPermission/ActivityPermissionListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_ActivityPermission: {
                title: 'Id',
                key: true,
            },
            ActivityPermissionName: {
                title: 'Name'
            }

        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ActivityPermission: $('#TxtPkActivityPermission').val(),
            ActivityPermissionName: $('#TxtActivityPermissionName').val()
        });

    });
    $('#TxtPkActivityPermission').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_ActivityPermission: $('#TxtPkActivityPermission').val(),
                ActivityPermissionName: $('#TxtActivityPermissionName').val()
            });
        }

    });

    $('#TxtActivityPermissionName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_ActivityPermission: $('#TxtPkActivityPermission').val(),
                ActivityPermissionName: $('#TxtActivityPermissionName').val()
            });
        }

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}


function CheckActivityPermissionDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("ActivityPermissionName", $('#ActivityPermissionName').val());
    oResult = _comLayer.executeSyncAction("ActivityPermission/ActivityPermissionDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Activity Permission Name Already added";

}


