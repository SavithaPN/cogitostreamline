﻿
//Name          : Inward Operations ----javascript files
//Description   : Contains the  material indent Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. Check date validations
//Author        : shantha
//Date            :   03/09/2015
//CRH Number      :   SL0047
//Modifications : 


var curViewModel = null;
var bFromSave = false;
var bEditContext = false;



function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Inward List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialInward/MaterialInwardListByFiter',
            updateAction: '',
            deleteAction: '',
            createAction: ''
        },
        fields: {
            Pk_Inward: {
                title: 'Inward No.',
                key: true
            },

            Inward_Date: {
                title: 'Date'
            },
            Fk_QualityCheck: {
                title: 'QualityCheck'
            },
            Fk_Indent: {
                title: 'IndentNumber'
            },
            VendorName: {
                title: 'Vendor'
            },
            Fk_Material1: {
                title: 'Material Name'
            },
            Quantity: {
                title: 'Quantity'
            },
            Price: {
                title: 'Price'
            },
            Weight: {
                title: 'Weight'
            },
            UnitName: {
                title: 'Unit'
            }
        }
    });

    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });

    $('#dtPInwardDate').datepicker({
        autoclose: true
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Inward: $('#InwardNumber').val(),
            Vendor: $('#VendorName').val(),
            Material: $('#Material').val(),
            FromDate: $('#TxtFromDate').val(),
            ToDate: $('#TxtToDate').val(),
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    $('#dtPInwardDate').datepicker({ autoclose: true });



}

function afterNewShow(viewObject) {

    bEditContext = false;
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('dtPInwardDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#dtPInwardDate').datepicker({ autoclose: true });



    $('#cmdQualitySearch').click(function (e) {
        e.preventDefault();
        setUpQualitySearch();
        $("#searchQualityDialog").modal("show");
    });
}



function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    if ($("#txtFk_Material").val() == "") {
        $("#txtFk_Material").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#dtPInwardDate').datepicker({
        autoclose: true
    });



}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function setUpQualitySearch() {
    //Branch

    cleanSearchDialog();

    $("#srchsHeader").text("Quality Check Search");

    //Adding Search fields
    //var txtFieldQualityName = "<input type='text' id='txtdlgQualityName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    //$("#dlgSearchQualityFields").append(txtFieldQualityName);

    $('#QualitySearhContainer').jtable({
        title: 'QualityCheck List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/CheckQuality/QualityList'
        },
        fields: {
            Pk_QualityCheck: {
                title: 'Check No.',
                key: true
            },
            CheckDate: {
                title: 'Date'
            },
            Vendor: {
                title: 'Vendor Name'
            },
            InvoiceNo: {
                title: 'InvoiceNumber'
            },
            IndentNo: {
                title: 'IndentNumber'
            },
            MaterialID: {
                title: 'MaterialID',
                list:false
            },
            Mat_Name: {
                title: 'Material',
                List: true
            },
            PendQty: {
                title: 'P.Qty'
            },
            TanentName: {
                title: 'Branch',
                list: false
            },
            
            SQuantity: {
        title: 'Stock in Branch',
        list:false
            },
            UnitName: {
                title:"Unit"
            }
        }
    });
    $('#txtdlgQDate').datepicker({ autoclose: true });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#QualitySearhContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdQCheckSearch').click(function (e) {
        e.preventDefault();
        $('#QualitySearhContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val(),
            DDate: $('#txtdlgQDate').val(),
            Invno: $('#txtdlgInvoice').val()
        });
    });



    $('#cmdQCheckDone').click(function (e) {
        e.preventDefault();
        var rows = $('#QualitySearhContainer').jtable('selectedRows');
        $("#searchQualityDialog").modal("hide");

        document.getElementById('txtQualityCheck').value = rows[0].keyValue;
        document.getElementById('TxtIndent').value = rows[0].data.IndentNo;
        //document.getElementById('TxtMaterial').value = rows[0].data.Fk_Material;
        //document.getElementById('TxtMaterialId').value = rows[0].data.Fk_Material1;

        $('#txtFk_Material').wgReferenceField("setData", rows[0].data.MaterialID);

        document.getElementById('Quantity').value = rows[0].data.PendQty;
        document.getElementById('SQuantity').value = rows[0].data.SQuantity;
        document.getElementById('Unit').value = rows[0].data.UnitName;
        //document.getElementById('Unit1').value = rows[0].data.UnitName;
        //document.getElementById('Unit2').value = rows[0].data.UnitName;
        document.getElementById('TanentName').value = rows[0].data.TanentName;

    });
}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Inward_Date"] = $('#dtPInwardDate').val();
    viewModel.data["Fk_QualityCheck"] = $('#txtQualityCheck').val();
    // viewModel.data["Fk_Material"] = $('#txtFk_Material').val();

    viewModel.data["Fk_Indent"] = $('#TxtIndent').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Inward_Date"] = $('#dtPInwardDate').val();
    viewModel1.data["Fk_QualityCheck"] = $('#txtQualityCheck').val();
    // viewModel1.data["Fk_Material"] = $('#txtFk_Material').val();

    viewModel.data["Fk_Indent"] = $('#TxtIndent').val();
}


function ReferenceFieldNotInitilized(viewModel) {

    $('#txtFk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        //       $('#txtFk_IndentNumber').wgReferenceField("setData", viewModel.data["Fk_IndentNumber"]);
        $('#txtFk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgSearchQualityFields').empty();

}


function checkstock() {
    if (objContextEdit == false) {
        if (Number($('#Quantity').val()) < Number($('#InwQuantity').val())) {
            return "* " + "Inward Quantity cannot Exceed Quality Checked Qty :" + $('#Quantity').val();
        }
        else {

        }
    }
}