﻿
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Electrical Spares List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/ElectricalSpares/SparesListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: ' Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name',
                width: '20%'
            },
            Description: {
                title: 'Description',
                width: '20%'
            },
            Company: {
                title: 'Make',
                width: '10%'
            },
            Brand: {
                title: 'Qty.In Machine',
                width: '10%'
            },
            MaterialType: {
                title: 'Location',
                width: '5%'
            },
            
            
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtPaperName').val(),
            Company: $('#TxtPaperDeckle').val(),
           
        });

    });


    $('#TxtPaperName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Company: $('#TxtPaperDeckle').val(),
            });
        }
    });

    $('#TxtPaperType').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Company: $('#TxtPaperDeckle').val(),
            });
        }
    });



    $('#TxtPaperColour').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Company: $('#TxtPaperDeckle').val(),
            });
        }
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#cmdToMaterials').click(function (e) {
        e.preventDefault();
        document.location = "/" + "Material";
    });

}

function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Electric";
}

function CheckPaperDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("ElectricalSpares/SparesDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + " Name Already added";

}

//function beforeModelSaveEx() {
//    var viewModel = _page.getViewByName('New').viewModel;
//    //viewModel.data["Name"] = $.trim($('#MILL option:selected').text()) + " " + $.trim($('#BF').val()) + "BF " + $('#PaperType  option:selected').text() + " " + $.trim($('#GSM').val()) + "GSM"
//    viewModel.data["Name"] = $.trim($('#GSM').val()) + "GSM" + $.trim($('#BF').val()) + "BF" + $.trim($('#Deckle').val()) + "D"



//}
