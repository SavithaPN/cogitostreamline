﻿var BoxID;
var QtyVal;
var curViewModel = null;
var fkbox;
var BName;
var MatID;


function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Paper List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Paper/PaperListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: ' Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name',
                width: '20%'
            },
            Type: {
                title: 'Type',
                width: '10%'
            },
            Mill: {
                title: 'Mill',
                width: '10%'
            },
            Deckle: {
                title: 'Deckle',
                width: '5%'
            },
            GSM: {
                title: 'GSM',
                width: '5%'
            },
            BF: {
                title: 'BF',
                width: '5%'
            },
            PaperColor: {
                title: 'Color'
            },
            Unit: {
                title: 'Unit'
            }
        }
    });
    cmdNew.style.visibility = 'hidden';
    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    //    $('#MainSearchContainer').jtable('load', {
    //        CustName: $('#txtCust').val(),
    //        Name: $('#txtName').val()
    //    });
    //});
    //$('#LoadRecordsButton').click();
    
    var ID = _util.getParameterByName("Material");
    var IDLen=ID.length;

    var bstr1 = ID.search(",");
    PkMat = ID.substr(0, bstr1);

    var bstr2 = ID.search("=");
    Inwd = ID.substr(bstr2+1, IDLen);

  

    if (ID == "")
    { }
    else if (ID != null) {


        _page.showView('New');
        var x = document.getElementById('divSearchArea');
        if (x.style.display == 'none') {
            x.style.display = 'block';
        } else {
            x.style.visibility = 'hidden';
        }

    }
    //$('#cmdNew').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();

        _page.showView('New');


    });
    //$("#cmdNew").attr('disabled', true);
    //cmdNew.style.visibility = 'hidden';

    document.getElementById('Fk_MaterialID').value = PkMat;

    document.getElementById('Inwd').value = Inwd;

    setUpPaperInwdSummary();

}


function afterNewShow(viewObject) {


    document.getElementById('Qty').value = 1;
    document.getElementById('Name').value = BName;


    $('#cmdSearch').click(function (e) {
        document.getElementById('FkBoxID').value = fkbox;

        e.preventDefault();
        setUpOrderSearch(viewObject);
        setUpPaperSummary(viewObject);
        setUpAssStk(viewObject);
    });
    $('#cmdReport').click(function (e) {
        document.getElementById('FkBoxID').value = fkbox;

        e.preventDefault();

        _comLayer.parameters.add("fkbox", fkbox);
        _comLayer.executeSyncAction("BOM/BomRep", _comLayer.parameters);
        var strval = "ConvertPDF/BomReport" + fkbox + ".pdf"

        ////////////////////////////


        var xhr = new XMLHttpRequest();
        var urlToFile = "ConvertPDF/BomReport" + fkbox + ".pdf"
        xhr.open('HEAD', urlToFile, false);
        xhr.send();

        if (xhr.status == "404") {
            alert('Data Not Available , File does not Exist');
            return false;


        } else {
            window.open(strval, '_blank ', 'width=700,height=250');
            return true;
        }


        /////////////////////////



    });

    $('#Qty').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            setUpOrderSearch(viewObject);
            setUpPaperSummary(viewObject);
            setUpAssStk(viewObject);
        }

    });
}

function setUpIssueList(viewObject) {
   
    $('#divPaperStock').jtable({
        title: 'Issue List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/PaperDistribution/IssueRec',
            //updateAction: ''
        },

        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },


            PkMaterial: {
                title: 'Material ID',
                key: false,
                list: true
            },

            Name: {
                title: 'Mat.Name'

            },
            MatIssueID: {
                title: 'Mat.Issue ID'

            },
            IssueDate: {
                title: 'IssueDate'

            },
            JobCardID: {
                title: 'JobCard ID'

            },
            JobDate: {
                title: 'Job Card Date'

            },
            OrderNo: {
                title: 'Order No'

            },
            Customer: {
                title: 'Customer'

            },
            Quantity: {
                title: 'Quantity',
                key: false
            },
           
        }
    });

    $('#divPaperStock').jtable('load', {
        Pk_Mat: $('#Fk_MaterialID').val(),
        DateValue: $('#InwdDate').val(),

    });

}

//------1 divPaperInwdSummary
//InwardRec



function setUpPaperInwdSummary(viewObject) {
    //BoxID = document.getElementById('FkBoxID').value;
    //QtyVal = document.getElementById('Qty').value;


    $('#divPaperInwdSummary').jtable({
        title: 'Inward Summary',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/PaperDistribution/InwardRec',
            //updateAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                document.getElementById('Name').value = $(this).data("record").Name;
                document.getElementById('Qty').value = $(this).data("record").Quantity;
                document.getElementById('InwdDate').value = $(this).data("record").InwDate;

                setUpIssueList();


                setUpIssueReturnList();

            });
        },
        //PkMaterial = p.Pk_Material,
        //PkInwd = p.Pk_Inward,
        //InwDate = p.Inward_Date != null ? DateTime.Parse(p.Inward_Date.ToString()).ToString("dd/MM/yyyy") : "",
        //Name = p.Name,
        //Quantity = p.Qty,
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            PkMaterial: {
                title: 'Material ID',
                key: false,
                list: true
            },
            Name: {
                title: 'Name',
                key: false
            },
            PkInwd: {
                title: 'Inwd.No'
            },
            InwDate: {
                title: 'InwDate',
                key: false
            },
         
            Quantity: {
                title: 'Quantity',
                list: true
            },
           
        }
    });
    //$('#divPaperInwdSummary').jtable('load');
    $('#divPaperInwdSummary').jtable('load', {
        Pk_Inward: $('#Inwd').val(),
        Pk_Mat: $('#Fk_MaterialID').val(),

    });

}


//------2 divPaperStock
//InwardRec

//function fillIssue()
//{
//    setUpIssueList();
//}
function setUpIssueReturnList(viewObject) {
    //BoxID = document.getElementById('FkBoxID').value;


    $('#divDeliveryschedule').jtable({
        title: 'Issue Return List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PaperDistribution/IssueReturnsRec',
           
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            PkMaterial: {
                title: 'PkMaterial',
                list: true

            },
            Name: {
                title: 'Mat.Name',
                list: true

            },
            Pk_IssueReturnMasterId: {
                title: 'Return ID',
                key: false
            },
            IssueReturnDate: {
                title: 'Return Date',
                key: false
            },
         
            Fk_IssueId: {
                title: 'Fk_IssueId',
                list: false
            },

            IssueDate: {
                title: 'IssueDate',
                list: false
            },
            ReturnQuantity: {
                title: 'ReturnQuantity',
                list: true
            },
          
        }
    });
    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    $('#divDeliveryschedule').jtable('load', {
        Pk_Mat: $('#Fk_MaterialID').val(),
        DateValue: $('#InwdDate').val(),
    });



}