﻿var ssum = 0;
var samt = 0;
function IssuedMaterial() {
    this.data = new Object();
    this.data["Name"] = "";
    this.data["Fk_Material"] = "";
    this.data["IssueWeight"] = "";
    this.data["ReelNo"] = "";
    this.data["Length"] = "";
    this.data["Width"] = "";
    this.data["AppNos"] = "";
    this.data["Height"] = "";
    this.data["LayerWt"] = "";
    this.data["Ply"] = "";
    this.updateValues = function () {
        this.data["Name"] = $('#txtFk_Material').val();
        this.data["RecdWeight"] = $('#IssueWeight').val();
        this.data["ReelNo"] = $('#ReelNo').val();
        this.data["Length"] = $('#Length').val();

        this.data["Width"] = $('#Width').val();
        this.data["AppNos"] = $('#AppNos').val();
        this.data["Height"] = $('#Height').val();
        this.data["LayerWt"] = $('#LayerWt').val();
        this.data["Ply"] = $('#Ply').val();
    };
    this.load = function (Pk_SrcDetID, Name, Fk_Material, RecdWeight, ReelNo, Length, Width, AppNos, LayerWt, Ply) {
        this.data["Pk_SrcDetID"] = Pk_SrcDetID;
        this.data["Name"] = Name;
        this.data["Fk_Material"] = Fk_Material;
        this.data["RecdWeight"] = RecdWeight;
        this.data["ReelNo"] = ReelNo;
        this.data["Length"] = Length;
        this.data["Width"] = Width;
        this.data["AppNos"] = AppNos;
      
        this.data["LayerWt"] = LayerWt;
        this.data["Ply"] = Ply;
    };
}
