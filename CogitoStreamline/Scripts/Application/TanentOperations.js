﻿//Name          : Unit Operations ----javascript files
//Description   : Contains the  Unit Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckUnitDuplicate(duplication checking) 
//Author        : shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0003
//Modifications : 


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'TanentList',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/WTenant/TenantListByFiter',
            deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Tanent: {
                title: 'Id',
                key: true,
            },
            TanentName: {
                title: 'Name'
            }
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            TanentName: $('#TxtTenantName').val()
        
        });

    });

    //$('#TxtPkUnit').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Unit: $('#TxtPkUnit').val(),
    //            UnitName: $('#TxtUnitName').val(),
    //            UnitType: $('#TxtUnitType').val()
    //        });
    //    }
    //});

    //$('#TxtName').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Unit: $('#TxtPkUnit').val(),
    //            UnitName: $('#TxtUnitName').val(),
    //            UnitType: $('#TxtUnitType').val()
    //        });
    //    }
    //});

    //$('#TxtUnitType').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Unit: $('#TxtPkUnit').val(),
    //            UnitName: $('#TxtUnitName').val(),
    //            UnitType: $('#TxtUnitType').val()
    //        });
    //    }
    //});

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });




}





function CheckTanentDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("TanentName", $('#Name').val());
    oResult = _comLayer.executeSyncAction("WTenant/TanentDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Name Already added";

}


