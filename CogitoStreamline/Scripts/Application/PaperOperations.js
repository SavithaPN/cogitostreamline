﻿

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Paper List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Paper/PaperListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: ' Id',
                key: true,
               width:'5%'
            },
            Name: {
                title: 'Name',
                width: '20%'
            },
            Type: {
                title: 'Type',
                width: '10%'
            },
            Mill: {
                title: 'Mill',
                width: '10%'
            },
            Deckle: {
                title: 'Deckle',
                width: '5%'
            },
            GSM: {
                title: 'GSM',
                width: '5%'
            },
            BF: {
                title: 'BF',
                width: '5%'
            },
            PaperColor: {
                title: 'Color'
            },
             Unit: {
                title: 'Unit'
            }
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtPaperName').val(),
            Type: $('#TxtPaperType').val(),
            Fk_Color: $('#TxtPaperColour').val(),
            GSM: $('#TxtPaperGSM').val(),
            BF: $('#TxtPaperBF').val(),
            Mill: $('#TxtPaperMill').val(),
            Deckle: $('#TxtPaperDeckle').val()
        });

    });


    $('#TxtPaperName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Type: $('#TxtPaperType').val(),
                Fk_Color: $('#TxtPaperColour').val(),
                GSM: $('#TxtPaperGSM').val(),
                BF: $('#TxtPaperBF').val(),
                Mill: $('#TxtPaperMill').val(),
                Deckle: $('#TxtPaperDeckle').val()
            });
        }
    });

    $('#TxtPaperType').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Type: $('#TxtPaperType').val(),
                Fk_Color: $('#TxtPaperColour').val()
            });
        }
    });

  

    $('#TxtPaperColour').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Type: $('#TxtPaperType').val(),
                Fk_Color: $('#TxtPaperColour').val()
            });
        }
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#cmdToMaterials').click(function (e) {
        e.preventDefault();
        document.location = "/" + "Material";
    });
    
   
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Color", "getColor", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Color", "getColor", null);

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("PaperType", "getPaperType", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("PaperType", "getPaperType", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Mill", "getMill", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Mill", "getMill", null);

}

function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Paper";
}

function CheckPaperDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("Paper/PaperDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Paper Name Already added";

}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    //viewModel.data["Name"] = $.trim($('#MILL option:selected').text()) + " " + $.trim($('#BF').val()) + "BF " + $('#PaperType  option:selected').text() + " " + $.trim($('#GSM').val()) + "GSM"
    viewModel.data["Name"] = $.trim($('#GSM').val()) + "GSM" + $.trim($('#BF').val()) + "BF"  + $.trim($('#Deckle').val()) + "D"


        
}
