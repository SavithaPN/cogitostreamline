﻿var ComboTextVal="";


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Invno List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CoaReport/COAListByFiter',
            //updateAction: '',
            //createAction: '',
            //deleteAction: ''
        },
           fields: {
            Pk_ID: {
                title: 'Pk_ID',
                key: true,
                list:false
            },
            RDate: {
                title: 'Date'
            },
            Fk_Invno: {
                title: 'Invno',                
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        JCNO = row.record.Pk_ID;
                        _comLayer.parameters.add("JCNO", JCNO);
                        _comLayer.executeSyncAction("CoaReport/COARep", _comLayer.parameters);
                        var strval = "ConvertPDF/COARep" + JCNO + ".pdf"

                        window.open(strval, '_blank ', 'width=700,height=250');
                        //return true;
                        ///////////////////////////


                    });
                    return button;
                }

            },
            //Fk_Invno: {
            //    title: 'Fk_Invno'
            //},
            //UOM: {
            //    title: 'UOM',
            //},

            //Specification: {
            //    title: 'Specification'
            //},
            //Result: {
            //    title: 'Result'
            //},
            //Remarks: {
            //    title: 'Remarks'
            //},
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Material: $('#TxtPkColor').val(),
            Name: $('#TxtColorName').val()
        });

    });
    //$('#TxtPkColor').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Material: $('#TxtPkColor').val(),
    //            Name: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //$('#TxtColorName').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Color: $('#TxtPkColor').val(),
    //            ColorName: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();
  

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

  setUpInvnoSearch();
}

function afterNewShow(viewObject) {

    var dNow = new Date();
    document.getElementById('dtRDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    $('#cmdInvnoSearch').click(function (e) {
        e.preventDefault();

        $("#searchCustomerDialog").modal("show");

    });


    $('#dtRDate').datepicker({ autoclose: true });
    
    $('#Parameter').change(function (e) {
        //e.preventDefault();
        document.getElementById('ParaID').value = $('#Parameter').Pk_ID;
    });

    $('#Parameter').change(function () {
        eID = document.getElementById("Parameter");
        ComboTextVal=eID.options[eID.selectedIndex].text;
        ParaID = eID.options[eID.selectedIndex].value;
       // _comLayer.parameters.add("JCode", FrcatID);
       // var oResult2 = _comLayer.executeSyncAction("/Stk_Transfer/GetTagCode", _comLayer.parameters);
        document.getElementById('ParaID').value = ParaID;
        //FrBr = document.getElementById('BrID').value;
    });
}

function setUpInvnoSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    //$("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Bills List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InvBilling/BillingListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },

        fields: {
            Pk_Invoice: {
                title: 'Invoice Number',
                key: true
            },

            //Invno: {
            //    title: 'InvNo'
            //},
            InvDate: {
                title: 'Inv Date'
            },
            //Fk_OrderNo: {
            //    title: 'OrderNo'
            //},
            Fk_Customer: {
                title: 'Customer'
            },
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Invoice: $('#txtdlgCustomerName').val(),
            Customer: $('#txtdlgCustomerNam').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Invoice: $('#txtdlgCustomerName').val(),
            Customer: $('#txtdlgCustomerNam').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#Fk_Invno').wgReferenceField("setData", rows[0].keyValue);

        //CustomerID = rows[0].keyValue;

        $("#searchCustomerDialog").modal("hide");
        //$("#ReferenceEnquiry").focus();
    });


    _page.getViewByName('New').viewModel.addAddtionalDataSources("Parameter", "getParameter", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Parameter", "getParameter", null);

}

function ReferenceFieldNotInitilized(viewModel) {

    //if (objContextEdit == true) {
    //    $('#cmdSearchMaterialsdetails').attr('disabled', true);
    //}
    //else if (objContextEdit == false) {
    //    $('#cmdSearchMaterialsdetails').attr('disabled', false);
    //}

    $('#Fk_Invno').wgReferenceField({
        keyProperty: "Fk_Invno",
        displayProperty: "Pk_Invoice",
        loadPath: "InvBilling/Load",
        viewModel: viewModel
    });

    //$('#txtFk_Material').wgReferenceField({
    //    keyProperty: "Fk_BoxID",
    //    displayProperty: "Name",
    //    loadPath: "BoxMaster/Load",
    //    viewModel: viewModel
    //});


    if (viewModel.data != null) {
        $('#Fk_Invno').wgReferenceField("setData", viewModel.data["Fk_Invno"]);
       // $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
    }



}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $('#SearchContainer').empty();

    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#searchBoxDialog').empty();
    $('#MainSearhContainer').append("<div id='searchBoxDialog'></div>");
}


function AddData() {

    var rows = "";
    var ParaID = document.getElementById("ParaID").value;
    var ParaName = ComboTextVal;
    var UOM = document.getElementById("UOM").value;
    var Specification = document.getElementById("Specification").value;
    var Result = document.getElementById("Result").value;
    var Remarks = document.getElementById("Remarks").value;
    //var Cost = document.getElementById("Cost").value;
    //var PapReq = document.getElementById("PReq").value;
    //var total = document.getElementById("Total").value;

    rows += "<tr><td>" + ParaID + "</td><td>" + ParaName + "</td><td>" + UOM + "</td><td>" + Specification + "</td><td>" + Result + "</td><td>" + Remarks + "</td><td>" 
    //  rows += "<tr><td>" + name + "</td><td>" + gender + "</td><td>" + age + "</td><td>" + city + "</td></tr>";
    $(rows).appendTo("#list tbody");

    document.getElementById("ParaID").value = "";
    document.getElementById("Parameter").value = "";
    document.getElementById("UOM").value = "";
    document.getElementById("Specification").value = "";
    document.getElementById("Result").value = "";
    document.getElementById("Remarks").value = "";
    //document.getElementById("Rate").value = "";
    //document.getElementById("BS").value = "";


    //FunCalc();
}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;

    this.COADtlsList = [];
    COADtlsList = getTableData($('#list'));

    var arrayLength = COADtlsList.length;

    if (typeof (viewModel.data["COADetails"]) == "undefined" || viewModel.data["COADetails"]() == "")
    { viewModel.data["COADetails"] = ko.observableArray(); }

    //if (that.viewModel.data[property]().indexOf(objContext) < 0) {

    //    that.viewModel.data[property].push(objContext);
    //}

    for (var i = 0; i < arrayLength; i++) {
        //curViewModel.data["BoxDetails"]()[i].data.Fk_Material
        var oCut = new COADetail();

        //this.partRec = JSON.parse(JSON.stringify(viewModel.data["JobDetails"]()[0]));
        oCut.Fk_ParaID = COADtlsList[i][0];
        oCut.ParaName = COADtlsList[i][1];
        oCut.UOM = COADtlsList[i][2];
        oCut.Specification = COADtlsList[i][3];
        oCut.Result = COADtlsList[i][4];
        oCut.Remarks = COADtlsList[i][5];

        viewModel.data["COADetails"]().push(oCut);

        //curViewModel.data["BoxDetails"]()[i].data.Fk_Material

        //Do something
    }

}

function getTableData(table) {
    var data = [];
    table.find('tr:gt(0)').each(function (rowIndex, r) {
        var cols = [];
        $(this).find('td').each(function (colIndex, c) {
            cols.push(c.textContent);
        });
        data.push(cols);
    });
    return data;
}
