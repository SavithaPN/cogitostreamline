﻿var baseURL = "Register";
var SelectedYear;
var SelectedMonth;
var dayCount = 0;
var MaximumDays;
var bSilent = false;

$(document).ready(function () {

    $('#cmdLoadAttendance').click(function (e) {
        e.preventDefault();
        loadRegister();
    });


    options = {
        pattern: 'yyyy-mmmm', // Default is 'mm/yyyy' and separator char is not mandatory
        selectedYear: 2013,
        startYear: 2013,
        finalYear: 2050,
        monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    };

    $('#txtMonth').monthpicker(options);

    $('#txtMonth').monthpicker().bind('monthpicker-click-month', function (e, month) {
        SelectedMonth = month;
    }).bind('monthpicker-change-year', function (e, year) {
        SelectedYear = year;
    });


    var d = new Date();
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";

    var currentMonth = month[d.getMonth()]; +1;
    var currentYear = d.getFullYear();

    SelectedMonth = currentMonth;
    SelectedYear = currentYear;

    $('#txtMonth').val(currentYear + "-" + currentMonth);



   
});

function loadNumberOfWorkingDays() {

    _comLayer.parameters.clear();
    _comLayer.parameters.add("Month",$('#txtMonth').val());
    var oResult = _comLayer.executeSyncAction("/Register/TotalWorkingdaysCount", _comLayer.parameters);

    //$('#NCC').text(oResult.data);
}

function loadRegister() {

    document.getElementById("SearchByChar").style.display = "block";

    $('#grdRegister').jtable({
        title: 'Attendance Register',
        paging: true,
        pageSize: 15,
        sorting: true,
        edit: false,
        actions: {
            listAction: '/Register/GetEmployees'
        },
        fields: getFields()
    });

    $('#grdRegister').jtable('load', {
        MonthYear: $('#txtMonth').val()
    });

    $('.charSearch').click(function () {
            if ($(this).text() != "ALL") {
            $('#grdRegister').jtable('load', {
                MonthYear: $('#txtMonth').val(),
                Char: $(this).text()
            });
        }
        else {
            $('#grdRegister').jtable('load', {
                MonthYear: $('#txtMonth').val()
            });
        }

        loadNumberOfWorkingDays();
    });

    loadNumberOfWorkingDays();

}

function getFields() {

    dayCount = 0;

    var oMainFieldsObject = new Object();

    var oRegisterPk = new Object();
    oRegisterPk.title = "Register";
    oRegisterPk.list = false;

    var oEmployeePk = new Object();
    oEmployeePk.title = "Employee Id";
    oEmployeePk.key = true;
    oEmployeePk.edit = false;
    oEmployeePk.sorting = false;

    var oEmployeeName = new Object();
    oEmployeeName.title = "Name";
    oEmployeeName.edit = false;
    oEmployeeName.sorting = false;
    oEmployeeName.display = function (data) {
        if (data.record.Id == "Working Day") {
            $txt = $("<a class='btn'>No:</a><a class='btn' id='NCC'></a>");
            return $txt;
        }
        else {
            return $("<label>" + data.record.Name + "</label>");
        }
    };

    oMainFieldsObject.Id = oEmployeePk;
    oMainFieldsObject.Name = oEmployeeName;
    oMainFieldsObject.PkRegister = oRegisterPk;



    var MonthYear = $('#txtMonth').val().split('-');

      var str = "";
      if (MonthYear[1] == "Jan")
                str = "01";

      else if (MonthYear[1] == "Feb")
                str = "02";
      else if (MonthYear[1] == "Mar")
                str = "03";
      else if (MonthYear[1] == "Apr")
                str = "04";

      else if (MonthYear[1] == "May")
                str = "05";

      else if (MonthYear[1] == "Jun")
                str = "06";
      else if (MonthYear[1] == "Jul")
                str = "07";
      else if (MonthYear[1] == "Aug")
                str = "08";
      else if (MonthYear[1] == "Sep")
                str = "09";

      else if (MonthYear[1] == "Oct")
                str = "10";

      else if (MonthYear[1] == "Nov")
                str = "11";
            else
                str = "12";

    var days = daysInMonth(str, MonthYear[0]);
    MaximumDays = days;
    //Now the number of days
    for (i = 1; i <= days; i++) {
        //dayCount = i;
        var oday = new Object();
        oday.title = i;
        oday.edit = false;
        oday.sorting = false;
        oday.width = '2%';
        oday.display = function (data) {

            dayCount++;

            if (dayCount > MaximumDays) dayCount = 1;

            var Key = "Day_" + dayCount;
            var CurValue = data.record[Key];
            var $txt = null;


            var d = new Date();
            var currentMonth = d.getMonth('mmmm') + 1;
            if (data.record.Id == "Working Day") {
                if (str == currentMonth) {
                    if (getDayName(dayCount) != "Su" && dayCount <= new Date().getDate()) {
                        if (CurValue == true)
                        { $txt = $("<a class='bntOn' style='font-size:small' id=Class" + dayCount + " Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">" + getDayName(dayCount) + "</a>"); }
                        else {
                            $txt = $("<a class='bntOff' style='font-size:small' id=Class" + dayCount + " Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">" + getDayName(dayCount) + "</a>");
                        }
                    }
                    else {
                        return $("<label>" + getDayName(dayCount) + "</label>");
                    }
                }
                else {
                    if (getDayName(dayCount) != "Su") {
                        if (CurValue == true)
                        { $txt = $("<a class='bntOn' style='font-size:small' id=Class" + dayCount + " Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">" + getDayName(dayCount) + "</a>"); }
                        else {
                            $txt = $("<a class='bntOff' style='font-size:small' id=Class" + dayCount + " Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">" + getDayName(dayCount) + "</a>");
                        }
                    }
                    else {
                        return $("<label>" + getDayName(dayCount) + "</label>");
                    }
                }
                $txt.click(function () {
                    var Id = "Working Day";
                    var Register = $(this).attr("Register");
                    var Day = $(this).attr("Day");

                    var goAhead = false;

                    if ($(this).hasClass('bntOn')) {
                        $(this).removeClass('bntOn').addClass('bntOff');
                        switchOffDay(Day);
                        goAhead = true;
                    }
                    else {
                        goAhead = switchOnDay(Day);
                        if (goAhead)
                        { $(this).removeClass('bntOff').addClass('bntOn'); }
                    }

                    if (goAhead) {
                        var Status = $(this).hasClass('bntOn');

                       // var obj = new Object();
//                        obj["Id"] = Id;
//                        obj["Register"] = Register;
//                        obj["State"] = Status
//                        obj["Day"] = Day

                        //var Url = "Register/UpdateAttendance";
                        _comLayer.parameters.clear();
                        _comLayer.parameters.add("Id", Id);
                        _comLayer.parameters.add("Register", Register);
                        _comLayer.parameters.add("State", Status);
                        _comLayer.parameters.add("Day", Day);
                        var oResult = _comLayer.executeSyncAction("/Register/UpdateAttendance", _comLayer.parameters);
                        //var oResult = executeSyncAction(Url, JSON.stringify(obj));

                        loadNumberOfWorkingDays();

                        alertify.success("Successfully Updated");
                    }
                    else {
                        alertify.error("More than 26 Wokring Days are not possible in this month");
                    }

                });
            }
            else {

                if (dayOn(dayCount)) {
                    if (CurValue == false)
                    { $txt = $("<img class='chkOff Day_" + dayCount + "' style='height:20px; width:20px;' src='../Images/Wrong.jpg'  Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">"); }
                    else if (CurValue == true || dayOn(dayCount))
                    { $txt = $("<img class='chkOn Day_" + dayCount + "' style='height:20px; width:20px;' src='../Images/Tick.jpg'  Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">"); }
                    else
                    { $txt = $("<img class='chkOn Day_" + dayCount + "' Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">"); }
                }
                else {
                    $txt = $("<img class='chkOn Day_" + dayCount + "' Day=Day_" + dayCount + "  EmployeeId=" + data.record.Id + " Register=" + data.record.PkRegister + ">");
                }
                $txt.click(function () {
                    var Id = $(this).attr("EmployeeId");
                    var Register = $(this).attr("Register");
                    var Day = $(this).attr("Day");

                    if ($(this).hasClass('chkOn')) {
                        $(this).removeClass('chkOn').addClass('chkOff');
                        $(this).attr('src', '../Images/Wrong.jpg');
                    }
                    else {
                        $(this).removeClass('chkOff').addClass('chkOn');
                        $(this).attr('src', '../Images/Tick.jpg');
                    }

                    var Status = $(this).hasClass('chkOn');

//                    var obj = new Object();
//                    obj["Id"] = Id;
//                    obj["Register"] = Register;
//                    obj["State"] = Status
//                    obj["Day"] = Day

//                    var Url = "Register/UpdateAttendance";

//                    var oResult = executeSyncAction(Url, JSON.stringify(obj));
                    _comLayer.parameters.clear();
                    _comLayer.parameters.add("Id", Id);
                    _comLayer.parameters.add("Register", Register);
                    _comLayer.parameters.add("State", Status);
                    _comLayer.parameters.add("Day", Day);
                    var oResult = _comLayer.executeSyncAction("/Register/UpdateAttendance", _comLayer.parameters);
                    if (!bSilent)
                        alertify.success("Successfully Updated");

                    //$('#grdRegister').jtable('reload');

                });
            }

            return $txt;
        };
        var key = "Day_" + eval(i);
        oMainFieldsObject[key] = oday;
    }

    var oEmployeePercentage = new Object();
    oEmployeePercentage.title = "%";
    oEmployeePercentage.width = '2%';
    oEmployeePercentage.edit = false;
    oEmployeePercentage.sorting = false;
    oEmployeePercentage.display = function (data) {
        if (data.record.Id == "Working Day") {
            $txt = $("<i class='btn icon-reload-CW'>");
            $txt.click(function () {
                $('#grdRegister').jtable('reload');
                loadNumberOfWorkingDays();
            });
            return $txt;
        }
        else {
            $txt = $("<label style='font-size:small'>" + data.record.Percentage + "</label>");
            return $txt;
        }
    };

    oMainFieldsObject.Percentage = oEmployeePercentage;

    return oMainFieldsObject;

}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function getDayName(day) {

    var date = SelectedMonth + "/" + day + "/" + SelectedYear;
    var dt = new Date(date);
    var d = dt.getDay();

    var weekday = new Array(7);
    weekday[0] = "Su";
    weekday[1] = "Mo";
    weekday[2] = "Tu";
    weekday[3] = "We";
    weekday[4] = "Th";
    weekday[5] = "Fr";
    weekday[6] = "Sa";

    return weekday[d];

}

function dayOn(day) {
    var controller = '#Class' + day;
    return $(controller).hasClass('bntOn');
}

function switchOnDay(day) {
    var val = $('#NCC').text();

    if (val == "" || eval(val) < 26) {

        var depentents = '.' + day;
        var Day = day;


//        var obj = new Object();
//        obj["Day"] = Day;
//        obj["Register"] = $(depentents).attr("Register");

//        showMessageDialog("", false);

//        var Url = "Register/NewDay";
//        var oResult = executeSyncAction(Url, JSON.stringify(obj));


        _comLayer.parameters.clear();
        _comLayer.parameters.add("Day", Day);
        _comLayer.parameters.add("Register", $(depentents).attr("Register"));
        var oResult = _comLayer.executeSyncAction("Register/NewDay", _comLayer.parameters);



        $(depentents).attr("src", "../Images/Tick.jpg");

        closeMessageDialog();

        return true;
    }

    return false;
}

function closeMessageDialog() {

    $('#messageDialog').modal('hide');;

}

function switchOffDay(day) {

    bSilent = true;

    var depentents = '.' + day;
    var Day = day;


    _comLayer.parameters.clear();
    _comLayer.parameters.add("Day", Day);
    _comLayer.parameters.add("Register", $(depentents).attr("Register"));
    var oResult = _comLayer.executeSyncAction("Register/ClearDay", _comLayer.parameters);

//    var obj = new Object();
//    obj["Day"] = Day;
//    obj["Register"] = $(depentents).attr("Register");

    showMessageDialog("", false);

//    var Url = "Register/ClearDay";
//    var oResult = executeSyncAction(Url, JSON.stringify(obj));

    $(depentents).removeClass('chkOn').addClass('chkOff').removeAttr("src");

    closeMessageDialog();

    bSilent = false;
}



function showMessageDialog(message, withMessage) {

    if (withMessage) {
        $('#divProcessing').hide();
        if (message.indexOf("Success") != -1) {
            $('#divSuccess').show();
            $('#divError').hide();
        } else {
            $('#divSuccess').hide();
            $('#divError').show();
        }
        $('#divMessage').html("<label>" + message + "</label>");
    }
    else {
        $('#divSuccess').hide();
        $('#divError').hide();
        $('#divMessage').html("<label>Processing...</label>");
        $('#divProcessing').show();
        $('#messageDialog').modal('show');
    }
}
