﻿


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Die List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Die/DieListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
            },
            Name: {
                title: 'Name'
            }

        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Material: $('#TxtPkColor').val(),
            Name: $('#TxtColorName').val()
        });

    });
    //$('#TxtPkColor').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Material: $('#TxtPkColor').val(),
    //            Name: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //$('#TxtColorName').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Color: $('#TxtPkColor').val(),
    //            ColorName: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}

function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Die";
}

function CheckDieDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#DieName').val());
    oResult = _comLayer.executeSyncAction("Die/DieDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Die Name Already added";

}


