﻿var Estimation = new Estimation();
var bLoadFromModel = false;
var prtSequence = -1;

var oWizard = null;

$(document).ready(function () {


    oWizard = $("#estWizard").wgWizard({
        enableAllSteps: true
    });

    $('#cmdPublish').click(function (e) {
        e.preventDefault();

        oWizard.actions.clearAllSteps();
        createFormPartsWizard();
        oWizard.actions.refreshWizard();
    });

    $('#cmdNext').click(function (e) {
        e.preventDefault();
        $("#pivot").pivot(1);
    });

    $(".ckhPart").click(function (e) {
        if (e.currentTarget.checked) {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](1);
        }
        else {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](0);
        }
    });

    ko.applyBindings(Estimation, document.getElementById("shellSelection"));

    var EnqNumber = _util.getParameterByName("Enquiry");
    Estimation.Fk_Enquiry = EnqNumber;
    if (Estimation.fetch()) {
        bLoadFromModel = true;
        createFormPartsWizard();
        $("#Basic").removeClass("active");
        $("#Details").addClass("active");
        $("#pivot").pivot(1);

        var status = Estimation.EnquiryStatus;

        if (status == "Approved" || status == "Closed"
        || status == "EstimationSent" || status == "OrderRecived") {

            $(".input-mini").attr('disabled', 'disabled');

            $("#cmdSave").attr('disabled', 'disabled');
            $("#cmdPublish").attr('disabled', 'disabled');
            $("#cmdSaveLayers").attr('disabled', 'disabled');

        }
    }
    _comLayer.parameters.clear();
    _comLayer.parameters.add("Id", EnqNumber);

    //var sPath = _comLayer.buildURL(this.modelName,this.modelTag,service);	
    var oResult1 = _comLayer.executeSyncAction("/Enquiry/Load", _comLayer.parameters);
    if (oResult1.success) {
        
        $("#lblCustomer").text(oResult1.data.CustomerName);
        $("#lblFkEnquiry").text(EnqNumber);
        $('#chkOuterShell').attr('checked', true);
        document.getElementById("txtOuterShell").value = 1;
        var lenpart = oResult1.data.LengthPartitionRequired;
        if (lenpart == true) {
            $("#chkLengthPartation").attr("checked", true);
            document.getElementById("txtLPartation").value = 1;
        }
        else
            $("#chkLengthPartation").attr("checked", false);

        var widthpart = oResult1.data.WidthPartitionRequired;
        if (widthpart == true) {
            $("#chkWidthPartation").attr("checked", true);
            document.getElementById("txtWidthPartation").value = 1;
        }
        else
            $("#chkWidthPartation").attr("checked", false);
        var platepart = oResult1.data.PlateRequired;
        if (platepart == true) {
            $("#chkPlate").attr("checked", true);
            document.getElementById("txtPlate").value = 1;
        }
        else
            $("#chkPlate").attr("checked", false);
        var cappart = oResult1.data.CapRequired;
        if (cappart == true) {
            $("#chkCap").attr("checked", true);
            document.getElementById("txtCap").value = 1;
        }
        else
            $("#chkCap").attr("checked", false);

    }
    document.getElementById("totalWeight").value = 0;
    document.getElementById("totalPrice").value = 0;

});


//function createFormParts() {

//    $("#prtList").empty();

//    //Check Outershell
//    addPart($('#chkOuterShell')[0].checked, $('#txtOuterShell').val(), "Outer Shell", true, false, false);

//    //Check Plate
//    addPart($('#chkPlate')[0].checked, $('#txtPlate').val(), "Plate", false, false, true);

//    //Check LengthPartation
//    addPart($('#chkLengthPartation')[0].checked, $('#txtLPartation').val(), "Length Partation", false, false, false);

//    //Check WidthPartation
//    addPart($('#chkWidthPartation')[0].checked, $('#txtWidthPartation').val(), "Width Partation", false, false, false);

//    //Check Cap
//    addPart($('#chkCap')[0].checked, $('#txtCap').val(), "Cap", false, true);

//    //Now add one rate part to this
//    addRatePart();

//    //Load the existing model if available

//}

function createFormPartsWizard() {

    //Check Outershell
    addPartWizard($('#chkOuterShell')[0].checked, $('#txtOuterShell').val(), "Outer Shell", "OuterShell");

    //Check Plate
    addPartWizard($('#chkPlate')[0].checked, $('#txtPlate').val(), "Plate", "GenericPart");

    //Check LengthPartation
    addPartWizard($('#chkLengthPartation')[0].checked, $('#txtLPartation').val(), "Length Partation", "GenericPart");

    //Check WidthPartation
    addPartWizard($('#chkWidthPartation')[0].checked, $('#txtWidthPartation').val(), "Width Partation", "GenericPart");

    //Check Cap
    addPartWizard($('#chkCap')[0].checked, $('#txtCap').val(), "Cap", "CapPart");


    //addRatePart();
    //Now add one rate part to this
    addPartWizard(true, 1, "Additional Cost", "Rate");

    //Now add one Tax part to this
    addPartWizard(true, 1, "Tax", "Tax");

    
    //Load the existing model if available

}

function wizardCompleteHook(objs, context) {
    Estimation.save();
}

function wizardRefreshedHook(wizardSteps) {

    var i = 0;

    while (i < wizardSteps.length) {
        i++;

        var stepDiv = "#WizStep_" + i + "_Content";
        var stepMain = "#step-" + i;

        var control = $(stepMain).slWizPart({
            viewTag: "Estimation",
            id: stepDiv,
            layerChanged: calculateLayerChanged,
            partChanged: calculatePartChanged
        });
    }
    
    
}

function addPartWizard(bConsider, iValue, sTitle, viewName) {

    var i = 0;
    var localModel = null;
    if(bConsider)
    {
       
        while (i < iValue) {
            i++;
            localModel = new part();
            Estimation.addParts(localModel);
            oWizard.actions.addStep("Estimation", sTitle, sTitle + " - " + i, viewName);
     
        }
    }
}

function addRatePart() {
    var divId = "rateContainer";
    var rowMain = $("<tr></tr>");
    var colMain = $("<td></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var lblTotalWeight = $("<label>Total Weight</label>");
    var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalWeight' type='text' placeholder='Weight' style='width: 70px' readonly />");
    var colTotalWeight = $("<td valign='bottom'></td>");

    colTotalWeight.append(lblTotalWeight);
    colTotalWeight.append(txtTotalWeight);
    rowMain.append(colTotalWeight);

    //Create a row for column for total rate
    var lblTotalRate = $("<label>Total Price</label>");
    var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalPrice' type='text' placeholder='Price' style='width: 70px' readonly />");
    var colTotalRate = $("<td valign='bottom'></td>");

    colTotalRate.append(lblTotalRate);
    colTotalRate.append(txtTotalRate);
    rowMain.append(colTotalRate);

    var control = $(colMain).slWizPart({
        viewTag: "Estimation",
        title: "Addtional Costs",
        id: divId,
        estimationModel: Estimation,
        rate: true,
        rateChanged: rateChanged
    });

    var divId = "save";
    var rowMain = $("<tr id='saveBlock'></tr>");
    var colMain = $("<td colspan='3' align='center'></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var cmdCreate = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdSave' >Save</button>");
        
    colMain.append(cmdCreate);
    rowMain.append(colMain);

    ko.applyBindings(Estimation, document.getElementById("saveBlock"));
}

//function addPart(bConsider, iValue, sTitle, outherShell, cap, plate) {

//    var localModel = null;
//    if (bConsider) {

//        for (i = 0; i < iValue; i++) {
//            //let us sort out the model
//            if (bLoadFromModel) {
//                 prtSequence++;
//                 localModel = Estimation.parts[prtSequence];
//            }
//            else
//            {
//                localModel = new part();
//                Estimation.addParts(localModel);
//            }

//            var divId = "prtContainer" + i;
//            var rowMain = $("<tr></tr>");
//            var colMain = $("<td></td>");

//            rowMain.append(colMain);
//            $("#prtList").append(rowMain);

//            //Create a row for column for total weight
//            var lblTotalWeight = $("<label>Total Weight</label>");
//            var txtTotalWeight = $("<input class='input-mini weight validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' style='width: 70px' readonly />");
//            var colTotalWeight = $("<td></td>");

//            colTotalWeight.append(lblTotalWeight);
//            colTotalWeight.append(txtTotalWeight);
//            rowMain.append(colTotalWeight);

//            //Create a row for column for total rate
//            var lblTotalRate = $("<label>Total Rate</label>");
//            var txtTotalRate = $("<input class='input-mini rate validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' style='width: 70px' readonly />");
//            var colTotalRate = $("<td></td>");

//            colTotalRate.append(lblTotalRate);
//            colTotalRate.append(txtTotalRate);
//            rowMain.append(colTotalRate);

//          var sNewTitle = sTitle + "-" + (i + 1);

//          var control = $(colMain).slPart({
//                    viewTag: "Estimation",
//                    title: sNewTitle,
//                    id: divId,
//                    outershell: outherShell,
//                    cap: cap,
//                    plate: plate,
//                    model: localModel,
//                    loadFromModel: bLoadFromModel,
//                    partChanged: calculatePartChanged,
//                    layerChanged: calculateLayerChanged
//                });
                
//        }
//    }

//}

//function calculatePartChanged(model, outerShell, cap, plate) {

//    if (outerShell) {
//        outerShellCalculation(model)
//    }
//    else if (cap || plate) {
//        plateCapCalculation(model);
//    }
//    else {
//        partationCalculation(model);
//    }

//    rateChanged(Estimation);
//}

//function calculateLayerChanged(mainModel, model, outerShell, cap, plate)
//{
//    if (outerShell) {
//        outerShellCalculation(mainModel)
//    }
//    else if (cap || plate) {
//        plateCapCalculation(mainModel);
//    }
//    else {
//        partationCalculation(mainModel);
//    }

//    rateChanged(Estimation);
//}


////Outershell
//function outerShellCalculation(model) {
//    var length = model.length();
//    var width = model.width();
//    var height = model.height();
//    var noBoards = model.noBoards();

//    //Board Size
//    var boardArea = ((Number(length) * Number(width) * 4) + Number(50)) / 1000;
//    model.boardArea(boardArea);

//    //Cuttingsize
//    var cuttingsize = Math.round((Number(length) * 2 + Number(width) * 2 + Number(50)) / 100) * 100;
//    model.cuttingSize(cuttingsize);

//    //Deckle
//    var deckel = Math.round((Number(width) + Number(height) + 20) / 100) * 100;
//    model.deckle(deckel);

//    var i = 0;

//    var weight = 0;

//    while (model.layers[i]) {

//        if (i % 2 != 0) {
//            //fluted
//            var layerWeight = cuttingsize * deckel * 1.5 * model.layers[i].gsm();
//            weight = weight + layerWeight;
//        }
//        else {
//            var layerWeight = cuttingsize * deckel * model.layers[i].gsm();
//            weight = weight + layerWeight;
//        }

//        i++;
//    }

//    weight = ((weight / 1000) * noBoards).toFixed(3);

//    model.weight(weight);

//}



////partationcalculation
//function partationCalculation(model) {

//    var length = model.length();
//    var width = model.width();
//    var quantity = model.quantity();


//    var adjustedLength = (Number(length) + Number(10)) / 10;
//    var adjustedWidth = Number(Number(width) + Number(20)) / 10;

//    var boardArea = (Number(adjustedWidth) * Number(adjustedLength)) / 1000;
//    model.boardArea(boardArea);

//    //Cuttingsize
//    var cuttingSize = adjustedLength + adjustedWidth + 50;
//    model.cuttingSize(cuttingSize);

//    //Deckle
//    var deckel = Math.round((Number(width) + 20) / 100) * 100;
//    model.deckle(deckel);

//    var i = 0;

//    var weight = 0;
//    var flutedParameter = 0;
//    var nonFlutedParameter = 0;

//    while (model.layers[i]) {

//        if (i % 2 != 0) {
//            //fluted
//            var layerWeight = 1.5 * model.layers[i].gsm();
//            flutedParameter = flutedParameter + layerWeight;
//        }
//        else {
//            var layerWeight = model.layers[i].gsm();
//            nonFlutedParameter = nonFlutedParameter + layerWeight;
//        }

//        i++;
//    }
//    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
//    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

//    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
//    model.weight(weight);
//}



////platecalculation
//function plateCapCalculation(model) {

//    var length = model.length();
//    var width = model.width();
//    var quantity = model.quantity();
  

//    var adjustedLength = (Number(length) + Number(240));
//    var adjustedWidth = Number(Number(width) + Number(240));

//    var boardArea = (Number(adjustedWidth) * Number(adjustedLength))/1000;
//    model.boardArea(boardArea);

//    //Cuttingsize
//    var cuttingSize = adjustedLength + adjustedWidth + 50;
//    model.cuttingSize(cuttingSize);

//    //Deckle
//    var deckel = Math.round((Number(width) + 20) / 100) * 100;
//    model.deckle(deckel);

//    var i = 0;

//    var weight = 0;
//    var flutedParameter = 0;
//    var nonFlutedParameter = 0;

//    while (model.layers[i]) {

//        if (i % 2 != 0) {
//            //fluted
//            var layerWeight = 1.5 * model.layers[i].gsm();
//            flutedParameter = flutedParameter + layerWeight;
//        }
//        else {
//            var layerWeight = model.layers[i].gsm();
//            nonFlutedParameter = nonFlutedParameter + layerWeight;
//        }

//        i++;
//    }
//    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
//    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

//    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
//    model.weight(weight);
//}

////Final Rate calculation
//function rateChanged(estModel) {
//    var i = 0;
//    var totalWeight = 0;

//    while (estModel.parts[i]) {
//        totalWeight = Number(totalWeight) + Number(estModel.parts[i].weight());
//        i++;
//    }

//    estModel.totalWeight(totalWeight);

//    //Now rate calculation
//    //Declaring variables

//    var convRate = estModel.convRate();
//    var convValue = 0;
//    var gMarginPercentage = estModel.gMarginPercentage();
//    var gMarginValue = 0;
//    var taxesPercntage = estModel.taxesPercntage();
//    var taxesValue = 0;
//    var transportValue = estModel.transportValue();
//    var weightHValue = estModel.weightHValue();
//    var handlingChanrgesValue = estModel.handlingChanrgesValue();
//    var packingChargesValue = estModel.packingChargesValue();
//    var rejectionPercentage = estModel.rejectionPercentage();
//    var rejectionValue = 0;
//    var totalPrice = 0;

//    //Calculations
//    convValue = (convRate * totalWeight) / 100;
//    gMarginValue = (convValue * gMarginPercentage) / 100;
//    taxesValue = (convValue * taxesPercntage) / 100;
//    rejectionValue = (convValue * rejectionPercentage) / 100;

//    estModel.convValue(convValue);
//    estModel.gMarginValue(gMarginValue);
//    estModel.taxesValue(taxesValue);
//    estModel.rejectionValue(rejectionValue);

//    totalPrice = (Number(convValue) +
//                  Number(gMarginValue) +
//                  Number(taxesValue) +
//                  Number(transportValue) +
//                  Number(weightHValue) +
//                  Number(handlingChanrgesValue) +
//                  Number(packingChargesValue) +
//                  Number(rejectionValue));
 
//    estModel.totalPrice(totalPrice);
//}