﻿function calculatePartChanged(model, outerShell, cap, plate) {

    if (outerShell) {
        outerShellCalculation(model)
    }
    else if (cap || plate) {
        plateCapCalculation(model);
    }
    else {
        partationCalculation(model);
    }

    rateChanged(Estimation);
}

function calculateLayerChanged(mainModel, model, outerShell, cap, plate) {
    if (outerShell) {
        outerShellCalculation(mainModel)
    }
    else if (cap || plate) {
        plateCapCalculation(mainModel);
    }
    else {
        partationCalculation(mainModel);
    }

    rateChanged(Estimation);
}


//Outershell
function outerShellCalculation(model) {
    var length = model.length();
    var width = model.width();
    var height = model.height();
    var noBoards = model.noBoards();

    //Board Size
    var boardArea = ((Number(length) * Number(width) * 4) + Number(50)) / 1000;
    model.boardArea(boardArea);

    //Cuttingsize
    var cuttingsize = Math.round((Number(length) * 2 + Number(width) * 2 + Number(50)) / 100) * 100;
    model.cuttingSize(cuttingsize);

    //Deckle
    var deckel = Math.round((Number(width) + Number(height) + 20) / 100) * 100;
    model.deckle(deckel);

    var i = 0;

    var weight = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = cuttingsize * deckel * 1.5 * model.layers[i].gsm();
            weight = weight + layerWeight;
        }
        else {
            var layerWeight = cuttingsize * deckel * model.layers[i].gsm();
            weight = weight + layerWeight;
        }

        i++;
    }

    weight = ((weight / 1000) * noBoards).toFixed(3);

    model.weight(weight);

}



//partationcalculation
function partationCalculation(model) {

    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();


    var adjustedLength = (Number(length) + Number(10)) / 10;
    var adjustedWidth = Number(Number(width) + Number(20)) / 10;

    var boardArea = (Number(adjustedWidth) * Number(adjustedLength)) / 1000;
    model.boardArea(boardArea);

    //Cuttingsize
    var cuttingSize = adjustedLength + adjustedWidth + 50;
    model.cuttingSize(cuttingSize);

    //Deckle
    var deckel = Math.round((Number(width) + 20) / 100) * 100;
    model.deckle(deckel);

    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = 1.5 * model.layers[i].gsm();
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    model.weight(weight);
}



//platecalculation
function plateCapCalculation(model) {

    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();


    var adjustedLength = (Number(length) + Number(240));
    var adjustedWidth = Number(Number(width) + Number(240));

    var boardArea = (Number(adjustedWidth) * Number(adjustedLength)) / 1000;
    model.boardArea(boardArea);

    //Cuttingsize
    var cuttingSize = adjustedLength + adjustedWidth + 50;
    model.cuttingSize(cuttingSize);

    //Deckle
    var deckel = Math.round((Number(width) + 20) / 100) * 100;
    model.deckle(deckel);

    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = 1.5 * model.layers[i].gsm();
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
    var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    model.weight(weight);
}

//Final Rate calculation
function rateChanged(estModel) {
    //var i = 0;
    //var totalWeight = 0;

    //while (estModel.parts[i]) {
    //    totalWeight = Number(totalWeight) + Number(estModel.parts[i].weight());
    //    i++;
    //}

    //estModel.totalWeight(totalWeight);

    ////Now rate calculation
    ////Declaring variables

    //var convRate = estModel.convRate();
    //var convValue = 0;
    //var gMarginPercentage = estModel.gMarginPercentage();
    //var gMarginValue = 0;
    //var taxesPercntage = estModel.taxesPercntage();
    //var taxesValue = 0;
    //var transportValue = estModel.transportValue();
    //var weightHValue = estModel.weightHValue();
    //var handlingChanrgesValue = estModel.handlingChanrgesValue();
    //var packingChargesValue = estModel.packingChargesValue();
    //var rejectionPercentage = estModel.rejectionPercentage();
    //var rejectionValue = 0;
    //var totalPrice = 0;

    ////Calculations
    //convValue = (convRate * totalWeight) / 100;
    //gMarginValue = (convValue * gMarginPercentage) / 100;
    //taxesValue = (convValue * taxesPercntage) / 100;
    //rejectionValue = (convValue * rejectionPercentage) / 100;

    //estModel.convValue(convValue);
    //estModel.gMarginValue(gMarginValue);
    //estModel.taxesValue(taxesValue);
    //estModel.rejectionValue(rejectionValue);

    //totalPrice = (Number(convValue) +
    //              Number(gMarginValue) +
    //              Number(taxesValue) +
    //              Number(transportValue) +
    //              Number(weightHValue) +
    //              Number(handlingChanrgesValue) +
    //              Number(packingChargesValue) +
    //              Number(rejectionValue));

    //estModel.totalPrice(totalPrice);
}