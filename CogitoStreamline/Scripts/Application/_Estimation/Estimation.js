﻿function layer(gsm, bf, color, rate) {
    this.id = null;
    this.gsm = ko.observable(gsm);
    this.bf = ko.observable(bf);
    this.color = ko.observable(color);
    this.rate = ko.observable(rate);
}
function part() {
    this.id = null;
    this.layers = Array();
    this.length=ko.observable(0);
    this.width=ko.observable(0);
    this.height=ko.observable(0);
    this.boardArea=ko.observable(0);
    this.deckle=ko.observable(0);
    this.cuttingSize = ko.observable(0);
    this.quantity = ko.observable(1);
    this.noBoards = ko.observable(0);
    this.weight = ko.observable(0);
    this.rate = ko.observable(0);

    this.addNewChild = function(sChildType)
    {
        if (sChildType == "layers") {
            var lyr = new layer(0, 0, "N", 0);
            this.addLayer(lyr);
            return lyr;
        }

        return null;
    }

    this.addLayer = function(layer)
    {
        this.layers.push(layer);
    }
}

function Estimation() {
    this.id = null;
    this.plateYes = ko.observable();
    this.widthPartationYes = ko.observable();
    this.lenghtPartationYes = ko.observable();
    this.outerShellYes = ko.observable();
    this.capYes = ko.observable();
    this.outerShell = ko.observable();
    this.plate = ko.observable();
    this.widthPartation = ko.observable();
    this.lenghtPartation = ko.observable();
    this.cap = ko.observable();
    this.parts = Array();
    this.Fk_Enquiry = null;
    this.convRate = ko.observable(9.5);
    this.convValue = ko.observable(0);
    this.gMarginPercentage = ko.observable(15);
    this.gMarginValue = ko.observable(0);
    this.taxesPercntage = ko.observable(12);
    this.taxesValue = ko.observable(0);
    this.transportValue = ko.observable(0);
    this.weightHValue = ko.observable(0);
    this.handlingChanrgesValue = ko.observable(0);
    this.packingChargesValue = ko.observable(0);
    this.rejectionPercentage = ko.observable(5);
    this.rejectionValue = ko.observable(0);
    this.totalWeight = ko.observable(0);
    this.totalPrice = ko.observable(0);
    this.partsCopy = null;
    this.EnquiryStatus = null;

    this.addParts = function (part) {
        this.parts.push(part);
    };

    this.addNewChild = function (sChildType)
    {
        if (sChildType == "parts") {
            var prt = new part();
            this.addParts(prt);
            return prt;
        }

        return null;
    }

    this.save = function () {

        var ToPass = ko.toJSON(this);
        var sPath = "";

        //if ($("#frmHost").validationEngine('validate')) {
        _util.showMessageDialog("", false);

        if (this.id == null) {
            sPath = _comLayer.buildURL(this.modelName, "Estimation", "Save");
        }
        else { sPath = _comLayer.buildURL(this.modelName, "Estimation", "Update"); }

        _comLayer.parameters.clear();
        _comLayer.parameters.setJSON(ToPass);

        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

        if (oResult.Success) {
            _util.showSuccess("Data Saved successfully", function () { document.location.reload(); });
        }
        else {
            _util.showError("Unable save please contact administrator", function () { });
        }
        //}
    };

    this.deleteRecord = function () {
        if (_util.fireSubscribedHooks(this.hookSubscribers, 'beforeModelDelete', { Name: this.modelName, Tag: this.modelTag })) {
            var ToPass = ko.toJSON(this.data);
            var sPath = "";

            if (this.id != null) {
                sPath = _comLayer.buildURL(this.modelName, this.modelTag, "Delete");
                _comLayer.parameters.clear();
                _comLayer.parameters.setJSON(ToPass);

                var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

                _util.fireSubscribedHooks(this.hookSubscribers, 'afterModelDelete', { result: oResult });
            }

        };
    };

    this.fetch = function () {
        if (this.Fk_Enquiry == null || this.Fk_Enquiry == "") return;
        var oResult = this._getDataFromSource(null);

        if (oResult.success == true) {
            _util.deepObjectCopy(oResult.data, this);
            return true;
        }

        return false;
    };

    this._getDataFromSource = function (filters) {
        //if filters are set just go with the filters
        if (filters != null) {
            _comLayer.parameters = filters;
        }
        else {
            _comLayer.parameters.clear();
            _comLayer.parameters.add("EnquiryId", this.Fk_Enquiry);
        }

        var sPath = _comLayer.buildURL(this.modelName, "Estimation", "LoadByEnquiry");
        var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

        var oResultObject = oResult;

        return oResultObject;

    };
    

}