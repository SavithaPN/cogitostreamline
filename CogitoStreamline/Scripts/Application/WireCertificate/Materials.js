﻿var ssum = 0;
var samt = 0;
function IssuedMaterial() {
    this.data = new Object();
    this.data["Pk_CharID"] = "";
    this.data["Fk_Characteristics"] = "";
    this.data["PCode"] = "";
    this.data["Remarks"] = "";
    this.data["Observed"] = "";
    this.data["Status"] = "";
    

    this.updateValues = function () {
        this.data["Pk_CharID"] = $('#Pk_CharID').val();
        this.data["Fk_Characteristics"] = $('#txtFk_Material').val();     
    
        this.data["Observed"] = $('#Observed').val();
        this.data["Remarks"] = $('#Remarks').val();
        this.data["Status"] = $('#Status').val();
    };
    this.load = function (Pk_IdDet, PCode, MixingRatio, Pk_CharID, Fk_Characteristics, txtFMaterial, Observed, Remarks, Status) {
        this.data["PCode"] = PCode;
        this.data["MixingRatio"] = MixingRatio;
        this.data["Pk_CharID"] = Pk_CharID;
        this.data["Fk_Characteristics"] = Fk_Characteristics;
        this.data["txtFMaterial"] = txtFMaterial;      
        this.data["Observed"] = Observed;
        this.data["Remarks"] = Remarks;
        this.data["Status"] = Status;
    };
}
