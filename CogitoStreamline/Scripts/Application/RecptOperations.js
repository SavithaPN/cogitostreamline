﻿//Name          : Receipt Operations ----javascript files
//Description   : Contains the  Receipt Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  
//Author        : Nagesh
//Date 	        : 28/10/2015
//Crh Number    : 
//Modifications : 

var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Receipt List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Recpt/ReceiptListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },

        fields: {
            Pk_ReceiptID: {
                title: 'Id',
                key: true

            },
            Fk_Customer: {
                title: 'CustomerName',
                width: '25%'
            },
            Amount: {
                title: 'Amount',
                width: '25%'
            },

            ChequeNo: {
                title: 'ChequeNo',
                width: '25%'
            },
            ChequeDate: {
                title: 'ChequeDate',
                width: '25%'
            },
            Description: {
                title: 'Description',
                width: '25%'
            },
            BankName: {
                title: 'BankName',
                width: '25%'
            }

        }


    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ReceiptID: $('#TxtRecId').val(),
            Fk_Customer: $('#TxtCustomer').val(),
            ChequeNo: $('#TxtChNo').val(),
            ChequeDate: $('#txtChDate').val()
        });
    });
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
    setUpCustomerSearch();

    $('#txtChDate').datepicker({
        autoclose: true
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Customer", "getCustomer", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Customer", "getCustomer", null);
    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Countrys", "getCountry", null);
    // _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Countrys", "getCountry", null);

   

}


function afterNewShow(viewObject) {
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    $('#ChequeDate').datepicker({ autoclose: true });
  //  document.getElementById('ComplaintDate').value = dNow.getDate() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getFullYear();
  //  $('#ComplaintDate').datepicker({ autoclose: true });

    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        //setUpCustomerSearch();
        $("#searchDialog").modal("show");
    });
}

//function CheckCityDuplicate() {

//    _comLayer.parameters.clear();
//    var oResult;
//    _comLayer.parameters.add("Country", $('#Country').val());
//    _comLayer.parameters.add("Fk_StateID", $('#Fk_StateID').val());
//    _comLayer.parameters.add("CityName", $('#CityName').val());
//    //sPath = _comLayer.buildURL("RawProduct", null, "BindRawProduct");
//    oResult = _comLayer.executeSyncAction("City/CityDuplicateChecking", _comLayer.parameters);
//    if (oResult.TotalRecordCount > 0)
//        return "* " + "City Already added";

//    //return true;
//}


//function afterNewShow(viewObject) {
//    $('#Country').change(function () {
//        if ($('#Country').val() != '') {
//            loadComboState('#Fk_StateID', $('#Country').val());
//            $('#Fk_StateID').change();
//        }
//    });
//}



//function loadComboState(combo, parent) {
//    var oResult = getStateComboValues(parent);
//    $(combo).empty();

//    var option = $("<option value=''>Select</option>");
//    $(combo).append(option);

//    if (oResult.Success) {
//        var i = 0;

//        while (oResult.Records[i]) {

//            var option = $("<option value='" + oResult.Records[i].Pk_StateID + "'>" + oResult.Records[i].StateName + "</option>");
//            $(combo).append(option);
//            i++;
//        }
//    }
//}

//function getStateComboValues(parent) {
//    _comLayer.parameters.clear();
//    _comLayer.parameters.add("CountryId", parent);

//    var sPath = _comLayer.buildURL("State", null, "StateListByCountry");
//    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

//    var oResultObject = oResult;

//    return oResultObject;
//}

function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#Fk_Customer').wgReferenceField("setData", rows[0].keyValue);
        $("#searchDialog").modal("hide");
    });

}

function ReferenceFieldNotInitilized(viewModel) {

    $('#Fk_Customer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });



    if (viewModel.data != null) {
        $('#Fk_Customer').wgReferenceField("setData", viewModel.data["Fk_CustomerId"]);
    }



}


function beforeModelSaveEx() {
    //   bFromSave = true;

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["ComplaintDate"] = $('#ComplaintDate').val();
    //viewModel.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
   // viewModel.data["Fk_EnquiryNo"] = $('#Fk_EnquiryNo').val();


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel.data["ComplaintDate"] = $('#ComplaintDate').val();
  //  viewModel.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
    //viewModel.data["Fk_EnquiryNo"] = $('#Fk_EnquiryNo').val();
}