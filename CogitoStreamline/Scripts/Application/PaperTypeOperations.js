﻿//Name          : PaperType Operations ----javascript files 
//Description   : Contains the  PaperType Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckPaperDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0009
//Modifications : 



var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Paper Type List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PaperType/PaperListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction:''
        },
        fields: {
            Pk_PaperType: {
                title: 'Id',
                key: true,
                list: true
            },
            PaperTypeName: {
                title: 'Name',
                width: '25%'
            }
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_PaperType: $('#txtPaperID').val(),
            PaperTypeName: $('#txtPaperName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckPaperDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("PaperTypeName", $('#PaperTypeName').val());
    oResult = _comLayer.executeSyncAction("PaperType/PaperDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "PaperTypeName Already added";
}









