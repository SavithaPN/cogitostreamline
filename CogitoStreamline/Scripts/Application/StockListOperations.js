﻿var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Cons.Stock List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/StockList/StockListByFiter',
            //updateAction: '',
            //createAction: '',
            //deleteAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
              


                document.getElementById('txtMatID').value = $(this).data("record").Pk_Material;
                document.getElementById('txtPkPStock').value = $(this).data("record").Pk_Stock;
                document.getElementById('txtstock').value = $(this).data("record").Quantity;
                //document.getElementById('txtPartId').value = $(this).data("record").PartID;


                return true;
            });


        },
        fields: {
            Pk_Stock: {
                title: 'Id',
                key: true
            },
            Pk_Material: {
                title: 'Material ID',
                width: '25%'
            },
            MaterialName: {
                title: 'MaterialName',
                width: '25%'
            },
            CatName: {
                title: 'Cat.Name',
                width: '25%'
            },
            Quantity: {
                title: 'Stock Qty',
                width: '25%'
            }

        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Stock: $('#txtStockID').val(),
            MaterialName: $('#txtMaterialName').val()
        });
    });
    $('#LoadRecordsButton').click();


    $('#cmdShow').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("StockList/StockList", _comLayer.parameters);
        var strval = "ConvertPDF/ConsStockList" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });




    $('#cmdInsert').click(function (e) {
        e.preventDefault();
        //document.getElementById('txtMatID').value = $(this).data("record").Pk_Material;
        //document.getElementById('txtPkPStock').value = $(this).data("record").StockID;
        //document.getElementById('txtstock').value = $(this).data("record").Quantity;
        //document.getElementById('txtPartId').value = $(this).data("record").PartID;

        var MatID = document.getElementById('txtMatID').value;
        var StockVal = document.getElementById('txtstock').value;
        var StockID = document.getElementById('txtPkPStock').value;
        //var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("MatID", MatID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("StockID", StockID);
        //_comLayer.parameters.add("PartId", PartId);

        _comLayer.executeSyncAction("StockList/InsertRow", _comLayer.parameters);
        //var strval = "ConvertPDF/ShadeWise" + ".pdf";
        document.getElementById('txtMatID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";
        //document.getElementById('txtPartId').value = "";


        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });



    $('#cmddelete').click(function (e) {
        e.preventDefault();
        var MatID = document.getElementById('txtMatID').value;
        var StockVal = document.getElementById('txtstock').value;
        var StockID = document.getElementById('txtPkPStock').value;
        //var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("MatID", MatID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("StockID", StockID);
        //_comLayer.parameters.add("PartId", PartId);


        _comLayer.executeSyncAction("StockList/DeleteRow", _comLayer.parameters);

        document.getElementById('txtMatID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";
        //document.getElementById('txtPartId').value = "";


    });
    $('#cmdUpdate').click(function (e) {
        e.preventDefault();

        var MatID = document.getElementById('txtMatID').value;
        var StockVal = document.getElementById('txtstock').value;
        var StockID = document.getElementById('txtPkPStock').value;
        //var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("MatID", MatID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("StockID", StockID);
        //_comLayer.parameters.add("PartId", PartId);


        _comLayer.executeSyncAction("StockList/UpdateRow", _comLayer.parameters);

        document.getElementById('txtMatID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";
        //document.getElementById('txtPartId').value = "";

        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });


    }

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["MaterialCategory"] = $.trim($('#MaterialCategory').val());

    //var viewModel1 = _page.getViewByName('Edit').viewModel;
    //viewModel1.data["MaterialCategory"] = $.trim($('#MaterialCategory').val());
}
function radioClass(str1) {
    if (str1 == "Board") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "Gum") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "Pin") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "Plate") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "Ink") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "Cap") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "Paper") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "HoneyComb") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "OuterShell") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "LengthPartition") {
        document.getElementById('MaterialCategory').value = str1;
    }
    else if (str1 == "WidthPartition") {
        document.getElementById('MaterialCategory').value = str1;
    }
 }

function showOnlyBoard() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Board",
    });
}
function showOnlyGum() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Gum",
    });
}
function showOnlyInk() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Ink",
    });
}
function showOnlyPaper() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Paper",
    });
}
function showOnlyInk() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Ink",
    });
}
function showOnlyPlate() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Plate",
    });
}
function showOnlyHoneyComb() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "HoneyComb",
    });
}
function showOnlyCap() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "Cap",
    });
}
function showOnlyLengthPartition() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "LengthPartition",
    });
}
function showOnlyWidthPartition() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "WidthPartition",
    });
}
function showOnlyOuterShell() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "OuterShell",
    });
}
function showAll() {
    $('#MainSearchContainer').jtable('load', {
        MaterialCategory: "",
    });
}


function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearchContainer').empty();
    $('#MainSearchContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();
}
