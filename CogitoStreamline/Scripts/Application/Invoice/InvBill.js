﻿
function InvoiceBill() {
    this.data = new Object();
    this.data["Pk_Inv_Details"] = "";
    this.data["Quantity"] = "";
    this.data["Price"] = "";
    //this.data["Fk_TaxID"] = "";
    //this.data["TaxValue"] = "";
    this.data["Discount"] = "";
    this.data["TotalAmount"] = "";
    this.data["Description"] = "";
    this.data["NetAmount"] = "";
    this.data["BoxID"] = "";
    this.data["Fk_JobCardID"] = "";
    this.data["Fk_BoxID"] = "";
    this.data["Fk_OrderNo"] = "";
    this.data["StkID"] = "";
    this.data["Fk_PartID"] = "";
    this.updateValues = function () {


        //alert($('#BoxID').val());
        //alert($('#Fk_BoxID').val());

        if ($('#BoxID').val() > 0) {
            this.data["Description"] = $('#txtDescription').val();
            this.data["Price"] = $('#Price').val();
            this.data["Discount"] = $('#Discount').val();
            this.data["Quantity"] = $('#TxtQuantity').val();
            this.data["TotalAmount"] = $('#TxtTotal').val();
            this.data["NetAmount"] = $('#TxtNetTotal').val();
            this.data["BoxID"] = $('#BoxID').val();
            this.data["Fk_BoxID"] = $('#BoxID').val();
            this.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
            this.data["Fk_JobCardID"] = $('#Fk_JobCardID').val();
            this.data["StkID"] = $('#StkID').val();
            this.data["Fk_PartID"] = $('#Fk_PartID').val();
            this.data["TaxValue"] = $('#TaxValue').val();
            this.data["PName"] = $('#PName').val();
            this.data["BName"] = $('#BName').val();
        }
        else {
            this.data["Description"] = $('#txtDescription').val();
            this.data["Price"] = $('#Price').val();
            this.data["Discount"] = $('#Discount').val();
            this.data["Quantity"] = $('#TxtQuantity').val();
            this.data["TotalAmount"] = $('#TxtTotal').val();
            this.data["NetAmount"] = $('#TxtNetTotal').val();
            this.data["BoxID"] = $('#BoxID').val();
            this.data["Fk_BoxID"] = $('#Fk_BoxID').val();
            this.data["Fk_Material"] = $('#Fk_BoxID').val();
            this.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
            this.data["Fk_JobCardID"] = $('#Fk_JobCardID').val();
            this.data["StkID"] = $('#StkID').val();
            this.data["Fk_PartID"] = $('#Fk_PartID').val();
            this.data["PName"] = $('#PName').val();
            this.data["BName"] = $('#BName').val();
        }

        document.getElementById("GrandTotal").value = Number($('#GrandTotal').val()) + Number($('#TxtNetTotal').val());
        if ($.trim($('#TaxType').val()) == "SGST") {
            document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + ((Number($('#GrandTotal').val()) * 0.06) * 2);
        }
        else if ($.trim($('#TaxType').val()) == "IGST") {
            document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val()) * 0.12);
        }
        else if ($.trim($('#TaxType').val()) == "9%") {
            document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + ((Number($('#GrandTotal').val()) * 0.09) * 2);
        }
        else if ($.trim($('#TaxType').val()) == "2.5%") {
            document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + ((Number($('#GrandTotal').val()) * 0.025) * 2);
        }
        else if ($.trim($('#TaxType').val()) == "5%") {
            document.getElementById('NETVALUE').value = Number($('#GrandTotal').val()) + ((Number($('#GrandTotal').val()) * 0.05) * 2);
        }



        //document.getElementById("ED").value = document.getElementById('NETVALUE').value;
        // document.getElementById("NETVALUE").value = document.getElementById("ED").value;
        document.getElementById("GrandTot").value = document.getElementById("GrandTotal").value;

        //document.getElementById("NET1").value = (Number($('#GrandTotal').val()) + Number($('#TxtNetTotal').val())) + (Number($('#GrandTotal').val()) + Number($('#TxtNetTotal').val())) * 0.055;
        //document.getElementById("NET2").value= document.getElementById("NET1").value 

    };

    this.load = function (Pk_Inv_Details, Quantity, Price, Discount, NetAmount, Description, TotalAmount, BoxID, Fk_PartID, HsnCode, Fk_BoxID, Fk_OrderNo, StkID, PName, BName) {
        this.data["Pk_Inv_Details"] = Pk_Inv_Details;
        this.data["Quantity"] = Quantity;
        this.data["Price"] = Price;
        //this.data["Fk_TaxID"] = Fk_TaxID;

        this.data["Discount"] = Discount;
        this.data["NetAmount"] = NetAmount;
        this.data["Description"] = Description;
        this.data["TotalAmount"] = TotalAmount;
        this.data["BoxID"] = BoxID;
        //this.data["Fk_JobCardID"] = Fk_JobCardID;
        this.data["Fk_PartID"] = Fk_PartID;
        this.data["HsnCode"] = HsnCode;
        this.data["Fk_BoxID"] = BoxID;
        this.data["Fk_OrderNo"] = Fk_OrderNo;
        this.data["StkID"] = StkID;
        //this.data["StkID"] = StkID;
        this.data["PName"] = PName;
        this.data["BName"] = BName;
        //this.updateValues();
    };
}


