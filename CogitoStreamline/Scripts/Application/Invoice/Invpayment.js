﻿
//Name          : Bill Operations ----javascript files
//Description   : Contains the  materials and its quantity data Operations  
//                 1. Jtable Loading and Columns Descriptions 
//Author        : shantha
//Date            :   09/09/2015
//CRH Number      :   SL0049
//Modifications : 

function Invpayment() {
    this.data = new Object();
    this.data["Pk_PaymtNo"] = "";
    this.data["CardNo"] = "";
    this.data["CName"] = "";
    this.data["BankName"] = "";
    this.data["CardType"] = "";
    this.data["ChequeNo"] = "";
    this.data["ChequeDate"] = "";
    this.data["CBankName"] = "";
    this.data["TransNo"] = "";
    this.data["GateWay"] = "";
    this.data["TBankName"] = "";
    this.data["TargetedAccount"] = "";
    this.data["Trans_Type"] = "";
    this.data["CashDenominations"] = "";


    //this.updateValues = function () {
    //    this.data["Description"] = $('#txtDescription').val();
    //    this.data["Price"] = $('#Price').val();
    //    this.data["Discount"] = $('#Discount').val();
    //    this.data["Quantity"] = $('#TxtQuantity').val();
    //    this.data["TotalAmount"] = $('#TxtTotal').val();
    //    this.data["NetAmount"] = $('#TxtNetTotal').val();
    //    this.data["Fk_TaxID"] = $('#Fk_TaxID').val();
    //    this.data["TaxValue"] = $('#TaxValue').val();
    //    document.getElementById("GrandTotal").value = Number($('#GrandTotal').val()) + Number($('#TxtNetTotal').val());
    //    document.getElementById("GrandTot").value = document.getElementById("GrandTotal").value;

    //};

    this.load = function (Pk_PaymtNo, Cardno, Name, BnkName, typeofcard, ChequeNo, ChequeDate, CBankName, TransNo, GateWay, TBankName, TargetedAccount, typeoftrn, CashDenominations) {
        this.data["Pk_PaymtNo"] = Pk_PaymtNo;
        this.data["CardNo"] = Cardno;
        this.data["CName"] = Name;
        this.data["BankName"] = BnkName;
        this.data["CardType"] = typeofcard;
        this.data["ChequeNo"] = ChequeNo;
        this.data["ChequeDate"] = ChequeDate;
        this.data["CBankName"] = CBankName;
        this.data["TransNo"] = TransNo;
        this.data["GateWay"] = GateWay;
        this.data["TBankName"] = TBankName;
        this.data["TargetedAccount"] = TargetedAccount;
        this.data["Trans_Type"] = typeoftrn;
        this.data["CashDenominations"] = CashDenominations;
      


    };
}


