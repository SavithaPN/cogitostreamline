﻿function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Wire List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Wire/WireListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name'
            },
            Unit: {
                title: 'Unit'
            },

            Brand: {
                title: 'Brand'
            }
            ,
            MaterialType: {
                title: 'MaterialType'
            }


        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtGumName').val(),
            MaterialType: $('#TxtMatType').val(),
            Brand: $('#TxtBrand').val()
        });

    });

    $('#TxtGumName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtGumName').val(),
                MaterialType: $('#TxtMatType').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });


    $('#TxtMatType').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtGumName').val(),
                MaterialType: $('#TxtMatType').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });


    $('#TxtBrand').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtGumName').val(),
                MaterialType: $('#TxtMatType').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });




    _page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);


}
function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Wire";
}


function CheckGumDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("Wire/WireDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Wire Name Already added";
}