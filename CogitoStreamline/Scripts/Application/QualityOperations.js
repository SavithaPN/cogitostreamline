﻿//Name          : Quality Check Operations ----javascript files
//Description   : Contains the  material indent Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. Check date validations
//Author        : shantha
//Date            :   01/09/2015
//CRH Number      :   SL0046
//Modifications : 


var curViewModel = null;
var bFromSave = false;
var objContextEdit = false;

var gMaterials = "";
var VendorID = "";
var MaterialID = "";
var IndentNo = "";
var div = null;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Quality Check List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/QualityCheckListByFiter',
            updateAction: '',
            deleteAction: '',
            createAction: ''
        },
        fields: {
            Pk_QualityCheck: {
                title: 'Check No.',
                key: true
            },

            CheckDate: {
                title: 'Date'
            },
         
            InvoiceNumber: {
                title: 'InvoiceNumber'
            },
            Fk_PONo: {
                title: 'PONo'
            },
            DCNo: {
                title: 'DCNo'
            },
           
            }
    });

    $('#dtDate').datepicker({
        autoclose: true
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            QCNo: $('#txtIndentNumber').val(),
            Vendor: $('#TxtVendorName').val(),
            InvoiceNo: $('#Txtinward').val(),
            CheckDate: $('#TxtDate').val(),

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

   

 

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });



    $('#TxtDate').datepicker({ autoclose: true });

    $("#searchIndentDialog").width(1000);
    $("#searchIndentDialog").css("top", "10px");
    $("#searchIndentDialog").css("left", "400px");


}

//function afterNewShow(viewObject) {

//    objContextEdit = false;
//    curViewModel = viewObject.viewModel;
//    var dNow = new Date();
//    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
//    $('#dtDate').datepicker({ autoclose: true });


//    $("#dropzone").wgUpload({
//        wgTag: "CheckQuality",
//        upLoadUrl: "/FileUpload",
//        property: "Documents",
//        viewModel: viewObject.viewModel,
//        fileLocation: _comLayer.curServer() + "/uploads/"
//    });


//    $('#cmdVendorSearch').click(function (e) {
//        e.preventDefault();
//        setUpVendorSearch();
//        $("#searchDialog").modal("show");
//    });
//    $('#cmdMaterialSearch').click(function (e) {
//        e.preventDefault();
//        setUpMaterialSearch();
//        $("#searchMaterialDialog").modal("show");
//    });
   
//    //setUpMaterialSearch();
//    //setUpCustomerSearch();
//}
function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });
    $('#txtdlgFromDate').datepicker({ autoclose: true });
    $('#txtdlgToDate').datepicker({ autoclose: true });
    
    

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            }

        }
    });


    $('#cmdMPOSearch').click(function (e) {
        e.preventDefault();
        setUpPOSearch();
        $("#searchIndentDialog").modal("show");
    });

    //configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "CheckQuality", "_AddMaterial", function () { return new IssuedMaterial() });



    $("#dropzone").wgUpload({
        wgTag: "CheckQuality",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });

}
function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }


    //if ($("#dtRequiredDate").val() == "") {
    //    $("#dtRequiredDate").validationEngine('showPrompt', 'Enter Required Date', 'error', true)
    //    bValidation = false;
    //}

    if ($("#txtFk_Material").val() == "") {
        $("#txtFk_Material").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}



function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#dtDate').datepicker({
        autoclose: true
    });


    bEditContext = true;
    $("#dropzone").wgUpload({
        wgTag: "CheckQuality",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });

    if ($.trim($('#Result').val()) == "True") {
        $("#Pass").prop("checked", true);
    }
    else if ($.trim($('#Result').val()) == "true") {
        $("#Pass").prop("checked", true);
    }
    else if ($.trim($('#Result').val()) == "False") {
        $("#Fail").prop("checked", true);
    }
    else if ($.trim($('#Result').val()) == "false") {
        $("#Fail").prop("checked", true);
    }
    else {
        $("#Fail").prop("checked", false);
        $("#Pass").prop("checked", false);
    }


  
}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {

    $('#txtFk_Vendor').wgReferenceField({
        keyProperty: "Pk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewObject.viewModel
    });

}


function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchsHeader").text("Material Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndentNo' placeholder='Indent No.' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendorName = "<input type='text' id='txtdlgVedndorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";


    $("#dlgSearchMaterialFields").append(txtFieldMaterialName);
    $("#dlgSearchMaterialFields").append(txtFieldIndentNo);
    $("#dlgSearchMaterialFields").append(txtFieldVendorName);

    $('#MaterialSearhContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Material/MaterialList'
        },
        fields: {
            Fk_Material: {
                title: 'MaterialID',
                edit: false,
                list:false
            },
            Name: {
                   title: 'Material Name'
                    },
            Pk_MaterialOrderMasterId: {
                title: 'IndentNo',
                key: true
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false
            },
            Quantity:
                 {
                     title: 'IndQty'
                 },
            QC_Qty:
                {
                    title: 'QC Qty'
                },
            Pending:
                {
                    title: 'PQty'
                }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearhContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val(),

            Vendor: $('#txtdlgIndentNo').val,
            IndentNo: $('#txtFk_IndentNumber').val()


        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearhContainer').jtable('load', {
            IndentNo: $('#txtFk_IndentNumber').val(),
            Name: $('#txtdlgMaterialName').val()
        });
    });



    $('#cmdMatDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearhContainer').jtable('selectedRows');
        $("#searchMaterialDialog").modal("hide");

        $('#txtFk_Material').wgReferenceField("setData", rows[0].data['Fk_Material'])

        document.getElementById('Pending').value = rows[0].data.Pending;

        MaterialID = rows[0].keyValue;

        IndentNo = document.getElementById('#txtFk_IndentNumber').value;

    });
}
function setUpPOSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchHeader").text("PO Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Purchase Order List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/PurchaseOrder/PurchaseOrderListByFiter'
        },
        fields: {
            Pk_PONo: {
                title: 'Po Number',
                key: true
            },

            PODate: {
                title: 'PO Date'
            },

            Fk_Vendor: {
                title: 'Vendor Name'
            },
         
            Status: {
                title: 'Status'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgIndent').val(),
            Fk_Vendor: $('#txtdlgVendor').val(),
            TxtFromDate: $('#txtdlgFromDate').val(),
            TxtToDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
   
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_PO').value = rows[0].keyValue;
        IndentNo = rows[0].keyValue;
        document.getElementById('txtFk_Vendor').value = rows[0].data.Fk_Vendor;

        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}

//function setUpVendorSearch() {
//    //Enquiry

//    cleanSearchDialog();

//    $("#srchssHeader").text("Vendor Search");

//    //Adding Search fields
//    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

//    $("#dlgSearchFields").append(txtFieldVendorName);


//    $('#VendorSearhContainer').jtable({
//        title: 'Vendor List',
//        paging: true,
//        pageSize: 15,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/Vendor/VendorListByFiter',
//        },
//        fields: {
//            Pk_Vendor: {
//                title: 'Vendor ID',
//                key: true
//            },
//            VendorName: {
//                title: 'Vendor Name',
//                edit: false
//            }
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#VendorSearhContainer').jtable('load', {
//            VendorName: $('#txtdlgVendorName').val()
//        });
//    });
//    $('#LoadRecordsButton').click();

//    $('#cmdSearch').click(function (e) {
//        e.preventDefault();
//        $('#VendorSearhContainer').jtable('load', {
//            VendorName: $('#txtdlgVendorName').val()
//        });
//    });

//    $('#cmdDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#VendorSearhContainer').jtable('selectedRows');
//        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].keyValue);
//        VendorID = rows[0].keyValue;
//        $("#searchDialog").modal("hide");
//    });

//}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["CheckDate"] = $('#dtDate').val();
    // viewModel.data["Fk_Vendor"] = $('#TxtVendorId').val();
    //viewModel.data["Fk_Vendor"] = ;
    viewModel.data["Pending"] = $('#Quantity').val();
    viewModel.data["Result"] = $('#Result').val();

    //viewModel.data["Fk_Material"] = $('#txtFk_Material').val();
    //viewModel.data["Fk_Material"] = MaterialID;

    viewModel.data["Fk_IndentNumber"] = $('#txtFk_IndentNumber').val();
    viewModel.data["Fk_IndentNumber"] = IndentNo;

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["CheckDate"] = $('#dtDate').val();
    viewModel1.data["Pending"] = $('#Quantity').val();
    // viewModel1.data["Fk_Vendor"] = $('#TxtVendorId').val();

    viewModel1.data["Result"] = $('#Result').val();

    //viewModel1.data["Fk_Material"] = $('#txtFk_Material').val();
    //viewModel1.data["Fk_Material"] = MaterialID;

    viewModel1.data["Fk_IndentNumber"] = $('#txtFk_IndentNumber').val();
    viewModel1.data["Fk_IndentNumber"] = IndentNo;


}


function ReferenceFieldNotInitilized(viewModel) {

    $('#txtFk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtFk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);

    }
}
function checkstock() {
    //if (objContextEdit == false) {
        if (Number($('#Pending').val()) < Number($('#Quantity').val())) {
            return "* " + "CheckQuantity cannot Exceed Pending Qty :" + $('#Pending').val();
        }
        else {

        }
    }
//}
function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgSearchMaterialFields').empty();
}



function radioClass(str1) {
    if (str1 == "Pass") {
        document.getElementById('Result').value = "True";

    }
    else if (str1 == "Fail") {
        document.getElementById('Result').value = "False";
    }
}

function showOnlyBoard() {


    div = document.getElementById('searchDialogBoard');
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";

        $('#searchDialogInk').hide();
        $('#searchDialogGum').hide();
        $('#searchDialogHoney').hide();
        $('#searchDialogPaper').hide();

    }

}
function showOnlyInk() {


    div = document.getElementById('searchDialogInk');
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";

        $('#searchDialogBoard').hide();
        $('#searchDialogGum').hide();
        $('#searchDialogHoney').hide();
        $('#searchDialogPaper').hide();

    }

}

function showOnlyGum() {


    div = document.getElementById('searchDialogGum');
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";

        $('#searchDialogBoard').hide();
        $('#searchDialogInk').hide();
        $('#searchDialogHoney').hide();
        $('#searchDialogPaper').hide();

    }

}

function showOnlyHoney() {


    div = document.getElementById('searchDialogHoney');
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";

        $('#searchDialogBoard').hide();
        $('#searchDialogInk').hide();
        $('#searchDialogGum').hide();
        $('#searchDialogPaper').hide();

    }

}
function showOnlyPaper() {


    div = document.getElementById('searchDialogPaper');
    if (div.style.display !== "none") {
        div.style.display = "none";
    }
    else {
        div.style.display = "block";

        $('#searchDialogBoard').hide();
        $('#searchDialogInk').hide();
        $('#searchDialogGum').hide();
        $('#searchDialogHoney').hide();

    }

}