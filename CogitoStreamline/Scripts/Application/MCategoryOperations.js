﻿//Name          : material category Operations ----javascript files
//Description   : Contains the  material category Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckMaterialCategoryDuplicate(duplication checking) 
//Author        : shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0004
//Modifications : 



function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'MaterialCategoryList',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MCatListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_MaterialCategory: {
                title: 'Id',
                key: true
            },
            Name: {
                title: 'Name'
            }
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_MaterialCategory: $('#TxtPkMcat').val(),
            Name: $('#TxtName').val()
        });

    });
    $('#TxtPkMcat').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_MaterialCategory: $('#TxtPkMcat').val(),
                Name: $('#TxtName').val()
            });
        }

    });

    $('#TxtName').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_MaterialCategory: $('#TxtPkMcat').val(),
                Name: $('#TxtName').val()
            });
        }

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}


function CheckMCatDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("MaterialCategory/MCatDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Category Name Already added";

}


