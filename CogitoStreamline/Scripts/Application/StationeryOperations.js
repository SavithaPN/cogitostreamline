﻿
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Stationery Items List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Stationery/StationeryListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: ' Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name',
                width: '20%'
            },
            Description: {
                title: 'Description',
                width: '20%'
            },
            


        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtPaperName').val(),
            Description: $('#TxtPaperDeckle').val(),

        });

    });


    $('#TxtPaperName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Description: $('#TxtPaperDeckle').val(),
            });
        }
    });

    $('#TxtPaperType').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Description: $('#TxtPaperDeckle').val(),
            });
        }
    });



    $('#TxtPaperColour').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtPaperName').val(),
                Description: $('#TxtPaperDeckle').val(),
            });
        }
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

   _page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


 


}

function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Stationery";

    //$('#cmdCreate').click(function (e) {
    //    e.preventDefault();
    //    alert('save clicked');
    //});

}

function beforeModelSaveEx() {
    //alert('save clicked at before');
    //var viewModel = _page.getViewByName('New').viewModel;
    //viewModel.data["Description"] = $('#Description').val();

    //var viewModel1 = _page.getViewByName('Edit').viewModel;
    //viewModel1.data["Description"] = $('#Description').val();
}

function CheckPaperDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("Stationery/StationeryDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Name Already added";

}

//function beforeModelSaveEx() {
//    var viewModel = _page.getViewByName('New').viewModel;
//    //viewModel.data["Name"] = $.trim($('#MILL option:selected').text()) + " " + $.trim($('#BF').val()) + "BF " + $('#PaperType  option:selected').text() + " " + $.trim($('#GSM').val()) + "GSM"
//    viewModel.data["Name"] = $.trim($('#GSM').val()) + "GSM" + $.trim($('#BF').val()) + "BF" + $.trim($('#Deckle').val()) + "D"



//}
