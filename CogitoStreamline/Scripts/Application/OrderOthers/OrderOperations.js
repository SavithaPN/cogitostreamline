﻿ 
var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var CustCode = "";
var EnqID;
var PaperTrue = false;


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Order List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order/OrderListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            CustDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Customer Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Customer Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerListByFiter?Pk_Customer=' + data.record.CustID
                                        },
                                        fields: {

                                            Pk_Customer: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            CustomerName: {
                                                title: 'CustomerName',
                                                width: '15%'
                                            },
                                            CustomerAddress: {
                                                title: 'CustomerAddress'
                                            },
                                            City: {
                                                title: 'City',
                                                width: '10%'
                                            },
                                            State: {
                                                title: 'State',
                                                width: '10%'
                                            },
                                            Country: {
                                                title: 'Country',
                                                width: '10%'
                                            },
                                            PinCode: {
                                                title: 'PinCode',
                                                width: '10%'
                                            },
                                            CustomerContact: {
                                                title: 'Mobile',
                                                key: false
                                            },
                                            LandLine: {
                                                title: 'LandLIne'
                                            },
                                            Email: {
                                                title: 'Email',
                                                width: '10%'
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Contacts'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Contacts List',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                                                        },
                                                                        fields: {
                                                                            Pk_CustomerContacts: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            ContactPersonName: {
                                                                                title: 'Person Name',
                                                                                key: false
                                                                            },
                                                                            ContactPersonDesignation: {
                                                                                title: 'Designation'

                                                                            },
                                                                            ContactPersonNumber: {
                                                                                title: 'Contact Number',
                                                                                width: '2%'
                                                                            },
                                                                            ContactPersonEmailId: {
                                                                                title: 'Email',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            Pk_Order: {
                title: 'Order No',
                key: true,
                width: '5%'
            },
        
            CustomerName: {
                title: 'CustomerName',
                width: '12%'
            },
            PONo: {
                title: 'Cust PoNo',
                edit: false
            },
            OrderDate: {
                title: 'P.Order Date'
            },
            Fk_Enquiry: {
                title: 'Enq.No.',
                width: '3%'
            },
          
            Select_A: {
                title: "Close Ord. Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Close Ord.</button>");
                    $(button1).click(function () {
                        PO = row.record.Pk_Order;
                        _comLayer.parameters.add("POno", PO);
                        oResult = _comLayer.executeSyncAction("Order/StatusUpdate", _comLayer.parameters);
                        if (oResult = true)
                        { alert("Ord. Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Select_B: {
                title: "Open Ord. Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Open Ord.</button>");
                    $(button1).click(function () {
                        PO = row.record.Pk_Order;
                        _comLayer.parameters.add("POno", PO);
                        oResult = _comLayer.executeSyncAction("Order/OpenStatus", _comLayer.parameters);
                        if (oResult = true)
                        { alert("Ord. Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        OrderID = row.record.Pk_Order;
                        _comLayer.parameters.add("OrderID", OrderID);
                        _comLayer.executeSyncAction("Order/OrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Order" + OrderID + ".pdf"

                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/Order" + OrderID + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');

                            return true;
                        }


                        /////////////////////////

                       
                    });
                    return button;
                }
            },
            
            ///////////////////////////////////////////////
            Details: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-cart-3' title='Delivery Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Delivery Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Order/OrderDeliveryDetails?Pk_Order=' + data.record.Pk_Order
                                        },
                                        fields: {
                                            Ddate: {
                                                title: 'Delivery Date',
                                                key: false
                                            },
                                            BId: {
                                                title: 'BoxID'
                                            },
                                            BName: {
                                                title: 'BoxName'
                                            },
                                            PartName: {
                                                title: 'PartName'
                                            },
                                            OrdQty: {
                                                title: 'OrdQty'
                                            },
                                            EnqQty: {
                                                title: 'EnqQty',
                                                width: '20%'
                                            },
                                         
                                        //},

                                        ///
                                        BoxDet: {
                                            title: 'Box',
                                            width: '2%',
                                            listClass: 'child-opener-image-column',
                                            display: function (data) {
                                                var $img = $("<i class='icon-iphone' title='Box'></i>");
                                                $img.click(function () {
                                                    //var BoxID = data.record.Fk_BoxID;
                                                    $('#MainSearchContainer').jtable('openChildTable',
                                                                $img.closest('tr'),
                                                                {
                                                                    title: 'Box Details',
                                                                    paging: true,
                                                                    pageSize: 10,
                                                                    actions: {
                                                                        listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.BId
                                                                    },
                                                                    fields: {
                                                                        Pk_BoxSpecID: {
                                                                            title: 'Spec ID',
                                                                            key: true,
                                                                            list:false
                                                                        },
                                                                        OYes: {
                                                                            title: 'OuterShell',
                                                                            display: function (data) {
                                                                                if (data.record.OYes == true || data.record.OYes == 'True') {
                                                                                    return 'Yes';
                                                                                }

                                                                            },

                                                                        },
                                                                        CYes: {
                                                                            title: 'Cap',
                                                                            display: function (data) {
                                                                                if (data.record.CYes == true || data.record.CYes == 'True') {
                                                                                    return 'Yes';
                                                                                }

                                                                            },
                                                                        },
                                                                        LPYes: {
                                                                            title: 'L.Partition',
                                                                            display: function (data) {
                                                                                if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                                                    return 'Yes';
                                                                                }
                                                                                //else {
                                                                                //    width: '2%';
                                                                                //}
                                                                            },
                                                                        },
                                                                        WPYes: {
                                                                            title: 'W.Partition',
                                                                            display: function (data) {
                                                                                if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                                                    return 'Yes';
                                                                                }

                                                                            },
                                                                        },
                                                                        PYes: {
                                                                            title: 'Plate',
                                                                            display: function (data) {
                                                                                if (data.record.PYes == true || data.record.PYes == 'True') {
                                                                                    return 'Yes';
                                                                                }

                                                                            },
                                                                        },
                                                                        Details: {
                                                                            title: '',
                                                                            width: '1%',
                                                                            sorting: false,
                                                                            paging: true,
                                                                            pageSize: 5,
                                                                            edit: false,
                                                                            create: false,
                                                                            listClass: 'child-opener-image-column',
                                                                            display: function (data) {
                                                                                var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                                                $img.click(function () {
                                                                                    $('#MainSearchContainer').jtable('openChildTable',
                                                                                                $img.closest('tr'),
                                                                                                {
                                                                                                    title: 'Specifications',
                                                                                                    paging: true,
                                                                                                    pageSize: 5,
                                                                                                    actions: {
                                                                                                        listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                                                    },
                                                                                                    fields: {
                                                                                                        Pk_PartPropertyID: {
                                                                                                            title: 'Property ID',
                                                                                                            key: true,
                                                                                                            create: false,
                                                                                                            edit: false,
                                                                                                            width: '2%',
                                                                                                            list: false
                                                                                                        },
                                                                                                        Length: {
                                                                                                            title: 'Length',
                                                                                                            key: false,
                                                                                                            width: '2%'
                                                                                                        },
                                                                                                        Width: {
                                                                                                            title: 'Width',
                                                                                                            width: '2%'
                                                                                                        },
                                                                                                        Height: {
                                                                                                            title: 'Height',
                                                                                                            width: '2%'
                                                                                                        },
                                                                                                        Weight: {
                                                                                                            title: 'Weight',
                                                                                                            width: '2%'
                                                                                                        },

                                                                                                        Details: {
                                                                                                            title: '',
                                                                                                            width: '1%',
                                                                                                            sorting: false,
                                                                                                            paging: true,
                                                                                                            pageSize: 5,
                                                                                                            edit: false,
                                                                                                            create: false,
                                                                                                            listClass: 'child-opener-image-column',
                                                                                                            display: function (data) {
                                                                                                                var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                                                $img.click(function () {
                                                                                                                    $('#MainSearchContainer').jtable('openChildTable',
                                                                                                                                $img.closest('tr'),
                                                                                                                                {
                                                                                                                                    title: 'Layers Details',
                                                                                                                                    paging: true,
                                                                                                                                    pageSize: 10,
                                                                                                                                    actions: {
                                                                                                                                        listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                                                    },
                                                                                                                                    fields: {
                                                                                                                                        Pk_LayerID: {
                                                                                                                                            title: 'Layer ID',
                                                                                                                                            key: true,
                                                                                                                                            create: false,
                                                                                                                                            edit: false,
                                                                                                                                            list: false
                                                                                                                                        },
                                                                                                                                        GSM: {
                                                                                                                                            title: 'GSM',
                                                                                                                                            key: false
                                                                                                                                        },
                                                                                                                                        BF: {
                                                                                                                                            title: 'BF'

                                                                                                                                        },
                                                                                                                                        Weight: {
                                                                                                                                            title: 'Weight'
                                                                                                                                        }
                                                                                                                                    },
                                                                                                                                    formClosed: function (event, data) {
                                                                                                                                        data.form.validationEngine('hide');
                                                                                                                                        data.form.validationEngine('detach');
                                                                                                                                    }
                                                                                                                                }, function (data) { //opened handler
                                                                                                                                    data.childTable.jtable('load');
                                                                                                                                });
                                                                                                                });
                                                                                                                return $img;
                                                                                                            }
                                                                                                        },
                                                                                                    },
                                                                                                    formClosed: function (event, data) {
                                                                                                        data.form.validationEngine('hide');
                                                                                                        data.form.validationEngine('detach');
                                                                                                    }
                                                                                                }, function (data) { //opened handler
                                                                                                    data.childTable.jtable('load');
                                                                                                });
                                                                                });
                                                                                return $img;
                                                                            }
                                                                        },
                                                                    },
                                                                    formClosed: function (event, data) {
                                                                        data.form.validationEngine('hide');
                                                                        data.form.validationEngine('detach');
                                                                    }
                                                                }, function (data) { //opened handler
                                                                    data.childTable.jtable('load');
                                                                });
                                                });
                                                return $img;
                                            }
                                        },
                                        },
                                        ////
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },

    }
});


    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            CustomerName: $('#TxtCustomerName').val(),
            Pk_Order: $('#TxtOrderNo').val(),
            FromOrderDate: $('#TxtFromDate').val(),
            ToOrderDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    //Add addtional data sources (Combobox methods)
    //_page.getViewByName('New').viewModel.addAddtionalDataSources("fk_Customer", "getCustomer", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("fk_Customer", "getCustomer", null);


    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });
    
    $("#searchDialog").width(1000);
    $("#searchDialog").css("top", "50px");
    $("#searchDialog").css("left", "300px");

    $('#dtDdate').datepicker({ autoclose: true });
}

function afterNewShow(viewObject) {

    bEditContext = false;
    // $('#cmdCreate').attr('disabled', true);
    //$('#dtPOrderDate').datepicker({  autoclose: true});

    var dNow = new Date();
    document.getElementById('dtPOrderDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    $('#dtPOrderDate').datepicker({ autoclose: true });
    $('#dtDdate').datepicker({ autoclose: true });


    EnqID = document.getElementById('fk_Enquiry').value;


    $('#cmdEnquerySearch').click(function (e) {
        e.preventDefault();
        setUpEnquirySearch(viewObject);
        $("#searchDialog").modal("show");
    });
    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        setUpCustomerSearch(viewObject);
        $("#searchBoxDialog").modal("show");
        $("#searchBoxDialog").width(900);
        $("#searchBoxDialog").height(700);
    });

    curViewModel = viewObject.viewModel;


    $('#divDeliveryschedule').jtable({
        title: 'Item Details',
        paging: true,
        pageSize: 7,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order_Others/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            //Ddate: {
            //    title: 'Del. Date',
            //    key: false
            //},
            Fk_EnquiryChild: {
                title: 'Fk_EnquiryChild',
                list: false

            },
            Fk_Material: {
                title: 'Item No.',
                list: true
            },

            Name: {
                title: 'B.Name',
            },

            Rollno: {
                title: 'Rollno',
                list:false

            },
            
            OrdQty: {
                title: 'OrdQty',
                width: '2%'
            },
            EnqQty: {
                title: 'PkStockID',
                width: '25%',
                list:false
            },
        }
    });

    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "Order_Others", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });
    //configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");

    //$('#cmdCreate').click(function () {
    //    alert('Yess');
    //});

}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Ddate"] = $('#dtDeliveryDate').val();
 
   


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Ddate"] = $('#dtDeliveryDate').val();
   
}

function afterOneToManyDialogShow(property) {
    $('#dtDeliveryDate').datepicker({
        autoclose: true
    });

   

    $('#dtDdate').datepicker({
        autoclose: true
    });

    //dtDdate

    $('#txtdlgEnquiryToDate').datepicker({
        autoclose: true
    });
 
    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpEnquiryBoxSearch();
        document.getElementById("Others").checked = false;
        document.getElementById("Paper").checked = false;
        document.getElementById('Selected').value = "";
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateDeliverySchedule", "none");

    });
    //$('#cmdSearchPaperdetails').click(function (e) {
    //    e.preventDefault();
    //    PaperTrue = true;
    //    //setUpPaperSearch();
    //    _util.setDivPosition("divSearchMaterial", "block");
    //    _util.setDivPosition("divCreateDeliverySchedule", "none");

    //});
}
function setUpPaperSearch()
{
    $('#MaterialSearchContainer').jtable({
        title: 'Stock List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/PaperStockList/PaperStockListAge'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
                //AddToMaterialList();

                //$('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

            });
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '2%'
            },
            Pk_PaperStock: {
                title: 'Pk_PaperStock',
                list: false,
                width: '2%'
            },
            SName: {
                title: 'Mill',
                width: '5%'
            },
            RollNo: {
                title: 'RollNo',
                width: '5%'
            },

            MaterialName: {
                title: 'MaterialName',
                width: '15%'
            },
            GSM: {
                title: 'GSM',
                width: '5%'
            },
            BF: {
                title: 'BF',
                width: '2%'
            },
            Deckle: {
                title: 'Deckle',
                width: '2%'
            },
            //Mill: {
            //    title: 'Mill',
            //    width: '5%'
            //},
            Color: {
                title: 'Color',
                width: '5%'
            },
            Quantity: {
                title: 'Stock Qty',
                width: '5%'
            },
            //Age: {
            //    title: 'Age',
            //    width: '5%'
            //},
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            //MaterialName: $('#txtdlgMaterialName').val(),
            GSM: $('#txtdlgGSM').val(),
            BF: $('#txtdlgBF').val(),
            Deckle: $('#txtdlgD').val(),
            RollNo: $('#txtdlgReel').val(),


        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();

       
            $('#MaterialSearchContainer').jtable('load', {
                //MaterialName: $('#txtdlgMaterialName').val(),
                GSM: $('#txtdlgGSM').val(),
                BF: $('#txtdlgBF').val(),
                Deckle: $('#txtdlgD').val(),
                RollNo: $('#txtdlgReel').val(),

            });
        
    });



    $('#cmdMatDone').click(function (e) {
        //alert(PaperTrue);
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Box').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('OrdQty').value = rows[0].data.Quantity;
        document.getElementById('EnqQty').value = rows[0].data.Pk_PaperStock;
        document.getElementById('Rate').value = rows[0].data.RollNo;
       
      
    });
}


function setUpEnquiryBoxSearch() {
    var rowCount = $('#MaterialSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MaterialSearchContainer').jtable('destroy');

    }
    var selValue = document.getElementById('Selected').value;
    if (selValue != "Paper")
        $('#MaterialSearchContainer').jtable({
            title: 'Items List',
            paging: true,
            pageSize: 7,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/Order_Others/GetOrderItems'
            },

            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdMaterialDone').click();
                    $("#OrdQty").focus();
                });
            },


            fields: {
                Pk_Material: {
                    title: 'Mat.No',
                    key: true,
                    list: true
                },
                MaterialName: {
                    title: 'Mat Name',
                    width: '25%'
                },
                //PName=p.PName,
                //PartQty=p.PartQty,


                CatName: {
                    title: 'Category',
                    width: '25%'
                },

            }
        });
    else
    {
        $('#MaterialSearchContainer').jtable({
            title: 'Stock List',
            paging: true,
            pageSize: 7,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/PaperStockList/PaperStockListAge'
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdMaterialDone').click();
                    //AddToMaterialList();

                    //$('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

                });
            },
            fields: {
                Pk_Material: {
                    title: 'Id',
                    key: true,
                    width: '2%'
                },
                Pk_PaperStock: {
                    title: 'Pk_PaperStock',
                    list: false,
                    width: '2%'
                },
                SName: {
                    title: 'Mill',
                    width: '5%'
                },
                RollNo: {
                    title: 'RollNo',
                    width: '5%'
                },

                MaterialName: {
                    title: 'MaterialName',
                    width: '15%'
                },
                GSM: {
                    title: 'GSM',
                    width: '5%'
                },
                BF: {
                    title: 'BF',
                    width: '2%'
                },
                Deckle: {
                    title: 'Deckle',
                    width: '2%'
                },
                //Mill: {
                //    title: 'Mill',
                //    width: '5%'
                //},
                Color: {
                    title: 'Color',
                    width: '5%'
                },
                Quantity: {
                    title: 'Stock Qty',
                    width: '5%'
                },
                //Age: {
                //    title: 'Age',
                //    width: '5%'
                //},
            }
        });
    }
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#MaterialSearchContainer').jtable('load', {
                MaterialName: $('#txtMaterial').val(),
                //Pk_MaterialIssueID: $('#Fk_Vendor').val()
             //  MatName: $('#txtfk_Customer').val(),
                    
            });
        });
        $('#LoadRecordsButton').click();

        $('#cmdMaterialSearch').click(function (e) {
            e.preventDefault();
            $('#MaterialSearchContainer').jtable('load', {
                //CustName: $('#txtfk_Customer').val(),
                //BoxName: $('#txtMaterial').val(),
                MaterialName: $('#txtMaterial').val(),
                //Pk_MaterialIssueID: $('#Fk_Vendor').val()
            });
        });

        $('#cmdMaterialDone').click(function (e) {
            e.preventDefault();
           
                var rows = $('#MaterialSearchContainer').jtable('selectedRows');
                $('#txtFk_Box').wgReferenceField("setData", rows[0].keyValue);
       
                if (rows[0].data.Quantity > 0) {
                    document.getElementById('OrdQty').value = rows[0].data.Quantity;
                    document.getElementById('EnqQty').value = rows[0].data.Pk_PaperStock;
                    document.getElementById('Rollno').value = rows[0].data.RollNo;
                }
                else {
                    $("#OrdQty").focus();
                }
            
            
            _util.setDivPosition("divCreateDeliverySchedule", "block");
                _util.setDivPosition("divSearchMaterial", "none");
               
         
         
   });
        


    //}
}


function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;

    // Based on State Can Disable the Save Button
    //var status = viewObject.viewModel.data["Fk_Status"]();

    //if (status == "Completed") {

    //    $(".modelControl").attr('disabled', 'disabled');
    //    $("#cmdAddSchedule").attr('disabled', true);
    //    $("#cmdCreate").attr('disabled', true);
    //}
    $('#cmdAddressSearch').click(function (e) {
        e.preventDefault();
        CustCode = viewObject.viewModel.data["Fk_Customer"]();
        setUpAddressSearch(viewObject);
        $("#searchAddressDialog").modal("show");
    });

    $("#cmdDeliverySchedule").click(function (e) {
        e.preventDefault();
        var OrderNumber = $("#hidPkOrder").val();
        window.open("/DeliverySchedule?Order=" + OrderNumber, "Schedule");
    });

    $('#dtPOrderDate').datepicker({
        autoclose: true
    });
    $('#dtDdate').datepicker({ autoclose: true });

    //$('#cmdSearchMaterialsdetails').click(function (e) {
    //    e.preventDefault();
    //    setUpEnquiryBoxSearch();
    //    _util.setDivPosition("divSearchMaterial", "block");
    //    _util.setDivPosition("divCreateDeliverySchedule", "none");

    //});

    $('#cmdEnquerySearch').click(function (e) {
        e.preventDefault();
        setUpEnquirySearch(viewObject);
        $("#searchDialog").modal("show");
    });
    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        setUpBoxSearch(viewObject);
        $("#searchDialog").modal("show");
    });
    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        setUpCustomerSearch();
        $("#searchDialog").modal("show");
    });

    $('#cmdProductSearch').click(function (e) {
        e.preventDefault();
        setUpProductSearch();
        $("#searchDialog").modal("show");
    });

    $('#divDeliveryschedule').jtable({
        title: 'Box Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order/Bounce',
            updateAction: '',
            deleteAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Pk_OrderChild: {
                title: 'OrderChild',
                key: true,
                list: false
            },
            Fk_BoxID: {
                title: 'box',
                list: false
            },

            Name: {
                title: 'B.Name',
            },
            //txtPart: {
            //    title: 'Part Name',
            //},
            EnqQty: {
                title: 'Enq.Qty',
            },

            OrdQty: {
                title: 'OrdQty',
                width: '2%'
            },
            //DeliveryDate: {
            //    title: 'Dd ate',
            //    width: '2%'
            //},
            Ddate: {
        title: 'Delv.Date',
        width: '2%'
    }
        }
    });


    var oSCuts = viewObject.viewModel.data.deliverySchedule();
    viewObject.viewModel.data["deliverySchedules"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new DeliveryScheduleItem();
        //Pk_OrderChild, OrdQty, txtFk_Box, Name, EnqQty, Ddate, Fk_PartID, txtPart
        oCut.load(oSCuts[i].Pk_OrderChild, oSCuts[i].OrdQty, oSCuts[i].txtFk_Box, oSCuts[i].Name, oSCuts[i].EnqQty, oSCuts[i].Ddate, oSCuts[i].Fk_PartID, oSCuts[i].txtPart);
        viewObject.viewModel.data["deliverySchedules"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "Order_Others", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });

    bEditContext = true;

    $('#wfTransDisplay').wgWorkFlowHistory({
        wgTag: "Order",
        viewModel: viewObject.viewModel
    });

    $('#wfButtons').wgWorkFlowButtons({
        wgTag: "Order",
        viewModel: viewObject.viewModel,
        stateField: "Fk_Status",
        belongsTo: "Order",
        pkField: "Pk_Order"
    });


}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {
    $('#fk_Enquiry').wgReferenceField({
        keyProperty: "fk_Enquiry",
        displayProperty: "Description",
        loadPath: "Enquiry/Load",
        viewModel: viewObject.viewModel
    });

}

function afterModelSaveEx(result) {
    alertify.set({
        labels: {
            ok: "Yes",
            cancel: "No"
        },
        buttonReverse: true
    });

    //strvalcheck = "http://123.63.33.43/blank/sms/user/urlsms.php?username=usermaedhaa&pass=maedhaa@123&senderid=MAEDHA&dest_mobileno=9900444553&message=" + "Order  " + result.Message + "&" + "response=Y"
    //window.open(strvalcheck, '_blank ', 'width=100,height=150');

    ////var x = window.open(strvalcheck, '_self ').close();
    //var bValidation = true;
    //if ($("#Fk_ShippingId").val() < 1) {
    //    $("#Fk_ShippingId").validationEngine('showPrompt', 'Select Shipping Address', 'error', true)
    //    bValidation = false;
    //}
    //return bValidation;
}

function setUpEnquirySearch(viewObject) {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCommunicationType = "<input type='text' id='txtdlgCommunicationType' placeholder='Communication Type' class='input-large search-query'/>";


    var txtFieldEnquiryFromDate = "<input type='text' id='txtdlgEnquiryFromDate' placeholder='EnquiryID' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldEnquiryToDate = "<input type='text' id='txtdlgEnquiryToDate' placeholder='EnquiryToDate' class='input-large search-query'/>";


    $('#SearchContainer').jtable({
        title: 'Enquiry List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Enquiry/EnquiryListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {
            Pk_Enquiry: {
                title: 'Enquiry No',
                key: true
            },
            Date: {
                title: 'Date'
            },
            Customer: {
                title: 'Customer',
                edit: false
            },
            CommunicationType: {
                title: 'CommunicationType'
            },
            Description: {
                title: 'Description'
            },

            Fk_Status: {
                title: 'Status'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Customer: $('#txtdlgCustomerName').val(),
            CommunicationType: $('#txtdlgCommunicationType').val(),
            EnquiryFromDate: $('#txtdlgEnquiryFromDate').val(),
            EnquiryToDate: $('#txtdlgEnquiryToDate').val()

        });

    });
    $('#txtdlgEnquiryFromDate').datepicker({
        autoclose: true
    });
    $('#txtdlgEnquiryToDate').datepicker({
        autoclose: true
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Customer: $('#txtdlgCustomerName').val(),
            CommunicationType: $('#txtdlgCommunicationType').val(),
            EnquiryFromDate: $('#txtdlgEnquiryFromDate').val(),
            EnquiryToDate: $('#txtdlgEnquiryToDate').val()
        });
    });


    $('#cmdDone').click(function (e) {
        e.preventDefault();

        //e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Customer: $('#txtdlgCustomerName').val(),
            CommunicationType: $('#txtdlgCommunicationType').val(),
            EnquiryFromDate: $('#txtdlgEnquiryFromDate').val(),
            EnquiryToDate: $('#txtdlgEnquiryToDate').val()
        });


        var rows = $('#SearchContainer').jtable('selectedRows');
        if (curViewModel.data["fk_Enquiry"] == undefined) {
            curViewModel.data["fk_Enquiry"] = ko.observable(rows[0].keyValue);
        }
        else { curViewModel.data["fk_Enquiry"](rows[0].keyValue) }

        $("#fk_Enquiry").val(rows[0].keyValue);


        $('#txtfk_Customer').wgReferenceField("setData", rows[0].data.Fk_Customer);
        CustCode = rows[0].data.Fk_Customer;

        $("#searchDialog").modal("hide");

        _comLayer.parameters.clear();
        _comLayer.parameters.add("Id", rows[0].keyValue);

        $("#PO").focus();
     

    });

}

function setUpCustomerSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#MainSearhContainer2').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdBoxDone').click();
            });
        },

        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            CustomerAddress: {
                title: 'Address',
                width:'35%',
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            },
            Email: {
                title: 'Email'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgBoxName').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgBoxName').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MainSearhContainer2').jtable('selectedRows');
        $('#txtfk_Customer').wgReferenceField("setData", rows[0].keyValue);
        CustCode = rows[0].keyValue;
        //document.getElementById('CustID').value = CustCode;
        //alert(CustCode);
        $("#searchBoxDialog").modal("hide");
        //$("#ReferenceEnquiry").focus();
    });

}


function setUpAddressSearch(viewObject) {
    //Enquiry

    ////cleanSearchDialog();

    $('#searchAddressDialog').jtable({
        title: 'Shipping Address List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Customer/CustomerShipping'
        },
        fields: {
            Pk_CustomerShippingId: {
                title: 'ShippingID',
                key: true
            },
            ShippingCustomerName: {
                title: 'Customer Name',
                edit: false
            },
            ShippingAddress: {
                title: 'Address'
            },
            ShippingState: {
                title: 'State'
            },
            ShippingCity: {
                title: 'City'
            }
        }
    });



    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchAddressDialog').jtable('load', {
            CustomerName: $('#txtdlgName').val(),
            SCity: $('#txtdlgCity').val(),
            SState: $('#txtdlgState').val(),
            CustCode: CustCode
        });

    });
    $('#LoadRecordsButton').click();
    //Load all records when page is first shown

    $('#cmdAddSearch').click(function (e) {
        e.preventDefault();
        $('#searchAddressDialog').jtable('load', {
            CustomerName: $('#txtdlgName').val(),
            SCity: $('#txtdlgCity').val(),
            SState: $('#txtdlgState').val(),
            CustCode: CustCode
        });
    });

    $('#cmdAddDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchAddressDialog').jtable('selectedRows');
        $('#Fk_ShippingId').val(rows[0].keyValue);
        $('#ShippingName').val(rows[0].data.ShippingCustomerName);
        $('#ShippingAddress').val(rows[0].data.ShippingAddress);
        $('#ShippingCity').val(rows[0].data.ShippingCity);
        $('#ShippingState').val(rows[0].data.ShippingState);
        $("#searchAddressDialog").modal("hide");
    });

}

function setUpBoxSearch(viewObject) {
    //Enquiry

    // cleanSearchDialog();

    //Adding Search fields
    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldPartNo = "<input type='text' id='txtdlgPartNo' placeholder='PartNo' class='input-large search-query'/>";


    $('#searchBoxDialog').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter'
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdBoxDone').click();
            });
        },
        fields: {
            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            PartNo: {
                title: 'PartNo',
                width: '25%'
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            //Description: {
            //    title: 'Description',
            //    width: '25%'
            //},
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            //Name: $('#txtdlgBoxName').val(),
            //PartNo: $('#txtdlgPartNo').val(),
            BoxName: $('#txtMaterial').val()

        });

    });
    //$('#txtdlgEnquiryToDate').datepicker({
    //    autoclose: true
    //});
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            //Name: $('#txtdlgBoxName').val(),
            //PartNo: $('#txtdlgPartNo').val(),
            BoxName: $('#txtMaterial').val()

        });
    });


    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchBoxDialog').jtable('selectedRows');


        //document.getElementById('Fk_BoxID').value = rows[0].keyValue;
        // document.getElementById('TxtIndent').value = rows[0].data.IndentNo;
        //document.getElementById('TxtMaterial').value = rows[0].data.Fk_Material;
        //document.getElementById('TxtMaterialId').value = rows[0].data.Fk_Material1;

        $('#Fk_BoxID').wgReferenceField("setData", rows[0].data.Pk_BoxID);
        $("#searchBoxDialog").modal("hide");

        //document.getElementById('Quantity').value = rows[0].data.PendQty;
        //document.getElementById('SQuantity').value = rows[0].data.SQuantity;
        //document.getElementById('Unit').value = rows[0].data.UnitName;
        ////document.getElementById('Unit1').value = rows[0].data.UnitName;
        ////document.getElementById('Unit2').value = rows[0].data.UnitName;
        //document.getElementById('TanentName').value = rows[0].data.TanentName;

    });

}

function setUpProductSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldProductName = "<input type='text' id='txtdlgProductName' placeholder='Product Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldProductName);


    $('#SearchContainer').jtable({
        title: 'Product List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Product/ProductListByFiter',
        },
        fields: {
            Pk_Product: {
                title: 'Product Id',
                key: true
            },
            ProductName: {
                title: 'ProductName',
                edit: false
            },
            ProductDescription: {
                title: 'ProductDescription'
            }
        }
    });


    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            ProductName: $('#txtdlgProductName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#txtfk_Product').wgReferenceField("setData", rows[0].keyValue);
        $("#searchDialog").modal("hide");
    });

}

function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["OrderDate"] = $('#dtPOrderDate').val(); 
    //viewModel.data["Ddate"] = $('#dtPOrderDate').val();
    //var rows = $('#divShippingDetails').jtable('selectedRows');
    //viewModel.data["Fk_ShippingId"] = $('#Fk_ShippingId').val();
    // viewModel.data["fk_Customer"] = rows[0].data["fk_Customer"].toString();
    viewModel.data["Fk_Customer"] = CustCode;
    //viewModel.data["DeliveryDate"] = $('#dtDeliveredDate').val();

    //viewModel.data["Fk_EnquiryChild"] = $('#Fk_EnquiryChild').val();
    
    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["OrderDate"] = $('#dtPOrderDate').val();
    //viewModel1.data["Ddate"] = $('#dtDdate').val();
    viewModel1.data["Fk_Customer"] = CustCode;
    //viewModel1.data["Fk_ShippingId"] = $('#Fk_ShippingId').val();
    //viewModel1.data["DeliveryDate"] = $('#dtDeliveredDate').val();
    //txtFk_Box = p.eq_EnquiryChild.Fk_BoxID,
    //viewModel1.data["Fk_EnquiryChild"] = $('#Fk_EnquiryChild').val();
}

function ReferenceFieldNotInitilized(viewModel) {


    $('#txtfk_Customer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });
 

    $('#txtFk_Box').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

 
    if (viewModel.data != null) {
        $('#txtfk_Customer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
        $('#txtfk_Product').wgReferenceField("setData", viewModel.data["fk_Product"]);
        $('#txtFk_Box').wgReferenceField("setData", viewModel.data["Fk_Material"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#SearchDialog').empty();
    $("#txtdlgCustomerName").val("");
    $("#txtdlgCommunicationType").val("");
    $("#txtdlgEnquiryFromDate").val("");
    $("#txtdlgEnquiryToDate").val("");

}


function orderDateValidate(field, rules, i, options) {
    if (bFromSave) {
        var isDateValid = _util.isDate(field.val());

        if (isDateValid.valid == false) {

            return "* " + isDateValid.Message;
        }

        if (!_util.dateGraterThanToday(field.val())) {
            return "* Order date cannot be greater than today";
        }
    }
    bFromSave = false;
}

function deliveryDateValidate(field, rules, i, options) {

    var isDateValid = _util.isDate(field.val());

    if (isDateValid.valid == false) {
        return "* " + isDateValid.Message;
    }

    if (field.val() == undefined || $('#dtPOrderDate').val() == undefined) {
        return "* Please select the order date before adding the delivery schedule";
    }

    var orderDate = $('#dtPOrderDate').val();


    if (_util.dateGraterThan(field.val(), orderDate)) {
        return "* Delivery date cannot be earlier than order date";
    }

}
function OrderdeliveryDateValidate(field, rules, i, options) {

    var isDateValid = _util.isDate(field.val());

    if (isDateValid.valid == false) {
        return "* " + isDateValid.Message;
    }

    if (field.val() == undefined || $('#dtPOrderDate').val() == undefined) {
        return "* Please select the order date before adding the delivery schedule";
    }

    var orderDate = $('#dtPOrderDate').val();


    if (_util.dateGraterThan(field.val(), orderDate)) {
        return "* Delivery date cannot be earlier than Order Date";
    }

}
function showOnlyOpen() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#open').val()
    });
}

function showOnlyInProgress() {
    var rowCount = $('#MainSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MainSearchContainer').jtable('destroy');

    }
    //var rowCount = $('#MainSearchContainer2 tr').length;
    //if (rowCount > 0) {
    //    $('#MainSearchContainer2').jtable('destroy');

    //}

    //MainSearchContainer.style.visibility = 'hidden';
    //MainSearchContainer2.style.visibility = 'hidden';
    //MainSearchContainer1.style.visibility = 'visible';
   Schedule();
}

function showOnlyCompleted() {
    var rowCount = $('#MainSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MainSearchContainer').jtable('destroy');

    }
    //var rowCount = $('#MainSearchContainer1 tr').length;
    //if (rowCount > 0) {
    //    $('#MainSearchContainer1').jtable('destroy');
    //}
    //MainSearchContainer.style.visibility = 'hidden';
    //MainSearchContainer1.style.visibility = 'hidden';
    //MainSearchContainer2.style.visibility = 'visible';
    UnSchedule();
}


function showOnlyCancelled() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Cancelled').val()
    });
}

function showOnlyClosed() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#closed').val()
    });
}

function showAll() {
    //$('#MainSearchContainer').jtable('load', {
    //    CustomerName: $('#CustomerName').val()
    //});
    var rowCount = $('#MainSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MainSearchContainer').jtable('destroy');
        DisplayAll();
    }
}

function validatebuttonActionHook() {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("Id", $("#Pk_Order").val());

    var oResult1 = _comLayer.executeSyncAction("/Order/Load", _comLayer.parameters);
    if (oResult1.data.deliverySchedule.length == 0)
        return true;
    else
        return false;
}

function ProductChange(selectObj) {
    var selectIndex = selectObj.selectedIndex;
    var selectValue = selectObj.options[selectIndex].text;
    if (selectValue == "Select") {
        document.getElementById('ProductVal').value;
        //var PVal=  document.getElementById('PrintVal').value;


    }
    else {
        document.getElementById('ProductVal').value = selectValue;
    }

}

function UnSchedule()
{

    $('#MainSearchContainer').jtable({
        title: 'Un-Scheduled Order List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order/OrderUnScheduledListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            CustDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Customer Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Customer Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerListByFiter?Pk_Customer=' + data.record.CustID
                                        },
                                        fields: {

                                            Pk_Customer: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            CustomerName: {
                                                title: 'CustomerName',
                                                width: '15%'
                                            },
                                            CustomerAddress: {
                                                title: 'CustomerAddress'
                                            },
                                            City: {
                                                title: 'City',
                                                width: '10%'
                                            },
                                            State: {
                                                title: 'State',
                                                width: '10%'
                                            },
                                            Country: {
                                                title: 'Country',
                                                width: '10%'
                                            },
                                            PinCode: {
                                                title: 'PinCode',
                                                width: '10%'
                                            },
                                            CustomerContact: {
                                                title: 'Mobile',
                                                key: false
                                            },
                                            LandLine: {
                                                title: 'LandLIne'
                                            },
                                            Email: {
                                                title: 'Email',
                                                width: '10%'
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Contacts'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Contacts List',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                                                        },
                                                                        fields: {
                                                                            Pk_CustomerContacts: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            ContactPersonName: {
                                                                                title: 'Person Name',
                                                                                key: false
                                                                            },
                                                                            ContactPersonDesignation: {
                                                                                title: 'Designation'

                                                                            },
                                                                            ContactPersonNumber: {
                                                                                title: 'Contact Number',
                                                                                width: '2%'
                                                                            },
                                                                            ContactPersonEmailId: {
                                                                                title: 'Email',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            Pk_Order: {
                title: 'Order No',
                key: true
            },

            CustomerName: {
                title: 'CustomerName'
            },
            PONo: {
                title: 'Cust PoNo',
                edit: false
            },
            OrderDate: {
                title: 'P.Order Date'
            },
            //Fk_Enquiry: {
            //    title: 'Enquiry No.'
            //},
            //Price: {
            //    title: 'Price'
            //},
            //Status: {
            //    title: 'Status'
            //},
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        OrderID = row.record.Pk_Order;
                        _comLayer.parameters.add("OrderID", OrderID);
                        _comLayer.executeSyncAction("Order/OrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Order" + OrderID + ".pdf"

                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/Order" + OrderID + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');

                            return true;
                        }


                        /////////////////////////


                    });
                    return button;
                }
            },

            ///////////////////////////////////////////////
            Details: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-cart-3' title='Delivery Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Delivery Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Order/OrderDeliveryDetails?Pk_Order=' + data.record.Pk_Order
                                        },
                                        fields: {
                                            Ddate: {
                                                title: 'Delivery Date',
                                                key: false
                                            },
                                            BId: {
                                                title: 'BoxID'
                                            },
                                            BName: {
                                                title: 'BoxName'
                                            },
                                            PartName: {
                                                title: 'PartName'
                                            },
                                            OrdQty: {
                                                title: 'OrdQty'
                                            },
                                            EnqQty: {
                                                title: 'EnqQty',
                                                width: '20%'
                                            },

                                            //},

                                            ///
                                            BoxDet: {
                                                title: 'Box',
                                                width: '2%',
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Box'></i>");
                                                    $img.click(function () {
                                                        //var BoxID = data.record.Fk_BoxID;
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Box Details',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.BId
                                                                        },
                                                                        fields: {
                                                                            Pk_BoxSpecID: {
                                                                                title: 'Spec ID',
                                                                                key: true,
                                                                                list: false
                                                                            },
                                                                            OYes: {
                                                                                title: 'OuterShell',
                                                                                display: function (data) {
                                                                                    if (data.record.OYes == true || data.record.OYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },

                                                                            },
                                                                            CYes: {
                                                                                title: 'Cap',
                                                                                display: function (data) {
                                                                                    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            LPYes: {
                                                                                title: 'L.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                                    //else {
                                                                                    //    width: '2%';
                                                                                    //}
                                                                                },
                                                                            },
                                                                            WPYes: {
                                                                                title: 'W.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            PYes: {
                                                                                title: 'Plate',
                                                                                display: function (data) {
                                                                                    if (data.record.PYes == true || data.record.PYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            Details: {
                                                                                title: '',
                                                                                width: '1%',
                                                                                sorting: false,
                                                                                paging: true,
                                                                                pageSize: 5,
                                                                                edit: false,
                                                                                create: false,
                                                                                listClass: 'child-opener-image-column',
                                                                                display: function (data) {
                                                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                                                    $img.click(function () {
                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                    $img.closest('tr'),
                                                                                                    {
                                                                                                        title: 'Specifications',
                                                                                                        paging: true,
                                                                                                        pageSize: 5,
                                                                                                        actions: {
                                                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                                                        },
                                                                                                        fields: {
                                                                                                            Pk_PartPropertyID: {
                                                                                                                title: 'Property ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                width: '2%',
                                                                                                                list: false
                                                                                                            },
                                                                                                            Length: {
                                                                                                                title: 'Length',
                                                                                                                key: false,
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Width: {
                                                                                                                title: 'Width',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Height: {
                                                                                                                title: 'Height',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Weight: {
                                                                                                                title: 'Weight',
                                                                                                                width: '2%'
                                                                                                            },

                                                                                                            Details: {
                                                                                                                title: '',
                                                                                                                width: '1%',
                                                                                                                sorting: false,
                                                                                                                paging: true,
                                                                                                                pageSize: 5,
                                                                                                                edit: false,
                                                                                                                create: false,
                                                                                                                listClass: 'child-opener-image-column',
                                                                                                                display: function (data) {
                                                                                                                    var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                                                    $img.click(function () {
                                                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                                                    $img.closest('tr'),
                                                                                                                                    {
                                                                                                                                        title: 'Layers Details',
                                                                                                                                        paging: true,
                                                                                                                                        pageSize: 10,
                                                                                                                                        actions: {
                                                                                                                                            listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                                                        },
                                                                                                                                        fields: {
                                                                                                                                            Pk_LayerID: {
                                                                                                                                                title: 'Layer ID',
                                                                                                                                                key: true,
                                                                                                                                                create: false,
                                                                                                                                                edit: false,
                                                                                                                                                list: false
                                                                                                                                            },
                                                                                                                                            GSM: {
                                                                                                                                                title: 'GSM',
                                                                                                                                                key: false
                                                                                                                                            },
                                                                                                                                            BF: {
                                                                                                                                                title: 'BF'

                                                                                                                                            },
                                                                                                                                            Weight: {
                                                                                                                                                title: 'Weight'
                                                                                                                                            }
                                                                                                                                        },
                                                                                                                                        formClosed: function (event, data) {
                                                                                                                                            data.form.validationEngine('hide');
                                                                                                                                            data.form.validationEngine('detach');
                                                                                                                                        }
                                                                                                                                    }, function (data) { //opened handler
                                                                                                                                        data.childTable.jtable('load');
                                                                                                                                    });
                                                                                                                    });
                                                                                                                    return $img;
                                                                                                                }
                                                                                                            },
                                                                                                        },
                                                                                                        formClosed: function (event, data) {
                                                                                                            data.form.validationEngine('hide');
                                                                                                            data.form.validationEngine('detach');
                                                                                                        }
                                                                                                    }, function (data) { //opened handler
                                                                                                        data.childTable.jtable('load');
                                                                                                    });
                                                                                    });
                                                                                    return $img;
                                                                                }
                                                                            },
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        ////
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },

        }
    });


    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            CustomerName: $('#TxtCustomerName').val(),
            Pk_Order: $('#TxtOrderNo').val(),
            FromOrderDate: $('#TxtFromDate').val(),
            ToOrderDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();
}

function Schedule() {

    $('#MainSearchContainer').jtable({
        title: 'Scheduled Order List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order/OrderScheduledListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            CustDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Customer Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Customer Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerListByFiter?Pk_Customer=' + data.record.CustID
                                        },
                                        fields: {

                                            Pk_Customer: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            CustomerName: {
                                                title: 'CustomerName',
                                                width: '15%'
                                            },
                                            CustomerAddress: {
                                                title: 'CustomerAddress'
                                            },
                                            City: {
                                                title: 'City',
                                                width: '10%'
                                            },
                                            State: {
                                                title: 'State',
                                                width: '10%'
                                            },
                                            Country: {
                                                title: 'Country',
                                                width: '10%'
                                            },
                                            PinCode: {
                                                title: 'PinCode',
                                                width: '10%'
                                            },
                                            CustomerContact: {
                                                title: 'Mobile',
                                                key: false
                                            },
                                            LandLine: {
                                                title: 'LandLIne'
                                            },
                                            Email: {
                                                title: 'Email',
                                                width: '10%'
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Contacts'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Contacts List',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                                                        },
                                                                        fields: {
                                                                            Pk_CustomerContacts: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            ContactPersonName: {
                                                                                title: 'Person Name',
                                                                                key: false
                                                                            },
                                                                            ContactPersonDesignation: {
                                                                                title: 'Designation'

                                                                            },
                                                                            ContactPersonNumber: {
                                                                                title: 'Contact Number',
                                                                                width: '2%'
                                                                            },
                                                                            ContactPersonEmailId: {
                                                                                title: 'Email',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            Pk_Order: {
                title: 'Order No',
                key: true
            },

            CustomerName: {
                title: 'CustomerName'
            },
            PONo: {
                title: 'Cust PoNo',
                edit: false
            },
            OrderDate: {
                title: 'P.Order Date'
            },
            //Fk_Enquiry: {
            //    title: 'Enquiry No.'
            //},
            //Price: {
            //    title: 'Price'
            //},
            //Status: {
            //    title: 'Status'
            //},
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        OrderID = row.record.Pk_Order;
                        _comLayer.parameters.add("OrderID", OrderID);
                        _comLayer.executeSyncAction("Order/OrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Order" + OrderID + ".pdf"

                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/Order" + OrderID + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');

                            return true;
                        }


                        /////////////////////////


                    });
                    return button;
                }
            },

            ///////////////////////////////////////////////
            Details: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-cart-3' title='Delivery Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Delivery Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Order/OrderDeliveryDetails?Pk_Order=' + data.record.Pk_Order
                                        },
                                        fields: {
                                            Ddate: {
                                                title: 'Delivery Date',
                                                key: false
                                            },
                                            BId: {
                                                title: 'BoxID'
                                            },
                                            BName: {
                                                title: 'BoxName'
                                            },
                                            PartName: {
                                                title: 'PartName'
                                            },
                                            OrdQty: {
                                                title: 'OrdQty'
                                            },
                                            EnqQty: {
                                                title: 'EnqQty',
                                                width: '20%'
                                            },

                                            //},

                                            ///
                                            BoxDet: {
                                                title: 'Box',
                                                width: '2%',
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Box'></i>");
                                                    $img.click(function () {
                                                        //var BoxID = data.record.Fk_BoxID;
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Box Details',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.BId
                                                                        },
                                                                        fields: {
                                                                            Pk_BoxSpecID: {
                                                                                title: 'Spec ID',
                                                                                key: true,
                                                                                list: false
                                                                            },
                                                                            OYes: {
                                                                                title: 'OuterShell',
                                                                                display: function (data) {
                                                                                    if (data.record.OYes == true || data.record.OYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },

                                                                            },
                                                                            CYes: {
                                                                                title: 'Cap',
                                                                                display: function (data) {
                                                                                    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            LPYes: {
                                                                                title: 'L.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                                    //else {
                                                                                    //    width: '2%';
                                                                                    //}
                                                                                },
                                                                            },
                                                                            WPYes: {
                                                                                title: 'W.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            PYes: {
                                                                                title: 'Plate',
                                                                                display: function (data) {
                                                                                    if (data.record.PYes == true || data.record.PYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            Details: {
                                                                                title: '',
                                                                                width: '1%',
                                                                                sorting: false,
                                                                                paging: true,
                                                                                pageSize: 5,
                                                                                edit: false,
                                                                                create: false,
                                                                                listClass: 'child-opener-image-column',
                                                                                display: function (data) {
                                                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                                                    $img.click(function () {
                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                    $img.closest('tr'),
                                                                                                    {
                                                                                                        title: 'Specifications',
                                                                                                        paging: true,
                                                                                                        pageSize: 5,
                                                                                                        actions: {
                                                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                                                        },
                                                                                                        fields: {
                                                                                                            Pk_PartPropertyID: {
                                                                                                                title: 'Property ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                width: '2%',
                                                                                                                list: false
                                                                                                            },
                                                                                                            Length: {
                                                                                                                title: 'Length',
                                                                                                                key: false,
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Width: {
                                                                                                                title: 'Width',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Height: {
                                                                                                                title: 'Height',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Weight: {
                                                                                                                title: 'Weight',
                                                                                                                width: '2%'
                                                                                                            },

                                                                                                            Details: {
                                                                                                                title: '',
                                                                                                                width: '1%',
                                                                                                                sorting: false,
                                                                                                                paging: true,
                                                                                                                pageSize: 5,
                                                                                                                edit: false,
                                                                                                                create: false,
                                                                                                                listClass: 'child-opener-image-column',
                                                                                                                display: function (data) {
                                                                                                                    var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                                                    $img.click(function () {
                                                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                                                    $img.closest('tr'),
                                                                                                                                    {
                                                                                                                                        title: 'Layers Details',
                                                                                                                                        paging: true,
                                                                                                                                        pageSize: 10,
                                                                                                                                        actions: {
                                                                                                                                            listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                                                        },
                                                                                                                                        fields: {
                                                                                                                                            Pk_LayerID: {
                                                                                                                                                title: 'Layer ID',
                                                                                                                                                key: true,
                                                                                                                                                create: false,
                                                                                                                                                edit: false,
                                                                                                                                                list: false
                                                                                                                                            },
                                                                                                                                            GSM: {
                                                                                                                                                title: 'GSM',
                                                                                                                                                key: false
                                                                                                                                            },
                                                                                                                                            BF: {
                                                                                                                                                title: 'BF'

                                                                                                                                            },
                                                                                                                                            Weight: {
                                                                                                                                                title: 'Weight'
                                                                                                                                            }
                                                                                                                                        },
                                                                                                                                        formClosed: function (event, data) {
                                                                                                                                            data.form.validationEngine('hide');
                                                                                                                                            data.form.validationEngine('detach');
                                                                                                                                        }
                                                                                                                                    }, function (data) { //opened handler
                                                                                                                                        data.childTable.jtable('load');
                                                                                                                                    });
                                                                                                                    });
                                                                                                                    return $img;
                                                                                                                }
                                                                                                            },
                                                                                                        },
                                                                                                        formClosed: function (event, data) {
                                                                                                            data.form.validationEngine('hide');
                                                                                                            data.form.validationEngine('detach');
                                                                                                        }
                                                                                                    }, function (data) { //opened handler
                                                                                                        data.childTable.jtable('load');
                                                                                                    });
                                                                                    });
                                                                                    return $img;
                                                                                }
                                                                            },
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        ////
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },

        }
    });


    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            CustomerName: $('#TxtCustomerName').val(),
            Pk_Order: $('#TxtOrderNo').val(),
            FromOrderDate: $('#TxtFromDate').val(),
            ToOrderDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();
}

function DisplayAll()
{
    $('#MainSearchContainer').jtable({
        title: 'Order List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order/OrderListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            CustDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Customer Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Customer Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerListByFiter?Pk_Customer=' + data.record.CustID
                                        },
                                        fields: {

                                            Pk_Customer: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            CustomerName: {
                                                title: 'CustomerName',
                                                width: '15%'
                                            },
                                            CustomerAddress: {
                                                title: 'CustomerAddress'
                                            },
                                            City: {
                                                title: 'City',
                                                width: '10%'
                                            },
                                            State: {
                                                title: 'State',
                                                width: '10%'
                                            },
                                            Country: {
                                                title: 'Country',
                                                width: '10%'
                                            },
                                            PinCode: {
                                                title: 'PinCode',
                                                width: '10%'
                                            },
                                            CustomerContact: {
                                                title: 'Mobile',
                                                key: false
                                            },
                                            LandLine: {
                                                title: 'LandLIne'
                                            },
                                            Email: {
                                                title: 'Email',
                                                width: '10%'
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Contacts'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Contacts List',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                                                        },
                                                                        fields: {
                                                                            Pk_CustomerContacts: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            ContactPersonName: {
                                                                                title: 'Person Name',
                                                                                key: false
                                                                            },
                                                                            ContactPersonDesignation: {
                                                                                title: 'Designation'

                                                                            },
                                                                            ContactPersonNumber: {
                                                                                title: 'Contact Number',
                                                                                width: '2%'
                                                                            },
                                                                            ContactPersonEmailId: {
                                                                                title: 'Email',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            Pk_Order: {
                title: 'Order No',
                key: true
            },

            CustomerName: {
                title: 'CustomerName'
            },
            PONo: {
                title: 'Cust PoNo',
                edit: false
            },
            OrderDate: {
                title: 'P.Order Date'
            },
            Fk_Enquiry: {
                title: 'Enquiry No.'
            },
            //Price: {
            //    title: 'Price'
            //},
            //Status: {
            //    title: 'Status'
            //},
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        OrderID = row.record.Pk_Order;
                        _comLayer.parameters.add("OrderID", OrderID);
                        _comLayer.executeSyncAction("Order/OrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Order" + OrderID + ".pdf"

                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/Order" + OrderID + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');

                            return true;
                        }


                        /////////////////////////


                    });
                    return button;
                }
            },

            ///////////////////////////////////////////////
            Details: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-cart-3' title='Delivery Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Delivery Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Order/OrderDeliveryDetails?Pk_Order=' + data.record.Pk_Order
                                        },
                                        fields: {
                                            Ddate: {
                                                title: 'Delivery Date',
                                                key: false
                                            },
                                            BId: {
                                                title: 'BoxID'
                                            },
                                            BName: {
                                                title: 'BoxName'
                                            },
                                            PartName: {
                                                title: 'PartName'
                                            },
                                            OrdQty: {
                                                title: 'OrdQty'
                                            },
                                            EnqQty: {
                                                title: 'EnqQty',
                                                width: '20%'
                                            },

                                            //},

                                            ///
                                            BoxDet: {
                                                title: 'Box',
                                                width: '2%',
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Box'></i>");
                                                    $img.click(function () {
                                                        //var BoxID = data.record.Fk_BoxID;
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Box Details',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.BId
                                                                        },
                                                                        fields: {
                                                                            Pk_BoxSpecID: {
                                                                                title: 'Spec ID',
                                                                                key: true,
                                                                                list: false
                                                                            },
                                                                            OYes: {
                                                                                title: 'OuterShell',
                                                                                display: function (data) {
                                                                                    if (data.record.OYes == true || data.record.OYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },

                                                                            },
                                                                            CYes: {
                                                                                title: 'Cap',
                                                                                display: function (data) {
                                                                                    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            LPYes: {
                                                                                title: 'L.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                                    //else {
                                                                                    //    width: '2%';
                                                                                    //}
                                                                                },
                                                                            },
                                                                            WPYes: {
                                                                                title: 'W.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            PYes: {
                                                                                title: 'Plate',
                                                                                display: function (data) {
                                                                                    if (data.record.PYes == true || data.record.PYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            Details: {
                                                                                title: '',
                                                                                width: '1%',
                                                                                sorting: false,
                                                                                paging: true,
                                                                                pageSize: 5,
                                                                                edit: false,
                                                                                create: false,
                                                                                listClass: 'child-opener-image-column',
                                                                                display: function (data) {
                                                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                                                    $img.click(function () {
                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                    $img.closest('tr'),
                                                                                                    {
                                                                                                        title: 'Specifications',
                                                                                                        paging: true,
                                                                                                        pageSize: 5,
                                                                                                        actions: {
                                                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                                                        },
                                                                                                        fields: {
                                                                                                            Pk_PartPropertyID: {
                                                                                                                title: 'Property ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                width: '2%',
                                                                                                                list: false
                                                                                                            },
                                                                                                            Length: {
                                                                                                                title: 'Length',
                                                                                                                key: false,
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Width: {
                                                                                                                title: 'Width',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Height: {
                                                                                                                title: 'Height',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Weight: {
                                                                                                                title: 'Weight',
                                                                                                                width: '2%'
                                                                                                            },

                                                                                                            Details: {
                                                                                                                title: '',
                                                                                                                width: '1%',
                                                                                                                sorting: false,
                                                                                                                paging: true,
                                                                                                                pageSize: 5,
                                                                                                                edit: false,
                                                                                                                create: false,
                                                                                                                listClass: 'child-opener-image-column',
                                                                                                                display: function (data) {
                                                                                                                    var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                                                    $img.click(function () {
                                                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                                                    $img.closest('tr'),
                                                                                                                                    {
                                                                                                                                        title: 'Layers Details',
                                                                                                                                        paging: true,
                                                                                                                                        pageSize: 10,
                                                                                                                                        actions: {
                                                                                                                                            listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                                                        },
                                                                                                                                        fields: {
                                                                                                                                            Pk_LayerID: {
                                                                                                                                                title: 'Layer ID',
                                                                                                                                                key: true,
                                                                                                                                                create: false,
                                                                                                                                                edit: false,
                                                                                                                                                list: false
                                                                                                                                            },
                                                                                                                                            GSM: {
                                                                                                                                                title: 'GSM',
                                                                                                                                                key: false
                                                                                                                                            },
                                                                                                                                            BF: {
                                                                                                                                                title: 'BF'

                                                                                                                                            },
                                                                                                                                            Weight: {
                                                                                                                                                title: 'Weight'
                                                                                                                                            }
                                                                                                                                        },
                                                                                                                                        formClosed: function (event, data) {
                                                                                                                                            data.form.validationEngine('hide');
                                                                                                                                            data.form.validationEngine('detach');
                                                                                                                                        }
                                                                                                                                    }, function (data) { //opened handler
                                                                                                                                        data.childTable.jtable('load');
                                                                                                                                    });
                                                                                                                    });
                                                                                                                    return $img;
                                                                                                                }
                                                                                                            },
                                                                                                        },
                                                                                                        formClosed: function (event, data) {
                                                                                                            data.form.validationEngine('hide');
                                                                                                            data.form.validationEngine('detach');
                                                                                                        }
                                                                                                    }, function (data) { //opened handler
                                                                                                        data.childTable.jtable('load');
                                                                                                    });
                                                                                    });
                                                                                    return $img;
                                                                                }
                                                                            },
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        ////
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },

        }
    });


    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            CustomerName: $('#TxtCustomerName').val(),
            Pk_Order: $('#TxtOrderNo').val(),
            FromOrderDate: $('#TxtFromDate').val(),
            ToOrderDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();
}


function showOnlyPaper() {

    document.getElementById("Others").checked = false;
    document.getElementById("Paper").checked = true;
    document.getElementById("Selected").value = "Paper";
    setUpEnquiryBoxSearch();

}
function showOthers() {

    document.getElementById("Others").checked = true;
    document.getElementById("Paper").checked = false;
    document.getElementById("Selected").value = "Others";
    // checkedVal = "Others";

    setUpEnquiryBoxSearch();
}

