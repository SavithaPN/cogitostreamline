﻿
function IssuedMaterial() {
    this.data = new Object();
    this.data["Name"] = "";
    this.data["Fk_Material"] = "";
    this.data["Fk_PaperStock"] = "";
    this.data["RollNo"] = "";
    this.data["Quantity"] = "";

    this.updateValues = function () {
        this.data["Name"] = $('#txtFk_Material').val();
        //this.data["Fk_Material"] = Fk_Material;
        this.data["Fk_PaperStock"] = $('#Pk_PaperStock').val();;
        this.data["RollNo"] = $('#RollNo').val();
        this.data["Quantity"] = $('#Quantity').val();
       
    };
    this.load = function (Pk_JobCardPartsDet, Name, Fk_Material, Pk_PaperStock, RollNo, Quantity) {
        this.data["Pk_JobCardPartsDet"] = Pk_JobCardPartsDet;
        this.data["Name"] = Name;
        this.data["Fk_Material"] = Fk_Material;
        this.data["Fk_PaperStock"] = Pk_PaperStock;
        this.data["RollNo"] = RollNo;
        this.data["Quantity"] = Quantity;

    };
}
