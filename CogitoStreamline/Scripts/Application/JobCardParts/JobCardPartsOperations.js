﻿

var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var CustCode = "";

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Job Card-Parts List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCardParts/JCPartsListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_JobCardPartsID: {
                title: 'J C-Parts ID',
                key: true,
                width: '2%'
            },
            JDate: {
                title: 'J.Card Date',
                width: '3%'
            },
            Fk_Order: {
                title: 'Cust Order No',
                width: '5%'
            },
            Fk_Status: {
                title: 'JobCard Status',
                width: '5%'
            },
            Print: {
        title: 'Print',
        width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
        display: function (row) {
            var button = $("<i class='icon-printer'></i>");
            $(button).click(function () {

                JCNO = row.record.Pk_JobCardPartsID;
                _comLayer.parameters.add("JCNO", JCNO);
                _comLayer.executeSyncAction("JobCardParts/JCRep", _comLayer.parameters);
                var strval = "ConvertPDF/JCPartsRep" + JCNO + ".pdf"

                window.open(strval, '_blank ', 'width=700,height=250');
                //return true;
                ///////////////////////////
                      

            });
            return button;
        }

    },
        }
    });

    $('#dtJCDate').datepicker({
        autoclose: true
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_JobCardID: $('#txtPKJob').val(),
            Fk_Order: $('#txtOrder').val(),
            FromJDate: $('#TxtFromDate').val(),
            ToJDate: $('#TxtToDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });


}

function afterNewShow(viewObject) {
    curViewModel = viewObject.viewModel;

    bEditContext = false;
    // $('#cmdCreate').attr('disabled', true);
    //$('#dtPOrderDate').datepicker({  autoclose: true});

    var dNow = new Date();
    document.getElementById('dtJCDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    $('#dtJCDate').datepicker({ autoclose: true });
   
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Material Id',
                key: true,
                list: false
            },
            Pk_PaperStock: {
                title: 'Pk PaperStock',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'
            },
            RollNo: {
                title: 'Roll No'
            },
            Quantity: {
                title: "Quantity"
            }

        }
    });


    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JCPartsDetails", "JobCard", "_AddMaterial", function () { return new IssuedMaterial() });


$('#cmdShowBoxDetails').click(function (e) {
   
    });
    $('#cmdOrderMSearch').click(function (e) {
        e.preventDefault();
        $("#searchBoxDialog").height(700);
        $("#searchBoxDialog").width(1000);
        setUpOrderSearch(viewObject);
        $("#searchBoxDialog").modal("show");
    });
    $('#cmdPartsSearch').click(function (e) {
        e.preventDefault();
        setUpPartsSearch(viewObject);
        $("#searchBoxDialog1").height(700);
        $("#searchBoxDialog1").modal("show");
        //$("#searchBoxDialog1").height(1000);
    });
    


    $('#cmdCreate').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
        //$(this).tab('show')
        //alert($('.nav-tabs .active').text());
        if ($('.nav-tabs .active').text() == "Details Page 2") {
            //$('input:submit').show();
            $('#cmdCreate').hide();
        }
        else if 
            ($('.nav-tabs .active').text() == "Details Page 1") {
            $('#cmdCreate').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
    })
}



function setUpPartsSearch(viewObject) {

    $('#MainSearhContainer2').jtable({
        title: 'Parts List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/JobCardParts/MaterialPartsList'
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            PartNo: {
                title: 'PartNo',
                width: '25%'
            },
            //Quantity: {
            //    title: 'Quantity',
            //    width: '25%'
            //},
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load');
    });
    $('#LoadRecordsButton').click();

    $('#cmdPartsSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load');
    });

    $('#cmdPartsDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MainSearhContainer2').jtable('selectedRows');
        //$('#Fk_PartID').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('Fk_PartID').value = rows[0].data..;
        //$("#BoxID").val(rows[0].data.Fk_BoxID);
        //$("#Quantity").val(rows[0].data.Quantity);
        $("#searchBoxDialog1").modal("hide");

        //////////////////////////////

        //var BoxID = document.getElementById('BoxID');


        //$('#divPaperStock').jtable({
        //    title: 'Job Card List',
        //    paging: true,
        //    pageSize: 15,
        //    sorting: false,
        //    defaultSorting: 'Name ASC',
        //    actions: {

        //        listAction: '/JobCard/PaperStockList?Fk_BoxID=' + BoxID,
        //        updateAction: ''
        //    },
        //    fields: {
        //        slno: {
        //            title: 'slno',
        //            key: true,
        //            list: false
        //        },
        //        Pk_Material: {
        //            title: 'Pk_Material',
        //            key: false
        //        },
        //        Name: {
        //            title: 'Name'

        //        },
        //        Quantity: {
        //            title: 'Quantity',
        //            key: false
        //        }
        //    }
        //});



        ///////////////////////
        //$('#LoadRecordsButton').click(function (e) {
        //    e.preventDefault();

        //        $('#divPaperStock').jtable('load', {

        //        Fk_BoxID: BoxID

        //    });

        //    //Load all records when page is first shown
        //    $('#LoadRecordsButton').click();

        //});

        //loadPaperStock();







    });

}


function setUpIssueMaterialSearch() {
    //Adding Search fields
    //var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";

    //$("#dlgMaterialSearchFields").append(txtFieldMaterialName);
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            Pk_PaperStock: {
                title: 'PKStock',
                key: false,
                width: '2%',
                list: false
            },
            RollNo: {
                title: 'Reel No',
                edit: false,
                width: '2%'
            },
            Name: {
                title: 'Material Name',
                edit: false
            },
            Quantity: {
                title: 'Ex.Stk.Qty',
                edit: false,
                width: '2%'
            },
            Category: {
                title: 'Cat',
                edit: false,
                list: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();

        if (document.getElementById('SType').value == '')
        { alert("Select any one of the Material Type and Press Search"); }
        else
        {
            $('#MaterialSearchContainer').jtable('load', {
                Name: $('#txtMaterial').val(),
                MaterialCategory: $('#SType').val()

            });
        }
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('txtMaterial').value = rows[0].data.Pk_Material;
        if (rows[0].data["Category"] != "Paper") {
            document.getElementById('RollNo').value = "";
        }
        else {
            document.getElementById('RollNo').value = rows[0].data["RollNo"];
        }
        document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('Pk_PaperStock').value = rows[0].data["Pk_PaperStock"];
        document.getElementById('Pk_Material').value = rows[0].data["Pk_Material"];

        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Quantity").focus();
    });



}

function afterOneToManyDialogShow() {
    $('#dtJCDate').datepicker({
        autoclose: true
    });

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();

        $("#dataDialog").width(900);
        //$("#dataDialog").css("left", "60px");
        $("#divSearchMaterial").width(800);

        setUpIssueMaterialSearch();

        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;

    // Based on State Can Disable the Save Button
    var status = viewObject.viewModel.data["State"]();

    if (status == "Completed") {

        $(".modelControl").attr('disabled', 'disabled');
        $("#cmdAddSchedule").attr('disabled', true);
        $("#cmdCreate").attr('disabled', true);
    }

    var JCstatus = viewObject.viewModel.data["Fk_Status"]();

    if (JCstatus == "4") {
        $("#chkCloseJC").attr('checked', true);
    }

    $('#cmdAddressSearch').click(function (e) {
        e.preventDefault();
        CustCode = viewObject.viewModel.data["fk_Customer"]();
        setUpAddressSearch(viewObject);
        $("#searchAddressDialog").modal("show");
    });

    $("#cmdDeliverySchedule").click(function (e) {
        e.preventDefault();
        var OrderNumber = $("#hidPkOrder").val();
        window.open("/DeliverySchedule?Order=" + OrderNumber, "Schedule");
    });

    $('#dtJCDate').datepicker({
        autoclose: true
    });

    $('#cmdOrderMSearch').click(function (e) {
        e.preventDefault();
        setUpOrderSearch(viewObject);
        $("#searchBoxDialog").modal("show");
    });
    $('#cmdSave').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
        //$(this).tab('show')
        //alert($('.nav-tabs .active').text());
        if ($('.nav-tabs .active').text() == "Details Page 2") {
            //$('input:submit').show();
            $('#cmdSave').hide();
        }
        else if
            ($('.nav-tabs .active').text() == "Details Page 1") {
            $('#cmdSave').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
    })
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/JobCard/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Material Id',
                key: true,
                list: false
            },
            Pk_PaperStock: {
                title: 'Pk PaperStock',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'
            },
            RollNo: {
                title: 'Roll No'
            },
            Quantity: {
                title: "Quantity"
            }

        }
    });

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["JCPartsDetails"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_JobCardPartsDet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Pk_PaperStock, oSCuts[i].RollNo, oSCuts[i].Quantity);
        viewObject.viewModel.data["JCPartsDetails"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "JCPartsDetails", "JobCard", "_AddMaterial", function () { return new IssuedMaterial() });


    bEditContext = true;

    $('#wfTransDisplay').wgWorkFlowHistory({
        wgTag: "JobCard",
        viewModel: viewObject.viewModel
    });

    $('#wfButtons').wgWorkFlowButtons({
        wgTag: "Order",
        viewModel: viewObject.viewModel,
        stateField: "Fk_Status",
        belongsTo: "JobCard",
        pkField: "Pk_JobCardID"
    });

}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {
    $('#Fk_Order').wgReferenceField({
        keyProperty: "Fk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewObject.viewModel
    });

}

function afterModelSaveEx(result) {
    alertify.set({
        labels: {
            ok: "Yes",
            cancel: "No"
        },
        buttonReverse: true
    });


    //var bValidation = true;
    //if ($("#Fk_ShippingId").val() < 1) {
    //    $("#Fk_ShippingId").validationEngine('showPrompt', 'Select Shipping Address', 'error', true)
    //    bValidation = false;
    //}
    //return bValidation;
}



function checkValues() {
    _comLayer.parameters.clear();
    var BoxID = document.getElementById('BoxID').value;
    var PartID = document.getElementById('Fk_PartID').value;
    if (BoxID != 0 || BoxID > 0) {
        _comLayer.parameters.add("Pk_BoxID", BoxID);
    }
    else {

        _comLayer.parameters.add("Pk_BoxID", BoxValue);
    }
    if (PartID != 0 || PartID > 0) {
        _comLayer.parameters.add("PartID", PartID);
    }

    var oResult1 = _comLayer.executeSyncAction("BoxMaster/SBoxDet", _comLayer.parameters);
    //alert(oResult1.data[0].Length);
    //if (oResult1.) {
    document.getElementById('Length').value = oResult1.data[0].Length;
    document.getElementById('Width').value = oResult1.data[0].Width;
    document.getElementById('Height').value = oResult1.data[0].Height;

    document.getElementById('Deckle').value = oResult1.data[0].Deck;
    document.getElementById('BoardSize').value = oResult1.data[0].BoardArea;
    document.getElementById('CuttingSize').value = oResult1.data[0].CuttingSize;
    document.getElementById('Ply').value = oResult1.data[0].Ply;

    document.getElementById('MDeckel').value = "132";

    var DeckVal = oResult1.data[0].Deck;
    var CalDeck = Math.floor(132 / Number(DeckVal));
    if (CalDeck != 0)

    { document.getElementById('Ups').value = CalDeck };
}







function setUpOrderSearch(viewObject) {
    //Enquiry

    // cleanSearchDialog();

    //Adding Search fields
    var txtFieldOrdNo = "<input type='text' id='txtdlgOrdNo' placeholder='OrderNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldEnqNo = "<input type='text' id='txtdlgEnq' placeholder='EnqNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldPoNo = "<input type='text' id='txtdlgPoNo' placeholder='PONo' class='input-large search-query'/>";
 //   var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";


    $('#searchBoxDialog').jtable({
        title: 'Orders List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/DelSchedule/PartsScheduleList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdOrderDone').click();
            });
        },
        fields: {
            OrderNo: {
                title: 'Order No.',
                key: true
            },

            CustPO: {
                title: 'Cust_PONo.'
            },
            EnquiryNo: {
                title: 'Enquiry No',
                edit: false
            },
            CustomerName: {
                title: 'CustomerName'
            },
            BoxName: {
                title: 'BoxName'
            },
            PartID: {
                title: 'Part ID',
                list:false
            },
            PName: {
                title: 'Part Name'
            },
            OrdQty: {
                title: 'OrdQty.'
            },
            Pk_DelID: {
                title: 'Prod.Sch.No'
            },
            DeliveryDate: {
                title: 'Del.Date'
            },

            Quantity: {
                title: 'Prod.Sch.Qty'
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Pk_Order: $('#txtdlgOrdNo').val(),
            Fk_Enquiry: $('#txtdlgEnq').val(),
            CustomerName: $('#txtdlgCustomerName').val(),
            PONo: $('#txtdlgPoNo').val(),
            BoxName: $('#txtdlgBoxName').val()
        });

    });
    //$('#txtdlgEnquiryDate').datepicker({
    //    autoclose: true
    //});
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdOrderSearch').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Pk_Order: $('#txtdlgOrdNo').val(),
            Fk_Enquiry: $('#txtdlgEnq').val(),
            CustomerName: $('#txtdlgCustomerName').val(),
            PONo: $('#txtdlgPoNo').val(),
            BoxName: $('#txtdlgBoxName').val()

        });
    });


    $('#cmdOrderDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchBoxDialog').jtable('selectedRows');


        //document.getElementById('Fk_BoxID').value = rows[0].keyValue;
        // document.getElementById('TxtIndent').value = rows[0].data.IndentNo;
        //document.getElementById('TxtMaterial').value = rows[0].data.Fk_Material;
        //document.getElementById('TxtMaterialId').value = rows[0].data.Fk_Material1;

        // $('#Fk_Order').wgReferenceField("setData", rows[0].data.Pk_Order);
        curViewModel.data["Fk_Order"] = ko.observable(rows[0].keyValue);
        $("#Fk_Order").val(rows[0].keyValue);
        $('#Fk_Enquiry').val(rows[0].data.EnquiryNo);
        //$('#txtfk_Customer').wgReferenceField("setData", rows[0].data.CustomerName);
        $("#Fk_PartID").val(rows[0].data.PartID);
        $("#txtPart").val(rows[0].data.PName);
        $("#txtfk_Customer").val(rows[0].data.CustomerName);
        $("#Fk_Schedule").val(rows[0].data.Pk_DelID);
        $("#PO").val(rows[0].data.CustPO);
        $("#POQuantity").val(rows[0].data.OrdQty);
        $("#BoxID").val(rows[0].data.Pk_BoxID);
        $("#Fk_BoxID").val(rows[0].data.BoxName);
        $("#Fk_Enquiry").val(rows[0].data.EnquiryNo);
        $("#TQty").val(rows[0].data.Quantity);
        var QtyVal = document.getElementById('TQty').value;
        $("#searchBoxDialog").modal("hide");

        checkValues();


        $('#divPaperStock').jtable({
            title: 'Box Paper List',
            paging: true,
            pageSize: 15,
            sorting: false,
            defaultSorting: 'Name ASC',
            actions: {

                listAction: '/Order/OrderPaperDetails?Fk_BoxID=',
                //updateAction: ''
            },
            fields: {
                slno: {
                    title: 'slno',
                    key: true,
                    list: false
                },
                Pk_Material: {
                    title: 'Pk_Material',
                    key: false,
                    list: false
                },

                MName: {
                    title: 'Mat.Name'

                },
                PName: {
                    title: 'Part Name'

                },
                PQty: {
                    title: 'Part Qty. in Box '

                },
                StkQty: {
                    title: 'Ex.Stock',
                    key: false
                },
                PaperWt: {
                    title: 'Paper Wt./Box',
                    key: false,
                    list: false
                },

                PWt: {
                    title: 'Paper Wt./Box',
                    key: false,
                    list: true
                },
                PaperReq: {
                    title: 'Paper Req.',
                    display: function (data) {
                        if (data.record.PaperWt > 0) {
                            return (data.record.PaperWt * QtyVal * data.record.PQty);
                        }
                    }
                }

            }
        });

        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();

            $('#divPaperStock').jtable('load', {
                Fk_BoxID: $('#BoxID').val()
                // Fk_BoxID: BoxID

            });
        });
        //Load all records when page is first shown
        $('#LoadRecordsButton').click();

    });

}


function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["OrderDate"] = $('#dtPOrderDate').val();
    viewModel.data["Fk_Schedule"] = $('#Fk_Schedule').val();
    viewModel.data["InvDate"] = $('#dtInvDate').val();
    viewModel.data["Fk_PartID"] = $('#Fk_PartID').val();
    viewModel.data["Fk_BoxID"] = $('#BoxID').val();
    viewModel.data["DPRSc"] = $('#DPRScVal').val();
    viewModel.data["Printing"] = $('#PrintVal').val();
    viewModel.data["TotalQty"] = $('#TQty').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["OrderDate"] = $('#dtPOrderDate').val();
    viewModel1.data["fk_Customer"] = CustCode;
    viewModel1.data["Fk_PartID"] = $('#txtPartID').val();
    viewModel1.data["Fk_ShippingId"] = $('#Fk_ShippingId').val();
    viewModel1.data["RM_Consumed"] = $('#Quantity').val();
    //viewModel1.data["Fk_Status"] = $('#cjc').val();
    viewModel1.data["Fk_Schedule"] = $('#Fk_Schedule').val();
    viewModel1.data["ChkQuality"] = $('#cqty').val();
    viewModel1.data["Fk_BoxID"] = $('#BoxID').val();
    viewModel1.data["TotalQty"] = $('#TQty').val();
    viewModel1.data["InvDate"] = $('#dtInvDate').val();

}

function ReferenceFieldNotInitilized(viewModel) {


    $('#txtfk_Customer').wgReferenceField({
        keyProperty: "fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });


    $('#Fk_BoxID').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#Fk_BoxID').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);

    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();
    
    $('#MainSearhContainer2').empty();
    //$('#searchBoxDialog').empty();
    $('#MaterialSearchContainer').empty();
}

function orderDateValidate(field, rules, i, options) {
    if (bFromSave) {
        var isDateValid = _util.isDate(field.val());

        if (isDateValid.valid == false) {

            return "* " + isDateValid.Message;
        }

        if (!_util.dateGraterThanToday(field.val())) {
            return "* Order date cannot be greater than today";
        }
    }
    bFromSave = false;
}

function deliveryDateValidate(field, rules, i, options) {

    var isDateValid = _util.isDate(field.val());

    if (isDateValid.valid == false) {
        return "* " + isDateValid.Message;
    }

    if (field.val() == undefined || $('#dtPOrderDate').val() == undefined) {
        return "* Please select the order date before adding the delivery schedule";
    }

    var orderDate = $('#dtPOrderDate').val();


    if (_util.dateGraterThan(field.val(), orderDate)) {
        return "* Delivery date cannot be less than order date";
    }

}

function CheckInvDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("invno", $('#Invoice').val());
    oResult = _comLayer.executeSyncAction("JobCardParts/InvDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Invoice No Already added";
}
function showOnlyOpen() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#open').val()
    });
}

function showOnlyInProgress() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#InProgress').val()
    });
}

function showOnlyCompleted() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Completed').val()
    });
}

function showOnlyCancelled() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Cancelled').val()
    });
}

function showOnlyClosed() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#closed').val()
    });
}

function showAll() {
    $('#MainSearchContainer').jtable('load', {
        CustomerName: $('#CustomerName').val()
    });
}

function validatebuttonActionHook() {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("Id", $("#Pk_Order").val());

    var oResult1 = _comLayer.executeSyncAction("/Order/Load", _comLayer.parameters);
    if (oResult1.data.deliverySchedule.length == 0)
        return true;
    else
        return false;
}


function showOnlyBoard() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Board').val()
    });
    document.getElementById('SType').value = "Board";
}
function showOnlyGum() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Gum').val()
    });
    document.getElementById('SType').value = "Gum";
}
function showOnlyInk() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Ink').val()
    });
    document.getElementById('SType').value = "Ink";
}


function showOnlyPaper() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Paper').val()
    });
    document.getElementById('SType').value = "Paper";
}

function showOnlyHoneycomb() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Honeycomb').val()
    });
    document.getElementById('SType').value = "Honeycomb";
}