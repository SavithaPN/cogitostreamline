﻿
var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Item Parts Name List',
        paging: true,
        pageSize: 8,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/ItemsPart/ItemNameListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_PartID: {
                title: 'Id',
                key: true,
                list: true
            },
            PartName: {
                title: 'Name',
                width: '25%'
            }

        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_PartID: $('#txtPartID').val(),
            PartName: $('#txtName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckHeadDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("PartName", $('#txtName').val());
    oResult = _comLayer.executeSyncAction("ItemsPart/NameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Part Name Already added";
}









