﻿//Name          : Payment Operations ----javascript files
//Description   : Contains the  Unit Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  
//Author        : shantha
//Date 	        : 20/10/2015
//Crh Number    : SL0003
//Modifications : 


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Payment List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Paymt/PaymentListByFiter',
            deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_PaymentID: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Fk_Vendor: {
                title: 'Vendor'
            },

            Description: {
                title: 'Description'
            },
            Amount: {
                title: 'Amount'
            },
            ChequeNo: {
                title: 'ChequeNo'
            },
            ChequeDate: {
                title: 'ChequeDate'
            }
        }
    });

    $('#TxtDate').datepicker({ autoclose: true });
    
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_PaymentID: $('#TxtID').val(),
            Description: $('#TxtDescription').val(),
            Fk_Vendor: $('#TxtVendor').val(),
            ChequeNo: $('#TxtChequeNo').val(),
            ChequeDate: $('#TxtDate').val()
        });

    });
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Vendor", "getVendor", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Vendor", "getVendor", null);


    _page.getViewByName('New').viewModel.addAddtionalDataSources("Bank", "getBank", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Bank", "getBank", null);



    //$('#TxtPkUnit').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Unit: $('#TxtPkUnit').val(),
    //            UnitName: $('#TxtUnitName').val(),
    //            UnitType: $('#TxtUnitType').val()
    //        });
    //    }
    //});

    //$('#TxtUnitName').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Unit: $('#TxtPkUnit').val(),
    //            UnitName: $('#TxtUnitName').val(),
    //            UnitType: $('#TxtUnitType').val()
    //        });
    //    }
    //});

    //$('#TxtUnitType').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Unit: $('#TxtPkUnit').val(),
    //            UnitName: $('#TxtUnitName').val(),
    //            UnitType: $('#TxtUnitType').val()
    //        });
    //    }
    //});

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    $('#ChequeDate').datepicker({
        autoclose: true
    });

}

function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('ChequeDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    $('#ChequeDate').datepicker({ autoclose: true });

  

}



//function CheckUnitDuplicate() {

//    _comLayer.parameters.clear();
//    var oResult;
//    _comLayer.parameters.add("UnitName", $('#UnitName').val());
//    oResult = _comLayer.executeSyncAction("Unit/UnitDuplicateChecking", _comLayer.parameters);
//    if (oResult.TotalRecordCount > 0)
//        return "* " + "Unit Name Already added";

//}


