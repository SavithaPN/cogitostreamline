﻿//Name          : Account Operations ----javascript files
//Description   : Contains the  Account Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  
//Author        : Shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0002
//Modifications : 

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Account Types List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/AccountType/AccountListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_AccountTypeID: {
                title: 'Id',
                key: true,
            },
            AccTypeName: {
                title: 'Name'
            },
            Description: {
                title: 'Description'
            }

        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_AccountTypeID: $('#TxtPkAccID').val(),
            AccTypeName: $('#TxtAccName').val()
        });

    });
    //$('#TxtPkColor').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Color: $('#TxtPkColor').val(),
    //            ColorName: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //$('#TxtColorName').keypress(function (e) {
    //    if (e.KeyCode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_Color: $('#TxtPkColor').val(),
    //            ColorName: $('#TxtColorName').val()
    //        });
    //    }

    //});

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}


function CheckAccountDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("AccTypeName", $('#AccTypeName').val());
    oResult = _comLayer.executeSyncAction("AccountType/AccountDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Account Name Already added";

}


