﻿//var curViewModel = null;
//var curEdit = false;
//var VendorID;
//var gtot;
//var EDVal;
//var IndentNo;
//var ssum = 0;
//var tempamt = 0;
//var intval = 0;
//var Indent = 0;
//var GSM = 0;
//var BF = 0;
//var Dec = 0;
//function initialCRUDLoad() {

//    $('#MainSearchContainer').jtable({
//        title: 'PO List',
//        paging: true,
//        pageSize: 10,
//        sorting: false,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/PurchaseOrder/PurchaseOrderListByFiter',
//            //deleteAction: '',            
//            //createAction: '',
//            //updateAction: ''
//        },
//        fields: {
//            Pk_PONo: {
//                title: 'PO No',
//                key: true,
//                list: true,
//                width: '4%'
//            },
//            //Pk_Disp: {
//            //    title: 'PO No.',
//            //    width: '4%'
//            //},
//            PODate: {
//                title: 'PO Date',
//                width: '4%'
//            },

//            Fk_Vendor: {
//                title: 'Vendor',
//                width: '10%'
//            },
//            Fk_Indent: {
//                title: 'Indent No.',
//                width: '10%'
//            },
//            Status: {
//                title: 'Status',
//                width: '10%'
//            },
//            Select_A: {
//                title: "Close PO Manually",
//                display: function (row) {
//                    var button1 = $("<button class='bnt'>Close PO</button>");
//                    $(button1).click(function () {
//                        PO = row.record.Pk_PONo;
//                        _comLayer.parameters.add("POno", PO);
//                        oResult = _comLayer.executeSyncAction("PurchaseOrder/StatusUpdate", _comLayer.parameters);
//                        if (oResult = true)
//                        { alert("PO Status Updated Successfully") }

//                    });
//                    return button1;
//                }
//            },
//            Select_B: {
//                title: "Open PO Manually",
//                display: function (row) {
//                    var button1 = $("<button class='bnt'>Open PO</button>");
//                    $(button1).click(function () {
//                        PO = row.record.Pk_PONo;
//                        _comLayer.parameters.add("POno", PO);
//                        oResult = _comLayer.executeSyncAction("PurchaseOrder/OpenStatus", _comLayer.parameters);
//                        if (oResult = true)
//                        { alert("PO Status Updated Successfully") }

//                    });
//                    return button1;
//                }
//            },
//            Print: {
//                title: 'Print',
//                width: '2%',
//                //display: function (data) {
//                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
//                display: function (row) {
//                    var button = $("<i class='icon-printer'></i>");
//                    $(button).click(function () {

//                        Pk_PONO = row.record.Pk_PONo;
//                        _comLayer.parameters.add("Pk_PONO", Pk_PONO);
//                        _comLayer.executeSyncAction("PurchaseOrder/POrderRep", _comLayer.parameters);
//                        var strval = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
//                        window.open(strval, '_blank ', 'width=700,height=250');
//                        ////////////////////////////


//                        //var xhr = new XMLHttpRequest();
//                        //var urlToFile = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
//                        //xhr.open('HEAD', urlToFile, false);
//                        //xhr.send();

//                        //if (xhr.status == "404") {
//                        //    alert('Data Not Available , File does not Exist');
//                        //    return false;


//                        //} else {
//                        //    window.open(strval, '_blank ', 'width=700,height=250');
//                        //    return true;
//                        //}


//                        /////////////////////////


//                    });
//                    return button;
//                }
//            },
//            PrintConsumables: {
//                title: 'CON',
//                width: '2%',
//                //display: function (data) {
//                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
//                display: function (row) {
//                    var button = $("<i class='icon-printer'></i>");
//                    $(button).click(function () {

//                        Pk_PONO = row.record.Pk_PONo;
//                        _comLayer.parameters.add("Pk_PONO", Pk_PONO);
//                        _comLayer.executeSyncAction("PurchaseOrder/POrderRepConsumable", _comLayer.parameters);
//                        var strval = "ConvertPDF/POrder_Consumables" + Pk_PONO + ".pdf"
//                        window.open(strval, '_blank ', 'width=700,height=250');
//                        ////////////////////////////


//                        //var xhr = new XMLHttpRequest();
//                        //var urlToFile = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
//                        //xhr.open('HEAD', urlToFile, false);
//                        //xhr.send();

//                        //if (xhr.status == "404") {
//                        //    alert('Data Not Available , File does not Exist');
//                        //    return false;


//                        //} else {
//                        //    window.open(strval, '_blank ', 'width=700,height=250');
//                        //    return true;
//                        //}


//                        /////////////////////////


//                    });
//                    return button;
//                }
//            },
//            //////////////////////////////////////////////////
//            Details: {
//                title: 'Details',
//                width: '5%',
//                sorting: false,
//                edit: false,
//                create: false,
//                listClass: 'child-opener-image-column',
//                display: function (data) {
//                    //Create an image that will be used to open child table
//                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
//                    var $img = $("<i class='icon-users'></i>");
//                    //Open child table when user clicks the image
//                    $img.click(function () {
//                        $('#MainSearchContainer').jtable('openChildTable',
//                                    $img.closest('tr'),
//                                    {
//                                        title: 'Material List',
//                                        actions: {
//                                            listAction: '/PurchaseOrder/POGetRec?Pk_PONo=' + data.record.Pk_PONo

//                                        },
//                                        fields: {


//                                            MaterialName: {
//                                                title: 'MaterialName'
//                                            },

//                                            Quantity: {
//                                                title: 'Quantity',
//                                                key: true,
//                                            },
//                                            Color: {
//                                                title: 'Shade',
//                                                key: true,
//                                            },
//                                            //Mill: {
//                                            //    title: 'Mill',
//                                            //    key: true,
//                                            //},
//                                            Fk_IndentVal: {
//                                                title: 'Indent',
//                                                key: true,

//                                            },
//                                        },
//                                        formClosed: function (event, data) {
//                                            data.form.validationEngine('hide');
//                                            data.form.validationEngine('detach');
//                                        }
//                                    }, function (data) { //opened handler
//                                        data.childTable.jtable('load');
//                                    });
//                    });
//                    //Return image to show on the person row
//                    return $img;
//                }
//            },

//            ///////////////////////////////////////////////
//        }
//    });

//    //Re-load records when Customer click 'load records' button.
//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#MainSearchContainer').jtable('load', {
//            Pk_PONo: $('#txtPkPONo').val(),
//            PODate: $('#txtPODate').val(),
//            FkIndent: $('#txtIndNo').val(),

//        });

//    });

//    //Load all records when page is first shown
//    $('#LoadRecordsButton').click();

//    $('#txtPODate').change(function (e) {
//        e.preventDefault();
//        $('#MainSearchContainer').jtable('load', {
//            PODate: $('#txtPODate').val()
//        });
//    });
//    $('#txtPODate').datepicker({
//        autoclose: true
//    });

//    $('#cmdNew').click(function (e) {
//        e.preventDefault();
//        _page.showView('New');
//    });

//    $('#dtDate').datepicker({ autoclose: true });


//    $('#Pk_PONo').keypress(function (e) {
//        if (e.keyCode == 13) {
//            e.preventDefault();

//            $('#MainSearchContainer').jtable('load', {
//                Pk_PONo: $('#txtPkPONo').val()
//            });
//        }
//    });

//    $('#dtDate').change(function (e) {
//        e.preventDefault();
//        $('#MainSearchContainer').jtable('load', {
//            PODate: $('#txtPODate').val()
//        });
//    });

//}


//function afterNewShow(viewObject) {
//    var dNow = new Date();
//    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
//    curViewModel = viewObject.viewModel;

//    $('#dtDate').datepicker({ autoclose: true });

//    $('#tlbMaterials').jtable({
//        title: 'Item List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/PurchaseOrder/Bounce',
//            deleteAction: '',
//            updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            Fk_Material: {
//                title: 'Id',
//                key: false,
//                list: false
//            },
//            Name: {
//                title: 'Material Name'

//            },
//            Color: {
//                title: 'Shade'

//            },
//            Mill: {
//                title: 'Mill'

//            },
//            Quantity: {
//                title: 'Quantity',
//                width: '1%'

//            },

//            Rate: {
//                title: 'Rate',
//                width: '1%'
//            },
//            Amount: {
//                title: 'Amount',
//                width: '1%'

//            },
//            Fk_IndentVal: {
//                title: 'Fk_IndentVal',
//                list: false,
//                width: '1%'

//            },
//            HSNCode: {
//                title: 'HSNCode',
//                width: '1%'

//            },
//        }
//    });


//    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });



//    Calc();

//    $('#cmdVendorMSearch').click(function (e) {
//        e.preventDefault();
//        setUpVendorSearch(viewObject);
//        $("#searchDialog").modal("show");
//        $("#searchDialog").width(600);
//        $("#searchDialog").height(500);

//    });


//    $('#cmdMIndentSearch').click(function (e) {
//        e.preventDefault();
//        setUpIndentSearch();
//        $("#searchIndentDialog").modal("show");
//        //$("#dataDialog").width(700);
//        //$("#dataDialog").height(500);

//        $("#searchIndentDialog").css("top", "50px");
//        $("#searchIndentDialog").css("left", "350px");
//    });
//}


//function beforeModelSaveEx() {
//    var viewModel = _page.getViewByName('New').viewModel;

//    Calc();

//    viewModel.data["PODate"] = $('#dtDate').val();
//    viewModel.data["Fk_Vendor"] = VendorID;
//    viewModel.data["Fk_Indent"] = $('#Fk_Indent').val();
//    viewModel.data["TaxType"] = $('#TaxVal').val();
//    viewModel.data["NETVALUE"] = $('#NETVALUE').val();
//    viewModel.data["GrandTotal"] = $('#GrandTotal').val();
//    viewModel.data["CGST"] = $('#CGST').val();
//    viewModel.data["SGST"] = $('#SGST').val();

//    var viewModel1 = _page.getViewByName('Edit').viewModel;
//    viewModel1.data["PODate"] = $('#dtDate').val();
//    viewModel1.data["Fk_Vendor"] = $('#Fk_Vendor').val();
//    viewModel1.data["Fk_Vendor"] = VendorID;
//    viewModel1.data["Fk_Indent"] = $('#Fk_Indent').val();
//    viewModel1.data["NETVALUE"] = $('#NETVALUE').val();
//    viewModel1.data["GrandTotal"] = $('#GrandTotal').val();
//    viewModel.data["CGST"] = $('#CGST').val();
//    viewModel.data["SGST"] = $('#SGST').val();
//    viewModel1.data["TaxType"] = $('#TaxVal').val();
//    viewModel1.data["Amount"] = $('#Amount').val();
//    viewModel1.data["Rate"] = $('#Rate').val();

//    //var statVal = document.getElementById('txtClose').value;

//    //if (statVal == 1) {
//    //    viewModel1.data["Fk_Status"] = 4;
//    //}


//    //else { viewModel1.data["Fk_Status"] = 1; }
//}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;

    var TaxSel = document.getElementById('TaxVal').value;

    switch (TaxSel) {
        case "":
            //  alert('Select TaxType and Proceed')
            break;
        case "6%":
            var TaxValue = Number(GTot * 0.06);
            document.getElementById('CGST').value = TaxValue;
            document.getElementById('SGST').value = TaxValue;
            document.getElementById('TaxAmount').value = Number(TaxValue * 2);
            document.getElementById('NETVALUE').value = Number(TaxValue * 2) + Number(GTot);
            break;
        case "IGST":
            var TaxValue = Number(GTot * 0.06);
            document.getElementById('CGST').value = Number(TaxValue * 2);
            //document.getElementById('SGST').value = TaxValue;
            document.getElementById('TaxAmount').value = Number(TaxValue * 2);

            document.getElementById('NETVALUE').value = Number(TaxValue * 2) + Number(GTot);
            break;
        case "9%":
            var TaxValue = Number(GTot * 0.09);
            document.getElementById('CGST').value = TaxValue;
            document.getElementById('SGST').value = TaxValue;
            document.getElementById('TaxAmount').value = Number(TaxValue * 2);

            document.getElementById('NETVALUE').value = Number(TaxValue * 2) + Number(GTot);
            break;
        case "2.5%":
            var TaxValue = Number(GTot * 0.025);
            document.getElementById('CGST').value = TaxValue;
            document.getElementById('SGST').value = TaxValue;
            document.getElementById('TaxAmount').value = Number(TaxValue * 2);

            document.getElementById('NETVALUE').value = Number(TaxValue * 2) + Number(GTot);
            break;
        case "0%":
            var TaxValue = 0;
            document.getElementById('CGST').value = 0;
            document.getElementById('SGST').value = 0;
            document.getElementById('TaxAmount').value = 0;

            document.getElementById('NETVALUE').value =Number(GTot);
            break;
    }
}


//function afterEditShow(viewObject) {
//    //curEdit = true;
//    curViewModel = viewObject.viewModel;
//    $('#dtDate').datepicker({ autoclose: true });
//    curEdit = true;


//    $('#tlbMaterials').jtable({
//        title: 'Item List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/PurchaseOrder/Bounce',
//            deleteAction: '',
//            updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            Fk_Material: {
//                title: 'Id',
//                key: false,
//                list: false
//            },
//            Name: {
//                title: 'Material Name',
//                width: '5%'
//            },
//            Quantity: {
//                title: 'Quantity',
//                width: '1%'
//            },

//            Rate: {
//                title: 'Rate',
//                width: '1%'
//            },
//            Amount: {
//                title: 'Amount',
//                width: '1%'

//            },
//            HSNCode: {
//                title: 'HSNCode',
//                width: '1%'

//            }
//        }
//    });

//    if ($.trim($('#TaxType').val()) == "VAT") {
//        $("#VAT").prop("checked", true);
//    }
//    else if ($.trim($('#TaxType').val()) == "CST") {
//        $("#CST").prop("checked", true);




//    }

//    var oSCuts = viewObject.viewModel.data.MaterialData();
//    viewObject.viewModel.data["Materials"] = ko.observableArray();
//    var i = 0;
//    ssum = 0;
//    while (oSCuts[i]) {
//        var oCut = new IssuedMaterial();

//        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount, oSCuts[i].HSNCode);
//        viewObject.viewModel.data["Materials"].push(oCut);

//        i++;
//    }

//    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });


//    //var POstatus = viewObject.viewModel.data["Fk_Status"]();

//    //if (POstatus == "4") {
//    //    $("#chkCloseJC").attr('checked', true);
//    //}
//}

//function setUpVendorSearch() {
//    //Enquiry

//    // cleanSearchDialog();

//    $("#srchssHeader").text("Vendor Search");

//    //Adding Search fields
//    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

//    $("#dlgSearchFields").append(txtFieldVendorName);


//    $('#searchDialog').jtable({
//        title: 'Vendor List',
//        paging: true,
//        pageSize: 15,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/Vendor/VendorListByFiter',
//        },
//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdDone').click();
//            });
//        },


//        fields: {
//            Pk_Vendor: {
//                title: 'Vendor ID',
//                key: true,
//                width: '2%'
//            },
//            VendorName: {
//                title: 'Vendor Name',
//                edit: false,
//                width: '5%'
//            }
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#searchDialog').jtable('load', {
//            VendorName: $('#txtdlgVendorName').val()
//        });
//    });
//    $('#LoadRecordsButton').click();

//    $('#cmdSearch').click(function (e) {
//        e.preventDefault();
//        $('#searchDialog').jtable('load', {
//            VendorName: $('#txtdlgVendorName').val()
//        });
//    });

//    $('#cmdDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#searchDialog').jtable('selectedRows');
//        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
//        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
//        VendorID = rows[0].keyValue;
//        $("#searchDialog").modal("hide");
//    });

//}

//function setUpIssueMaterialSearch() {
//    Indent = document.getElementById('Fk_Indent').value;
//    if (GSM > 0 & BF > 0 & Dec > 0) {
//        document.getElementById('txtMaterial').value = GSM;
//        document.getElementById('txtBF').value = BF;
//        document.getElementById('txtDec').value = Dec;
//    }
//    $('#MaterialSearchContainer').jtable({
//        title: 'Material List',
//        paging: true,
//        pageSize: 10,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/PurchaseOrder/getMaterialIndentDetails?Indent=' + Indent,
//        },
//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdMaterialDone').click();
//            });
//        },

//        fields: {
//            Pk_Material: {
//                title: 'Material Id',
//                key: true
//            },
//            MatIndent: {
//                title: 'IndentNo.'
//            },
//            MaterialName: {
//                title: 'Material Name'
//            },

//            Color: {
//                title: "Shade"
//            },
//            Mill: {
//                title: "Mill"
//            },
//            Quantity: {
//                title: "Quantity"
//            },
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#MaterialSearchContainer').jtable('load', {
//            MaterialName: $('#txtMaterial').val(),
//            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
//            GSM: $('#txtMaterial').val(),
//            BF: $('#txtBF').val(),
//            Dec: $('#txtDec').val(),
//        });
//    });
//    $('#LoadRecordsButton').click();

//    $('#cmdMaterialSearch').click(function (e) {
//        e.preventDefault();
//        $('#MaterialSearchContainer').jtable('load', {
//            MaterialName: $('#txtMaterial').val(),
//            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
//            GSM: $('#txtMaterial').val(),
//            BF: $('#txtBF').val(),
//            Dec: $('#txtDec').val(),
//        });
//    });

//    $('#cmdMaterialDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
//        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
//        document.getElementById('Quantity').value = rows[0].data["Quantity"];
//        document.getElementById('MaterialName').value = rows[0].data["MaterialName"];
//        document.getElementById('Fk_IndentVal').value = rows[0].data["MatIndent"];
//        document.getElementById('Shade').value = rows[0].data["Color"];
//        SaveSearch();
//        _util.setDivPosition("divCreateMaterial", "block");
//        _util.setDivPosition("divSearchMaterial", "none");
//        $("#Rate").focus();
//    });



//}



//function setUpOtherMaterialSearch() {
//    //Indent = document.getElementById('Fk_Indent').value;
//    //if (GSM > 0 & BF > 0 & Dec > 0) {
//    //    document.getElementById('txtMaterial').value = GSM;
//    //    document.getElementById('txtBF').value = BF;
//    //    document.getElementById('txtDec').value = Dec;
//    //}
//    $('#MaterialSearchContainerSec').jtable({
//        title: 'Material List',
//        paging: true,
//        pageSize: 10,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/Paper/PaperListByFiter'
//        },
//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdMaterialDone').click();
//                //document.getElementById('txtMatID').value = row_id;
//                //setUpOtherMaterialSearch();
//            });
//        },

//        fields: {
//            Pk_Material: {
//                title: 'Material Id',
//                key: true
//            },
//            //MatIndent: {
//            //    title: 'IndentNo.'
//            //},
//            Name: {
//                title: 'Material Name'
//            },

//            PaperColor: {
//                title: "Shade"
//            },
//            Mill: {
//                title: "Mill"
//            },
//            //Quantity: {
//            //    title: "Quantity"
//            //},
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#MaterialSearchContainerSec').jtable('load', {
//            MaterialName: $('#txtMaterial').val(),
//            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
//            GSM: $('#txtMaterial').val(),
//            BF: $('#txtBF').val(),
//            Deckle: $('#txtDec').val(),
//        });
//    });
//    $('#LoadRecordsButton').click();

//    $('#cmdMaterialSearch').click(function (e) {
//        e.preventDefault();
//        $('#MaterialSearchContainerSec').jtable('load', {
//            MaterialName: $('#txtMaterial').val(),
//            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
//            GSM: $('#txtMaterial').val(),
//            BF: $('#txtBF').val(),
//            Deckle: $('#txtDec').val(),
//        });
//    });

//    $('#cmdMaterialDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#MaterialSearchContainerSec').jtable('selectedRows');
//        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
//        document.getElementById('Quantity').value = rows[0].data["Quantity"];
//        document.getElementById('Mill').value = rows[0].data["Mill"];
//        document.getElementById('Fk_IndentVal').value = rows[0].data["MatIndent"];
//        document.getElementById('Shade').value = rows[0].data["Color"];
//        SaveSearch();
//        _util.setDivPosition("divCreateMaterial", "block");
//        _util.setDivPosition("divSearchMaterial", "none");
//        $("#Rate").focus();
//    });



//}


//function afterOneToManyDialogShow(property) {

//    $('#cmdSearchMaterialsdetails').click(function (e) {
//        e.preventDefault();
//        setUpIssueMaterialSearch();
//        _util.setDivPosition("divSearchMaterial", "block");
//        _util.setDivPosition("divCreateMaterial", "none");
//    });
//}

//function ReferenceFieldNotInitilized(viewModel) {

//    //document.getElementById('ReturnableQty').style.display = "none";

//    if (objContextEdit == true) {
//        $('#cmdSearchMaterialsdetails').attr('disabled', true);
//    }
//    else if (objContextEdit == false) {
//        $('#cmdSearchMaterialsdetails').attr('disabled', false);
//    }
//    $('#Fk_Vendor').wgReferenceField({
//        keyProperty: "Fk_Vendor",
//        displayProperty: "VendorName",
//        loadPath: "Vendor/Load",
//        viewModel: viewModel
//    });
//    $('#txtFk_Material').wgReferenceField({
//        keyProperty: "Fk_Material",
//        displayProperty: "Name",
//        loadPath: "Material/Load",
//        viewModel: viewModel
//    });

//    //$('#Fk_Indent').wgReferenceField({
//    //    keyProperty: "Fk_Indent",
//    //    displayProperty: "Fk_Indent",
//    //    loadPath: "MaterialIndent/Load",
//    //    viewModel: viewModel
//    //});

//    if (viewModel.data != null) {
//        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
//        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
//        //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
//    }
//}

//function checkDuplicate() {

//    if (objContextEdit == false) {
//        var i = 0;

//        while (curViewModel.data["Materials"]()[i]) {
//            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

//                return "* " + "Item Already added";
//            }
//            i++;
//        }
//    }
//}

//function checkstock() {
//    if (objContextEdit == false) {

//        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
//            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
//        }
//        else {

//        }
//    } else if (objContextEdit == true) {
//        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
//            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
//        }
//        else {
//        }
//    }
//}
//function setUpIndentSearch() {
//    //Branch

//    //cleanSearchDialog();

//    $("#srchHeader1").text("Indent Search");

//    //Adding Search fields
//    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
//    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
//    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
//    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




//    $('#IndentSearhContainer').jtable({
//        title: 'Indent List',
//        paging: true,
//        pageSize: 10,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        actions: {
//            listAction: '/MaterialIndent/MaterialIndentPOList'
//        },
//        recordsLoaded: function (event, data) {
//            $('.jtable-data-row').click(function () {
//                var row_id = $(this).attr('data-record-key');
//                $('#cmdIndentDone').click();
//            });
//        },

//        fields: {
//            Pk_MaterialOrderMasterId: {
//                title: 'Indent Number',
//                key: true
//            },

//            MaterialIndentDate: {
//                title: 'Indent Date'
//            },
//            //CustomerName=p.CustomerName,
//            //MaterialName=p.Name,
//            //CustomerName: {
//            //    title: 'CustomerName'
//            //},
//            MaterialName: {
//                title: 'MaterialName'
//            },
//            Color: {
//                title: 'Shade'
//            },
//            //Mill: {
//            //    title: 'Mill'
//            //},
//            Quantity: {
//                title: 'Ind.Qty'
//            },
//            PendingQty: {
//                title: 'PendingQty'
//            },
//            Pk_MaterialOrderDetailsId: {
//                title: 'Pk_MaterialOrderDetailsId',
//                list: false
//            },
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#IndentSearhContainer').jtable('load', {
//            Pk_MaterialIndent: $('#txtdlgIndent').val()

//        });
//    });

//    $('#LoadRecordsButton').click();


//    $('#cmdIndentSearch').click(function (e) {
//        e.preventDefault();

//        $('#IndentSearhContainer').jtable('load', {
//            Pk_MaterialIndent: $('#txtdlgIndent').val(),
//            Vendor: $('#txtdlgVendor').val(),
//            FromIndentDate: $('#txtdlgFromDate').val(),
//            ToIndentDate: $('#txtdlgToDate').val()

//        });
//    });

//    $('#cmdIndentDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#IndentSearhContainer').jtable('selectedRows');
//        $("#searchIndentDialog").modal("hide");
//        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
//        document.getElementById('Fk_Indent').value = rows[0].keyValue;
//        IndentNo = rows[0].keyValue;

//        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

//    });

//}
//function CalcAmt() {
//    //  curViewModel = viewObject.viewModel;
//    if (objContextEdit == false) {
//        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
//            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));


//        }

//        else {
//        }
//    } else if (objContextEdit == true) {
//        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
//            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
//        }
//        else {
//        }



//    }
//}

//function radioClass1(intval) {
//    if (intval == 1) {


//        document.getElementById('TaxType').value = "VAT";
//        if (FORM_CT3.checked == true)
//        { document.getElementById('ED').value = Number($('#GrandTotal').val()); }
//        else {
//            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
//        }

//        FORM_CT3.style.visibility = 'visible';
//        FORM_H.style.visibility = 'visible';

//        //    fct3.style.visibility = 'hidden';
//        fct3.style.visibility = 'visible';
//        fh.style.visibility = 'visible';

//        if (Number($('#ED').val()) > 0) {
//            document.getElementById('NETVALUE').value = 0;
//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
//        }

//        else if (Number($('#ED').val()) == 0) {
//            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
//            //  NETVALUE
//        }

//    }
//    else if (intval == 2) {

//        document.getElementById('TaxType').value = "CST";

//        FORM_H.style.visibility = 'hidden';

//        fh.style.visibility = 'hidden';


//        if (Number($('#ED').val()) > 0) {

//            document.getElementById('NETVALUE').value = 0;
//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
//        }

//        else if (Number($('#ED').val()) == 0) {
//            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
//            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
//        }

//    }


//}

//function SaveSearch() {
//    GSM = document.getElementById('txtMaterial').value;
//    BF = document.getElementById('txtBF').value;
//    Dec = document.getElementById('txtDec').value;
//}

function TaxChange(selectObj) {
    var selectIndex = selectObj.selectedIndex;
    var selectValue = selectObj.options[selectIndex].text;
    if (selectValue == "Select") {
        document.getElementById('TaxVal').value;
        //var PVal=  document.getElementById('PrintVal').value;

        Calc();
    }
    else {
        document.getElementById('TaxVal').value = selectValue;
        Calc();
    }

}
var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
var GSM = 0;
var BF = 0;
var Dec = 0;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'PO List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/PurchaseOrderListByFiter',
            //deleteAction: '',            
            //createAction: '',
            updateAction: ''
        },
        fields: {
            Pk_PONo: {
                title: 'PO No',
                key: true,
                list: true,
                width: '4%'
            },
            Pk_Disp: {
                title: 'PO No.',
                width: '4%'
            },
            PODate: {
                title: 'PO Date',
                width: '4%'
            },

            Fk_Vendor: {
                title: 'Vendor',
                width: '10%'
            },
            Fk_Indent: {
                title: 'Indent No.',
                width: '10%'
            },
            Status: {
                title: 'Status',
                width: '10%'
            },
            Comments: {
                title: 'Reason',
                width: '10%'
            },
            Select_A: {
                title: "Close PO Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Close PO</button>");
                    $(button1).click(function () {
                        PO = row.record.Pk_PONo;
                        _comLayer.parameters.add("POno", PO);
                        oResult = _comLayer.executeSyncAction("PurchaseOrder/StatusUpdate", _comLayer.parameters);
                        if (oResult = true)
                        { alert("PO Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Select_B: {
                title: "Open PO Manually",
                display: function (row) {
                    var button1 = $("<button class='bnt'>Open PO</button>");
                    $(button1).click(function () {
                        PO = row.record.Pk_PONo;
                        _comLayer.parameters.add("POno", PO);
                        oResult = _comLayer.executeSyncAction("PurchaseOrder/OpenStatus", _comLayer.parameters);
                        if (oResult = true)
                        { alert("PO Status Updated Successfully") }

                    });
                    return button1;
                }
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_PONO = row.record.Pk_PONo;
                        _comLayer.parameters.add("Pk_PONO", Pk_PONO);
                        _comLayer.executeSyncAction("PurchaseOrder/POrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////


                        //var xhr = new XMLHttpRequest();
                        //var urlToFile = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
                        //xhr.open('HEAD', urlToFile, false);
                        //xhr.send();

                        //if (xhr.status == "404") {
                        //    alert('Data Not Available , File does not Exist');
                        //    return false;


                        //} else {
                        //    window.open(strval, '_blank ', 'width=700,height=250');
                        //    return true;
                        //}


                        /////////////////////////


                    });
                    return button;
                }
            },
            PrintConsumables: {
                title: 'CON',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_PONO = row.record.Pk_PONo;
                        _comLayer.parameters.add("Pk_PONO", Pk_PONO);
                        _comLayer.executeSyncAction("PurchaseOrder/POrderRepConsumable", _comLayer.parameters);
                        var strval = "ConvertPDF/POrder_Consumables" + Pk_PONO + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////


                        //var xhr = new XMLHttpRequest();
                        //var urlToFile = "ConvertPDF/POrder" + Pk_PONO + ".pdf"
                        //xhr.open('HEAD', urlToFile, false);
                        //xhr.send();

                        //if (xhr.status == "404") {
                        //    alert('Data Not Available , File does not Exist');
                        //    return false;


                        //} else {
                        //    window.open(strval, '_blank ', 'width=700,height=250');
                        //    return true;
                        //}


                        /////////////////////////


                    });
                    return button;
                }
            },
            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/PurchaseOrder/POGetRec?Pk_PONo=' + data.record.Pk_PONo

                                        },
                                        fields: {


                                            MaterialName: {
                                                title: 'MaterialName'
                                            },

                                            Quantity: {
                                                title: 'Quantity',
                                                key: true,
                                            },
                                            Color: {
                                                title: 'Shade',
                                                key: true,
                                            },
                                            //Mill: {
                                            //    title: 'Mill',
                                            //    key: true,
                                            //},
                                            Fk_IndentVal: {
                                                title: 'Indent',
                                                key: true,

                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },

            ///////////////////////////////////////////////
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_PONo: $('#txtPkPONo').val(),
            PODate: $('#txtPODate').val(),
            FkIndent: $('#txtIndNo').val(),

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#txtPODate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            PODate: $('#txtPODate').val()
        });
    });
    $('#txtPODate').datepicker({
        autoclose: true
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtDate').datepicker({ autoclose: true });


    $('#Pk_PONo').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            $('#MainSearchContainer').jtable('load', {
                Pk_PONo: $('#txtPkPONo').val()
            });
        }
    });

    $('#dtDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            PODate: $('#txtPODate').val()
        });
    });

}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            Color: {
                title: 'Shade'

            },
            Mill: {
                title: 'Mill'

            },
            Quantity: {
                title: 'Quantity',
                width: '1%'

            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            },
            Fk_IndentVal: {
                title: 'Fk_IndentVal',
                list: false,
                width: '1%'

            },
            HSNCode: {
                title: 'HSNCode',
                width: '1%'

            },
        }
    });


    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });



    Calc();

    $('#cmdVendorMSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });


    $('#cmdMIndentSearch').click(function (e) {
        e.preventDefault();
        setUpIndentSearch();
        $("#searchIndentDialog").modal("show");
        //$("#dataDialog").width(700);
        //$("#dataDialog").height(500);

        $("#searchIndentDialog").css("top", "50px");
        $("#searchIndentDialog").css("left", "350px");
    });
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;

    Calc();

    viewModel.data["PODate"] = $('#dtDate').val();
    viewModel.data["Fk_Vendor"] = VendorID;
    viewModel.data["Fk_Indent"] = $('#Fk_Indent').val();
    viewModel.data["TaxType"] = $('#TaxVal').val();
    viewModel.data["NETVALUE"] = $('#NETVALUE').val();
    viewModel.data["GrandTotal"] = $('#GrandTotal').val();
    viewModel.data["CGST"] = $('#CGST').val();
    viewModel.data["SGST"] = $('#SGST').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["PODate"] = $('#dtDate').val();
    viewModel1.data["Fk_Vendor"] = $('#Fk_Vendor').val();
    viewModel1.data["Fk_Vendor"] = VendorID;
    viewModel1.data["Fk_Indent"] = $('#Fk_Indent').val();
    viewModel1.data["NETVALUE"] = $('#NETVALUE').val();
    viewModel1.data["GrandTotal"] = $('#GrandTotal').val();
    viewModel1.data["CGST"] = $('#CGST').val();
    viewModel1.data["SGST"] = $('#SGST').val();
    viewModel1.data["TaxType"] = $('#TaxVal').val();
    viewModel1.data["Amount"] = $('#Amount').val();
    viewModel1.data["Rate"] = $('#Rate').val();

    //var statVal = document.getElementById('txtClose').value;

    //if (statVal == 1) {
    //    viewModel1.data["Fk_Status"] = 4;
    //}


    //else { viewModel1.data["Fk_Status"] = 1; }
}

//function Calc() {
//    var GTot = document.getElementById('GrandTotal').value;
//    var TaxVal = Number(GTot * 0.06);
//    document.getElementById('CGST').value = TaxVal;
//    document.getElementById('SGST').value = TaxVal;

//    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

//}
function afterEditShow(viewObject) {
    //curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;


    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            },
            HSNCode: {
                title: 'HSNCode',
                width: '1%'

            }
        }
    });

    if ($.trim($('#TaxType').val()) == "VAT") {
        $("#VAT").prop("checked", true);
    }
    else if ($.trim($('#TaxType').val()) == "CST") {
        $("#CST").prop("checked", true);




    }

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount, oSCuts[i].HSNCode);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });


    //var POstatus = viewObject.viewModel.data["Fk_Status"]();

    //if (POstatus == "4") {
    //    $("#chkCloseJC").attr('checked', true);
    //}
}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true,
                width: '2%'
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {
    Indent = document.getElementById('Fk_Indent').value;
    if (GSM > 0 & BF > 0 & Dec > 0) {
        document.getElementById('txtMaterial').value = GSM;
        document.getElementById('txtBF').value = BF;
        document.getElementById('txtDec').value = Dec;
    }
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/getMaterialIndentDetails?Indent=' + Indent,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //  $('#cmdMaterialDone').click();



                //var GsmVal = $(this).data("record").GSM;
                //var BfVal = $(this).data("record").BF;
                //var DeckVal = $(this).data("record").Deckle;
                ////var PkMat = $(this).data("record").Pk_Material;
                ////var RateVal = $(this).data("record").lblrate;
                //document.getElementById('txtMaterial').value = GsmVal;
                //document.getElementById('txtBF').value = BfVal;
                //document.getElementById('txtDec').value = DeckVal;

                //setUpOtherMaterialSearch();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            MatIndent: {
                title: 'IndentNo.'
            },
            MaterialName: {
                title: 'Material Name'
            },

            Color: {
                title: "Shade"
            },
            //Mill: {
            //    title: "Mill"
            //},
            Quantity: {
                title: "Quantity"
            },

            GSM: {
                title: "GSM",
                list: false
            },
            BF: {
                title: "BF",
                list: false
            },
            Deckle: {
                title: "Deckle",
                list: false
            },

            Details: {

                title: 'Det.',
                paging: true,
                pageSize: 10,
                selecting: true, //Enable selecting
                multiselect: false, //Allow multiple selecting
                selectingCheckboxes: true,
                defaultSorting: 'Name ASC',
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {

                        var qty = data.record.Quantity;
                        var ind = data.record.MatIndent;
                        var matName = data.record.MaterialName;
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/Paper/PaperListByFiter?FullString=' + data.record.GSM + ',' + data.record.BF + ',' + data.record.Deckle
                                        },
                                        recordsLoaded: function (event, data) {
                                            $('.jtable-data-row').click(function () {
                                                var row_id = $(this).attr('data-record-key');
                                                //  $('#cmdMaterialDone').click();

                                                var rows = $('#MaterialSearchContainer').jtable('selectedRows');

                                                document.getElementById('txtFk_Material').value = $(this).data("record").Pk_Material;
                                                document.getElementById('MaterialName').value = matName;

                                                document.getElementById('Quantity').value = qty;
                                                document.getElementById('Mill').value = $(this).data("record").Mill;
                                                document.getElementById('Fk_IndentVal').value = ind;
                                                document.getElementById('Shade').value = $(this).data("record").PaperColor;
                                                SaveSearch();
                                                _util.setDivPosition("divCreateMaterial", "block");
                                                _util.setDivPosition("divSearchMaterial", "none");
                                                $("#HSNCode").focus();


                                                //var GsmVal = $(this).data("record").GSM;
                                                //var BfVal = $(this).data("record").BF;
                                                //var DeckVal = $(this).data("record").Deckle;
                                                ////var PkMat = $(this).data("record").Pk_Material;
                                                ////var RateVal = $(this).data("record").lblrate;
                                                //document.getElementById('txtMaterial').value = GsmVal;
                                                //document.getElementById('txtBF').value = BfVal;
                                                //document.getElementById('txtDec').value = DeckVal;

                                                //setUpOtherMaterialSearch();
                                            });
                                        },

                                        fields: {
                                            Pk_Material: {
                                                title: 'Material Id',
                                                key: true
                                            },
                                            //MatIndent: {
                                            //    title: 'IndentNo.'
                                            //},
                                            Name: {
                                                title: 'Material Name'
                                            },

                                            PaperColor: {
                                                title: "Shade"
                                            },
                                            Mill: {
                                                title: "Mill"
                                            },
                                            //Quantity: {
                                            //    title: "Quantity"
                                            //},

                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Dec: $('#txtDec').val(),
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Dec: $('#txtDec').val(),
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Quantity').value = rows[0].data["Quantity"];
        //document.getElementById('Mill').value = rows[0].data["Mill"];
        document.getElementById('Fk_IndentVal').value = rows[0].data["MatIndent"];
        document.getElementById('Shade').value = rows[0].data["Color"];
        SaveSearch();
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Rate").focus();
    });



}



function setUpOtherMaterialSearch() {
    //Indent = document.getElementById('Fk_Indent').value;
    //if (GSM > 0 & BF > 0 & Dec > 0) {
    //    document.getElementById('txtMaterial').value = GSM;
    //    document.getElementById('txtBF').value = BF;
    //    document.getElementById('txtDec').value = Dec;
    //}
    $('#MaterialSearchContainerSec').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
                //document.getElementById('txtMatID').value = row_id;
                //setUpOtherMaterialSearch();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            //MatIndent: {
            //    title: 'IndentNo.'
            //},
            Name: {
                title: 'Material Name'
            },

            PaperColor: {
                title: "Shade"
            },
            Mill: {
                title: "Mill"
            },
            //Quantity: {
            //    title: "Quantity"
            //},
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainerSec').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Deckle: $('#txtDec').val(),
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainerSec').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val(),
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Deckle: $('#txtDec').val(),
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainerSec').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('Mill').value = rows[0].data["Mill"];
        document.getElementById('Fk_IndentVal').value = rows[0].data["MatIndent"];
        document.getElementById('Shade').value = rows[0].data["Color"];
        SaveSearch();
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Rate").focus();
    });



}


function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        //alert(curEdit);
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    //$('#txtFk_Material').wgReferenceField({
    //    keyProperty: "Fk_Material",
    //    displayProperty: "Name",
    //    loadPath: "Material/Load",
    //    viewModel: viewModel
    //});

    //$('#Fk_Indent').wgReferenceField({
    //    keyProperty: "Fk_Indent",
    //    displayProperty: "Fk_Indent",
    //    loadPath: "MaterialIndent/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        //  $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
        //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}
function setUpIndentSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader1").text("Indent Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Indent List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialIndent/MaterialIndentPOList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true
            },

            MaterialIndentDate: {
                title: 'Indent Date'
            },
            //CustomerName=p.CustomerName,
            //MaterialName=p.Name,
            //CustomerName: {
            //    title: 'CustomerName'
            //},
            MaterialName: {
                title: 'MaterialName'
            },
            Color: {
                title: 'Shade'
            },
            //Mill: {
            //    title: 'Mill'
            //},
            Quantity: {
                title: 'Ind.Qty'
            },
            PendingQty: {
                title: 'PendingQty'
            },
            Pk_MaterialOrderDetailsId: {
                title: 'Pk_MaterialOrderDetailsId',
                list: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val(),
            Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_Indent').value = rows[0].keyValue;
        IndentNo = rows[0].keyValue;

        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}


function CalcAmt() {
    //  curViewModel = viewObject.viewModel;
    if (objContextEdit == false) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));


        }

        else {
        }
    } else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
        }
        else {
        }



    }
}

function radioClass1(intval) {
    if (intval == 1) {


        document.getElementById('TaxType').value = "VAT";
        if (FORM_CT3.checked == true)
        { document.getElementById('ED').value = Number($('#GrandTotal').val()); }
        else {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
        }

        FORM_CT3.style.visibility = 'visible';
        FORM_H.style.visibility = 'visible';

        //    fct3.style.visibility = 'hidden';
        fct3.style.visibility = 'visible';
        fh.style.visibility = 'visible';

        if (Number($('#ED').val()) > 0) {
            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
            //  NETVALUE
        }

    }
    else if (intval == 2) {

        document.getElementById('TaxType').value = "CST";

        FORM_H.style.visibility = 'hidden';

        fh.style.visibility = 'hidden';


        if (Number($('#ED').val()) > 0) {

            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
        }

    }


}

function SaveSearch() {
    GSM = document.getElementById('txtMaterial').value;
    BF = document.getElementById('txtBF').value;
    Dec = document.getElementById('txtDec').value;
}