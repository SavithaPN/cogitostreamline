﻿var ssum = 0;
var samt = 0;
function IssuedMaterial() {
    this.data = new Object();
    this.data["Name"] = "";
    this.data["Fk_Material"] = "";
    this.data["Pk_PODet"] = "";
    this.data["Quantity"] = "";
    this.data["Rate"] = "";
    this.data["Amount"] = "";
    this.data["Color"] = "";
    this.data["Mill"] = "";
    this.data["Fk_IndentVal"] = "";
    this.data["HSNCode"] = "";
    this.data["MaterialName"] = "";


    this.updateValues = function () {
        this.data["Name"] = $('#MaterialName').val();
        this.data["Fk_Material"] = $('#txtFk_Material').val();
        this.data["Quantity"] = $('#Quantity').val();
        this.data["Amount"] = $('#Amount').val();
        this.data["Rate"] = $('#Rate').val();
        this.data["Color"] = $('#Shade').val();
        this.data["Mill"] = $('#Mill').val();
        this.data["HSNCode"] = $('#HSNCode').val();
        this.data["Fk_IndentVal"] = $('#Fk_IndentVal').val();

        document.getElementById("GrandTotal").value = Number($('#GrandTotal').val()) + Number($('#Amount').val());
       
    };
    //oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount);
    this.load = function (Pk_PODet, Name, Fk_Material, Quantity, Rate, Amount, Color, Mill, Fk_IndentVal, HSNCode) {
        this.data["Pk_PODet"] = Pk_PODet;
        this.data["Name"] = Name;
        this.data["Fk_Material"] = Fk_Material;
        this.data["Quantity"] =Quantity;
        this.data["Rate"] = Rate;
        this.data["Amount"] = Amount;
        this.data["Color"] = Color;
        this.data["Mill"] = Mill;
        this.data["Fk_IndentVal"] = Fk_IndentVal;
        this.data["HSNCode"] = HSNCode;
//        document.getElementById("GrandTotal").value = Number($('#GrandTotal').val()) + Number($('#Amount').val());
        //document.getElementById("GrandTot").value = document.getElementById("Amount").value;
    };
}
