﻿//Name          : City Operations ----javascript files
//Description   : Contains the  city Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckcityDuplicate(duplication checking) .Loading of state combo
            // based on country selection
//Author        : Nagesh
//Date 	        : 07/08/2015
//Crh Number    : SL0012
//Modifications : 

var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'City List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/City/CityListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },

        fields: {
            Pk_CityID: {
                title: 'Id',
                key: true

            },
            Country: {
                title: 'Country',
                width: '25%'
            },
            State: {
                title: 'State',
                width: '25%'
            },
            CityName: {
                title: 'City',
                width: '25%'
            }

        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_CityID: $('#TxtCityID').val(),
            CityName: $('#TxtCityName').val()
        });
    });
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("State", "getState", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("State", "getState", null);
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Countrys", "getCountry", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Countrys", "getCountry", null);
}


function CheckCityDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Country", $('#Country').val());
    _comLayer.parameters.add("Fk_StateID", $('#Fk_StateID').val());
    _comLayer.parameters.add("CityName", $('#CityName').val());
    //sPath = _comLayer.buildURL("RawProduct", null, "BindRawProduct");
    oResult = _comLayer.executeSyncAction("City/CityDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "City Already added";

    //return true;
}


function afterNewShow(viewObject) {
    $('#Country').change(function () {
        if ($('#Country').val() != '') {
            loadComboState('#Fk_StateID', $('#Country').val());
            $('#Fk_StateID').change();
        }
    });
}



function loadComboState(combo, parent) {
    var oResult = getStateComboValues(parent);
    $(combo).empty();

    var option = $("<option value=''>Select</option>");
    $(combo).append(option);

    if (oResult.Success) {
        var i = 0;

        while (oResult.Records[i]) {

            var option = $("<option value='" + oResult.Records[i].Pk_StateID + "'>" + oResult.Records[i].StateName + "</option>");
            $(combo).append(option);
            i++;
        }
    }
}

function getStateComboValues(parent) {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("CountryId", parent);

    var sPath = _comLayer.buildURL("State", null, "StateListByCountry");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    var oResultObject = oResult;

    return oResultObject;
}

