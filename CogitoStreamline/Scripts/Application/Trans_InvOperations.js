﻿var TransID;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Trans.Inv List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/TransInv/TransInvListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
       
            Pk_ID: {
                title: 'Id',
                key: true,
            },
            Invno: {
                title: 'Invno'
            },
            InvDate: {
                title: 'InvDate'
            },

            Amount: {
                title: 'Amount'
            },
            VehicleNo: {
                title: 'VehicleNo'
            },
            Fk_Transporter: {
                title: 'Transporter'
            },
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ID: $('#TxtPkId').val(),
            VehicleNo: $('#TxtVehicleNo').val(),
            Invno: $('#TxtInvno').val()
            
        });

    });
    $('#TxtPkId').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_ID: $('#TxtPkId').val(),
                VehicleNo: $('#TxtVehicleNo').val(),
                Invno: $('#TxtInvno').val()
            });
        }

    });

    $('#TxtVehicleNo').keypress(function (e) {
        if (e.KeyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_ID: $('#TxtPkId').val(),
                VehicleNo: $('#TxtVehicleNo').val(),
                Invno: $('#TxtInvno').val()
            });
        }

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });



    $("#searchCustomerDialog").width(1000);
    $("#searchCustomerDialog").css("top", "50px");
    $("#searchCustomerDialog").css("left", "300px");
    setUpTransporterSearch();



}
function setUpTransporterSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='TxtPkId' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='TxtName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
 //   var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
   // $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Transporter List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Transporter/TransporterListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {
            Pk_ID: {
                title: 'Id',
                key: true,
            },
            TransporterName: {
                title: 'Name'
            },
          LLine: {
                title: 'Land Line'
            },
            MobileNo: {
                title: 'Mobile'
            },
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_ID: $('#TxtPkId').val(),
            TransporterName: $('#TxtName').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_ID: $('#TxtPkId').val(),
            TransporterName: $('#TxtName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#txtTransporter').wgReferenceField("setData", rows[0].keyValue);
        TransID = rows[0].keyValue;
        $("#searchCustomerDialog").modal("hide");
    });

}

function ReferenceFieldNotInitilized(viewModel) {

    $('#txtTransporter').wgReferenceField({
        keyProperty: "Fk_Transporter",
        displayProperty: "TransporterName",
        loadPath: "Transporter/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {

        //$('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_OrderNo"]);
        $('#txtTransporter').wgReferenceField("setData", viewModel.data["Fk_Transporter"]);
    }

}


function afterNewShow(viewObject) {
  
    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();


    $('#dtDate').datepicker({ autoclose: true });
    curViewModel = viewObject.viewModel;


    $('#cmdTransporterSearch').click(function (e) {
        e.preventDefault();
        $("#searchCustomerDialog").modal("show");
    });




    

 
}
