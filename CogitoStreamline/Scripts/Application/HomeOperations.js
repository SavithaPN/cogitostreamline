﻿var data1 = [];

$(document).ready(function () {
    $("#search-btn").click(function (e) {
        e.preventDefault();
        _comLayer.parameters.clear();
        _comLayer.parameters.add("Search", $('#txtMainSearch').val());
        var oResultMenu = _comLayer.executeSyncAction("/Dashboard/GetModulesBySearch", _comLayer.parameters);
        $('#dlgModules').empty();

        $('#modulesList').text("List of Modules for " + $('#txtMainSearch').val());

        while (oResultMenu[i]) {

            var a = $("<a title='" + oResultMenu[i].ActivityName + "'  href=" + oResultMenu[i].Link + "></a>");
            var ix = $("<i " + oResultMenu[i].Image + "></i>");
            var lbl = $("<label> " + oResultMenu[i].ActivityName + "</label>");

            a.append(ix);
            a.append($("</br>"));
            a.append(lbl);

            $('#dlgModules').append(a);

            i++;
        }
        $('#moduleDialog').modal('show');
    });
    _comLayer.parameters.clear();
    var oResultSalesStatus = _comLayer.executeSyncAction("/Dashboard/MonthlySalesStatus", _comLayer.parameters);
    if (oResultSalesStatus.data[0] == "Improve the Sales" && $("#sessionInput").val()==2) {
        var sMonthlySalesStatus = "Improve the Sales";
        alertify.error(sMonthlySalesStatus);
    }
    LowStock(0);
    TommorowDeliveries(0);
    IndentApproval(0);
    EnquiryApproval(0);
    InwardStock(0);

    $("#cmdSignOut").click(function (e) {
        e.preventDefault();
        document.location = "Login";
    });

    $("#divMinStockValueViewAll").click(function (e) {
        e.preventDefault();
        LowStock(1);
    });

    $("#divDeliveryViewAll").click(function (e) {
        e.preventDefault();
        TommorowDeliveries(1);
    });

    $("#vwIndentApproval").click(function (e) {
        e.preventDefault();
        IndentApproval(1);
    });

    $("#vwEnquiryApproval").click(function (e) {
        e.preventDefault();
        EnquiryApproval(1);
    });

    $("#vwInwardStock").click(function (e) {
        e.preventDefault();
        InwardStock(1);
    });
    _util.render_gauge(document.getElementById("power-gauge"), "/Dashboard/OnTimeDeliveryPercentage");
    _util.render_gauge(document.getElementById("power-gauge1"), "/Dashboard/OnTimeDeliveryPercentage");

    //Quaterly Customer Details
    _comLayer.parameters.clear();
    var today = new Date();
    var y1 = String(Number(today.getFullYear()) - 2);
    var y2 = String(Number(today.getFullYear()) - 1);
    var y3 = String(today.getFullYear());

    var oResultCustYear = _comLayer.executeSyncAction("/Dashboard/QuaterlyCustomersYear", _comLayer.parameters);

    var data = [[y1, oResultCustYear.data[0]], [y2, oResultCustYear.data[1]], [y3, oResultCustYear.data[2]]];

    var oResultCust = _comLayer.executeSyncAction("/Dashboard/QuaterlyCustomers", _comLayer.parameters);

    var subData1 = [['Q1', oResultCust.data[0]], ['Q2', oResultCust.data[1]], ['Q3', oResultCust.data[2]], ['Q4', oResultCust.data[3]]];
    var subData2 = [['Q1', oResultCust.data[4]], ['Q2', oResultCust.data[5]], ['Q3', oResultCust.data[6]], ['Q4', oResultCust.data[7]]];
    var subData3 = [['Q1', oResultCust.data[8]], ['Q2', oResultCust.data[9]], ['Q3', oResultCust.data[10]], ['Q4', oResultCust.data[11]]];

    var subData = [subData1, subData2, subData3];

    function initChart() {
        $('#jqChart').jqChart({
            title: 'Customers per Year',
            animation: { duration: 1 },
            series: [
                    {
                        title: '',
                        type: 'column',
                        data: data,
                        cursor: 'pointer'
                    }
            ]
        });
    }

    initChart();

    $('#jqChart').bind('dataPointMouseDown', function (event, data) {

        var title = $('#jqChart').jqChart('option', 'title');
        if (title.text != 'Customers per Year') {
            return;
        }

        var newData = subData[data.index];

        $('#jqChart').jqChart({
            title: 'Customers per Quarter',
            animation: { duration: 1 },
            series: [
                    {
                        title: '',
                        type: 'column',
                        data: newData
                    }
            ]
        });

    });

    $('#jqChart').bind("contextmenu", function (e) {

        var title = $('#jqChart').jqChart('option', 'title');
        if (title.text == 'Customers per Year') {
            return;
        }

        initChart();

        return false;
    });


    //_util.render_basic_pie(document.getElementById("divGraph"),"/Home/getData");
    //_util.render_basic_bars(document.getElementById("divGraph"), "/Home/getData");
    // _util.render_monthly_line(document.getElementById("divGraph"), "/Dashboard/MonthWiseEnquiryOrders");


    $(".column").sortable({
        connectWith: ".column",
        handle: ".portlet-header",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all"
    });

    $(".portlet")
      .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
      .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

    $(".portlet-toggle").click(function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".portlet").find(".portlet-content").toggle();
    });

    // Vendor Rejected

    _comLayer.parameters.clear();
    var oResult1 = _comLayer.executeSyncAction("/Dashboard/IndentRejectedReport", _comLayer.parameters);
    var i;
    for (i = 0; i < oResult1.Count; i++) {

        yValue = Math.round(oResult1.data[i] * 10);
        data1.push(yValue);
    }

    var background = {
        type: 'linearGradient',
        x0: 0,
        y0: 0,
        x1: 0,
        y1: 1,
        colorStops: [{ offset: 0, color: '#d2e6c9' },
                     { offset: 1, color: 'white' }]
    };

    $('#VjqChart').jqChart({
        title: { text: 'Vendor Wise Rejected' },
        border: { strokeStyle: '#6ba851' },
        background: background,
        animation: { duration: 1 },
        shadows: {
            enabled: true
        },
        axes: [
            {
                type: 'linear',
                location: 'left',
                minimum: 10,
                maximum: 100,
                interval: 10
            }
        ],
        series: [
            {
                title: '',
                type: 'column',
                data: data1
            }
        ]
    });


});



function LowStock(s) {
    //  Stock Reached to Minimum Values 
    $("#divMinStockValue").empty();
    $("#totstck").empty();
    $("#totalMinStockValues").empty();

    _comLayer.parameters.clear();
    var oResultMinStock = _comLayer.executeSyncAction("/Dashboard/MinLevelStocks", _comLayer.parameters);
    var j = 0;
    if (s == 0)
        j = 5;
    else if (s == 1)
        j = oResultMinStock.Count;
    if (oResultMinStock.Count > 0) {
        var rowMain = $("<ul></ul>");
        for (i = 0; i < j; i++) {


            if (i < oResultMinStock.Count) {
                var colMain = $("<li >" + oResultMinStock.data[i] + "</li>");

                rowMain.append(colMain);
            }
        }
        $("#divMinStockValue").append(rowMain);
        $("#totstck").append(oResultMinStock.Count);
        $("#totalMinStockValues").append(" you have " + oResultMinStock.Count + " Items Under Min Value");
        if (oResultMinStock.Count > 5 && s == 0) {
            $("#divMinStockValueViewAll").show();
        }
        else
            $("#divMinStockValueViewAll").hide();

        $("#divMinStockValueStatus").show();
    }
    else {
        $("#divMinStockValueStatus").hide();
    }

}

function TommorowDeliveries(s) {
    //  Tommorow Delivery Orders

    $("#divDelivery").empty();
    $("#totDelivery").empty();
    $("#totalDeliverys").empty();


    _comLayer.parameters.clear();
    var oResultOrderDeliveries = _comLayer.executeSyncAction("/Dashboard/OrderDeliveries", _comLayer.parameters);
    var j = 0;
    if (s == 0)
        j = 5;
    else if (s == 1)
        j = oResultOrderDeliveries.Count;
    if (oResultOrderDeliveries.Count > 0) {
        var rowMain = $("<ul></ul>");
        for (i = 0; i < j; i++) {
            if (i < oResultOrderDeliveries.Count) {


                var colMain = $("<li>" + oResultOrderDeliveries.data[i] + "</li>");

                rowMain.append(colMain);
            }
        }

        $("#divDeliveryViewAll").click(function () {
            document.location = "Order";
        });
        $("#divDelivery").append(rowMain);
        $("#totDelivery").append(oResultOrderDeliveries.Count);
        $("#totalDeliverys").append(" you have " + oResultOrderDeliveries.Count + " Orders To Delivery");
        if (oResultOrderDeliveries.Count > 5 && s == 0) {
            $("#divDeliveryViewAll").show();
        }
        //else
        //    $("#divDeliveryViewAll").hide();

        $("#divDeliveryStatus").show();
    }
    else {
        $("#divDeliveryStatus").hide();
    }
}

function IndentApproval(s) {
    //  Indent Approval
    $("#divIndentApproval").empty();
    $("#totIndentApproval").empty();
    $("#totalIndentApproval").empty();

    _comLayer.parameters.clear();
    var oResultIndentApproval = _comLayer.executeSyncAction("/Dashboard/IndentApproval", _comLayer.parameters);
    var j = 0;
    var $txt = null;
    if (s == 0)
        j = 5;
    else if (s == 1)
        j = oResultIndentApproval.Count;
    if (oResultIndentApproval.Count > 0) {
        var rowMain = $("<ul></ul>");
        //var rowMain = $("<tr id='saveBlock'></tr>");
        //var colMain = $("<td colspan='3' align='center'></td>");

        //rowMain.append(colMain);
        //$("#prtList").append(rowMain);

        //Create a row for column for total weight

        for (i = 0; i < j; i++) {
            if (i < oResultIndentApproval.Count) {
                //      var cmdCreate = $("<button type='submit' background-color='white';fore-color='black';  id='cmdCreate' onclick='window.location.href = /MaterialIndent//Load? + $.param({ Id:"+ oResultIndentApproval.data[i] + "})'>" + oResultIndentApproval.data[i] + "</button>");

                //    colMain.append(cmdCreate);
                //  rowMain.append(colMain);
                var colMain = $("<li>" + oResultIndentApproval.data[i] + "</li>");
                //$txt=$("<a>" + oResultIndentApproval.data[i] +"</a>");
                rowMain.append(colMain);
            }
        }

        $("#vwIndentApproval").click(function () {
            document.location = "MaterialIndent";
        });
        $("#divIndentApproval").append(rowMain);
        $("#totIndentApproval").append(oResultIndentApproval.Count);
        $("#totalIndentApproval").append(" you have " + oResultIndentApproval.Count + " Indent Approval ");

        if (oResultIndentApproval.Count > 5 && s == 0) {
            $("#vwIndentApproval").show();
        }
        //else
        //    $("#vwIndentApproval").hide();

        $("#divIndentApprovalStatus").show();
    }
    else {
        $("#divIndentApprovalStatus").hide();
    }
}


function EnquiryApproval(s) {
    //  Enquiry Approval
    $("#divEnquiryApproval").empty();
    $("#totEnquiryApproval").empty();
    $("#totalEnquiryApproval").empty();

    _comLayer.parameters.clear();
    var oResultEnquiryApproval = _comLayer.executeSyncAction("/Dashboard/EnquiryApproval", _comLayer.parameters);
    var j = 0;
    if (s == 0)
        j = 5;
    else if (s == 1)
        j = oResultEnquiryApproval.Count;
    if (oResultEnquiryApproval != null) {
        if (oResultEnquiryApproval.Count > 0) {
            var rowMain = $("<ul></ul>");
            for (i = 0; i < j; i++) {
                if (i < oResultEnquiryApproval.Count) {

                    var colMain = $("<li>" + oResultEnquiryApproval.data[i] + "</li>");

                    rowMain.append(colMain);
                }
            }
            $("#vwEnquiryApproval").click(function () {
                document.location = "Enquiry";
            });
            $("#divEnquiryApproval").append(rowMain);
            $("#totEnquiryApproval").append(oResultEnquiryApproval.Count);
            $("#totalEnquiryApproval").append(" you have " + oResultEnquiryApproval.Count + " Enquiry Approval ");
            if (oResultEnquiryApproval.Count > 5 && s == 0) {
                $("#vwEnquiryApproval").show();
            }
            //else
            //    $("#vwEnquiryApproval").hide();

            $("#divEnquiryApprovalStatus").show();
        }
        else {
            $("#divEnquiryApprovalStatus").hide();
        }
    }
}
function InwardStock(s) {
    //Inward Stock
    $("#divInwardStock").empty();
    $("#totInwardStock").empty();
    $("#totalInwardStock").empty();

    _comLayer.parameters.clear();
    var oResultInwardStock = _comLayer.executeSyncAction("/Dashboard/InwardStock", _comLayer.parameters);
    var j = 0;
    if (s == 0)
        j = 5;
    else if (s == 1)
        if (oResultInwardStock != null) {
            j = oResultInwardStock.Count;
            if (oResultInwardStock.Count > 0) {
                var rowMain = $("<ul></ul>");
                for (i = 0; i < j; i++) {
                    if (i < oResultInwardStock.Count) {

                        var colMain = $("<li>" + oResultInwardStock.data[i] + "</li>");
                        // var colMain = $("<li>" + "<a href='#'>" + i + " " + "</a>" + oResultInwardStock.data[i] + "</li>");
                        rowMain.append(colMain);
                    }
                }
                $("#vwInwardStock").click(function () {
                    document.location = "MaterialInward";
                });
                $("#divInwardStock").append(rowMain);


                $("#divInwardStock li").click(function () {
                    var selected = $(this).text();
                    var str = selected;
                    var n = str.indexOf("-");
                    var substring = str.substr(0, n);
                    //$.trim(str)
                    INWDNO = $.trim(substring);
                    _comLayer.parameters.add("Id", INWDNO);
                    _comLayer.executeSyncAction("MaterialInward/Load", _comLayer.parameters);



                    var sPath = _comLayer.buildURL(this.modelName, "MaterialInward", "Load");
                    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

                    var oResultObject = oResult;

                    return oResultObject;


                    //_util.showSuccess("", function () { document.location.reload(); });
                    ////_util.fireSubscribedHooks(this.hookSubscribers, 'afterModelDelete', { result: oResult });

                    //_util.fireSubscribedHooks(this.hookSubscribers, 'onEditRecord', { data: record, key: Pk_Inward, keyValue: INWDNO });

                    window.open(sPath, '_blank ', 'width=960,height=576');
                });

                $("#totInwardStock").append(oResultInwardStock.Count);
                $("#totalInwardStock").append(" Latest Inwarded Stock ");
                $("#divInwardStockStatus").show();
                if (oResultInwardStock.Count > 5 && s == 0) {
                    $("#vwInwardStock").show();
                }

            }

            else {
                $("#divInwardStockStatus").hide();
            }
        }
}