﻿function DeliveryScheduleItem() {
    this.data = new Object();
    this.data["Pk_DeliverySechedule"] = "";
    this.data["DeliveryDate"] = "";
    this.data["Quantity"] = "";
    this.data["Comments"] = "";
    this.data["DeliveryCompleted"] = null;
    this.data["DeliveredDate"] = "";
    this.data["Fk_EnquiryChild"] = document.getElementById('pk').value;

    this.updateValues = function () {
        this.data["DeliveryDate"] = $('#dtDeliveryDate').val();
        this.data["Quantity"] = $('#Quantity').val();
        this.data["Comments"] = $('#Comments').val();
    };

    this.load = function (Pk_DeliverySchedule, DeliveryDate, Quantity, Comments, DeliveryCompleted, DeliveredDate, Fk_Box, Fk_EnquiryChild) {
        this.data["Pk_DeliverySechedule"] = Pk_DeliverySchedule;
        this.data["DeliveryDate"] = DeliveryDate;
        this.data["Quantity"] = Quantity;
        this.data["Comments"] = Comments;
        this.data["DeliveryCompleted"] = DeliveryCompleted;
        this.data["DeliveredDate"] = DeliveredDate;
        this.data["Fk_Box"] = Fk_Box;
        this.data["Fk_EnquiryChild"] = Fk_EnquiryChild;
    };
}