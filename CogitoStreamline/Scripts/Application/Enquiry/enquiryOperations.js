﻿
var curViewModel = null;
var PkEnqChildId = 0;
var BoxId = 0;
var BType = 0;
var CustomerID = 0;
var IsEdit=false;
var CustIDVal=0;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Enquiry List',
        paging: true,
        pageSize: 15,      
        actions: {
            listAction: '/Enquiry/EnquiryListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            CustDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Customer Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Customer Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerListByFiter?Pk_Customer=' + data.record.Fk_Customer
                                        },
                                        fields: {

                                            Pk_Customer: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            CustomerName: {
                                                title: 'CustomerName',
                                                width: '15%'
                                            },
                                            CustomerAddress: {
                                                title: 'CustomerAddress'
                                            },
                                            City: {
                                                title: 'City',
                                                width: '10%'
                                            },
                                            State: {
                                                title: 'State',
                                                width: '10%'
                                            },
                                            Country: {
                                                title: 'Country',
                                                width: '10%'
                                            },
                                            PinCode: {
                                                title: 'PinCode',
                                                width: '10%'
                                            },
                                            CustomerContact: {
                                                title: 'Mobile',
                                                key: false
                                            },
                                            LandLine: {
                                                title: 'LandLIne'
                                            },
                                            Email: {
                                                title: 'Email',
                                                width: '10%'
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Contacts'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Contacts List',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                                                        },
                                                                        fields: {
                                                                            Pk_CustomerContacts: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            ContactPersonName: {
                                                                                title: 'Person Name',
                                                                                key: false
                                                                            },
                                                                            ContactPersonDesignation: {
                                                                                title: 'Designation'

                                                                            },
                                                                            ContactPersonNumber: {
                                                                                title: 'Contact Number',
                                                                                width: '2%'
                                                                            },
                                                                            ContactPersonEmailId: {
                                                                                title: 'Email',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            Pk_Enquiry: {
                title: 'Enquiry Id',
                key: true,
                width: '4%'
            },
            Date: {
                title: 'Enquiry Date',
                width: '4%'
            },
            Fk_Customer: {
                title: 'CustomerId',
                list: false
            },
            Customer: {
                title: 'Customer Name',
                edit: false
            },
            CommunicationType: {
                title: 'Communication Type',
                width: '5%'
            },
            //Description: {
            //    title: 'Description'
            //},

            //SpecialRequirements: {
            //    title: 'Special Req'
            //},
            //EqComments: {
            //    title: 'Comments'
            //},
            Fk_Status: {
                title: 'Status',
                width: '5%'
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_EQID = row.record.Pk_Enquiry;
                        _comLayer.parameters.add("Pk_EQID", Pk_EQID);
                        _comLayer.executeSyncAction("Enquiry/EnqRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Enquiry" + Pk_EQID + ".pdf"
                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/Enquiry" + Pk_EQID + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');

                            return true;
                        }


                        /////////////////////////
                     

                    });
                    return button;
                }
            },
            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'PO List',
                                        actions: {
                                            listAction: '/Enquiry/EnquiryChildDetails?Pk_Enquiry=' + data.record.Pk_Enquiry

                                        },
                                        fields: {

                                            Pk_EnquiryChild: {
                                                title: 'Pk_EnquiryChild',
                                                list:false
                                            },
                                            Fk_BoxID: {
                                                title: 'BoxID'
                                            },
                                            Name: {
                                                title: 'BoxName'
                                            },
                                            Quantity: {
                                                title: 'Quantity',
                                                key: true,
                                            },

                                            Estimate: {
                                                title: 'Estimate',
                                                width: '2%',
                                                //display: function (data) {
                                                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                                                display: function (row) {
                                                    //var button = $("<i class='icon-printer'></i>");
                                                    var button = $("<button class='bnt'>Estimate</button>");
                                                    $(button).click(function () {

                                                        PkEnqChildId = row.record.Pk_EnquiryChild;
                                                        //document.getElementById('pk').value = PkEnqChildId;
                                                        _comLayer.parameters.add("Pk_EnquiryChild", PkEnqChildId);

                                                        var oResult1 = _comLayer.executeSyncAction("Enquiry/CheckEstimate", _comLayer.parameters);


                                                        if (oResult1.data.Estimated) {
                                                           
                                                            alert('Already Estimated!!');
                                                            BoxId = row.record.Fk_BoxID;
                                                            window.open("/Estimation");
                                                              }
                                                        else {
                                                            BoxId = row.record.Fk_BoxID;
                                                           
                                                           // window.open("/EnqEstimate?Box=" + BoxId + ", EnqChild=" + PkEnqChildId + ", BT=" + BType);
                                                            window.open("/EnqEstimate?Box=" + BoxId + ", EnqChild=" + PkEnqChildId);
                                                        }

                                                    });
                                                    return button;
                                                }

                                            }  /////estimate -- print close
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },

            ///////////////////////////////////////////////
        }

    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Enquiry: $('#txtSearchEnquiryId').val(),
            Customer: $('#txtSearchCustomer').val(),
            //CommunicationType: $('#txtSearchCommunicationType').val(),
            EnquiryFromDate: $('#txtFromDate').val(),
            EnquiryToDate: $('#TxtToDate').val()
        });

    });


    //$('#txtSearchEnquiryDate').change(function (e) {
    //    e.preventDefault();
    //    $('#MainSearchContainer').jtable('load', {
    //        EnquiryDate: $('#txtSearchEnquiryDate').val()
    //    });
    //});
    $('#txtFromDate').datepicker({
        autoclose: true
    });
    $('#TxtToDate').datepicker({
        autoclose: true
    });
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Communication", "getCommunicationType", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Communication", "getCommunicationType", null);

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Customers", "getCustomers", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Customers", "getCustomers", null);

    $('#txtSearchEnquiryId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Enquiry: $('#txtSearchEnquiryId').val()
            });
        }
    });

    $('#txtSearchCustomer').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Customer: $('#txtSearchCustomer').val()
            });
        }
    });

    $('#txtSearchCommunicationType').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                CommunicationType: $('#txtSearchCommunicationType').val()
            });
        }
    });

    $('#dtDeliveryDate').datepicker({ autoclose: true });

    $("#searchDialog").width(1000);
    $("#searchDialog").css("top", "50px");
    $("#searchDialog").css("left", "300px");

    $("#searchCustomerDialog").width(1000);
    $("#searchCustomerDialog").css("top", "50px");
    $("#searchCustomerDialog").css("left", "300px");


    setUpEnquirySearch();
    
    setUpCustomerSearch();
}


function beforeModelSaveEx() {
    //$(".ui-accordion-content").show();
    _util.setDivPosition("tbSpcitication", "none");
    _util.setDivPosition("tbDeliverySchedule", "none");
    _util.setDivPosition("tbDocument", "none");
    _util.setDivPosition("tbCustomerDetails", "block");

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Date"] = $('#dtDate').val();
    viewModel.data["Ddate"] = $('#dtDeliveryDate').val();


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Date"] = $('#dtDate').val();
    viewModel1.data["Ddate"] = $('#dtDeliveryDate').val();
    
}
function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    CaluculateWeight();
    $('#dtDeliveryDate').datepicker({ autoclose: true });
    $('#PrintingRequired').change(function (e) {
        e.preventDefault();
        if ($('#PrintingRequired').is(':checked') == true) {
            $('#PrintingDetailsInfo').show();
        }
        else if ($('#PrintingRequired').is(':checked') == false) {
            $('#PrintingDetailsInfo').hide();
        }
    });

    $('#cmdNewCustomer').click(function (e) {
        e.preventDefault();
        window.open("/Customer");
    });

    $('#cmdNext').click(function (e) {
        e.preventDefault();
        $('#MyTab a[href="#tbSpecification"]').tab('show');
        $('#cmdCreate').show();
    })

    //if (Corr == "Yes")
    //{ $("#chkCorr").attr('checked', true); }
    //else
    //{ $("#chkCorr").attr('checked', false); }


  

    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
       
        $("#searchCustomerDialog").modal("show");
       
    });

    $('#dtDate').datepicker({ autoclose: true });
    $('#dtDeliveryDate').datepicker({ autoclose: true });
    document.getElementById('Joints').value = "Stitched";
    $("#Fk_Stitched").prop("checked", true);

    $("#dropzone").wgUpload({
        wgTag: "Enquiry",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });

    curViewModel = viewObject.viewModel;

    $('#divDeliveryschedule').jtable({
        title: 'Delivery Schedule',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Enquiry/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            DeliveryDate: {
                title: 'Date',
                key: false
            },
            Quantity: {
                title: 'Quantity'

            },
            Comments: {
                title: 'Comments',
                width: '20%'
            },
            Fk_Box: {
                title: 'Fk_Box',
                width: '20%'
            }

        }
    });

    //configureOne2Many("#cmdNewDelivery", '#divDeliveryschedule', "#cmdAddToDelivery", viewObject, "deliverySchedules", "Enquiry", "NOTUSED", function () { return new DeliveryScheduleItem() }, "divDelivery");
    configureOne2Many("#cmdNewDelivery", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "Enquiry", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() }, "divDelivery");

    $('#cmdSearchOrderdetails').click(function (e) {
        e.preventDefault();
        // setUpEnquirySearch();
        $("#searchDialog").modal("show");
    });

    $('#tlbMaterials').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Enquiry/Bounce',
            //deleteAction: '',
            //updateAction: ''

        },
        fields: {
            slno: {
                title: 'slno',
                key: false,
                list: false
            },
            Fk_EnquiryId: {
                title: 'Fk_EnquiryId',
                key: true,
                list: false
            },
            Fk_BoxID: {
                title: 'Fk_BoxID',
                key: true,
                list: false
            },
            Name: {
                title: 'Box Name'

            },
            Quantity: {
                title: 'Quantity',
                width: '1%'

            },

            ConvRate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            },
            Ddate: {
                title: 'Delv.Date',
                width: '2%'
            }
        },
    });

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterialBox", viewObject, "enquiryChilds", "Enquiry", "_AddMaterial", function () { return new IssuedMaterial() });
    $('#cmdCreate').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
        //$(this).tab('show')
        //alert($('.nav-tabs .active').text());
        //alert($('.nav-tabs .active > a').attr('href'));
        //alert($(this).attr("href"));
        if ($(this).attr("href") == "#tbCustomerDetails") {
            //$('input:submit').show();
            $('#cmdCreate').hide();
        }
        else if
            ($(this).attr("href") == "#tbSpecification") {
            $('#cmdCreate').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
        else if 
           ($(this).attr("href") == "#tbDocument") {
            $('#cmdCreate').show();
        }
    })


   

}

function afterEditShow(viewObject) {
    CaluculateWeight();
    IsEdit = true;
    var status = viewObject.viewModel.data["Fk_Status_Name"]();
   CustIDVal = viewObject.viewModel.data["Customer"]();
    if (status == "Completed") {

        $("#cmdSave").attr('disabled', true);
    }

    var Samp = viewObject.viewModel.data["SampleRecd"]();
    if (Samp == "Yes")
    { $("#chkSample").attr('checked', true); }
    else
    { $("#chkSample").attr('checked', false); }



    $('#cmdDetailedEstimation').click(function (e) {
        e.preventDefault();
    });


    $('#PrintingRequired').change(function (e) {
        e.preventDefault();
        if ($('#PrintingRequired').is(':checked') == true) {
            $('#PrintingDetailsInfo').show();
        }
        else if ($('#PrintingRequired').is(':checked') == false) {
            document.getElementById('PrintingDetails').value = '';
            $('#PrintingDetailsInfo').hide();
        }
    });


    $('#cmdSave').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
      
        if ($(this).attr("href") == "#tbCustomerDetails") {
            //$('input:submit').show();
            $('#cmdSave').hide();
        }
        else if
            ($(this).attr("href") == "#tbSpecification") {
            $('#cmdSave').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
        else if 
           ($(this).attr("href") == "#tbDocument") {
            $('#cmdSave').show();
        }
    })

    $('#cmdSearchOrderdetails').click(function (e) {
        e.preventDefault();
        // setUpEnquirySearch();
        $("#searchDialog").modal("show");
    });

    $('#cmdBoxMSearch').click(function (e) {
        e.preventDefault();
        setUpBoxSearch(viewObject);
        $("#searchBoxDialog").modal("show");
    });

    curViewModel = viewObject.viewModel;

    $('#dtDate').datepicker({ autoclose: true });
    $('#dtDeliveryDate').datepicker({ autoclose: true });


    $('#cmdNext').click(function (e) {
        e.preventDefault();
        $('#cmdCreate').show();
        $('#MyTab a[href="#tbSpecification"]').tab('show');
       
    })
    $('#cmdNext1').click(function (e) {
        e.preventDefault();
        $('#MyTab a[href="#tbDocument"]').tab('show');
        $('#cmdCreate').show();
    })

    $('#tlbMaterials').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Enquiry/Bounce',
            //deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: false,
                list: false
            },
            Pk_EnquiryChild: {
                title: 'Pk_EnquiryChild',
                key: true,
                list: false
            },
            Fk_BoxID: {
                title: 'Fk_BoxID',
                key: true,
                list: false
            },
            BType: {
                title: 'Type',
                key: true,
                list: false
            },

            Name: {
                title: 'Box Name'

            },
            Quantity: {
                title: 'Quantity',
                width: '1%'

            },

            ConvRate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            },
            Ddate: {
                title: 'Delv.Date',
                width: '2%'
            },
            Print: {
                title: 'Estimate',
                width: '2%',
                        //display: function (data) {
                        //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    //var button = $("<i class='icon-printer'></i>");
                    var button = $("<button class='bnt'>Estimate</button>");
                    $(button).click(function () {

                        PkEnqChildId = row.record.Pk_EnquiryChild;
                        document.getElementById('pk').value = PkEnqChildId;                    
                        _comLayer.parameters.add("Pk_EnquiryChild", PkEnqChildId);


                        BType = row.record.BType;
                       // window.open("/BoxSpec?BoxMaster=" + fkbox1 + ", Type=" + BType);
                        _comLayer.parameters.add("BT", BType);




                        var oResult1 = _comLayer.executeSyncAction("Enquiry/CheckEstimate", _comLayer.parameters);

                        
                        if (oResult1.data.Estimated)
                        {
                            //alert(oResult1.data.EstBoxChild);
                            // alert(oResult1.data.BoxEstimation);
                            // alert( oResult1.data.EnqChild);
                            alert('Already Estimated!!');
                            BoxId = row.record.Fk_BoxID;
                            window.open("/Estimation");
                            //window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);

                        }
                        else {
                            BoxId = row.record.Fk_BoxID;
                            document.getElementById('BoxID').value = BoxId;

                           // window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild=" + PkEnqChildId + ", BT=" + BType);     existing setup
                        window.open("/EnqEstimate?Box=" + BoxId + ", EnqChild=" + PkEnqChildId + ", BT=" + BType);   

                        }
                     
                    });
                    return button;
                }

            }

        }
    });

    var oSCuts = viewObject.viewModel.data.enquiryChild();
    viewObject.viewModel.data["enquiryChilds"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_EnquiryChild, oSCuts[i].Name, oSCuts[i].Fk_BoxID, oSCuts[i].Quantity, oSCuts[i].ConvRate, oSCuts[i].Amount, oSCuts[i].Ddate, oSCuts[i].DeliveryDate,oSCuts[i].BType);
        viewObject.viewModel.data["enquiryChilds"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterialBox", viewObject, "enquiryChilds", "Enquiry", "_AddMaterial", function () { return new IssuedMaterial() });

    
    //$('#wfTransDisplay').wgWorkFlowHistory({
    //    wgTag: "Enquiry",
    //    viewModel: viewObject.viewModel
    //});
    $("#dropzone").wgUpload({
        wgTag: "Enquiry",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });

    //$('#wfButtons').wgWorkFlowButtons({
    //    wgTag: "Enquiry",
    //    viewModel: viewObject.viewModel,
    //    stateField: "Fk_Status",
    //    belongsTo: "Enquiry",
    //    pkField: "Pk_Enquiry"
    //});


  
    var status = viewObject.viewModel.data["Fk_Status_Name"]();

    if (status == "Closed") {

        $(".modelControl").attr('disabled', 'disabled');

        $("#cmdSave").attr('disabled', 'disabled');
    }


    //$('#divDeliveryschedule').jtable({
    //    title: 'Delivery Schedule',
    //    paging: true,
    //    pageSize: 10,
    //    defaultSorting: 'Name ASC',
    //    actions: {
    //        listAction: '/Enquiry/Bounce',
    //        updateAction: '',
    //        deleteAction: ''
    //    },
    //    fields: {
    //        slno: {
    //            title: 'slno',
    //            key: false,
    //            list: false
    //        },
    //        Pk_DeliverySechedule: {
    //            title: 'Delivery ID',
    //            key: true,
    //            list: true
    //        },
    //        DeliveryDate: {
    //            title: 'Delivery Date',
    //            key: false
    //        },
    //        Quantity: {
    //            title: 'Quantity'

    //        },
    //        Comments: {
    //            title: 'Comments',
    //            width: '20%'
    //        },
    //        DeliveredDate: {
    //            title: 'Delivered Date'
    //        },
    //        Fk_EnquiryChild: {
    //            title: 'Fk_EnquiryChild',
    //            key: true,
    //            list: true
    //        },
    //        Fk_Box: {
    //            title: 'Fk_Box'
    //        }

    //    }
    //});

    //var oSCuts = viewObject.viewModel.data.deliverySchedule();
    //viewObject.viewModel.data["deliverySchedules"] = ko.observableArray();
    //var i = 0;

    //while (oSCuts[i]) {
    //    var oCut = new DeliveryScheduleItem();
    //    oCut.load(oSCuts[i].Pk_DeliverySchedule, oSCuts[i].DeliveryDate, oSCuts[i].Quantity, oSCuts[i].Comments, oSCuts[i].DeliveryCompleted, oSCuts[i].DeliveredDate, oSCuts[i].Fk_Box, oSCuts[i].Fk_EnquiryChild);
    //    viewObject.viewModel.data["deliverySchedules"].push(oCut);
    //    i++;
    //}

    
    //configureOne2Many("#cmdNewDelivery", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "Enquiry", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() }, "divDelivery");

   
}

function CalcAmt() {

    if (objContextEdit == false) {
        if (Number($('#QuantityBox').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#QuantityBox').val()) * Number($('#Rate').val()));


        }

        else {
        }
    } else if (objContextEdit == true) {
        if (Number($('#QuantityBox').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#QuantityBox').val()) * Number($('#Rate').val()));
        }
        else {
        }



    }

}

function setUpEnquirySearch(viewObject) {

    $('#EnquiryOrderSearchContainer').jtable({
        title: 'Enquiry List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting,
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdSpecificationDone').click();
            });
        },

        fields: {
            Fk_Enquiry: {
                title: 'Enquiry Id',
                key: true,
                width: '3%'
            },
            EnquiryDate: {
                title: 'Enquiry Date',
                width: '4%'
            },
            CustomerName: {
                title: 'Customer Name'
            },
            ProductName: {
                title: 'Product Name'
            },
            Pk_Order: {
                title: 'Order Number',
                width: '4%'
            },
            Quantity: {
                title: 'Quantity',
                width: '3%'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#EnquiryOrderSearchContainer').jtable('load', {
            CustomerName: $('#txtSpecificationCustomer').val(),
            Pk_Order: $('#txtSpecificationOrderId').val(),
            Fk_EnquiryId: $('#txtSpecificationEnquiryId').val(),
            FromEnquiryDate: $('#txtSpecificationFromEnquiryDate').val(),
            ToEnquiryDate: $('#txtSpecificationToEnquiryDate').val()
        });
    });
    $('#LoadRecordsButton').click();

    //Re-load records when user click 'load records' button.
    $('#cmdSpecificationsSearch').click(function (e) {
        e.preventDefault();
        $('#EnquiryOrderSearchContainer').jtable('load', {
            CustomerName: $('#txtSpecificationCustomer').val(),

        });
    });

    $('#txtSpecificationFromEnquiryDate').change(function (e) {
        e.preventDefault();
        $('#EnquiryOrderSearchContainer').jtable('load', {
            FromEnquiryDate: $('#txtSpecificationFromEnquiryDate').val(),
            ToEnquiryDate: $('#txtSpecificationToEnquiryDate').val()
        });
    });

    $('#txtSpecificationToEnquiryDate').change(function (e) {
        e.preventDefault();
        $('#EnquiryOrderSearchContainer').jtable('load', {
            FromEnquiryDate: $('#txtSpecificationFromEnquiryDate').val(),
            ToEnquiryDate: $('#txtSpecificationToEnquiryDate').val()
        });
    });

    $('#txtSpecificationCustomer').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#EnquiryOrderSearchContainer').jtable('load', {
                CustomerName: $('#txtSpecificationCustomer').val()
            });
        }
    });

    $('#txtSpecificationEnquiryId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#EnquiryOrderSearchContainer').jtable('load', {
                Fk_EnquiryId: $('#txtSpecificationEnquiryId').val()
            });
        }
    });

    $('#txtSpecificationOrderId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#EnquiryOrderSearchContainer').jtable('load', {
                Pk_Order: $('#txtSpecificationOrderId').val()
            });
        }
    });


}

function afterOneToManyDialogShow(property) {


    $('#dtDeliveryDate').datepicker({ autoclose: true });

    $('#cmdSearchMaterialsdetails').click(function (e) {

        e.preventDefault();
       // alert(document.getElementById('Customer').value);
        setUpBoxSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function setUpCustomerSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },

        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            CustomerAddress: {
                title: 'Address'
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            },
            Email: {
                title: 'Email'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#Customer').wgReferenceField("setData", rows[0].keyValue);

        CustomerID = rows[0].keyValue;

        $("#searchCustomerDialog").modal("hide");
        $("#ReferenceEnquiry").focus();
    });

}


function setUpBoxSearch(viewObject) {
    //Enquiry

     //cleanSearchDialog();
    if (IsEdit == true)
    {
        CustomerID = CustIDVal;
    }
    //Adding Search fields
    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldPartNo = "<input type='text' id='txtdlgPartNo' placeholder='PartNo' class='input-large search-query'/>";


    $('#MaterialSearchContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter?CustID=' + CustomerID
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {
            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            //PartNo: {
            //    title: 'PartNo',
            //    width: '25%'
            //},
            Name: {
                title: 'Name',
                width: '25%'
            },
           
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val()//,   

        });

    });
    
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val()//,
            //PartNo: $('#txtdlgPartNo').val()

        });
    });


    $('#cmdMaterialDone').click(function (e) {
        //e.preventDefault();
        //var rows = $('#MaterialSearchContainer').jtable('selectedRows');

        //$('#Fk_BoxID').wgReferenceField("setData", rows[0].data.Pk_BoxID);
        //$("#searchBoxDialog").modal("hide");

        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('ReturnableQty').value = rows[0].data["Quantity"];
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#QuantityBox").focus();
    });

}

function setUpEnquiryBoxSearch(data) {

    $('#EnquiryBoxContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Enquiry/EnquiryBoxDetails?Pk_Enquiry=' + data.record.Pk_Enquiry
        },
       
       

        fields: {
            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            //BoxType: {
            //    title: 'BoxType',
            //    width: '25%'
            //},
            PartNo: {
                title: 'PartNo',
                width: '25%'
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            //Description: {
            //    title: 'Description',
            //    width: '25%'
            //},
        }
    });

$('#LoadRecordsButton').click(function (e) {
    e.preventDefault();
    $('#EnquiryBoxContainer').jtable('load', {
        Pk_Enquiry: data.record.Pk_Enquiry
    });

});

//Load all records when page is first shown
$('#LoadRecordsButton').click();

}


function ReferenceFieldNotInitilized(viewModel) {

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }

    $('#Customer').wgReferenceField({
        keyProperty: "Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#Customer').wgReferenceField("setData", viewModel.data["Customer"]);
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
    }



}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $('#SearchContainer').empty();
   
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#searchBoxDialog').empty();
    $('#MainSearhContainer').append("<div id='searchBoxDialog'></div>");
}

function showOnlyOpen() {
    $('#MainSearchContainer').jtable('load', {
        State: $('#open').val()
    });
}

function showOnlyInProgress() {
    $('#MainSearchContainer').jtable('load', {
        State: $('#InProgress').val()
    });
}

function showOnlyCompleted() {
    $('#MainSearchContainer').jtable('load', {
        State: $('#Completed').val()
    });
}


function showOnlyCancelled() {
    $('#MainSearchContainer').jtable('load', {
        State: $('#Cancelled').val()
    });
}

function showOnlyClosed() {
    $('#MainSearchContainer').jtable('load', {
        State: $('#closed').val()
    });
}

function showAll() {
    $('#MainSearchContainer').jtable('load', {
        Customer: $('#txtSearchCustomer').val(),
        CommunicationType: $('#txtSearchCommunicationType').val()
    });
}

function CaluculateWeight() {
    $(".input-large").focusout(function () {
        try {
            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            var height = document.getElementById('Height').value;
            var BF = document.getElementById('BF').value;
            var GSM = document.getElementById('GSM').value;
            //var PPly = document.getElementById('Ply').value;
            var cal1 = Number(length) + Number(width) + 50;
            cal1 = (Math.round(Number(cal1) / 100)) * 100;
            var cal2 = (Math.round(Number(height) / 100)) * 100;
            var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(PPly) * 8;
            document.getElementById('Weight').value = Number(weight) / 1000000;
        }
        catch (e) { }
    });
}

function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;
    //If the call is from delevery schedule
    if (objectContext.data.DeliveryScheduleItem != null) {
        if ($("#Quantity").val() == "") {
            $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
            bValidation = false;
        }
        else {
            if (!$.isNumeric($("#Quantity").val())) {
                $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
                bValidation = false;
            }
        }
        if ($("#dtDeliveryDate").val() == "") {
            $("#dtDeliveryDate").validationEngine('showPrompt', 'Select Date', 'error', true)
            bValidation = false;
        }
        else {
            if (_util.dateGraterThan($('#dtDeliveryDate').val(), $('#dtDate').val())) {
                $("#dtDeliveryDate").validationEngine('showPrompt', 'Delivery date cannot be less than Enquiry date', 'error', true);
                bValidation = false;
            }
        }
        if ($("#Customer").val() < 1) {
            $("#Customer").validationEngine('showPrompt', 'Select Customer', 'error', true)
            bValidation = false;
        }
    }
    return bValidation;
}

function resetOneToManyForm(property) {
    $("#Quantity").val("");
}

function radioClass(str1) {
    if (str1 == "Stitched") {
        document.getElementById('Joints').value = str1;
    }
    else if (str1 == "Glued") {
        document.getElementById('Joints').value = str1;
    }
}
 