﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
//var stockqty = null;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Enquiry List',
        paging: true,
        pageSize: 15,
        actions: {
            listAction: '/Enquiry/EnquiryListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            CustDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Customer Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Customer Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Customer/CustomerListByFiter?Pk_Customer=' + data.record.Fk_Customer
                                        },
                                        fields: {

                                            Pk_Customer: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            CustomerName: {
                                                title: 'CustomerName',
                                                width: '15%'
                                            },
                                            CustomerAddress: {
                                                title: 'CustomerAddress'
                                            },
                                            City: {
                                                title: 'City',
                                                width: '10%'
                                            },
                                            State: {
                                                title: 'State',
                                                width: '10%'
                                            },
                                            Country: {
                                                title: 'Country',
                                                width: '10%'
                                            },
                                            PinCode: {
                                                title: 'PinCode',
                                                width: '10%'
                                            },
                                            CustomerContact: {
                                                title: 'Mobile',
                                                key: false
                                            },
                                            LandLine: {
                                                title: 'LandLIne'
                                            },
                                            Email: {
                                                title: 'Email',
                                                width: '10%'
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Contacts'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Contacts List',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/Customer/CustomerContacts?Pk_Customer=' + data.record.Pk_Customer
                                                                        },
                                                                        fields: {
                                                                            Pk_CustomerContacts: {
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            ContactPersonName: {
                                                                                title: 'Person Name',
                                                                                key: false
                                                                            },
                                                                            ContactPersonDesignation: {
                                                                                title: 'Designation'

                                                                            },
                                                                            ContactPersonNumber: {
                                                                                title: 'Contact Number',
                                                                                width: '2%'
                                                                            },
                                                                            ContactPersonEmailId: {
                                                                                title: 'Email',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            Pk_Enquiry: {
                title: 'Enquiry Id',
                key: true,
                width: '4%'
            },
            Date: {
                title: 'Enquiry Date',
                width: '4%'
            },
            Fk_Customer: {
                title: 'CustomerId',
                list: false
            },
            Customer: {
                title: 'Customer Name',
                edit: false
            },
            CommunicationType: {
                title: 'Communication Type',
                width: '5%'
            },
            Description: {
                title: 'Description'
            },

            SpecialRequirements: {
                title: 'Special Req'
            },
            EqComments: {
                title: 'Comments'
            },
            Fk_Status: {
                title: 'Status',
                width: '5%'
            }

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Enquiry: $('#txtSearchEnquiryId').val(),
            Customer: $('#txtSearchCustomer').val(),
            CommunicationType: $('#txtSearchCommunicationType').val(),
            EnquiryDate: $('#txtSearchEnquiryDate').val()
        });

    });


    $('#txtSearchEnquiryDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            EnquiryDate: $('#txtSearchEnquiryDate').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Communication", "getCommunicationType", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Communication", "getCommunicationType", null);

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Customers", "getCustomers", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Customers", "getCustomers", null);

    $('#txtSearchEnquiryId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Enquiry: $('#txtSearchEnquiryId').val()
            });
        }
    });

    $('#txtSearchCustomer').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Customer: $('#txtSearchCustomer').val()
            });
        }
    });

    $('#txtSearchCommunicationType').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                CommunicationType: $('#txtSearchCommunicationType').val()
            });
        }
    });

    $("#searchDialog").width(1000);
    $("#searchDialog").css("top", "50px");
    $("#searchDialog").css("left", "300px");

    $("#searchCustomerDialog").width(1000);
    $("#searchCustomerDialog").css("top", "50px");
    $("#searchCustomerDialog").css("left", "300px");


    setUpEnquirySearch();
    setUpCustomerSearch();
}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    CaluculateWeight();

    $('#PrintingRequired').change(function (e) {
        e.preventDefault();
        if ($('#PrintingRequired').is(':checked') == true) {
            $('#PrintingDetailsInfo').show();
        }
        else if ($('#PrintingRequired').is(':checked') == false) {
            $('#PrintingDetailsInfo').hide();
        }
    });

    $('#cmdNewCustomer').click(function (e) {
        e.preventDefault();
        window.open("/Customer");
    });


    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        $("#searchCustomerDialog").modal("show");
    });

    $('#dtDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Enquiry/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Box Name'

            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            
        },

        Rate: {
            title: 'Rate',
            width: '1%'
        },
        Amount: {
            title: 'Amount',
            width: '1%'

        }

        }
    });

   
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "enquiryChilds", "Enquiry", "_AddMaterial", function () { return new IssuedMaterial() });
    //configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "Enquiry", "_AddMaterial", function () { return new IssuedMaterial() });

    $('#cmdVendorMSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
    });


    $('#cmdMIndentSearch').click(function (e) {
        e.preventDefault();
        setUpIndentSearch();
        $("#searchIndentDialog").modal("show");
    });
}

function beforeModelSaveEx() {
    //$(".ui-accordion-content").show();
    _util.setDivPosition("tbSpcitication", "none");
    _util.setDivPosition("tbDeliverySchedule", "none");
    _util.setDivPosition("tbDocument", "none");
    _util.setDivPosition("tbCustomerDetails", "block");

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Date"] = $('#dtDate').val();
    viewModel.data["Weight"] = $('#Weight').val();
    viewModel.data["PlateRequired"] = $('#PlateRequired').is(':checked');
    viewModel.data["LengthPartitionRequired"] = $('#LengthPartitionRequired').is(':checked');
    viewModel.data["WidthPartitionRequired"] = $('#WidthPartitionRequired').is(':checked');
    viewModel.data["CapRequired"] = $('#CapRequired').is(':checked');
    viewModel.data["PrintingRequired"] = $('#PrintingRequired').is(':checked');
    viewModel.data["Joints"] = $.trim($('#Joints').val());
    viewModel.data["Length"] = $.trim($('#Length').val());
    viewModel.data["Width"] = $.trim($('#Width').val());
    viewModel.data["Height"] = $.trim($('#Height').val());
    viewModel.data["GSM"] = $.trim($('#GSM').val());
    viewModel.data["BF"] = $.trim($('#BF').val());
    viewModel.data["NumberOfJoints"] = $.trim($('#NumberOfJoints').val());
    viewModel.data["MoistureContent"] = $.trim($('#MoistureContent').val());
    viewModel.data["NumberOfColors"] = $.trim($('#NumberOfColors').val());
    viewModel.data["Ply"] = $.trim($('#Ply').val());


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Date"] = $('#dtDate').val();
    viewModel1.data["PlateRequired"] = $('#PlateRequired').is(':checked');
    viewModel1.data["Weight"] = $('#Weight').val();
    viewModel1.data["LengthPartitionRequired"] = $('#LengthPartitionRequired').is(':checked');
    viewModel1.data["WidthPartitionRequired"] = $('#WidthPartitionRequired').is(':checked');
    viewModel1.data["CapRequired"] = $('#CapRequired').is(':checked');
    viewModel1.data["PrintingRequired"] = $('#PrintingRequired').is(':checked');
    viewModel1.data["Joints"] = $.trim($('#Joints').val());
    viewModel1.data["Length"] = $.trim($('#Length').val());
    viewModel1.data["Width"] = $.trim($('#Width').val());
    viewModel1.data["Height"] = $.trim($('#Height').val());
    viewModel1.data["GSM"] = $.trim($('#GSM').val());
    viewModel1.data["BF"] = $.trim($('#BF').val());
    viewModel1.data["NumberOfJoints"] = $.trim($('#NumberOfJoints').val());
    viewModel1.data["MoistureContent"] = $.trim($('#MoistureContent').val());
    viewModel1.data["NumberOfColors"] = $.trim($('#NumberOfColors').val());
    viewModel1.data["Ply"] = $.trim($('#Ply').val());
}

function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            Rate: {
                title: 'Rate',
                width: '1%'
            },
            Amount: {
                title: 'Amount',
                width: '1%'

            }
        }
    });

    if ($.trim($('#TaxType').val()) == "VAT") {
        $("#VAT").prop("checked", true);
    }
    else if ($.trim($('#TaxType').val()) == "CST") {
        $("#CST").prop("checked", true);


    }

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();
      
        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }
  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });

}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            CustomerAddress: {
                title: 'Address'
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            },
            Email: {
                title: 'Email'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#Customer').wgReferenceField("setData", rows[0].keyValue);
        $("#searchCustomerDialog").modal("hide");
    });

}

function setUpBoxSearch(viewObject) {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldPartNo = "<input type='text' id='txtdlgPartNo' placeholder='PartNo' class='input-large search-query'/>";


    $('#searchBoxDialog').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter'
        },
        fields: {
            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            PartNo: {
                title: 'PartNo',
                width: '25%'
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            //Description: {
            //    title: 'Description',
            //    width: '25%'
            //},
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Name: $('#txtdlgBoxName').val()//,
            //PartNo: $('#txtdlgPartNo').val()

        });

    });
    //$('#txtdlgEnquiryDate').datepicker({
    //    autoclose: true
    //});
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Name: $('#txtdlgBoxName').val()//,
            //PartNo: $('#txtdlgPartNo').val()

        });
    });


    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchBoxDialog').jtable('selectedRows');


        //document.getElementById('Fk_BoxID').value = rows[0].keyValue;
        // document.getElementById('TxtIndent').value = rows[0].data.IndentNo;
        //document.getElementById('TxtMaterial').value = rows[0].data.Fk_Material;
        //document.getElementById('TxtMaterialId').value = rows[0].data.Fk_Material1;

        $('#Fk_BoxID').wgReferenceField("setData", rows[0].data.Pk_BoxID);
        $("#searchBoxDialog").modal("hide");

        //document.getElementById('Quantity').value = rows[0].data.PendQty;
        //document.getElementById('SQuantity').value = rows[0].data.SQuantity;
        //document.getElementById('Unit').value = rows[0].data.UnitName;
        ////document.getElementById('Unit1').value = rows[0].data.UnitName;
        ////document.getElementById('Unit2').value = rows[0].data.UnitName;
        //document.getElementById('TanentName').value = rows[0].data.TanentName;

    });

}

function setUpIssueMaterialSearch() {

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/getMaterialDetails',
        },
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            MaterialName: {
                title: 'Material Name'
            },
            //Quantity: {
            //    title: "Quantity"
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('ReturnableQty').value = rows[0].data["Quantity"];
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
    });



}

function ReferenceFieldNotInitilized(viewModel) {

    $('#Customer').wgReferenceField({
        keyProperty: "Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });

    $('#Fk_BoxID').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#Customer').wgReferenceField("setData", viewModel.data["Customer"]);
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
    }



}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#searchBoxDialog').empty();
    $('#MainSearhContainer').append("<div id='searchBoxDialog'></div>");
}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpBoxSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function CaluculateWeight() {
    $(".input-large").focusout(function () {
        try {
            var length = document.getElementById('Length').value;
            var width = document.getElementById('Width').value;
            var height = document.getElementById('Height').value;
            var BF = document.getElementById('BF').value;
            var GSM = document.getElementById('GSM').value;
            var PPly = document.getElementById('Ply').value;
            var cal1 = Number(length) + Number(width) + 50;
            cal1 = (Math.round(Number(cal1) / 100)) * 100;
            var cal2 = (Math.round(Number(height) / 100)) * 100;
            var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(PPly) * 8;
            document.getElementById('Weight').value = Number(weight) / 1000000;
        }
        catch (e) { }
    });
}

//function afterOneToManyDialogShow(property) {

//    $('#cmdSearchMaterialsdetails').click(function (e) {
//        e.preventDefault();
//        setUpIssueMaterialSearch();
//        _util.setDivPosition("divSearchMaterial", "block");
//        _util.setDivPosition("divCreateMaterial", "none");
//    });
//}

//function ReferenceFieldNotInitilized(viewModel) {

//    //document.getElementById('ReturnableQty').style.display = "none";

//    if (objContextEdit == true) {
//        $('#cmdSearchMaterialsdetails').attr('disabled', true);
//    }
//    else if (objContextEdit == false) {
//        $('#cmdSearchMaterialsdetails').attr('disabled', false);
//    }
//    $('#Fk_Vendor').wgReferenceField({
//        keyProperty: "Fk_Vendor",
//        displayProperty: "VendorName",
//        loadPath: "Vendor/Load",
//        viewModel: viewModel
//    });
//    $('#txtFk_Material').wgReferenceField({
//        keyProperty: "Fk_Material",
//        displayProperty: "Name",
//        loadPath: "Material/Load",
//        viewModel: viewModel
//    });

//    //$('#Fk_Indent').wgReferenceField({
//    //    keyProperty: "Fk_Indent",
//    //    displayProperty: "Fk_Indent",
//    //    loadPath: "MaterialIndent/Load",
//    //    viewModel: viewModel
//    //});

//    if (viewModel.data != null) {
//        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
//        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
//     //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
//    }
//}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}
function setUpIndentSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader").text("Indent Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Indent List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialIndent/MaterialIndentListByFiter'
        },
        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true
            },

            MaterialIndentDate: {
                title: 'Indent Date'
            },

            Vendor: {
                title: 'Vendor Name'
            },
            //VendorID: {
            //    title: 'VendorID'
            //},
            Branch: {
                title: 'Branch'
            },
            StateName: {
                title: 'Status'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val(),
            Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_Indent').value = rows[0].keyValue;
         IndentNo = rows[0].keyValue;

        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}
function CalcAmt() {
  //  curViewModel = viewObject.viewModel;
    if  (objContextEdit == false) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));

            
                   }
   
        else {
        }
    }else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
        }
        else {
        }


    
    }
}

function radioClass1(intval) {
    if (intval == 1) {


        document.getElementById('TaxType').value = "VAT";

        document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

        if (Number($('#ED').val()) > 0) {
            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
            //  NETVALUE
        }

    }
    else if (intval == 2) {

        document.getElementById('TaxType').value = "CST";



        if (Number($('#ED').val()) > 0) {

            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
        }

    }

 
}


//$('#cmdSaveMaterial').click(function (e) {
//    e.preventDefault();
//    var i = 0;

//    for (i = 0; i < '#tblmaterial'. ; i++) {
//        text += cars[i] + "<br>";
//    }
//});
