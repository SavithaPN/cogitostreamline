﻿function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Ink List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Ink/InkListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Name: {
                title: 'Name'
            }
            ,
            Fk_UnitId: {
                title: 'Unit'
            }
            ,

            Fk_Color: {
                title: 'Color'
            }
            ,
            Brand: {
                title: 'Brand'
            }


        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Name: $('#TxtInkName').val(),
            Fk_Color: $('#TxtInkColor').val(),
            Brand: $('#TxtBrand').val()
        });

    });

    $('#TxtInkName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtInkName').val(),
                Fk_Color: $('#TxtInkColor').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });


    $('#TxtInkColor').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtInkName').val(),
                Fk_Color: $('#TxtInkColor').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });


    $('#TxtBrand').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Name: $('#TxtInkName').val(),
                Fk_Color: $('#TxtInkColor').val(),
                Brand: $('#TxtBrand').val()
            });
        }
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


  


    _page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Color", "getColor", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Color", "getColor", null);

}
function afterNewShow(viewObject) {
    viewObject.viewModel.data["Category"] = "Ink";


    $('#cmdColor').click(function (e) {
        e.preventDefault();
        window.open('/Color');
    });
}


function CheckInkDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("Ink/InkDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Ink Name Already added";
}