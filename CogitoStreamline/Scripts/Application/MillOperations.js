﻿//Name          : Mill Operations ----javascript files
//Description   : Contains the  Mill Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckMillDuplicate(duplication checking)  CheckContactDuplicate(duplication checking) 
//Author        : shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0001
//Modifications : 

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Mill List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Mill/MillListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Mill: {
                title: 'Id',
                key: true,
                list: true,
                width:'5%'
            },
            MillName: {
                title: 'Name'
            },            
            ContactNumber: {
                title: 'Contact Number'
            },      
            Email: {
                title: 'EmailId'
            },
            ContactPerson1: {
                title: 'Contact Person 1'
            },
            LandLine: {
                title: 'Land Line'
            }
        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Mill: $('#TxtPkMill').val(),
            MillName: $('#TxtMillName').val()
        });

    });

    $('#TxtPkMill').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Mill: $('#TxtPkMill').val(),
                MillName: $('#TxtMillName').val()
            });
        }
    });

    $('#TxtMillName').keypress(function (e) {
        if (e.keycode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Mill: $('#TxtPkMill').val(),
                MillName: $('#TxtMillName').val()
            });
        }
    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#btnCloseLayout').click(function (e) {
        e.preventDefault();
        $("#frmDataDialog").validationEngine('hide');
    });
}

function afterNewShow(viewObject) {
    $('#contacts').wgContact({
        viewModel: viewObject.viewModel,
        property: "Contacts"
    });


}

function afterEditShow(viewObject) {
    $('#contacts').wgContact({
        viewModel: viewObject.viewModel,
        property: "Contacts"
    });


}

function afterModelSaveEx() {
    document.location.reload();
}

function CheckMillDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("MillName", $('#MillName').val());
    oResult = _comLayer.executeSyncAction("Mill/MillDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Mill Name Already added";

}



function ContactDuplicateChecking() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("ContactPersonName", $('#ContactPersonName').val());
    _comLayer.parameters.add("Pk_Mill", $('#Pk_Mill').val());
    oResult = _comLayer.executeSyncAction("Mill/ContactDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0) {
        document.getElementById("ContactPersonName").value = "";
        return "* " + "Contact Name Already added";
    }
}

