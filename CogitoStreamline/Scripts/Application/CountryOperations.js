﻿
//Name          : Country Operations ----javascript files
//Description   : Contains the  PaperType Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckCountryDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0010
//Modifications : 

var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Countries List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Country/CountryListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction:''
        },
        fields: {
            Pk_Country: {
                title: 'Id',
                key: true,
                list: true
            },
            CountryName: {
                title: 'Name',
                width: '25%'
            }
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Country: $('#txtCountryID').val(),
            CountryName: $('#txtCName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckCountryDuplicate() {
   _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("CountryName", $('#CountryName').val());
    oResult = _comLayer.executeSyncAction("Country/CountryDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Country Already added";
}










