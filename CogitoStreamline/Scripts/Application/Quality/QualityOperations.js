﻿var curViewModel = null;
var bFromSave = false;
var objContextEdit = false;

var gMaterials = "";
var VendorID = "";
var MaterialID = "";
var POQty;
var IndentNo = "";
var div = null;

//var stockqty = null;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Quality Check List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/QualityCheckListByFiter',
            updateAction: '',
            //deleteAction: '',
            createAction: ''
        },
        fields: {
            Pk_QualityCheck: {
                title: 'Check No.',
                key: true
            },
            CheckDate: {
                title: 'Date'
            },
            VName: {
                title: 'Vendor'
            },
          
            InvoiceNumber: {
                title: 'InvoiceNumber'
            },
            Fk_PONo: {
                title: 'PONo'
            },
            DCNo: {
                title: 'Cert.No'
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_QC = row.record.Pk_QualityCheck;
                        _comLayer.parameters.add("Pk_QC", Pk_QC);
                        _comLayer.executeSyncAction("CheckQuality/QCRep", _comLayer.parameters);
                        var strval = "ConvertPDF/QualityCheck" + Pk_QC + ".pdf"

                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/QualityCheck" + Pk_QC + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');
                            return true;
                        }

  
                        /////////////////////////

                        

                    });
                    return button;
                }
            },

            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/CheckQuality/QualityGetRec?Pk_QualityCheck=' + data.record.Pk_QualityCheck

                                        },
                                        fields: {


                                            Name: {
                                                title: 'MaterialName'
                                            },
                                            POQty: {
                                                title: 'PO Qty',
                                                key: true,
                                            },
                                            Quantity: {
                                                title: 'Accepted Qty',
                                                key: true,
                                            },
                                            Color: {
                                                title: 'Shade',
                                                key: true,
                                            },
                                            Mill: {
                                                title: 'Mill',
                                                key: true,
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },


            //////////////////////////////////////////////////
        }
    });
  
    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            QCNo: $('#txtIndentNumber').val(),
            Vendor: $('#TxtVendorName').val(),
            InvoiceNo: $('#Txtinward').val(),
            CheckDate: $('#TxtDate').val(),

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#TxtDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            CheckDate: $('#TxtDate').val()
        });
    });
    $('#TxtDate').datepicker({
        autoclose: true
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });



    $('#TxtDate').datepicker({ autoclose: true });

    $("#searchIndentDialog").width(1000);
    $("#searchIndentDialog").css("top", "10px");
    $("#searchIndentDialog").css("left", "400px");


}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;


    $("#dropzone").wgUpload({
        wgTag: "CheckQuality",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });


    $('#cmdMPOSearch').click(function (e) {
        e.preventDefault();
        setUpPOSearch();
        $("#searchIndentDialog").modal("show");
    });

    //$('#cmdSearchMaterialsdetails').click(function (e) {
    //    e.preventDefault();

    //    _util.displayView("CheckQuality", "_AddMaterialIndent", "dataDialogArea");
    //    $('#dataDialogHeading').text("Search");
    //    $("#dataDialog").modal("show");
    //    setUpIssueMaterialSearch();
    //});
    $('#dtDate').datepicker({ autoclose: true });

    $('#divMaterialDetail').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            Color: {
                title: 'Shade'

            },
            Mill: {
                title: 'Mill'

            },
            ReelNo: {
                title: 'ReelNo'

            },
            POQty: {
                title: 'PO Qty',
                width: '1%'
            },
            AccQty: {
                title: 'Pass Qty',
                width: '1%'
            }

        }
    });


    //configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "Materials", "CheckQuality", "_AddMaterialIndent", function () { return new IssuedMaterial() });
    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "Materials", "CheckQuality", "NOTUSED", function () { return new IssuedMaterial() }, "divCreateMaterialIndent");
    //configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");


    $('#cmdCreate').hide();

    $('#MyTab a').click(function (e) {
        e.preventDefault();
        //$(this).tab('show')
        //alert($('.nav-tabs .active').text());
        if ($(this).attr("href") == "#tab1") {
            //$('input:submit').show();
            $('#cmdCreate').hide();
        }
        else if 
            ($(this).attr("href") == "#tab2") {
            $('#cmdCreate').show();
            //$('input:submit').hide();
            //document.getElementById("cmdCreate").style.display = 'none';
        }
    })

}
function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgSearchMaterialFields').empty();
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["CheckDate"] = $('#dtDate').val();
  
    viewModel.data["Fk_PONo"] = IndentNo;
  

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["CheckDate"] = $('#dtDate').val();

    viewModel1.data["Fk_PONo"] = IndentNo;


}
function afterEditShow(viewObject) {
    curEdit = true;
    curViewModel = viewObject.viewModel;
    $('#divMaterialDetail').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            ReelNo: {
                title: 'ReelNo'

            },
            Color: {
                title: 'Shade'

            },
            Mill: {
                title: 'Mill'

            },
            POQty: {
                title: 'PO Qty',
                width: '1%'
            },
            AccQty: {
                title: 'Pass Qty',
                width: '1%'
            }


            //Rate: {
            //    title: 'Rate',
            //    width: '1%'
            //},
            //Amount: {
            //    title: 'Amount',
            //    width: '1%'

        }
    });

  
    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();
        
        oCut.load(oSCuts[i].Pk_QualityChild, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].POQty, oSCuts[i].AccQty, oSCuts[i].ReelNo, oSCuts[i].Color, oSCuts[i].Mill);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "Materials", "CheckQuality", "NOTUSED", function () { return new IssuedMaterial() }, "divCreateMaterialIndent");


    $("#dropzone").wgUpload({
        wgTag: "CheckQuality",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });
}


function setUpIssueMaterialSearch() {
    IndentNo = document.getElementById('Fk_PO').value;
    
    cleanSearchDialog();

    $("#srchCHeader").text("Customer Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgName' placeholder='Material' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldCustomerName);


    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/CheckQuality/POList?Pk_PONo=' + IndentNo
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMatDone').click();
            });
        },
        fields: {
            Pk_PONo: {
                title: 'PO No',            
                list: true
            },
            Pk_Material: {
                title: 'Material ID' ,
                key: true,
                list: true
            },
            Mat_Name: {
                title: 'MaterialName',
                key: true
            },
            Color: {
                title: 'Shade'
            },
            Mill: {
                title: 'Mill'
            },
            Quantity: {
                title: 'PO Qty'
            }
        }
    });
    $('#MaterialSearchContainer').jtable('load');

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();

        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtdlgName').val()
        });
    });

    $('#cmdMatDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        //$("#dataDialog").modal("hide");
        $("#dataDialog").modal("hide");
        document.getElementById('Fk_Material').value = rows[0].data["Pk_Material"];
        $('#txtMaterial').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('POQty').value = rows[0].data["Quantity"];

        MaterialID = rows[0].data["Pk_Material"];
        POQty = rows[0].data["Quantity"];
        Shade = rows[0].data["Color"];

        document.getElementById('Shade').value = Shade;
        document.getElementById('Mill').value = rows[0].data["Mill"];
        $("#ReelNo").focus();
        //$("#txtMaterial").focus();

    });
  

}

function afterOneToManyDialogShow(property) {
    $('#cmdMPOSearch').click(function (e) {
        e.preventDefault();
        setUpPOSearch();
        $("#searchIndentDialog").modal("show");
    });

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();

        _util.displayView("CheckQuality", "_AddMaterialIndent", "dataDialogArea");
   $('#dataDialogHeading').text("Search");
        $("#dataDialog").modal("show");
        setUpIssueMaterialSearch();
       


    });
}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
  

    $('#txtMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });



    if (viewModel.data != null) {
        $('#txtMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
   }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

//function checkstock() {
//    if (objContextEdit == false) {

//        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
//            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
//        }
//        else {

//        }
//    } else if (objContextEdit == true) {
//        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
//            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
//        }
//        else {
//        }
//    }
//}
function checkValue() {
    if (objContextEdit == false) {

        if (Number($('#AccQuantity').val()) > Number($('#POQty').val())) {
            return "* " + "Checked Quantity Exceeded PO Quantity. PO Quantity:" + $('#POQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#AccQuantity').val()) > Number($('#POQty').val())) {
            return "* " + "Checked Quantity Exceeded PO Quantity. PO Quantity:" + $('#POQty').val();
        }
        else {
        }
    }
}
function setUpIndentSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader").text("Indent Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Indent List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialIndent/MaterialIndentListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true
            },

            MaterialIndentDate: {
                title: 'Indent Date'
            },

            Vendor: {
                title: 'Vendor Name'
            },
            //VendorID: {
            //    title: 'VendorID'
            //},
            Branch: {
                title: 'Branch'
            },
            StateName: {
                title: 'Status'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val(),
            Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_Indent').value = rows[0].keyValue;
        IndentNo = rows[0].keyValue;

        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}
function setUpPOSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchHeader").text("PO Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Purchase Order List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/PurchaseOrder/PurchaseOrderListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_PONo: {
                title: 'Po Number',
                key: true
            },

            PODate: {
                title: 'PO Date'
            },

            Fk_Vendor: {
                title: 'Vendor Name'
            },

            Status: {
                title: 'Status'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_PONo: $('#txtdlgIndent').val(),
            Fk_Vendor: $('#txtdlgVendor').val(),
            TxtFromDate: $('#txtdlgFromDate').val(),
            TxtToDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');

        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        //document.getElementById('txtFk_PONumber').value = rows[0].keyValue;
        IndentNo = rows[0].keyValue;

     
        document.getElementById('Fk_PO').value = rows[0].keyValue;
        document.getElementById('txtFk_Vendor').value = rows[0].data.Fk_Vendor;

        $("#searchIndentDialog").modal("hide");

        //$('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}


