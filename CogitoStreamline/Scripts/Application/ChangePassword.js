﻿    var _util = null;
var _comLayer = null;
var bLoginPressed = false;

$(document).ready(function () {

    _config = this;
    _comLayer = new WgCommunicationLayer();

    _utils = new WgUtilities();



    $("#cmdChangePassword").click(function (e) {
        e.preventDefault();



        _util.showMessageDialog("", false);

        _util.displayView("User", "_ChangePassword", "dataDialogArea1");

        _util.closeMessageDialog();

        $("#dataDialog1").modal("show");

        $('#dataDialog1').on('hide', function () {
            $('#frmDataDialog1').validationEngine('hide')
        });

        $("#cmdChangePwd").click(function (e) {
            e.preventDefault();

            if ($('#frmDataDialog').validationEngine('validate')) {
                _comLayer.parameters.clear();
                _comLayer.parameters.add("CurrentPassword", $("#txtCurrentPassword").val());
                _comLayer.parameters.add("NewPassword", $("#txtNewPassword").val());

                _util.showMessageDialog("", false);

                var oResult = _comLayer.executeSyncAction("User/UpdatePassword", _comLayer.parameters);

                if (oResult.Success) {
                    _util.showSuccess("Password changed successfully", function () { });
                }
                else {
                    _util.showError("Error -" + oResult.Message, function () { });
                }

                $("#dataDialog").modal("hide");
            }


        });

    });

});
