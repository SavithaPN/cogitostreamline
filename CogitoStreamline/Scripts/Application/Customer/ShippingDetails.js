﻿//Name          : Order Operations ----javascript files
//Description   : Contains the  delivery data details
//                 1. Jtable Loading and Columns Descriptions 
//Author        : shantha
//Date            :   17/08/2015
//CRH Number      :   SL0030-SL0035
//Modifications : 



function CustomerShippingDetails() {
    this.data = new Object();
    this.data["Pk_CustomerShippingId"] = "";
    this.data["ShippingCustomerName"] = "";
    this.data["ShippingAddress"] = "";
    this.data["ShippingPincode"] = "";
    this.data["ShippingMobile"] = "";
    this.data["ShippingLandLine"] = "";
    this.data["ShippingEmailId"] = "";
    this.data["ShippingCountry"] = "";
    this.data["ShippingState"] = "";
    this.data["ShippingCity"] = "";


    this.updateValues = function () {
    };

    this.load = function (Pk_CustomerShippingId, ShippingCustomerName, ShippingAddress, ShippingPincode, ShippingMobile, ShippingLandLine, ShippingEmailId, ShippingCountry, ShippingState, ShippingCity) {
        this.data["Pk_CustomerShippingId"] = Pk_CustomerShippingId;
        this.data["ShippingCustomerName"] = ShippingCustomerName;
        this.data["ShippingAddress"] = ShippingAddress;
        this.data["ShippingPincode"] = ShippingPincode;
        this.data["ShippingMobile"] = ShippingMobile;
        this.data["ShippingLandLine"] = ShippingLandLine;
        this.data["ShippingEmailId"] = ShippingEmailId;
        this.data["ShippingCountry"] = ShippingCountry;
        this.data["ShippingState"] = ShippingState;
        this.data["ShippingCity"] = ShippingCity;

    };
}