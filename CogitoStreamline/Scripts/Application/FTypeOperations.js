﻿
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Flute Type List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/FType/FluteListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_FluteID: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            FluteName: {
                title: 'Name'
            },
            TKFactor: {
                title: 'TakeUPFactor'
            },
        


        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            FluteID: $('#TxtPkFluteID').val(),
            Name: $('#TxtName').val(),
            TakeUpFactor: $('#TxtTake').val(),
         


        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });



}

function beforeModelSaveEx() {

}

function afterNewShow(viewObject) {
    
}
function afterEditShow(viewObject) {

 
}
function CheckTypeDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("FType/FTypeDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "FluteType Name Already added";

}
