﻿
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'ID to OD List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/ODID/ODIDListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_ID: {
                title: 'Id',
                key: true,
                width: '5%'
            },
            Fk_FluteID: {
                title: 'FluteType'
            },
            PlyValue: {
                title: 'PLY',
                width: '5%'
            },

            Length: {
                title: 'Length'
            },
            Width: {
                title: 'Width'
            },
            Height: {
                title: 'Height'
            } 
            
            

        }
    });

    //Re-load records when Staff click 'load records' button.

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
           
            ID: $('#TxtID').val(),
            Length: $('#TxtLength').val(),
            Width: $('#TxtWidth').val(),
            Height: $('#TxtHeight').val()
         


        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    _page.getViewByName('New').viewModel.addAddtionalDataSources("FluteType", "getFluteType", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("FluteType", "getFluteType", null);
}

function beforeModelSaveEx() {

    //var viewModel = _page.getViewByName('New').viewModel;
    //viewModel.data["Weight"] = $('#Weight').val();

    //var viewModel1 = _page.getViewByName('Edit').viewModel;
    //viewModel1.data["Weight"] = $('#Weight').val();
}

function afterNewShow(viewObject) {
    //viewObject.viewModel.data["Category"] = "Cap";

    //$(".input-large").focusout(function () {
    //    try {
    //        var length = document.getElementById('Length').value;
    //        var width = document.getElementById('Width').value;
    //        var height = document.getElementById('Height').value;
    //        var BF = document.getElementById('BF').value;
    //        var GSM = document.getElementById('GSM').value;

    //        var PPly = document.getElementById('PPly').value;

    //        var cal1 = Number(length) + Number(width) + 50;
    //        cal1 = (Math.round(Number(cal1) / 100)) * 100;
    //        var cal2 = (Math.round(Number(height) / 100)) * 100;

    //        var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(PPly) * 8;
    //        document.getElementById('Weight').value = Number(weight) / 1000000;
    //    }
    //    catch (e) { }

    //});
}
function afterEditShow(viewObject) {

    //$(".input-large").focusout(function () {
    //    try {
    //        var length = document.getElementById('Length').value;
    //        var width = document.getElementById('Width').value;
    //        var height = document.getElementById('Height').value;
    //        var BF = document.getElementById('BF').value;
    //        var GSM = document.getElementById('GSM').value;

    //        var PPly = document.getElementById('PPly').value;

    //        var cal1 = Number(length) + Number(width) + 50;
    //        cal1 = (Math.round(Number(cal1) / 100)) * 100;
    //        var cal2 = (Math.round(Number(height) / 100)) * 100;

    //        var weight = Number(cal1) * 2 * Number(cal2) * Number(GSM) * Number(PPly) * 8;
    //        document.getElementById('Weight').value = Number(weight) / 1000000;
    //    }
    //    catch (e) { }
    //});
}
//function CheckCapDuplicate() {

//    _comLayer.parameters.clear();
//    var oResult;
//    _comLayer.parameters.add("Name", $('#Name').val());
//    oResult = _comLayer.executeSyncAction("Cap/CapDuplicateChecking", _comLayer.parameters);
//    if (oResult.TotalRecordCount > 0)
//        return "* " + "Cap Name Already added";

//}
