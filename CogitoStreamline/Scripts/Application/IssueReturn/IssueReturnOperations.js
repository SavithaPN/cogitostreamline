﻿var curViewModel = null;
var curEdit = false;
//var stockqty = null;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Return List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/IssueReturn/IssueReturnListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_IssueReturnMasterId: {
                title: 'Return No',
                key: true,
                list: true,
                width: '4%'
            },
            IssueReturnDate: {
                title: 'Return Date',
                width: '4%'
            },
            Fk_EnteredUserId: {
                title: 'Raised By',
                width: '10%',
                list: false
            },
            Fk_IssueId: {
                title: 'Issue ID',
                width: '10%'
            },
            IssueDate: {
                title: 'Issue Date',
                width: '10%'
            },
            CustName: {
                title: 'Cust.Name'
            },
            JobCardID: {
                title: 'JobCardID'
            },
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_IssRet = row.record.Pk_IssueReturnMasterId;
                        _comLayer.parameters.add("Pk_IssRet", Pk_IssRet);
                        _comLayer.executeSyncAction("IssueReturn/ReturnRep", _comLayer.parameters);
                        var strval = "ConvertPDF/IssReturn" + Pk_IssRet + ".pdf"
                        ////////////////////////////


                        var xhr = new XMLHttpRequest();
                        var urlToFile = "ConvertPDF/IssReturn" + Pk_IssRet + ".pdf"
                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');
                            return true;
                        }


                        /////////////////////////
                       

                    });
                    return button;
                }
            },
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_IssueReturnMasterId: $('#PkIssueReturnMasterId').val(),
            IssueReturnDate: $('#PkIssueReturnMasterDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#PkIssueReturnMasterDate').datepicker({ autoclose: true });


    $('#PkIssueReturnMasterId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            $('#MainSearchContainer').jtable('load', {
                Pk_IssueReturnMasterId: $('#PkIssueReturnMasterId').val()
            });
        }
    });

    $('#PkIssueReturnMasterDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            IssueReturnDate: $('#PkIssueReturnMasterDate').val()
        });
    });
    
    $("#searchDialog").width(900);
    $("#searchDialog").css("top", "10px");
    $("#searchDialog").css("left", "400px");

    setUpIssueSearch();
}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtIssueReturnDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtIssueReturnDate').datepicker({ autoclose: true });

    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/IssueReturn/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            Color: {
                title: 'Shade'

            },
            Mill: {
                title: 'Mill'

            },
            RollNo: {
                title: 'RollNo'

            },
            ReturnQuantity: {
                title: 'Ret.Qty',
                width: '1%'
            },
            Fk_StockID: {
                title: 'stkid',
                width: '1%',
                list:false

            }

        }
    });

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "IssueReturn", "_AddMaterial", function () { return new IssuedMaterial() });

    $('#cmdIssueSearch').click(function (e) {
        e.preventDefault();
        $("#searchDialog").modal("show");
    });
}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["IssueReturnDate"] = $('#dtIssueReturnDate').val();
    viewModel.data["Fk_IssueId"] = $('#Fk_IssueId').val();
    viewModel.data["IssueDate"] = $('#dtIssueDate').val();
    viewModel.data["Product"] = $('#Product').val();
    
    //viewModel.data["Pk_StockID"] = $('#Pk_StockID').val();


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["IssueReturnDate"] = $('#dtIssueReturnDate').val();
    viewModel1.data["Fk_IssueId"] = $('#Fk_IssueId').val();
    viewModel1.data["IssueDate"] = $('#dtIssueDate').val();
    viewModel1.data["Product"] = $('#Product').val();

    //viewModel1.data["Pk_StockID"] = $('#Pk_StockID').val();

}
function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#ReturnDate').datepicker({ autoclose: true });
    curEdit = true;
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/IssueReturn/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name'

            },
            Color: {
                title: 'Shade'

            },
            Mill: {
                title: 'Mill'

            },
            RollNo: {
                title: 'RollNo'

            },
            ReturnQuantity: {
                title: 'Ret.Qty',
                width: '1%'
            },
            Fk_StockID: {
                title: 'stkid',
                width: '1%',
                list: false

            }
        }
    });

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();
        oCut.load(oSCuts[i].Pk_IssueReturnDetailsId, oSCuts[i].Fk_StockID, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].ReturnQuantity, oSCuts[i].RollNo, oSCuts[i].Color,oSCuts[i].Mill);
        viewObject.viewModel.data["Materials"].push(oCut);
        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "IssueReturn", "_AddMaterial", function () { return new IssuedMaterial() });

}

function setUpIssueSearch() {

    $('#MainSearhContainer').jtable({
        title: 'Issue List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/MaterialIssueListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },

        fields: {
            Pk_MaterialIssueID: {
                title: 'Issue ID',
                key: true
            },
            IssueDate: {
                title: 'Issue Date',
            },
            //DCNo: {
            //    title: 'Issue No.'
            //},
            JobCardID:{
                title: 'Job Card Id'
            },
            BoxID:{
        title: 'BName'
            },
            
            PName:{
        title: 'Box Part'
            },
            productVal: {
                title: 'Prod',
                list: false
            },

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            CustomerName: $('#txtdlgIssueCustomerName').val(),
            IssueDate: $('#txtdlgIssueFromDate').val(),
            ToIssueDate: $('#txtdlgIssueToDate').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            CustomerName: $('#txtdlgIssueCustomerName').val(),
            IssueDate: $('#txtdlgIssueFromDate').val(),
            ToIssueDate: $('#txtdlgIssueToDate').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MainSearhContainer').jtable('selectedRows');
        if (rows.length > 0) {
            document.getElementById('Fk_IssueId').value = rows[0].keyValue;
            document.getElementById('dtIssueDate').value = rows[0].data["IssueDate"];
            document.getElementById('Product').value = rows[0].data["productVal"];
        }
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/IssueReturn/getIssueDetails',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Fk_Material: {
                title: 'Material Id',
                key: true
            },
            MaterialName: {
                title: 'Material Name'
            },
            Color: {
                title: 'Shade'
            },
            Mill: {
                title: 'Mill'

            },
            RollNo: {
                title: "RollNo"
            },
            Quantity: {
                title: "Quantity"
            },
            Pk_StockID: {
                title: "Stkid",
                list:false
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_IssueId').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_IssueId').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('IssQuantity').value = rows[0].data["Quantity"];
        document.getElementById('ReturnableQty').value = rows[0].data["Quantity"];
        document.getElementById('Pk_StockID').value = rows[0].data["Pk_StockID"];
        document.getElementById('RollNo').value = rows[0].data["RollNo"];
        document.getElementById('Shade').value = rows[0].data["Color"];
        document.getElementById('Mill').value = rows[0].data["Mill"];
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#ReturnQuantity").focus();
    });

}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpIssueMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}

function ReferenceFieldNotInitilized(viewModel) {

   document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
    }
}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["Materials"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["Materials"]()[i].data.Fk_SiteIssueMasterId) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}