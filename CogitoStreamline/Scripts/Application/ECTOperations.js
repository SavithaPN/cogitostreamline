﻿//Name          : Gum Operations ----javascript files
//Description   : Contains the  gum Operations  definition
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckgumDuplicate(duplication checking) 
//Author        : shantha
//Date 	        : 07/08/2015
//Crh Number    : SL0005
//Modifications : 


function initialCRUDLoad() {

    //$('#MainSearchContainer').jtable({
    //    title: 'Gum List',
    //    paging: true,
    //    pageSize: 15,
    //    sorting: false,
    //    defaultSorting: 'Name ASC',
    //    actions: {
    //        listAction: '/Gum/GumListByFiter',
    //        //deleteAction: '',
    //        updateAction: '',
    //        createAction: ''
    //    },
    //    fields: {
    //        Pk_Material: {
    //            title: 'Id',
    //            key: true,
    //            width: '5%'
    //        },
    //        Name: {
    //            title: 'Name'
    //        }
    //        ,
    //        Unit: {
    //            title: 'Unit'
    //        }
    //        ,

    //        Brand: {
    //            title: 'Brand'
    //        }
    //        ,
    //        MaterialType: {
    //            title: 'MaterialType'
    //        }


    //    }
    //});

    ////Re-load records when Staff click 'load records' button.

    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    //    $('#MainSearchContainer').jtable('load', {
    //        Name: $('#TxtGumName').val(),
    //        MaterialType: $('#TxtMatType').val(),
    //        Brand: $('#TxtBrand').val()
    //    });

    //});

    //$('#TxtGumName').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Name: $('#TxtGumName').val(),
    //            MaterialType: $('#TxtMatType').val(),
    //            Brand: $('#TxtBrand').val()
    //        });
    //    }
    //});


    //$('#TxtMatType').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Name: $('#TxtGumName').val(),
    //            MaterialType: $('#TxtMatType').val(),
    //            Brand: $('#TxtBrand').val()
    //        });
    //    }
    //});


    //$('#TxtBrand').keypress(function (e) {
    //    if (e.keycode == 13) {
    //        e.preventDefault();
    //        $('#MainSearchContainer').jtable('load', {
    //            Name: $('#TxtGumName').val(),
    //            MaterialType: $('#TxtMatType').val(),
    //            Brand: $('#TxtBrand').val()
    //        });
    //    }
    //});
    ////Load all records when page is first shown
    //$('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });




    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Unit", "getUnit", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Unit", "getUnit", null);


}
function afterNewShow(viewObject) {
    // viewObject.viewModel.data["Category"] = "Gum";

    document.getElementById('ECT').value = 32;

   
    

}


//function beforeModelSaveEx() {
//    var viewModel = _page.getViewByName('New').viewModel;
//    //viewModel.data["Name"] = $.trim($('#MILL option:selected').text()) + " " + $.trim($('#BF').val()) + "BF " + $('#PaperType  option:selected').text() + " " + $.trim($('#GSM').val()) + "GSM"
//    //viewModel.data["BCT"] =


//    viewModel.data["BCT"] = CalcVal;

//}


function Calc()
{

    var L = document.getElementById('Length').value;

    var W = document.getElementById('Width').value;

    var ect = document.getElementById('ECT').value;

    var CAL = document.getElementById('Caliper').value;
    var s1=Number(L)+ Number(W);
    var peri = 2 * Number(s1);

    var CalcVal = 5.87 * ect * Math.sqrt(peri);


    document.getElementById('BCT').value = CalcVal;
}