﻿var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Tax List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Taxx/TaxListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            PkTax: {
                title: 'Id',
                key: true,
                list: true
            },
            TaxName: {
                title: 'Name',
                width: '25%'
            },
            TaxValue: {
                title: 'Value',
                width: '25%'
            }
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            PkTax: $('#txtTaxId').val(),
            TaxValue: $('#txtTaxValue').val(),
            TaxName: $('#txtTaxName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckTaxDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("TaxName", $('#TaxName').val());
    oResult = _comLayer.executeSyncAction("Taxx/TaxDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Tax Name Already added";
}


