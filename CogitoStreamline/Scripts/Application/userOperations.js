﻿var objShortCut = null;
function initialCRUDLoad() {
   
    $('#MainSearchContainer').jtable({
        title: 'User List',
        paging: true,
        pageSize: 15,
        sorting: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/User/UserListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Users: {
                title: 'UserName',
                key: true,
                list: false
            },
            UserName: {
                title: 'UserName',
                edit: false
            },
            FirstName: {
                title: 'FirstName'
            },
            LastName: {
                title: 'LastName'
            },
            Password: {
                title: 'Password',
                type: 'password',
                list: false
            },
            Company: {
                title: 'Company'
            },
            Office: {
                title: 'Office'
            }
        }
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val()
        });

    });
    
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    //Add addtional data sources (Combobox methods)

    _page.getViewByName('New').viewModel.addAddtionalDataSources("Tenants", "getTenants", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Tenants", "getTenants", null);

}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Password"] = $('#UserName').val();
}
