﻿
//Name          : Country Operations ----javascript files
//Description   : Contains the  PaperType Operations  definition with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  3. CheckCountryDuplicate(duplication checking)
//Author        : Nagesh V Rao
//Date 	        : 07/08/2015
//Crh Number    : SL0010
//Modifications : 
var curViewModel = null;
var objShortCut = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Complaints List',
        paging: true,
        pageSize: 8,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Complaints/CmpListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_ComplaintID: {
                title: 'Id',
                key: true,
                list: true
            },
            ComplaintDate: {
                title: 'Date',
            
            },
            CustomerID: {
                title: 'Customer Name',
                width: '25%'
                // key: true,
                //list: true
            }
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_ComplaintID: $('#txtComplaintID').val(),
            ComplaintDate: $('#txtComplaintDate').val(),
            Fk_CustomerID: $('#txtCustomerName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    setUpCustomerSearch();
    setUpOrderSearch();
    setUpEnquirySearch();

    $('#txtComplaintDate').datepicker({
        autoclose: true
    });


}
function afterNewShow(viewObject) {
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('ComplaintDate').value = dNow.getDate() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getFullYear();
    $('#ComplaintDate').datepicker({ autoclose: true });

    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        //setUpCustomerSearch();
        $("#searchDialog").modal("show");
    });


    $('#cmdOrderSearch').click(function (e) {
        e.preventDefault();
        //   setUpOrderSearch();
        $("#searchDialogOrder").modal("show");
    });


    $('#cmdEnquirySearch').click(function (e) {
        e.preventDefault();
        //    setUpEnquirySearch();
        $("#searchDialogEnquiry").modal("show");
    });

}

function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        $('#Fk_CustomerID').wgReferenceField("setData", rows[0].keyValue);
        $("#searchDialog").modal("hide");
    });

}

function setUpOrderSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldOrderNo = "<input type='text' id='txtdlgOrderNo' placeholder='Order No.' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldOrderdate = "<input type='text' id='txtdlgOrderDate' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFieldsOrder").append(txtFieldOrderNo);
    $("#dlgSearchFieldsOrder").append(txtFieldOrderdate);

    $('#SearchOrderContainer').jtable({
        title: 'Order List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Order/OrderListByFiter',
        },
        fields: {
            Pk_Order: {
                title: 'OrderID',
                key: true
            },
            OrderDate: {
                title: 'OrderDate'
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            
            State: {
                title: 'Status'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchOrderContainer').jtable('load', {
            Pk_Order: $('#txtdlgOrderNo').val(),
            CustomerName: $('#txtdlgOrderDate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdOrderSearch').click(function (e) {
        e.preventDefault();
        $('#SearchOrderContainer').jtable('load', {
            Pk_Order: $('#txtdlgOrderNo').val(),
            CustomerName: $('#txtdlgOrderDate').val()
        });
    });

    $('#cmdOrderDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchOrderContainer').jtable('selectedRows');
        $("#Fk_OrderNo").val(rows[0].data.Pk_Order);
        $("#searchDialogOrder").modal("hide");
    });
}

function beforeModelSaveEx() {
    //   bFromSave = true;

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["ComplaintDate"] = $('#ComplaintDate').val();
    viewModel.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
    viewModel.data["Fk_EnquiryNo"] = $('#Fk_EnquiryNo').val();


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel.data["ComplaintDate"] = $('#ComplaintDate').val();
    viewModel.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
    viewModel.data["Fk_EnquiryNo"] = $('#Fk_EnquiryNo').val();
}
function setUpEnquirySearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldEnquiry = "<input type='text' id='txtdlgEnquiry' placeholder='Enquiry Id' class='input-large search-query'/>&nbsp;&nbsp;";
    
    $("#dlgSearchFieldsEnquiry").append(txtFieldEnquiry);
    
   

    $('#SearchEnquiryContainer').jtable({
        title: 'Enquiry List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Enquiry/EnquiryListByFiter',
        },
        fields: {
            Pk_Enquiry: {
                title: 'EnquiryID',
                key: true
            },
            Date: {
                title: 'Enq.Date'
            },
            Customer: {
                title: 'Customer Name',
                edit: false
            },
            
            Fk_Status: {
                title: 'Status'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchEnquiryContainer').jtable('load', {
            Pk_Enquiry: $('#txtdlgEnquiry').val()
           
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdEnqSearch').click(function (e) {
        e.preventDefault();
        $('#SearchEnquiryContainer').jtable('load', {
            Pk_Enquiry: $('#txtdlgEnquiry').val()
           
        });
    });

    $('#cmdEnqDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchEnquiryContainer').jtable('selectedRows');
        //    $('#Fk_EnquiryNo').wgReferenceField("setData", rows[0].keyValue);
        $("#Fk_EnquiryNo").val(rows[0].data.Pk_Enquiry);
        $("#searchDialogEnquiry").modal("hide");
    });

}
function ReferenceFieldNotInitilized(viewModel) {

    $('#Fk_CustomerID').wgReferenceField({
        keyProperty: "Fk_CustomerID",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });



    if (viewModel.data != null) {
        $('#Fk_CustomerID').wgReferenceField("setData", viewModel.data["Fk_CustomerID"]);
    }



}





function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;

    $('#txtComplaintDate').datepicker({
        autoclose: true
    });


    bEditContext = true;

    $('#wfTransDisplay').wgWorkFlowHistory({
        wgTag: "Complaints",
        viewModel: viewObject.viewModel
    });

    $('#wfButtons').wgWorkFlowButtons({
        wgTag: "Complaints",
        viewModel: viewObject.viewModel,
        stateField: "Fk_Status",
        belongsTo: "Complaints",
        pkField: "Pk_ComplaintID"
    });
}

