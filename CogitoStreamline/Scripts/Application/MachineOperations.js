﻿
var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Machines List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Machine/MachineListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
        },
        fields: {
            Pk_Machine: {
                title: 'Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            //Location: {
            //    title: 'Location',
            //    width: '25%'
            //}

        
    

        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Machine: $('#TxtMachineID').val(),
            Location: $('#TxtLocation').val(),
            Name: $('#TxtName').val()

        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
function CheckMachineNameDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("Machine/MachineNameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Machine Name Already added";
}

function CheckMachineLocationDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Location", $('#Location').val());
    oResult = _comLayer.executeSyncAction("Machine/MachineLocationDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Machine Location Already added";
}










