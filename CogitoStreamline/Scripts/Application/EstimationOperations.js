﻿var objShortCut = null;
var Fk_BoxEstimation = 0;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Estimation List',
        paging: true,
        pageSize: 15,
        actions: {
            listAction: '/Estimation/EstList',
         
        },
        fields: {
            Details: {
                title: 'BoxDet.',
                width: '2%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Box Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Estimation/EstimationListByFiter?Fk_BoxEstimation=' + data.record.Pk_BoxEstimation
                                        },
                                        fields: {
                                            Pk_BoxEstimationChild: {
                                                title: 'ID',
                                                key: true,
                                                list: false
                                            },
                                            Fk_EnquiryChild: {
                                                title: 'Enq Child ID',
                                                key: true,
                                                list: false
                                            },
                                            Fk_BoxID: {
                                                title: 'Box ID',
                                                key: true
                                            },
                                            Name: {
                                                title: 'Box Name',
                                                key: true
                                            },
                                         
                                            Details: {
                                                title: 'Edit Est.',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (row) {
                                                    var button = $("<button class='bnt'> Edit </button>");
                                                    $(button).click(function () {
                                                        Pk_EnqChild = row.record.Fk_EnquiryChild;
                                                        BoxID = row.record.Fk_BoxID;
                                                        Pk_BoxEstChild = row.record.Pk_BoxEstimationChild
                                                        window.open("/FlexEst?Enquiry=" + BoxID + ", EnqChild=" + Pk_EnqChild + ', Fk_BoxEstimation=' + data.record.Pk_BoxEstimation + ', Pk_BoxEstimationChild=' + Pk_BoxEstChild);
                                                    });
                                                    return button;
                                                }
                                            },
                                            BoxDet: {
                                                title: 'Box Details',
                                                width: '2%',
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                                                    $img.click(function () {
                                                       //var BoxID = data.record.Fk_BoxID;
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Box Details',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.Fk_BoxID
                                                                        },
                                                                        fields: {
                                                                            Pk_BoxSpecID: {
                                                                                title: 'Spec ID',
                                                                                key: true,
                                                                                list:false
                                                                            },
                                                                            OYes: {
                                                                                title: 'OuterShell',
                                                                                display: function (data) {
                                                                                    if (data.record.OYes == true || data.record.OYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                           
                                                                                },
                                                                                
                                                                            },
                                                                            CYes: {
                                                                                title: 'Cap',
                                                                                display: function (data) {
                                                                                    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                               
                                                                                },
                                                                            },
                                                                            LPYes: {
                                                                                title: 'L.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                                    //else {
                                                                                    //    width: '2%';
                                                                                    //}
                                                                                },
                                                                            },
                                                                            WPYes: {
                                                                                title: 'W.Partition',
                                                                                display: function (data) {
                                                                                    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                                    
                                                                                },
                                                                            },
                                                                            PYes: {
                                                                                title: 'Plate',
                                                                                display: function (data) {
                                                                                    if (data.record.PYes == true || data.record.PYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }
                                                                                 
                                                                                },
                                                                            },
                                                                            TYes: {
                                                                                title: 'Top',
                                                                                display: function (data) {
                                                                                    if (data.record.TYes == true || data.record.TYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            BYes: {
                                                                                title: 'Bottom',
                                                                                display: function (data) {
                                                                                    if (data.record.BYes == true || data.record.BYes == 'True') {
                                                                                        return 'Yes';
                                                                                    }

                                                                                },
                                                                            },
                                                                            Details: {
                                                                                title: '',
                                                                                width: '1%',
                                                                                sorting: false,
                                                                                paging: true,
                                                                                pageSize: 5,
                                                                                edit: false,
                                                                                create: false,
                                                                                listClass: 'child-opener-image-column',
                                                                                display: function (data) {
                                                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                                                    $img.click(function () {
                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                    $img.closest('tr'),
                                                                                                    {
                                                                                                        title: 'Specifications',
                                                                                                        paging: true,
                                                                                                        pageSize: 5,
                                                                                                        actions: {
                                                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                                                        },
                                                                                                        fields: {
                                                                                                            Pk_PartPropertyID: {
                                                                                                                title: 'Property ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                width: '2%',
                                                                                                                list: false
                                                                                                            },
                                                                                                            Length: {
                                                                                                                title: 'Length',
                                                                                                                key: false,
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Width: {
                                                                                                                title: 'Width',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Height: {
                                                                                                                title: 'Height',
                                                                                                                width: '2%'
                                                                                                            },
                                                                                                            Weight: {
                                                                                                                title: 'Weight',
                                                                                                                width: '2%'
                                                                                                            },

                                                                                                            Details: {
                                                                                                                title: '',
                                                                                                                width: '1%',
                                                                                                                sorting: false,
                                                                                                                paging: true,
                                                                                                                pageSize: 5,
                                                                                                                edit: false,
                                                                                                                create: false,
                                                                                                                listClass: 'child-opener-image-column',
                                                                                                                display: function (data) {
                                                                                                                    var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                                                    $img.click(function () {
                                                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                                                    $img.closest('tr'),
                                                                                                                                    {
                                                                                                                                        title: 'Layers Details',
                                                                                                                                        paging: true,
                                                                                                                                        pageSize: 10,
                                                                                                                                        actions: {
                                                                                                                                            listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                                                        },
                                                                                                                                        fields: {
                                                                                                                                            Pk_LayerID: {
                                                                                                                                                title: 'Layer ID',
                                                                                                                                                key: true,
                                                                                                                                                create: false,
                                                                                                                                                edit: false,
                                                                                                                                                list: false
                                                                                                                                            },
                                                                                                                                            GSM: {
                                                                                                                                                title: 'GSM',
                                                                                                                                                key: false
                                                                                                                                            },
                                                                                                                                            BF: {
                                                                                                                                                title: 'BF'

                                                                                                                                            },
                                                                                                                                            Weight: {
                                                                                                                                                title: 'Weight'
                                                                                                                                            }
                                                                                                                                        },
                                                                                                                                        formClosed: function (event, data) {
                                                                                                                                            data.form.validationEngine('hide');
                                                                                                                                            data.form.validationEngine('detach');
                                                                                                                                        }
                                                                                                                                    }, function (data) { //opened handler
                                                                                                                                        data.childTable.jtable('load');
                                                                                                                                    });
                                                                                                                    });
                                                                                                                    return $img;
                                                                                                                }
                                                                                                            },
                                                                                                        },
                                                                                                        formClosed: function (event, data) {
                                                                                                            data.form.validationEngine('hide');
                                                                                                            data.form.validationEngine('detach');
                                                                                                        }
                                                                                                    }, function (data) { //opened handler
                                                                                                        data.childTable.jtable('load');
                                                                                                    });
                                                                                    });
                                                                                    return $img;
                                                                                }
                                                                            },
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },

            Pk_BoxEstimation: {
                title: 'Estimate Id',
                key: true,
                width: '4%'
            },
            EstDATE: {
                title: 'Est. Date',
                width: '4%'
            },
            Enquiry:{
                title: 'Enq.No.',
                width: '4%'
            },
            BoxID: {
                title: 'Box ID',
                key: true
            },
            BoxName: {
                title: 'Box Name',
                key: true
            },
            Dimension: {
                title: 'Dimension',
                key: true
            },
            Quantity: {
                title: 'Qty.Enq.',
                width: '4%'
            },
            CustomerName: {
                title: 'Customer Name',
            },
            GrandTotal: {
                title: 'Cost/Box',
            },
       
            Print: {
                title: 'Quotation',
                width: '2%',
            
                display: function (row) {
                    //var button = $("<i class='icon-printer'></i>");
                    var button = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdCreate'> Generate Quote </button>");

                    $(button).click(function () {
                        
                        //Fk_BoxEstimation = row.record.Pk_BoxEstimation;
                                           
                        //_comLayer.parameters.add("Fk_BoxEstimation", Fk_BoxEstimation);
                     

                        //var oResult1 = _comLayer.executeSyncAction("/Quote/SaveQuoteDet", _comLayer.parameters);

                        window.open('/Quote');
                    
                    });
                    return button;
                }
            }
        },
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
        }
    }, function (data) { //opened handler
        data.childTable.jtable('load');
    });
    $('#txtEstDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            EstDate: $('#txtEstDate').val()
        });
    });
    $('#txtEstDate').datepicker({
        autoclose: true
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_BoxEstimation: $('#txtEstimate').val(),
            Fk_Enquiry: $('#txtEnquiry').val(),
            Customer: $('#txtCustomer').val(),
            EstDate: $('#txtEstDate').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
  
}

//function ShowBoxEstRecord() {
//    //PkEnqChildId = row.record.Pk_EnquiryChild;
//    //document.getElementById('pk').value = PkEnqChildId;
//    //BoxId = row.record.Fk_BoxID;
//    //document.getElementById('BoxID').value = BoxId;

//    BoxId = 24;
//    PkEnqChildId = 30;

//    window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild=" + PkEnqChildId);

//}


