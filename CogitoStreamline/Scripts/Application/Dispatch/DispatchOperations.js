﻿var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var ssum = 0;
var tempamt = 0;
var intval = 0;
var Indent = 0;
//var invqty = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'PO List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Dispatch/DispatchListByFiter',
            //deleteAction: '',            
            //createAction: '',
            //updateAction: ''
        },
        fields: {
            Pk_Dispatch: {
                title: 'PO No',
                key: true,
                list: true,
                width: '4%'
            },
            Dis_Date: {
                title: 'PO Date',
                width: '4%'
            },

            Fk_Customer: {
                title: 'Customer',
                width: '10%'
            },
            //Status: {
            //    title: 'Status',
            //    width: '10%'
            //},
            //Select_A: {
            //    title: "Close PO Manually",
            //    display: function (row) {
            //        var button1 = $("<button class='bnt'>Close PO</button>");
            //        $(button1).click(function () {
            //            PO = row.record.Pk_Dispatch;
            //            _comLayer.parameters.add("POno", PO);
            //           oResult=_comLayer.executeSyncAction("PurchaseOrder/StatusUpdate", _comLayer.parameters);
            //           if(oResult=true)
            //               {alert("PO Status Updated Successfully")}
                      
            //        });
            //        return button1;
            //    }
            //},
            //Select_B: {
            //    title: "Open PO Manually",
            //    display: function (row) {
            //        var button1 = $("<button class='bnt'>Open PO</button>");
            //        $(button1).click(function () {
            //            PO = row.record.Pk_Dispatch;
            //            _comLayer.parameters.add("POno", PO);
            //            oResult = _comLayer.executeSyncAction("PurchaseOrder/OpenStatus", _comLayer.parameters);
            //            if (oResult = true)
            //            { alert("PO Status Updated Successfully") }

            //        });
            //        return button1;
            //    }
            //},
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        Pk_Dispatch = row.record.Pk_Dispatch;
                        _comLayer.parameters.add("Pk_Dispatch", Pk_Dispatch);
                        _comLayer.executeSyncAction("PurchaseOrder/POrderRep", _comLayer.parameters);
                        var strval = "ConvertPDF/POrder" + Pk_Dispatch + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////


                        //var xhr = new XMLHttpRequest();
                        //var urlToFile = "ConvertPDF/POrder" + Pk_Dispatch + ".pdf"
                        //xhr.open('HEAD', urlToFile, false);
                        //xhr.send();

                        //if (xhr.status == "404") {
                        //    alert('Data Not Available , File does not Exist');
                        //    return false;


                        //} else {
                        //    window.open(strval, '_blank ', 'width=700,height=250');
                        //    return true;
                        //}


                        /////////////////////////
                      

                    });
                    return button;
                }
            },
            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/PurchaseOrder/POGetRec?Pk_Dispatch=' + data.record.Pk_Dispatch

                                        },
                                        fields: {
                                            
                                          
                                            MaterialName: {
                                                title: 'MaterialName'
                                            },
                                          
                                            Quantity: {
                                                title: 'Quantity',
                                                key: true,
                                            },
                                            Color: {
                                                title: 'Shade',
                                                key: true,
                                            },
                                            Mill: {
                                                title: 'Mill',
                                                key: true,
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },

            ///////////////////////////////////////////////
        }
    });

    //Re-load records when Customer click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_Dispatch: $('#txtPkPONo').val(),
            Dis_Date: $('#txtPODate').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#txtPODate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Dis_Date: $('#txtPODate').val()
        });
    });
    $('#txtPODate').datepicker({
        autoclose: true
    });

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    $('#dtDate').datepicker({ autoclose: true });


    $('#Pk_Dispatch').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            $('#MainSearchContainer').jtable('load', {
                Pk_Dispatch: $('#txtPkPONo').val()
            });
        }
    });

    $('#dtDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Dis_Date: $('#txtPODate').val()
        });
    });
    setUpCustomerSearch();
}

function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    //var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    //$("#dlgSearchFields").append(txtFieldCustomerName);
    //$("#dlgSearchFields").append(txtFieldCustomerNam);
    //$("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#MainSearhContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            CustomerAddress: {
                title: 'Address'
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            },
            Email: {
                title: 'Email'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            //Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgVendorName').val(),
            //CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
           // Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgVendorName').val(),
         //   CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        //var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        //$('#txtFk_Box').wgReferenceField("setData", rows[0].keyValue);

        var rows = $('#MainSearhContainer').jtable('selectedRows');
        $('#txtCustomer').wgReferenceField("setData", rows[0].keyValue);
        //CustId = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtDis_Date').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtDis_Date').datepicker({ autoclose: true });


    $('#cmdOrdersSearch').click(function (e) {
        e.preventDefault();
        //setUpOrderSearch(viewObject);
        $("#searchDialog").modal("show");
    });

    $('#divDeliveryschedule').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Dispatch/Bounce',
            deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },          
            txtFk_Box: {
                title: 'Id',
                key: false,
                list: true
            },
            Fk_BoxID: {
                title: 'Id',
                key: false,
                list: true
            },
            Name: {
                title: 'Box Name'
            },
          
            Quantity: {
                title: 'Quantity',
                width: '1%'
            
        },
            Weight: {
                title: 'Weight'

            },
            Bundle: {
                title: 'Bundle'

            },
        NosInBundle: {
            title: 'Nos.In Bundle',
            width: '1%'
        },
        NoOfRows: {
            title: 'No.Of Rows',
            width: '1%'

        },
        NoOfColumns: {
            title: 'No.Of Columns',
            width: '1%'

        }
        }
    });

   
    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveMaterial", viewObject, "DispatchDetails", "Dispatch", "_AddMaterial", function () { return new IssuedMaterial() });

 

    //Calc();

    //$('#cmdVendorMSearch').click(function (e) {
    //    e.preventDefault();
    //    setUpVendorSearch(viewObject);
    //    $("#searchDialog").modal("show");
    //    $("#searchDialog").width(600);
    //    $("#searchDialog").height(500);

    //});


    //$('#cmdMIndentSearch').click(function (e) {
    //    e.preventDefault();
    //    setUpIndentSearch();
    //    $("#searchIndentDialog").modal("show");
    //    //$("#dataDialog").width(700);
    //    //$("#dataDialog").height(500);

    //    $("#searchIndentDialog").css("top", "50px");
    //    $("#searchIndentDialog").css("left", "350px");
    //});
}


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;

    Calc();

    viewModel.data["Dis_Date"] = $('#dtDate').val();
    viewModel.data["Fk_Customer"] = VendorID;
    viewModel.data["Fk_Indent"] = $('#Fk_Indent').val();
   // viewModel.data["TaxType"] = $('#TaxType').val();
    viewModel.data["NETVALUE"] = $('#NETVALUE').val();
    viewModel.data["GrandTotal"] = $('#GrandTotal').val();
    viewModel.data["CGST"] = $('#CGST').val();
    viewModel.data["SGST"] = $('#SGST').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Dis_Date"] = $('#dtDate').val();
    viewModel1.data["Fk_Customer"] = $('#Fk_Customer').val();
    viewModel1.data["Fk_Customer"] = VendorID;
    viewModel1.data["Fk_Indent"] = $('#Fk_Indent').val();
    viewModel1.data["NETVALUE"] = $('#NETVALUE').val();
    viewModel1.data["GrandTotal"] = $('#GrandTotal').val();
    viewModel.data["CGST"] = $('#CGST').val();
    viewModel.data["SGST"] = $('#SGST').val();
   // viewModel1.data["TaxType"] = $('#TaxType').val();
    viewModel1.data["Amount"] = $('#Amount').val();
    viewModel1.data["Rate"] = $('#Rate').val();

    //var statVal = document.getElementById('txtClose').value;

    //if (statVal == 1) {
    //    viewModel1.data["Fk_Status"] = 4;
    //}

  
    //else { viewModel1.data["Fk_Status"] = 1; }
}

function Calc() {
    var GTot = document.getElementById('GrandTotal').value;
    var TaxVal =  Number(GTot * 0.06);
    document.getElementById('CGST').value = TaxVal;
    document.getElementById('SGST').value = TaxVal;

    document.getElementById('NETVALUE').value = Number(TaxVal * 2) + Number(GTot);

}


function afterOneToManyDialogShow(property) {

  



    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpBoxSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");

    });
}
function setUpBoxSearch(viewObject) {
    //Enquiry

    // cleanSearchDialog();

    //Adding Search fields
    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldPartNo = "<input type='text' id='txtdlgPartNo' placeholder='PartNo' class='input-large search-query'/>";


    $('#MaterialSearchContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter'
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdBoxDone').click();
            });
        },
        fields: {
            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            PartNo: {
                title: 'PartNo',
                width: '25%'
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            //Description: {
            //    title: 'Description',
            //    width: '25%'
            //},
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgBoxName').val(),
            PartNo: $('#txtdlgPartNo').val()

        });

    });
    //$('#txtdlgEnquiryToDate').datepicker({
    //    autoclose: true
    //});
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgBoxName').val(),
            PartNo: $('#txtdlgPartNo').val()

        });
    });


    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');


        //document.getElementById('Fk_BoxID').value = rows[0].keyValue;
        // document.getElementById('TxtIndent').value = rows[0].data.IndentNo;
        //document.getElementById('TxtMaterial').value = rows[0].data.Fk_Material;
        //document.getElementById('TxtMaterialId').value = rows[0].data.Fk_Material1;

        $('#txtFk_Box').wgReferenceField("setData", rows[0].data.Pk_BoxID);
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Quantity").focus();
        //document.getElementById('Quantity').value = rows[0].data.PendQty;
        //document.getElementById('SQuantity').value = rows[0].data.SQuantity;
        //document.getElementById('Unit').value = rows[0].data.UnitName;
        ////document.getElementById('Unit1').value = rows[0].data.UnitName;
        ////document.getElementById('Unit2').value = rows[0].data.UnitName;
        //document.getElementById('TanentName').value = rows[0].data.TanentName;

    });

}


//function afterEditShow(viewObject) {
//    //curEdit = true;
//    curViewModel = viewObject.viewModel;
//    $('#dtDate').datepicker({ autoclose: true });
//    curEdit = true;


//    $('#tlbMaterials').jtable({
//        title: 'Item List',
//        paging: true,
//        pageSize: 10,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/PurchaseOrder/Bounce',
//            deleteAction: '',
//            updateAction: ''
//        },
//        fields: {
//            slno: {
//                title: 'slno',
//                key: true,
//                list: false
//            },
//            Fk_Material: {
//                title: 'Id',
//                key: false,
//                list: false
//            },
//            Name: {
//                title: 'Material Name',
//                width: '5%'
//            },
//            Quantity: {
//                title: 'Quantity',
//                width: '1%'
//            },

//            Rate: {
//                title: 'Rate',
//                width: '1%'
//            },
//            Amount: {
//                title: 'Amount',
//                width: '1%'

//            }
//        }
//    });

//    //if ($.trim($('#TaxType').val()) == "VAT") {
//    //    $("#VAT").prop("checked", true);
//    //}
//    //else if ($.trim($('#TaxType').val()) == "CST") {
//    //    $("#CST").prop("checked", true);

        

        
//    }

//    var oSCuts = viewObject.viewModel.data.MaterialData();
//    viewObject.viewModel.data["Materials"] = ko.observableArray();
//    var i = 0;
//    ssum = 0;
//    while (oSCuts[i]) {
//        var oCut = new IssuedMaterial();
//        //(Pk_DispDet, Name, Fk_BoxID, Quantity, Weight, Bundle, NosInBundle, NoOfRows
//        oCut.load(oSCuts[i].Pk_PODet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity, oSCuts[i].Rate, oSCuts[i].Amount);
//        viewObject.viewModel.data["Materials"].push(oCut);

//        i++;
//    }
  
//    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "PurchaseOrder", "_AddMaterial", function () { return new IssuedMaterial() });


    //var POstatus = viewObject.viewModel.data["Fk_Status"]();

    //if (POstatus == "4") {
    //    $("#chkCloseJC").attr('checked', true);
    //}
//}

function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true,
                width:'2%'
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Customer').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Customer').wgReferenceField("setData", rows[0].data.Fk_Customer);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}

function setUpIssueMaterialSearch() {
    Indent = document.getElementById('Fk_Indent').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/getMaterialIndentDetails?Indent=' + Indent,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            MatIndent: {
                title: 'IndentNo.'
            },
            MaterialName: {
                title: 'Material Name'
            },
           
            Color: {
                title: "Shade"
            },
            Mill: {
                title: "Mill"
            },
             Quantity: {
                title: "Quantity"
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Customer').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            Pk_MaterialIssueID: $('#Fk_Customer').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('Mill').value = rows[0].data["Mill"];
        document.getElementById('Shade').value = rows[0].data["Color"];
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Rate").focus();
    });



}

//function afterOneToManyDialogShow(property) {

//    $('#cmdSearchMaterialsdetails').click(function (e) {
//        e.preventDefault();
//        setUpIssueMaterialSearch();
//        _util.setDivPosition("divSearchMaterial", "block");
//        _util.setDivPosition("divCreateMaterial", "none");
//    });
//}

function ReferenceFieldNotInitilized(viewModel) {

    //document.getElementById('ReturnableQty').style.display = "none";

    if (objContextEdit == true) {
        $('#cmdSearchMaterialsdetails').attr('disabled', true);
    }
    else if (objContextEdit == false) {
        $('#cmdSearchMaterialsdetails').attr('disabled', false);
    }
    $('#txtCustomer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });

    $('#txtFk_Box').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });

    //$('#Fk_Indent').wgReferenceField({
    //    keyProperty: "Fk_Indent",
    //    displayProperty: "Fk_Indent",
    //    loadPath: "MaterialIndent/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        $('#txtFk_Box').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
        $('#txtCustomer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
     //   $('#Fk_Indent').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

//function checkDuplicate() {

//    if (objContextEdit == false) {
//        var i = 0;

//        while (curViewModel.data["DispatchDetails"]()[i]) {
//            if (objContext.data.Fk_Material == curViewModel.data["DispatchDetails"]()[i].data.Fk_Material && objContext.data.Fk_SiteId == curViewModel.data["DispatchDetails"]()[i].data.Fk_SiteId && $('#Fk_SiteIssueMasterId1').val() == curViewModel.data["DispatchDetails"]()[i].data.Fk_SiteIssueMasterId) {

//                return "* " + "Item Already added";
//            }
//            i++;
//        }
//    }
//}

function checkstock() {
    if (objContextEdit == false) {

        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {

        }
    } else if (objContextEdit == true) {
        if (Number($('#ReturnableQty').val()) < Number($('#ReturnQuantity').val())) {
            return "* " + "Return Quantity Excceded Invoice Quantity. Return Quantity:" + $('#ReturnableQty').val();
        }
        else {
        }
    }
}
function setUpIndentSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader1").text("Indent Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='IndentNo' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";




    $('#IndentSearhContainer').jtable({
        title: 'Indent List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialIndent/MaterialIndentPOList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdIndentDone').click();
            });
        },

        fields: {
            Pk_MaterialOrderMasterId: {
                title: 'Indent Number',
                key: true
            },

            MaterialIndentDate: {
                title: 'Indent Date'
            },
            //CustomerName=p.CustomerName,
            //MaterialName=p.Name,
            CustomerName: {
                title: 'CustomerName'
            },
            MaterialName: {
                title: 'MaterialName'
            },
                   Color: {
                title: 'Shade'
                   },
                   Mill: {
                       title: 'Mill'
                   },
            Quantity: {
                title: 'Qty'
            },
            //StateName: {
            //    title: 'Status'
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val()

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdIndentSearch').click(function (e) {
        e.preventDefault();

        $('#IndentSearhContainer').jtable('load', {
            Pk_MaterialIndent: $('#txtdlgIndent').val(),
            Vendor: $('#txtdlgVendor').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdIndentDone').click(function (e) {
        e.preventDefault();
        var rows = $('#IndentSearhContainer').jtable('selectedRows');
        $("#searchIndentDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('Fk_Indent').value = rows[0].keyValue;
         IndentNo = rows[0].keyValue;

        $('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}
function CalcAmt() {
  //  curViewModel = viewObject.viewModel;
    if  (objContextEdit == false) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));

            
                   }
   
        else {
        }
    }else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > 0 && Number($('#Rate').val()) > 0) {
            document.getElementById('Amount').value = (Number($('#Quantity').val()) * Number($('#Rate').val()));
        }
        else {
        }


    
    }
}

function radioClass1(intval) {
    if (intval == 1) {


        document.getElementById('TaxType').value = "VAT";
        if (FORM_CT3.checked == true)
        { document.getElementById('ED').value = Number($('#GrandTotal').val());}
        else {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
        }

        FORM_CT3.style.visibility = 'visible';
        FORM_H.style.visibility = 'visible';

        //    fct3.style.visibility = 'hidden';
        fct3.style.visibility = 'visible';
        fh.style.visibility = 'visible';

        if (Number($('#ED').val()) > 0) {
            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.055));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));

            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.055);
            //  NETVALUE
        }

    }
    else if (intval == 2) {

        document.getElementById('TaxType').value = "CST";

        FORM_H.style.visibility = 'hidden';

        fh.style.visibility = 'hidden';


        if (Number($('#ED').val()) > 0) {

            document.getElementById('NETVALUE').value = 0;
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + (Number($('#ED').val() * 0.02));
        }

        else if (Number($('#ED').val()) == 0) {
            document.getElementById('ED').value = Number($('#GrandTotal').val()) + (Number($('#GrandTotal').val() * 0.06));
            document.getElementById('NETVALUE').value = Number($('#ED').val()) + Number($('#GrandTotal').val() * 0.02);
        }

    }

 
}

