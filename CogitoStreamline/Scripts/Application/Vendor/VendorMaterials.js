﻿function VendorMaterials() {
    this.data = new Object();
    this.data["Fk_Material"] = "";
    this.data["MaterialName"] = "";
    this.data["Rate"] = "";
    this.data["Pk_VendorMaterial"] = "";

    this.updateValues = function () {
        this.data["MaterialName"] = $('#txtFk_Material').val();
        this.data["Rate"] = 1;
    };
        
    this.load = function (Fk_Material, MaterialName, Rate, Pk_VendorMaterial) {
        this.data["Fk_Material"] = Fk_Material;
        this.data["MaterialName"] = MaterialName;
        this.data["Rate"] = 1;
        this.data["Pk_VendorMaterial"] = Pk_VendorMaterial;
    };
}