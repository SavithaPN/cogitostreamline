﻿

var curViewModel = null;
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
            //deleteAction: '',
            updateAction: '',
            createAction: ''
        },
        fields: {
            ContactDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-phone' title='Contacts'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Contacts List',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Vendor/VendorContacts?Pk_Vendor=' + data.record.Pk_Vendor
                                        },
                                        fields: {
                                            Pk_VendorContacts: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            ContactPersonName: {
                                                title: 'Person Name',
                                                key: false
                                            },
                                            ContactPersonDesignation: {
                                                title: 'Designation'

                                            },
                                            ContactPersonNumber: {
                                                title: 'Contact Number',
                                                width: '2%'
                                            },
                                            ContactPersonEmailId: {
                                                title: 'Email',
                                                width: '2%'
                                            }
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
            MaterialDetails: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img1 = $("<i class='icon-tools' title='Materials'></i>");
                    $img1.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img1.closest('tr'),
                                    {
                                        title: 'Material List',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/Vendor/VendorMaterials?Pk_Vendor=' + data.record.Pk_Vendor
                                        },
                                        fields: {
                                            Pk_VendorMaterial: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            Name: {
                                                title: 'Name'
                                            },
                                            ColorName: {
                                                title: 'Color'
                                            },
                                            MillName: {
                                                title: 'Mill'
                                            },
                                            PaperTypeName: {
                                                title: 'Paper Type'
                                            }
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img1;
                }
            },
            Pk_Vendor: {
                title: 'Id',
                key: true,
                width: '2%'
            },
            //VendorType: {
            //    title: 'Vendor Type',
            //    width: '5%'
            //},
            VendorName: {
                title: 'Vendor Name'
            },
            City: {
                title: 'City',
                width: '4%'
            },
            LandLine: {
                title: 'Land Line'
            },
            LandLine: {
                title: 'Land Line'
            },
            Email: {
                title: 'Email'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            VendorName: $('#txtSearchVendorName').val(),
            Pk_Vendor: $('#txtSearchPkVendor').val(),
            VendorContact: $('#txtSearchVendorContact').val(),
            LandLine: $('#txtSearchVendorLandLine').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("city", "getCity", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("State", "getState", null);
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Country", "getCountry", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Country", "getCountry", null);

    $('#txtSearchVendorName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                VendorName: $('#txtSearchVendorName').val()
            });
        }
    });

    $('#txtSearchPkVendor').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Vendor: $('#txtSearchPkVendor').val()
            });
        }
    });

    $('#txtSearchVendorContact').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                VendorContact: $('#txtSearchVendorContact').val()
            });
        }
    });

    $('#txtSearchVendorLandLine').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                LandLine: $('#txtSearchVendorLandLine').val()
            });
        }
    });

    $("#dataDialog").width(900);
    $("#dataDialog").css("top", "10px");
    $("#dataDialog").css("left", "400px");

    $('#btnCloseLayout').click(function (e) {
        e.preventDefault();
        $("#frmDataDialog").validationEngine('hide');
    });

}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["VendorType"] = $.trim($('#VendorType').val());

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["VendorType"] = $.trim($('#VendorType').val());
}


function afterNewShow(viewObject) {
    document.getElementById('VendorType').value = "Gold";
    $("#Fk_Gold").prop("checked", true);
    curViewModel = viewObject.viewModel;
    $('#contacts').wgContact({
        viewModel: viewObject.viewModel,
        property: "Contacts"
    });
    $('#tlbProducts').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/Bounce',
            deleteAction: '',
            //updateAction: '',
            createAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            MaterialName: {
                title: 'Material Name'
            },
            Rate: {
                title: 'Rate',
                list:false
            }

        }
    });

    configureOne2Many("#cmdNewMaterial", '#tlbProducts', "#cmdAddToList", viewObject, "Materials", "Vendor", "NOTUSED", function () { return new VendorMaterials() }, "divMaterials");
    
    $('#Fk_Country').change(function () {
        if ($('#Fk_Country').val() != '') {
            loadComboState('#Fk_State', $('#Fk_Country').val());
            $('#Fk_State').change();
        }
    });

    $('#Fk_State').change(function () {
        if ($('#Fk_State').val() != '') {
            loadComboCity('#Fk_City', $('#Fk_State').val());
            $('#Fk_City').change();
        }
    });

    //$('#cmdAddToList').click(function (e) {
    //    e.preventDefault();

    //    $("#cmdCreate").attr('disabled', false);
    //    //configureOne2Many("#cmdNewMaterial", '#tlbProducts', "#cmdAddToList", viewObject, "Materials", "Vendor", "NOTUSED", function () { return new VendorMaterials() }, "divMaterials");



    //});
}

function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;

    if ($.trim($('#VendorType').val()) == "Gold") {
        $("#Fk_Gold").prop("checked", true);
    }
    else if ($.trim($('#VendorType').val()) == "Silver") {

        $("#Fk_Silver").prop("checked", true);
    }
    else if ($.trim($('#VendorType').val()) == "Normal") {
        $("#Fk_Normal").prop("checked", true);
    }
    $('#contacts').wgContact({
        viewModel: viewObject.viewModel,
        property: "Contacts"
    });
    $('#tlbProducts').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/Bounce',
            deleteAction: '',
            //updateAction: '',
            createAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            MaterialName: {
                title: 'Material Name'
            },
            Rate: {
                title: 'Rate',
                list:false
            }
        }
    });

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        //Fk_Material, name, Rate, Pk_VendorMaterial
        var oCut = new VendorMaterials();
        oCut.load(oSCuts[i].Fk_Material, oSCuts[i].MaterialName, oSCuts[i].Rate, oSCuts[i].Pk_VendorMaterial);
        viewObject.viewModel.data["Materials"].push(oCut);
        i++;
    }
    configureOne2Many("#cmdNewMaterial", '#tlbProducts', "#cmdAddToList", viewObject, "Materials", "Vendor", "NOTUSED", function () { return new VendorMaterials() }, "divMaterials");
}

function setUpMaterialSearch(viewObject) {

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Material/MaterialListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {
            Pk_Material: {
                title: 'Id',
                key: true
            },
            Name: {
                title: 'Name'
            },
            MillName: {
                title: 'Mill'
            },
            //Length: {
            //    title: 'Length'
            //},
            //Width: {
            //    title: 'Width'
            //},
            GSM: {
                title: 'GSM'
            },
            BF: {
                title: 'BF'
            }
        }
    });

    //Re-load records when user click 'load records' button.
    //$('#cmdMaterialSearch').click(function (e) {
    //    e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtFk_MaterialName').val(),
            //Length: $('#txtFk_Length').val(),
            //Width: $('#txtFk_Width').val(),
            GSM: $('#txtFk_GSM').val(),
            BF: $('#txtFk_BF').val(),
            Fk_Mill: $('#txtFk_Mill').val()
        });
    //});

    $('#txtFk_MaterialName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MaterialSearchContainer').jtable('load', {
                Name: $('#txtFk_MaterialName').val()
            });
        }
    });
    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            GSM: $('#txtFk_GSM').val(),
            BF: $('#txtFk_BF').val(),
            Fk_Mill: $('#txtFk_Mill').val()
        });
        });
    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $("#dataDialog").modal("hide");
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Rate').value = 1;
    });
}

function afterOneToManyDialogShow(property) {
   // configureOne2Many("#cmdNewMaterial", '#tlbProducts', "#cmdAddToList", viewObject, "Materials", "Vendor", "NOTUSED", function () { return new VendorMaterial() }, "divMaterials");
    $("#cmdSearchMaterialsdetails").click(function (e) {
        e.preventDefault();
        _util.displayView("Vendor", "_AddMaterial", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").modal("show");
        setUpMaterialSearch();
    });
}

function ReferenceFieldNotInitilized(viewModel) {
    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    if (viewModel.data != null)
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
}

function showOnlyGold() {
    $('#MainSearchContainer').jtable('load', {
        VendorType: "Gold",
    });
}

function showOnlySilver() {
    $('#MainSearchContainer').jtable('load', {
        VendorType: "Silver",
    });
}

function showOnlyNormal() {
    $('#MainSearchContainer').jtable('load', {
        VendorType: "Normal",
    });
}


function showAll() {
    $('#MainSearchContainer').jtable('load', {
        VendorType: "",
    });
}

function radioVClass(str1) {

    if (str1 == "Gold") {
        document.getElementById('VendorType').value = str1;
    }
    else if (str1 == "Silver") {
        document.getElementById('VendorType').value = str1;
    }
    else if (str1 == "Normal") {
        document.getElementById('VendorType').value = str1;
    }
}


// Validation has to be manually done as validation does not support nested froms
function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;
    if ($("#txtFk_Material").val() == "") {
        $("#txtFk_Material").validationEngine('showPrompt', 'Please select material', 'error', true)
        bValidation = false;
    }
    if ($("#Rate").val() == "") {
        $("#Rate").validationEngine('showPrompt', 'Enter Rate', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Rate").val())) {
        $("#Rate").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }
    return bValidation;
}

function resetOneToManyForm(property) {
    $("#txtFk_Material").val("");
}

var str;
function radioClass(str1) {
    if (str1 == "Paper") {
        _util.setDivPosition("divPaper", "block");
        _util.setDivPosition("divOther", "none");
    }
    else if (str1 == "Other") {
        _util.setDivPosition("divOther", "block");
        _util.setDivPosition("divPaper", "none");
    }
    $("#txtFk_MaterialName").val("");
    $("#txtFk_Length").val("");
    $("#txtFk_Width").val("");
    $("#txtFk_GSM").val("");
    $("#txtFk_BF").val("");
}


function loadComboState(combo, parent) {
    var oResult = getStateComboValues(parent);
    $(combo).empty();

    var option = $("<option value=''>Select</option>");
    $(combo).append(option);

    if (oResult.Success) {
        var i = 0;

        while (oResult.Records[i]) {
            var option = $("<option value='" + oResult.Records[i].Pk_StateID + "'>" + oResult.Records[i].StateName + "</option>");
            $(combo).append(option);
            i++;
        }
    }
}

function getStateComboValues(parent) {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("CountryId", parent);

    var sPath = _comLayer.buildURL("State", null, "StateListByCountry");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    var oResultObject = oResult;

    return oResultObject;
}

function loadComboCity(combo, parent) {
    var oResult = getCityComboValues(parent);
    $(combo).empty();

    var option = $("<option value=''>Select</option>");
    $(combo).append(option);

    if (oResult.Success) {
        var i = 0;

        while (oResult.Records[i]) {
            var option = $("<option value='" + oResult.Records[i].Pk_CityID + "'>" + oResult.Records[i].CityName + "</option>");
            $(combo).append(option);
            i++;
        }
    }
}

function getCityComboValues(parent) {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("StateId", parent);

    var sPath = _comLayer.buildURL("City", null, "CityListByState");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    var oResultObject = oResult;

    return oResultObject;
}

function CheckVendorDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("VendorName", $('#VendorName').val());
    oResult = _comLayer.executeSyncAction("Vendor/VendorDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Vendor Already added";
}

function ContactDuplicateChecking() {
    if ($("#Pk_Vendor").val() != "") {
        _comLayer.parameters.clear();
        var oResult;
        _comLayer.parameters.add("ContactPersonName", $('#ContactPersonName').val());
        _comLayer.parameters.add("Pk_Vendor", $("#Pk_Vendor").val());
        oResult = _comLayer.executeSyncAction("Vendor/ContactDuplicateChecking", _comLayer.parameters);
        if (oResult.TotalRecordCount > 0)
            return "* " + "Vendor Contact Already added";
    }
}