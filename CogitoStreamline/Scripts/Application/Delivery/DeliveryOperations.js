﻿
var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var CustCode = "";
var OrdID;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Production Schedule List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/DelSchedule/DelListByFiter',
            updateAction: '',
            createAction: '',
        },
        
        fields: {
            Pk_DeliverySechedule: {
                title: 'Schd.No',
                key: true,
                width: '5%'
            }
            ,  Fk_Order: {
                title: 'Order No.',
                width: '5%'
            },
            CustName: {
                title: 'Cust.Name',
                width: '5%'
            },
            BName: {
                title: 'BoxName'
            },
            PartID: {
                title: 'Part ID',
                list:false
            },
            PartName: {
                title: 'Part Name'
            },
            Quantity: {
                title: 'Sch.Qty',
                width: '5%'
            },
            OrdQty: {
                title: 'Ord.Qty',
                width: '5%'
            },
            //Pending: {
            //    title: 'Pending Qty',
            //    width: '5%'
            //},
            DeliveryDate: {
                title: 'DeliveryDate'
            },
         
            Fk_BoxID: {
                title: 'Fk_BoxID',
                list:false
            },
          
           
            DeliveredDate: {
                title: 'DeliveredDate',
                width: '5%'
            },
            DeliveryCompleted: {
                title: 'Del.Completed',
                display: function (data) {
                    if (data.record.DeliveryCompleted == true || data.record.DeliveryCompleted == 'True') {
                        return 'Yes';
                    }
                    else {
                        return 'No';
                    }
                },
                width: '2%'
            },

            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Box Dimension',
                                        actions: {
                                            listAction: '/DelSchedule/DelGetRec?Pk_DeliverySechedule=' + data.record.Pk_DeliverySechedule + ',' + data.record.PartID

                                        },
                                        fields: {
                                            //PName = p.PName,
                                            //GSM=p.GSM,
                                            //BF=p.BF,
                                            //Weight=p.Weight,
                                            //BoxName=p.Name,
                                            //Deckel=p.Deckle,
                                            //CuttingSize=p.CuttingSize
                                            Pk_PartPropertyID: {
                                                title: 'Part ID'
                                            },
                                            PName: {
                                                title: 'PName'
                                            },

                                            Weight: {
                                                title: 'Weight',
                                                key: true,
                                            },
                                            Deckel: {
                                                title: 'Deckel',
                                                key: true,
                                            },
                                            CuttingSize: {
                                                title: 'CuttingSize',
                                                key: true,
                                            },
                                            GSM: {
                                                title: 'GSM',
                                                key: true,
                                            },
                                            BF: {
                                                title: 'BF',
                                                key: true,
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },

            ///////////////////////////////////////////////
            Details: {
                title: 'Tot.Schd',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Total Stock',
                                        actions: {
                                            listAction: '/DelSchedule/TotalScheduled?OrderNo=' + data.record.Fk_Order
                                        },
                                        fields: {

                                            //Pk_Order= p.Pk_Order,
                                            //OrdQty=   p.OrdQty,
                                            //TotPendingQty= p.TotPendingQty,
                                            //TotSchQty=   p.TotSchQty
                                            OrdQty: {
                                                title: 'OrdQty',
                                             //   list: false
                                            },
                                          
                                            TotSchQty: {
                                                title: 'TotSchQty'
                                            },
                                            TotPendingQty: {
                                                title: 'TotPendingQty'
                                            },

                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            //////////////////////////////////////
        }
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            BoxName: $('#TxtCustomerName').val(),
            Pk_DeliverySechedule: $('#TxtOrderNo').val(),
            FromDate: $('#TxtFromDate').val(),
            ToDate: $('#TxtToDate').val(),
            Fk_Order: $('#TxtOrdNo').val(),
            CustName: $('#TxtCust').val(),
            
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();
    
    $('#cmdJC').click(function (e) {
        e.preventDefault();
        window.open("../JobCard");

    });
    $('#cmdPPC').click(function (e) {
        e.preventDefault();
        window.open("../Reports?Rep=51");

    });
     
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
    $('#dtDeliveredDate').datepicker({ autoclose: true });
    $('#txtFromDate').datepicker({ autoclose: true });
    $('#txtToDate').datepicker({ autoclose: true });
    $("#searchDialog").width(1000);
    $("#searchDialog").css("top", "50px");
    $("#searchDialog").css("left", "300px");

    $('#dtDdate').datepicker({ autoclose: true });
  
}

function afterNewShow(viewObject) {

    bEditContext = false;
    //var dNow = new Date();
    //document.getElementById('dtPOrderDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();


    OrdID = document.getElementById('Fk_Order').value;
    $('#dtDeliveredDate').datepicker({ autoclose: true });
    
    $('#cmdOrdersSearch').click(function (e) {
        e.preventDefault();
        setUpOrderSearch(viewObject);
        $("#searchDialog").modal("show");
    });


    curViewModel = viewObject.viewModel;


    $('#divDeliveryschedule').jtable({
        title: 'Box Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/DelSchedule/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            DeliveryDate: {
                title: 'DDate',
                list: true
            },
            Fk_BoxID: {
                title: 'BoxID',
                list: false
            },

            Name: {
                title: 'BoxName',
            },
            Fk_PartID: {
                title: 'PartID',
                list:false
            },
            txtPart: {
                title: 'Part Name',
            },
           
            OrdQty: {
                title: 'OrdQty',
                width: '2%'
            },
            
            Quantity: {
                title: 'Sch.Qty',
                width: '25%'

            },
        }
    });

    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "DelSchedule", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });



    //Pk_StkID = p.Pk_StkID,
    //           Stock = p.Stock,
    //           Name = p.Name,
    //           Description = p.Description,
    //           PName = p.PName,
    //           Pk_JobCardID = p.Pk_JobCardID,
    $('#divPaperStock').jtable({
        title: 'WIP Stock',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/DelSchedule/WIPStkList',
            //updateAction: ''
        },
        
        //Pk_StkID=p.Pk_StkID,
        //Stock=p.Stock,
        //Name=p.Name,
        //Description=p.Description,
        //PName=p.BoxPartName,
        //Pk_JobCardID = p.Pk_JobCardID,
        //PartName = p.CatName
        fields: {
            Pk_StkID: {
                title: 'Pk_ID',
                key: true,
            },

            Name: {
                title: 'BoxName',

            },
            PName: {
                title: 'Part Name',
                //key: true,
            },
            Description: {
                title: 'Description',
                //key: true,
            },
         
            Stock: {
                title: 'Stock',
                //key: true,
            },
            Pk_JobCardID: {
                title: 'JobCardID',
                //key: true,
            },

            PartName: {
                title: 'Cat.Name',
                //key: true,
            },


        }
    });

    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();

    $('#divPaperStock').jtable('load', {

        //Pk_ID: SVal

    });
}


function afterOneToManyDialogShow(property) {

    $('#dtDeliveryDate').datepicker({ autoclose: true });



    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpEnquiryBoxSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateDeliverySchedule", "none");

    });
}

function setUpEnquiryBoxSearch() {
    OrdID = document.getElementById('Fk_Order').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderChildDetails'
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
                $("#Quantity").focus();
            });
        },


        fields: {
            BoxID: {
                title: 'BoxID',
                key: true,
                list: true
            },
        
            PartID: {
                title: 'PartID',
                width: '25%',
                list:false
            },
            Pk_Order: {
                title: 'Pk_Order',
                width: '25%',
                list: false
            },
            PName: {
                title: 'PName',
                width: '25%'
            },
            BName: {
                title: 'Box Name',
                width: '25%'
            },
            //EnqQty: {
            //    title: 'Enq.Qty',
            //    width: '25%'

            //},
            OrdQty: {
                title: 'Ord.Qty',
                width: '25%'

            },
            EnqChild: {
                title: 'EnqChild',
                width: '25%',
                list:false

            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Fk_OrderID: $('#Fk_Order').val(),
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Box').wgReferenceField("setData", rows[0].keyValue);      
        document.getElementById('OrdQty').value = rows[0].data["OrdQty"];
        document.getElementById('Fk_PartID').value = rows[0].data["PartID"];
        document.getElementById('txtPart').value = rows[0].data["PName"];
        _comLayer.parameters.clear();
        _comLayer.parameters.add("BoxID", rows[0].data["BoxID"]);
        _comLayer.parameters.add("OrderNo", rows[0].data["Pk_Order"]);
        _comLayer.parameters.add("PartID", rows[0].data["PartID"]);

        var oResult1 = _comLayer.executeSyncAction("/Order/OrderEnqDetails", _comLayer.parameters);
        if (oResult1.TotalRecordCount > 0) {
            document.getElementById('TSchQty').value = oResult1.data[0].TSchQty;
        }
        else { document.getElementById('TSchQty').value = 0; }
        //document.getElementById('TSchQty').value = TSchQty
        document.getElementById('PQty').value = Number($('#OrdQty').val()) - Number($('#TSchQty').val());

        _util.setDivPosition("divCreateDeliverySchedule", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Quantity").focus();
    });



}


function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;
    curEdit = true;
    // Based on State Can Disable the Save Button
    //var status = viewObject.viewModel.data["State"]();

    //if (status == "Completed") {

    //    $(".modelControl").attr('disabled', 'disabled');
    //    $("#cmdAddSchedule").attr('disabled', true);
    //    $("#cmdCreate").attr('disabled', true);
    //}

    $('#dtDeliveredDate').datepicker({ autoclose: true });

    $("#cmdDeliverySchedule").click(function (e) {
        e.preventDefault();
        var OrderNumber = $("#hidPkOrder").val();
        window.open("/DeliverySchedule?Order=" + OrderNumber, "Schedule");
    });

    $('#dtPOrderDate').datepicker({
        autoclose: true
    });
    $('#dtDdate').datepicker({ autoclose: true });
    //$('#cmdOrderSearch').click(function (e) {
    //    e.preventDefault();
    //    setUpOrderSearch(viewObject);
    //    $("#searchDialog").modal("show");
    //});
    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        setUpBoxSearch(viewObject);
        $("#searchDialog").modal("show");
    });
    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        setUpCustomerSearch();
        $("#searchDialog").modal("show");
    });




    //$('#divDeliveryschedule').jtable({
    //    title: 'Box Details',
    //    paging: true,
    //    pageSize: 10,
    //    defaultSorting: 'Name ASC',
    //    actions: {
    //        listAction: '/DelSchedule/Bounce',
    //        updateAction: ''
    //    },
    //    fields: {
    //        slno: {
    //            title: 'slno',
    //            key: true,
    //            list: false
    //        },
    //        DeliveryDate: {
    //            title: 'DDate',
    //            list: true
    //        },
    //        Fk_BoxID: {
    //            title: 'BoxID',
    //            list: true
    //        },

    //        Name: {
    //            title: 'BoxName',
    //        },


    //        OrdQty: {
    //            title: 'OrdQty',
    //            width: '2%'
    //        },

    //        Quantity: {
    //            title: 'Sch.Qty',
    //            width: '25%'

    //        },
    //    }
    //});

   
    //_comLayer.parameters.clear;
    //_comLayer.parameters.add("Pkval", $('#Pk_DeliverySechedule').val());
    //var oResult1 = _comLayer.executeSyncAction("DelSchedule/DelDetails", _comLayer.parameters);
    //if (oResult1.TotalRecordCount>0) {
    //    //afterOneToManyDialogShow = true;
    //    //('#cmdAddSchedule').click();
    //    ////_util.setDivPosition("divSearchMaterial", "block");
    //    ////_util.setDivPosition("divCreateDeliverySchedule", "none");
    //    //configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "DelSchedule", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });

    //}

    bEditContext = true;

    $('#wfTransDisplay').wgWorkFlowHistory({
        wgTag: "DelSchedule",
        viewModel: viewObject.viewModel
    });

    $('#wfButtons').wgWorkFlowButtons({
        wgTag: "DelSchedule",
        viewModel: viewObject.viewModel,
        stateField: "Fk_Status",
        belongsTo: "DelSchedule",
        pkField: "Pk_DeliverySechedule"
    });


}

function beforeOneToManyDataBind(property) {
    if (bEditContext) {
        _util.setDivPosition("divDeliveryStatus", "block");
    }

}

function beforeNewShow(viewObject) {
    $('#fk_Enquiry').wgReferenceField({
        keyProperty: "fk_Enquiry",
        displayProperty: "Description",
        loadPath: "Enquiry/Load",
        viewModel: viewObject.viewModel
    });

}

function afterModelSaveEx(result) {
    alertify.set({
        labels: {
            ok: "Yes",
            cancel: "No"
        },
        buttonReverse: true
    });


    //var bValidation = true;
    //if ($("#Fk_ShippingId").val() < 1) {
    //    $("#Fk_ShippingId").validationEngine('showPrompt', 'Select Shipping Address', 'error', true)
    //    bValidation = false;
    //}
    //return bValidation;
}

function setUpOrderSearch(viewObject) {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    //var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldOrder = "<input type='text' id='txtdlgOrder' placeholder='Order' class='input-large search-query'/>";


    //var txtFieldEnquiryFromDate = "<input type='text' id='txtdlgEnquiryFromDate' placeholder='EnquiryID' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldEnquiryToDate = "<input type='text' id='txtdlgEnquiryToDate' placeholder='EnquiryToDate' class='input-large search-query'/>";


    $('#SearchContainer').jtable({
        title: 'Order List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderChildDetails'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {

           

            Pk_Order: {
                title: 'Order No',
                key: true,
                list:true
            },
            CustPoNo: {
                title: 'Cust_PO'
            },
            OrderDate: {
                title: 'Ord.Date'
            },
            CustomerName: {
                title: 'Cust.Name',
                edit: false
            },
            BName: {
                title: 'B.Name'
            },
            PName: {
                title: 'P.Name'
            },
          
            
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            Fk_OrderID: $('#txtdlgOrder').val(),
            BName: $('#txtdlgBName').val(),

        });

    });
  
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            Fk_OrderID: $('#txtdlgOrder').val(),
            BName: $('#txtdlgBName').val(),
        });
    });


    $('#cmdDone').click(function (e) {
        e.preventDefault();

        //e.preventDefault();
        $('#SearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            Pk_Order: $('#txtdlgOrder').val(),
        });


        var rows = $('#SearchContainer').jtable('selectedRows');
        if (rows.length > 0) {
            //if (curViewModel.data["Fk_Order"] == undefined) {
            //    curViewModel.data["Fk_Order"] = ko.observable(rows[0].keyValue);
            //}
            //else { curViewModel.data["Fk_Order"](rows[0].keyValue) }

            $("#Fk_Order").val(rows[0].keyValue);
            $("#dtPOrderDate").val(rows[0].data.OrderDate);

            $("#Fk_Enquiry").val(rows[0].data.Fk_Enquiry);
  _comLayer.parameters.clear();
        _comLayer.parameters.add("Id", rows[0].keyValue);


        }

        $("#searchDialog").modal("hide");

      
        //$("#PO").focus();


    });

}


function setUpBoxSearch(viewObject) {
    //Enquiry

    // cleanSearchDialog();

    //Adding Search fields
    var txtFieldBoxName = "<input type='text' id='txtdlgBoxName' placeholder='Box Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldPartNo = "<input type='text' id='txtdlgPartNo' placeholder='PartNo' class='input-large search-query'/>";


    $('#searchBoxDialog').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter'
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdBoxDone').click();
            });
        },
        fields: {
            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            PartNo: {
                title: 'PartNo',
                width: '25%'
            },
            Name: {
                title: 'Name',
                width: '25%'
            },
            //Description: {
            //    title: 'Description',
            //    width: '25%'
            //},
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Name: $('#txtdlgBoxName').val(),
            PartNo: $('#txtdlgPartNo').val()

        });

    });
    //$('#txtdlgEnquiryToDate').datepicker({
    //    autoclose: true
    //});
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#searchBoxDialog').jtable('load', {
            Name: $('#txtdlgBoxName').val(),
            PartNo: $('#txtdlgPartNo').val()

        });
    });


    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchBoxDialog').jtable('selectedRows');


        //document.getElementById('Fk_BoxID').value = rows[0].keyValue;
        // document.getElementById('TxtIndent').value = rows[0].data.IndentNo;
        //document.getElementById('TxtMaterial').value = rows[0].data.Fk_Material;
        //document.getElementById('TxtMaterialId').value = rows[0].data.Fk_Material1;

        $('#Fk_BoxID').wgReferenceField("setData", rows[0].data.Pk_BoxID);
        $("#searchBoxDialog").modal("hide");

        //document.getElementById('Quantity').value = rows[0].data.PendQty;
        //document.getElementById('SQuantity').value = rows[0].data.SQuantity;
        //document.getElementById('Unit').value = rows[0].data.UnitName;
        ////document.getElementById('Unit1').value = rows[0].data.UnitName;
        ////document.getElementById('Unit2').value = rows[0].data.UnitName;
        //document.getElementById('TanentName').value = rows[0].data.TanentName;

    });

}



function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    
    viewModel.data["Fk_Order"] = $('#Fk_Order').val();
    viewModel.data["Fk_Enquiry"] = $('#Fk_Enquiry').val();
    //viewModel.data["DeliveryDate"] = $('#dtDeliveredDate').val();

    //viewModel.data["Fk_EnquiryChild"] = $('#Fk_EnquiryChild').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
 
    viewModel1.data["Fk_Order"] = $('#Fk_Order').val();
    viewModel1.data["Fk_Enquiry"] = $('#Fk_Enquiry').val();
}

function ReferenceFieldNotInitilized(viewModel) {


    $('#txtfk_Customer').wgReferenceField({
        keyProperty: "fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });


    $('#txtFk_Box').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#txtfk_Customer').wgReferenceField("setData", viewModel.data["fk_Customer"]);
        $('#txtfk_Product').wgReferenceField("setData", viewModel.data["fk_Product"]);
        $('#txtFk_Box').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#SearchDialog').empty();
    $("#txtdlgCustomerName").val("");
    $("#txtdlgCommunicationType").val("");
    $("#txtdlgEnquiryFromDate").val("");
    $("#txtdlgEnquiryToDate").val("");

}


function orderDateValidate(field, rules, i, options) {
    if (bFromSave) {
        var isDateValid = _util.isDate(field.val());

        if (isDateValid.valid == false) {

            return "* " + isDateValid.Message;
        }

        if (!_util.dateGraterThanToday(field.val())) {
            return "* Order date cannot be greater than today";
        }
    }
    bFromSave = false;
}

function deliveryDateValidate(field, rules, i, options) {

    var isDateValid = _util.isDate(field.val());

    if (isDateValid.valid == false) {
        return "* " + isDateValid.Message;
    }

    if (field.val() == undefined || $('#dtDeliveredDate').val() == undefined) {
        return "* Please select the order date before adding the delivery schedule";
    }

    var orderDate = $('#dtDeliveryDate').val();


    if (_util.dateGraterThan(field.val(), orderDate)) {
        return "* Delivery date cannot be earlier than order date";
    }

}
function OrderdeliveryDateValidate(field, rules, i, options) {

    //var isDateValid = _util.isDate(field.val());

    //if (isDateValid.valid == false) {
    //    return "* " + isDateValid.Message;
    //}

    //if (field.val() == undefined || $('#dtPOrderDate').val() == undefined) {
    //    return "* Please select the order date before adding the delivery schedule";
    //}dtDdate

    var orderDate = $('#dtPOrderDate').val();


    if (_util.dateGraterThan(field.val(), orderDate)) {
        //return "* Delivery date cannot be earlier than Order Date";
    }

}
function showOnlyOpen() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#open').val()
    });
}

function showOnlyInProgress() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#InProgress').val()
    });
}

function showOnlyCompleted() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Completed').val()
    });
}


function showOnlyCancelled() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#Cancelled').val()
    });
}

function showOnlyClosed() {
    $('#MainSearchContainer').jtable('load', {
        OnlyPending: $('#closed').val()
    });
}

function showAll() {
    $('#MainSearchContainer').jtable('load', {
        CustomerName: $('#CustomerName').val()
    });
}

function validatebuttonActionHook() {
    _comLayer.parameters.clear();
    _comLayer.parameters.add("Id", $("#Pk_Order").val());

    var oResult1 = _comLayer.executeSyncAction("/Order/Load", _comLayer.parameters);
    if (oResult1.data.deliverySchedule.length == 0)
        return true;
    else
        return false;
}