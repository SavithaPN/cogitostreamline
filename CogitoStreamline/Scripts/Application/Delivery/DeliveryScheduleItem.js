﻿
function DeliveryScheduleItem() {
    this.data = new Object();
    this.data["Pk_DeliverySechedule"] = "";
    this.data["DeliveryDate"] = "";
    this.data["Quantity"] = "";
    this.data["Comments"] = "";
    this.data["DeliveryCompleted"] = null;
    this.data["DeliveredDate"] = "";
    this.data["Fk_EnquiryChild"] = "";
    this.data["OrdQty"] = "";
    this.data["txtPart"] = "";
    this.data["Fk_PartID"] = "";

 
    this.updateValues = function () {
        this.data["Fk_Box"] = $('#Fk_Box').val();
        this.data["Fk_EnquiryChild"] = $('#Fk_EnquiryChild').val();
        this.data["Quantity"] = $('#Quantity').val();
        this.data["Comments"] = $('#Comments').val();
        this.data["DeliveryCompleted"] = $('#chkDeliveryCompleted').is(":checked");
        this.data["DeliveredDate"] = $('#dtDeliveredDate').val();
        this.data["Name"] = $('#txtFk_Box').val();
        this.data["OrdQty"] = $('#OrdQty').val();
        this.data["txtPart"] = $('#txtPart').val();
        this.data["Fk_PartID"] = $('#Fk_PartID').val();
    };

    this.load = function (Pk_DeliverySchedule, DeliveryDate, Quantity, DeliveryCompleted, DeliveredDate, Fk_BoxID, Name, OrdQty, txtPart, Fk_PartID) {
        this.data["Pk_DeliverySchedule"] = Pk_DeliverySchedule;
      

        var dArr = DeliveryDate.split("-");  // ex input "2010-01-18"
        if (dArr.length > 1) {
            this.data["DeliveryDate"] = dArr[0] + "/" + dArr[1] + "/" + dArr[2];
        }
        else
            if (dArr.length == 1) {
                this.data["DeliveryDate"] = dArr[0];
            }
        this.data["Quantity"] = Quantity;     
        this.data["DeliveryCompleted"] = DeliveryCompleted;
        this.data["DeliveredDate"] = DeliveredDate;
        this.data["Fk_BoxID"] = Fk_BoxID;
        this.data["Name"] = Name;
        this.data["OrdQty"] = OrdQty;
        this.data["txtPart"] = txtPart;
        this.data["Fk_PartID"] = Fk_PartID;
        //this.data["Fk_EnquiryChild"] = Fk_EnquiryChild;
    




    };
    
}



