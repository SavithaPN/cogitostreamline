﻿

var curViewModel = null;
var curEdit = false;
var VendorID;
var gtot;
var EDVal;
var IndentNo;
var QCValue;
var ssum = 0;
var rowcount = 0;
var tempamt = 0;
var GSM = 0;
var BF = 0;
var Dec = 0;
var checkedVal;
var CustCode = 0;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Inward List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InwdBox/MatInwdListByFiter',
            //deleteAction: '',
            //updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_Inwd: {
                title: 'Inward No',
                key: true,
                list: true,
                width: '4%'
            },
       
            Inward_Date: {
                title: 'Inwd.Date',
                width: '4%'
            },
            PONo: {
                title: 'PONo',
                
                list: false,
                width: '4%'
            },
            
            VendorName: {
                title: 'Vendor',
                width: '10%'
            },
           
            Pur_InvDate: {
                title: 'Pur_InvDate',
                list: true,
                width: '4%'
            },
            Pur_InvNo: {
                title: 'Pur_InvNo',
                list: true,
                width: '4%'
            },
            //Print: {
            //    title: 'Print',
            //    width: '2%',
            //    //display: function (data) {
            //    //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
            //    display: function (row) {
            //        var button = $("<i class='icon-printer'></i>");
            //        $(button).click(function () {

            //            Pk_Inw = row.record.Pk_Inward;
            //            _comLayer.parameters.add("Pk_Inw", Pk_Inw);
            //            _comLayer.executeSyncAction("InwdBox/InwdRep", _comLayer.parameters);
            //            var strval = "ConvertPDF/MInward" + Pk_Inw + ".pdf"
            //            ////////////////////////////


            //            var xhr = new XMLHttpRequest();
            //            var urlToFile = "ConvertPDF/MInward" + Pk_Inw + ".pdf"
            //            xhr.open('HEAD', urlToFile, false);
            //            xhr.send();

            //            if (xhr.status == "404") {
            //                alert('Data Not Available , File does not Exist');
            //                return false;


            //            } else {
            //                window.open(strval, '_blank ', 'width=700,height=250');
            //                return true;
            //            }


            //            /////////////////////////
                        

            //        });
            //        return button;
            //    }
            //},

            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/InwdBox/InwardGetRec?Pk_Inward=' + data.record.Pk_Inward

                                        },
                                        fields: {
                                           
                                     
                                            PkMaterial: {
                                                title: 'MaterialID',
                                                list: true
                                            },
                                            InwardDet: {
                                                title: 'InwardDet',
                                                list:false
                                            },
                                            PkInwd: {
                                                title: 'PkInwd',
                                                list: false
                                            },
                                            InwDate: {
                                                title: 'InwDate',
                                                list: false
                                            },
                                            Name: {
                                                title: 'MaterialName'
                                            },
                                            Color: {
                                                title: 'Shade'
                                            },
                                            Mill: {
                                                title: 'Mill'

                                            },
                                            Quantity: {
                                                title: 'Qty',
                                                key: true,
                                            },
                                            ReelNo: {
                                                title: 'ReelNo',
                                                key: true,
                                            },
                                            Indent: {
                                                title: 'Indent ID',
                                                key: true,
                                            },
                                            PONo: {
                                                title: 'PONo',
                                                key: true,
                                            },
                                            //QC: {
                                            //    title: 'QC ID',
                                            //    key: true,
                                            //},

                                            /////////////////////////////
                                            //Distribution: {
                                            //    title: 'Distribution',
                                            //    width: '2%',
                                            //    //display: function (data) {
                                            //    //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                                            //    display: function (row) {
                                            //        //var button = $("<i class='icon-printer'></i>");
                                            //        var button = $("<button class='bnt'>Distribution</button>");
                                            //        $(button).click(function () {

                                            //            PkMaterial = row.record.PkMaterial;
                                            //            PkInwd = row.record.PkInwd;
                                            //            //PName = row.record.Name;
                                            //            //document.getElementById('pk').value = PkEnqChildId;
                                            //            //_comLayer.parameters.add("PkMaterial", PkMaterial);
                                            //            //comLayer.parameters.add("PName", PName);
                                            //            //$('#cmdNew').click();
                                            //            //var oResult1 = _comLayer.executeSyncAction("Enquiry/CheckEstimate", _comLayer.parameters);


                                            //            //if (oResult1.data.Estimated) {

                                            //            //    alert('Already Estimated!!');
                                            //            //    BoxId = row.record.Fk_BoxID;
                                            //            //    window.open("/Estimation");
                                            //            //}
                                            //            //else {
                                            //            //    BoxId = row.record.Fk_BoxID;

                                            //            //    // window.open("/EnqEstimate?Box=" + BoxId + ", EnqChild=" + PkEnqChildId + ", BT=" + BType);
                                            //            window.open("/PaperDistribution?Material=" + PkMaterial + ", Inwd=" + PkInwd);
                                            //            //}

                                            //        });
                                            //        return button;
                                            //    }

                                            //}

                                            //////////////////////////////////
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },


            //////////////////////////////////////////////////



        }
    });

      

    //Re-load records when Customer click 'load records' button.
    $('#TxtFromDate').datepicker({ autoclose: true });
        $('#TxtToDate').datepicker({ autoclose: true });

        $('#dtPInwardDate').datepicker({
            autoclose: true
        });

        //Re-load records when Order click 'load records' button.
        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_Inward: $('#InwardNumber').val(),
                Vendor: $('#VendorName').val(),
                Material: $('#Material').val(),
                FromDate: $('#TxtFromDate').val(),
                ToDate: $('#TxtToDate').val(),
            });

        });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
    
 


    $('#dtPInwardDate').datepicker({ autoclose: true });


    //$('#Pk_PONo').keypress(function (e) {
    //    if (e.keyCode == 13) {
    //        e.preventDefault();

    //        $('#MainSearchContainer').jtable('load', {
    //            Pk_PONo: $('#txtPkPONo').val()
    //        });
    //    }
    //});

    //$('#dtDate').change(function (e) {
    //    e.preventDefault();
    //    $('#MainSearchContainer').jtable('load', {
    //        PODate: $('#txtPODate').val()
    //    });
    //});
        $("#searchDialog").width(900);
    $("#searchDialog").css("top", "10px");
    $("#searchDialog").css("left", "400px");

    //setUpQualitySearch();
    setUpIssueMaterialSearch();

}


function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtPInwardDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    curViewModel = viewObject.viewModel;

    $('#dtPInwardDate').datepicker({ autoclose: true });
    $('#dtPur_InvDate').datepicker({ autoclose: true });
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InwdBox/Bounce',
            deleteAction: '',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Box Name'

            },
            //Color: {
            //    title: 'Shade'

            //},
            //SName: {
            //    title: 'Mill',
            //    width: '1%'
            //},
            //    RollNo: {
            //        title: 'ReelNo',
            //        width: '1%'
            //    },
            Quantity: {
                title: 'Quantity',
                width: '1%'

            },

          
            
        //    QCNo: {
        //        title: 'QCNo',
        //width: '1%'
        //    },

        //    Fk_Mill: {
        //        title: 'Fk_Mill',
        //        width: '1%',
        //        list:false
        //    }
       }
    });

  
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "InwdBox", "_AddMaterial", function () { return new IssuedMaterial() });


    $('#cmdVendorMSearch').click(function (e) {
        e.preventDefault();
        setUpVendorSearch(viewObject);
        $("#searchDialog").modal("show");
        $("#searchDialog").width(600);
        $("#searchDialog").height(500);

    });


    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        setUpCustomerSearch(viewObject);
        $("#searchBoxDialog").modal("show");
        $("#searchBoxDialog").width(700);
        $("#searchBoxDialog").height(700);
    });
}
function setUpVendorSearch() {
    //Enquiry

    // cleanSearchDialog();

    $("#srchssHeader").text("Vendor Search");

    //Adding Search fields
    var txtFieldVendorName = "<input type='text' id='txtdlgVendorName' placeholder='Vendor Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgSearchFields").append(txtFieldVendorName);


    $('#searchDialog').jtable({
        title: 'Vendor List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Vendor/VendorListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },


        fields: {
            Pk_Vendor: {
                title: 'Vendor ID',
                key: true,
                width: '2%'
            },
            VendorName: {
                title: 'Vendor Name',
                edit: false,
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#searchDialog').jtable('load', {
            VendorName: $('#txtdlgVendorName').val()
        });
    });

    $('#cmdDone').click(function (e) {
        e.preventDefault();
        var rows = $('#searchDialog').jtable('selectedRows');
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].keyValue);
        $('#Fk_Vendor').wgReferenceField("setData", rows[0].data.Fk_Vendor);
        VendorID = rows[0].keyValue;
        $("#searchDialog").modal("hide");
    });

}
function setUpCustomerSearch() {
    //Enquiry

    cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='text' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#MainSearhContainer2').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdBoxDone').click();
            });
        },

        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            CustomerAddress: {
                title: 'Address',
                width: '35%',
            },
            City: {
                title: 'City'
            },
            CustomerContact: {
                title: 'Office Contact'
            },
            Email: {
                title: 'Email'
            }
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgBoxName').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdBoxSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer2').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgBoxName').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdBoxDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MainSearhContainer2').jtable('selectedRows');
        $('#txtfk_Customer').wgReferenceField("setData", rows[0].keyValue);
        CustCode = rows[0].keyValue;
        //document.getElementById('CustID').value = CustCode;
        //alert(CustCode);
        $("#searchBoxDialog").modal("hide");
        //$("#ReferenceEnquiry").focus();
    });

}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    
    

    viewModel.data["Inward_Date"] = $('#dtPInwardDate').val();
    viewModel.data["Pur_InvDate"] = $('#dtPur_InvDate').val();
    viewModel.data["Fk_Vendor"] = VendorID;
    viewModel.data["Fk_Customer"] = CustCode;
    viewModel.data["PODate"] = $('#dtDate').val();
    //viewModel.data["Fk_QC"] = $('#txtQualityCheck').val();
    viewModel.data["PONo"] = $('#TxtPO').val();
    //viewModel.data["Fk_Indent"] = $('#Indent').val();
    //viewModel.data["Fk_Mill"] = $('#Fk_Mill').val();
    ////viewModel.data["NETVALUE"] = $('#NETVALUE').val();
    //viewModel.data["GrandTotal"] = $('#GrandTotal').val();
    //viewModel.data["ED"] = $('#ED').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Inward_Date"] = $('#dtPInwardDate').val();
    viewModel1.data["Pur_InvDate"] = $('#dtPur_InvDate').val();
    viewModel1.data["PODate"] = $('#dtDate').val();
    //viewModel1.data["Fk_QC"] = $('#txtQualityCheck').val();
    //viewModel1.data["Fk_Mill"] = $('#Fk_Mill').val();
    viewModel1.data["PONo"] = $('#TxtPO').val();
    viewModel1.data["Fk_Customer"] = CustCode;
    //QCValue = $('#TxtPO').val();
    //viewModel1.data["Fk_Indent"] = $('#Indent').val();
    //viewModel.data["Fk_Indent"] = IndentNo;
}
function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#dtDate').datepicker({ autoclose: true });
    curEdit = true;
    $('#tlbMaterials').jtable({
        title: 'Item List',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/InwdBox/Bounce',
            //deleteAction: '',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Id',
                key: false,
                list: false
            },
            Name: {
                title: 'Material Name',
                width: '5%'
            },

            Color: {
                title: 'Shade',
                width: '5%'
            },
            Mill: {
                title: 'Mill',
                width: '5%'
            },
            RollNo: {
                title: 'ReelNo',
                width: '1%'
            },
            Quantity: {
                title: 'Quantity',
                width: '1%'
            },

            //Price: {
            //    title: 'Rate',
            //    width: '1%'
            //}
        }
    });

    if ($.trim($('#TaxType').val()) == "VAT") {
        $("#VAT").prop("checked", true);
    }
    else if ($.trim($('#TaxType').val()) == "CST") {
        $("#CST").prop("checked", true);


    }

    var oSCuts = viewObject.viewModel.data.MaterialData();
    viewObject.viewModel.data["Materials"] = ko.observableArray();
    var i = 0;
    ssum = 0;
    while (oSCuts[i]) {
        var oCut = new IssuedMaterial();

        oCut.load(oSCuts[i].Pk_InwardDet, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].Quantity,  oSCuts[i].AccQty, oSCuts[i].RollNo, oSCuts[i].Fk_Mill, oSCuts[i].Color, oSCuts[i].Mill);
        viewObject.viewModel.data["Materials"].push(oCut);

        i++;
    }

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "Materials", "InwdBox", "_AddMaterial", function () { return new IssuedMaterial() });

}


function setUpPOSearch() {
    //Branch

    //cleanSearchDialog();
    var rowCount = $('#QualitySearhContainer tr').length;
    if (rowCount > 0) {
        $('#QualitySearhContainer').jtable('destroy');

    }

    $("#srchHeader").text("PO Search");

    //Adding Search fields
    var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='PONo' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";

    var selValue = document.getElementById('Selected').value;

   // alert(selValue);
    if (selValue != "Others")
        $('#QualitySearhContainer').jtable({
            title: 'PO List',
            paging: true,
            pageSize: 4,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/PurchaseOrder/PurchaseOrderList'
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdQCheckDone').click();
                });
            },

            fields: {
                Pk_PONo: {
                    title: 'PO Number',
                    key: true,
                    list:false
                },
                PkDisp: {
                    title: 'PO Number',
                },
                PODate: {
                    title: 'PO Date'
                },
                Name: {
                    title: 'Mat.Name'
                },
                Fk_Vendor: {
                    title: 'Vendor Name',
                    width:'25%'
                },
                Fk_Indent: {
                    title: 'Indent No.'
                },
                Fk_Id: {
                    title: 'Cert.ID'
                },
                Pk_PODet: {
                    title: 'Pk_PODet',
                    list:false
                }
            }
        });

    else
    {
        $('#QualitySearhContainer').jtable({
            title: 'PO List',
            paging: true,
            pageSize: 4,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/PurchaseOrder/PurchaseOrderOthersList'
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    $('#cmdQCheckDone').click();
                });
            },

            fields: {
                Pk_PONo: {
                    title: 'PO Number',
                    key: true,
                    list:false
                },
                PkDisp: {
                    title: 'PO Number',                 

                },

                PODate: {
                    title: 'PO Date'
                },
                Name: {
                    title: 'Mat.Name',
                    width:'5%',
                },
                Fk_Vendor: {
                    title: 'Vendor Name',
                    width: '5%',
                },
                Fk_Indent: {
                    title: 'Indent No.'
                },
                Desc: {
                    title: 'Desc',
                    width: '5%',
                },
                CatName: {
                    title: 'Cat.'
                },
                UnitName: {
                    title: 'UnitName'
                },
                Pk_PODet: {
                    title: 'Pk_PODet.',
                    list: false
                },
            }
        });
    }

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#QualitySearhContainer').jtable('load', {
            PkDisp: $('#txtdlgInvoice').val(),
            MaterialName: $('#txtOthers').val(),

        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdQCheckSearch').click(function (e) {
        e.preventDefault();

        $('#QualitySearhContainer').jtable('load', {
            PkDisp: $('#txtdlgInvoice').val(),
            MaterialName: $('#txtOthers').val(),
            FromIndentDate: $('#txtdlgFromDate').val(),
            ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdQCheckDone').click(function (e) {
        e.preventDefault();
        var rows = $('#QualitySearhContainer').jtable('selectedRows');
        $("#searchQualityDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        document.getElementById('TxtPO').value = rows[0].keyValue;
        IndentNo = rows[0].data.Fk_Indent;
        document.getElementById('Indent').value = IndentNo;
        //$('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}

function setUpConsumablesSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchHeader").text("Search Consumables");

    //Adding Search fields
    //var txtFieldIndentNo = "<input type='text' id='txtdlgIndent' placeholder='PONo' class='input-large search-query'/>&nbsp;&nbsp;";
    ////var txtFieldVendor = "<input type='text' id='txtdlgVendor' placeholder='Vendor' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldFromDate = "<input type='text' id='txtdlgFromDate' placeholder='FromDate' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldToDate = "<input type='text' id='txtdlgToDate' placeholder='ToDate' class='input-large search-query'/>&nbsp;&nbsp;";

    var selValue = document.getElementById('Selected').value;

    alert(selValue);
    $('#QualitySearhContainer').jtable({
        title: 'PO List',
        paging: true,
        pageSize: 4,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialCategory/MaterialIndentSearch?MaterialCategory=' + "Others"
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdQCheckDone').click();
            });
        },

        fields: {
            Pk_PONo: {
                title: 'PO Number',
                key: true
            },

            PODate: {
                title: 'PO Date'
            },
            Name: {
                title: 'Mat.Name'
            },
            Fk_Vendor: {
                title: 'Vendor Name'
            },
            Fk_Indent: {
                title: 'Indent No.'
            },
            //Fk_Id: {
            //    title: 'Cert.ID'
            //},
            //StateName: {
            //    title: 'Status'
            //}
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#QualitySearhContainer').jtable('load', {
            //Pk_PONo: $('#txtdlgInvoice').val()
            MaterialCategory: $('#Selected').val(),
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdQCheckSearch').click(function (e) {
        e.preventDefault();

        $('#QualitySearhContainer').jtable('load', {
            MaterialName: $('#txtOthers').val(),
            MaterialCategory: $('#Selected').val(),
            //Vendor: $('#txtdlgVendor').val(),
            //FromIndentDate: $('#txtdlgFromDate').val(),
            //ToIndentDate: $('#txtdlgToDate').val()

        });
    });

    $('#cmdQCheckDone').click(function (e) {
        e.preventDefault();
        var rows = $('#QualitySearhContainer').jtable('selectedRows');
        $("#searchQualityDialog").modal("hide");
        //$('#txtFk_IndentNumber') = document.getElementById(rows[0].keyValue
        //document.getElementById('TxtPO').value = rows[0].keyValue;
        //IndentNo = rows[0].data.Fk_Indent;
        //document.getElementById('Indent').value = IndentNo;
        //$('#txtFk_Vendor').wgReferenceField("setData", rows[0].data.VendorID);

    });

}


function setUpIssueMaterialSearch() {
    var rowCount = $('#MaterialSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MaterialSearchContainer').jtable('destroy');

    }

    //if (checkedVal == "Others")
    //    gMaterials = "Others";
    //else {
    //    gMaterials = "Paper";
    //}

    PONo = document.getElementById('TxtPO').value;
    //SaveSearch();
    if (GSM > 0 & BF > 0 & Dec > 0) {
        document.getElementById('txtMaterial').value = GSM;
        document.getElementById('txtBF').value = BF;
        document.getElementById('txtDec').value = Dec;
    }

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/PurchaseOrder/POGetRec?Pk_PONo=' + PONo,
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                
                $('#cmdMaterialDone').click();
            });
        },

       

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },

            MaterialName: {
                title: 'Mat.Name'
            },
            //PONo: {
            //    title: 'PoNo'
            //},
            Color: {
                title: 'Shade'
            },
            Mill: {
                title: 'MillName',
                list: true
            },
            UnitName: {
                title: 'UnitName',
                list: true
            },
            CatName: {
                title: 'Cat..',
                list: true
            },
            //Quantity: {
            //    title: "Quantity"
            //},
            Pk_Mill: {
                title: "Fk_Mill",
                list: false

            },
            Pk_PoDet: {
                title: "Pk_PoDet",
                list: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Dec: $('#txtDec').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            GSM: $('#txtMaterial').val(),
            BF: $('#txtBF').val(),
            Dec: $('#txtDec').val(),
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        var $selectedRows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('SName').value = rows[0].data["Mill"];
        document.getElementById('Fk_Mill').value = rows[0].data["Pk_Mill"];
        //document.getElementById('RollNo').value = rows[0].data["ReelNo"];
        document.getElementById('Shade').value = rows[0].data["Color"];
        //document.getElementById('Mill').value = rows[0].data["Mill"];
        //var KVal = rows[0].data["Pk_Material"];
     //   $('#MaterialSearchContainer').jtable('deleteRows',$selectedRows);
        //$('#MaterialSearchContainer').jtable('deleteRecord', { key: KVal, clientOnly: true });

SaveSearch();
        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        
        $("#Quantity").focus();
    });



}




function afterOneToManyDialogShow(property) {
    
    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpEnquiryBoxSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");

    });
}
function setUpEnquiryBoxSearch() {
    //alert(CustCode);
    $('#MaterialSearchContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 7,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
             
            listAction: '/BoxMaster/BoxPartDetails?CustIDVal=' + CustCode
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
                $("#Quantity").focus();
            });
        },


        fields: {
            Pk_BoxID: {
                title: 'Box Id',
                key: true,
                list: true
            },
            Name: {
                title: 'Box Name',
                width: '25%'
            },
            //PName=p.PName,
            //PartQty=p.PartQty,
            PName: {
                title: 'Part Name',
                width: '25%'
            },
            Pk_PartID: {
                title: 'PartID',
                list: false
            },
            Quantity: {
                title: 'PartQty',
                width: '25%'
            },

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            BoxName: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
            CustIDVal: CustCode

        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            //CustName: $('#txtfk_Customer').val(),
            BoxName: $('#txtMaterial').val(),
            //PartName: $('#txtPart').val(),
            CustIDVal: CustCode
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //document.getElementById('Fk_EnquiryChild').value = rows[0].data["Pk_EnquiryChild"];

        //var a = rows[0].data["Quantity"]
        //var b = rows[0].data["PartQty"]
        //if (b == undefined) {
        //    var b = 1;
        //}
        //else {
        //    var b = rows[0].data["PartQty"]
        //}

        //document.getElementById('EnqQty').value = Number(a) * Number(b);
        //document.getElementById('Fk_PartID').value = rows[0].data["Pk_PartID"];
        //document.getElementById('txtPart').value = rows[0].data["PName"];
        //document.getElementById('OrdQty').value = 1;
        //_comLayer.parameters.clear();
        //_comLayer.parameters.add("Fk_EnquiryChild", rows[0].data["Pk_EnquiryChild"]);


        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        $("#Quantity").focus();
    });





    //}
}

function SaveSearch()
{
    GSM = document.getElementById('txtMaterial').value;
    BF = document.getElementById('txtBF').value;
    Dec = document.getElementById('txtDec').value;
}
function ReferenceFieldNotInitilized(viewModel) {

    $('#txtfk_Customer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_BoxID",
        displayProperty: "Name",
        loadPath: "BoxMaster/Load",
        viewModel: viewModel
    });

    $('#Fk_Vendor').wgReferenceField({
        keyProperty: "Fk_Vendor",
        displayProperty: "VendorName",
        loadPath: "Vendor/Load",
        viewModel: viewModel
    });
    if (viewModel.data != null) {
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_BoxID"]);
        $('#Fk_Vendor').wgReferenceField("setData", viewModel.data["Fk_Vendor"]);
        $('#txtfk_Customer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();

    $('#dlgSearchQualityFields').empty();

}

function checkDuplicate() {

    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["Materials"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["Materials"]()[i].data.Fk_Material && objContext.data.RollNo == curViewModel.data["Materials"]()[i].data.RollNo) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}

function checkValue() {
    if (objContextEdit == false) {

        if (Number($('#Quantity').val()) > Number($('#AccQuantity').val())) {
            return "* " + "Inward Quantity Exceeded Quality Checked Qty. Quality Checked Quantity:" + $('#AccQuantity').val();
        }
        else {
           
        }
    } else if (objContextEdit == true) {
        if (Number($('#Quantity').val()) > Number($('#AccQuantity').val())) {
            return "* " + "Inward Quantity Exceeded Quality Checked Qty. Quality Checked Quantity:" + $('#AccQuantity').val();
        }
        else {
        }
    }
}

//function showOnlyPaper() {
//    setUpIssueMaterialSearch();

//}



function showOnlyBoard() {


    var rowCount = $('#MaterialSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MaterialSearchContainer').jtable('destroy');

    }


    document.getElementById('SType').value = "Board";


    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 10,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MaterialCategory/MaterialSearchListByFiter',
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //$(this).css('background', 'red');
                $('#cmdMaterialDone').click();
            });
        },
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true,
                width: '2%'
            },
            Pk_PaperStock: {
                title: 'PKStock',
                key: false,
                width: '2%',
                list: false
            },
            RollNo: {
                title: 'Reel No',
                edit: false,
                width: '2%'
            },
            Name: {
                title: 'Material Name',
                edit: false
            },
            Color: {
                title: 'Shade',
                edit: false,
                width: '2%'
            },
            Quantity: {
                title: 'Ex.Stk.Qty',
                edit: false,
                width: '2%'
            },

            Category: {
                title: 'Cat',
                edit: false,
                list: false
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            //Name: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
            MaterialCategory: $('#SType').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();

        if (document.getElementById('SType').value == '')
        { alert("Select any one of the Material Type and Press Search"); }
        else
        {
            $('#MaterialSearchContainer').jtable('load', {
                //Name: $('#txtMaterial').val(),
                MaterialCategory: $('#SType').val()

            });
        }
    });

 
    $('#cmdMaterialDone').click(function (e) {

        valCat=document.getElementById('SType').value;
        if (valCat == "Board") {
            e.preventDefault();
            var rows = $('#MaterialSearchContainer').jtable('selectedRows');
            //var rctr = $('#MaterialSearchContainer').jtable('selectedRows').TotalRecordCount;
            if (rows.length > 0) {
                $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
                document.getElementById('txtMaterial').value = rows[0].data.Pk_Material;
                if (rows[0].data["Category"] != "Paper") {
                    document.getElementById('RollNo').value = "";
                }
                else {
                    document.getElementById('RollNo').value = rows[0].data["RollNo"];
                }
                //document.getElementById('Quantity').value = rows[0].data["Quantity"];
                //document.getElementById('Pk_PaperStock').value = rows[0].data["Pk_PaperStock"];
                //document.getElementById('Pk_Material').value = rows[0].data["Pk_Material"];
                //document.getElementById('Color').value = rows[0].data["Color"];
            }
            _util.setDivPosition("divCreateMaterial", "block");
            _util.setDivPosition("divSearchMaterial", "none");
            $("#Quantity").focus();
        }
    });


   
}

function showOnlyPaper() {
     
        document.getElementById("Others").checked = false;
        document.getElementById("Paper").checked = true;
        document.getElementById("Selected").value = "Paper";
        setUpPOSearch();

    }
    function showOthers() {
       
        document.getElementById("Others").checked = true;
        document.getElementById("Paper").checked = false;
        document.getElementById("Selected").value = "Others";
        // checkedVal = "Others";

        setUpPOSearch();
    }

