﻿var curViewModel = null;
var curEdit = false;

var Pk_QuotationVal = 0;


function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Quotation List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Quote/QuotationListByFiter',
            updateAction: '',
            //createAction: ''
        },
        fields: {

            Pk_Quotation: {
                title: 'Quotation Id',
                key: true,
                list: true
            },
            QuotationDate: {
                title: 'Date',
                width: '25%'
            },
            Customer: {
                title: 'Customer',
                width: '25%'
            },
           
            Pk_Enquiry: {
                title: 'Enq.No.',
                width: '25%'
            },
            Print: {
                title: 'Print',
                width: '2%',
                display: function (row) {    
                
                    var button = $("<button class='bnt'>Print</button>");
                    $(button).click(function () {

                        Pk_QuotationVal = row.record.Pk_Quotation;
                        _comLayer.parameters.add("Pk_Quotation", Pk_QuotationVal);

                        _comLayer.executeSyncAction("/Quote/QuoteRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Quote-" + " " + Pk_QuotationVal + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////


                         /////////////////////////

                    

                    });
                    return button;
                }
            },
            PrintA: {
                title: 'Sp.Format',
                width: '2%',
                display: function (row) {

                    var button = $("<button class='bnt'>Sp.Format</button>");
                    $(button).click(function () {

                        Pk_QuotationVal = row.record.Pk_Quotation;
                        _comLayer.parameters.add("Pk_Quotation", Pk_QuotationVal);

                        _comLayer.executeSyncAction("/Quote/QuoteRepSF", _comLayer.parameters);
                        var strval = "ConvertPDF/SFQuote-" + " " + Pk_QuotationVal + ".pdf"
                        window.open(strval, '_blank ', 'width=700,height=250');
                        ////////////////////////////


                        /////////////////////////



                    });
                    return button;
                }
            },
        }
    });
    $('#TxtFromDate').datepicker({ autoclose: true });
    $('#TxtToDate').datepicker({ autoclose: true });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Customer: $('#txtSearchID').val(),
            FromDate: $('#TxtFromDate').val(),
            ToDate: $('#TxtToDate').val()
        });
    });
    $('#LoadRecordsButton').click();
    

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
}
//function cmdNew_Click() {
//    e.preventDefault();
//    _page.showView('New');

//}

function afterNewShow(viewObject) {
    curViewModel = viewObject.viewModel;
    var dNow = new Date();
    document.getElementById('QuotationDate').value = ((dNow.getDate() < 10) ? ("0" + dNow.getDate()) : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? ("0" + (dNow.getMonth() + 1)) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();

    $('#QuotationDate').datepicker({ autoclose: true });
   // $('#dtDdate').datepicker({ autoclose: true });
    //curEdit = true;
    //var st = _util.getParameterByName('Estimate');

    //document.getElementById('Fk_BoxEstimation').value = st;



    $('#cmdEnquirySearch').click(function (e) {
        e.preventDefault();
        setUpEnquirySearch(viewObject);
        $("#searchDialog").modal("show");
    });


    $('#divDeliveryschedule').jtable({
        title: 'Estimation Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Quote/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
         
            Fk_EstimationID: {
                title: 'Est.ID',
                list: true

            },
            BoxID: {
                title: 'BoxID',
                list: true,
                list:false
            },
            BName: {
                title: 'BoxName',
                list: true
            },
            Quantity: {
                title: 'Qty',
                list: true
            },

            CostPerBox: {
                title: 'Rate/Box',
            },
            Dimension: {
                title: 'Dimension',
            },
            BSVal: {
                title: 'BS',
            },
            //EnqQty: {
            //    title: 'Enq.Qty'

            //},
            //Amount: {
            //    title: 'Amount',
            //    width: '2%'
            //}
        }
    });

    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "Quote", "_AddEstimation", function () { return new DeliveryScheduleItem() });

}

function afterOneToManyDialogShow(property) {

    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpEstimationSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateDeliverySchedule", "none");

    });
}

function setUpEstimationSearch() {
    EnqID = document.getElementById('Fk_Enquiry').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Estimation List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Estimation/EstList?Fk_Enquiry=' + EnqID
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
                //$("#OrdQty").focus();
            });
        },


        fields: {
            Pk_BoxEstimation: {
                title: 'Estimate Id',
                key: true,
                width: '4%'
            },
            EstDATE: {
                title: 'Est. Date',
                width: '4%'
            },
            //Enquiry: {
            //    title: 'Enq.No.',
            //    width: '4%'
            //},
            BoxID: {
                title: 'Box ID',
                list: false,
            },
            BoxName: {
                title: 'Box Name',
                key: true
            },
            Dimension: {
                title: 'Dimension',
                key: true
            },
            CostPerBox: {
                title: 'Cost/Box',
                key: true
            },
            Quantity: {
                title: 'Enq.Qty',
                width: '25%',
                list:false
            },
            BSVal: {
                title: 'BS',
                width: '25%',
                list: true
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtMaterial').val(),
            //Pk_MaterialIssueID: $('#Fk_Vendor').val()
        });
    });

    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');
        $('#txtEstimationID').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('BoxID').value = rows[0].data["BoxID"];
        document.getElementById('BName').value = rows[0].data["BoxName"];        
        document.getElementById('Quantity').value = rows[0].data["Quantity"];
        document.getElementById('Dimension').value = rows[0].data["Dimension"];
        document.getElementById('BSValue').value = rows[0].data["BSVal"];
        document.getElementById('Price').value = Math.round(rows[0].data["CostPerBox"]);
        _comLayer.parameters.clear();
       // _comLayer.parameters.add("Fk_EnquiryChild", rows[0].data["Pk_EnquiryChild"]);


        _util.setDivPosition("divCreateDeliverySchedule", "block");
        _util.setDivPosition("divSearchMaterial", "none");
        //$("#OrdQty").focus();
    });



}


function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
 
    curEdit = true;
    var st = _util.getParameterByName('Estimate');

    document.getElementById('Fk_BoxEstimation').value = st;
   

}

function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Fk_Enquiry"] = $('#Fk_Enquiry').val();
    viewModel.data["QuotationDate"] = $('#QuotationDate').val();  

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Fk_Enquiry"] = $('#Fk_Enquiry').val();
    viewModel1.data["QuotationDate"] = $('#QuotationDate').val();
}


function setUpEnquirySearch(viewObject) {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldCommunicationType = "<input type='text' id='txtdlgCommunicationType' placeholder='Communication Type' class='input-large search-query'/>";


    //var txtFieldEnquiryFromDate = "<input type='text' id='txtdlgEnquiryFromDate' placeholder='EnquiryID' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldEnquiryToDate = "<input type='text' id='txtdlgEnquiryToDate' placeholder='EnquiryToDate' class='input-large search-query'/>";


    $('#MainSearhContainer').jtable({
        title: 'Enquiry List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Enquiry/EnquiryListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdDone').click();
            });
        },
        fields: {
            Pk_Enquiry: {
                title: 'Enquiry No',
                key: true
            },
            Date: {
                title: 'Date'
            },
            Customer: {
                title: 'Customer',
                edit: false
            },
            
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            Customer: $('#txtdlgCustomerName').val(),         
        });

    });
    $('#txtdlgEnquiryFromDate').datepicker({
        autoclose: true
    });
    $('#txtdlgEnquiryToDate').datepicker({
        autoclose: true
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdSearch').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            Customer: $('#txtdlgCustomerName').val(),
          
        });
    });


    $('#cmdDone').click(function (e) {
        e.preventDefault();
        $('#MainSearhContainer').jtable('load', {
            Customer: $('#txtdlgCustomerName').val(),          
        });
        var rows = $('#MainSearhContainer').jtable('selectedRows');
        $("#Fk_Enquiry").val(rows[0].keyValue);
        $("#searchDialog").modal("hide");
    });

}

function ReferenceFieldNotInitilized(viewModel) {


    //$('#txtfk_Customer').wgReferenceField({
    //    keyProperty: "fk_Customer",
    //    displayProperty: "CustomerName",
    //    loadPath: "Customer/Load",
    //    viewModel: viewModel
    //});


    $('#txtEstimationID').wgReferenceField({
        keyProperty: "Fk_EstimationID",
        displayProperty: "Pk_BoxEstimation",
        loadPath: "Estimation/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        $('#txtEstimationID').wgReferenceField("setData", viewModel.data["Fk_EstimationID"]);
      
    }
}
