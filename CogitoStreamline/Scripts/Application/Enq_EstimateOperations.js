﻿var curViewModel = null;
var curEdit = false;
var objShortCut = null;
var fkbox = 0;
var BName;
var TopID = 0;
var TopName = 0;
var PPly = 0;
var fkbox1 = 0;




function initialCRUDLoad() {
    TopID = _util.getParameterByName("Box");


  
    var bstr1 = TopID.search(",");
    fkbox1 = TopID.substr(0, bstr1);

  
   _page.showView('New');

}



function afterModelSaveEx(result) {
    //e.preventDefault();
    //alert(result.Message);
    if (result.Message != null) {
        //   fkbox1 = document.getElementById('hidPkBox').value;

        var bstr = result.Message;

        //var strlen=length(
        var bstr1 = bstr.search("-");
        var bstr2 = bstr.substr(0, bstr1);
        fkbox1 = bstr2;

        var bstr3 = bstr.search(".");
        var bstr4 = bstr.substr((bstr1 + 1))
        // fkbox1 = result.Message;
    }
    else {
        fkbox1 = document.getElementById('hidPkBox').value;

    }
    //window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);

    var BType = document.getElementById('Fk_BoxType').value;

    var FType = document.getElementById('Fk_FluteType').value;

    window.open("/BoxSpec?BoxMaster=" + fkbox1 + ", Type=" + BType + ", TakeUp~" + bstr4 + "; FType=" + FType);



};

function afterOneToManyDialogShow() {

    //$("#cmdMaterialSearch").click(function (e) {
    //    e.preventDefault();
    //    setUpMaterialSearch();
    //    _util.setDivPosition("divSearchMaterial", "block");
    //    _util.setDivPosition("divCreateMaterial", "none");
    //});
    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });

  
}
$(document).ready(function () {
    loadMainSearchContainer();
    $('#Qty').focus();
});


function beforeModelSaveEx() {
    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["Total"] = $('#SumVal').val();
    viewModel.data["convValue"] = $('#ConvRate').val();
    viewModel.data["transportValue"] = $('#TransportValue').val();
    viewModel.data["gMarginValue"] = $('#MargV').val();
    viewModel.data["taxesValue"] = $('#Taxes').val();
    viewModel.data["VATValue"] = $('#VATV').val();
    viewModel.data["CSTValue"] = $('#CSTV').val();
    viewModel.data["NetTotal"] = $('#NetTotal').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["Total"] = $('#SumVal').val();
    viewModel1.data["convValue"] = $('#ConvRate').val();
    viewModel1.data["transportValue"] = $('#TransportValue').val();
    viewModel1.data["gMarginValue"] = $('#MargV').val();
    viewModel1.data["taxesValue"] = $('#Taxes').val();
    viewModel1.data["VATValue"] = $('#VATV').val();
    viewModel1.data["CSTValue"] = $('#CSTV').val();
    viewModel1.data["NetTotal"] = $('#NetTotal').val();

    //viewModel.data["Fk_Indent"] = IndentNo;
}




function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchMHeader").text("Paper Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldMaterialName);

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            Name: {
                title: 'Material Name',
                edit: false
            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
            //Deckle: {
            //    title: 'Deckle',
            //    edit: false
            //},


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val()
        });
    });



    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');

        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //$("#GSM").val(rows[0].data.GSM);
        //$("#BF").val(rows[0].data.BF);
        //$("#Deckel").val(rows[0].data.Deckle);

        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");

    });
}
function resetOneToManyForm(property) {

    $("#txtFk_Material").val("");

}
function afterNewShow(viewObject) {
    TopID = _util.getParameterByName("Box");



    var bstr1 = TopID.search(",");
    fkbox1 = TopID.substr(0, bstr1);
  

    //alert("hi:");

    //var x = document.getElementById('divSearchArea');
    //if (x.style.display == 'none') {
    //    x.style.display = 'block';
    //} else {
    //    x.style.visibility = 'hidden';
    //}
  

    
}

function loadMainSearchContainer()
{
    $('#MainSearchContainer').jtable({
        title: 'Box for Estimation',
        paging: true,
        pageSize: 8,
        useBootstrap: true,
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter?Pk_BoxID=' + fkbox1,

        },
        fields: {
            Details: {
                title: 'Det..',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Box Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/BoxMaster/SBoxDet?Pk_BoxID=' + bstr2
                                        },

                                        fields: {
                                            Pk_PartPropertyID: {
                                                title: 'Property ID',
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                            },
                                            PName: {
                                                title: 'PName',

                                            },
                                            Length: {
                                                title: 'Length',

                                            },
                                            Width: {
                                                title: 'Width',
                                            },
                                            Height: {
                                                title: 'Height',

                                            },
                                            Weight: {
                                                title: 'Weight',
                                                width: '2%'
                                            },

                                            Details: {
                                                title: 'Spec',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Specifications',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                        },
                                                                        fields: {
                                                                            Pk_PartPropertyID: {
                                                                                title: 'Property ID',
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: false
                                                                            },
                                                                            Length: {
                                                                                title: 'Length',
                                                                                key: false
                                                                            },
                                                                            Width: {
                                                                                title: 'Width'

                                                                            },
                                                                            Height: {
                                                                                title: 'Height',
                                                                                width: '2%'
                                                                            },
                                                                            Weight: {
                                                                                title: 'Weight',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },


            Pk_BoxID: {
                title: 'BoxID',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            //PartNo: {
            //    title: 'PartNo',
            //    width: '25%'
            //},
            Name: {
                title: 'Name',
                width: '25%'
            },
            BTypeId: {
                title: 'BTypeId',
                width: '25%',
                list: false
            },
            Customer: {
                title: 'Customer',
                width: '25%',
                list: true
            },
            Fk_FluteType: {
                title: 'FType',
                width: '25%',
                list: false
            },

            BoxDetails: {
                title: 'P.Det.',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-copy'></i>");
                    $(button).click(function () {

                        fkbox = row.record.Pk_BoxID;
                        BName = row.record.Name;
                        document.getElementById('txtBoxid').value = fkbox;
                        _comLayer.parameters.add("Boxid", fkbox);
                                            var oResult1 = _comLayer.executeSyncAction("/BoxMaster/FindWeight", _comLayer.parameters);
                                            document.getElementById('txtBoxWt').value = oResult1;
                        setUpOrderSearch();

                        /////////////////////////


                    });
                    return button;
                }
            },

            //B_Spec: {
            //    title: 'B_Spec',
            //    width: '25%',
            //    listClass: 'child-opener-image-column',
            //    display: function (row) {

            //        var button1 = $("<button class='bnt'>View Specs</button>");
            //        button1.click(function (e) {
            //            fkbox = row.record.Pk_BoxID;
            //            PPly = row.record.Ply;
            //            TypeId = row.record.BTypeId;
            //            var FluteT = row.record.Fk_FluteType;
            //            //_comLayer.parameters.add("fkbox", fkbox);
            //            //_comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
            //            window.open("/BoxSpec?BoxMaster=" + fkbox + "," + "Type=" + TypeId + "," + "FType=" + FluteT);
            //            //window.open("/BoxSpec?BoxMaster=" + fkbox );
            //            window.parent.close();
            //            window.location.reload(false);
            //        });
            //        return button1;
            //    }

            //},


            Details: {
                title: 'Det..',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Box Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.Pk_BoxID
                                        },
                                        fields: {
                                            Pk_BoxSpecID: {
                                                title: 'Spec ID',
                                                key: true,
                                                list: false
                                            },
                                            OYes: {
                                                title: 'OuterShell',
                                                display: function (data) {
                                                    if (data.record.OYes == true || data.record.OYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //else {
                                                    //    list:false;
                                                    //    //width: '2%';

                                                    //}
                                                },
                                                //list: true
                                            },
                                            CYes: {
                                                title: 'Cap',
                                                display: function (data) {
                                                    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //else {
                                                    //    return 'No';
                                                    //}
                                                },
                                            },
                                            LPYes: {
                                                title: 'L.Partition',
                                                display: function (data) {
                                                    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //else {
                                                    //    width: '2%';
                                                    //}
                                                },
                                            },
                                            WPYes: {
                                                title: 'W.Partition',
                                                display: function (data) {
                                                    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //    else {
                                                    //        list: false;
                                                    //    }
                                                },
                                            },
                                            PYes: {
                                                title: 'Plate',
                                                display: function (data) {
                                                    if (data.record.PYes == true || data.record.PYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //    else {
                                                    //        list: false;
                                                    //    }
                                                },
                                            },
                                            TYes: {
                                                title: 'Top',
                                            },
                                            BYes: {
                                                title: 'Bottom',
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Specifications',
                                                                        paging: true,
                                                                        pageSize: 5,
                                                                        actions: {
                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                        },
                                                                        fields: {
                                                                            Pk_PartPropertyID: {
                                                                                title: 'Property ID',
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                width: '2%',
                                                                                list: false
                                                                            },
                                                                            Length: {
                                                                                title: 'Length',
                                                                                key: false,
                                                                                width: '2%'
                                                                            },
                                                                            Width: {
                                                                                title: 'Width',
                                                                                width: '2%'
                                                                            },
                                                                            Height: {
                                                                                title: 'Height',
                                                                                width: '2%'
                                                                            },
                                                                            Weight: {
                                                                                title: 'Weight',
                                                                                width: '2%'
                                                                            },
                                                                            BS: {
                                                                                title: 'BS',
                                                                                width: '2%'
                                                                            },
                                                                            Details: {
                                                                                title: '',
                                                                                width: '1%',
                                                                                sorting: false,
                                                                                paging: true,
                                                                                pageSize: 5,
                                                                                edit: false,
                                                                                create: false,
                                                                                listClass: 'child-opener-image-column',
                                                                                display: function (data) {
                                                                                    var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                    $img.click(function () {
                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                    $img.closest('tr'),
                                                                                                    {
                                                                                                        title: 'Layers Details',
                                                                                                        paging: true,
                                                                                                        pageSize: 10,
                                                                                                        actions: {
                                                                                                            listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                        },
                                                                                                        fields: {
                                                                                                            Pk_LayerID: {
                                                                                                                title: 'Layer ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                list: false
                                                                                                            },
                                                                                                            GSM: {
                                                                                                                title: 'GSM',
                                                                                                                key: false
                                                                                                            },
                                                                                                            BF: {
                                                                                                                title: 'BF'
                                                                                                            },
                                                                                                            Weight: {
                                                                                                                title: 'Weight'
                                                                                                            }

                                                                                                        },
                                                                                                        formClosed: function (event, data) {
                                                                                                            data.form.validationEngine('hide');
                                                                                                            data.form.validationEngine('detach');
                                                                                                        }
                                                                                                    }, function (data) { //opened handler
                                                                                                        data.childTable.jtable('load');
                                                                                                    });
                                                                                    });
                                                                                    return $img;
                                                                                }
                                                                            },
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
        },
    });
    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    //    $('#MainSearchContainer').jtable('load', {
    //        CustName: $('#txtCust').val(),
    //        Name: $('#txtName').val(),
    //        Pk_BoxID: fkbox1
    //    });
    //});
    //$('#LoadRecordsButton').click();
    $('#MainSearchContainer').jtable('load');

    document.getElementById('Qty').value = 1;
}
function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='hidden' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='hidden' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdCustDone').click();
            });
        },

        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            //CustomerAddress: {
            //    title: 'Address'
            //},
            City: {
                title: 'City'
            }
            //CustomerContact: {
            //    title: 'Office Contact'
            //},
            //Email: {
            //    title: 'Email'
            //}
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdCustSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdCustDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        if (rows.length > 0)
        { $('#Customer').wgReferenceField("setData", rows[0].keyValue); }
        $("#searchCustomerDialog").modal("hide");
    });

}
function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#tlbMaterials').jtable({
        title: 'Papers',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxMaster/Bounce',
            //updateAction: ''
            deleteAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            Name: {
                title: 'Paper Name'

            },
            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
        }
    });


    var oSCuts = viewObject.viewModel.data.BoxDetails();
    viewObject.viewModel.data["BoxDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new MaterialIndent();
        oCut.load(oSCuts[i].Pk_BoxCID, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].GSM, oSCuts[i].BF);
        viewObject.viewModel.data["BoxDetails"].push(oCut);
        i++;
    }

    //  configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "_AddMaterialIndent", function () { return new MaterialIndent() });
    //configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "BoxDetails", "BoxMaster", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "BoxDetails", "BoxMaster", "_AddMaterial", function () { return new MaterialIndent() });

    bEditContext = true;

    $('#cmdBoxSpecs').click(function (e) {
        e.preventDefault();
        //window.open("/Estimation?Enquiry=" + $('#hidPkEnquiry').val());
        fkbox = $('#hidPkBox').val()
        _comLayer.parameters.add("fkbox", fkbox);
        _comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
        window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val() + "&Ply=" + $('#Ply').val());


    });
}
function ReferenceFieldNotInitilized(viewModel) {

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    $('#Customer').wgReferenceField({
        keyProperty: "Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        //$('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);

        $('#Customer').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function setUpOrderSearch(viewObject) {
    BoxID = fkbox;
    QtyVal = document.getElementById('Qty').value;


    $('#divPaperStock').jtable({
        title: 'Box Paper List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/Order/GetPaperDetails?Fk_BoxID=' + BoxID,
            //updateAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');

               
                
                document.getElementById('Pk').value = $(this).data("record").Pk_Material;
                document.getElementById('name').value = $(this).data("record").MName;
            
                var PaperReq = 0;
                var wt = $(this).data("record").PaperWt;
                if (wt < 1)
                {
                    PaperReq = QtyVal * wt;
                }
                else
                {
                    PaperReq = (QtyVal * wt);
                }
                 

                document.getElementById('PReq').value = PaperReq;
                $("#Rate").focus();
            });
                
          
        },

        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Pk_Material: {
                title: 'Material ID',
                key: false,
                list: true
            },
            //Pk_LayerId: {
            //    title: 'Pk_LayerId',
            //    key: false,
            //    list: false
            //},

            MName: {
                title: 'Mat.Name'

            },
            //StkQty: {
            //    title: 'Ex.Stock',
            //    key: false
            //},
            PaperWt: {
                title: 'Paper Wt.',
                key: false
            },
            PaperReq: {
                title: 'Paper Req.',
                display: function (data) {
                    if (data.record.PaperWt > 0) {
                        if (data.record.PaperWt * QtyVal < 1)
                        { return (data.record.PaperWt * QtyVal); }
                        else
                        { return ((data.record.PaperWt * QtyVal)) };

                    }
                }
            }
        }
    });

    $('#divPaperStock').jtable('load', {
        Fk_BoxID: $('#txtBoxid').val(),


    });
}
function Calc1()
{
   
    var Rate =document.getElementById('Rate').value;
    var qty = document.getElementById('PReq').value;
   document.getElementById('Cost').value = (Rate * qty);
}

function AddData() {
   
    var rows = "";
    var Pk = document.getElementById("Pk").value;
    var name = document.getElementById("name").value;
      
    var Qty = document.getElementById("Qty").value;
    var Rate = document.getElementById("Rate").value;
    var Cost = document.getElementById("Cost").value;
    var PapReq = document.getElementById("PReq").value;
    //var total = document.getElementById("Total").value;

    rows += "<tr><td>" + Pk + "</td><td>" + name + "</td><td>" + PapReq + "</td><td>" + Rate + "</td><td>" + Cost + "</td>"
      //  rows += "<tr><td>" + name + "</td><td>" + gender + "</td><td>" + age + "</td><td>" + city + "</td></tr>";
        $(rows).appendTo("#list tbody");
        
        document.getElementById("name").value = "";
        document.getElementById("Pk").value = "";
        //document.getElementById("Qty").value = "";
        document.getElementById("Cost").value = "";
        document.getElementById("PReq").value = "";
        document.getElementById("Rate").value = "";

        FunCalc();
        //window.location.reload(true);
     
    }

function SaveData() {
    var bstr = _util.getParameterByName("Box");
    var bstr1 = bstr.search(",");
    var bstr2 = bstr.substr(0, bstr1);

    var Nstr = bstr.search("EnqChild");
    var nstr = bstr.substr(bstr1, nstr);
    var nLen = nstr.length;
    var Nstr1 = nstr.search("=");
    var Nstr2 = nstr.substr(Nstr1+1, nLen);
    var Nstr3 = Nstr2.search(",");
    var Nstr4 = Nstr2.search("=");
    var Nstr5 = Nstr2.substr(Nstr4 + 1, Nstr3 - 1);

    var Fk_EnquiryChild = Nstr2;
    var ConvRate = document.getElementById('Conv').value;
    var ConvValue = document.getElementById('ConvRate').value;
    var GMarginPercentage = document.getElementById('Marg').value;
    var GMarginValue = document.getElementById('MargV').value;
    var TaxesPercntage = document.getElementById('Tax').value;
    var TaxesValue = document.getElementById('Taxes').value;
    var TransportPercentage = document.getElementById('Transport').value;
    var TransportValue = document.getElementById('TransportValue').value;

    var Printing = document.getElementById('Printing').value;
    var Development = document.getElementById('Development').value;
    var Inventory = document.getElementById('Inventory').value;
    var TotalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var totADD =0;
    var TotalPrice = Number(totRate) + Number(totADD);
    var VATPercentage = document.getElementById('VAT').value;
    var VATValue = document.getElementById('VATV').value;
    var CSTPercentage = document.getElementById('CST').value;
    var CSTValue = document.getElementById('CSTV').value;
    var QtyValue = document.getElementById('Qty').value;

    _comLayer.parameters.add("Fk_EnquiryChild", Fk_EnquiryChild);
    _comLayer.parameters.add("ConvRate", ConvRate);
    _comLayer.parameters.add("ConvValue", ConvValue);
    _comLayer.parameters.add("GMarginPercentage", GMarginPercentage);
    _comLayer.parameters.add("GMarginValue", GMarginValue);
    _comLayer.parameters.add("TaxesPercntage", TaxesPercntage);
    _comLayer.parameters.add("TaxesValue", TaxesValue);
    _comLayer.parameters.add("TransportValue", TransportValue);
    _comLayer.parameters.add("Printing", Printing);
    _comLayer.parameters.add("Development", Development);
    _comLayer.parameters.add("Inventory", Inventory);
    _comLayer.parameters.add("TotalWeight", TotalWeight);
    _comLayer.parameters.add("TotalPrice", TotalPrice);
    _comLayer.parameters.add("VATPercentage", VATPercentage);
    _comLayer.parameters.add("VATValue", VATValue);
    _comLayer.parameters.add("CSTPercentage", CSTPercentage);
    _comLayer.parameters.add("CSTValue", CSTValue);
    _comLayer.parameters.add("TransportPercentage", TransportPercentage);

    _comLayer.parameters.add("QtyValue", QtyValue);
    var oResult = _comLayer.executeSyncAction("EnqEstimate/Save", _comLayer.parameters);
    //window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val() + "&Ply=" + $('#Ply').val());
    if (oResult.Success) {
        //_util.showSuccess("Data Saved successfully", function () { document.location.href = "/Estimation"; });
        alert("Data Saved successfully");
        var x = window.open('/Estimation');
        x.focus();
        window.parent.close();
        window.location.reload(false);
    }
}

function FunCalc() {
    var WastePer = 0;
    var WasteValue = 0;

    var n1 = document.getElementById("list").rows.length;

    var i=0,j=0;
    var str = "";
    var SVal = 0;
 
    for(i=0; i<n1;i++){
 
        var n2 = document.getElementById("list").rows[i].cells.length;
 
        for(j=0; j<n2;j++){
 
            var x=document.getElementById("list").rows[i].cells.item(j).innerHTML;
 
            str=str+x+":";


            if (i > 0) {
                if (j == 4) {
                    var Temp = 0;
                    Temp = x;
                    SVal = Number(SVal) +Number(Temp);
                }
            }
        }
        str=str+"#";
   
  
    }
   // document.getElementById("SumVal").innerHTML=SVal;
    WastePer = document.getElementById('Wastage').value;
    if (WastePer < 0 || WastePer == undefined)
    { WastePer = 1; }
    else
    { WasteValue = Number(WastePer / 100) * SVal; }

    SVal=SVal + WasteValue;

    document.getElementById('SumVal').value = SVal;
}


function CalcConvRate() {
    
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var QtyVal = document.getElementById('Qty').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;


    var convRate = document.getElementById('Conv').value;

    ///////////////////

    var n1 = document.getElementById("list").rows.length;
    WastePer = document.getElementById('Wastage').value;

    var i = 0, j = 0;
    var str = "";
    var PVal = 0;
    var CVal = 0;
    var TempVal = 0;
    var NetConv = 0;
    for (i = 0; i < n1; i++) {

        var n2 = document.getElementById("list").rows[i].cells.length;

        for (j = 0; j < n2; j++) {

            var x = document.getElementById("list").rows[i].cells.item(j).innerHTML;

            str = str + x + ":";


            if (i > 0) {
                if (j == 2) {
                    var Temp = 0;
                    Temp = x;
                    PVal = Number(PVal) + Number(Temp);

                    CVal =PVal+( PVal * (WastePer/100))


                    TempVal = TempVal + CVal;

                   
                }
             
            }
        }
        str = str + "#";
        NetConv = ((Number(CVal) * Number(convRate)));

    }
    // document.getElementById("SumVal").innerHTML=SVal;
    //WastePer = document.getElementById('Wastage').value;
    //if (WastePer < 0 || WastePer == undefined)
    //{ WastePer = 1; }
    //else
    //{ WasteValue = Number(WastePer / 100) * PVal; }

    //PVal = PVal + WasteValue;

    document.getElementById('ConvRate').value = NetConv;






    ///////////////////////
     //Calculations
    //convValue = (Number(convRate) * Number(QtyVal) * Number(totalWeight), 2);
    //document.getElementById('ConvRate').value = convValue;

      
}

//inventory-- - rejectionvalue
//cgst--taxesvalue
//sgst--vatvalue




function CalcGMarginValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;
    SumVal
    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    var PaperCost = document.getElementById('SumVal').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory= document.getElementById('Inventory').value;
    var Development= document.getElementById('Development').value;
    var TransVal = document.getElementById('TransportValue').value;
    var gMarginPercentage = document.getElementById('Marg').value;


    var gMarginValue = (Number(convValue) + Number(PaperCost) + Number(Printing) + Number(Development) + Number(TransVal)) * Number(gMarginPercentage) / 100;
    document.getElementById('MargV').value = (gMarginValue);

    var Margin = document.getElementById('MargV').value;

    document.getElementById('NetAmt').value = Number(convValue) + Number(PaperCost) + Number(Printing) + Number(Inventory) + Number(Development) + Number(TransVal) + Number(Margin);

    transportValue = document.getElementById('TransportValue').value;

    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  Number(Inventory) +
                  Number(Printing) +
                  Number(Development) 

                 );

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcEDValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
  var Transport = document.getElementById('TransportValue').value;
    var Printing = document.getElementById('Printing').value;
    var Inventory= document.getElementById('Inventory').value;
    var Development= document.getElementById('Development').value;


    var gMarginValue = document.getElementById('MargV').value;
    var taxesPercntage = document.getElementById('Tax').value;
    //var taxesValue = (Number(convValue) + Number(totRate)  + Number(gMarginValue)) * (Number(taxesPercntage / 100));

    var taxesValue = (Number(totRate) + Number(convValue) + Number(Printing) + Number(Development) + Number(Inventory) + Number(Transport) + Number(gMarginValue)) * (Number(taxesPercntage) / 100);
    document.getElementById('Taxes').value = taxesValue;

    transportValue = document.getElementById('TransportValue').value;



    totalAddPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  Number(Printing) +
                  Number(Inventory) +
                  Number(Development) 
                 );

    //estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcVATValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
 // var rejectionValue = document.getElementById('Rejection').value;
    var Development= document.getElementById('Development').value;   
    var Printing = document.getElementById('Printing').value;
    var Inventory= document.getElementById('Inventory').value;
    var Transport = document.getElementById('TransportValue').value;
    var gMarginValue = document.getElementById('MargV').value;

    var taxesValue = document.getElementById('Taxes').value;

    var VATPercentage = document.getElementById('VAT').value;


    var VatValue = (Number(totRate) + Number(convValue) + Number(Printing) + Number(Development) + Number(Inventory) + Number(Transport) + Number(gMarginValue)) * (Number(VATPercentage) / 100);
    document.getElementById('VATV').value = VatValue;

 

    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +                  
                    Number(Inventory) +
                  Number(Printing) +
                  Number(Development) 
                 );

    //estModel.totalPrice(totalPrice);
   // rateChanged(Estimation);
    //alert(totalPrice);

    CalcNet();
}


function CalcCSTValue() {

    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
   // var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;
    var VatValue = document.getElementById('VATV').value;
    TransportValue = document.getElementById('TransportValue').value;
 //   var weightHValue = document.getElementById('WeightH').value;
    var Development= document.getElementById('Development').value;   
    var Printing = document.getElementById('Printing').value;
    var Inventory= document.getElementById('Inventory').value;

    //var handlingChanrgesValue = document.getElementById('HCharges').value;
    //var packingChargesValue = document.getElementById('PCharges').value;


    var CSTPercentage = document.getElementById('CST').value;
    var CstValue = (Number(convValue) + Number(totRate) + Number(gMarginValue)) * (Number(CSTPercentage) / 100);
    document.getElementById('CSTV').value = CstValue;





    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(TransportValue) +                
               Number(Inventory) +
                  Number(Printing) +
                  Number(Development)
                  );

   // rateChanged(Estimation);
    // estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}


function CalcTransValue() {
    var totalWeight = document.getElementById('txtBoxWt').value;
    var totalQty = document.getElementById('Qty').value;
    var totRate = document.getElementById('SumVal').value;
    var taxesValue = 0;
    var transportValue = 0;
    var weightHValue = 0;
    //Now rate calculation
    //Declaring variables
    var convValue = document.getElementById('ConvRate').value;
    //var rejectionValue = document.getElementById('Rejection').value;
    var gMarginValue = document.getElementById('MargV').value;
    var taxesValue = document.getElementById('Taxes').value;
    var VatValue = document.getElementById('VATV').value;
    TransportValue = document.getElementById('TransportValue').value;
    TransportPercentage = document.getElementById('Transport').value;
   // var weightHValue = document.getElementById('WeightH').value;
    //var handlingChanrgesValue = document.getElementById('HCharges').value;
    //var packingChargesValue = document.getElementById('PCharges').value;
    var Development= document.getElementById('Development').value;   
    var Printing = document.getElementById('Printing').value;
    var Inventory= document.getElementById('Inventory').value;



    if (TransportValue == null || TransportValue == 0 || TransportValue == '') {
        if (TransportPercentage != null || TransportPercentage != 0) {

         //   var TransportValue = (Number(convValue) + Number(totRate)) * Number(TransportPercentage);
            var TransportValue = Number(totalQty) * Number(TransportPercentage);
            document.getElementById('TransportValue').value = TransportValue;
        }


    }

    CalcInv();
    //var CSTPercentage = document.getElementById('CST').value;
    //var CstValue = (Number(convValue) + Number(totRate) + Number(rejectionValue) + Number(gMarginValue) + Number(taxesValue)) * (Number(CSTPercentage) / 100);
    //document.getElementById('CSTV').value = CstValue;






    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(TransportValue) +                 
                     Number(Inventory) +
                  Number(Printing) +
                  Number(Development) 
                );

   // rateChanged(Estimation);
    // estModel.totalPrice(totalPrice);

    //alert(totalPrice);
}

function CalcPrint()
{
    var Qtyval = document.getElementById('Qty').value;
    var PrintVal = document.getElementById('PrintingPer').value;
    document.getElementById('Printing').value = Number(Qtyval) * Number(PrintVal);

}

function CalcDev() {
    var Qtyval = document.getElementById('Qty').value;
    var DevVal = document.getElementById('DevelopmentPer').value;
    document.getElementById('Development').value = Number(Qtyval) * Number(DevVal);

}
function CalcInv() {
    var Qtyval = document.getElementById('Qty').value;
    var DevVal = document.getElementById('Development').value;
    var PapVal = document.getElementById('SumVal').value;
    var PrintVal = document.getElementById('Printing').value;
    var ConvVal = document.getElementById('ConvRate').value;
    var TransVal = document.getElementById('TransportValue').value;
    var Invper = document.getElementById('InvPer').value;
    //document.getElementById('Inventory').value = (Number((Invper/100)) * (Number(Qtyval) + Number(DevVal) + Number(PapVal) + Number(ConvVal) + Number(TransVal)));
    document.getElementById('Inventory').value = (Number(ConvVal) + Number(PapVal) + Number(PrintVal) + Number(DevVal) + Number(TransVal)) * Number(Invper) / 100;
}

function CalcNet()
{
    var f=0;
  var a=  document.getElementById('ConvRate').value;
   //var b= document.getElementById('Rejection').value;
   var c = document.getElementById('MargV').value;
   var d = document.getElementById('Taxes').value;
   var e = document.getElementById('VATV').value;
   if (document.getElementById('CSTV').value == "") {
       f = "0";
   }
   var g = document.getElementById('TransportValue').value;
 //  var h = document.getElementById('WeightH').value;
   var i = document.getElementById('Printing').value;
   var j = document.getElementById('Development').value;
   var k = document.getElementById('Inventory').value;

 

   var NetVal = Number(a) +  Number(c) + Number(d) + Number(e) + Number(f) + Number(g) +  Number(i) + Number(j) + Number(k);
    
   //document.getElementById('Total').value=NetVal;
 
    //document.getElementById('SumVal').value


   
   var PC=document.getElementById('SumVal').value; 

  

   var NVal=Number(NetVal) +Number(PC);
 
   document.getElementById('NetTotal').value=(NVal); 
   
    
   //SaveData();
}

function CalcWeight()
{

    var length = document.getElementById('Length').value;
    var width = document.getElementById('Width').value;
    var height = document.getElementById('Height').value;
    var gsm = document.getElementById('GSM').value;
    var tkf = document.getElementById('TKF').value;

    var TLength = (Number(length) * 2 + Number(width) * 2) + Number(50);    /////////=cut length
    var deckel = ((Number(width) + Number(height) + 20) / 10);
    var addl = ((TLength * deckel) / 100000);

    //var TF = Math.floor(132 / Number(deckel)); /////// field takeupfactor--- screen name no.of ups

    //model.TakeUpfactor(TF);
    ///////////////no of ups

    //Cuttingsize

    //var cuttingsize = (TLength / 10);

    //model.cuttingSize(cuttingsize);
    ///////////////cutting size

    ///////////////////
    //var addl = ((TLength * deckel) / 1000000);

    //var GSM = model.OLength1();
    //var TkF = model.OHeight1();

    var CWeight = (addl * Number(tkf) * Number(gsm))/1000 ;

    document.getElementById('Weight').value = CWeight;
}