﻿var curViewModel = null;
var curEdit = false;
var objShortCut = null;
var fkbox = 0;
var TopID = 0;
var TopName = 0;
var PPly = 0;
var fkbox1 = 0;
var copyPaste = false;



function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Box  List',
        paging: true,
        pageSize: 8,    
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter',
            updateAction: '',
            createAction: '',
            //deleteAction: ''
          
        },
        fields: {
            Details: {
                title: '',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Box Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.Pk_BoxID

                                        },
                                       
                                        fields: {
                                            Pk_BoxSpecID: {
                                                title: 'Spec ID',
                                                key: true,
                                                list:false
                                            },
                                            OYes: {
                                                title: 'OuterShell',
                                             
                                            },
                                            CYes: {
                                                title: 'Cap',
                                                //display: function (data) {
                                                //    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                //        list: true;
                                                //    }
                                                //    else {
                                                //        list: false;
                                                //    }
                                                //},
                                            },
                                            LPYes: {
                                                title: 'L.Partition',
                                                //display: function (data) {
                                                //    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                //        list: true;
                                                //    }
                                                //    else {
                                                //        list: false;
                                                //    }
                                                //},
                                            },
                                            WPYes: {
                                                title: 'W.Partition',
                                                //display: function (data) {
                                                //    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                //        list: true;
                                                //    }
                                                //    else {
                                                //        list: false;
                                                //    }
                                                //},
                                            },
                                            PYes: {
                                                title: 'Plate',                                                
                                            },
                                            TYes: {
                                                title: 'Top',
                                            },
                                            BYes: {
                                                title: 'Bottom',
                                            },
                                            Details: {
                                                title: 'Box Det.',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Specifications',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                        },
                                                                       
                                                                      
                                                                        fields: {
                                                                            Pk_PartPropertyID: {
                                                                                title: 'Property ID',
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                list: true
                                                                            },
                                                                            Length: {
                                                                                title: 'Length',
                                                                                key: false
                                                                            },
                                                                            Width: {
                                                                                title: 'Width'

                                                                            },
                                                                            Height: {
                                                                                title: 'Height',
                                                                                width: '2%'
                                                                            },
                                                                            Weight: {
                                                                                title: 'Weight',
                                                                                width: '2%'
                                                                            },
                                                                            BS: {
                                                                                title: 'BS',
                                                                                width: '2%'
                                                                            }
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
           

            Pk_BoxID: {
                title: 'Box ID',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            //PartNo: {
            //    title: 'PartNo',
            //    width: '25%'
            //},
            Name: {
                title: 'Name',
                width: '25%'
            },
            BTypeId: {
                title: 'BTypeId',
                width: '25%',
                list:false
            },
            Customer: {
                title: 'Customer',
                width: '25%',
                list: true
            },
            Fk_FluteType: {
                title: 'FType',
                width: '25%',
                list: false
            },

            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        fkbox = row.record.Pk_BoxID;
                        var BType = row.record.BoxType;
                        _comLayer.parameters.add("fkbox", fkbox);
                        if (BType != "Plain Board") {
                            _comLayer.executeSyncActionPrint("BoxMaster/Print", _comLayer.parameters);
                            var strval = "ConvertPDF/BoxReport" + fkbox + ".pdf"

                            window.open(strval, '_blank ', 'width=700,height=250');
                        }
                        else {
                            _comLayer.executeSyncActionPrint("BoxMaster/BoxRepPBoard", _comLayer.parameters);
                            var strval = "ConvertPDF/BoxReportPBoard" + fkbox + ".pdf"

                            window.open(strval, '_blank ', 'width=700,height=250');
                        }
                    });
                    return button;
                }
            },

            B_Spec: {
                title: 'B_Spec',
                width: '25%',
                listClass: 'child-opener-image-column',
                display: function (row) {
                
                    var button1 = $("<button class='bnt'>View Specs</button>");
                    button1.click(function (e) {
                        fkbox = row.record.Pk_BoxID;
                        PPly = row.record.Ply;
                        TypeId = row.record.BTypeId;
                        var FluteT = row.record.Fk_FluteType;
                        //_comLayer.parameters.add("fkbox", fkbox);
                        //_comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
                        window.open("/BoxSpec?BoxMaster=" + fkbox + "," + "Type=" + TypeId + "," + "FType=" + FluteT);
                        //window.open("/BoxSpec?BoxMaster=" + fkbox );
                        
                    });
                    return button1;
                }
            
            },
            Copy: {
                title: 'Copy',
                width: '25%',
                listClass: 'child-opener-image-column',
                display: function (row) {

                    var button1 = $("<button class='bnt'>Copy</button>");
                    button1.click(function (e) {
                        fkbox = row.record.Pk_BoxID;
                        //alert(fkbox);
                        copyPaste = true;
                        PPly = row.record.Ply;
                        TypeId = row.record.BTypeId;
                        var FluteT = row.record.Fk_FluteType;
                        //_comLayer.parameters.add("fkbox", fkbox);
                        //_comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
                        //window.open("/BoxSpec?BoxMaster=" + fkbox + "," + "Type=" + TypeId + "," + "FType=" + FluteT);
                        //window.open("/BoxSpec?BoxMaster=" + fkbox );
                        e.preventDefault();
                        _page.showView('New');
                        //document.getElementById('PartNo').value = fkbox;
                    });
                    return button1;
                }

            },

            Details: {
                title: 'Box Det.',
                width: '1%',
                sorting: false,
                paging: true,
                pageSize: 5,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Box Details',
                                        paging: true,
                                        pageSize: 10,
                                        actions: {
                                            listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.Pk_BoxID
                                        },
                                        
                                        fields: {
                                            Pk_BoxSpecID: {
                                                title: 'Spec ID',
                                                key: true,
                                                list:false
                                            },
                                            OYes: {
                                                title: 'OuterShell',
                                                display: function (data) {
                                                    if (data.record.OYes == true || data.record.OYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //else {
                                                    //    list:false;
                                                    //    //width: '2%';

                                                    //}
                                                },
                                                //list: true
                                            },
                                            CYes: {
                                                title: 'Cap',
                                                display: function (data) {
                                                    if (data.record.CYes == true || data.record.CYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //else {
                                                    //    return 'No';
                                                    //}
                                                },
                                            },
                                            LPYes: {
                                                title: 'L.Partition',
                                                display: function (data) {
                                                    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                    //else {
                                                    //    width: '2%';
                                                    //}
                                                },
                                            },
                                            WPYes: {
                                                title: 'W.Partition',
                                                display: function (data) {
                                                    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                        return 'Yes';
                                                    }
                                                //    else {
                                                //        list: false;
                                                //    }
                                                },
                                            },
                                            PYes: {
                                                title: 'Plate',
                                                display: function (data) {
                                                    if (data.record.PYes == true || data.record.PYes == 'True') {
                                                       return 'Yes';
                                                    }
                                                //    else {
                                                //        list: false;
                                                //    }
                                                },
                                            },
                                            TYes: {
                                                title: 'Top',
                                            },
                                            BYes: {
                                                title: 'Bottom',
                                            },
                                            Details: {
                                                title: '',
                                                width: '1%',
                                                sorting: false,
                                                paging: true,
                                                pageSize: 5,
                                                edit: false,
                                                create: false,
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                    $img.click(function () {
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Specifications',
                                                                        paging: true,
                                                                        pageSize: 5,
                                                                        actions: {
                                                                            listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                        },
                                                                        recordsLoaded: function (event, data) {
                                                                            $('.jtable-data-row').click(function () {
                                                                                var row_id = $(this).attr('data-record-key');

                                                                                //Pk_Material = $(this).data("record").Pk_Material;
                                                                                document.getElementById('txtPropid').value = row_id;

                                                                                document.getElementById('txtDeck').value = $(this).data("record").Deckle;
                                                                                document.getElementById('txtCutLength').value = $(this).data("record").CL;
                                                                                //document.getElementById('txtPartId').value = $(this).data("record").PartID;
                                                                                return true;
                                                                            });
                                                                        },
                                                                        fields: {
                                                                            Pk_PartPropertyID: {
                                                                                title: 'Property ID',
                                                                                key: true,
                                                                                create: false,
                                                                                edit: false,
                                                                                width: '2%',
                                                                                list: false
                                                                            },
                                                                            Length: {
                                                                                title: 'Length',
                                                                                key: false,
                                                                                width: '2%'
                                                                            },
                                                                            Width: {
                                                                                title: 'Width',
                                                                                width: '2%'
                                                                            },
                                                                            Height: {
                                                                                title: 'Height',
                                                                                width: '2%'
                                                                            },
                                                                            Weight: {
                                                                                title: 'Weight',
                                                                                width: '2%'                                                                            
                                                                        },
                                                                            Deckle: {
                                                                                title: 'Deckle',
                                                                                width: '2%'
                                                                            },
                                                                            CL: {
                                                                                title: 'CL',
                                                                                width: '2%'
                                                                            },

                                                                            Details: {
                                                                                title: '',
                                                                                width: '1%',
                                                                                sorting: false,
                                                                                paging: true,
                                                                                pageSize: 5,
                                                                                edit: false,
                                                                                create: false,
                                                                                listClass: 'child-opener-image-column',
                                                                                display: function (data) {
                                                                                    var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                    $img.click(function () {
                                                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                                                    $img.closest('tr'),
                                                                                                    {
                                                                                                        title: 'Layers Details',
                                                                                                        paging: true,
                                                                                                        pageSize: 10,
                                                                                                        actions: {
                                                                                                            listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                        },
                                                                                                        
                                                                                                        fields: {
                                                                                                            Pk_LayerID: {
                                                                                                                title: 'Layer ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                list: false
                                                                                                            },
                                                                                                            GSM: {
                                                                                                                title: 'GSM',
                                                                                                                key: false
                                                                                                            },
                                                                                                            BF: {
                                                                                                                title: 'BF'
                                                                                                            },
                                                                                                            Weight: {
                                                                                                                title: 'Weight'
                                                                                                            }

                                                                                                        },
                                                                                                        formClosed: function (event, data) {
                                                                                                            data.form.validationEngine('hide');
                                                                                                            data.form.validationEngine('detach');
                                                                                                        }
                                                                                                    }, function (data) { //opened handler
                                                                                                        data.childTable.jtable('load');
                                                                                                    });
                                                                                    });
                                                                                    return $img;
                                                                                }
                                                                            },
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    return $img;
                }
            },
        },
    });
    $('#LoadRecordsButton').click(function (e) {
                e.preventDefault();
                $('#MainSearchContainer').jtable('load', {
                    CustName: $('#txtCust').val(),
                    Name: $('#txtName').val(),
                    Pk_BoxID: $('#txtBoxid').val()
                });
            });
            $('#LoadRecordsButton').click();

            $('#cmdNew').click(function (e) {
                e.preventDefault();
                _page.showView('New');

        
            });
            $('#cmdpdf').click(function (e) {
                e.preventDefault();
                fkbox = document.getElementById('txtBoxid').value;
                _comLayer.parameters.add("fkbox", fkbox);
                _comLayer.executeSyncAction("BoxMaster/BoxDist", _comLayer.parameters);
                var strval = "ConvertPDF/Box" + fkbox + ".pdf"
                window.open(strval, '_blank ', 'width=700,height=250');

            });


            _page.getViewByName('New').viewModel.addAddtionalDataSources("BoxType", "getBoxType", null);
            _page.getViewByName('Edit').viewModel.addAddtionalDataSources("BoxType", "getBoxType", null);

            _page.getViewByName('New').viewModel.addAddtionalDataSources("FluteType", "getFluteType", null);
            _page.getViewByName('Edit').viewModel.addAddtionalDataSources("FluteType", "getFluteType", null);

            setUpCustomerSearch();

    ///cmdCopyPaste hide
            $('#cmdCopyPaste').hide();



            $('#cmdUpd').click(function (e) {
                e.preventDefault();
                fkbox = document.getElementById('txtPropid').value;
               var dec= document.getElementById('txtDeck').value;
                var CL=document.getElementById('txtCutLength').value;
                var Ups = document.getElementById('txtUps').value;


                _comLayer.parameters.add("fkbox", fkbox);
                _comLayer.parameters.add("dec", dec);
                _comLayer.parameters.add("CL", CL);
                _comLayer.parameters.add("Ups", Ups);


                _comLayer.executeSyncAction("BoxMaster/UpdateRow", _comLayer.parameters);
                //var strval = "ConvertPDF/Box" + fkbox + ".pdf"
                //window.open(strval, '_blank ', 'width=700,height=250');

                document.getElementById('txtPropid').value = "";
                document.getElementById('txtDeck').value = "";
                document.getElementById('txtCutLength').value = "";
                document.getElementById('txtUps').value = "";
                alert('Values Updated Successfully');

            });

            
        }

function afterModelSaveEx(result) {
    //e.preventDefault();
    //alert(result.Message);
    if (result.Message != null) {
        //   fkbox1 = document.getElementById('hidPkBox').value;

        var bstr = result.Message;
     
        //var strlen=length(
        var bstr1 = bstr.search("-");
        var bstr2 = bstr.substr(0, bstr1);
         fkbox1 = bstr2;

         var bstr3 = bstr.search(".");
         var bstr4 = bstr.substr((bstr1 + 1))
        // fkbox1 = result.Message;
         fkbox1 = fkbox1;


        //window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);

         var BType = document.getElementById('Fk_BoxType').value;

         var FType = document.getElementById('Fk_FluteType').value;

         window.open("/BoxSpec?BoxMaster=" + fkbox1 + ", Type=" + BType + ", TakeUp~" + bstr4 + "; FType=" + FType);
    }
    else if (result.Success == false) {
        alert("Box with the same name against the same customer already exists in the database.");
    }
    else {
        fkbox1 = document.getElementById('hidPkBox').value;


        //window.open("/FlexEst?Enquiry=" + BoxId + ", EnqChild," + oResult1.data.EnqChild + ', Fk_BoxEstimation,' + oResult1.data.BoxEstimation + ', Pk_BoxEstimationChild,' + oResult1.data.EstBoxChild);

        var BType = document.getElementById('Fk_BoxType').value;

        var FType = document.getElementById('Fk_FluteType').value;

        window.open("/BoxSpec?BoxMaster=" + fkbox1 + ", Type=" + BType + ", TakeUp~" + bstr4 + "; FType=" + FType);
    }

};

function afterOneToManyDialogShow() {

    //$("#cmdMaterialSearch").click(function (e) {
    //    e.preventDefault();
    //    setUpMaterialSearch();
    //    _util.setDivPosition("divSearchMaterial", "block");
    //    _util.setDivPosition("divCreateMaterial", "none");
    //});
    $('#cmdSearchMaterialsdetails').click(function (e) {
        e.preventDefault();
        setUpMaterialSearch();
        _util.setDivPosition("divSearchMaterial", "block");
        _util.setDivPosition("divCreateMaterial", "none");
    });
}
function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchMHeader").text("Paper Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldMaterialName);
    
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMaterialDone').click();
            });
        },

        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            Name: {
                title: 'Material Name',
                edit: false
            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
            //Deckle: {
            //    title: 'Deckle',
            //    edit: false
            //},
            

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMaterialSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtMaterial').val()
        });
    });



    $('#cmdMaterialDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');

        $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);
        //$("#GSM").val(rows[0].data.GSM);
        //$("#BF").val(rows[0].data.BF);
        //$("#Deckel").val(rows[0].data.Deckle);

        _util.setDivPosition("divCreateMaterial", "block");
        _util.setDivPosition("divSearchMaterial", "none");

    });
}
function resetOneToManyForm(property) {

    $("#txtFk_Material").val("");

}
function afterNewShow(viewObject) {
    var dNow = new Date();

    $("#dropzone").wgUpload({
        wgTag: "BoxMaster",
        upLoadUrl: "/FileUpload",
        property: "Documents",
        viewModel: viewObject.viewModel,
        fileLocation: _comLayer.curServer() + "/uploads/"
    });


    if (copyPaste == true) {
        $("#cmdCopyPaste").show();
       // $("#cmdCopyPaste").attr('disabled', 'disabled');
  
    }
    else {
        $("#cmdCopyPaste").hide();
    }



    $('#cmdCopyPaste').click(function (e) {
        e.preventDefault();
        //alert(fkbox);
        var BoxID = fkbox;
        _comLayer.parameters.add("Pk_BoxID", BoxID);

        var BName = document.getElementById('Name').value;
        _comLayer.parameters.add("BName", BName);
        oResult = _comLayer.executeSyncAction("BoxMaster/BoxCopyPaste", _comLayer.parameters);
        var ResVal = oResult.success;
        if (ResVal == null) {
            alert('Box Created Successfully');

            _page.showView('Search');
        }
        else {
            alert('Could not Create the Box !!');

            _page.showView('Search');
        }

    });
    curViewModel = viewObject.viewModel;
    bEditContext = false;
    $('#tlbMaterials').jtable({
        title: 'Papers',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxMaster/Bounce',
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Fk_Material: {
                title: 'Material',
                list: false
            },
            Name: {
                title: 'Paper Name'

            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
        }
    });
  
    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();
        $("#searchCustomerDialog").modal("show");
    });

    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "BoxDetails", "BoxMaster", "_AddMaterial", function () { return new MaterialIndent() });


}


function setUpCustomerSearch() {
    //Enquiry

    //cleanSearchDialog();

    //Adding Search fields
    var txtFieldCustomerName = "<input type='hidden' id='txtdlgCustomerName' placeholder='Customer Id' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNam = "<input type='text' id='txtdlgCustomerNam' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldCustomerNumber = "<input type='hidden' id='txtdlgCustomerContact' placeholder='Contact number' class='input-large search-query'/>&nbsp;&nbsp;";
    $("#dlgSearchFields").append(txtFieldCustomerName);
    $("#dlgSearchFields").append(txtFieldCustomerNam);
    $("#dlgSearchFields").append(txtFieldCustomerNumber);

    $('#SearchContainer').jtable({
        title: 'Customer List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/Customer/CustomerListByFiter',
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdCustDone').click();
            });
        },

        fields: {
            Pk_Customer: {
                title: 'CustomerID',
                key: true,
                list: false
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            //CustomerAddress: {
            //    title: 'Address'
            //},
            City: {
                title: 'City'
            }
            //CustomerContact: {
            //    title: 'Office Contact'
            //},
            //Email: {
            //    title: 'Email'
            //}
        }
    });


    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#cmdCustSearch').click(function (e) {
        e.preventDefault();
        $('#SearchContainer').jtable('load', {
            Pk_Customer: $('#txtdlgCustomerName').val(),
            CustomerName: $('#txtdlgCustomerNam').val(),
            CustomerContact: $('#txtdlgCustomerContact').val()
        });
    });

    $('#cmdCustDone').click(function (e) {
        e.preventDefault();
        var rows = $('#SearchContainer').jtable('selectedRows');
        if (rows.length > 0)
        { $('#Customer').wgReferenceField("setData", rows[0].keyValue); }
        $("#searchCustomerDialog").modal("hide");
    });

}
function afterEditShow(viewObject) {
    curViewModel = viewObject.viewModel;
    $('#tlbMaterials').jtable({
        title: 'Papers',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxMaster/Bounce',
            //updateAction: ''
            deleteAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            Name: {
                title: 'Paper Name'

            },
            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
        }
    });


    var BoxID = curViewModel.id;
    _comLayer.parameters.add("Pk_BoxID", BoxID);
    oResult = _comLayer.executeSyncAction("BoxMaster/SBoxDetails", _comLayer.parameters);
    //  var SBoxID = oResult.data[0].Pk_BoxID;
    var BType = oResult.data[0].BType;
    var FType = oResult.data[0].Fk_FluteType;
    var TakeUp = oResult.data[0].TakeUp;
    //var BType oResult.data[0].Pk_BoxID;
    window.open("/BoxSpec?BoxMaster=" + BoxID + ", Type=" + BType + ", TakeUp~" + TakeUp + "; FType=" + FType + "; Edit");

    var oSCuts = viewObject.viewModel.data.BoxDetails();
    viewObject.viewModel.data["BoxDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new MaterialIndent();
        oCut.load(oSCuts[i].Pk_BoxCID, oSCuts[i].Name, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].GSM, oSCuts[i].BF);
        viewObject.viewModel.data["BoxDetails"].push(oCut);
        i++;
    }

    //  configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "_AddMaterialIndent", function () { return new MaterialIndent() });
    //configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "BoxDetails", "BoxMaster", "NOTUSED", function () { return new MaterialIndent() }, "divCreateMaterialIndent");
    configureOne2Many("#cmdAddMaterial", '#tlbMaterials', "#cmdSaveMaterial", viewObject, "BoxDetails", "BoxMaster", "_AddMaterial", function () { return new MaterialIndent() });

    bEditContext = true;

    $('#cmdBoxSpecs').click(function (e) {
        e.preventDefault();
        //window.open("/Estimation?Enquiry=" + $('#hidPkEnquiry').val());
        fkbox = $('#hidPkBox').val()
        _comLayer.parameters.add("fkbox", fkbox);
        _comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
        window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val() + "&Ply=" + $('#Ply').val());


    });
}
function ReferenceFieldNotInitilized(viewModel) {

    $('#txtFk_Material').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });

    $('#Customer').wgReferenceField({
        keyProperty: "Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });


    if (viewModel.data != null) {
        //$('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtFk_Material').wgReferenceField("setData", viewModel.data["Fk_Material"]);

        $('#Customer').wgReferenceField("setData", viewModel.data["Fk_Indent"]);
    }
}

function CheckHeadDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add(  "Name", $('#txtName').val());
    oResult = _comLayer.executeSyncAction("BoxMaster/NameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Box Name Already added";
}

//function beforeOneToManySaveHook(objectContext) {
//    var bValidation = true;


//    if ($("#txtMaterial").val() == "") {
//        $("#txtMaterial").validationEngine('showPrompt', 'Select Paper', 'error', true)
//        bValidation = false;
//    }

//    return bValidation;
//    //curViewModel = viewObject.viewModel;

//}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();


}


function checkDuplicate() {
    //curViewModel = viewObject.viewModel;
    if (objContextEdit == false) {
        var i = 0;

        while (curViewModel.data["BoxDetails"]()[i]) {
            if (objContext.data.Fk_Material == curViewModel.data["BoxDetails"]()[i].data.Fk_Material) {

                return "* " + "Item Already added";
            }
            i++;
        }
    }
}
function CheckBNameDuplicate() {

    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#Name').val());
    oResult = _comLayer.executeSyncAction("BoxMaster/NameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Box Name Already added";

}

function Show() {
    var id = document.getElementById('Ply').value;

    var r = 0;
    
    var table = document.createElement('table'), tr, td, row, cell;
    for (row = 0; row < 10; row++) {
        tr = document.createElement('tr');
        for (cell = 0; cell < 3; cell++) {
            td = document.createElement('td');
            tr.appendChild(td);
            td.innerHTML = row * 3 + cell + 1;
        }
        table.appendChild(tr);
    }
    document.getElementById('container').appendChild(table);
}

    //for (i=0;i<id;i++)
    //{
    //    var row = $("<tr><td><input type='text' name='AliasFirstNames' value='" + First + i + "'>" + First +i+ "</td></tr>");
    //    //var row = $("<tr><td><input type='text' name='AliasFirstNames' value='" + First + i + "'>" + First + i + "</td><td><input type='hidden' name='AliasMiddleNames' value='" + Middle + "'>" + Middle + "</td><td><input type='hidden' name='AliasLastNames' value='" + Last + "'>" + Last + "</td></tr>");

    //    $("#aliasTable").append(row);
