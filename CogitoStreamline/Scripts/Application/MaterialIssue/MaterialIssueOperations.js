﻿/// <reference path="MaterialIssueOperations.js" />
var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var gMaterials = "";
var VendorID = "";
var IssueNO = 0;
var stockval = 0
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Material Issue List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/MaterialIssueListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
          
            Pk_MaterialIssueID: {
                title: 'Issue ID',
                key: true
            },
            IssueDate: {
                title: 'Issue Date',
                     width:'5%'
            },
            Fk_OrderNo: {
                title: 'Order No',
                width: '5%'
            },
            CustName: {
                title: 'Cust.Name',
                width:'25%'
            },
            JobCardID: {
                title: 'JC-ID',
                width: '5%'
            },
            //////////////////////////////////////////////////
            Details: {
                title: 'Details',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (data) {
                    //Create an image that will be used to open child table
                    // var $img = $('<img class="child-opener-image" src="/images/redSignal.jpg" style="width:25px;height:25px" title="Author" />');
                    var $img = $("<i class='icon-users'></i>");
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#MainSearchContainer').jtable('openChildTable',
                                    $img.closest('tr'),
                                    {
                                        title: 'Material List',
                                        actions: {
                                            listAction: '/MatIssue/IssueGetRec?Pk_MaterialIssueID=' + data.record.Pk_MaterialIssueID

                                        },
                                        fields: {
                                            Pk_BoxID: {
                                                title: 'Pk_BoxID',
                                                list:false
                                            },
                                            MaterialName: {
                                                title: 'MaterialName'
                                            },
                                            Color: {
                                                title: 'Shade'
                                            },
                                            Mill: {
                                                title: 'Mill'
                                            },
                                            Quantity: {
                                                title: 'Quantity',
                                                key: true,
                                            },
                                            RollNo: {
                                                title: 'Reel No',
                                                key: true,
                                            },
                                            BoxDet: {
                                                title: 'Box',
                                                width: '2%',
                                                listClass: 'child-opener-image-column',
                                                display: function (data) {
                                                    var $img = $("<i class='icon-iphone' title='Box'></i>");
                                                    $img.click(function () {
                                                        //var BoxID = data.record.Fk_BoxID;
                                                        $('#MainSearchContainer').jtable('openChildTable',
                                                                    $img.closest('tr'),
                                                                    {
                                                                        title: 'Box Layer Details',
                                                                        paging: true,
                                                                        pageSize: 10,
                                                                        actions: {
                                                                            listAction: '/BoxMaster/BoxLayerDetails?Pk_BoxID=' + data.record.Pk_BoxID
                                                                        },
                                                                        fields: {
                                                                           
                                                                                                            Pk_LayerID: {
                                                                                                                title: 'Layer ID',
                                                                                                                key: true,
                                                                                                                create: false,
                                                                                                                edit: false,
                                                                                                                list: true
                                                                                                            },
                                                                                                            PName: {
                                                                                                                title: 'Part Name',
                                                                                                                key: false
                                                                                                            },
                                                                                                            //BF: {
                                                                                                            //    title: 'BF'

                                                                                                            //},
                                                                                                           MaterialName: {
                                                                                                                title: 'Mat.Name'
                                                                                                            },
                                                                                                    
                                                                                                        
                                                                        },
                                                                        formClosed: function (event, data) {
                                                                            data.form.validationEngine('hide');
                                                                            data.form.validationEngine('detach');
                                                                        }
                                                                    }, function (data) { //opened handler
                                                                        data.childTable.jtable('load');
                                                                    });
                                                    });
                                                    return $img;
                                                }
                                            },
                                        },
                                        formClosed: function (event, data) {
                                            data.form.validationEngine('hide');
                                            data.form.validationEngine('detach');
                                        }
                                    }, function (data) { //opened handler
                                        data.childTable.jtable('load');
                                    });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },

            ///////////////////////////////////////////////
            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        IssueNO = row.record.Pk_MaterialIssueID;
                        _comLayer.parameters.add("Pk_MaterialIssueID", IssueNO);
                        _comLayer.executeSyncAction("MatIssue/IssueRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Issue" + IssueNO + ".pdf"
                        ////////////////////////////


                        //var xhr = new XMLHttpRequest();
                        //var urlToFile = "ConvertPDF/MaterialIssue.pdf"
                        //xhr.open('HEAD', urlToFile, false);
                        //xhr.send();

                        //if (xhr.status == "404") {
                        //    alert('Data Not Available , File does not Exist');
                        //    return false;


                        //} else {
                            window.open(strval, '_blank ', 'width=960,height=576');
                        //    return true;
                        //}


                        /////////////////////////
                       
                    });
                    return button;
                }
            },
            ////////////////
            //IssueDev: {
            //    title: 'Dev.',
            //    width: '2%',
            //    //display: function (data) {
            //    //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
            //    display: function (row) {
            //        var button = $("<i class='icon-printer'></i>");
            //        $(button).click(function () {

            //            IssueNO = row.record.Pk_MaterialIssueID;
            //            _comLayer.parameters.add("Pk_MaterialIssueID", IssueNO);
            //            _comLayer.executeSyncAction("MatIssue/IssueDevRep", _comLayer.parameters);
            //            var strval = "ConvertPDF/IssueDev" + IssueNO + ".pdf"
            //            //window.open(strval, '_blank ', 'width=960,height=576');
                       
            //            /////////////////////////

            //            _comLayer.parameters.add("Pk_MaterialIssueID", IssueNO);
            //            _comLayer.executeSyncAction("MatIssue/IssueDevRepForIssue", _comLayer.parameters);

            //            var strval1 = "ConvertPDF/IssueDevIss" + ".pdf"
            //            //window.open(strval1, '_blank ', 'width=960,height=576');



            //            var xhr1 = new XMLHttpRequest();
            //            var urlToFile = strval;
            //            //var urlToFile = "ConvertPDF/JCRep" + JCNO + ".pdf"
            //            xhr1.open('HEAD', urlToFile, false);
            //            xhr1.send();

            //            if (xhr1.status == "404") {
            //                alert('Data Not Available , File does not Exist');
            //                //return false;


            //            } else {
            //                window.open(strval, '_blank ', 'width=900,height=250');
            //                // return true;
            //            }


            //            var xhr2 = new XMLHttpRequest();
            //            var urlToFile = strval1;
            //            //var urlToFile = "ConvertPDF/JCRep" + JCNO + ".pdf"
            //            xhr2.open('HEAD', urlToFile, false);
            //            xhr2.send();

            //            if (xhr2.status == "404") {
            //                alert('Data Not Available , File does not Exist');
            //                //return false;


            //            } else {
            //                window.open(strval1, '_blank ', 'width=900,height=250');
            //                // return true;
            //            }

            //        });
            //        return button;
            //    }
            //}
        }
    });

    $('#dtIssueDate').datepicker({
        autoclose: true
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_MaterialIssueID: $('#IssueId').val(),
            DCNo: $('#DCno').val(),
            IssueDate: $('#TxtIssueDate').val()

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Branch", "getBranch", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Branch", "getBranch", null);

    $('#TxtIssueDate').datepicker({ autoclose: true });
    //$('#TxtToDate').datepicker({ autoclose: true });

    //$("#searchDialog").width(1000);
    //$("#searchDialog").css("top", "50px");
    //$("#searchDialog").css("left", "300px");

    //$("#searchMaterialDialog").width(1000);
    //$("#searchMaterialDialog").css("top", "50px");
    //$("#searchMaterialDialog").css("left", "300px");

    $("#searchCustomerDialog").width(1000);
    $("#searchCustomerDialog").css("top", "50px");
    $("#searchCustomerDialog").css("left", "300px");

    setUpCustomerSearch();
    //setUpCustomerSearch();
}
function resetOneToManyForm(property) {
    //$("#dtIssueDate").val("");
    $("#txtMaterial").val("");
    //$("#txtCustomerOrder").val("");
    $("#Quantity").val("");
}
function afterNewShow(viewObject) {

    //bEditContext = false;


    //curViewModel = viewObject.viewModel;
    var dNow = new Date();


    document.getElementById('dtIssueDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $("#cmdCreate").attr('disabled', 'disabled');

    //document.getElementById('dtIssueDate').value = dNow.getDate() + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#dtIssueDate').datepicker({ autoclose: true });

    //curViewModel = viewObject.viewModel;

    $('#divMaterialDetail').jtable({
        title: 'Material Issue',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            txtFk_Material: {
                title: 'Material Name'

            },
            Color: {
                title: 'Shade',            

            },
            Mill: {
                title: 'Mill',

            },
            RollNo: {
                title: 'Reel No.',
                list: true

            },
            Pk_StockID: {
                title: 'Stkid',
                list:false

            },
            Quantity: {
                title: 'Quantity'

            }

        }
    });

    if (Number($('#Quantity').val()) > (Number(stockval))) {
         
        alert("Quantity cannot be Greater than StockValue, Stock Value:" + (Number(stockval)));
        $('#Quantity').val("");
        $("#Quantity").focus();
    }

    else {
        configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IssueDetails", "MatIssue", "NOTUSED", function () { return new MaterialIssue() }, "divCreateMaterialIssue");
    }

    $('#cmdCustomerOrderSearch').click(function (e) {
        e.preventDefault();

        $("#searchCustomerDialog").modal("show");
    });

    //$('#cmdJobCardMSearch').click(function (e) {
    //    e.preventDefault();
    //    setUpJobCardSearch(viewObject);


    //    $("#searchJobCardDialog").width(900);
    //    //$("#searchJobCardDialog").css("left", "80px");
    //    //$("#divSearchMaterial").width(800);
    //    $("#searchJobCardDialog").modal("show");
    //});

    $('#cmdJobCardPartSearch').click(function (e) {
        e.preventDefault();
        setUpJobCardPartSearch(viewObject);


        $("#searchJobCardDialog").width(900);
        //$("#searchJobCardDialog").css("left", "80px");
        //$("#divSearchMaterial").width(800);
        $("#searchJobCardDialog").modal("show");
    });

    $('#cmdMaterialIndent').click(function (e) {
        e.preventDefault();

        $("#cmdCreate").attr('disabled', false);



    });
    $('#cmdSaveAsJC').click(function (e) {
        e.preventDefault();

        var dtval= $('#dtIssueDate').val();
        var CustOrder = $('#Fk_OrderNo').val();
        var Fk_JobCardID = $('#Fk_JobCardID').val();

        _comLayer.parameters.add("IssueDate", dtval);
        _comLayer.parameters.add("Fk_OrderNo", CustOrder);
        _comLayer.parameters.add("Fk_JobCardID", Fk_JobCardID);
        //_comLayer.parameters.add("TransportPercentage", TransportPercentage);
      
        _comLayer.parameters.add("IssueDetails",1)
      
        var oResult = _comLayer.executeSyncAction("MatIssue/Save", _comLayer.parameters);
        //window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val() + "&Ply=" + $('#Ply').val());
        
    });

    
    $("#cmdShowRegular").click(function (e) {
        e.preventDefault();
        _util.displayView("MatIssue", "_AddMaterialIssue", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").height(700);
        $("#dataDialog").width(1000);
        $("#dataDialog").css("top", "50px");
        $("#dataDialog").css("left", "300px");

        $("#dataDialog").modal("show");
        setUpMaterialSearchRegular();

    });

}



function afterOneToManyDialogShow() {

    $('#dtIssueDate').datepicker({ autoclose: true });


    $("#cmdMaterialSearch").click(function (e) {
        e.preventDefault();
        _util.displayView("MatIssue", "_AddMaterialIssue", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").height(700);
        $("#dataDialog").width(1000);
        $("#dataDialog").css("top", "50px");
        $("#dataDialog").css("left", "300px");

        $("#dataDialog").modal("show");
        setUpMaterialSearch();



        $("#cmdShowRegular").click(function (e) {
            e.preventDefault();
            _util.displayView("MatIssue", "_AddMaterialIssue", "dataDialogArea");
            $('#dataDialogHeading').text("Search");
            $("#dataDialog").height(700);
            $("#dataDialog").width(1000);
            $("#dataDialog").css("top", "50px");
            $("#dataDialog").css("left", "300px");

            $("#dataDialog").modal("show");
            setUpMaterialSearchRegular();

        });

    });
   



}

function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }


    if ($("#dtIssueDate").val() == "") {
        $("#dtIssueDate").validationEngine('showPrompt', 'Enter Issue Date', 'error', true)
        bValidation = false;
    }

    if ($("#txtMaterial").val() == "") {
        $("#txtMaterial").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

//function showOnlyPending() {
//    $('#MainSearchContainer').jtable('load', {
//        CustomerName: $('#CustomerName').val(),
//        ProductName: $('#ProductName').val(),
//        OnlyPending: document.getElementById('chkonlyPending').checked
//    });
//}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#dtIssueDate').datepicker({
        autoclose: true
    });



    $('#divMaterialDetail').jtable({
        title: 'Material Issue',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            txtFk_Material: {
                title: 'Material Name'

            },
            Color: {
                title: 'Color',
                edit: false
            },
            Mill: {
                title: 'Mill',
                edit: false
            },
            RollNo: {
                title: 'Reel No.',
                list: true

            },
            Pk_StockID: {
                title: 'Stkid',
                list:false
            },
            Quantity: {
                title: 'Quantity'

            }
            //Comments: {
            //    title: 'Comments',
            //    width: '20%'
            //}
        }
    });


    var oSCuts = viewObject.viewModel.data.MaterialIssueDetails();
    viewObject.viewModel.data["IssueDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new MaterialIssue();
        oCut.load(oSCuts[i].Pk_MaterialIssueDetailsID, oSCuts[i].Pk_StockID, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].Quantity, oSCuts[i].RollNo, oSCuts[i].Color, oSCuts[i].Mill);
        viewObject.viewModel.data["IssueDetails"].push(oCut);
        i++;
    }

    //  configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "_AddMaterialIndent", function () { return new MaterialIndent() });
    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IssueDetails", "MatIssue", "NOTUSED", function () { return new MaterialIssue() }, "divCreateMaterialIssue");

    bEditContext = true;

    //$('#wfTransDisplay').wgWorkFlowHistory({
    //    wgTag: "Order",
    //    viewModel: viewObject.viewModel
    //});

    //$('#wfButtons').wgWorkFlowButtons({
    //    wgTag: "MaterialIndent",
    //    viewModel: viewObject.viewModel,
    //    stateField: "Fk_Status",
    //    belongsTo: "MaterialIndent",
    //    pkField: "Pk_MaterialOrderMasterId"
    //});
}

//function beforeOneToManyDataBind(property) {
//    if (bEditContext) {
//        _util.setDivPosition("divDeliveryStatus", "block");
//    }

//}

function setUpAssignedStock()
{

    JCID=document.getElementById('Fk_JobCardID').value;
    $('#tlbIssueMaterials').jtable({
        title: 'Paper Assigned List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/MatIssue/JCAssignedGetRec?Pk_JobCardID=' + JCID,
            //updateAction: ''
        },

        //Pk_PaperStock = p.Pk_PaperStock,
        //MaterialName = p.Name,
        //Quantity = p.Quantity,
        //RollNo = p.RollNo,
        //Pk_JobCardID = p.Pk_JobCardID,


        fields: {
            Pk_JobCardID: {
                title: 'JobCard',
                key: true,
            },
            Pk_Material: {
                title: 'Pk_Material',
              list:false,
            },
            MaterialName: {
                title: 'M.Name'
            },

            Quantity: {
                title: 'Quantity',
                key: true,
            },
            RollNo: {
                title: 'RollNo',
                key: true,
            },
            
            //Fk_Order: {
            //    title: 'Order',
            //    key: true,
            //},


        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();

        $('#tlbIssueMaterials').jtable('load', {
            Pk_JobCardID: JCID,


        });
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

}


//function beforeNewShow(viewObject) {

//    $('#txtCustomerOrder').wgReferenceField({
//        keyProperty: "Fk_OrderNo",
//        displayProperty: "OrderNo",
//        loadPath: "Order/Load",
//        viewModel: viewObject.viewModel
//    });

//}


function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();
    var rowCount = $('#MaterialSearchContainer tr').length;
    if (rowCount > 0) {
        $('#MaterialSearchContainer').jtable('destroy');

    }
   
 //   var PName = document.getElementById('PName').value;

  //  if (PName == 'OuterShell' || PName == '') {

        $("#srchMHeader").text("Material Search");

        //Adding Search fields
   
        var txtFieldGSM = "<input type='text' id='txtdlgGSM' placeholder='GSM' class='input-large search-query'/>&nbsp;&nbsp;";
        var txtFieldBF = "<input type='text' id='txtdlgBF' placeholder='BF' class='input-large search-query'/>&nbsp;&nbsp;";
        var txtFieldD = "<input type='text' id='txtdlgD' placeholder='Deckle' class='input-large search-query'/>&nbsp;&nbsp;";
        var txtFieldMill = "<input type='text' id='txtdlgMill' placeholder='Mill' class='input-large search-query'/>&nbsp;&nbsp;";
        var txtFieldReel = "<input type='text' id='txtdlgReel' placeholder='Reel No' class='input-large search-query'/>&nbsp;&nbsp;";
        //var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Others-Name' class='input-large search-query'/>&nbsp;&nbsp;";

        $("#dlgMaterialSearchFields").append(txtFieldGSM);
        $("#dlgMaterialSearchFields").append(txtFieldBF);
        $("#dlgMaterialSearchFields").append(txtFieldD);
        $("#dlgMaterialSearchFields").append(txtFieldMill);
        $("#dlgMaterialSearchFields").append(txtFieldReel);
        //$("#dlgMaterialSearchFields").append(txtFieldMaterialName);


        var JobID = document.getElementById('Fk_JobCardID').value;
        var StockList = document.getElementById('Product').value;

        if (StockList == "Sales") {
            $('#MaterialSearchContainer').jtable({
                title: 'Regular Stock List',
                paging: true,
                pageSize: 7,
                selecting: true, //Enable selecting
                multiselect: false, //Allow multiple selecting
                selectingCheckboxes: true,
                actions: {
                    listAction: '/PaperStockList/PaperStockListAge',
                    //deleteAction: ''
                },
                recordsLoaded: function (event, data) {
                    $('.jtable-data-row').click(function () {
                        var row_id = $(this).attr('data-record-key');
                        //$('#cmdMatDone').click();
                        AddToMaterialList();

                        $('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

                    });
                },
                fields: {
                    Pk_Material: {
                        title: 'Id',
                        key: true,
                        width: '2%'
                    },
                    Pk_PaperStock: {
                        title: 'Pk_PaperStock',
                      list:true,
                        width: '2%'
                    },
                    SName: {
                        title: 'Mill',
                        width: '5%'
                    },
                    RollNo: {
                        title: 'RollNo',
                        width: '5%'
                    },

                    MaterialName: {
                        title: 'MaterialName',
                        width: '15%'
                    },
                    GSM: {
                        title: 'GSM',
                        width: '5%'
                    },
                    BF: {
                        title: 'BF',
                        width: '2%'
                    },
                    Deckle: {
                        title: 'Deckle',
                        width: '2%'
                    },
                    //Mill: {
                    //    title: 'Mill',
                    //    width: '5%'
                    //},
                    Color: {
                        title: 'Color',
                        width: '5%'
                    },
                    Quantity: {
                        title: 'Stock Qty',
                        width: '5%'
                    },
                    Age: {
                        title: 'Age',
                        width: '5%'
                    },
                }
            });
        }
        else if (StockList == "Job Work") {

            $('#MaterialSearchContainer').jtable({
                title: 'JobWork Stock List',
                paging: true,
                pageSize: 7,
                selecting: true, //Enable selecting
                multiselect: false, //Allow multiple selecting
                selectingCheckboxes: true,
                actions: {
                    listAction: '/JWStockList/PaperStockListByFiter'
                },
                recordsLoaded: function (event, data) {
                    $('.jtable-data-row').click(function () {
                        var row_id = $(this).attr('data-record-key');
                        //$('#cmdMatDone').click();
                        AddToMaterialList();

                        $('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

                    });
                },
                fields: {
                    Pk_Material: {
                        title: 'Id',
                        key: true,
                        width: '2%'
                    },

                    RollNo: {
                        title: 'RollNo',
                        width: '5%'
                    },
                    Pk_PaperStock: {
                        title: 'Pk_PaperStock',
                        list: true,
                        width: '2%'
                    },
                    MaterialName: {
                        title: 'MaterialName',
                        width: '15%'
                    },
                    GSM: {
                        title: 'GSM',
                        width: '5%'
                    },
                    BF: {
                        title: 'BF',
                        width: '2%'
                    },
                    Deckle: {
                        title: 'Deckle',
                        width: '2%'
                    },
                    //Mill: {
                    //    title: 'Mill',
                    //    width: '5%'
                    //},
                    //Color: {
                    //    title: 'Color',
                    //    width: '5%'
                    //},
                    Quantity: {
                        title: 'Stock Qty',
                        width: '5%'
                    },
                    //Age: {
                    //    title: 'Age',
                    //    width: '5%'
                    //},
                }
            });
        }
        //else if (StockList == "Others") {

        //    $('#MaterialSearchContainer').jtable({
        //        title: 'Consumables Stock List',
        //        paging: true,
        //        pageSize: 7,
        //        selecting: true, //Enable selecting
        //        multiselect: false, //Allow multiple selecting
        //        selectingCheckboxes: true,
        //        actions: {
        //            listAction: '/MaterialInward/ConsumablesListByFiter'
        //        },
        //        recordsLoaded: function (event, data) {
        //            $('.jtable-data-row').click(function () {
        //                var row_id = $(this).attr('data-record-key');
        //                //$('#cmdMatDone').click();
        //                AddToMaterialList();

        //                $('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

        //            });
        //        },
        //        //MatName = p.MatName,
        //        //CatName = p.CatName,
        //        //Quantity = p.Quantity,
        //        //Fk_MaterialCategory = p.Fk_MaterialCategory,
        //        //Pk_Material = p.Pk_Material,
                 
        //        fields: {
        //            Pk_Material: {
        //                title: 'Id',
        //                key: true,
        //                width: '2%'
        //            },

        //            MatName: {
        //                title: 'MaterialName',
        //                width: '15%'
        //            },
        //            CatName: {
        //                title: 'Cat.Name',
        //                width: '5%'
        //            },
              
        //            Quantity: {
        //                title: 'Stock Qty',
        //                width: '5%'
        //            },
                  
        //        }
        //    });
        //}

        $('#LoadRecordsButton').click(function (e) {
            e.preventDefault();
            $('#MaterialSearchContainer').jtable('load', {
                //MaterialName: $('#txtdlgMaterialName').val(),
                GSM: $('#txtdlgGSM').val(),
                BF: $('#txtdlgBF').val(),
                Deckle: $('#txtdlgD').val(),
                RollNo: $('#txtdlgReel').val(),
                
              
            });
        });

        $('#LoadRecordsButton').click();

        $('#cmdMatSearch').click(function (e) {
            e.preventDefault();

            var Selval = document.getElementById('Selected').value;
            if (Selval == "JW")

            {
                $('#MaterialSearchContainer').jtable('load', {
                    //MaterialName: $('#txtdlgMaterialName').val(),
                    GSM: $('#txtdlgGSM').val(),
                    BF: $('#txtdlgBF').val(),
                    Deckle: $('#txtdlgD').val(),
                    RollNo: $('#txtdlgReel').val(),

                });
            }
            else if (Selval == "Sales") {

                $('#MaterialSearchContainer').jtable('load', {
                    //MaterialName: $('#txtdlgMaterialName').val(),
                    GSM: $('#txtdlgGSM').val(),
                    BF: $('#txtdlgBF').val(),
                    Deckle: $('#txtdlgD').val(),
                    RollNo: $('#txtdlgReel').val(),

                });
            }
            else  {

                $('#MaterialSearchContainer').jtable('load', {
                    MatName: $('#txtdlgMaterialName').val(),
                    GSM: $('#txtdlgGSM').val(),
                    BF: $('#txtdlgBF').val(),
                    Deckle: $('#txtdlgD').val(),
                    RollNo: $('#txtdlgReel').val(),

                });
            }
        });



        $('#cmdMatDone').click(function (e) {
            e.preventDefault();
            var rows = $('#MaterialSearchContainer').jtable('selectedRows');


            $("#dataDialog").modal("hide");
            if (rows.length > 0) {
                $('#txtMaterial').wgReferenceField("setData", rows[0].keyValue);
                document.getElementById('Quantity').value = rows[0].data.Quantity;
                document.getElementById('Pk_StockID').value = rows[0].data.Pk_PaperStock;
                document.getElementById('RollNo').value = rows[0].data.RollNo;
                document.getElementById('SName').value = rows[0].data.SName;
                //document.getElementById('Fk_Mill').value = rows[0].data.Fk_Mill;
                document.getElementById('Shade').value = rows[0].data.Color;
            }
            stockval = document.getElementById('Quantity').value;

            //$("#Quantity").focus();

            var rows = "";
            this.paperRecList = [];
            //$('#listMain tbody').remove();
            paperRecList = getTableData($('#list'));

            var arrayLength = paperRecList.length;

            for (var i = 0; i < arrayLength; i++) {

                //check if row for material id is already added, if so then the addition of row is skipped 
                numRows = $("#listMain tr").length;
                var rowexists = false;
                for (var j = 1 ; j < numRows ; j++) {
                    var Fk_PaperStock = $("#listMain tr:nth-child(" + j + ") td:nth-child(3)").html();
                    //var qty = $("#listMain  tr:nth-child(" + j + ") td:nth-child(5)").html();
                    if (Fk_PaperStock == paperRecList[i][2]) {
                        //$("tr:nth-child(" + j + ") td:nth-child(4)").html(parseInt(qty) + paperRecList[i][4]);
                        rowexists = true;
                        break;
                    }
                }

                if (!rowexists) {
                    rows += "<tr><td>" + paperRecList[i][0]
                        + "</td><td style='display:none;'>" + paperRecList[i][1]
                        + "</td><td style='display:none;'>" + paperRecList[i][2]
                        + "</td><td >" + paperRecList[i][3]
                        //+ "</td><td class = 'count-me'>" + paperRecList[i][4]
                        + "</td><td >" + paperRecList[i][4]
                        + "</td><td><input type='button' value='Delete' onclick='deleteRowM(this)'> </td></tr>"
                }
            }

            $(rows).appendTo("#listMain tbody");
            var btnSaveMaterial = document.getElementById("cmdMaterialIndent");
            // Programmatically click the save button
            btnSaveMaterial.click();

            makeSumryFromColumn();
        });
    }


function setUpMaterialSearchRegular() {
    //Branch

    cleanSearchDialog();


    //   var PName = document.getElementById('PName').value;

    //  if (PName == 'OuterShell' || PName == '') {

    $("#srchMHeader").text("Material Search");

    //Adding Search fields
    // var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldGSM = "<input type='text' id='txtdlgGSM' placeholder='GSM' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldBF = "<input type='text' id='txtdlgBF' placeholder='BF' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldD = "<input type='text' id='txtdlgD' placeholder='Deckle' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldMill = "<input type='text' id='txtdlgMill' placeholder='Mill' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldReel = "<input type='text' id='txtdlgReel' placeholder='Reel No' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldGSM);
    $("#dlgMaterialSearchFields").append(txtFieldBF);
    $("#dlgMaterialSearchFields").append(txtFieldD);
    $("#dlgMaterialSearchFields").append(txtFieldMill);
    $("#dlgMaterialSearchFields").append(txtFieldReel);

    var JobID = document.getElementById('Fk_JobCardID').value;
    var StockList = document.getElementById('Product').value;

  
        $('#MaterialSearchContainer').jtable({
            title: 'Material List',
            paging: true,
            pageSize: 7,
            selecting: true, //Enable selecting
            multiselect: false, //Allow multiple selecting
            selectingCheckboxes: true,
            actions: {
                listAction: '/PaperStockList/PaperStockListAge'
            },
            recordsLoaded: function (event, data) {
                $('.jtable-data-row').click(function () {
                    var row_id = $(this).attr('data-record-key');
                    //$('#cmdMatDone').click();
                    AddToMaterialList();

                    $('#MaterialSearchContainer').find(".jtable tbody tr:eq(" + $(this).index() + ")").css({ "background": "LightPink" });

                });
            },
            fields: {
                Pk_Material: {
                    title: 'Id',
                    key: true,
                    width: '2%'
                },
                Pk_PaperStock: {
                    title: 'Pk_PaperStock',
                    list: false,
                    width: '2%'
                },
                SName: {
                    title: 'Mill',
                    width: '5%'
                },
                RollNo: {
                    title: 'RollNo',
                    width: '5%'
                },

                MaterialName: {
                    title: 'MaterialName',
                    width: '15%'
                },
                GSM: {
                    title: 'GSM',
                    width: '5%'
                },
                BF: {
                    title: 'BF',
                    width: '2%'
                },
                Deckle: {
                    title: 'Deckle',
                    width: '2%'
                },
                //Mill: {
                //    title: 'Mill',
                //    width: '5%'
                //},
                Color: {
                    title: 'Color',
                    width: '5%'
                },
                Quantity: {
                    title: 'Stock Qty',
                    width: '5%'
                },
                Age: {
                    title: 'Age',
                    width: '5%'
                },
            }
        });
     
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val(),
            GSM: $('#txtdlgGSM').val(),
            BF: $('#txtdlgBF').val(),
            Deckle: $('#txtdlgD').val(),
            RollNo: $('#txtdlgReel').val(),


        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val(),
            GSM: $('#txtdlgGSM').val(),
            BF: $('#txtdlgBF').val(),
            Deckle: $('#txtdlgD').val(),
            RollNo: $('#txtdlgReel').val(),

        });
    });



    $('#cmdMatDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');


        $("#dataDialog").modal("hide");
        $('#txtMaterial').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Quantity').value = rows[0].data.Quantity;
        document.getElementById('Pk_StockID').value = rows[0].data.Pk_PaperStock;
        document.getElementById('RollNo').value = rows[0].data.RollNo;
        document.getElementById('SName').value = rows[0].data.SName;
        //document.getElementById('Fk_Mill').value = rows[0].data.Fk_Mill;
        document.getElementById('Shade').value = rows[0].data.Color;

        stockval = document.getElementById('Quantity').value;

        //$("#Quantity").focus();

        var rows = "";
        this.paperRecList = [];
        //$('#listMain tbody').remove();
        paperRecList = getTableData($('#list'));

        var arrayLength = paperRecList.length;

        for (var i = 0; i < arrayLength; i++) {

            //check if row for material id is already added, if so then the addition of row is skipped 
            numRows = $("#listMain tr").length;
            var rowexists = false;
            for (var j = 1 ; j < numRows ; j++) {
                var Fk_PaperStock = $("#listMain tr:nth-child(" + j + ") td:nth-child(3)").html();
                //var qty = $("#listMain  tr:nth-child(" + j + ") td:nth-child(5)").html();
                if (Fk_PaperStock == paperRecList[i][2]) {
                    //$("tr:nth-child(" + j + ") td:nth-child(4)").html(parseInt(qty) + paperRecList[i][4]);
                    rowexists = true;
                    break;
                }
            }

            if (!rowexists) {
                rows += "<tr><td>" + paperRecList[i][0]
                    + "</td><td style='display:none;'>" + paperRecList[i][1]
                    + "</td><td style='display:none;'>" + paperRecList[i][2]
                    + "</td><td >" + paperRecList[i][3]
                    //+ "</td><td class = 'count-me'>" + paperRecList[i][4]
                    + "</td><td >" + paperRecList[i][4]
                    + "</td><td><input type='button' value='Delete' onclick='deleteRowM(this)'> </td></tr>"
            }
        }

        $(rows).appendTo("#listMain tbody");
        var btnSaveMaterial = document.getElementById("cmdMaterialIndent");
        // Programmatically click the save button
        btnSaveMaterial.click();

        makeSumryFromColumn();
    });
}



function makeSumryFromColumn() {
    var arr = [];
    var qty = [];
    var arrQty = [];
    //$("tr td:nth-child(1)").addClass('date');
    //$("#ItemsTable tr").each(function(){
    $("#listMain tr td:nth-child(1)").each(function () {
        var indx = arr.indexOf($(this).text());
        var qty;
        var str;
        //if ($.inArray($(this).text(), arr) == -1) {
        if (indx == -1) {
            arr.push($(this).text());
            str = $(this).parent().find("td:nth-child(5)").text(); //$(this).next().text();
            qty = isNaN(str) ? 0 : parseInt(str);
            arrQty.push(qty);
        }
        else {
            str = $(this).parent().find("td:nth-child(5)").text(); //$(this).next().text();
            qty = isNaN(str) ? 0 : parseInt(str);
            arrQty[indx] += qty;
        }
    });

    $("#listSumry > tbody").html("");

    var rows = "";
    for (var i = 0; i < arr.length; i++) {
        rows += "<tr><td>" + arr[i] + "</td>" + "<td>" + arrQty[i] + "</td></tr>";
    }

    $(rows).appendTo("#listSumry tbody");

};

function deleteRow(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("list").deleteRow(i);
};

function deleteRowM(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("listMain").deleteRow(i);
    makeSumryFromColumn();
};


function getTableData(table) {
    var data = [];
    table.find('tr:gt(0)').each(function (rowIndex, r) {
        var cols = [];
        $(this).find('td').each(function (colIndex, c) {
            cols.push(c.textContent);
        });
        data.push(cols);
    });
    return data;
};

function AddToMaterialList() {

    var rows = "";

    var selPaprows = $('#MaterialSearchContainer').jtable('selectedRows');

    if (selPaprows.length > 0) {

        rows += "<tr><td>" + selPaprows[0].data.MaterialName
            + "</td><td style='display:none;'>" + selPaprows[0].data["Pk_Material"]
            + "</td><td style='display:none;'>" + selPaprows[0].data["Pk_PaperStock"]
            + "</td><td>" + selPaprows[0].data["RollNo"]
            + "</td><td>" + selPaprows[0].data["Quantity"]
            + "</td><td>" + "<td><input type='button' value='Delete' onclick='deleteRow(this)'></td>"

        $(rows).appendTo("#list tbody");

    }

};

function setUpCustomerSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchCHeader").text("Customer Order Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";
    var txtFieldJC = "<input type='text' id='txtdlgJC' placeholder='JC' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgCustomerSearchFields").append(txtFieldCustomerName);
    $("#dlgCustomerSearchFields").append(txtFieldJC);

    $('#CustSearchContainer').jtable({
        title: 'Customer Order List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/DelSchedule/IssueScheduleList'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdCustDone').click();
            });
        },
        fields: {
            OrderNo: {
                title: 'Ord.No.',
                key: true
            },

            CustPO: {
                title: 'Cust_PONo.'
            },
            EnquiryNo: {
                title: 'Enq.No',
                edit: false
            },
            CustomerName: {
                title: 'C.Name'
            },
            BoxName: {
                title: 'B.Name'
            },
            PName: {
                title: 'PartName'
            },
            OrdQty: {
                title: 'OrdQty.'
            },
            Pk_JobCardID: {
                title: 'JC.No.'
            },
            Pk_DelID: {
                title: 'Prdn.Sch.No'
            },
            DeliveryDate: {
                title: 'Del.Date'
            },

            Quantity: {
                title: 'Prdn.Sch.Qty'
            },
            PartID: {
                title: 'Partid',
                list: false
            },
            Product: {
                title: 'Product',
                list: false
            },
        }
      
   
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#CustSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            JCNO: $('#txtdlgJC').val(),
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdCustSearch').click(function (e) {
        e.preventDefault();

        $('#CustSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val(),
            JCNO: $('#txtdlgJC').val(),
        });
    });

    $('#cmdCustDone').click(function (e) {
        e.preventDefault();
        var rows = $('#CustSearchContainer').jtable('selectedRows');

        if (rows.length>0)
            {
            $("#searchCustomerDialog").modal("hide");
            // $('#txtCustomerOrder').wgReferenceField("setData", rows[0].keyValue);
            document.getElementById('Fk_OrderNo').value = rows[0].keyValue;
            document.getElementById('PName').value = rows[0].data.PName;
            document.getElementById('Fk_JobCardID').value = rows[0].data.Pk_JobCardID;
            document.getElementById('Product').value = rows[0].data.Product;

            setUpAssignedStock();
            
    }
        $("#Fk_JobCardID").focus();
    });




}

function setUpJobCardSearch(viewObject) {
    //Branch

    cleanSearchDialog();

    $("#srchCHeader").text("JobCard Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgJobCard' placeholder='JobCardNo.' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgJobCardSearchFields").append(txtFieldCustomerName);

    var OrdId = document.getElementById('Fk_OrderNo').value;

    $('#JobCardSearchContainer').jtable({
        title: 'JobCard List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/JobCard/JobListByFiter?Fk_Order=' + OrdId
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdJobCardDone').click();
                $("#txtDcNo").focus();
            });
        },
        fields: {
            Pk_JobCardID: {
                title: 'CardID',
                key: true,
                width: '2%'
            },
            JDate: {
                title: 'J.Card Date',
                width: '3%'
            },
            Fk_Order: {
                title: 'Cust_Order No',
                width: '2%'
            },
            BoxName: {
                title: 'BoxName',
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#JobCardSearchContainer').jtable('load', {
            Pk_JobCardID: $('#txtdlgJobCard').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdJobCardSearch').click(function (e) {
        e.preventDefault();

        $('#JobCardSearchContainer').jtable('load', {
            Pk_JobCardID: $('#txtdlgJobCard').val()
        });
    });

    $('#cmdJobCardDone').click(function (e) {
        e.preventDefault();
        var rows = $('#JobCardSearchContainer').jtable('selectedRows');
        
        if (rows.length > 0) {
            document.getElementById('Fk_JobCardID').value = rows[0].data.Pk_JobCardID;
        }
        //$('#txtFk_JobCardID').val(rows[0].data.Fk_JobCardID);
        $("#searchJobCardDialog").modal("hide");
        $("#txtDcNo").focus();
    });




}


function setUpJobCardPartSearch(viewObject) {
    //Branch

    cleanSearchDialog();

    $("#srchCHeader").text("JobCard Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgJobCard' placeholder='JobCardNo.' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgJobCardSearchFields").append(txtFieldCustomerName);

    var OrdId = document.getElementById('Fk_OrderNo').value;

    $('#JobCardSearchContainer').jtable({
        title: 'JobCard-Parts List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/JobCardParts/JCPartsList?Fk_Order=' + OrdId
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdJobCardDone').click();
                $("#txtDcNo").focus();
            });
        },
        fields: {
            Pk_JobCardPartsID: {
                title: 'CardID',
                key: true,
                width: '2%'
            },
            JDate: {
                title: 'J.Card Date',
                width: '3%'
            },
            Fk_Order: {
                title: 'Cust_Order No',
                width: '2%'
            },
            BoxName: {
                title: 'BoxName',
                width: '5%'
            },
            PartName: {
        title: 'Part Name',
        width: '5%'
    }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#JobCardSearchContainer').jtable('load', {
            Pk_JobCardPartsID: $('#txtdlgJobCard').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdJobCardSearch').click(function (e) {
        e.preventDefault();

        $('#JobCardSearchContainer').jtable('load', {
            Pk_JobCardPartsID: $('#txtdlgJobCard').val()
        });
    });

    $('#cmdJobCardDone').click(function (e) {
        e.preventDefault();
        var rows = $('#JobCardSearchContainer').jtable('selectedRows');
        //document.getElementById('Quantity').value = rows[0].data.Quantity;
        document.getElementById('Fk_JobCardID').value = rows[0].data.Pk_JobCardPartsID;
        //$('#txtFk_JobCardID').val(rows[0].data.Fk_JobCardID);
        document.getElementById('PName').value = rows[0].data.PartName,
        $("#searchJobCardDialog").modal("hide");
        $("#txtDcNo").focus();
    });




}


function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["IssueDate"] = $('#dtIssueDate').val();
    viewModel.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
    viewModel.data["Fk_JobCardID"] = $('#Fk_JobCardID').val();

    this.paperRecList = [];
    paperRecList = getTableData($('#listMain'));

    var arrayLength = paperRecList.length;

    viewModel.data["IssueDetails"]().length = 0;

    for (var i = 0; i < arrayLength; i++) {

        //this.paperRec = JSON.parse(JSON.stringify(viewModel.data["IssueDetails"]()[0]));
        this.paperRec = new MaterialIssue();

        this.paperRec.data.Name = paperRecList[i][0];
        this.paperRec.data.Fk_Material = paperRecList[i][1];
        //this.paperRec.data.Fk_PaperStock = paperRecList[i][2];
        this.paperRec.data.Pk_StockID = paperRecList[i][2];
        this.paperRec.data.RollNo = paperRecList[i][3];
        this.paperRec.data.Quantity = paperRecList[i][4];

        viewModel.data["IssueDetails"]().push(this.paperRec);

    }

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["IssueDate"] = $('#dtIssueDate').val();
    viewModel1.data["Fk_OrderNo"] = $('#Fk_OrderNo').val();
    viewModel1.data["Fk_JobCardID"] = $('#Fk_JobCardID').val();
}


function ReferenceFieldNotInitilized(viewModel) {

    $('#txtMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });
    //$('#txtCustomerOrder').wgReferenceField({
    //    keyProperty: "Pk_Order",
    //    displayProperty: "Pk_Order",
    //    loadPath: "Order/Load",
    //    viewModel: viewModel
    //});
    //$('#txtFk_JobCardID').wgReferenceField({
    //    keyProperty: "Pk_JobCardID",
    //    displayProperty: "Pk_JobCardID",
    //    loadPath: "JobCard/Load",
    //    viewModel: viewModel
    //});
    if (viewModel.data != null) {
        $('#txtMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        //$('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_CustomerOrder"]);
        //$('#txtFk_VendorId').wgReferenceField("setData", viewModel.data["Fk_VendorId"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
 //   $('#MaterialSearchContainer').empty();
    $('#dlgMaterialSearchFields').empty();
    //$('#JobCardSearchContainer').empty();
    //$('#MaterialSearchContainer').append("<div id='SearchContainer'></div>");
    //$("#dlgSearchFields").empty();
    //$('#cmdSearch').off();
    //$('#cmdDone').off();
}

function showOnlyBoard() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Board').val()
    });



}

function showOnlyGum() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Gum').val()
    });



}

function showOnlyInk() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Ink').val()
    });



}

function showOnlyPaper() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Paper').val()
    });



}


function showOnlyHoneycomb() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Honeycomb').val()
    });


}

function QtyCheck() {

    if (Number($('#Quantity').val()) > (Number(stockval))) {

        alert("Quantity cannot be Greater than StockValue, Stock Value:" + (Number(stockval)));
        $('#Quantity').val("");
        $("#Quantity").focus();
    }
}

function showJW() {
    document.getElementById('Product').value = "Job Work"
    document.getElementById("JW").checked = true;
    document.getElementById("Regular").checked = false;
    document.getElementById("Others").checked = false;

    document.getElementById('Selected').value = "JW";
    setUpMaterialSearch();
   
}
function showOnlyRegular() {
    document.getElementById('Product').value = "Sales"

    document.getElementById("Regular").checked = true;
    document.getElementById("JW").checked = false;
    document.getElementById("Others").checked = false;
    document.getElementById('Selected').value = "Regular";
   

    setUpMaterialSearch();

}
function showOthers() {
    document.getElementById('Product').value = "Others"
    document.getElementById("JW").checked = false;
    document.getElementById("Regular").checked = false;
    document.getElementById("Others").checked = true;
    document.getElementById('Selected').value = "Others";
    setUpMaterialSearch();

}