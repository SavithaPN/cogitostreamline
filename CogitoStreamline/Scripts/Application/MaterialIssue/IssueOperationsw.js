﻿var curViewModel = null;
var bFromSave = false;
var bEditContext = false;
var gMaterials = "";
var VendorID = "";
var IssueNO = 0;
var stockval = 0
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Material Issue List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/MaterialIssueListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {

            Pk_MaterialIssueID: {
                title: 'Issue ID',
                key: true
            },
            IssueDate: {
                title: 'Issue Date'
            },
            Fk_OrderNo: {
                title: 'Order No'
            },
            CustName: {
                title: 'Cust.Name'
            },
            JobCardID: {
                title: 'JobCardID'
            },
            //DCNo: {
            //    title: 'Issue No.',
            //},

            Print: {
                title: 'Print',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        IssueNO = row.record.Pk_MaterialIssueID;
                        _comLayer.parameters.add("Pk_MaterialIssueID", IssueNO);
                        _comLayer.executeSyncAction("MatIssue/IssueRep", _comLayer.parameters);
                        var strval = "ConvertPDF/Issue" + IssueNO + ".pdf"
                        ////////////////////////////


                        //var xhr = new XMLHttpRequest();
                        //var urlToFile = "ConvertPDF/MaterialIssue.pdf"
                        //xhr.open('HEAD', urlToFile, false);
                        //xhr.send();

                        //if (xhr.status == "404") {
                        //    alert('Data Not Available , File does not Exist');
                        //    return false;


                        //} else {
                        window.open(strval, '_blank ', 'width=960,height=576');
                        //    return true;
                        //}


                        /////////////////////////

                    });
                    return button;
                }
            }


        }
    });

    $('#dtIssueDate').datepicker({
        autoclose: true
    });

    //Re-load records when Order click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_MaterialIssueID: $('#IssueId').val(),
            DCNo: $('#DCno').val(),
            IssueDate: $('#TxtIssueDate').val()

        });

    });

    //Load all records when page is first shown
    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Branch", "getBranch", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Branch", "getBranch", null);

    $('#TxtIssueDate').datepicker({ autoclose: true });
    //$('#TxtToDate').datepicker({ autoclose: true });

    //$("#searchDialog").width(1000);
    //$("#searchDialog").css("top", "50px");
    //$("#searchDialog").css("left", "300px");

    //$("#searchMaterialDialog").width(1000);
    //$("#searchMaterialDialog").css("top", "50px");
    //$("#searchMaterialDialog").css("left", "300px");

    $("#searchCustomerDialog1").width(1000);
    $("#searchCustomerDialog1").css("top", "50px");
    $("#searchCustomerDialog1").css("left", "300px");

    setUpCustomerSearch();
    //setUpCustomerSearch();

    $('#cmdCreate').hide();
}
function resetOneToManyForm(property) {
    //$("#dtIssueDate").val("");
    $("#txtMaterial").val("");
    //$("#txtCustomerOrder").val("");
    $("#Quantity").val("");
}
function afterNewShow(viewObject) {

    //bEditContext = false;


    //curViewModel = viewObject.viewModel;
    var dNow = new Date();


    document.getElementById('dtIssueDate').value = (((dNow.getDate()) < 10) ? "0" + (dNow.getDate()) : (dNow.getDate())) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    //$("#cmdCreate").attr('disabled', 'disabled');

    //document.getElementById('dtIssueDate').value = dNow.getDate() + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();
    $('#dtIssueDate').datepicker({ autoclose: true });

    //curViewModel = viewObject.viewModel;

    $('#divMaterialDetail').jtable({
        title: 'Material Issue',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            txtFk_Material: {
                title: 'Material Name'

            },
            Color: {
                title: 'Shade'

            },
            RollNo: {
                title: 'RollNo',
                list: true

            },
            Pk_StockID: {
                title: 'Stkid',
                list: false

            },
            Quantity: {
                title: 'Quantity'

            }

        }
    });

    if (Number($('#Quantity').val()) > (Number(stockval))) {

        alert("Quantity cannot be Greater than StockValue, Stock Value:" + (Number(stockval)));
        $('#Quantity').val("");
        $("#Quantity").focus();
    }

    else {
        configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IssueDetails", "MatIssue", "NOTUSED", function () { return new MaterialIssue() }, "divCreateMaterialIssue");
    }

    $('#cmdCustomerOrderSearch1').click(function (e) {
        e.preventDefault();

        $("#searchCustomerDialog1").modal("show");
    });

    $('#cmdJobCardSearch1').click(function (e) {
        e.preventDefault();
        setUpJobCardSearch(viewObject);


        $("#searchJobCardDialog1").width(900);
        //$("#searchJobCardDialog1").css("left", "80px");
        //$("#divSearchMaterial").width(800);
        $("#searchJobCardDialog1").modal("show");
    });

    $('#cmdMaterialIndent').click(function (e) {
        e.preventDefault();


        //$('#cmdCreate').show();
       



    });

   $("#cmdCreate").attr('disabled', false);

}

function afterOneToManyDialogShow() {

    $('#dtIssueDate').datepicker({ autoclose: true });


    $("#cmdMaterialSearch").click(function (e) {
        e.preventDefault();
        _util.displayView("MatIssue", "_AddMaterialIssue", "dataDialogArea");
        $('#dataDialogHeading').text("Search");
        $("#dataDialog").height(700);

        $("#dataDialog").css("top", "50px");
        $("#dataDialog").css("left", "300px");

        $("#dataDialog").modal("show");
        setUpMaterialSearch();


    });




}

function beforeOneToManySaveHook(objectContext) {
    var bValidation = true;

    if ($("#Quantity").val() == "") {
        $("#Quantity").validationEngine('showPrompt', 'Enter Quantity', 'error', true)
        bValidation = false;
    }
    else if (!$.isNumeric($("#Quantity").val())) {
        $("#Quantity").validationEngine('showPrompt', 'Enter Numbers', 'error', true)
        bValidation = false;
    }


    if ($("#dtIssueDate").val() == "") {
        $("#dtIssueDate").validationEngine('showPrompt', 'Enter Issue Date', 'error', true)
        bValidation = false;
    }

    if ($("#txtMaterial").val() == "") {
        $("#txtMaterial").validationEngine('showPrompt', 'Select Material', 'error', true)
        bValidation = false;
    }

    return bValidation;


}

//function showOnlyPending() {
//    $('#MainSearchContainer').jtable('load', {
//        CustomerName: $('#CustomerName').val(),
//        ProductName: $('#ProductName').val(),
//        OnlyPending: document.getElementById('chkonlyPending').checked
//    });
//}

function afterEditShow(viewObject) {

    curViewModel = viewObject.viewModel;



    $('#dtIssueDate').datepicker({
        autoclose: true
    });



    $('#divMaterialDetail').jtable({
        title: 'Material Issue',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MatIssue/Bounce',
            updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },

            Fk_Material: {
                title: 'Material',
                list: false
            },
            txtFk_Material: {
                title: 'Material Name'

            },
            RollNo: {
                title: 'RollNo',
                list: true

            },
            Pk_StockID: {
                title: 'Stkid',
                list: false
            },
            Quantity: {
                title: 'Quantity'

            }
            //Comments: {
            //    title: 'Comments',
            //    width: '20%'
            //}
        }
    });


    var oSCuts = viewObject.viewModel.data.MaterialIssueDetails();
    viewObject.viewModel.data["IssueDetails"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new MaterialIssue();
        oCut.load(oSCuts[i].Pk_MaterialIssueDetailsID, oSCuts[i].Pk_StockID, oSCuts[i].Fk_Material, oSCuts[i].txtFk_Material, oSCuts[i].Quantity, oSCuts[i].RollNo);
        viewObject.viewModel.data["IssueDetails"].push(oCut);
        i++;
    }

    //  configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IndentDetails", "MaterialIndent", "_AddMaterialIndent", function () { return new MaterialIndent() });
    configureOne2Many("#cmdAddMaterial", '#divMaterialDetail', "#cmdMaterialIndent", viewObject, "IssueDetails", "MatIssue", "NOTUSED", function () { return new MaterialIssue() }, "divCreateMaterialIssue");

    bEditContext = true;

    //$('#wfTransDisplay').wgWorkFlowHistory({
    //    wgTag: "Order",
    //    viewModel: viewObject.viewModel
    //});

    //$('#wfButtons').wgWorkFlowButtons({
    //    wgTag: "MaterialIndent",
    //    viewModel: viewObject.viewModel,
    //    stateField: "Fk_Status",
    //    belongsTo: "MaterialIndent",
    //    pkField: "Pk_MaterialOrderMasterId"
    //});
}

//function beforeOneToManyDataBind(property) {
//    if (bEditContext) {
//        _util.setDivPosition("divDeliveryStatus", "block");
//    }

//}

function beforeNewShow(viewObject) {

    $('#txtCustomerOrder').wgReferenceField({
        keyProperty: "Fk_OrderNo",
        displayProperty: "OrderNo",
        loadPath: "Order/Load",
        viewModel: viewObject.viewModel
    });

}


function setUpMaterialSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchMHeader").text("Material Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Material Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldMaterialName);
    var JobID = document.getElementById('Fk_JobCardID').value;
    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/MaterialCategory/MIssueListByFiter?JobCardID=' + JobID
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdMatDone').click();
            });
        },
        fields: {
            Pk_Material: {
                title: 'Material Id',
                key: true
            },
            Fk_Mill: {
                title: 'FkMill',
                list: false
            },
            SName: {
                title: 'Mill Name',
                edit: false
            },
            Name: {
                title: 'Material Name',
                edit: false
            },
            Color: {
                title: 'Shade',
                edit: false
            },
            RollNo: {
                title: 'RollNo',
                edit: false,
                list: true
            },
            Pk_PaperStock: {
                title: 'PaperStockID',
                edit: false,
                list: true
            },
            Quantity:
            {
                title: 'Quantity',

            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            MaterialName: $('#txtdlgMaterialName').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#cmdMatSearch').click(function (e) {
        e.preventDefault();
        $('#MaterialSearchContainer').jtable('load', {
            Name: $('#txtdlgMaterialName').val()
        });
    });



    $('#cmdMatDone').click(function (e) {
        e.preventDefault();
        var rows = $('#MaterialSearchContainer').jtable('selectedRows');


        $("#dataDialog").modal("hide");
        $('#txtMaterial').wgReferenceField("setData", rows[0].keyValue);
        document.getElementById('Quantity').value = rows[0].data.Quantity;
        document.getElementById('Pk_StockID').value = rows[0].data.Pk_PaperStock;
        document.getElementById('RollNo').value = rows[0].data.RollNo;
        document.getElementById('SName').value = rows[0].data.SName;
        document.getElementById('Fk_Mill').value = rows[0].data.Fk_Mill;
        document.getElementById('Shade').value = rows[0].data.Color;


        stockval = document.getElementById('Quantity').value;

        $("#Quantity").focus();
    });
}
function setUpCustomerSearch() {
    //Branch

    //cleanSearchDialog();

    $("#srchCHeader").text("Customer Order Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgCustomerSearchFields1").append(txtFieldCustomerName);

    $('#CustSearchContainer1').jtable({
        title: 'Customer Order List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
      
        actions: {
            listAction: '/Order/OrderListByFiter'
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdCustDone1').click();
            });
        },
        fields: {
            Pk_Order: {
                title: 'Order No',
                key: true
            },
            OrderDate: {
                title: 'P.Order Date'
            },
            CustomerName: {
                title: 'CustomerName'
            },
            //ProductName: {
            //    title: 'ProductName',
            //    edit: false
            //},
            //Quantity: {
            //    title: 'Quantity'
            //},
            //Price: {
            //    title: 'Price'
            //},
            Status: {
                title: 'Status'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#CustSearchContainer1').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdCustSearch1').click(function (e) {
        e.preventDefault();

        $('#CustSearchContainer1').jtable('load', {
            CustomerName: $('#txtdlgCustomerName').val()
        });
    });

    $('#cmdCustDone1').click(function (e) {
        e.preventDefault();
        var rows = $('#CustSearchContainer1').jtable('selectedRows');

        $("#searchCustomerDialog1").modal("hide");
        $('#txtCustomerOrder').wgReferenceField("setData", rows[0].keyValue);
        $("#Fk_JobCardID").focus();
    });




}

function setUpJobCardSearch(viewObject) {
    //Branch

    cleanSearchDialog();

    $("#srchCHeader").text("JobCard Search");

    //Adding Search fields
    var txtFieldCustomerName = "<input type='text' id='txtdlgJobCard' placeholder='JobCardNo.' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgJobCardSearchFields1").append(txtFieldCustomerName);

    var OrdId = document.getElementById('txtCustomerOrder').value;

    $('#JobCardSearchContainer1').jtable({
        title: 'JobCard List',
        paging: true,
        pageSize: 8,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/JobCard/JobListByFiter?Fk_Order=' + OrdId
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                $('#cmdJobCardDone1').click();
                $("#txtDcNo").focus();
            });
        },
        fields: {
            Pk_JobCardID: {
                title: 'CardID',
                key: true,
                width: '2%'
            },
            JDate: {
                title: 'J.Card Date',
                width: '3%'
            },
            Fk_Order: {
                title: 'Cust_Order No',
                width: '2%'
            },
        
            BoxName: {
                title: 'BoxName',
                width: '5%'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#JobCardSearchContainer1').jtable('load', {
            Pk_JobCardID: $('#txtdlgJobCard').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdJobCardSearch1').click(function (e) {
        e.preventDefault();

        $('#JobCardSearchContainer1').jtable('load', {
            Pk_JobCardID: $('#txtdlgJobCard').val()
        });
    });

    $('#cmdJobCardDone1').click(function (e) {
        e.preventDefault();
        var rows = $('#JobCardSearchContainer1').jtable('selectedRows');
        //document.getElementById('Quantity').value = rows[0].data.Quantity;
        document.getElementById('Fk_JobCardID').value = rows[0].data.Pk_JobCardID;
        //$('#txtFk_JobCardID').val(rows[0].data.Fk_JobCardID);
        $("#searchJobCardDialog1").modal("hide");
        $("#txtDcNo").focus();
    });




}


function beforeModelSaveEx() {

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["IssueDate"] = $('#dtIssueDate').val();
    viewModel.data["Fk_OrderNo"] = $('#txtCustomerOrder').val();
    viewModel.data["Fk_JobCardID"] = $('#Fk_JobCardID').val();

    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["IssueDate"] = $('#dtIssueDate').val();
    viewModel1.data["Fk_OrderNo"] = $('#txtCustomerOrder').val();
    viewModel1.data["Fk_JobCardID"] = $('#Fk_JobCardID').val();
}


function ReferenceFieldNotInitilized(viewModel) {

    $('#txtMaterial').wgReferenceField({
        keyProperty: "Fk_Material",
        displayProperty: "Name",
        loadPath: "Material/Load",
        viewModel: viewModel
    });
    $('#txtCustomerOrder').wgReferenceField({
        keyProperty: "Pk_Order",
        displayProperty: "Pk_Order",
        loadPath: "Order/Load",
        viewModel: viewModel
    });
    //$('#txtFk_JobCardID').wgReferenceField({
    //    keyProperty: "Pk_JobCardID",
    //    displayProperty: "Pk_JobCardID",
    //    loadPath: "JobCard/Load",
    //    viewModel: viewModel
    //});
    if (viewModel.data != null) {
        $('#txtMaterial').wgReferenceField("setData", viewModel.data["Fk_Material"]);
        $('#txtCustomerOrder').wgReferenceField("setData", viewModel.data["Fk_CustomerOrder"]);
        //$('#txtFk_VendorId').wgReferenceField("setData", viewModel.data["Fk_VendorId"]);
    }
}

function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#dlgJobCardSearchFields1').empty();
    //$('#JobCardSearchContainer1').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();
}

function showOnlyBoard() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Board').val()
    });



}

function showOnlyGum() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Gum').val()
    });



}

function showOnlyInk() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Ink').val()
    });



}

function showOnlyPaper() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Paper').val()
    });



}


function showOnlyHoneycomb() {
    $('#MaterialSearchContainer').jtable('load', {
        MaterialCategory: $('#Honeycomb').val()
    });


}

function QtyCheck() {

    if (Number($('#Quantity').val()) > (Number(stockval))) {

        alert("Quantity cannot be Greater than StockValue, Stock Value:" + (Number(stockval)));
        $('#Quantity').val("");
        $("#Quantity").focus();
    }
}