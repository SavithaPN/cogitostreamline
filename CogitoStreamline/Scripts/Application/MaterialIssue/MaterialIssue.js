﻿
function MaterialIssue() {
    this.data = new Object();
    this.data["Pk_MaterialIssueDetailsID"] = "";
    this.data["Fk_IssueID"] = "";
    this.data["Fk_Material"] = "";
    this.data["txtFk_Material"] = "";
    this.data["Quantity"] = "";
    this.data["RollNo"] = "";
    this.data["Color"] = "";
    this.data["Mill"] = "";


    this.updateValues = function () {
        //var i = 0;

        //while (gMaterials[i]) {
        //    if (gMaterials[i].Id == this.data["Fk_Material"]) {

        //        this.data["MaterialName"] = gMaterials.Name;
        //        i++;
        //    }

        //}
        //,
        //Pk_StockID: {
        //        title: 'Stkid'

        //},
        //this.data["RequiredDate"] = $('#dtRequiredDate').val();
        this.data["txtFk_Material"] = $('#txtMaterial').val();
        //this.data["Fk_CustomerOrder"] = $('#txtCustomerOrder').val();
        this.data["Quantity"] = $('#Quantity').val();
        this.data["Pk_StockID"] = $('#Pk_StockID').val();
        this.data["RollNo"] = $('#RollNo').val();
        this.data["Color"] = $('#Shade').val();
        this.data["Mill"] = $('#SName').val();
    };


    this.load = function (Pk_MaterialIssueDetailsID, Pk_StockID, Fk_Material, txtFk_Material, Quantity, RollNo,Color,Mill) {
        this.data["Pk_MaterialIssueDetailsID"] = Pk_MaterialIssueDetailsID;
        
        this.data["Fk_Material"] = Fk_Material;
        this.data["txtFk_Material"] = txtFk_Material;
        this.data["Pk_StockID"] = Pk_StockID;
        this.data["Quantity"] = Quantity;
        this.data["RollNo"] = RollNo;
        this.data["Color"] = Color;
        this.data["Mill"] = Mill;
        //this.updateValues();
    };
}


