﻿
var objShortCut = null;
var fkbox = 0;

function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {

            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },
            //PartNo: {
            //    title: 'PartNo',
            //    width: '25%'
            //},
            Name: {
                title: 'Name',
                width: '25%'
            },
            Description: {
                title: 'Description',
                width: '25%'
            },

            Print: {
                title: 'Box Spec Det',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-printer'></i>");
                    $(button).click(function () {

                        fkbox = row.record.Pk_BoxID;
                        var BType = row.record.BoxType;
                        _comLayer.parameters.add("fkbox", fkbox);
                        _comLayer.executeSyncAction("BoxMaster/BoxRep", _comLayer.parameters);

                        if (BType != "Plain Board") {
                            var strval = "~/ConvertPDF/BoxReport" + fkbox + ".pdf"
                            ////////////////////////////


                            var xhr = new XMLHttpRequest();
                            var urlToFile = "~/ConvertPDF/BoxReport" + fkbox + ".pdf"
                        }
                        else {
                            var strval = "~/ConvertPDF/BoxReportPBoard" + fkbox + ".pdf"
                            ////////////////////////////


                            var xhr = new XMLHttpRequest();
                            var urlToFile = "~/ConvertPDF/BoxReportPBoard" + fkbox + ".pdf"
                            
                        }


                        xhr.open('HEAD', urlToFile, false);
                        xhr.send();

                        if (xhr.status == "404") {
                            alert('Data Not Available , File does not Exist');
                            return false;


                        } else {
                            window.open(strval, '_blank ', 'width=700,height=250');
                            return true;
                        }


                        /////////////////////////
                       
                        
                    });
                    return button;
                }
            }
        }
        });

                $('#LoadRecordsButton').click(function (e) {
                    e.preventDefault();
                    $('#MainSearchContainer').jtable('load', {
                        Pk_BoxID: $('#txtBoxID').val(),
                        Name: $('#txtName').val()
                    });
                });
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    _page.getViewByName('New').viewModel.addAddtionalDataSources("BoxType", "getBoxType", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("BoxType", "getBoxType", null);

}

function afterEditShow(viewObject) {
    $('#cmdBoxSpecs').click(function (e) {
        e.preventDefault();
        //window.open("/Estimation?Enquiry=" + $('#hidPkEnquiry').val());
        fkbox = $('#hidPkBox').val()
        _comLayer.parameters.add("fkbox", fkbox);
        _comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
        window.open("/BoxSpec?BoxMaster=" + $('#hidPkBox').val());


    });
}


function CheckHeadDuplicate() {
    _comLayer.parameters.clear();
    var oResult;
    _comLayer.parameters.add("Name", $('#txtName').val());
    oResult = _comLayer.executeSyncAction("BoxMaster/NameDuplicateChecking", _comLayer.parameters);
    if (oResult.TotalRecordCount > 0)
        return "* " + "Box Name Already added";
}










