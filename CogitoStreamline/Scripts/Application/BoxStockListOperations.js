﻿var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Stock List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxStockList/BoxStockListByFiter',
            //updateAction: '',
            //createAction: '',
            //deleteAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
          
                //Pk_Material = $(this).data("record").Pk_Material;
                document.getElementById('txtBoxID').value = $(this).data("record").Pk_Material;
                document.getElementById('txtPkPStock').value = $(this).data("record").StockID;
                document.getElementById('txtstock').value = $(this).data("record").Quantity;
                document.getElementById('txtPartId').value = $(this).data("record").PartID;

                
              
                return true;
            });


        },
        fields: {
      
        Details: {
            title: '',
            width: '1%',
            sorting: false,
            paging: true,
            pageSize: 5,
            edit: false,
            create: false,
            listClass: 'child-opener-image-column',
            display: function (data) {
                var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                $img.click(function () {
                    $('#MainSearchContainer').jtable('openChildTable',
                                $img.closest('tr'),
                                {
                                    title: 'Box Details',
                                    paging: true,
                                    pageSize: 10,
                                    actions: {
                                        listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.Pk_BoxID
                                    },
                                    fields: {
                                        Pk_BoxSpecID: {
                                            title: 'Spec ID',
                                            key: true,
                                            list:false
                                        },
                                        OYes: {
                                            title: 'OuterShell',
                                             
                                        },
                                        CYes: {
                                            title: 'Cap',
                                            //display: function (data) {
                                            //    if (data.record.CYes == true || data.record.CYes == 'True') {
                                            //        list: true;
                                            //    }
                                            //    else {
                                            //        list: false;
                                            //    }
                                            //},
                                        },
                                        LPYes: {
                                            title: 'L.Partition',
                                            //display: function (data) {
                                            //    if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                            //        list: true;
                                            //    }
                                            //    else {
                                            //        list: false;
                                            //    }
                                            //},
                                        },
                                        WPYes: {
                                            title: 'W.Partition',
                                            //display: function (data) {
                                            //    if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                            //        list: true;
                                            //    }
                                            //    else {
                                            //        list: false;
                                            //    }
                                            //},
                                        },
                                        PYes: {
                                            title: 'Plate',
                                            //display: function (data) {
                                            //    if (data.record.PYes == true || data.record.PYes == 'True') {
                                            //        list: true;
                                            //    }
                                            //    else {
                                            //        list: false;
                                            //    }
                                            //},
                                        },
                                        TYes: {
                                            title: 'Top',
                                        },
                                        BYes: {
                                            title: 'Bottom',
                                        },
                                        Details: {
                                            title: '',
                                            width: '1%',
                                            sorting: false,
                                            paging: true,
                                            pageSize: 5,
                                            edit: false,
                                            create: false,
                                            listClass: 'child-opener-image-column',
                                            display: function (data) {
                                                var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                $img.click(function () {
                                                    $('#MainSearchContainer').jtable('openChildTable',
                                                                $img.closest('tr'),
                                                                {
                                                                    title: 'Specifications',
                                                                    paging: true,
                                                                    pageSize: 10,
                                                                    actions: {
                                                                        listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                    },
                                                                    fields: {
                                                                        Pk_PartPropertyID: {
                                                                            title: 'Property ID',
                                                                            key: true,
                                                                            create: false,
                                                                            edit: false,
                                                                            list: false
                                                                        },
                                                                        Length: {
                                                                            title: 'Length',
                                                                            key: false
                                                                        },
                                                                        Width: {
                                                                            title: 'Width'

                                                                        },
                                                                        Height: {
                                                                            title: 'Height',
                                                                            width: '2%'
                                                                        },
                                                                        Weight: {
                                                                            title: 'Weight',
                                                                            width: '2%'
                                                                        }
                                                                    },
                                                                    formClosed: function (event, data) {
                                                                        data.form.validationEngine('hide');
                                                                        data.form.validationEngine('detach');
                                                                    }
                                                                }, function (data) { //opened handler
                                                                    data.childTable.jtable('load');
                                                                });
                                                });
                                                return $img;
                                            }
                                        },
                                    },
                                    formClosed: function (event, data) {
                                        data.form.validationEngine('hide');
                                        data.form.validationEngine('detach');
                                    }
                                }, function (data) { //opened handler
                                    data.childTable.jtable('load');
                                });
                });
                return $img;
            }
        },


       
        StockID: {
            title: 'StockID',
            key: true,
                width: '25%'
            },
 Pk_Material: {
            title: 'BoxId',
            //key: true

        },
        MaterialName: {
            title: 'Box Name',
            width: '25%'
        },
        PartName: {
            title: 'Part Name',
            width: '25%'
        },
        CustName: {
            title: 'Cust.Name',
            width: '25%'
        },
        Quantity: {
            title: 'Stock Qty',
            width: '25%'
        },
        PartID: {
            title: 'Partid',
           list:false
        },
        //Print: {
        //    title: 'Print',
        //    width: '2%',
        //    //display: function (data) {
        //    //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
        //    display: function (row) {
        //        var button = $("<i class='icon-printer'></i>");
        //        $(button).click(function () {

        //            fkbox = row.record.Pk_BoxID;
        //            _comLayer.parameters.add("fkbox", fkbox);
        //            _comLayer.executeSyncAction("BoxMaster/BoxRep", _comLayer.parameters);
        //            var strval = "ConvertPDF/BoxReport" + fkbox + ".pdf"

        //            window.open(strval, '_blank ', 'width=700,height=250');

        //        });
        //        return button;
        //    }
        //},

        //B_Spec: {
        //    title: 'B_Spec',
        //    width: '25%',
        //    listClass: 'child-opener-image-column',
        //    display: function (row) {
                
        //        var button1 = $("<button class='bnt'>View Specs</button>");
        //        button1.click(function (e) {
        //            fkbox = row.record.Pk_BoxID;
        //            PPly = row.record.Ply;
        //            //_comLayer.parameters.add("fkbox", fkbox);
        //            //_comLayer.executeSyncAction("BoxSpec/RecvFk", _comLayer.parameters);
        //            window.open("/BoxSpec?BoxMaster=" + fkbox + "&Ply=" + PPly);
                        
        //        });
        //        return button1;
        //    }
            
        //},

        Details: {
            title: '',
            width: '1%',
            sorting: false,
            paging: true,
            pageSize: 5,
            edit: false,
            create: false,
            listClass: 'child-opener-image-column',
            display: function (data) {
                var $img = $("<i class='icon-iphone' title='Box Details'></i>");
                $img.click(function () {
                    $('#MainSearchContainer').jtable('openChildTable',
                                $img.closest('tr'),
                                {
                                    title: 'Box Details',
                                    paging: true,
                                    pageSize: 10,
                                    actions: {
                                        listAction: '/BoxMaster/BoxDetails?Pk_BoxID=' + data.record.Pk_Material
                                    },
                                    fields: {
                                        Pk_BoxSpecID: {
                                            title: 'Spec ID',
                                            key: true,
                                            list:false
                                        },
                                        OYes: {
                                            title: 'OuterShell',
                                            display: function (data) {
                                                if (data.record.OYes == true || data.record.OYes == 'True') {
                                                    return 'Yes';
                                                }
                                                //else {
                                                //    list:false;
                                                //    //width: '2%';

                                                //}
                                            },
                                            //list: true
                                        },
                                        CYes: {
                                            title: 'Cap',
                                            display: function (data) {
                                                if (data.record.CYes == true || data.record.CYes == 'True') {
                                                    return 'Yes';
                                                }
                                                //else {
                                                //    return 'No';
                                                //}
                                            },
                                        },
                                        LPYes: {
                                            title: 'L.Partition',
                                            display: function (data) {
                                                if (data.record.LPYes == true || data.record.LPYes == 'True') {
                                                    return 'Yes';
                                                }
                                                //else {
                                                //    width: '2%';
                                                //}
                                            },
                                        },
                                        WPYes: {
                                            title: 'W.Partition',
                                            display: function (data) {
                                                if (data.record.WPYes == true || data.record.WPYes == 'True') {
                                                    return 'Yes';
                                                }
                                                //    else {
                                                //        list: false;
                                                //    }
                                            },
                                        },
                                        PYes: {
                                            title: 'Plate',
                                            display: function (data) {
                                                if (data.record.PYes == true || data.record.PYes == 'True') {
                                                    return 'Yes';
                                                }
                                                //    else {
                                                //        list: false;
                                                //    }
                                            },
                                        },
                                        TYes: {
                                            title: 'Top',
                                        },
                                        BYes: {
                                            title: 'Bottom',
                                        },
                                        Details: {
                                            title: '',
                                            width: '1%',
                                            sorting: false,
                                            paging: true,
                                            pageSize: 5,
                                            edit: false,
                                            create: false,
                                            listClass: 'child-opener-image-column',
                                            display: function (data) {
                                                var $img = $("<i class='icon-iphone' title='Specifications'></i>");
                                                $img.click(function () {
                                                    $('#MainSearchContainer').jtable('openChildTable',
                                                                $img.closest('tr'),
                                                                {
                                                                    title: 'Specifications',
                                                                    paging: true,
                                                                    pageSize: 5,
                                                                    actions: {
                                                                        listAction: '/BoxSpec/BoxSpecDetails?Pk_BoxSpecID=' + data.record.Pk_BoxSpecID
                                                                    },
                                                                    fields: {
                                                                        Pk_PartPropertyID: {
                                                                            title: 'Property ID',
                                                                            key: true,
                                                                            create: false,
                                                                            edit: false,
                                                                            width: '2%',
                                                                            list: false
                                                                        },
                                                                        Length: {
                                                                            title: 'Length',
                                                                            key: false,
                                                                            width: '2%'
                                                                        },
                                                                        Width: {
                                                                            title: 'Width',
                                                                            width: '2%'
                                                                        },
                                                                        Height: {
                                                                            title: 'Height',
                                                                            width: '2%'
                                                                        },
                                                                        Weight: {
                                                                            title: 'Weight',
                                                                            width: '2%'                                                                            
                                                                        },

                                                                        Details: {
                                                                            title: '',
                                                                            width: '1%',
                                                                            sorting: false,
                                                                            paging: true,
                                                                            pageSize: 5,
                                                                            edit: false,
                                                                            create: false,
                                                                            listClass: 'child-opener-image-column',
                                                                            display: function (data) {
                                                                                var $img = $("<i class='icon-iphone' title='Layers'></i>");
                                                                                $img.click(function () {
                                                                                    $('#MainSearchContainer').jtable('openChildTable',
                                                                                                $img.closest('tr'),
                                                                                                {
                                                                                                    title: 'Layers Details',
                                                                                                    paging: true,
                                                                                                    pageSize: 10,
                                                                                                    actions: {
                                                                                                        listAction: '/ItemPartProperty/LayerDet?Pk_PartPropertyID=' + data.record.Pk_PartPropertyID
                                                                                                    },
                                                                                                    fields: {
                                                                                                        Pk_LayerID: {
                                                                                                            title: 'Layer ID',
                                                                                                            key: true,
                                                                                                            create: false,
                                                                                                            edit: false,
                                                                                                            list: false
                                                                                                        },
                                                                                                        GSM: {
                                                                                                            title: 'GSM',
                                                                                                            key: false
                                                                                                        },
                                                                                                        BF: {
                                                                                                            title: 'BF'

                                                                                                        },
                                                                                                        Weight: {
                                                                                                            title: 'Weight'
                                                                                                        }
                                                                                                    },
                                                                                                    formClosed: function (event, data) {
                                                                                                        data.form.validationEngine('hide');
                                                                                                        data.form.validationEngine('detach');
                                                                                                    }
                                                                                                }, function (data) { //opened handler
                                                                                                    data.childTable.jtable('load');
                                                                                                });
                                                                                });
                                                                                return $img;
                                                                            }
                                                                        },
                                                                    },
                                                                    formClosed: function (event, data) {
                                                                        data.form.validationEngine('hide');
                                                                        data.form.validationEngine('detach');
                                                                    }
                                                                }, function (data) { //opened handler
                                                                    data.childTable.jtable('load');
                                                                });
                                                });
                                                return $img;
                                            }
                                        },
                                    },
                                    formClosed: function (event, data) {
                                        data.form.validationEngine('hide');
                                        data.form.validationEngine('detach');
                                    }
                                }, function (data) { //opened handler
                                    data.childTable.jtable('load');
                                });
                });
                return $img;
            }
        },
    },

    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            //Pk_Material: $('#txtStockID').val(),
            Name: $('#txtMaterialName').val(),
            CustName: $('#txtCustName').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdPrint').click(function (e) {
        e.preventDefault();
        var fkBranch = "";
        _comLayer.parameters.add("fkBranch", fkBranch);
        _comLayer.executeSyncAction("BoxStockList/BoxStockRep", _comLayer.parameters);
        var strval = "ConvertPDF/BoxStock" + ".pdf";

        window.open(strval, '_blank ', 'width=700,height=250');
        return true;
    });


    $('#cmdInsert').click(function (e) {
        e.preventDefault();
        //document.getElementById('txtBoxID').value = $(this).data("record").Pk_Material;
        //document.getElementById('txtPkPStock').value = $(this).data("record").StockID;
        //document.getElementById('txtstock').value = $(this).data("record").Quantity;
        //document.getElementById('txtPartId').value = $(this).data("record").PartID;

        var BoxID = document.getElementById('txtBoxID').value;
        var StockVal = document.getElementById('txtstock').value;
        var StockID = document.getElementById('txtPkPStock').value;
        var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("BoxID", BoxID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("StockID", StockID);
        _comLayer.parameters.add("PartId", PartId);

        _comLayer.executeSyncAction("BoxStockList/InsertRow", _comLayer.parameters);
        //var strval = "ConvertPDF/ShadeWise" + ".pdf";
        document.getElementById('txtBoxID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";
        document.getElementById('txtPartId').value = "";
     

        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });



    $('#cmddelete').click(function (e) {
        e.preventDefault();
        var BoxID = document.getElementById('txtBoxID').value;
        var StockVal = document.getElementById('txtstock').value;
        var StockID = document.getElementById('txtPkPStock').value;
        var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("BoxID", BoxID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("StockID", StockID);
        _comLayer.parameters.add("PartId", PartId);


        _comLayer.executeSyncAction("BoxStockList/DeleteRow", _comLayer.parameters);
     
        document.getElementById('txtBoxID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";
        document.getElementById('txtPartId').value = "";
      

    });
    $('#cmdUpdate').click(function (e) {
        e.preventDefault();

        var BoxID = document.getElementById('txtBoxID').value;
        var StockVal = document.getElementById('txtstock').value;
        var StockID = document.getElementById('txtPkPStock').value;
        var PartId = document.getElementById('txtPartId').value;

        _comLayer.parameters.add("BoxID", BoxID);
        _comLayer.parameters.add("StockVal", StockVal);
        _comLayer.parameters.add("StockID", StockID);
        _comLayer.parameters.add("PartId", PartId);


        _comLayer.executeSyncAction("BoxStockList/UpdateRow", _comLayer.parameters);

        document.getElementById('txtBoxID').value = "";
        document.getElementById('txtPkPStock').value = "";
        document.getElementById('txtstock').value = "";
        document.getElementById('txtPartId').value = "";

        //window.open(strval, '_blank ', 'width=700,height=250');
        //return true;
    });


}


