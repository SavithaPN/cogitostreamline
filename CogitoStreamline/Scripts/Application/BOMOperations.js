﻿var BoxID;
var QtyVal;
var curViewModel = null;
var fkbox;
var BName;
var MatID;


function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Box List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BoxMaster/BoxNameListByFiter',
            
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');

                $('#cmdNew').click();
            });
        },
        fields: {

            Pk_BoxID: {
                title: 'Id',
                key: true,
                list: true
            },
            Customer: {
                title: 'Customer',
                width: '25%',
                list: true
            },
            Name: {
                title: 'Box Name',
                width: '25%'
            },
            BoxType: {
                title: 'BoxType',
                width: '25%'
            },

            //PartNo: {
            //    title: 'PartNo',
            //    width: '25%'
            //},
            
           
            BOM: {
                title: 'BOM',
                width: '2%',
                //display: function (data) {
                //    return '<button type="button" onclick="alert(' + data.record.Invno + ')">Create PDF</button> ';
                display: function (row) {
                    var button = $("<i class='icon-iphone' title='BOM'></i>");
                    $(button).click(function () {

                        fkbox = row.record.Pk_BoxID;
                        BName = row.record.Name;
                         $('#cmdNew').click();
          
                    });
                    return button;
                }
            },
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            CustName: $('#txtCust').val(),
            Name: $('#txtName').val()
        });
    });
    $('#LoadRecordsButton').click();

    $('#cmdNew').click(function (e) {
        e.preventDefault();
      
        _page.showView('New');

        
    }); 
    //$("#cmdNew").attr('disabled', true);
    cmdNew.style.visibility = 'hidden';
}


function afterNewShow(viewObject) {
    

    document.getElementById('Qty').value = 1;
    document.getElementById('Name').value = BName;


     $('#cmdSearch').click(function (e) {
         document.getElementById('FkBoxID').value = fkbox;
         
         e.preventDefault();
         setUpOrderSearch(viewObject);
         setUpPaperSummary(viewObject);
         setUpAssStk(viewObject);
     });
     $('#cmdReport').click(function (e) {
         document.getElementById('FkBoxID').value = fkbox;

         e.preventDefault();
   
         _comLayer.parameters.add("fkbox", fkbox);
         _comLayer.executeSyncAction("BOM/BomRep", _comLayer.parameters);
         var strval = "ConvertPDF/BomReport" + fkbox + ".pdf"

         ////////////////////////////


         var xhr = new XMLHttpRequest();
         var urlToFile = "ConvertPDF/BomReport" + fkbox + ".pdf"
         xhr.open('HEAD', urlToFile, false);
         xhr.send();

         if (xhr.status == "404") {
             alert('Data Not Available , File does not Exist');
             return false;


         } else {
   window.open(strval, '_blank ', 'width=700,height=250');
             return true;
         }


         /////////////////////////

      

     });

     $('#Qty').keypress(function (e) {
         if (e.keyCode == 13) {
             e.preventDefault();
             setUpOrderSearch(viewObject);
             setUpPaperSummary(viewObject);
             setUpAssStk(viewObject);
         }

     });
 }

function setUpOrderSearch(viewObject)
{
    BoxID = document.getElementById('FkBoxID').value;
    QtyVal = document.getElementById('Qty').value;


    $('#divPaperStock').jtable({
        title: 'Box Paper List',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/Order/OrderPaperDetails?Fk_BoxID=' ,
            //updateAction: ''
        },

        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Pk_Material: {
                title: 'Pk_Material',
                key: false,
                list: false
            },
     
            MName: {
                title: 'Mat.Name'

            },
            PName: {
                title: 'Part Name'

            },
            PQty: {
                title: 'Part Qty. in Box '

            },
            StkQty: {
                title: 'Ex.Stock',
                key: false
            },
            PaperWt: {
                title: 'Paper Wt./Box',
                key: false,
                list:false
            },

            PWt: {
                title: 'Paper Wt./Box',
                key: false,
                list: true
            },
            PaperReq: {
                title: 'Paper Req.',
                display: function (data) {
                    if (data.record.PaperWt > 0) {
                        return (data.record.PaperWt * QtyVal * data.record.PQty);
                    }
                }
            }
        }
    });
     
        $('#divPaperStock').jtable('load', {
            Fk_BoxID: $('#FkBoxID').val(),
          
  
        });
        
}

function setUpPaperSummary(viewObject) {
    BoxID = document.getElementById('FkBoxID').value;
    QtyVal = document.getElementById('Qty').value;


    $('#divSummary').jtable({
        title: 'PO',
        paging: true,
        pageSize: 15,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {

            listAction: '/BOM/PODetails?Pk_BoxID=',
            //updateAction: ''
        },
        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
              

            });
        },
        //Pk_BoxID = p.Pk_BoxID,
        //POQty=p.Quantity,
        //PoNo=p.Pk_PONo,
        //Name = p.Name,
        //PoDate=p.PODate,
        //MillName=p.MillName,
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Name: {
                title: 'Mat.Name',
                key: false,
                list: true
            },    
            PoNo: {
                title: 'PO No.'
            },
            PoDate: {
                title: 'PO Date',
                key: false
            },
            POQty: {
                title: 'PO Qty',
                key: false
            },
            InQty: {
                title: 'Inwd Qty',
                list: true
            },
            PendQty: {
                title: 'PendQty',
            },
            //PaperReq: {
            //    title: 'Paper Req.',
            //    display: function (data) {
            //        if (data.record.Weight > 0) {
            //            return (data.record.Weight * QtyVal);
            //        }
            //    }
            //},
            MillName: {
                title: 'Mill Name',
                key: false,
                list: true

            },
            //JCard: {
            //    title: 'JobC',
            //    width: '2%',
            //    listClass: 'child-opener-image-column',
            //    display: function (data) {
            //        var $img = $("<i class='icon-iphone' title='JCard'></i>");
            //        $img.click(function () {
            //            var MatID = data.record.Pk_Material;
            //            $('#MainSearchContainer').jtable('openChildTable',
            //                        $img.closest('tr'),
            //                        {
            //                            title: 'JobCard Details',
            //                            paging: true,
            //                            pageSize: 10,
            //                            actions: {
            //                                listAction: '/BOM/JobAssignedStock?Pk_Material=' + MatID,
            //                                            //updateAction: ''
            //                            },
            //                            fields: {
            //                                slno: {
            //                                    title: 'slno',
            //                                    key: true,
            //                                    list: false
            //                                },
            //                                MName: {
            //                                    title: 'Mat.Name',
            //                                    list: true

            //                                },
            //                                BoxName: {
            //                                    title: 'BoxName',
            //                                    list: true
            //                                },
            //                                Pk_JobCardID: {
            //                                    title: 'JobCard No.',
            //                                    list: true
            //                                },
            //                                Pk_Material: {
            //                                    title: 'Pk_Material',
            //                                    key: false,
            //                                    list: false
            //                                },
            //                                JDate: {
            //                                    title: 'JCard Date',
            //                                    key: false
            //                                },                    
            //                                Quantity: {
            //                                    title: 'Assigned Qty'

            //                                },
            //                                StockQty: {               
            //                                    title: 'Ex.Stk.Qty'                                            
            //                                },
            //                                Details: {
            //                                    title: 'PO',
            //                                    width: '1%',
            //                                    sorting: false,
            //                                    paging: true,
            //                                    pageSize: 5,
            //                                    edit: false,
            //                                    create: false,
            //                                    listClass: 'child-opener-image-column',
            //                                    display: function (data) {
            //                                        var $img = $("<i class='icon-iphone' title='PO'></i>");
            //                                        $img.click(function () {
            //                                            var MatID = data.record.Pk_Material;
            //                                            $('#MainSearchContainer').jtable('openChildTable',
            //                                                        $img.closest('tr'),
            //                                                        {
            //                                                            title: 'PO Details',
            //                                                            paging: true,
            //                                                            pageSize: 5,
            //                                                            actions: {
            //                                                                listAction: '/BoM/PODet?Pk_Material=' + MatID
            //                                                            },
            //                                                            fields: {
            //                                                                Pk_Material: {
            //                                                                    title: 'Pk_Material',
            //                                                                    key: true,
            //                                                                    list: false
            //                                                                },
            //                                                                Name: {
            //                                                                    title: 'Mat.Name',
            //                                                                    key: false,
            //                                                                    width: '2%'
            //                                                                },
            //                                                                Pk_PONo: {
            //                                                                    title: 'PO NO.',
            //                                                                    width: '2%'
            //                                                                },
            //                                                                PODate: {
            //                                                                    title: 'PO DT',
            //                                                                    width: '2%'
            //                                                                },
            //                                                                POQty: {
            //                                                                    title: 'PO Qty',
            //                                                                    width: '2%'
            //                                                                },

            //                                                                AccQty: {
            //                                                                    title: 'Inwd.Qty',
            //                                                                    width: '2%'
            //                                                                },
            //                                                                PendQty: {
            //                                                                    title: 'Pending Qty',
            //                                                                    width: '2%'
            //                                                                },
                                                                           
            //                                                            },
            //                                                            formClosed: function (event, data) {
            //                                                                data.form.validationEngine('hide');
            //                                                                data.form.validationEngine('detach');
            //                                                            }
            //                                                        }, function (data) { //opened handler
            //                                                            data.childTable.jtable('load');
            //                                                        });
            //                                        });
            //                                        return $img;
            //                                    }
            //                                },
            //                            },
            //                            formClosed: function (event, data) {
            //                                data.form.validationEngine('hide');
            //                                data.form.validationEngine('detach');
            //                            }
            //                        }, function (data) { //opened handler
            //                            data.childTable.jtable('load');
            //                        });
            //        });
            //        return $img;
            //    }
            //},
        }
    });
    //$('#divSummary').jtable('load');
    $('#divSummary').jtable('load', {
        Pk_BoxID: $('#FkBoxID').val(),


    });

}



function setUpAssStk(viewObject) {
    BoxID = document.getElementById('FkBoxID').value;
  

    $('#divDeliveryschedule').jtable({
        title: 'Job Card Details',
        paging: true,
        pageSize: 10,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/BOM/JobAssignedStock?Fk_BoxID=',
            
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            JDate: {
                title: 'JCard Date',
                key: false
            },
            MName: {
                title: 'Mat.Name',
                list: true

            },
            Pk_JobCardDet: {
                title: 'Pk_JobCardDet',
                list: false
            },

            Pk_JobCardID: {
                title: 'JobCardID',
                list:false
            },
            RollNo: {
                title: 'RollNo',
                list:true
            },
            Quantity: {
                title: 'Assigned Qty'

            },
            //StockQty: {               
            //    title: 'Ex.Stk.Qty'

            //},
        }
    });
    //$('#LoadRecordsButton').click(function (e) {
    //    e.preventDefault();
    $('#divDeliveryschedule').jtable('load', {
        Fk_BoxID: BoxID,
    });
     

    
}