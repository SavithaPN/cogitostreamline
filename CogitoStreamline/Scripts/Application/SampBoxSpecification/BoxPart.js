﻿var ODLenVal;
var ODWidVal;
var ODHtVal;
var PVal;
var e;
var i = 0;

$.fn.slPart = function (options) {


    this.options = $.extend({
        viewTag: "",
        title: "",
        id: "",
        outershell: false,
        cap: false,
        rate: false,
        top: false,
        bottom: false,
        sleeve: false,
        plate: false,
        model: new part(),
        loadFromModel: false
    }, options);

    this.model = this.options.model;

    _comLayer.parameters.clear();


    if (this.options.outershell) {
        _comLayer.parameters.add("ViewName", "_OuterShell");

    }
    else if (this.options.cap) {
        _comLayer.parameters.add("ViewName", "_CapPart");
    }
    else if (this.options.rate) {
        _comLayer.parameters.add("ViewName", "_Rate");
    }
        //else if (this.options.width) {
        //    _comLayer.parameters.add("ViewName", "_Width");
        //}
    else {
        _comLayer.parameters.add("ViewName", "_GenericPart");
    }
    var sPath = _comLayer.buildURL(this.options.viewTag, "LoadViewLayout");
    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);

    if (oResult.success == true) {
        $(this).empty();
        $(this).html(oResult.data);
    }

    //alert(this.options.id);

    $(this).find('.partHeader').text(this.options.title);
    $(this).find('.mainPartContainer').attr("id", this.options.id);

    var that = this;

    this.getModel = function () {
        return this.model;
    }

    if (!this.options.rate) {
        $(this).find('.mainPartContainer').find(".smalltile").click(function () {

            var cntr = $($(this).parent()).parent();

            var ix = $(this).attr("value");

            displayLayers(cntr, ix, that);
        });
    }
    else {
        var cntr = $($(this).parent()[0]);

        //ko.cleanNode($(this).parent()[0]);
        ko.applyBindings(that.options.estimationModel, $(this).parent()[0]);

        that = this;

        $(cntr).find('.input-mini').change(function () {
            that.options.rateChanged(that.options.estimationModel);
        });
    }

    //check if are loading from model
    if (this.options.loadFromModel) {
        displayLayers($($(this).parent()[0]), this.model.layers.length, that);
    }
    else {
        if (!this.options.rate) {
            //  var ix = _util.getParameterByName("Ply");

            var sel = this.options.title;

            var bstr = sel.search("-");
            //var bstr3 = bstr.substr(bstr + 6);
            var bstr2 = sel.substring(0, bstr);
            //alert(bstr2);
            switch (bstr2) {
                case "Box":
                    var ix = document.getElementById('txtOuterShellPly').value;
                    break;
                case "Length Partition":
                    var ix = document.getElementById('txtLPartationPly').value;
                    break;

                case "Width Partition":
                    var ix = document.getElementById('txtWidthPartationPly').value;
                    break;

                case "Plate":
                    var ix = document.getElementById('txtPlatePly').value;
                    break;

                case "Cap":
                    var ix = document.getElementById('txtCapPly').value;
                    break;
                case "Top":
                    var ix = document.getElementById('txtTopPly').value;
                    break;
                case "Bottom":
                    var ix = document.getElementById('txtBottPly').value;
                    break;
                case "Sleeve":
                    var ix = document.getElementById('txtSleevePly').value;
                    break;
            }

            var cntr = $($(this).parent());//.parent();
            displayLayers(cntr, ix, that);  //// 'ix' is the ply value

            document.getElementById('Ply').value = ix;

            var bstr = _util.getParameterByName("SampBoxMast");
            //var strlen=length(
            var bstr1 = bstr.search("TakeUp");
            var Ftype = bstr.search("FType");
            var bstr5 = bstr.search("~");
            var bstr4 = bstr.search(";");
            //var bstr2 = bstr.substr(bstr5 + 1, bstr4);
            var bstr2 = bstr.substring(bstr5 + 1, bstr4);
            //var bstr2 = bstr.substr((bstr1 + 7), bstr4);
            var bstr3 = bstr.substr((Ftype + 6));


            document.getElementById('FluteType').value = bstr3;

            if (sel == 'Outer Shell-1') {
                document.getElementById('OTakeUpFactor').value = bstr2;
            }
            //else {
            //    document.getElementById('OTakeUpFactor').value = '';
            //}

            _comLayer.parameters.add("FluteType", bstr3);
            _comLayer.parameters.add("Ply", ix);

            //oResult = _comLayer.executeSyncAction("ODID/LWHDetails", _comLayer.parameters);


            //ODLenVal = oResult.Records[0].Length;
            //ODWidVal = oResult.Records[0].Width;
            //ODHtVal = oResult.Records[0].Height;

            //document.getElementById('OLength1').value = oResult.Records[0].Length;
            //document.getElementById('OWidth1').value = oResult.Records[0].Width;
            //document.getElementById('OHeight1').value = oResult.Records[0].Height;
            //beforeModelSaveEx();
        }
    }

    return this;
}





function copyLayerValues(model, iSelected) {

    var i = 0;
    //var TF=document.getElementById('OTakeUpFactor').value;


    while (model.layers[i]) {

        var chk = "#chkLayer_" + i;

        if ($(chk).is(':checked')) {
            model.layers[i].gsm(model.layers[iSelected].gsm());
            model.layers[i].bf(model.layers[iSelected].bf());
            model.layers[i].color(model.layers[iSelected].color());
            model.layers[i].deckel(model.layers[iSelected].deckel());
            //var rval = TF;
            //if (rval.length == 0)
            //{
            //    document.getElementById('ORate').value = 1;
            //}

            if (i % 2 == 0) {

            }
            else {

                TF = document.getElementById('ORate').value;
            }

            model.layers[i].rate(model.layers[iSelected].rate());


            model.layers[i].Fk_Material(model.layers[iSelected].Fk_Material());
            model.layers[i].Weight(model.layers[iSelected].Weight());


            var rows = $('#MaterialSearchContainer').jtable('selectedRows');
            if (rows.length > 0) {
                $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);

                var fkMat = rows[0].keyValue;
                _comLayer.parameters.add("fkMat", fkMat);
                oResult = _comLayer.executeSyncAction("Paper/MaterialDetails", _comLayer.parameters);
                lblmat = oResult.data[0].Pk_Material;

                //alert(lblrate);
                //if (i % 2 != 0) {
                //    lblrate = document.getElementById('OR').value;
                //}
                //else {
                //    lblrate = 1;
                //}
            }
        }

        i++;
    }
}

function copySelection(model, type) {

    var i = 0;

    while (model.layers[i]) {
        var chk = "#chkLayer_" + i;
        $(chk).prop('checked', false);

        if (type == "Fluted") {
            if ((i % 2) != 0) {
                $(chk).prop('checked', true);
            }
        }
        else if (type == "NonFluted") {
            if ((i % 2) == 0) {
                $(chk).prop('checked', true);
            }
        }
        else {
            $(chk).prop('checked', true);
        }

        i++;
    }

}

function displayLayers(cntr, ix, that) {
    $(cntr).find('.selPly').hide();
    var lar = null;
    for (i = 0; i < ix; i++) {
        if (!that.options.loadFromModel) {
            var lar = new layer(0, 0, "N", 0);
            that.model.addLayer(lar);
        }
        else {
            lar = that.model.layers[i];
        }
        var lblGSm = $("<table><tr><td><label>GSM</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].gsm'></label></td></tr></table>");
        var lblbf = $("<table><tr><td><label>BF</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].bf'></label></td></tr></table>");
        var lblcolor = $("<table><tr><td><label></label></td><td><label class='GSM' data-bind='text: layers[" + i + "].color'></label></td></tr></table>");
        var lblrate = $("<table><tr><td><label>TK</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].rate'></label></td></tr></table>");
        var lblmat = $("<table><tr><td><label></label></td><td><label class='GSM' data-bind='text: layers[" + i + "].Fk_Material'></label></td></tr></table>");
        var lblwt = $("<table><tr><td><label>Wt</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].Weight'></label></td></tr></table>");
        var lbldeck = $("<table><tr><td><label>Deck</label></td><td><label class='GSM' data-bind='text: layers[" + i + "].deckel'></label></td></tr></table>");

        var dv = $("<div class='selector' style='height:100px; width:35px;' layerId ='" + i + "' ></div>");



        if (i % 2 != 0) {
            dv.addClass("flt");
        }

        dv.append(lblGSm);
        dv.append(lblbf);

        dv.append(lbldeck);
        dv.append(lblcolor);
        //dv.append(lblwt);
        //dv.append(lblrate);
        // dv.append(lblmat);
        var tr = $("<td></td>");
        tr.append(dv);
        //if ($(cntr).find(".row").length > 1)

        //$(cntr).find(".row")[(cntr).find(".row").length - 1].append(tr);
        $(cntr).find(".row").append(tr);

    }

    $(cntr).find('.selector').click(function () {
        ko.cleanNode(document.getElementById("estDialog"));

        var iCount = $(this).attr("layerId");
        var model = that.model.layers[iCount];

        //var rateval = document.getElementById('OTakeUpFactor').value;
        //alert(rate);
        ko.cleanNode("estDialog");
        ko.applyBindings(model, document.getElementById("estDialog"));

        var layerNumber = Number(iCount) + 1;

        var header = "Details of Layer " + layerNumber;

        $("#lyrHeader").text(header);

        //if (that.options.title == 'Outer Shell-1') {
        //    document.getElementById('ORate').value = rateval;
        //}
        //document.getElementById('OR').value = rateval;
        var i = 0;

        $("#dlgCopyOptions").empty();

        while (that.model.layers[i]) {
            var chk = $("<td><label class='checkbox inline'><input type='checkbox' value='" + i + "' id='chkLayer_" + i + "' /><span class='metro-checkbox'>" + (i + 1) + "</span></label><td>");
            $("#dlgCopyOptions").append(chk);
            i++;
        }

        $("#cmdCopyAll").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "");
        });

        $("#cmdCopyFluted").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "Fluted");
        });

        $("#cmdNonFluted").click(function (e) {
            e.preventDefault();
            copySelection(that.model, "NonFluted");
        });

        $("#cmdSaveLayers").click(function (e) {
            e.preventDefault();

            copyLayerValues(that.model, iCount);
            $("#estDialog").modal("hide");
        });
        //
        $('#cmdSearchdetails').click(function (e) {
            e.preventDefault();

            //_util.displayView("BoxSpec", "_AddMaterial", "dataDialogArea");
            //$('#dataDialogHeading').text("Search");

            //$("#dataDialog").modal("show");
            //$("#dataDialog").width(900);
            //$("#dataDialog").height(700);

            //$("#dataDialog").css("top", "50px");
            //$("#dataDialog").css("left", "350px");

            setUpMaterialSearch();
            //e.preventDefault();
            //setUpMaterialSearch();
            //_util.setDivPosition("divSearchMaterial", "block");
            //_util.setDivPosition("divCreateMaterial", "none");


        });


        $("#estDialog").find('.input-mini').change(function () {
            that.options.layerChanged(that.model, model, that.options.outershell, that.options.cap, that.options.plate, that.options.top, that.options.bottom, that.options.sleeve, that.options.title);
        });

        $("#estDialog").modal("show");
    });

    $(cntr).find('.selElements').show();


    ko.applyBindings(that.model, $(that).parent()[0]);

    $(cntr).find('.input-mini').change(function () {
        that.options.partChanged(that.model, that.options.outershell, that.options.cap, that.options.plate, that.options.top, that.options.bottom, that.options.sleeve, that.options.title);
    });

}


function SetDefNoBoards() {

    $("#NBoards").val("1");
    //$(this).model.noBoards = 1;

}
function cleanSearchDialog() {

    //Clean the search dialog of previous bindings and prepare for this selected dialog
    $('#MainSearhContainer').empty();
    $('#MainSearhContainer').append("<div id='SearchContainer'></div>");
    $("#dlgSearchFields").empty();
    $('#cmdSearch').off();
    $('#cmdDone').off();


}

function setUpMaterialSearch() {
    //Branch

    cleanSearchDialog();

    $("#srchMHeader").text("Paper Search");

    //Adding Search fields
    var txtFieldMaterialName = "<input type='text' id='txtdlgMaterialName' placeholder='Paper Name' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldGSM = "<input type='text' id='txtdlgGSM' placeholder='GSM' class='input-large search-query'/>&nbsp;&nbsp;";
    //var txtFieldBF= "<input type='text' id='txtdlgBF' placeholder='BF' class='input-large search-query'/>&nbsp;&nbsp;";

    $("#dlgMaterialSearchFields").append(txtFieldMaterialName);
    //$("#dlgMaterialSearchFields").append(txtFieldGSM);
    //$("#dlgMaterialSearchFields").append(txtFieldBF);

    $('#MaterialSearchContainer').jtable({
        title: 'Material List',
        paging: true,
        pageSize: 5,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Paper/PaperListByFiter'
        },

        recordsLoaded: function (event, data) {
            $('.jtable-data-row').click(function () {
                var row_id = $(this).attr('data-record-key');
                //alert(row_id.GSM);
                //alert(row_id.BF);
                //var rown = $(this).Records.BF;

                //$('#cmdMaterialDone').click();
                var GsmVal = $(this).data("record").GSM;
                var BfVal = $(this).data("record").BF;
                var DeckVal = $(this).data("record").Deckle;
                var ColorVal = $(this).data("record").PaperColor;
                var PkMat = $(this).data("record").Pk_Material;
                var RateVal = $(this).data("record").lblrate;
                //var PkMat = $(this).data("record").Pk_Material;
                //alert(GsmVal +"/"+ BfVal +"/" + PkMat);
                var rows = $('#MaterialSearchContainer').jtable('selectedRows');
                ////DataDisp();
                //$("#OGSM").val = row_id.GSM;
                //$("#OBF").val = row_id.BF;
                //document.getElementById('OGSM').value = GsmVal;
                //document.getElementById('OBF').value = BfVal;
                //document.getElementById('OPaper').value = PkMat;
                //document.getElementById('OColor').value = ColorVal;
                //var WtVal = STitleVal;
                //document.getElementById('OWeight').value = WtVal;
                //     document.getElementById('ORate').value = GsmVal;

                $("#OGSM").val(rows[0].data.GSM).trigger('change');
                $("#OBF").val(rows[0].data.BF).trigger('change');
                $("#OBF").val(rows[0].data.BF).trigger('change');
                $("#OD").val(rows[0].data.Deckle).trigger('change');

                $("#OColor").val(rows[0].data.PaperColor).trigger('change');
                $("#OPaper").val(rows[0].data.Pk_Material).trigger('change');
                $("#OWeight").val(rows[0].data.Weight).trigger('change');

                //document.getElementById('OR').value = $("#ORate").val(rows[0].data.rate);
                $("#ORate").val(rows[0].data.rate).trigger('change');

                //alert("Enter the TakeUpFactor Value");

                //$("#ORate").focus();
            });
        },

        fields: {
            Pk_Material: {
                title: 'M.Id',
                key: true
            },
            Name: {
                title: 'Mat.Name',
                edit: false,
                width: '30%'
            },

            GSM: {
                title: 'GSM',
                edit: false
            },
            BF: {
                title: 'BF',
                edit: false

            },
            Deckle: {
                title: 'Deck',
                edit: false

            },
            PaperColor: {
                title: 'Color',
                edit: false
            },
            Mill: {
                title: 'Mill',
                width: '95%'

            },
            Weight: {
                title: 'Weight',
                list: false
            },
        }

    });

    $('#MaterialSearchContainer').jtable('load', {
        //Name: $('#txtdlgMaterialName').val(),
        GSM: $('#OGSM').val(),
        BF: $('#OBF').val(),
        Deckle: $('#OD').val(),
        Fk_Color: $('#OColor').val(),
        //txtdlgMaterialName.
    });


    //$('#LoadRecordsButton').click();

    //$('#cmdMaterialSearch').click(function (e) {
    //    e.preventDefault();
    //    $('#MaterialSearchContainer').jtable('load', {
    //        //Name:$('#txtMaterial').val(),
    //        //Name: $('#txtdlgMaterialName').val(),
    //        GSM: $('#OGSM').val(),
    //        BF: $('#OBF').val()
    //    });
    //});



    //$('#cmdMaterialDone').click(function (e) {
    //    e.preventDefault();
    //    //alert(i);
    //    var rows = $('#MaterialSearchContainer').jtable('selectedRows');

    //    $('#txtFk_Material').wgReferenceField("setData", rows[0].keyValue);


    //$("#OGSM").val(rows[0].data.GSM).trigger('change');
    //$("#OBF").val(rows[0].data.BF).trigger('change');
    //$("#OColor").val(rows[0].data.PaperColor).trigger('change');
    //$("#OPaper").val(rows[0].data.Pk_Material).trigger('change');
    //$("#OWeight").val(rows[0].data.Weight).trigger('change');
    //$("#ORate").val(rows[0].data.rate).trigger('change');

    //    //$("#dataDialog").modal("hide");

    //});
}
