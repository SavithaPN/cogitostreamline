﻿var Estimation = new Estimation();
var BoxSpe;
var boxType;
var bLoadFromModel = false;
var prtSequence = -1;
var PartName;
var STitleVal;
var SPartCount=0;
var j = 0;
//  //////////////   ////////// in the backend, in item_layers table , takeup factor value is stored in rate field b-coz rate will not be entered in the boxmaster  ////  rate will come into picture 
//only in Estimation and then in quotation

$(document).ready(function () {

    $('#cmdPublish').click(function (e) {
        STitleVal = "";
        e.preventDefault();
        $("#Basic").removeClass("active");
        $("#Details").addClass("active");
        $("#pivot").pivot(1);

        createFormParts();

        //alert(SPartCount);
    });

    $('#cmdNext').click(function (e) {
        e.preventDefault();
        //$("#pivot").pivot(1);
        window.open("/SampBoxMast");
    });

    $(".ckhPart").click(function (e) {
        if (e.currentTarget.checked) {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](1);
            SPartCount = SPartCount + 1;
        }
        else {
            var stxtBox = $(e.currentTarget).val();
            Estimation[stxtBox](0);
        }
    });
    //ko.cleanNode("shellSelection");
    ko.applyBindings(Estimation, document.getElementById("shellSelection"));

    var BoxNumber = _util.getParameterByName("SampBoxMast");
    var IfEdit = BoxNumber.substr(BoxNumber.length - 4, BoxNumber.length)

    if (IfEdit == "Edit") {
        Estimation.Fk_BoxID = BoxNumber;
        if (Estimation.fetch()) {
            bLoadFromModel = true;
            EditTrue = true;
            createFormParts();
            $("#Basic").removeClass("active");
            $("#Details").addClass("active");
            $("#pivot").pivot(1);
        }
    }
    else {
        //BoxS.Fk_BoxID = BoxNumber;
        Estimation.Fk_BoxID = BoxNumber;
        if (Estimation.fetch()) {
            bLoadFromModel = true;
            createFormParts();
            $("#Basic").removeClass("active");
            $("#Details").addClass("active");
            $("#pivot").pivot(1);
        }
    }

    //_page.getViewByName('New').viewModel.addAddtionalDataSources("Ftype", "getFType", null);
    //_page.getViewByName('Edit').viewModel.addAddtionalDataSources("Ftype", "getFType", null);


});

function createFormParts() {
    
    $("#prtList").empty();

    //Check Outershell
    addPart($('#chkOuterShell')[0].checked, $('#txtOuterShell').val(), "Box", true, false, false);

    //Check Plate
    addPart($('#chkPlate')[0].checked, $('#txtPlate').val(), "Plate", false, false, true);

    //Check LengthPartation
    addPart($('#chkLengthPartation')[0].checked, $('#txtLPartation').val(), "Length Partation", false, false, false);

    //Check WidthPartation
    addPart($('#chkWidthPartation')[0].checked, $('#txtWidthPartation').val(), "Width Partation", false, false, false);

    //Check Cap
    addPart($('#chkCap')[0].checked, $('#txtCap').val(), "Cap", false, true);

   
    addPart($('#chkTop')[0].checked, $('#txtTop').val(), "Top", true, false, false);


    addPart($('#chkBott')[0].checked, $('#txtBott').val(), "Bottom", true, false, false);

    addPart($('#chkSleeve')[0].checked, $('#txtSleeve').val(), "Sleeve", true, false, false);

    //Now add one rate part to this
    addRatePart();

    //Load the existing model if available


    //$(this).find('.mainPartContainer').find(".smalltile").trigger('click');
}





function addRatePart() {
    var divId = "rateContainer";
    var rowMain = $("<tr></tr>");
    var colMain = $("<td></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight
    var lblTotalWeight = $("<label>Total Weight</label>");
    var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalWeight' type='text' placeholder='Weight' style='width: 70px' readonly />");
    var colTotalWeight = $("<td valign='bottom'></td>");

    //colTotalWeight.append(lblTotalWeight);
    //colTotalWeight.append(txtTotalWeight);
    //rowMain.append(colTotalWeight);

    //Create a row for column for total rate
    var lblTotalRate = $("<label>Total Price</label>");
    var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: totalPrice' type='text' placeholder='Price' style='width: 70px' readonly />");
    var colTotalRate = $("<td valign='bottom'></td>");

    //colTotalRate.append(lblTotalRate);
    //colTotalRate.append(txtTotalRate);
    //rowMain.append(colTotalRate);

    var control = $(colMain).slPart({
        viewTag: "SampBoxSpec",
        title: "",
        id: divId,
        estimationModel: Estimation,
        rate: true,
        rateChanged: rateChanged
    });

    var divId = "save";
    var rowMain = $("<tr id='saveBlock'></tr>");
    var colMain = $("<td colspan='3' align='center'></td>");

    rowMain.append(colMain);
    $("#prtList").append(rowMain);

    //Create a row for column for total weight

    //if (bLoadFromModel == true)
    //{ var cmdCreate = $("<button type='submit'class='btn cmdBack' data-bind='click: back' id='cmdBack' >Back</button>"); }
    //else


    if (bLoadFromModel == true && EditTrue == true) {
        var cmdCreate1 = $("<button type='submit' class='btn cmdBack' id='cmdBack' >Back</button>");
        var cmdCreate = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdCreate' >Save</button>");

    }
    else if (bLoadFromModel == false) {
        var cmdCreate1 = $("<button type='submit' class='btn cmdBack' id='cmdBack' >Back</button>");
        var cmdCreate = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdCreate' >Save</button>");

    }
    else {
        //var cmdCreate1 = $("<button type='submit' class='btn cmdBack' id='cmdBack' >Back</button>");
        //   var cmdCreate = $("<button type='submit' class='btn cmdCreate' data-bind='click: save' id='cmdCreate' >Save</button>");
        var cmdCreate = $("<button type='submit'class='btn cmdBack' data-bind='click: back' id='cmdBack' >Back</button>");
    }

    colMain.append(cmdCreate1);
    rowMain.append(colMain);

    colMain.append(cmdCreate);
    rowMain.append(colMain);

    //ko.cleanNode("saveBlock");
    ko.applyBindings(Estimation, document.getElementById("saveBlock"));
}


function addPart(bConsider, iValue, sTitle, outherShell, cap, plate,top,bottom,sleeve) {
    var j = 0;
    var localModel = null;
    if (bConsider) {

        for (j = 0; j < iValue; j++) {
            //let us sort out the model
            if (bLoadFromModel) {
                prtSequence++;
                localModel = Estimation.parts[prtSequence];
            }
            else {
                
                localModel = new part();
                Estimation.addParts(localModel);
            }
           // j = i;

            var divId = "prtContainer" + j;
            var rowMain = $("<tr></tr>");
            var colMain = $("<td></td>");

            rowMain.append(colMain);
            $("#prtList").append(rowMain);

            //Create a row for column for total weight
            var lblTotalWeight = $("<label>Wt.in Kgs</label>");
            //var txtTotalWeight = $("<input class='input-mini weight validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' style='width: 70px' readonly />");
            var txtTotalWeight = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: weight' type='text' placeholder='Weight' style='width: 70px' readonly />");
            var colTotalWeight = $("<td></td>");

            colTotalWeight.append(lblTotalWeight);
            colTotalWeight.append(txtTotalWeight);
            rowMain.append(colTotalWeight);

            //Create a row for column for total rate
            var lblTotalRate = $("<label>Total Rate</label>");
            //var txtTotalRate = $("<input class='input-mini rate validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' style='width: 70px' readonly />");
            var txtTotalRate = $("<input class='input-mini validate[funcCall[validateNumeric]]' data-bind='value: rate' type='text' placeholder='Rate' style='width: 70px' readonly />");
            var colTotalRate = $("<td></td>");

            //colTotalRate.append(lblTotalRate);
            //colTotalRate.append(txtTotalRate);
            //rowMain.append(colTotalRate);

            var sNewTitle = sTitle + "-" + (j + 1);

            STitleVal = STitleVal + sTitle + "`";
            //SPartCount = SPartCount + 1;
            Estimation.partsCopy = STitleVal;

            //alert(i);

            var control = $(colMain).slPart({
                viewTag: "SampBoxSpec",
                title: sNewTitle,
                id: divId,
                outershell: outherShell,
                cap: cap,
                plate: plate,
                top: top,
                bottom: bottom,
                sleeve: sleeve,
                //width: width,
                model: localModel,
                loadFromModel: bLoadFromModel,
                partChanged: calculatePartChanged,
                layerChanged: calculateLayerChanged
            });

            //j = j + 1;
            ////alert(j);
            //if (i != j)
            //{ i=j}
        }
    }

}

function calculatePartChanged(mainModel, model, outerShell, cap, plate, top, bottom, sleeve) {

    var valTitle = sleeve;
    var n = valTitle.search("-");
    PartName = valTitle.substring(0, n);
   // alert(STitleVal);---working fine

   // alert(PartName); ---working fine


    //alert(SPartCount);
    //var StrLen = STitleVal.length;
    //var findStr = "--";
    //var lastIndex=0;
    //var count = 0;
    //while (lastIndex != -1) {

    //    lastIndex = STitleVal.search(findStr, lastIndex)
    //    if (lastIndex != -1) {
    //        count++;
    //        lastIndex += findStr.length();
    //    }
    //}
    //alert (count);

       
    


    //alert(PartName);
    if (PartName == 'Box') {
        outerShellCalculation(mainModel)
    }
   
    else if (PartName == 'Top') {
        plateCapCalculation(mainModel);
    //else if (cap || plate || top || bottom) {
        //plateCapCalculation(mainModel);
    }
    else if (PartName == 'Plate') {
        plateCapCalculation(mainModel);
    }
    else if (PartName == 'Cap') {
        plateCapCalculation(mainModel)
    }
    else if (PartName == 'Bottom') {
        outerShellCalculation(mainModel)
    }
    else if (PartName == 'Sleeve') {
        outerShellCalculation(mainModel)
    }
    
    else {
        partationCalculation(mainModel);
    }

 //   rateChanged(Estimation);
}

function calculateLayerChanged(mainModel, model, outerShell, cap, plate, top, bottom, sleeve, stitle) {


    //alert(STitleVal);  -----working fine
    var valTitle = stitle;
    var n = valTitle.search("-");
    PartName = valTitle.substring(0, n);


    ////alert(PartName);
    if (PartName == 'Box' || PartName == 'Sleeve') {
        outerShellCalculation(mainModel)
    }
    else if (PartName == 'Top') {
        plateCapCalculation(mainModel);
        //else if (cap || plate || top || bottom) {
        //plateCapCalculation(mainModel);
    }
    else if (PartName == 'Plate') {
        plateCapCalculation(mainModel)
    }
    else if (PartName == 'Cap') {
        plateCapCalculation(mainModel)
    }
    else if (PartName == 'Bottom') {
        outerShellCalculation(mainModel)
    }
    else if (PartName == 'Sleeve') {
        outerShellCalculation(mainModel)
    }
    else {
        partationCalculation(mainModel);
    }

   // rateChanged(Estimation);
}


//Outershell
function outerShellCalculation(model) {
    var length = model.length();
    var width = model.width();
    var height = model.height();
    var rate = model.rate();
    //var tk_factor = model.TakeUpFactor();
    var noBoards = 1//model.noBoards();
    var PName = STitleVal;
    ////Board Size
    //var boardArea = ((Number(length) * Number(width) * 4) + Number(50)) / 1000;
    //model.boardArea(boardArea);

    //document.getElementById('ORate').value = 1;
    ////Deckle
    //var deckel = Math.round((Number(width) + Number(height) + 20) / 100) * 100;
    //model.deckle(deckel);
    var bstr = _util.getParameterByName("SampBoxMast");
    //var strlen=length(
    var bstr1 = bstr.search("Type");
    var bstr2 = bstr.substring(bstr1 + 6, 3);
    var bstr3 = bstr2.search("=");
    //var bstr4 = bstr2.substring(bstr3,7);
    var bstr4 = bstr2.length;
    var boxType = bstr2.substring((bstr3 + 1), bstr4);
    var btypeval = bstr.search("=");
    //boxType = bstr.substr(btypeval + 1);
    //alert(boxType);

    if (bstr1 > 0) {

        if (PartName == 'Box') {
            var TLength = (Number(length) * 2 + Number(width) * 2) + Number(50);    /////////=cut length
            var deckel = ((Number(width) + Number(height) + 20) / 10);
            var boardArea = (((TLength * deckel) / 100000));
            model.boardArea(boardArea);
            model.deckle(deckel);


          ///////////no of ups

            //var TF = Math.floor(132 / Number(deckel)); /////// field takeupfactor--- screen name no.of ups
            var TF = 1;
            model.TakeUpfactor(TF);
            ///////////////no of ups

            //Cuttingsize

            var cuttingsize = Math.round(TLength / 10);

            model.cuttingSize(cuttingsize);
            ///////////////cutting size

            ///////////////////
            var addl = ((TLength * deckel) / 1000000);

            var GSM = model.OLength1();
            var TkF = model.OHeight1();
           
            var CWeight =(addl * Number(TkF) * Number(GSM))/100;

            model.AddBLength(CWeight);
            ///////////////////

            /////////////////////  board sizes  === CALC. Deckel

            //var BSize = (((Number(width) + Number(height)) * TF + 20) / 10);   ///////stored in Rate field of itempartproperty
            
            model.Rate(deckel);
            ////////////////// board sizes
                   

            var i = 0;
           
            var weight = 0.000;
            var BBSVal = 0;
            var BGSMVal = 0;
            var Rrate = 0;
            while (model.layers[i]) {

                if (i % 2 != 0) {
                    //alert(model.layers[i].rate());
                    var layerWeight = addl * model.layers[i].rate() * model.layers[i].gsm();    //////////////////rate is takeup factor value
                    weight = weight + layerWeight;
                    BBSVal = BBSVal + model.layers[i].gsm() * model.layers[i].bf() / 1000 / 2
                    BGSMVal = BGSMVal + Number(model.layers[i].gsm())* Number(model.layers[i].rate());
                
                }
                else {
                    var layerWeight = addl * model.layers[i].gsm();
                    weight = weight + layerWeight;                   
                    BBSVal = BBSVal + model.layers[i].gsm() * model.layers[i].bf() / 1000
                    BGSMVal = BGSMVal + Number(model.layers[i].gsm());
                
                }

             
              
                i++;
            }
      
            //alert("BGS-"+BGSMVal); to be tested
            model.BoardGSM(BGSMVal);
            model.BoardBS(BBSVal);
            weight = (weight / 1000);
            PName = STitleVal;
            model.weight(weight);
            model.PName(PName);
            document.getElementById('PartsStr').value = STitleVal;
        
        }
        else if (PartName == 'Sleeve')
        {
            var cutLength = (((Number(length) * 2 + Number(width) * 2) + Number(50)) / 10);

            //BoardArea

            //Deckle
            var deckel = Math.round((Number(height * 3) + 20) / 10);
            model.deckle(deckel);

            var boardArea = (Number(cutLength / 100) * Number(deckel / 100));
            model.boardArea(boardArea);

            model.cuttingSize(cutLength);



            var i = 0;

            var weight = 0;
            var flutedParameter = 0;
            var nonFlutedParameter = 0;

            while (model.layers[i]) {

                if (i % 2 != 0) {
                    //fluted
                    var layerWeight = boardArea * model.layers[i].rate() * model.layers[i].gsm();     //////////////////rate is takeup factor value
                    weight = weight + layerWeight;
                }
                else {
                    var layerWeight = boardArea * model.layers[i].gsm();
                    weight = weight + layerWeight;
                }

                i++;
            }
           
            weight = (weight / 1000);

            model.weight(weight);
            model.PName(STitleVal);
        }


  
    }
    else {
        weight = 0;

        model.weight(weight);

    }
}



//partationcalculation
function partationCalculation(model) {



    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();
    //var tk_factor = model.tk_factor();
 
    if (PartName == 'Width Partation')
    { var cutLength = Math.round((Number(length * 2) + 20) / 10); }
    else
    {
        var cutLength = Math.round((Number(length * 3) + 20) / 10);
    }
    //BoardArea
   
    //Deckle
    var deckel = Math.round((Number(width*3) + 20) / 10) ;
    model.deckle(deckel);

    var boardArea = (Number(cutLength / 100) * Number(deckel/100));
    model.boardArea(boardArea);

    /////////////////no.of ups calc

    var TF = 182 / Number(deckel);

    model.TakeUpfactor(TF);

    /////////////////no.of ups calc

    //Cuttingsize
        model.cuttingSize(cutLength);
    //Cuttingsize


        var addl = ((boardArea * deckel) / 1000000);


        var GSM = model.OLength1();
        var TkF = model.OHeight1();

        var CWeight = addl * Number(TkF) * Number(GSM);

        model.AddBLength(CWeight);



    /////////////////
    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = addl * model.layers[i].rate() * model.layers[i].gsm();   //////////////////rate is takeup factor value
            flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = addl * model.layers[i].gsm();
            nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    var flutedWeight = cutLength * deckel * flutedParameter * quantity;
    var nonflutedWeight = cutLength * deckel * nonFlutedParameter * quantity;

    weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    model.weight(weight);
    model.PName (STitleVal);
}



//platecalculation
function plateCapCalculation(model) {

    //cutLength = Math.Round(Convert.ToDecimal(oPart.Length) + Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(50));
    //deckel = (Convert.ToDecimal(oPart.Width) * 3 + 20);
    //boardArea = (Convert.ToDecimal(cutLength / 100 * deckel / 100));

    //cuttingsize = Math.Round(Convert.ToDecimal(oPart.Length) + Convert.ToDecimal(oPart.Width) + Convert.ToDecimal(50));
    //var addl = Math.Round(Convert.ToDecimal(oPart.Length) * Convert.ToDecimal(oPart.Width), 2);


    var length = model.length();
    var width = model.width();
    var quantity = model.quantity();

    var TLength = (Number(length) + Number(width)) + Number(50);    /////////=cut length
    var deckel = ((Number(width) * 3) + 20);
    var boardArea = (((TLength/100 * deckel/100) ));
 
    model.deckle(deckel);
    model.boardArea(boardArea);

    //Deckle
    //var deckel = Math.round((Number(width) + 20) / 100) * 100;
    //model.deckle(deckel);



    var TF = 182 / Number(deckel);

    model.TakeUpfactor(TF);



    //Cuttingsize
    var cuttingSize = TLength;
    model.cuttingSize(cuttingSize);


    var addl = ((length* width) / 1000000);
    ///////////////////////////
    var GSM = model.OLength1();
    var TkF = model.OHeight1();

    var CWeight = addl * Number(TkF) * Number(GSM);

    model.AddBLength(CWeight);

    //var GSM = model.OLength1();
    //var TkF = model.OHeight1();

    //var CWeight = addl * Number(TkF) * Number(GSM);

    //model.AddBLength(CWeight);
    ///////////////////
    var i = 0;

    var weight = 0;
    var flutedParameter = 0;
    var nonFlutedParameter = 0;

    var weight = 0.000;
    var BBSVal = 0;
    var BGSMVal = 0;
    var Rrate = 0;

    while (model.layers[i]) {

        if (i % 2 != 0) {
            //fluted
            var layerWeight = addl * model.layers[i].rate() * model.layers[i].gsm();     //////////////////rate is takeup factor value
            weight = weight + layerWeight;
            BBSVal = BBSVal + model.layers[i].gsm() * model.layers[i].bf() / 1000 / 2
            BGSMVal = BGSMVal + Number(model.layers[i].gsm()) * Number(model.layers[i].rate());

            //flutedParameter = flutedParameter + layerWeight;
        }
        else {
            var layerWeight = addl * model.layers[i].gsm();
           
            weight = weight + layerWeight;
            BBSVal = BBSVal + model.layers[i].gsm() * model.layers[i].bf() / 1000
            BGSMVal = BGSMVal + Number(model.layers[i].gsm());
          //  nonFlutedParameter = nonFlutedParameter + layerWeight;
        }

        i++;
    }
    model.BoardGSM(BGSMVal);
    model.BoardBS(BBSVal);
    weight = ((weight / 1000 )*quantity);
    PName = STitleVal;
    model.weight(weight);
    model.PName(PName);
    document.getElementById('PartsStr').value = STitleVal;


    //var flutedWeight = cuttingSize * deckel * flutedParameter * quantity;
    //var nonflutedWeight = cuttingSize * deckel * nonFlutedParameter * quantity;

    //weight = ((flutedWeight + nonflutedWeight) / 1000).toFixed(3);
    //model.weight(weight);

    //model.PName(STitleVal);
}

//Final Rate calculation
function rateChanged(estModel) {
    var i = 0;
    var totalWeight = 0;

    while (estModel.parts[i]) {
        totalWeight = Number(totalWeight) + Number(estModel.parts[i].weight());
        i++;
    }

    estModel.totalWeight(totalWeight);

    //Now rate calculation
    //Declaring variables

    var convRate = estModel.convRate();
    var convValue = 0;
    var gMarginPercentage = estModel.gMarginPercentage();
    var gMarginValue = 0;
    var taxesPercntage = estModel.taxesPercntage();
    var taxesValue = 0;
    var transportValue = estModel.transportValue();
    var weightHValue = estModel.weightHValue();
    var handlingChanrgesValue = estModel.handlingChanrgesValue();
    var packingChargesValue = estModel.packingChargesValue();
    var rejectionPercentage = estModel.rejectionPercentage();
    var rejectionValue = 0;
    var totalPrice = 0;

    //Calculations
    convValue = (convRate * totalWeight) / 100;
    gMarginValue = (convValue * gMarginPercentage) / 100;
    taxesValue = (convValue * taxesPercntage) / 100;
    rejectionValue = (convValue * rejectionPercentage) / 100;

    estModel.convValue(convValue);
    estModel.gMarginValue(gMarginValue);
    estModel.taxesValue(taxesValue);
    estModel.rejectionValue(rejectionValue);

    totalPrice = (Number(convValue) +
                  Number(gMarginValue) +
                  Number(taxesValue) +
                  Number(transportValue) +
                  Number(weightHValue) +
                  Number(handlingChanrgesValue) +
                  Number(packingChargesValue) +
                  Number(rejectionValue));

    estModel.totalPrice(totalPrice);
}



$('#cmdBack').click(function (e) {
    e.preventDefault();
    //$("#pivot").pivot(1);
    _page.showView('Search');
});

