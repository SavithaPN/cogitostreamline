﻿var objShortCut = null;
function initialCRUDLoad() {
    $('#MainSearchContainer').jtable({
        title: 'Machine Maintenance  List',
        paging: true,
        pageSize: 10,
        sorting: false,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/MachineMaintenance/MachineMaintenanceListByFiter',
            updateAction: '',
            createAction: '',
            deleteAction: ''
        },
        fields: {
            Pk_MachineMaintenance: {
                title: 'Id',
                key: true

            },
            Fk_Machine: {
                title: 'Name',
                key: true

            },
            FromDate: {
                title: 'FromDate',
                width: '25%'
            },
            ToDate: {
                title: 'ToDate',
                width: '25%',
            }
        }
    });
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_MachineMaintenance: $('#txtMaintenanceID').val(),
            FromDate: $('#txtFromDate').val(),
            ToDate: $('#txtToDate').val()
        });
    });
    $('#LoadRecordsButton').click();
    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });
    _page.getViewByName('New').viewModel.addAddtionalDataSources("Machine", "getMachine", null);
    _page.getViewByName('Edit').viewModel.addAddtionalDataSources("Machine", "getMachine", null);
}

//function CheckStateDuplicate() {
//    _comLayer.parameters.clear();
//    var oResult;
//    _comLayer.parameters.add("StateName", $('#StateName').val());
//    oResult = _comLayer.executeSyncAction("State/StateDuplicateChecking", _comLayer.parameters);
//    if (oResult.TotalRecordCount > 0)
//        return "* " + "State Already added";
//}

