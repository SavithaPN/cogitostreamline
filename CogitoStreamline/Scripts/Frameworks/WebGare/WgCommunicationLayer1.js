function WgCommunicationLayer() {
    this.parameters = new WgParameters();

    this.buildURL = function (pName, pTag, pAction) {
        var sPath = this._appendToPath(pName, true)
								 + this._appendToPath(pTag, true)
								 + this._appendToPath(pAction, true);

        return sPath;
    }

    this._appendToPath = function (pAttr, pGivePathLinker) {
        if (pAttr != null && typeof pAttr != 'undefined') {
            return (pGivePathLinker) ? "/" + pAttr : pAttr;
        }
        else { return ""; }
    }

    this.curServer = function () {
        return location.protocol + "//" + location.host;
    }

    this.executeAction = function (Url, parms, callBack) {

        if (parms == null) parms = this.parameters;

        var valuesReturned = null;
        $.ajax({
            type: "POST",
            async: false,
            url: Url,
            dataType: "json",
            data: { data: parms.getJSON() },
            success: function (data) {
                valuesReturned = data;
                callBack(data);
            },
            complete: function () { },

            error: function (req, status, error) {
                return error;
            }
        });

        return valuesReturned;

    }

    this.executeSyncAction = function (Url, parms) {
        //alert(parms.getJSON());
        if (parms == null) parms = this.parameters;
        //var jsonData = parms.getJSON();
      
        var valuesReturned = null;
        $.ajax({

            type: "POST",
            //async: true,
            contentType: 'application/json',
            url: Url,
            dataType: "json",
            //data: JSON.stringify(jsonData),
            data: { data: parms.getJSON() },
            //data: JSON.stringify({ data: parms.getJSON() }),
            success: function (data) {
                valuesReturned = data;
            },
            //complete: function () { },

            error: OnError
        
        });
        function OnError(xhr, errorType, exception) {
            var responseText;
            
            try {
                responseText = jQuery.parseJSON(xhr.responseText);
              
            } catch (e) {
                responseText = xhr.responseText;

                var str = parms.getJSON();
                var strLen = parms.getJSON().length;
                var a1 = str.indexOf("}]")


                //alert(responseText);
                //alert(parms.jsonString);
            }
       
        }
        return valuesReturned;

    }

    this.getPartialLayout = function (Url) {

        var valuesReturned = null;
        $.ajax({
            async: false,
            url: Url,
            dataType: "html",
            success: function (data) {
                valuesReturned = data;
            },
            complete: function () { },

            error: function (req, status, error) {
                return error;
            }
        });

        return valuesReturned;
    }
}

function WgParameters() {
    this.jsonString = null;

    this._parameterObject = new Object();

    this.add = function (pParamName, pParamValue) {
        this._parameterObject[pParamName] = pParamValue;
    };

    this.clear = function () {
        this._parameterObject = new Object();
        this.jsonString = null;
    };

    this.getJSON = function () {
        if (this.jsonString == null)
        { return JSON.stringify(this._parameterObject); }
        else
        { return this.jsonString; }

    };

    this.setJSON = function (json) {
        this.jsonString = json;
    };
}