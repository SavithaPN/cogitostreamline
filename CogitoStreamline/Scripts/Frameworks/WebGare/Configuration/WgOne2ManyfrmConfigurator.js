﻿var objContext = null;
var objContextGlobal = null;
var objContextEdit = false;
var objContextEditValue = false;
var frmTarget = null;
var resetParameters = {
    p_viewObject: null,
    p_tag: null,
    p_subViewName: null,
    p_property: null,
    p_bntSave: null,
    p_tlbGrid: null,
    p_objContextFunction:null
};


function configureOne2Many(bntAddItem, tlbGrid, bntSave, viewObject, property, tag, subViewName, p_objContext, p_frmTraget) {
    that = viewObject;

    frmTarget = p_frmTraget;

    resetParameters.p_viewObject = viewObject;
    resetParameters.p_tag = tag;
    resetParameters.p_subViewName = subViewName;
    resetParameters.p_property = property;
    resetParameters.p_bntSave = bntSave;
    resetParameters.p_tlbGrid = tlbGrid;
    resetParameters.p_objContextFunction = p_objContext;

    objContextEdit = false;
    objContext = p_objContext();
    ResetFormFirst(objContext, resetParameters.p_viewObject, resetParameters.p_tag, resetParameters.p_subViewName,
        resetParameters.p_property, resetParameters.p_bntSave, resetParameters.p_tlbGrid);
    
    
    if (typeof (viewObject.viewModel.data[property]) == "undefined" || viewObject.viewModel.data[property]() == "")
    { viewObject.viewModel.data[property] = ko.observableArray(); }

     $(tlbGrid).jtable('subscribeHook', "onEditRecord", function (params) {
        objContextEdit = true;
        objContext = viewObject.viewModel.data[property]()[params.keyValue];

        var sBeforeEditMessage = null;

        try{
            sBeforeEditMessage = beforeOneToManyEditHook(objContext);
        }
        catch(e) {}

        if (sBeforeEditMessage ==null) {
            objContextEditValue = viewObject.viewModel.data[property]()[params.keyValue];
            objContextEdit = true;
            objContext = objContextEditValue;
            ResetForm(objContextEditValue, viewObject, tag, subViewName, property, bntSave, tlbGrid);
        }
        else {
            alert(sBeforeEditMessage);
        }
    });

    $(tlbGrid).jtable('subscribeHook', "onDeleteRecord", function (params) {

        alertify.confirm("Are you sure you want to delete?", function (e) {
            if (e) {

                var objContext = viewObject.viewModel.data[property]()[params.keyValue];

                var sBeforeDeleteMessage = null;

                try {
                    sBeforeDeleteMessage = beforeOneToManyDeleteHook(objContext);
                }
                catch (e) { }

                if (sBeforeDeleteMessage == null) {
                    viewObject.viewModel.data[property].remove(objContext);

                    $(tlbGrid).jtable('load', {
                        data: ko.toJSON(that.viewModel.data[property]())
                    });

                }
                else {
                    if (sBeforeDeleteMessage == true) {
                        viewObject.viewModel.data[property].remove(objContext);

                        $(tlbGrid).jtable('load', {
                            data: ko.toJSON(that.viewModel.data[property]())
                        });
                    }
                }
            }
            else {

            }
        });

    });


    $(tlbGrid).jtable('load', {
        data: ko.toJSON(viewObject.viewModel.data[property]())
    });
}

function onShotcutShow(viewObject, property, saveButton, tlbGrid, objContext) {

    if (typeof (that.viewModel.data[property]) == "undefined")
    {
        that.viewModel.data[property] = ko.observableArray();
    }


    $(saveButton).click(function (e) {
        e.preventDefault();

        var bSaveRow = true;
        try {
            bSaveRow = beforeOneToManySaveHook(objContext);
        }
        catch (e) { }

        if (bSaveRow) {

            if (that.viewModel.data[property]().indexOf(objContext) < 0 && !objContextEdit) {

                that.viewModel.data[property].push(objContext);
            }

            try {
                if (objContextEdit) objContextEditValue.updateValues();
                objContext.updateValues();
            } catch (e) { }

            $(tlbGrid).jtable('load', {
                data: ko.toJSON(that.viewModel.data[property]())
            });

            try
            {
                resetOneToManyForm(property);
            }
            catch (e)
            { }
            objContextEdit = false;
            objContext = resetParameters.p_objContextFunction();
            ResetForm(objContext, resetParameters.p_viewObject, resetParameters.p_tag,
            resetParameters.p_subViewName, resetParameters.p_property, resetParameters.p_bntSave, resetParameters.p_tlbGrid);
        }
    });
}

function ResetForm(objContext, viewObject, tag, subViewName, property, savebutton, tlbGrid) {
    
    try {
        beforeOneToManyDataBind(property);
    }
    catch (e) { }
    try
    {
        ko.cleanNode(document.getElementById(frmTarget));
        ko.applyBindings(objContext, document.getElementById(frmTarget));
    }
    catch (e) {
        console.log(e.message);
    }
   
    try {
        afterOneToManyDialogShow(property);
    }
    catch (e) {
        console.log(e.message);
    }

}

function ResetFormFirst(objContext, viewObject, tag, subViewName, property, savebutton, tlbGrid) {

    try {
        beforeOneToManyDataBind(property);
    }
    catch (e) { }
    try {
        ko.cleanNode(document.getElementById(frmTarget));
        ko.applyBindings(objContext, document.getElementById(frmTarget));
    }
    catch (e) {
        console.log(e.message);
    }

    onShotcutShow(viewObject, property, savebutton, tlbGrid, objContext);

    try {
        afterOneToManyDialogShow(property);
    }
    catch (e) {
        console.log(e.message);
    }
}



