﻿function CRUDConfiguration() {

    _config = this;
    var currResult = null;
    var searchViewAsDefault = true;

    this.configure = function (pageTag) {

        _page.subscribeHook("afterShowView", this.afterShowView);

        _util.fireSubscribedHooks(_page.hookSubscribers, 'startConfiguration', { type: 'CRUD' });

        var oSearch = new WgView("Search", pageTag, "divSearchArea", {}, { reloadOnShow: false });
        var oNew = new WgView("New", pageTag, "divDataArea", {}, null);
        var oEdit = new WgView("Edit", pageTag, "divDataArea", {}, { reloadOnShow: true, context: 'Edit' }); //Edit and Delete Same view

        oNew.dependentViews.push({ action: 'hide', view: oSearch });
        oEdit.dependentViews.push({ action: 'hide', view: oSearch });

        oSearch.dependentViews.push({ action: 'hide', view: oNew });
        oSearch.dependentViews.push({ action: 'hide', view: oEdit });

        oNew.viewModel.subscribeHook('beforeModelSave', this.beforeModelSave);
        oNew.viewModel.subscribeHook('afterModelSave', this.afterModelSave);

        oEdit.viewModel.subscribeHook('beforeModelSave', this.beforeModelSave);
        oEdit.viewModel.subscribeHook('afterModelSave', this.afterModelSave);
        oEdit.viewModel.subscribeHook('beforeModelDelete', this.beforeModelDelete);
        oEdit.viewModel.subscribeHook('afterModelDelete', this.afterModelDelete);

        oNew.viewModel.modelName = null;
        oEdit.viewModel.modelName = null;
        oSearch.viewModel.modelName = null;

        _page.addView(oSearch);
        _page.addView(oNew);
        _page.addView(oEdit);

        try {
            searchViewAsDefault = showSearchViewAsDefault();
        }
        catch (e) { }

        if (searchViewAsDefault) {
            _page.showView(_page.views[0].viewName);
        }  

        //Simplified Hook for CRUD clients
        try {
            initialCRUDLoad();

            //Subscribe to Jtable hooks

            $('#MainSearchContainer').jtable('subscribeHook', "onAddingRecord", function () {
                _page.showView('New');
            });

            $('#MainSearchContainer').jtable('subscribeHook', "onEditRecord", function (params) {
                _page.getViewByName('Edit').viewModel.setId(params.keyValue);
                _page.getViewByName('Edit').options.context = 'Edit';
                _page.showView('Edit');
            });

            $('#MainSearchContainer').jtable('subscribeHook', "onDeleteRecord", function (params) {
                _page.getViewByName('Edit').viewModel.setId(params.keyValue);
                _page.getViewByName('Edit').options.context = 'Delete';
                _page.showView('Edit');
            });

            $('.search-query').bind("keypress", function (e) {

                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#LoadRecordsButton').click();
                }
            });


        } catch (e) { }
        
        _util.fireSubscribedHooks(_page.hookSubscribers, 'doneConfiguration', { type: 'CRUD' });
    };

    this.beforeModelDelete = function () {
       _util.showMessageDialog("", false);
       return true;
    };

   this.beforeModelSave = function () {
       try {
           beforeModelSaveEx();
       }
       catch (e)
        { }

       if ($("#frmHost").validationEngine('validate')) {
           _util.showMessageDialog("", false);
           return true;
       }
       else {
           return false;
       }
   };

    this.afterModelDelete = function (object) {
        _util.fireSubscribedHooks(_page.hookSubscribers, 'refreshData', { result: object.result });

        if (object.result.Success) {
            _util.showSuccess("Data deleted successfully", _config.showSearch);
        }
        else {
            _util.showError("Unable delete please contact administrator", _config.showSearch);
        }
    };


    this.afterModelSave = function (object) {
        _util.fireSubscribedHooks(_page.hookSubscribers, 'refreshData', { result: object.result });

        currResult = object.result;

        if (object.result.Success) {
            _util.showSuccess("Data saved successfully", _config.showSearch);
        }
        else {
            _util.showError("Unable saved please contact administrator", _config.showSearch);
        }

    };

    this.showSearch = function() {

        try {
            afterModelSaveEx(currResult);
        }
        catch (e)
        { }

        _page.showView('Search');
    }

    //Simplify hooks for CRUD
    this.afterShowView = function (pView) {

        $('#cmdBackSearch').click(function (e) {
            e.preventDefault();
            _page.showView('Search');
            $("#frmHost").validationEngine('hide');
        });

        $('#divDataArea').dblclick(function (e) {
            e.preventDefault();
            $('#frmHost').validationEngine('hide')
        });

        var viewObject = _page.getViewByName(pView.Name)
        if (pView.Name == "Search") {
            try {
                _page.getViewByName("New").clearBindings();
                _page.getViewByName("Edit").clearBindings();
                $('#MainSearchContainer').jtable('reload');
                afterSearchShow(viewObject);
                $("#frmHost").validationEngine('hide');
            }
            catch (e) { }

            return;
        }
        if (pView.Name == "New") {
            viewObject.dataBind();

            $("#frmHost").validationEngine();

            try {
                afterNewShow(viewObject);
            }
            catch (e) { }
            return;
        }
        if (pView.Name == "Edit") {

            _util.showMessageDialog("", false);

            if (viewObject.options.context == 'Delete') {
                $("#cmdCreate").text("Delete");
                $("#cmdCreate").attr("data-bind", "click: deleteRecord");
            }
           
            viewObject.dataBind();

            $("#frmHost").validationEngine();

            try {
                afterEditShow(viewObject);
            }
            catch (e) { }

            _util.closeMessageDialog();

            return;
        }

    }
}

