/*
 * Name        : Page Class
 * Description : Contains the page class definition, every page in the application will be instance of this class with following
 * 			     1. Communication 2. Data binding 3. Loading preferences
 * Author      : Supreet HVR
 * Date 	   : 02/09/2013
 * Modifications
 */
 var _page = null;
 var _comLayer = null;
 var _util = null;
 var g_isPartialViewSupported = true;

 $(document).ready(function () {

     //Create global objects for communication and Utilities
     _comLayer = new WgCommunicationLayer();
     _util = new WgUtilities();

     //set an Ajax pre-filter to make sure the calls are only fired at Bussness Objects
     $.ajaxPrefilter("json text", function (options) {
         //options.url = _comLayer.curServer() + "/controller" + options.url;
     });

     //Get the page type from the body
     var pageType = $('#divMainSection').attr('wgPageType');

     //Get the page type from the body
     var pageTag = $('#divMainSection').attr('wgTag');

     //Assign the _page variable to WebPage Object
     _page = new WgPage();

     try {
         beforePageSetup();
     } catch (e) { }

     _page.setUp(pageType, pageTag);
  

     $('#cmdCloseMessageDialog').click(function (e) {
         e.preventDefault();
         $('#messageDialog').modal('hide');
         _util.messageCallback();
     });

     try {
         onPageLoadComplete();
     } catch (e) { }

 });
 
 /* Constructor Parameters 
 * PageType - provides the type of the page to enable the correct functionalities to load 
 * 
 * */
function WgPage()
{
	
	this.hookSubscribers = new Array();
	
    this.CustomConfiguration = null;

	this.setUp = function (pageType, pageTag) {
	    //Handling Views
	    this.views = [];

	    var configurator = new configurationFactory().getContiguration(pageType);
	    if (configurator != null) {
	        configurator.configure(pageTag);
	    }
	};

	this.addView = function(pView)
	{
		this.views.push(pView);
	};
	
	this.showView = function(pViewName)
	{
		if(_util.fireSubscribedHooks(this.hookSubscribers,'beforeShowView',{Name:pViewName}))
    	{
			this.getViewByName(pViewName).render();
    	}
		
		_util.fireSubscribedHooks(this.hookSubscribers,'afterShowView',{Name:pViewName});
	};
	
	this.subscribeHook = function(pHookName,pCallbackFunction)
	{
		var hookSub = {Hook: pHookName, CallBack: pCallbackFunction};
		this.hookSubscribers.push(hookSub);
	};
	
	this.afterModelDelete=function(object){
		_util.fireSubscribedHooks(_page.hookSubscribers,'refreshData',{result:object.result});
	
		if(object.result.success){
			alertify.alert("data is deleted succesfully");
			_page.showView('Search');
		}
		else 
		{
			alertify.error("Unable to delete the Data ");
			_page.showView('Search');
		}
			
	};
	
	this.getViewByName = function(pViewName)
	{
		var i=0;
		
		while(this.views[i])
		{
			if(this.views[i].viewName == pViewName)
			{
				return this.views[i];
			}
			
			i++;
		}
	};
}

function configurationFactory() {

    this.getContiguration = function (pageType) {
        if (pageType == "CRUD") {
            return new CRUDConfiguration();
        }
        else if (pageType == "TEST" || pageType == "test") {
            return new TESTConfiguration();

        }
      
        return _page.CustomConfiguration;
    }
    
}
