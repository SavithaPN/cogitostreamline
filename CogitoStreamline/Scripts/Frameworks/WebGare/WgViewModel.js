function WgViewModel(pName,pId,pTag)
{
	this.modelName = pName;
	this.modelTag = pTag;
	this.id = pId;
	this.filters = null;
	
	this.hookSubscribers = new Array();
	
	
	
	//Basic data object this is will be bound to the controls using Knockout
	this.data = new Object(); 
	
	this.subscribeHook = function(pHookName,pCallbackFunction)
	{
		var hookSub = {Hook: pHookName, CallBack: pCallbackFunction}
		this.hookSubscribers.push(hookSub);
	};
	
	//Id to be set
	this.setId = function (pId)
	{
		this.id = pId;
	};
	
	//Parameters can be set for the model before the query
	this.setFilters = function (pFilters)
	{
		this.filters = pFilters;
	};
	
	//Parameters can be set for the model before the query
	this.clearFilters = function (pFilters)
	{
		this.filters = null;
	};
	
	//Fetch the data 
	this.addModelField = function(pFieldName)
	{
		this.data[pFieldName] = ko.observable("");	
	};
	
	//Fetch the data
	this.fetch = function () {
	    if (this.filters == null && (this.id == null || this.id == "")) return;
	    var oResult = this._getDataFromSource(this.filters);

	    if (oResult.success == true) {
	        for (var propt in oResult.data) {
	            var dat = oResult.data[propt];
	            var check = Object.prototype.toString.call(dat);
	            if (check != "[object Array]") {                        
	                this.data[propt] = ko.observable(oResult.data[propt]);
	            }
	            else {
	                this.data[propt] = ko.observableArray();

	                var i = 0;

	                while (oResult.data[propt][i]) {
	                    this.data[propt].push(oResult.data[propt][i]);
	                    i++;
	                }
	            }
	        }
	    }


	};
	
	this._getDataFromService = function(service,filters)
	{
		//if filters are set just go with the filters
		if(filters != null)
		{
			_comLayer.parameters = filters;
		}
		else
		{
			_comLayer.parameters.clear();
			_comLayer.parameters.add("Id",this.id);
		}
		
		var sPath = _comLayer.buildURL(this.modelName,this.modelTag,service);	
		var oResult = _comLayer.executeSyncAction(sPath,_comLayer.parameters);
		
		var oResultObject = oResult;
		
		return oResultObject;
		
	};
	
	
	this._getDataFromSource = function(filters)
	{
		//if filters are set just go with the filters
		if(filters != null)
		{
			_comLayer.parameters = filters;
		}
		else
		{
			_comLayer.parameters.clear();
			_comLayer.parameters.add("Id",this.id);
		}
		
		var sPath = _comLayer.buildURL(this.modelName,this.modelTag,"Load");	
		var oResult = _comLayer.executeSyncAction(sPath,_comLayer.parameters);
		
		var oResultObject = oResult;
		
		return oResultObject;
		
	};
	
	this.setObservableFields = function(pfields)
	{
		var i=0;
		
		for(i=0;i<pfields.length;i++)
		{
			try{
				this.data[pfields[i]] = ko.observable(this.data[pfields[i]]);
			}			
			catch(e) {} //just ignore for now
		}
			
	};
	
	this.refresh = function()
	{
		//get data from source based on filters (if set)
		var oResult = this._getDataFromSource(this.filters);
		
		//Loop through the fields and update the data in the model data

		if(oResult.success == true)
		{
			for(var propt in oResult.data){
			    try
			    {
			    	if(ko.isObservable(this.data[propt]))
			    	{
			    		this.data[propt](oResult.data[propt]);
			    	}
			    	else {this.data[propt] = oResult.data[propt];}
			    }
			    catch(e) {} //just ignore for now
			}
		}
	};
	
	this.clear = function()
	{
		for(var propt in this.data){
		    try
		    {
		    	if(ko.isObservable(this.data[propt]))
		    	{
		    		this.data[propt]("");
		    	}else {this.data[propt] = "";}
		    }
		    catch(e) {} //ignore
		}
	};

	this.save = function () {
	    if (_util.fireSubscribedHooks(this.hookSubscribers, 'beforeModelSave', { Name: this.modelName, Tag: this.modelTag })) {
	        var ToPass = ko.toJSON(this.data);
	        var sPath = "";

	        if (this.id == null) {
	            sPath = _comLayer.buildURL(this.modelName, this.modelTag, "Save");
	        }
	        else { sPath = _comLayer.buildURL(this.modelName, this.modelTag, "Update"); }

	        _comLayer.parameters.clear();
	        _comLayer.parameters.setJSON(ToPass);

	        if (this.modelTag == "MaterialIndent" || this.modelTag == "BoxMaster" || this.modelTag == "OuterShell" || this.modelTag == "BoxType" || this.modelTag == "SampBoxMast" || this.modelTag == "FType" || this.modelTag == "Cap" || this.modelTag == "LPartition" || this.modelTag == "WPartition" || this.modelTag == "Plate" || this.modelTag == "Country" || this.modelTag == "State" || this.modelTag == "City" || this.modelTag == "Customer" || this.modelTag == "Vendor" || this.modelTag == "Mill" || this.modelTag == "PaperType" || this.modelTag == "Paper" || this.modelTag == "Board" || this.modelTag == "Gum" || this.modelTag == "Ink" || this.modelTag == "HoneyComb" || this.modelTag == "Pin" || this.modelTag == "Color" || this.modelTag == "Enquiry" || this.modelTag == "Stationery" || this.modelTag == "ElectricalSpares" || this.modelTag == "Miscellaneous" || this.modelTag == "MechanicalSpares" || this.modelTag == "OrderApproval" || this.modelTag == "JCardApproval" || this.modelTag == "Order_Others") {
	            var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
	        }
	        else {
	            var oResult = _comLayer.executeSyncAction1(sPath, _comLayer.parameters);
	        }

	        _util.fireSubscribedHooks(this.hookSubscribers, 'afterModelSave', { result: oResult });

	        return oResult;

	    };


	};
    
    this.deleteRecord = function()
    {
    	if(_util.fireSubscribedHooks(this.hookSubscribers,'beforeModelDelete',{Name:this.modelName, Tag:this.modelTag}))
    	{
    		var ToPass = ko.toJSON(this.data);
    		var sPath = "";
    		    		
    		if(this.id != null)
    		{
    			sPath = _comLayer.buildURL(this.modelName,this.modelTag,"Delete");	
    			_comLayer.parameters.clear();
    			_comLayer.parameters.setJSON(ToPass);
    			
        		var oResult = _comLayer.executeSyncAction(sPath,_comLayer.parameters);
        		
        		_util.fireSubscribedHooks(this.hookSubscribers,'afterModelDelete',{result:oResult});
    		}

    	};
    };
    
    this.obliterate = function()
    {
    	this.data = null;
    	this.filters = null;
    };
    
    this.addAddtionalDataSources = function(pName,pService,pFilters)
    {
    	this[pName] = this._getDataFromService(pService,pFilters);
    };
}
