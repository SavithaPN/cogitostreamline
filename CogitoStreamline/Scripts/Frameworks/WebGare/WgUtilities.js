var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function WgUtilities() {
   this.messageCallback = function () { };
   this.GenerateHeader = function (bWithLogout)
	{
	   
	    var sPath = _comLayer.buildURL("CurrentUser",null,null);	
		var p_oLoginInfo = _comLayer.executeSyncAction(sPath,null);
	   
		var l_oHeaderDiv = document.getElementById("divPageHeader");
	
		var l_sHeaderString = "<div id='header'>";
		l_sHeaderString += "<div id='headertopleft'>";
		l_sHeaderString += "<img src='images/RoLogo.bmp' border='0' width='100' height='55'>";	
		l_sHeaderString += "</div>";
		l_sHeaderString += "<div id='headertopCenter'>";
		l_sHeaderString += "<div id='headerText'>";
		l_sHeaderString += "WEB Tooling";
		//headertowardsRight
		l_sHeaderString += "</div>";
		l_sHeaderString += "</div>";
		if(p_oLoginInfo != null && p_oLoginInfo.userName != null && p_oLoginInfo.schemaName != null)
		{
			l_sHeaderString += "<div id='headertowardsRight'>";
			l_sHeaderString += "<div id='loginInfo'>"+"Welcome: "+p_oLoginInfo.userName+"/"+
			p_oLoginInfo.schemaName;
			l_sHeaderString += "</div>";
			l_sHeaderString += "</div>";
		}
		l_sHeaderString += "<div id='headertopright'>";
		l_sHeaderString += "</div>";
		l_sHeaderString += "<div id='headermidright'>";
		l_sHeaderString += "<img src='images/bosch-logo.gif' border='0'>";
		l_sHeaderString += "</div>";
		l_sHeaderString += "<div id='headerbottomleft'></div>";
		l_sHeaderString += "</div>";
	
		if(bWithLogout==true)
		{
			l_sHeaderString += "<div id='headerbottomright'>";
			l_sHeaderString += "<table width='100%'style='position: absolute;top: 0px;left: 0px;' " +
			"border='0' cellpadding='0' cellspacing='0'>";
			l_sHeaderString += "<tr><td width='92%'>&nbsp;</td>";
			l_sHeaderString += "<td><a onclick='logout()' style='font-family: Verdana;font-size: 12px;" +
			"color: white;' href='#' class='myText'><b>Logout</b></a>";
			l_sHeaderString += "</td></tr></table>";
			l_sHeaderString += "</div>";
		}
	
		l_oHeaderDiv.innerHTML = l_sHeaderString;
	
	}
	
    this.fireGlobalHook = function(hook,data)
    {
    	if(hook) {return hook(data);}
    	else {return true};
    }
   
    this.fireSubscribedHooks = function(hooks,hookName,data)
    {
    	var i=0;
    	var finalResult = true;
    	
    	while(hooks[i])
		{
			if(hooks[i].Hook == hookName)
			{
				 
				finalResult = finalResult 
							  && hooks[i].CallBack(data);
			}
			
			i++;
		}
    	
    	return finalResult;
    }
    
	this.toggleDiv = function (p_sControlName)
	{
		var ele = document.getElementById(p_sControlName);
	
		if(ele.style.display == "none") {
			ele.style.display = "block";
		}
		else {
			ele.style.display = "none";
		}
	}

	this.setDivPosition =  function (p_sControlName,p_sPosition)
	{
		var ele = document.getElementById(p_sControlName);
		ele.style.display = p_sPosition; 	
        
	}

	this.selContains = function selContains(p_oControl,p_sValue)
	{
		for(var i=0;i<p_oControl.options.length;i++)
		{
			if(p_oControl.options[i].value==p_sValue)
				return true;
		}
	
		return false;
	}


	this.ConfirmDialog = function(p_sMeassage)
	{
		//to be implemented using alertify later
		//should accept HTML templates
	}


	this.moveUp = function (selectId) {
		var selectList = document.getElementById(selectId);
		var selectOptions = selectList.getElementsByTagName('option');
		for (var i = 1; i<selectOptions.length; i++) {
			var opt = selectOptions[i];
			if (opt.selected) {
				selectList.removeChild(opt);
				selectList.insertBefore(opt, selectOptions[i - 1]);
			}
		}
	}

	this.moveDown = function (selectId) {
		var selectList = document.getElementById(selectId);
		var selectOptions = selectList.getElementsByTagName('option');
		for (var i = selectOptions.length - 2; i>= 0; i--) {
			var opt = selectOptions[i];
			if (opt.selected) {
				var nextOpt = selectOptions[i + 1];
				opt = selectList.removeChild(opt);
				nextOpt = selectList.replaceChild(opt, nextOpt);
				selectList.insertBefore(nextOpt, opt);
			}
		}
	}

	/**
	 * Adding enter key event
	 */
	this.enterKeyEvent = function(functionToBecalled,event){
		if (event.keyCode == 13) {
			functionToBecalled();
		}
	}
	
	/**
	 * Set value to combo
	 * @param comboId
	 * @param comboVal
	 */
	this.setCombo = function (comboId, comboVal)
	{
		var combo = document.getElementById(comboId);
		combo.value = comboVal;
	}

	/**
	 * Shows any page
	 */
	this.showPage = function(path)
	{
		location.href= _comLayer.curServer() + path;
	}

	this.alert = function (message,fn) {
	   // alertify.alert(message,);
	}

	this.closeMessageDialog = function() {
	    $('#messageDialog').modal('hide'); 
	}

	this.showSuccess = function (message, funct) {

	    this.messageCallback = funct;
	    $('#divProcessing').hide();
	    $('#divSuccess').show();
	    $('#divError').hide();
	    $('#divMessage').html("<label>" + message + "</label>");
	}

	this.showError = function(message,funct) {

	    this.messageCallback = funct;
	    $('#divProcessing').hide();
	    $('#divSuccess').hide();
	    $('#divError').show();
	    $('#divMessage').html("<label>" + message + "</label>");
	}

	this.showMessageDialog = function (message, withMessage,success) {

	    if (withMessage) {
	        $('#divProcessing').hide();
	        if (success) {
	            $('#divSuccess').show();
	            $('#divError').hide();
	        } else {
	            $('#divSuccess').hide();
	            $('#divError').show();
	        }
	        $('#divMessage').html("<label>" + message + "</label>");
	    }
	    else {
	        $('#divSuccess').hide();
	        $('#divError').hide();
	        $('#divMessage').html("<label>Processing...</label>");
	        $('#divProcessing').show();
	        $('#messageDialog').modal('show');
	    }
	}

	this.getParameterByName = function(name) {
	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	this.displayView = function (tag, viewName, viewTarget) {

	    _comLayer.parameters.add("ViewName", viewName);

	    var oResult = _comLayer.executeSyncAction(tag + "/LoadViewLayout", _comLayer.parameters);

	    if (oResult.success == true) {
	        this.layout = oResult.data;
	        $('#' + viewTarget).empty();
	        $('#' + viewTarget).html(this.layout);
	        viewLoaded = true;
	    }

	}


    this.isInteger = function(s){
	    var i;
        for (i = 0; i < s.length; i++){   
            // Check that current character is number.
            var c = s.charAt(i);
            if (((c < "0") || (c > "9"))) return false;
        }
        // All characters are numbers.
        return true;
    }

    this.stripCharsInBag = function(s, bag){
	    var i;
        var returnString = "";
        // Search through string's characters one by one.
        // If character is not in bag, append to returnString.
        for (i = 0; i < s.length; i++){   
            var c = s.charAt(i);
            if (bag.indexOf(c) == -1) returnString += c;
        }
        return returnString;
    }

    this.daysInFebruary = function(year){
	    // February has 29 days in any year evenly divisible by four,
        // EXCEPT for centurial years which are not also divisible by 400.
        return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
    }

    this.DaysArray = function(n) {
	    for (var i = 1; i <= n; i++) {
		    this[i] = 31
		    if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		    if (i==2) {this[i] = 29}
       } 
       return this
    }

    this.isDate = function (dtStr) {
        var daysInMonth = this.DaysArray(12)
        var pos1 = dtStr.indexOf(dtCh)
        var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
        var strDay = dtStr.substring(0, pos1)
        var strMonth = dtStr.substring(pos1 + 1, pos2)
        var strYear = dtStr.substring(pos2 + 1)
        strYr = strYear
        if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
        if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
        for (var i = 1; i <= 3; i++) {
            if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
        }
        month = parseInt(strMonth)
        day = parseInt(strDay)
        year = parseInt(strYr)
        if (pos1 == -1 || pos2 == -1) {
            return { Message: "The date format should be : dd/mm/yyyy",
                valid: false
            };
        }
        if (strMonth.length < 1 || month < 1 || month > 12) {
            return { Message: "Please enter a valid month",
                valid: false
            };
        }
        if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > this.daysInFebruary(year)) || day > daysInMonth[month]) {
            return { Message: "Please enter a valid day",
                valid: false
            };
        }
        if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
            return { Message: "Please enter a valid 4 digit year between " + minYear + " and " + maxYear,
                valid: false
            };
        }
        if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || this.isInteger(this.stripCharsInBag(dtStr, dtCh)) == false) {
            return { Message: "Please enter a valid date",
                valid: false
            };
        }
        return { Message: "", valid: true };
    }

    this.dateGraterThan = function (EnteredDate, today) {
    
        var date = EnteredDate.substring(0, 2);
        var month = EnteredDate.substring(3, 5);
        var year = EnteredDate.substring(6, 10);

        var myDate = new Date(year, month - 1, date);

        var date = today.substring(0, 2);
        var month = today.substring(3, 5);
        var year = today.substring(6, 10);

        var today = new Date(year, month - 1, date);

        if (myDate > today) {
            return false;
        }

        return true;
    }

    this.dateGraterThanToday = function (EnteredDate) {
        var date = EnteredDate.substring(0, 2);
        var month = EnteredDate.substring(3, 5);
        var year = EnteredDate.substring(6, 10);

        var myDate = new Date(year, month - 1, date);
        var today = new Date();

        if (myDate > today) {
            return false;
        }

        return true;
    }


    this.render_basic_pie = function(container,sourcePath) {

        _comLayer.parameters.clear();
        var oResult = _comLayer.executeSyncAction(sourcePath, _comLayer.parameters);

        var graph;

        graph = Flotr.draw(container, oResult, {
            HtmlText: false,
            grid: {
                verticalLines: false,
                horizontalLines: false
            },
            xaxis: {
                showLabels: false
            },
            yaxis: {
                showLabels: false
            },
            pie: {
                show: true,
                explode: 2
            },
            mouse: {
                track: true
            },
            legend: {
                position: 'se',
                backgroundColor: '#D2E8FF'
            }
        });
    }

    this.render_basic_bars = function (container, sourcePath, horizontal) {
        _comLayer.parameters.clear();
        var oResult = _comLayer.executeSyncAction(sourcePath, _comLayer.parameters);

        var horizontal = (horizontal ? true : false);
        // Draw the graph
        Flotr.draw(
        container, oResult, {
            bars: {
                show: true,
                horizontal: horizontal,
                shadowSize: 0,
                barWidth: 0.5
            },
            mouse: {
                track: true,
                relative: true
            },
            yaxis: {
                min: 0,
                autoscaleMargin: 1
            }
        });
    }

    this.render_basic_line = function(container, sourcePath) {
        _comLayer.parameters.clear();
        var oResult = _comLayer.executeSyncAction(sourcePath, _comLayer.parameters);

        // Draw Graph
        graph = Flotr.draw(container, oResult, {
            xaxis: {
                minorTickFreq: 4
            },
            grid: {
                minorVerticalLines: true
            }
        });
    }

    this.render_gauge = function (container, sourcePath) {
        _comLayer.parameters.clear();
        var oResult = _comLayer.executeSyncAction(sourcePath, _comLayer.parameters);

        var gauge = function (container, configuration) {
            var that = {};
            var config = {
                size: 200,
                clipWidth: 200,
                clipHeight: 110,
                ringInset: 20,
                ringWidth: 20,

                pointerWidth: 10,
                pointerTailLength: 5,
                pointerHeadLengthPercent: 0.9,

                minValue: 0,
                maxValue: 10,

                minAngle: -90,
                maxAngle: 90,

                transitionMs: 750,

                majorTicks: 10,
                labelFormat: d3.format(',g'),
                labelInset: 10,

                arcColorFn: d3.interpolateHsl(d3.rgb('#e8e2ca'), d3.rgb('#3e6c0a'))
            };
            var range = undefined;
            var r = undefined;
            var pointerHeadLength = undefined;
            var value = 0;

            var svg = undefined;
            var arc = undefined;
            var scale = undefined;
            var ticks = undefined;
            var tickData = undefined;
            var pointer = undefined;

            var donut = d3.layout.pie();

            function deg2rad(deg) {
                return deg * Math.PI / 180;
            }

            function newAngle(d) {
                var ratio = scale(d);
                var newAngle = config.minAngle + (ratio * range);
                return newAngle;
            }

            function configure(configuration) {
                var prop = undefined;
                for (prop in configuration) {
                    config[prop] = configuration[prop];
                }

                range = config.maxAngle - config.minAngle;
                r = config.size / 2;
                pointerHeadLength = Math.round(r * config.pointerHeadLengthPercent);

                // a linear scale that maps domain values to a percent from 0..1
                scale = d3.scale.linear()
                    .range([0, 1])
                    .domain([config.minValue, config.maxValue]);

                ticks = scale.ticks(config.majorTicks);
                tickData = d3.range(config.majorTicks).map(function () { return 1 / config.majorTicks; });

                arc = d3.svg.arc()
                    .innerRadius(r - config.ringWidth - config.ringInset)
                    .outerRadius(r - config.ringInset)
                    .startAngle(function (d, i) {
                        var ratio = d * i;
                        return deg2rad(config.minAngle + (ratio * range));
                    })
                    .endAngle(function (d, i) {
                        var ratio = d * (i + 1);
                        return deg2rad(config.minAngle + (ratio * range));
                    });
            }
            that.configure = configure;

            function centerTranslation() {
                return 'translate(' + r + ',' + r + ')';
            }

            function isRendered() {
                return (svg !== undefined);
            }
            that.isRendered = isRendered;

            function render(newValue) {
                svg = d3.select(container)
                    .append('svg:svg')
                        .attr('class', 'gauge')
                        .attr('width', config.clipWidth)
                        .attr('height', config.clipHeight);

                var centerTx = centerTranslation();

                var arcs = svg.append('g')
                        .attr('class', 'arc')
                        .attr('transform', centerTx);

                arcs.selectAll('path')
                        .data(tickData)
                    .enter().append('path')
                        .attr('fill', function (d, i) {
                            if (i < 4)
                                return "#006400";
                            else if (i < 8)
                                return "#FF8C00";
                            else
                                return "#DC143C";
                        })
                        .attr('d', arc);

                var lg = svg.append('g')
                        .attr('class', 'label')
                        .attr('transform', centerTx);
                lg.selectAll('text')
                        .data(ticks)
                    .enter().append('text')
                        .attr('transform', function (d) {
                            var ratio = scale(d);
                            var newAngle = config.minAngle + (ratio * range);
                            return 'rotate(' + newAngle + ') translate(0,' + (config.labelInset - r) + ')';
                        })
                        .text(config.labelFormat);

                var lineData = [[config.pointerWidth / 2, 0],
                                [0, -pointerHeadLength],
                                [-(config.pointerWidth / 2), 0],
                                [0, config.pointerTailLength],
                                [config.pointerWidth / 2, 0]];
                var pointerLine = d3.svg.line().interpolate('monotone');
                var pg = svg.append('g').data([lineData])
                        .attr('class', 'pointer')
                        .attr('transform', centerTx);

                pointer = pg.append('path')
                    .attr('d', pointerLine/*function(d) { return pointerLine(d) +'Z';}*/)
                    .attr('transform', 'rotate(' + config.minAngle + ')');

                update(newValue === undefined ? 0 : newValue);
            }
            that.render = render;

            function update(newValue, newConfiguration) {
                if (newConfiguration !== undefined) {
                    configure(newConfiguration);
                }
                var ratio = scale(oResult.data);
                var newAngle = config.minAngle + (ratio * range);
                pointer.transition()
                    .duration(config.transitionMs)
                    .ease('elastic')
                    .attr('transform', 'rotate(' + newAngle + ')');
            }
            that.update = update;

            configure(configuration);

            return that;
        };


        function onDocumentReady() {
            var powerGauge = gauge(container, {
                size: 300,
                clipWidth: 300,
                clipHeight: 200,
                ringWidth: 60,
                maxValue: 100,
                transitionMs: 4000,
            });



            powerGauge.render();

            function updateReadings() {
                // just pump in random data here...
                powerGauge.update(Math.random() * 10);
            }

            // every few seconds update reading values
            updateReadings();
            //setInterval(function () {
            //    updateReadings();
            //}, 5 * 1000);


        }

        if (!window.isLoaded) {
            window.addEventListener("load", function () {
                onDocumentReady();
            }, false);
        } else {
            onDocumentReady();
        }
    }

    this.render_monthly_line = function (container, sourcePath) {
        _comLayer.parameters.clear();
        var oResult = _comLayer.executeSyncAction(sourcePath, _comLayer.parameters);

        // Draw Graph
        graph = Flotr.draw(container, oResult, {
            xaxis: {
                noTicks: 12,
                tickFormatter: function (x) {
                    var 
                  x = parseInt(x),
                  months = ['Ja', 'Fe', 'Ma', 'Ap', 'Ma', 'Ju', 'Jl', 'Au', 'Se', 'Oc', 'No', 'De'];
                  return months[x-1];
                }
            },
            grid: {
                minorVerticalLines: true
            }
        });
    }
    
	this.deepObjectCopy = function (oFromObject, oToObject) {
	    for (var propt in oFromObject) {
	        var dat = oFromObject[propt];
	        var check = Object.prototype.toString.call(dat);
	        var chkDestination = Object.prototype.toString.call(oToObject[propt]);
	        if (check != "[object Array]") {
	            if (chkDestination == "[object Function]") {
	                oToObject[propt](oFromObject[propt]);
	            }
	            else {
	                oToObject[propt] = oFromObject[propt];
	            }
	        }
	        else {
	            //needs mode deep copy so call function in recursion
	            var i = 0;

	            while (oFromObject[propt][i]) {
	                this.deepObjectCopy(oFromObject[propt][i], oToObject.addNewChild(propt));
	                i++;
	            }

	        }
	    }
	}
}