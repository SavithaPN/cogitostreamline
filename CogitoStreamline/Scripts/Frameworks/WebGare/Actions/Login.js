﻿

var _util = null;
var _comLayer = null;

$(document).ready(function () {

    _config = this;
    _comLayer = new WgCommunicationLayer();

    _utils = new WgUtilities();
    var cmdLogin = $('#cmdLogin');



    $('.input-large').bind("keypress", function (e) {

        if (e.keyCode == 13) {
            e.preventDefault();
            cmdLogin.click();
        }
    });

    cmdLogin.click(function (e) {
        e.preventDefault();
        Login();
    });

    $('#frmLogin').dblclick(function (e) {
        e.preventDefault();
        jQuery('#frmLogin').validationEngine('hide');
    });

   
});


function Login() {
   _utils = new WgUtilities();
   _utils.showMessageDialog("", false);

    //Get UsrName and Password
    var UserName = $('#UserName');
    var Password = $('#Password');

    var UserCredentials = new Object();


    if (UserName.val() == "" || UserName.val() == undefined || Password.val() == "" || Password.val() == undefined) {
        _util.showError("Please provide, UserName and Password to login", true);
        return;
    }

    UserCredentials["UserName"] = UserName.val();
    UserCredentials["Password"] = Password.val();

    var stringUser = JSON.stringify(UserCredentials);

    var actionURL = "Login/Authenticate";

    $.ajax({
        type: "POST",
        url: actionURL,
        data: { Credentials: stringUser },
        success: function (data) {
            if (data.Success) {
                bLoginPressed = true;
                //document.location = "Payroll/Home";
                document.location = "Home";
            }
            else {
                _util.showError("Login Failed, " + data.Message, true);
            }
        },
        complete: function () { },

        error: function (req, status, error) {
            //do what you need to do here if an error occurs
        }
    });
}