﻿var WizardSteps = new Array();
var wgWizardObject = null;

function wgWizardStep() {
    this.wizardHeader;
    this.wizardContent;
}

$(document).ready(function () {

    
});

$.fn.wgWizard = function (options) {

    this.options = $.extend({
        bDirectLoad: true,
        enableAllSteps: false
    }, options);

    this.wizardSteps = WizardSteps;

    this.actions = $.extend({
        addStep: function (tag, stepName, stepDesc, viewName) {
            var iStepNumber = WizardSteps.length;
            iStepNumber++;

            var newStepHeader = $("<li><a href='#step-" + iStepNumber + "'><label class='stepNumber'>" + iStepNumber
                            + "</label><span class='stepDesc'>" + stepName + "<br /><small>" + stepDesc + "</small></span></a></li>");

            if (wgWizardObject.options.bDirectLoad) {
                _comLayer.parameters.clear();
                _comLayer.parameters.add("ViewName", "_" + viewName);
                var sPath = _comLayer.buildURL(tag, "LoadViewLayout");
                var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
                var wizardContantHTML = "";

                if (oResult.success == true) {
                    wizardContantHTML = oResult.data;
                }

                var newStepContent = $("<div id='step-" + iStepNumber + "'><h2 class='StepTitle'>" + stepDesc + "</h2><div id='WizStep_" + iStepNumber + "_Content'>" + wizardContantHTML + "</div></div>");

                var newStepWizard = new wgWizardStep();

                newStepWizard.wizardHeader = newStepHeader;
                newStepWizard.wizardContent = newStepContent;

                WizardSteps.push(newStepWizard);
            }
            else {
                var newStepContent = $("<div id='step-" + iStepNumber + "'><h2 class='StepTitle'>" + stepName + "</h2><div id='" + iStepNumber + "_Content'>" + wizardContantHTML + "</div></div>");

                newStepWizard.wizardHeader = newStepHeader;
                newStepWizard.wizardContent = newStepContent;

                WizardSteps.push(newStepWizard);
            }

            try {
                newWizardStepAddedHook(tag, stepName, stepDesc, iStepNumber);
            }
            catch (e) { }

            return iStepNumber + "_Content";

        },
        clearAllSteps: function () {
            WizardSteps = new Array();
            $(wgWizardObject).empty();

            try {
                wizardClearedHook();
            }
            catch (e) { }
        },
        onLeaveCallback: function (objs, context) {
            var validation = true;
            try {
                validation = wizardStepLeaveHook(objs, context);
            }
            catch (e) { }

            return validation;
        },
        onShowStepCallback: function (objs, context) {
            try {
                wizardStepShowHook(objs, context);
            }
            catch (e) { }
        },
        onFinishCallback: function (objs, context) {
            try {
                wizardCompleteHook(objs, context);
            }
            catch (e) { }
        },
        refreshWizard: function () {

            if (WizardSteps.length <= 0) return;

            //empty the wizard container
            $(wgWizardObject).empty();

            $wizard = $("<div id='wizard' class='swMain' style='overflow:auto;'></div>");
            $wizardHead = $("<ul id='ul'>");
            $wizard.append($wizardHead);

            var i = 0;

            while (i < WizardSteps.length) {
                $wizardHead.append(WizardSteps[i].wizardHeader);
                $wizard.append(WizardSteps[i].wizardContent);
                i++;
            }

            $(wgWizardObject).append($wizard);

            $wizard.smartWizard({ transitionEffect: 'slide',
                enableAllSteps: wgWizardObject.options.enableAllSteps,
                onLeaveStep: wgWizardObject.actions.onLeaveCallback, 
                onShowStep: wgWizardObject.actions.onShowStepCallback,
                onFinish: wgWizardObject.actions.onFinishCallback
            });

            try {
                wizardRefreshedHook(WizardSteps);
            }
            catch (e) { }
        }

    });

    wgWizardObject = this;
    return this;
}

