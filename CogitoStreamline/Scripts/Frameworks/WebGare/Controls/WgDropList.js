﻿wgDropLists = new Object();


$(document).ready(function () {

    ko.bindingHandlers.WgDropList = {

        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var jElement = $(element);
            var dataSource = jElement.attr("dataSource");

            var i = 0;

            var idVal = ko.unwrap(valueAccessor());

            while (viewModel[dataSource][i]) {

                var frmList = viewModel[dataSource][i].Id;

                if (frmList == idVal) {
                    $(jElement).find(".placeholder").remove();
                    var newElement = $("<div class='wgDropElement' dbId='" + viewModel[dataSource][i].Id + "'></div>").appendTo(jElement);
                    var listItem = $("<li dbId ='" + idVal + "' title='" + viewModel[dataSource][i].Details + "'>" + viewModel[dataSource][i].Name + "<br>" + viewModel[dataSource][i].Value + "</li>");
                    newElement.append(listItem);

                }

                i++;
            }

            // This will be called when the binding is first applied to an element
            // Set up any initial state, event handlers, etc. here
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            //Fired befre the dropzone formed
        }

    };

});

$.fn.wgDropList = function (options) {

    wgDropLists[this[0].id] = this;

    this.options = $.extend({
        viewModel: null,
        acceptSource: [],
        placeholder: "Please drop element here",
        acceptMany: false
    }, options);

    this.actions = $.extend({

    });

    var placeHolder = $("<div class='placeholder'>" + this.options.placeholder + "</div>");


    $(this).addClass("wgDorpList");

    if ($(this).children().length == 0) {
        $(this).append(placeHolder);
    }

    if (this.options.acceptMany == false) {
        $(this).addClass("wgSingle");
    }

    $(this).droppable({
        accept: function (src) {
            var bAccept = false;

            var acceptList = wgDropLists[this.id].options.acceptSource;

            var i = 0;

            while (acceptList[i]) {
                if (acceptList[i] == src.parents()[1].id) {
                    bAccept = true;
                }

                i++;
            }

            return bAccept;
        },

        drop: function (event, ui) {
            $(this).find(".placeholder").remove();
            if (!wgDropLists[this.id].options.acceptMany) {
                $(this).empty();
                wgDropLists[this.id].options.viewModel.data[wgDropLists[this.id].options.property] = ui.draggable[0].attributes.dbId.value;
            }
            else {
                if (wgDropLists[this.id].options.viewModel.data[wgDropLists[this.id].options.property] == null) {
                    wgDropLists[this.id].options.viewModel.data[wgDropLists[this.id].options.property] = ko.observableArray();
                }
                wgDropLists[this.id].options.viewModel.data[wgDropLists[this.id].options.property].push(ui.draggable[0].attributes.dbId);
            }
            var ele = $("<div class='wgDropElement' dbId='" + ui.draggable[0].attributes.dbId + "'></div>").appendTo(this);
            ele[0].innerHTML = ui.draggable[0].innerHTML;
        }
    });

    return this;
}