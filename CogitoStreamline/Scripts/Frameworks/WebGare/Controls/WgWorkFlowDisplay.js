﻿$.fn.wgWorkFlowHistory = function (options) {

    this.options = $.extend({
        viewModel: null,
        wgTag:null
    }, options);

    this.actions = $.extend({

    });

    $(this).jtable({
        title: 'Workflow History',
        paging: true,
        pageSize: 15,
        actions: {
            listAction: this.options.wgTag + '/GetWfHistory'
        },
        fields: {
            Pk_Transaction: {
                title: 'Transaction Id',
                list: false,
                key: true
            },
            FromState: {
                title: 'From State',
                edit: false
            },
             ToState: {
                title: 'To State',
                edit: false
            },
             Date: {
                title: 'Date',
                edit: false
            },
            User: {
                title: 'User',
                edit: false
            },
            Comment: {
                title: 'Comment',
                edit: false,
                width: "15%"
            }
        }
    });

    $(this).jtable('load', {
        Entityid: this.options.viewModel.id
    });

    return this;

}