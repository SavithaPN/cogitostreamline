﻿var buttonControl;
$.fn.wgWorkFlowButtons = function (options) {

    this.options = $.extend({
        viewModel: null,
        wgTag: null,
        stateField: null,
        belongsTo: null,
        pkField: null
    }, options);
    _configButtons = this;

    this.actions = $.extend({

    });

    buttonControl = this;

    _comLayer.parameters.clear();
    _comLayer.parameters.add("BelongsTo", this.options.belongsTo);
    _comLayer.parameters.add("State", this.options.viewModel.data[this.options.stateField]());
    //Try to get workflow paths
    
    var pths = _comLayer.executeSyncAction("/" + this.options.wgTag + "/GetWfPaths", null);

    var i = 0;

    while (pths[i]) {

        var wfButton = "<button class='win-command win-command-small' Askcomment=" + pths[i].AskComment + " pathId='" + pths[i].PathId + "' Entity= '" + this.options.viewModel.data[this.options.pkField]() + "'>";
        wfButton += "<span class='win-commandicon win-commandring " + pths[i].IconClass + "'></span>";
        wfButton += "<span class='win-label'>" + pths[i].ActionName + " </span>";

        //var btn = $("<button class='btn btn-info' pathId='" + pths[i].PathId + "' Entity= '" + this.options.viewModel.data[this.options.pkField]() + "'>" + pths[i].PathName + "</button>");
        var btn = $(wfButton);
        btn.click(function (e) {
            e.preventDefault();
            var pathId = $(this).attr("pathId");
            var Entity = $(this).attr("Entity");
            var AskComment = $(this).attr("Askcomment");
            var bProceed = false;
            if (pathId == 11) {
                var bProceed = validatebuttonActionHook();
            }


            _comLayer.parameters.clear();
            _comLayer.parameters.add("Entity", Entity);
            _comLayer.parameters.add("Path", pathId);
            _comLayer.parameters.add("PkField", buttonControl.options.pkField);
            _comLayer.parameters.add("StateField", buttonControl.options.stateField);
            //Try to get workflow paths
            if (bProceed == false) {
                if (AskComment == "true") {
                    $('#workFlowCommentDialog').modal('show');

                    $("#cmdWfCommentSave").click(function (e) {
                        e.preventDefault();
                        $('#workFlowCommentDialog').modal('hide');
                        var comment = $('#txtWfComment').val();
                        _comLayer.parameters.add("Comment", comment);
                        var oResult = _comLayer.executeSyncAction("/" + buttonControl.options.wgTag + "/Route", null);
                        buttonControl.options.viewModel.data.Fk_Status(oResult.Message);
                        buttonControl.options.viewModel.save();
                        _page.showView('Search');
                    });
                }
                else {
                    _comLayer.parameters.add("Comment", "");
                    var oResult = _comLayer.executeSyncAction("/" + buttonControl.options.wgTag + "/Route", null);
                    buttonControl.options.viewModel.data[buttonControl.options.stateField](oResult.Message);
                    buttonControl.options.viewModel.save();
                    _page.showView('Search');
                }
            }
            else {
                //_util.showError("Please contact administrator", _configButtons.showSearch);
                alert("Unable save please contact administrator");
            }
        });

        $(this).append(btn);
        $(this).append($("<a> </a>"));

        i++;
    }

    if (i == 0) $("#wfButtons").append($("<p class='label label-info'><b>No Actions Possible Currently</b></p>"));

    return this;

}