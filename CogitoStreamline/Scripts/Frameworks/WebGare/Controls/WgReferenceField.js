﻿$(document).ready(function () {

    ko.bindingHandlers.wgReferenceField = {

        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // refreshContactList(element, viewModel);
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            try {
                $(element).wgReferenceField("setValue", viewModel);
            }
            catch (e) {
                ReferenceFieldNotInitilized(viewModel);
            }

        }

    };

});


(function ($) {
    $.widget("hik.wgReferenceField", {
        /************************************************************************
        * DEFAULT OPTIONS / EVENTS                                              *
        *************************************************************************/
        options: {
            keyProperty: null,
            displayProperty: null,
            loadPath: null,
            control: null
        },
        /* Contructor.
        *************************************************************************/
        _create: function () {
            var test = 10;
        },

        setValue: function (viewModel) {
            _comLayer.parameters.clear();

            if(viewModel.data != undefined && viewModel.data[this.options.keyProperty] != undefined && viewModel.data[this.options.keyProperty] != null &&
            this.options.viewModel.data[this.options.keyProperty] != "")
            {
                var propValue = viewModel.data[this.options.keyProperty];

                try
                {
                  propValue = viewModel.data[this.options.keyProperty]();
                }catch(e) {}

                _comLayer.parameters.add("Id", propValue);
                var oResult = _comLayer.executeSyncAction(this.options.loadPath, _comLayer.parameters);

                if (oResult.success) {
                    $(this.element.context).val(oResult.data[this.options.displayProperty]);
                }
            }
        },

        setData: function (value) {
            this.options.viewModel.data[this.options.keyProperty] = value;
            this.setValue(this.options.viewModel);
        }

    });
})(jQuery);