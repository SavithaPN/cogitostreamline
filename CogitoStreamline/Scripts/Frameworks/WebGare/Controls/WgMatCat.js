﻿var oContacts = null;
$(document).ready(function () {

    ko.bindingHandlers.WgContact = {

        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // refreshContactList(element, viewModel);
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            refreshContactList(element, viewModel);
        }

    };

});

function refreshContactList(element, viewModel) {
    var jElement = $(element);
    var property = jElement.attr("dataSource");

    jElement.empty();

    if (viewModel.data[property] == undefined) {
        viewModel.data[property] = ko.observableArray();
    }

    var i = 0;

    while (viewModel.data[property]()[i]) {
        var oContact = viewModel.data[property]()[i];
        var newContact = $("<div class='contactHolder' id=" + oContact.id + "></div>");

        var tbl = $("<table cellpadding='2' cellspacing='0' width='100%'></table>");
        var edit = $("<i class='btn icon-pencil contactEdit' style='float:right' itemid='" + oContact.id + "'></i>");
        var remove = $("<i class='btn icon-trashcan contactDelete' style='float:right' itemid='" + oContact.id + "'></i>");

        var header = $("<tr><td class='contactName'>" + oContact.Name + "</td></tr>");
        var buttonContainer = $("<tr></tr>");

        buttonContainer.append(remove);
        buttonContainer.append(edit);

        tbl.append(header);
        tbl.append(buttonContainer);

        var details = oContact.Details.split(";");
        var x = 0;

        while (details[x]) {
            var detail = $("<tr><td class='contactDetail'>" + details[x] + "</td></tr>");
            tbl.append(detail);
            x++;
        }

        i++;
        var td = $("<td></td>");
        newContact.append(tbl);
        td.append(newContact);
        jElement.append(td);
    }

}

$.fn.wgContact = function (options) {

    this.options = $.extend({
        viewModel: null,
        property: null
    }, options);

    this.actions = $.extend({
        beforeModelSave: function () {
            if ($("#frmDataDialog").validationEngine('validate')) {
                _util.showMessageDialog("", false);
                return true;
            }
            else {
                return false;
            }
        },
        beforeModelDelete: function () {
            return true;
        },
        afterModelSave: function (object) {

            $('#dataDialog').modal('hide');

            currResult = object.result;

            if (object.result.Success) {
                _util.showSuccess("Data saved successfully", function () { });
            }
            else {
                _util.showError("Unable saved please contact administrator", function () { });
            }

            var i = 0;

            while (oContacts.options.viewModel.data[oContacts.options.property]()[i]) {
                if (oContacts.options.viewModel.data[oContacts.options.property]()[i].id == currResult.Message) {
                    oContacts.options.viewModel.data[oContacts.options.property].remove(oContacts.options.viewModel.data[oContacts.options.property]()[i]);
                }
                i++;
            }

            var oViewModel = _page.getViewByName('EditContact').viewModel;
            oViewModel.setId(currResult.Message)
            oViewModel.fetch();

            //create a new object now 
            var oContact = new Object();
            oContact["id"] = oViewModel.data.Pk_ContactPersonId();
            oContact["Name"] = oViewModel.data.ContactPersonName();
            oContact["Details"] = "Number:" + oViewModel.data.ContactPersonNumber() +
                                  ";eMail:" + oViewModel.data.ContactPersonEmailId() +
                                  ";State:" + oViewModel.data.Address_StateName() +
                                  ";City:" + oViewModel.data.Address_CityName() +
                                  ";Street:" + oViewModel.data.Address_No_Street() +
                                  ";Pin:" + oViewModel.data.Address_PinCode();

            oContacts.options.viewModel.data[oContacts.options.property].push(oContact);

            oContacts.actions.setUpFunctions();
        },
        afterModelDelete: function (object) {
            $('#dataDialog').modal('hide');

            currResult = object.result;

            if (object.result.Success) {
                _util.showSuccess("Data saved successfully", function () { });
            }
            else {
                _util.showError("Unable saved please contact administrator", function () { });
            }

            var i = 0;

            while (oContacts.options.viewModel.data[oContacts.options.property]()[i]) {
                if (oContacts.options.viewModel.data[oContacts.options.property]()[i].id == currResult.Message) {
                    oContacts.options.viewModel.data[oContacts.options.property].remove(oContacts.options.viewModel.data[oContacts.options.property]()[i]);
                }
                i++;
            }

            oContacts.actions.setUpFunctions();
        },
        setUpFunctions: function () {
            $('#cmdAddContact').click(function (e) {
                e.preventDefault();
                $('#dataDialogHeading').text("New Contact");
                _page.showView("NewContact");
                //Ravi 29-12-2015
                $("#frmHost").validationEngine('hide');
                $('#dataDialog').modal('show');
                _page.getViewByName('NewContact').dataBind();

                $("#frmDataDialog").validationEngine();
            });

            $('.contactEdit').click(function (e) {
                e.preventDefault();
                $('#dataDialogHeading').text("Edit Contact");
                _page.getViewByName('EditContact').viewModel.setId($(this).attr("itemid"));
                _page.getViewByName('EditContact').options.context = 'Edit';
                _page.showView('EditContact');
                _page.getViewByName('EditContact').dataBind();
                $('#dataDialog').modal('show');

                $("#frmDataDialog").validationEngine();
            });

            $('.contactDelete').click(function (e) {
                e.preventDefault();
                $('#dataDialogHeading').text("Delete Contact");
                _page.getViewByName('EditContact').viewModel.setId($(this).attr("itemid"));
                _page.getViewByName('EditContact').options.context = 'Delete';
                _page.showView('EditContact');
                $("#frmDataDialog #cmdCreate").attr("data-bind", "click: deleteRecord");
                _page.getViewByName('EditContact').dataBind();
                $('#dataDialog').modal('show');
            });

        }
    });

    //Add addtional view to page
    var oNewContact = new WgView("NewContact", "ContactPerson", "dataDialogArea", {}, { subViewName: 'New' });
    var oEditContact = new WgView("EditContact", "ContactPerson", "dataDialogArea", {}, { reloadOnShow: true, context: 'Edit', subViewName: 'Edit' }); //Edit and Delete Same view

    oNewContact.viewModel.modelName = null;
    oEditContact.viewModel.modelName = null;

    _page.addView(oNewContact);
    _page.addView(oEditContact);


    _page.getViewByName('NewContact').viewModel.addAddtionalDataSources("city", "getCity", null);
    _page.getViewByName('EditContact').viewModel.addAddtionalDataSources("city", "getCity", null);
    _page.getViewByName('NewContact').viewModel.addAddtionalDataSources("State", "getState", null);
    _page.getViewByName('EditContact').viewModel.addAddtionalDataSources("State", "getState", null);


    oNewContact.viewModel.subscribeHook('beforeModelSave', this.actions.beforeModelSave);
    oNewContact.viewModel.subscribeHook('afterModelSave', this.actions.afterModelSave);

    oEditContact.viewModel.subscribeHook('beforeModelSave', this.actions.beforeModelSave);
    oEditContact.viewModel.subscribeHook('afterModelSave', this.actions.afterModelSave);
    oEditContact.viewModel.subscribeHook('beforeModelDelete', this.actions.beforeModelDelete);
    oEditContact.viewModel.subscribeHook('afterModelDelete', this.actions.afterModelDelete);

    var newContactButtons = $("<div class='contactButtons'></div>");

    var addButton = $("<button class='btn btn-info' id='cmdAddContact'>Search</button>");
    newContactButtons.append(addButton);

    //$('#Address_State').change(function () {
    //    if ($('#Address_State').val() != '') {
    //        loadComboCity('#Address_City', $('#Address_State').val());
    //        $('#Address_City').change();
    //    }
    //});
    $('#contactButtons').append(newContactButtons);

    this.actions.setUpFunctions();

    oContacts = this;
    return this;
}

//function getCityComboValues(parent) {
//    _comLayer.parameters.clear();
//    _comLayer.parameters.add("StateId", parent);

//    var sPath = _comLayer.buildURL("City", null, "CityListByState");
//    var oResult = _comLayer.executeSyncAction(sPath, _comLayer.parameters);
//    var oResultObject = oResult;

//    return oResultObject;
//}

//function loadComboCity(combo, parent) {
//    var oResult = getCityComboValues(parent);
//    $(combo).empty();

//    var option = $("<option value=''>Select</option>");
//    $(combo).append(option);

//    if (oResult.Success) {
//        var i = 0;

//        while (oResult.Records[i]) {
//            var option = $("<option value='" + oResult.Records[i].Pk_CityID + "'>" + oResult.Records[i].CityName + "</option>");
//            $(combo).append(option);
//            i++;
//        }
//    }
//}

