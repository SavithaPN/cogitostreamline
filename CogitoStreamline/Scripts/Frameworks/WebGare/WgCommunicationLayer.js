function WgCommunicationLayer()
{
   this.parameters = new WgParameters(); 
        
	this.buildURL = function(pName,pTag,pAction)
	{
		var sPath = this._appendToPath(pName,true)
								 + this._appendToPath(pTag,true) 
								 + this._appendToPath(pAction,true);
		
		return sPath;
	}
	
	this._appendToPath = function(pAttr,pGivePathLinker)
	{
		if(pAttr != null && typeof pAttr != 'undefined')
		{
			return (pGivePathLinker)?"/"+pAttr : pAttr;
		}
		else
		{ return "";}
	}
	
	this.curServer = function()
	{
		return location.protocol +"//"+ location.host;
	}

	this.executeAction = function (Url, parms, callBack) {

	    if (parms == null) parms = this.parameters;

	    var valuesReturned = null;
	    $.ajax({
	        type: "POST",
	        async: false,
	        url: Url,
	        dataType: "json",
	        data: { data: parms.getJSON() },
	        success: function (data) {
	            valuesReturned = data;
	            callBack(data);
	        },
	        complete: function () { },

	        error: function (req, status, error) {
	            return error;
	        }
	    });

	    return valuesReturned;

	}

	this.executeSyncAction = function (Url, parms) {
	    
		if(parms == null) parms = this.parameters;
	    //	alert(parms.getJSON());

		var valuesReturned = null;
	    $.ajax({
	    	
	    	 type: "POST",
	    	 async: false,
	    	 url: Url,
	    	 dataType: "json",
	    	 data: { data: parms.getJSON() },
	    	 
	         success: function (data) {
	            valuesReturned = data;
	        },
	        complete: function () { },

	        error: function (req, status, error) {
	            return error;
	        }
	    });

	    return valuesReturned;
	}
	//////////////////////
	this.executeSyncAction1 = function (Url, parms) {
	    var indices = getIndicesOf("/", Url);
	    
	    var MethodName = Url.substr(indices[1] + 1);

	    var res = Url.substr(indices[0] + 1, indices[1]-1);
	    //alert(indices);
	    if (parms == null) parms = this.parameters;
	    //	alert(parms.getJSON());

	    var valuesReturned = null;

	    if (MethodName != "Update") {
	        $.ajax({

	            type: "POST",
	            async: false,
	            url: "/" + res + "/SaveNew",
	            dataType: "json",
	            data: { data: "=" + parms.getJSON() },

	            success: function (data) {
	                valuesReturned = data;
	            },
	            complete: function () { },

	            error: function (req, status, error) {
	                return error;
	            }
	        });
	    }
	    else {

	        $.ajax({

	            type: "POST",
	            async: false,
	            url: "/" + res + "/Update",
	            dataType: "json",
	            data: { data: parms.getJSON() },

	            success: function (data) {
	                valuesReturned = data;
	            },
	            complete: function () { },

	            error: function (req, status, error) {
	                return error;
	            }
	        });

	    }
	    return valuesReturned;
	}
    ///////////////////////////
	this.executeSyncActionPrint = function (Url, parms) {
	    var indices = getIndicesOf("/", Url);
	    //var Presence1 = indices[0];

	    var res = Url.substr(0, indices[0]);
	    //alert(indices);
	    if (parms == null) parms = this.parameters;
	    //	alert(parms.getJSON());

	    var valuesReturned = null;
	    $.ajax({

	        type: "POST",
	        async: false,
	        url: Url,
	        dataType: "text/plain",
	        data: { data: "=" + parms.getJSON() },

	        success: function (data) {
	            valuesReturned = data;
	        },
	        complete: function () { },

	        error: function (req, status, error) {
	            return error;
	        }
	    });

	    return valuesReturned;
	}
    ///////////////////////////
	function getIndicesOf(searchStr, str, caseSensitive) {
	    var searchStrLen = searchStr.length;
	    if (searchStrLen == 0) {
	        return [];
	    }
	    var startIndex = 0, index, indices = [];
	    if (!caseSensitive) {
	        str = str.toLowerCase();
	        searchStr = searchStr.toLowerCase();
	    }
	    while ((index = str.indexOf(searchStr, startIndex)) > -1) {
	        indices.push(index);
	        startIndex = index + searchStrLen;
	    }
	    return indices;
	}

	

	//document.getElementById("output").innerHTML = indices + "";


    //////////////////////////////////
	this.getPartialLayout = function (Url) {
	    
		var valuesReturned = null;
	    $.ajax({
	        async: false,
	        url: Url,
	        dataType: "html",
	        success: function (data) {
	            valuesReturned = data;
	        },
	        complete: function () { },

	        error: function (req, status, error) {
	            return error;
	        }
	    });

	    return valuesReturned;
	}
}

function WgParameters()
{
	this.jsonString = null;
	
	this._parameterObject = new Object();
	
	this.add = function(pParamName, pParamValue)
	{
		this._parameterObject[pParamName] = pParamValue;
	};

	this.clear = function () {
	    this._parameterObject = new Object();
	    this.jsonString = null;
	};
	
	this.getJSON = function()
	{
		if(this.jsonString==null)
		{return JSON.stringify(this._parameterObject);}
		else
		{return this.jsonString;}
				
	};
	
	this.setJSON = function(json)
	{
		this.jsonString = json;	
	};
}