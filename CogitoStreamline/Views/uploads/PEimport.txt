insert into PE04072015.dbo.gen_state
(
 [Pk_stateID]
      ,[stateName]
)
(
SELECT [Pk_stateID]
      ,[stateName]
  FROM PE20032015.dbo.[gen_state]
  )
GO


insert into PE04072015.dbo.gen_city
(
[Pk_CityID]
      ,[Fk_stateID]
      ,[cityName]
)

(SELECT [Pk_CityID]
      ,[Fk_stateID]
      ,[cityName]
  FROM PE20032015.[dbo].[gen_city]
  )
GO


insert into PE04072015.dbo.gen_StaffType(
[Pk_StaffType]
      ,[StaffTypeName]
	  )
(SELECT [Pk_StaffType]
      ,[StaffTypeName]
  FROM PE20032015.[dbo].[gen_StaffType]
  )
GO


insert into PE04072015.dbo.stf_Staff
(
[Pk_StaffId]
      ,[StaffId]
      ,[StaffName]
      ,[FatherName]
      ,[DOB]
      ,[City]
      ,[State]
      ,[Address]
      ,[StaffType]
      ,[UserId]
      ,[ImagePath]
      ,[Mobile]
      ,[Phone]
      ,[IsActive]
)

(
SELECT [Pk_StaffId]
      ,[StaffId]
      ,[StaffName]
      ,[FatherName]
      ,[DOB]
      ,[City]
      ,[State]
      ,[Address]
      ,[StaffType]
      ,[UserId]
      ,[ImagePath]
      ,[Mobile]
      ,[Phone]
      ,[IsActive]
  FROM PE20032015.[dbo].[stf_Staff]
  )
GO




insert into PE04072015.dbo.gen_Branch( Pk_BranchId
      ,BranchName
      ,BranchResponsiblePerson
      ,StaffName
      ,ContactNumber1
      ,ContactNumber2
      ,Address_No_Street
      ,Address_State
      ,Address_City
      ,Address_PinCode
      ,TinNo
      ,CSTNo
      ,Address2
  )
(
SELECT Pk_BranchId
      ,BranchName
      ,BranchResponsiblePerson
      ,StaffName
      ,ContactNumber1
      ,ContactNumber2
      ,Address_No_Street
      ,Address_State
      ,Address_City
      ,Address_PinCode
      ,TinNo
      ,CSTNo
      ,Address2
  FROM PE20032015.dbo.gen_Branch)
GO


insert into [PE04072015].dbo.ideUsers
(
 [Pk_Users]
      ,[UserName]
      ,[Password]
      ,[FirstName]
      ,[LastName]
      ,[Company]
      ,[Country]
      ,[Office]
      ,[IsActive]
      ,[CreatedDate]
)(



SELECT [Pk_Users]
      ,[UserName]
      ,[Password]
      ,[FirstName]
      ,[LastName]
      ,[Company]
      ,[Country]
      ,[Office]
      ,[IsActive]
      ,[CreatedDate]
  FROM PE20032015.[dbo].[ideUsers]
  )

insert into PE04072015.dbo.ideRoles
(
 [Pk_Roles]
      ,[RoleName]
      ,[Description]
	  )
	  (
SELECT [Pk_Roles]
      ,[RoleName]
      ,[Description]
  FROM PE20032015.[dbo].[ideRoles]
  )
GO


insert into PE04072015.dbo.ideRoleUsers
( [Pk_RoleUsers]
      ,[Fk_Roles]
      ,[Fk_Users]
	  )
	  (
SELECT [Pk_RoleUsers]
      ,[Fk_Roles]
      ,[Fk_Users]
  FROM PE20032015.[dbo].[ideRoleUsers]
  )