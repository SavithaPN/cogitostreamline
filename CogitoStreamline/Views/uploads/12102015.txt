1) Alerts - When we have 0 alerts,view all button should be removed. For ex - Delivery alert says "You have 0 orders to deliver" but view all but view all button still remains. This button should be enabled only if list exceeds >5 alerts.Also, instead of showing '0' alerts , alerts should not be shown at all.
 
2)When there are more than 5 alerts, after clicking view all, it displays all alerts but view all button still remains.

3)Clicking on alerts/view all, alerts box vanishes. Again clicking on alerts, list of all alerts are shown. View all should display all alerts as soon as clicked.

4)Suggestion - Each alert needs to be clickable and on cliking, it should show details. 

5)Unable to delete records in city table. 

6)Enquiry - Add enquiry form/delivery schedule, date field is greyed out by default. It should have date picker icon to indicate calendar selection. Text box should not be greyed out by default.

7)Enquiry - Add enquiry form/delivery schedule, date field-> calendar: Double click on selected date should select date 

8)Unable to save externally added documents. System remains in processing screen and hangs. None of the tabs customer details, specification, delivery schedule work. Any details entered in enquiry form is totally lost. 

9)Add material indent/comments - Only 9 characters are allowed to enter. Character limit should be much higher.

10) Transaction-Enquiry-After searching with ID,list of records with searched ID is displayed.But, on press of back button, home screen is displayed.It is suppose to display list of all enquiries.

11) Transaction-Enquiry - There is no option to view the records, one has to go to 'Edit' screen and then view the details.Each record should be viewable seperately.

12) Transaction- Enquiry - Customer specification tab - number of colors option does not make sense. Instead we can have color name/Multiple colors.

13) Transaction- Enquiry - Customer specification tab - On selection of printing check box, user should be give an option to enter text whcih has to be printed on the box.

14) Transation - Enquiry- Comunication type dropdown - If user selects E-mail/Phone no/In person, he should be give an option to enter E-mail ID/Phone no/ Person details like person name, company name , his contact info.

15) Transaction - enquiry - Delivery
 dates - not clear as to why workflow history option needs to be part of Edit enquiry form? Work flow history can be implemented seperately to track the status of enquiry flow. 

16) Main Screen - Search  is not functional.

17) Transaction - Enquiry -  There is no option to delete Enquiry records.

18) Transaction - Enquiry - When transaction date is equal to enquiry date and on trying to save form error is displayed along with processing pop up in the background.

19) Order form could not be saved.System remains in processing screen.

20) Transaction - Order - Order form is greyed out and it should not be greyed out.