﻿//Name          : FinishedGoods Operations ----javascript files
//Description   : Contains the  PaperType Operations  definition, every page in the application will be instance of this class with following
//                 1. Jtable Loading and Columns Descriptions 2.Button Click Events  
//Author        : CH.V.N. Ravi Kumar
//Date 	        : 10/09/2015
//Crh Number    : SL00060
//Modifications : 


var curViewModel = null;
var Customer = "";
function initialCRUDLoad() {

    $('#MainSearchContainer').jtable({
        title: 'Finished Goods List',
        paging: true,
        pageSize: 15,
        actions: {
            listAction: '/FinishedGoods/FinishedGoodsListByFiter',
            updateAction: '',
            createAction: ''
        },
        fields: {
            Pk_FinishedGoodsId: {
                title: ' Id',
                key: true,
                width: '4%'
            },
            FinishedDate: {
                title: ' Date',
                width: '4%'
            },
            CustomerName: {
                title: 'Customer Name',
                edit: false
            },
            Fk_Order: {
                title: 'Order No'
            }

        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            Pk_FinishedGoodsId: $('#txtSearchFinishedGoodsId').val(),
            Fk_Customer: $('#txtSearchCustomer').val(),
            Fk_Order: $('#txtSearchOrder').val(),
            FinishedDate: $('#txtSearchFinishedDate').val()
        });

    });


    $('#txtSearchFinishedGoodsDate').change(function (e) {
        e.preventDefault();
        $('#MainSearchContainer').jtable('load', {
            FinishedGoodsDate: $('#txtSearchFinishedGoodsDate').val()
        });
    });

    $('#LoadRecordsButton').click();


    $('#cmdNew').click(function (e) {
        e.preventDefault();
        _page.showView('New');
    });


    $('#txtSearchFinishedGoodsId').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Pk_FinishedGoods: $('#txtSearchFinishedGoodsId').val()
            });
        }
    });

    $('#txtSearchCustomer').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#MainSearchContainer').jtable('load', {
                Customer: $('#txtSearchCustomer').val()
            });
        }
    });


    $("#searchDialog").width(1000);
    $("#searchDialog").css("top", "50px");
    $("#searchDialog").css("left", "300px");

    $("#dataDialog").width(1000);
    $("#dataDialog").css("top", "50px");
    $("#dataDialog").css("left", "300px");

    //setUpCustomerSearch();
}


function beforeModelSaveEx() {
    //$(".ui-accordion-content").show();
    _util.setDivPosition("tbShippingDetails", "none");
    _util.setDivPosition("tbCustomerDetails", "block");

    var viewModel = _page.getViewByName('New').viewModel;
    viewModel.data["FinishedDate"] = $('#dtFinishedDate').val();
    viewModel.data["Fk_Order"] = $('#Fk_Order').val();
    viewModel.data["Product"] = $('#Product').val();


    var viewModel1 = _page.getViewByName('Edit').viewModel;
    viewModel1.data["FinishedDate"] = $('#dtFinishedDate').val();
    viewModel1.data["Fk_Order"] = $('#Fk_Order').val();
    viewModel1.data["Product"] = $('#Product').val();
}
function afterNewShow(viewObject) {
    var dNow = new Date();
    document.getElementById('dtFinishedDate').value = (((dNow.getDate()) < 10) ? "0" + dNow.getDate() : dNow.getDate()) + '/' + (((dNow.getMonth() + 1) < 10) ? "0" + (dNow.getMonth() + 1) : (dNow.getMonth() + 1)) + '/' + dNow.getFullYear();


    $('#dtFinishedDate').datepicker({ autoclose: true });

    curViewModel = viewObject.viewModel;

    $('#divDeliveryschedule').jtable({
        title: 'Delivery Schedule',
        paging: true,
        pageSize: 5,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/FinishedGoods/Bounce'
            //updateAction: ''
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            DeliveryDate: {
                title: 'Date',
                key: false
            },
            Quantity: {
                title: 'Quantity'

            },
            Comments: {
                title: 'Comments',
                width: '20%'
            }
        }
    });

    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "FinishedGoods", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });

    $('#cmdCustomerSearch').click(function (e) {
        e.preventDefault();

        $("#searchDialog").modal("show");
    });

    $('#cmdCustomerOrderSearch').click(function (e) {
        e.preventDefault();
        _util.displayView("FinishedGoods", "_AddCustomerOrder", "dataDialogArea");
        $("#dataDialog").modal("show");
        setUpOrderSearch(viewObject);
        //$("#searchCustomerDialog").modal("show");

    });


}

function afterEditShow(viewObject) {
    

    $('#cmdCustomerOrderSearch').click(function (e) {
        e.preventDefault();
        _util.displayView("FinishedGoods", "_AddCustomerOrder", "dataDialogArea");
        $("#dataDialog").modal("show");
        setUpOrderSearch(viewObject);
        //$("#searchCustomerDialog").modal("show");

    });
    curViewModel = viewObject.viewModel;

    

    $('#divDeliveryschedule').jtable({
        title: 'Delivery Schedule',
        paging: true,
        pageSize: 5,
        defaultSorting: 'Name ASC',
        actions: {
            listAction: '/FinishedGoods/Bounce'
        },
        fields: {
            slno: {
                title: 'slno',
                key: true,
                list: false
            },
            Pk_DeliverySechedule: {
                title: 'Delivery ID',
                key: false,
                list: false
            },
            DeliveryDate: {
                title: 'Delivery Date',
                key: false
            },
            Quantity: {
                title: 'Quantity'

            },
            Comments: {
                title: 'Comments',
                width: '20%'
            }
        }
    });

    var oSShipping = viewObject.viewModel.data.shippingDetails();
    document.getElementById('FirstName').value = oSShipping[0].FirstName;
    document.getElementById('LastName').value = oSShipping[0].LastName;
    document.getElementById('Address').value = oSShipping[0].Address;
    document.getElementById('Pincode').value = oSShipping[0].Pincode;
    document.getElementById('City').value = oSShipping[0].City;
    document.getElementById('State').value = oSShipping[0].State;
    document.getElementById('Country').value = oSShipping[0].Country;
    document.getElementById('Telephone').value = oSShipping[0].Telephone;
    document.getElementById('Mobile').value = oSShipping[0].Mobile;
    document.getElementById('Email').value = oSShipping[0].Email;
    document.getElementById('Fax').value = oSShipping[0].Fax;

    var oSCuts = viewObject.viewModel.data.deliverySchedule();
    viewObject.viewModel.data["deliverySchedules"] = ko.observableArray();
    var i = 0;

    while (oSCuts[i]) {
        var oCut = new DeliveryScheduleItem();
        oCut.load(oSCuts[i].Pk_DeliverySchedule, oSCuts[i].DeliveryDate, oSCuts[i].Quantity, oSCuts[i].Comments, oSCuts[i].DeliveryCompleted, oSCuts[i].DeliveredDate);
        viewObject.viewModel.data["deliverySchedules"].push(oCut);
        i++;
    }
    configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "FinishedGoods", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });
}


function afterOneToManyDialogShow() {






}

//function setUpCustomerSearch() {
//    //Enquiry

//    //cleanSearchDialog();

//    $("#srchHeader").text("Customer Search");

//    //Adding Search fields
//    var txtFieldCustomerName = "<input type='text' id='txtdlgCustomerName' placeholder='Customer Name' class='input-large search-query'/>&nbsp;&nbsp;";

//    $("#dlgSearchFields").append(txtFieldCustomerName);


//    $('#MainSearhContainer').jtable({
//        title: 'Customer List',
//        paging: true,
//        pageSize: 15,
//        selecting: true, //Enable selecting
//        multiselect: false, //Allow multiple selecting
//        selectingCheckboxes: true,
//        defaultSorting: 'Name ASC',
//        actions: {
//            listAction: '/Customer/CustomerListByFiter',
//        },
//        fields: {
//            Pk_Customer: {
//                title: 'Customer ID',
//                key: true
//            },
//            CustomerName: {
//                title: 'Customer Name',
//                edit: false
//            },
//            City: {
//                title: 'City',
//            },
//            CustomerContact: {
//                title: 'Contact'
//            },
//            LandLine: {
//                title: 'Land Line'
//            }
//        }
//    });

//    $('#LoadRecordsButton').click(function (e) {
//        e.preventDefault();
//        $('#MainSearhContainer').jtable('load', {
//            CustomerName: $('#txtdlgCustomerName').val()
//        });
//    });
//    $('#LoadRecordsButton').click();

//    $('#cmdSearch').click(function (e) {
//        e.preventDefault();
//        $('#MainSearhContainer').jtable('load', {
//            CustomerName: $('#txtdlgCustomerName').val()
//        });
//    });

//    $('#cmdDone').click(function (e) {
//        e.preventDefault();
//        var rows = $('#MainSearhContainer').jtable('selectedRows');
//        $('#txtFk_Customer').wgReferenceField("setData", rows[0].keyValue);
//        Customer = rows[0].keyValue;
//        $("#searchDialog").modal("hide");
//    });

//}
function setUpOrderSearch(viewObject) {

    $("#srchOrderHeader").text("Order Search");


    $('#OrderSearchContainer').jtable({
        title: 'Order List',
        paging: true,
        pageSize: 15,
        selecting: true, //Enable selecting
        multiselect: false, //Allow multiple selecting
        selectingCheckboxes: true,
        actions: {
            listAction: '/Order/OrderListByFiter'
        },
        fields: {
            Pk_Order: {
                title: 'Order No',
                key: true
            },
            OrderDate: {
                title: 'Order Date'
            },
            Fk_Customer:{
                title:'Customer Id',
                list:false
            },
            CustomerName: {
                title: 'CustomerName'
            },
            ProductName: {
                title: 'ProductName',
                edit: false
            },
            Quantity: {
                title: 'Quantity'
            }
        }
    });

    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#OrderSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgOrderCustomerName').val(),
            Pk_Order: $('#txtdlgOrderNo').val(),
            FromOrderDate: $('#txtdlgOrderFromDate').val(),
            ToOrderDate: $('#txtdlgOrderToDate').val()
        });
    });

    $('#LoadRecordsButton').click();

    $('#txtdlgOrderCustomerName').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#OrderSearchContainer').jtable('load', {
                CustomerName: $('#txtdlgOrderCustomerName').val()
            });
        }
    });

    $('#txtdlgOrderNo').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#OrderSearchContainer').jtable('load', {
                Pk_Order: $('#txtdlgOrderNo').val()
            });
        }
    });

    $('#txtdlgOrderFromDate').change(function (e) {
        e.preventDefault();
        $('#OrderSearchContainer').jtable('load', {
            FromOrderDate: $('#txtdlgOrderFromDate').val(),
            ToOrderDate: $('#txtdlgOrderToDate').val()
        });
    });
    $('#txtdlgOrderToDate').change(function (e) {
        e.preventDefault();
        $('#OrderSearchContainer').jtable('load', {
            FromOrderDate: $('#txtdlgOrderFromDate').val(),
            ToOrderDate: $('#txtdlgOrderToDate').val()
        });
    });

    $('#cmdOrderSearch').click(function (e) {
        e.preventDefault();

        $('#OrderSearchContainer').jtable('load', {
            CustomerName: $('#txtdlgOrderCustomerName').val(),
            Pk_Order: $('#txtdlgOrderNo').val(),
            FromOrderDate: $('#txtdlgOrderDate').val()
        });
    });

    $('#cmdOrderDone').click(function (e) {
        e.preventDefault();
        var rows = $('#OrderSearchContainer').jtable('selectedRows');
        $("#dataDialog").modal("hide");
        document.getElementById('Fk_Order').value= rows[0].keyValue;
        $('#txtFk_Customer').wgReferenceField("setData", rows[0].data.Fk_Customer);
        document.getElementById('Product').value = rows[0].data.ProductName;
        

        _comLayer.parameters.clear();
        _comLayer.parameters.add("Id", rows[0].keyValue);


        var oResult = _comLayer.executeSyncAction("/Order/Load", _comLayer.parameters);

        if (oResult.success) {


            var oSCuts = oResult.data.deliverySchedule;
            viewObject.viewModel.data["deliverySchedules"] = ko.observableArray();
            var i = 0;

            while (oSCuts[i]) {
                var oCut = new DeliveryScheduleItem();
                oCut.load(oSCuts[i].Pk_DeliverySchedule, oSCuts[i].DeliveryDate, oSCuts[i].Quantity, oSCuts[i].Comments, oSCuts[i].DeliveryCompleted, oSCuts[i].DeliveredDate);
                viewObject.viewModel.data["deliverySchedules"].push(oCut);
                i++;
            }
            configureOne2Many("#cmdAddSchedule", '#divDeliveryschedule', "#cmdSaveDeliverySchedule", viewObject, "deliverySchedules", "FinishedGoods", "_AddDeliverySchedule", function () { return new DeliveryScheduleItem() });
        }


    });

}
function ReferenceFieldNotInitilized(viewModel) {

    $('#txtFk_Customer').wgReferenceField({
        keyProperty: "Fk_Customer",
        displayProperty: "CustomerName",
        loadPath: "Customer/Load",
        viewModel: viewModel
    });

    //$('#txtFk_Order').wgReferenceField({
    //    keyProperty: "Pk_Order",
    //    displayProperty: "Pk_Order",
    //    loadPath: "Order/Load",
    //    viewModel: viewModel
    //});

    if (viewModel.data != null) {
        //$('#txtFk_Order').wgReferenceField("setData", viewModel.data["Fk_Order"]);
        $('#txtFk_Customer').wgReferenceField("setData", viewModel.data["Fk_Customer"]);
    }
}




