﻿//Name          : Order Operations ----javascript files
//Description   : Contains the  delivery data details
//                 1. Jtable Loading and Columns Descriptions 
//Author        : shantha
//Date            :   17/08/2015
//CRH Number      :   SL0030-SL0035
//Modifications : 



function DeliveryScheduleItem() {
    this.data = new Object();
    this.data["Pk_DeliverySechedule"] = "";
    this.data["DeliveryDate"] = "";
    this.data["Quantity"] = "";
    this.data["Comments"] = "";
    this.data["DeliveryCompleted"] = null;
    this.data["DeliveredDate"] = "";

    this.updateValues = function () {
    };

    this.load = function (Pk_DeliverySchedule, DeliveryDate, Quantity, Comments, DeliveryCompleted, DeliveredDate) {
        this.data["Pk_DeliverySchedule"] = Pk_DeliverySchedule;
        this.data["DeliveryDate"] = DeliveryDate;
        this.data["Quantity"] = Quantity;
        this.data["Comments"] = Comments;
        this.data["DeliveryCompleted"] = DeliveryCompleted;
        this.data["DeliveredDate"] = DeliveredDate;

    };
}